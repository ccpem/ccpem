#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import sys
import pytest
import os
import argparse
import ccpem_core


'''
N.B. Three levels of unit tests marked by tags in the class / function name.

   (1) Unit test (to execute in < 1sec, run always)
       class TestFoo
           or
       def test_foo

   (2) Integration tests (to execute in < 1min, run frequently)
       class TestIntegration
           or
       def test_integration

   (3) Performance tests (to execute in > 1min, run rarely / for development)
       class TestPerformance
           or
       def test_performance
'''
# Coverage settings
unit_test_only = ('not (performance or integration '
                  'or Performance or Integration)')
unit_and_integration = 'not performance'
unit_integration_performance = ''


ignore = []
# XXX ignore EMDB sfftk unittests for now
from ccpem_progs import emdb_sfftk
ignore.append(os.path.dirname(emdb_sfftk.__file__))

from ccpem_progs import emdb_xml_translator
ignore.append(os.path.dirname(emdb_xml_translator.__file__))

# XXX ignore Beeby lab symm unittests for now
from ccpem_progs import symm
ignore.append(os.path.dirname(symm.__file__))

# # XXX Debug
# try:
#     import xdist
#     print 'Xdist path : ', xdist.__file__
# except ImportError:
#     print 'Xdist not available'

def unittest_args():
    '''
    Unit test option parser.
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('-n',
                        '--ncpu',
                        help='number of CPUs',
                        type=int,
                        default=2)
    parser.add_argument('-c',
                        '--coverage',
                        help='coverage of test',
                        choices=['unit', 'integration', 'performance'],
                        type=str,
                        default='integration')
    parser.add_argument('-p',
                        '--path',
                        help='root path (will test recursively from this path)',
                        type=str,
                        default=None)
    parser.add_argument('--ignore',
                        help='paths to exclude from testing',
                        nargs='*',
                        type=str,
                        default=[])
    parser.add_argument('-xml',
                        '--xml_output',
                        help='output results.xml',
                        type=bool,
                        default=False)
    return parser.parse_args()

def main(args):
    '''
    See pytest docs for usage and invocations:
       http://doc.pytest.org/en/latest/usage.html
       pytest.main(sys.argv[1:])
    '''
    args = unittest_args()
    # Set test coverage
    coverage = unit_and_integration
    if args.coverage == 'unit':
        coverage = unit_test_only
    if args.coverage == 'performance':
        coverage = unit_integration_performance
    # Set number of CPUs available
    n_cpu = args.ncpu
    if args.path is None:
        ccpem_core_path = os.path.dirname(ccpem_core.__file__)
        # Move to root directory to include src and unittest directories
        path = os.path.dirname(os.path.dirname(ccpem_core_path))
        print "Running all tests in", path
    else:
        path = args.path
    ignore.extend(args.ignore)
    # Run pytest
    #     N.B. no doctest
    run_args = [path,
                 '-p', 'no:doctest',
                 '-k', coverage]
    for path in ignore:
        run_args += ['--ignore', path]
    if n_cpu > 1:
        run_args += ['-n', str(n_cpu)]
    if args.xml_output:
        run_args += ['--junitxml', 'results.xml']
    
    try:
        # If sip is available, turn off destroy on exit behaviour
        # (Destructors are called in random order which causes a segfault on
        # exit if multiple tests are run which use PyQt4)
        try:
            from PyQt4 import sip
        except ImportError:
            import sip
        sip.setdestroyonexit(False)
    except:
        print("Warning: sip is not available. The test runner might fail "
              "with segmentation faults after running PyQt4 tests.")
        pass

    # Work around for matplotlib Qt4Agg backend causing problems with tests
    # TODO: remove this after fixing the underlying Qt4Agg problem!
    import matplotlib
    matplotlib.use('agg')

    pytest.main(run_args)

if __name__ == '__main__':
    main(sys.argv)

