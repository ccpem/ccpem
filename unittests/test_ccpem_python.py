#
#     Copyright (C) 2018 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
"""
Tests to make sure the Python environment (i.e. ccpem-python) is working as expected
"""

import unittest

class CCPEMPythonTest(unittest.TestCase):
    '''
    Tests for ccpem-python environment
    '''
    def test_numpy_import(self):
        import numpy
        assert numpy.__version__

    def test_scipy_import(self):
        import scipy
        assert scipy.__version__

    def test_pandas_import(self):
        import pandas
        assert pandas.__version__

    def test_matplotlib_import(self):
        import matplotlib
        assert matplotlib.__version__

    def test_mpi4py_import(self):
        import mpi4py
        assert mpi4py.__version__

    def test_pyfftw_import(self):
        import pyfftw
        assert pyfftw.__version__

    def test_mrcfile_import(self):
        import mrcfile
        assert mrcfile.__version__

    def test_fdrcontrol_import(self):
        import FDRcontrol
        import confidenceMapUtil
        assert hasattr(FDRcontrol, 'main')
        assert hasattr(confidenceMapUtil, 'confidenceMapMain')

    def test_gemmi(self):
        # Example from Gemmi docs
        import gemmi
        assert str(gemmi.Op('x-y,x,z+1/6') * '-x,-y,z+1/2') == '<gemmi.Op("-x+y,-x,z+2/3")>'
