# N.B. runs all test scripts are csh therefore csh or similar (tcsh) must be installed
# Will be put into python framework at a later date
printf "\nTEST MRC Image libraries\n"
#
WORKDIR=${PWD}

if [ -z "$CCPEMINSTALL" ];
then
  printf "\n** Please source ccpem.setup first\n"
  exit
fi

# Remove any pre-existing log files if present
rm -f ./test_output_allspace.log
rm -f ./test_hlxtest.log
rm -f ./test_output_allspace.log
rm -f ./test_output_job2010.log
rm -f ./test_origmerg_test.log
rm -f ./test_output_fftir.log

# Expected results assumes g77 compiler
# These tests will converted to a in python framework in the future
# Need to run all MRC image tests from xx/MRCImageLibraries/image2010/test
source $CCPEMINSTALL/image.setup-sh
cd $IMAGETEST

## allspace test
printf "\n** Test allspace\n"
(
tcsh -c "source $CCPEMINSTALL/image.setup-csh; ./allspace.com"
) | tee $WORKDIR/test_output_allspace.log

## fftir test
printf "\n** Test fftir\n"
(
tcsh -c "source $CCPEMINSTALL/image.setup-csh; ./fftir.com"
) | tee $WORKDIR/test_output_fftir.log

# job2010 test [must be run post fftir]
printf "\n** Test job2010\n"
(
tcsh -c "source $CCPEMINSTALL/image.setup-csh; ./job2010.com"
) | tee $WORKDIR/test_output_job2010.log

# ctfg000 test
printf "\n** Test ctfg000.com\n"
(
tcsh -c "source $CCPEMINSTALL/image.setup-csh; ./ctfg000.com"
) | tee $WORKDIR/test_output_ctfg000.log

# do_s730_1150 test
# CCP4:   Open failed: File: s730.bck [not also in expected results]
printf "\n** Test do_s730_1150.com\n"
(
tcsh -c "source $CCPEMINSTALL/image.setup-csh; ./do_s730_1150.com"
) | tee $WORKDIR/test_output_do_s730_1150.log

# origmerg test
printf "\n** Test origmerg_test.com\n"
(
tcsh -c "source $CCPEMINSTALL/image.setup-csh; ./origmerg_test.com"
) | tee $WORKDIR/test_origmerg_test.log

# hlxtest test
printf "\n** Test origmerg_test.com\n"
(
tcsh -c "source $CCPEMINSTALL/image.setup-csh; ./hlxtest.com"
) | tee $WORKDIR/test_hlxtest.log

## Check against expected results
printf "\n** Test expected results\n"
WARNINGS=0
## allspace test
printf "\n** Test allspace\n"
#
if grep "17  p622         69.0  3668       45.0   744     23.9  -46.5   -0.43   0.13       20.9" $WORKDIR/test_output_allspace.log 
then
  printf "\nOK : mrc allspace\n"
else
  printf "\nWARNING : mrc allspace returned unexpected results\n"
  WARNINGS=$((WARNINGS+1))
fi

## fftir test
printf "\n** Test fftir\n"
#
if grep " LABEL Mode 1: Min/Max XYZ =   1000  2999  1000  2999     0     0                " $WORKDIR/test_output_fftir.log
then
  printf "\nOK : mrc fftir\n"
else
  printf "\nWARNING : mrc fftir returned unexpected results\n"
  WARNINGS=$((WARNINGS+1))
fi

# job2010 test [must be run post fftir]
printf "\n** Test job2010\n"
#
if grep "                   scaled intensities (perimeter averaged to 7.0)   scale factor =     0.000075746" $WORKDIR/test_output_job2010.log
then
  printf "\nOK : mrc job2010\n"
else
  printf "\nWARNING : mrc job2010 returned unexpected results\n"
  WARNINGS=$((WARNINGS+1))
fi

# ctfg000 test
printf "\n** Test ctfg000.com\n"
#
if grep "   14   4    17.8   319.9  7             319.9  7   0.934" $WORKDIR/test_output_ctfg000.log
then
  printf "\nOK : mrc ctfg000.com\n"
else
  printf "\nWARNING : mrc ctfg000.com returned unexpected results\n"
  WARNINGS=$((WARNINGS+1))
fi

# do_s730_1150 test
# CCP4:   Open failed: File: s730.bck [not also in expected results]
printf "\n** Test do_s730_1150.com\n"
#
if grep " IEXTRA(12) on autoindex output      0   -86   676  -599  -877   463  -961     6     6    19    17     0,  NSTEP=    4" $WORKDIR/test_output_do_s730_1150.log
then
  printf "\nOK : mrc do_s730_1150.com\n"
else
  printf "\nWARNING : mrc do_s730_1150.com returned unexpected results\n"
  WARNINGS=$((WARNINGS+1))
fi

# origmerg test
printf "\n** Test origmerg_test.com\n"
#
if grep "    1   1  0.0015     365.2 -178.7   3144 -1 1.00000      38.6 -0.218" $WORKDIR/test_origmerg_test.log
then
  printf "\nOK : mrc origmerg_test.com\n"
else
  printf "\nWARNING : mrc origmerg_test.com returned unexpected results\n"
  WARNINGS=$((WARNINGS+1))
fi

# hlxtest test
printf "\n** Test origmerg_test.com\n"
#
if grep "0DENSITIES HAVE BEEN SCALED BY FACTOR     0.1038E+05" $WORKDIR/test_hlxtest.log
then
  printf "\nOK : mrc hlxtest.com\n"
else
  printf "\nWARNING : mrc hlxtest.com returned unexpected results\n"
  WARNINGS=$((WARNINGS+1))
fi

if (($WARNINGS>0))
then
  printf "Number of warnings : $WARNINGS"
else
  printf "ALL TESTS OK"
fi
