#!/bin/bash

MRC_PROGS_DIR=/xtal/ccpem/build/bin

### select programs here
do_header=0
do_bandpass=0
do_histok=0
do_boximage=0
do_boxim=0
do_imedit=1

### example data
example_volume=emd_5009
example_image=635

if (($do_header)); then
##Just dumps header of file.
IN=${example_volume}.map $MRC_PROGS_DIR/mrc_header
fi 

if (($do_bandpass)); then
##Bandpass fourier filter stack of images in MRC image format.
$MRC_PROGS_DIR/mrc_bandpass <<EOF
0
${example_volume}.map
${example_volume}_out.map
0.1 0.4
1
0.025
0
EOF
fi

if (($do_histok)); then
##Program to calculate histogram of an image. 
##Output to HISTO.PS (hardcoded name).
##Can be calculated over a region, in this example (1000,1000) to (2000,2000).
IN=${example_image}.mrc $MRC_PROGS_DIR/mrc_histok <<EOF
1000 2000 1000 2000
0
EOF
fi 

if (($do_boximage)); then
##Program to cut out a region of an image. The rest of the image
##is kept, but the intensity set to an average value. 
##This example is a simple square but circles and polygons can be done too.
IN=${example_image}.mrc OUT=${example_image}_out.mrc $MRC_PROGS_DIR/mrc_boximage<<EOF
4
0 0
1000 1000
1000 2000
2000 2000
2000 1000
EOF
fi

if (($do_boxim)); then
##Program to cut out a region of an image. The rest of the image
##is deleted, so the output file is smaller.
##Need to set IPIXEL=0 (4th line of input) or get weird scaling of x,y
$MRC_PROGS_DIR/mrc_boxim<<EOF
${example_image}.mrc
${example_image}_out.mrc
1000 1000
0
4
0 0
1000 1000
1000 2000
2000 2000
2000 1000
EOF
fi

if (($do_imedit)); then
rm -f ${example_image}_out.mrc
##Program to do various edits on image files.
##This example changes NX,NY,NZ and NXSTART,NYSTART,NZSTART in the
##header. But it doesn't actually change the data, so this example
##just makes the data wrong.
$MRC_PROGS_DIR/mrc_imedit<<EOF
${example_image}.mrc
${example_image}_out.mrc
1
2000 2000 1
1000 1000 1
0
EOF
fi

