#
#     Copyright (C) 2018 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
"""
Tests to make sure the other programs in the suite run as expected
"""

import os
import shutil
import subprocess
import tempfile
import unittest

from ccpem_core import settings


class BinariesTest(unittest.TestCase):
    """
    Tests for other binaries built as part of the suite.

    These use ``settings.which()`` to find the programs, so results could be
    unreliable if CCP-EM is not set up correctly or other copies of the
    programs are also on the PATH.
    """

    def setUp(self):
        super(BinariesTest, self).setUp()
        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)
        super(BinariesTest, self).tearDown()

    def check_program(self, command_name, args, expected_output_strings, expected_return_code=0):
        """Run the given command and check for non-zero exit code and expected output contents.

        The command's output is combined from both stdout and stderr.
        """
        command = settings.which(command_name)
        assert command is not None
        full_command = [command] + args
        try:
            actual_output = subprocess.check_output(full_command, stderr=subprocess.STDOUT, cwd=self.test_dir).strip()
        except subprocess.CalledProcessError as ex:
            assert ex.returncode == expected_return_code
            actual_output = ex.output
        for expected_output in expected_output_strings:
            self.assertIn(expected_output, actual_output)

    def test_ccpem_python(self):
        self.check_program("ccpem-python", ["--version"], ["Python 2.7.18"])

    def test_relion(self):
        self.check_program("relion_refine", ["--version"], ["RELION version"])

    def test_lafter(self):
        # Expect return code 1 for lafter with no arguments
        self.check_program("lafter", [], ["LAFTER v1.1"], expected_return_code=1)

    def test_sidesplitter(self):
        # Expect return code 1 for sidesplitter with no arguments
        self.check_program("sidesplitter", [], ["SIDESPLITTER V1"], expected_return_code=1)

    def test_emda(self):
        self.check_program("emda_test", [], ["iotools test ... Passed",
                                             "maptools test ... Passed",
                                             "restools test ... Passed",
                                             "fcodes test ... Passed"])

    def test_coot_version(self):
        self.check_program("coot", ["--version-full"],
                           ["[with guile 1.8.8 embedded]",
                            "[with python 2.7.18 embedded]",
                            "Binary type: ", "-x86_64-", "-python-gtk2",
                            "Enhanced-ligand-tools", "Goocanvas", "GSL", "SQLite3", "LibCurl",
                            "Builder_info: CCP-EM, Oxfordshire, UK"])

    # Coot self test returns exit status 1 for success. (This will be fixed in a future Coot version)
    def test_coot_self_test(self):
        self.check_program("coot", ["--self-test"],
                           ["Running internal self tests", "Clipper core   : OK", "Clipper contrib: OK"],
                           expected_return_code=1)

    def test_mrcfile_header(self):
        self.check_program("mrcfile-header", [], [])

    def test_mpirun_version(self):
        self.check_program("ccpem-mpirun", ["--version"], ["mpirun (Open MPI) 3.1.1"])

    def test_mpirun_job(self):
        message = "Hello from MPI"
        self.check_program("ccpem-mpirun", ["-n", "2", "--oversubscribe", "echo", message], [message])

    def test_ccpem_buccaneer(self):
        self.check_program("ccpem-buccaneer", ["--help"], ["usage: buccaneer_window.py"])

    def test_ccpem_choyce(self):
        # Deliberately do not check output. The command fails on OS X because Modeller is not installed
        self.check_program("ccpem-choyce", ["--help"], [])

    def test_ccpem_dock_em(self):
        self.check_program("ccpem-dock-em", ["--help"], ["usage: dock_em_window.py"])

    def test_ccpem_flex_em(self):
        # Deliberately do not check output. The command fails on OS X because Modeller is not installed
        self.check_program("ccpem-flex-em", ["--help"], [])

    def test_ccpem_locscale(self):
        self.check_program("ccpem-locscale", ["--help"], ["usage: loc_scale_window.py"])

    def test_ccpem_molrep(self):
        self.check_program("ccpem-molrep", ["--help"], ["usage: molrep_window.py"])

    def test_ccpem_mrc_allspacea(self):
        self.check_program("ccpem-mrc-allspacea", ["--help"], ["usage: mrc_allspacea_window.py"])

    def test_ccpem_mrc_mrc2tif(self):
        self.check_program("ccpem-mrc-mrc2tif", ["--help"], ["usage: mrc_mrc2tif_window.py"])

    def test_ccpem_mrc_to_mtz(self):
        self.check_program("ccpem-mrc-to-mtz", ["--help"], ["usage: mrc_to_mtz_window.py"])

    def test_ccpem_nautilus(self):
        self.check_program("ccpem-nautilus", ["--help"], ["usage: nautilus_window.py"])

    def test_ccpem_prosmart(self):
        self.check_program("ccpem-prosmart", ["--help"], ["usage: prosmart_window.py"])

    def test_ccpem_refmac(self):
        self.check_program("ccpem-refmac", ["--help"], ["usage: refmac_window.py"])

    def test_ccpem_ribfind(self):
        self.check_program("ccpem-ribfind", ["--help"], ["usage: ribfind_window.py"])

    def test_ccpem_shake(self):
        self.check_program("ccpem-shake", ["--help"], ["usage: shake_window.py"])

    def test_ccpem_tempy_diffmap(self):
        self.check_program("ccpem-tempy-diffmap", ["--help"], ["usage: difference_map_window.py"])

    def test_ccpem_tempy_sccc(self):
        self.check_program("ccpem-tempy-sccc", ["--help"], ["usage: sccc_window.py"])

    def test_ccpem_tempy_smoc(self):
        self.check_program("ccpem-tempy-smoc", ["--help"], ["usage: smoc_window.py"])
