#!/bin/bash

cd `dirname "$0"`
dir=`pwd`
master=`dirname $dir`
subdir=`basename $dir`

echo "";
echo "";
echo "################################ Install CCP-EM ################################";

if [[ "$dir" == *" "* ]]; then
  echo
  echo "ERROR: there are spaces in the path to the CCP-EM installation directory!"
  echo
  echo "This is not supported. Please try to install again in a location without"
  echo "spaces in the path, or contact ccpem@stfc.ac.uk for assistance."
  echo
  exit 1
fi

################### confirm CCP-EM licence ###################

if [ -f $HOME/.agree2ccpemv1 ]; then
    agreed=yes
else
    more conditions.txt
    echo
    echo -n "Do you accept the terms of the CCP-EM licence? y/n [n] "
    read ans
    if [ "$ans" = y -o $ans = Y -o $ans = yes -o $ans = YES ]; then 
        agreed=yes
    fi
fi

if [ "$agreed" = "yes" ]; then
    echo `date` "$subdir" >> $HOME/.agree2ccpemv1
else
    echo " You have indicated that you do not agree to the conditions";
    echo " of use for the CCP-EM package. Please delete all CCP-EM software";
    echo " from your system. If you have any further queries please refer ";
    echo " to the CCP-EM web pages on http://www.ccpem.ac.uk/ or contact" ;
    echo " ccpem@stfc.ac.uk";
    exit 1
fi

################### final setup ###################

echo 'Setting up paths...'

sed "s|\(CCPEM=\).*|\1$dir|" include/setup_ccpem.sh.in > setup_ccpem.sh

sed "s|\(setenv CCPEM \).*|\1$dir|" include/setup_ccpem.csh.in > setup_ccpem.csh


# the --minimal option is for quick testing only
[ "$1" = "--minimal" ] && exit

echo 'Compiling py-files...'
(CCPEM=$dir $dir/bin/ccpem-python -Wignore -m compileall \
 $dir/lib/py2 $dir/lib/python2.7 >/dev/null)


# Clear extended file attributes to avoid OS X Gatekeeper problems
if [ "$(uname)" = "Darwin" ]; then
  echo "Clearing extended file attributes..."
  xattr -rc . >/dev/null 2>&1
fi

# Check for libfontconfig / libfreetype library dependency problems
# If found, try moving libfreetype files and see if that fixes it
if [ "$(uname)" = "Linux" ]; then
#  echo "Checking for common library problems..."
  if ! $dir/bin/ccpem-python -c "import PyQt4.QtGui" >/dev/null 2>&1; then
#    echo "Problem found. Trying to fix..."
    moved_dir=$dir/lib/moved
    mkdir -p $moved_dir
    mv $dir/lib/libfreetype.so.* $moved_dir
    if ! $dir/bin/ccpem-python -c "import PyQt4.QtGui" >/dev/null 2>&1; then
      echo
      echo "ERROR: cannot load PyQt GUI libraries. Please see"
      echo "https://www.ccpem.ac.uk/user_help/known_issues.php"
      echo "or contact ccpem@stfc.ac.uk for assistance."
      echo
      mv $moved_dir/libfreetype.so.* $dir/lib
#    else
#      echo "Fixed."
    fi
#  else
#    echo "No problems found."
  fi
fi

################### checks for common problems ###################

if ! $dir/bin/ccpem-python -c "import numpy" >/dev/null 2>&1; then
    echo
    echo "ERROR: starting an example CCP-EM program failed !!!"
    echo "Please check you downloaded the version for your operating system,"
    echo "or contact ccpem@stfc.ac.uk for assistance."
    echo
fi

# Try and find CCP4 if on Mac
for ccp4_ver in 8.0 7.1 7.0; do
  MAC_CCP4=/Applications/ccp4-${ccp4_ver}/bin/ccp4.setup-sh
  if [[ -e $MAC_CCP4 ]]; then
    source $MAC_CCP4
    break
  fi
done

if [ -z "$CCP4" ]; then
    echo
    echo "  Warning: CCP4 not found!"
    echo
    echo "  CCP-EM will still run, but several tasks that use CCP4 programs will be"
    echo "  unavailable. To use those tasks, please install and set up CCP4 before"
    echo "  running CCP-EM. CCP4 is available from www.ccp4.ac.uk."
    echo
else
    output="$($CCP4/bin/refmac5 -i | head -1)"
    if [ -z "$output" ]; then
        echo
        echo "  Warning: problem running an example CCP4 program!"
        echo
        echo "  CCP-EM will still run, but several tasks that use CCP4 programs will be"
        echo "  unavailable or might run with errors. Please check your CCP4 installation,"
        echo "  or contact ccpem@stfc.ac.uk for assistance."
        echo
    else
        ccp4_version=${output##* }  # bash substring removal: keep only the text after the last space
        ccp4_majmin=${ccp4_version%.*}  # bash substring removal: keep up to the last '.'
        ccp4_maj=${ccp4_majmin%.*}  # bash substring removal: keep everything before the '.'
        ccp4_min=${ccp4_majmin#*.}  # bash substring removal: keep everything after the '.'
        ccp4_patch=${ccp4_version##*.}  # bash substring removal: keep only the text after the last '.'
        if [[ "$ccp4_maj" -lt 7 ]]; then
            echo
            echo "  Your CCP4 installation (version ${ccp4_version}) is not supported."
            echo "  This version of CCP-EM requires CCP4 version 7.0 or newer."
            echo
        # Test patch version numerically in base 10 (to avoid interpretation as octal literal)
        elif [[ "$ccp4_majmin" == "7.0" && "10#$ccp4_patch" -lt 72 ]]; then
            echo
            echo "  Your CCP4 installation (version ${ccp4_version}) appears to be out of date."
            echo "  We recommend using version 7.0.072 or later for best results."
            echo
        fi
    fi
fi

if [ "$(uname)" = "Darwin" ]; then
    if [ ! -e "/usr/X11" ]; then
        echo
        echo "  Warning: XQuartz not found!"
        echo
        echo "  Graphical programs in CCP-EM require XQuartz."
        echo "  Please install the latest version from https://www.xquartz.org/"
        echo
    else
        xq_version="$(defaults read /Applications/Utilities/XQuartz.app/Contents/Info.plist CFBundleShortVersionString)"
        if [ -z "$xq_version" ]; then
            echo
            echo "  Warning: problem getting XQuartz version information"
            echo
            echo "  Please check your XQuartz installation, or contact ccpem@stfc.ac.uk for assistance."
            echo
        else
            xq_majmin=${xq_version%.*}  # bash substring removal: keep up to the last '.'
            xq_patch=${xq_version##*.}  # bash substring removal: keep only from the last '.' onwards
            if [[ "$xq_majmin" != "2.8" && "$xq_majmin" != "2.7" \
                  || "$xq_majmin" = "2.7" && "10#$xq_patch" -lt 11 ]]; then
                echo
                echo "  Warning: XQuartz version is too old or not recognised"
                echo
                echo "  Your XQuartz version (version ${xq_version}) is out of date and might cause"
                echo "  some graphical CCP-EM programs to fail."
                echo
                echo "  Please install the latest version from https://www.xquartz.org/, or update"
                echo "  using the \"Check for X11 Updates\" option in the XQuartz menu."
                echo
            fi
        fi
    fi
fi

echo
echo "Before running CCPEM programs from command line,"
echo "         source setup_ccpem.sh"
echo "         (or setup_ccpem.csh if you use csh)."
echo
echo "To install Strudel, please run"
echo "     install_strudel.sh"
echo

################### Install Modeller ###################
# Only required for linux; modeller found automatically on mac
if [ "$(uname)" = "Linux" ]; then
    if [ -d "$dir/lib/modeller" ]; then
        echo
        echo "To install modeller (for Flex-EM and other programs) please run ";
        echo "     install_modeller.sh";
        echo
#       Don't run now as halts build system
#        $dir/install_modeller.sh
    fi
fi
