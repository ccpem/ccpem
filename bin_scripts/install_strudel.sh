#!/bin/bash

strudel_lib="strudel-libs_ver-4.0_voxel-0.5"
strudel_lib_tar="${strudel_lib}.tar.gz"

cd `dirname "$0"`
dir=`pwd`

echo
#echo "  - Download Strudel Score library and set-up plugin for ChimeraX ? y/n [y] "
#read ans
#if [[ "$ans" == "n" || "$ans" == "N" || "$ans" == "no" || "$ans" == "NO" ]]; then 
#    echo
#    echo "  Download Strudel Score library from https://ftp.ebi.ac.uk/pub/databases/emdb_vault/strudel_libs/$strudel_lib_tar"
#    echo "  To set up Strudel Score plugin for ChimeraX, "
#    echo "  Please follow the instructions here: "
#    echo "  https://cxtoolshed.rbvi.ucsf.edu/apps/chimeraxstrudelscore "
#    echo "  or contact ccpem@stfc.ac.uk for assistance."
#    echo
#else
#echo
echo "     Please wait: Downloading Strudel Score library to lib/strudel  "
echo
if [ ! -d lib/strudel ]; then
	mkdir lib/strudel
fi
if [ ! -f lib/strudel/$strudel_lib_tar ]; then
	wget --directory-prefix=lib/strudel https://ftp.ebi.ac.uk/pub/databases/emdb_vault/strudel_libs/$strudel_lib_tar 2>/dev/null
fi
if [ ! -f lib/strudel/$strudel_lib_tar ]; then
	curl -o lib/strudel/$strudel_lib_tar https://ftp.ebi.ac.uk/pub/databases/emdb_vault/strudel_libs/$strudel_lib_tar 2>/dev/null
fi
if [ ! -f lib/strudel/$strudel_lib_tar ]; then
ftp -n "ftp.ebi.ac.uk" << END
cd "pub/databases/emdb_vault/strudel_libs"
get $strudel_lib.tar.gz "lib/strudel" 
quit
END
fi

if [ -f lib/strudel/$strudel_lib_tar ]; then
	tar -xf lib/strudel/$strudel_lib_tar -C lib/strudel >/dev/null 2>&1

	echo "  Searching for local ChimeraX installation "
	output="$(type chimeraX 2> /dev/null)"
	output="$(echo $output | awk '{print $NF}' )"
	chimerax_path=$output
	if [ -z "$output" ]; then
		output="$(which ChimeraX)"
		chimerax_path=$output
	fi
	
	if [[ -z "$output" && "$(uname)" = "Darwin" ]]; then
		output="$(find /Applications -type f -name 'ChimeraX' 2> /dev/null)"
	elif [[ -z "$output" && "$(uname)" = "Linux" ]]; then
		output="$(find /usr -type f -iname 'chimerax' 2> /dev/null)"
	fi
	if [ -z "$output" ]; then
		output="$(locate -i ChimeraX | grep -i ChimeraX 2> /dev/null)"
	fi
	
	if [ -n "$output" ]; then
		chimerax_path=''
		re='^[0-9]+$'
		for i in $output; do
			filename=$i
			basename="$(basename $i)"
			if [[ -f "$filename" && -x "$filename" ]]; then
				if [[ ( $basename == 'ChimeraX'* || $basename == 'chimerax'* ) && ( $basename != *"."* || ${basename: -1} =~ $re ) ]]; then
					chimerax_path=$filename
					break
				fi
			fi
		done
	fi
	
	
	if [ -z "$output" ] || [ ${#chimerax_path} == 0 ]; then
		echo
		echo "  Warning: ChimeraX installation not found!"
		echo
		echo "  CCP-EM will still run, but Strudel Score that runs with ChimeraX will be"
		echo "  unusable. Please check your ChimeraX installation,"
		echo "  To set up Strudel Score plugin for ChimeraX, "
		echo "  Please follow the instructions here: "
		echo "  https://cxtoolshed.rbvi.ucsf.edu/apps/chimeraxstrudelscore "
		echo "  or contact ccpem@stfc.ac.uk for assistance."
		echo
	else
		chim_path="$(dirname $chimerax_path)"
		PATH=$PATH:$chim_path
		echo
		echo "     ChimeraX installation found : " $chimerax_path
		$chimerax_path --exit --nogui --cmd "toolshed install strudelscore" >strudel_setup.out 2>strudel_setup.err
		if grep -Fq "Error preloading available bundles" strudel_setup.out
			then
			echo "     Strudel Score setup failed!, ensure ChimeraX version is > 1.2"
			echo "     To set up Strudel Score plugin for ChimeraX, "
			echo "     Please follow the instructions here: "
			echo "     https://cxtoolshed.rbvi.ucsf.edu/apps/chimeraxstrudelscore "
			echo "     or contact ccpem@stfc.ac.uk for assistance."
			echo
		else
			$dir/bin/ccpem-python $dir/bin/strudel_setChimeraX.py $chimerax_path
			$chimerax_path --exit --nogui --cmd "strudel setLib $dir/lib/strudel/$strudel_lib" >>strudel_setup.out 2>>strudel_setup.err
			if grep -Fq "Unknown command: strudel" strudel_setup.out
			then
				echo "     Strudel Score setup failed!, ensure ChimeraX version is > 1.2"
				echo "     To set up Strudel Score plugin for ChimeraX, "
				echo "     Please follow the instructions here: "
				echo "     https://cxtoolshed.rbvi.ucsf.edu/apps/chimeraxstrudelscore "
				echo "     or contact ccpem@stfc.ac.uk for assistance."
				echo
			else
				echo "     To test Strudel Score plugin for ChimeraX, "
				echo "     please follow the tutorial here: "
				echo "     https://cxtoolshed.rbvi.ucsf.edu/apps/chimeraxstrudelscore "
				echo
			fi
		fi
	
		if [ -f "strudel_setup.out" ]; then
			rm strudel_setup.out
		fi
		if [ -f "strudel_setup.err" ]; then
			rm strudel_setup.err
		fi
	fi
else
	echo "     Strudel Score library download failed!"
	echo "     Download from: https://ftp.ebi.ac.uk/pub/databases/emdb_vault/strudel_libs "
	echo "     To set up Strudel Score plugin for ChimeraX, "
	echo "     Please follow the instructions here: "
	echo "     https://cxtoolshed.rbvi.ucsf.edu/apps/chimeraxstrudelscore "
	echo "     or contact ccpem@stfc.ac.uk for assistance."
	echo
fi
