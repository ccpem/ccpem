#!/bin/bash

cd `dirname "$0"`
dir=`pwd`

################### install Modeller ###################
echo "";
echo " Modeller Install";
echo "";
echo " This is used for Flex-EM and other CCP-EM supported applications.";
echo "";
echo " N.B. you will require a seperate license and license key for ";
echo " Modeller.  This is available here: ";
echo "     https://salilab.org/modeller/registration.html";
echo "";
echo -n " Do you want to install Modeller? y/n [n] "
read ans
if [ "$ans" = y -o $ans = Y -o $ans = yes -o $ans = YES ]; then 
    agreed=yes
fi

if [ "$agreed" = "yes" ]; then
    echo -n " Please enter Modeller license key: "
    read key
    echo "Install Modeller";
    cd $dir/lib/modeller
    ./Install << EOF
3
$dir/lib/modeller/install
$key
EOF
cd $dir

else
    echo " You have indicated that you do not want to install modeller. ";
    echo " This means Flex-EM and some other programs will be disabled. ";
    echo " If you would like to install Modeller for CCP-EM later please run :";
    echo "     <ccpem>/install_modeller.sh" ;
    echo "";
    exit 1
fi
