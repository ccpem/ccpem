#!/bin/bash

# ccpem-mpirun
#
# Runs mpirun with a modified environment to ensure it finds its dependencies
# correctly.

if [[ -z "$CCPEM" ]]; then
    echo "Error: CCPEM environment variable must be set to run ccpem-mpirun"
    exit 1
fi

export OPAL_PREFIX=$CCPEM
export OPAL_BINDIR=$CCPEM/libexec/openmpi

# Check if the Linux kernel supports CMA for vader single-copy transfers.
# This is the same test that is done internally by the vader component. If CMA
# is not available, it prints a warning to stderr, so we set an environment
# variable first to avoid the warning.
if [[ -e /proc/sys/kernel/yama/ptrace_scope && \
      "$(cat /proc/sys/kernel/yama/ptrace_scope)" != "0" ]]; then
  export OMPI_MCA_btl_vader_single_copy_mechanism=none
fi

exec $OPAL_BINDIR/mpirun "$@"

