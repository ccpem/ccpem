#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import os
import numpy as np
from scipy import stats
import mrcfile
from PIL import Image, ImageSequence

def main():
    '''
    Apply gain correction to image stacks
    '''
    # Set movie and gain reference
    movie = '/Users/tom/ccpem/data/relion_3.0_beta_tutorial/relion30_tutorial/Movies/20170629_00021_frameImage.tiff'
    gain_ref = '/Users/tom/ccpem/data/relion_3.0_beta_tutorial/relion30_tutorial/Movies/gain.mrc'

    # Open gain corrected mrcfile
    print '\n\nGain reference : ', os.path.basename(gain_ref)
    with mrcfile.open(gain_ref) as mrc:
        ref_array = mrc.data
    print stats.describe(ref_array)

    # Open tiff movie and iterate through frames
    im = Image.open(movie)
    movie_basename = os.path.basename(movie)
    print '\n\nMovie : ', movie_basename
    for i, frame in enumerate(ImageSequence.Iterator(im)):
        print 'Frame : ', i
        # Convert to numpy array
        array = np.array(frame)
        print 'Raw image'
        print stats.describe(array)
        # Apply gain correction
        print 'Gain corrected'
        frame_gc = ref_array * array
        print stats.describe(frame_gc)
        # Output 
        path = '/Users/tom/temp'
        path = os.path.join(
            path,
            str(i)+'_gc_'+os.path.splitext(movie_basename)[0]+'.tiff')
        im_gc = Image.fromarray(frame_gc)
        im_gc.save(path)
        print 'Save : ', path

if __name__ == '__main__':
    main()