

from Process import MapEdit
import copy
import os
import numpy as np
import math
from numpy.fft import fftn,fftshift,ifftn,ifftshift
#from scipy.fftpack import fftn, ifftn, fftshift
from scipy.ndimage.interpolation import  shift, affine_transform, zoom,\
                        map_coordinates
from scipy.ndimage import laplace, uniform_filter, generic_filter,\
                        minimum_filter, measurements
from scipy.ndimage.morphology import binary_opening, binary_dilation,\
                        binary_closing
import mrcfile
import warnings
# For CCP-EM mac install.
try:
    from TEMPy.ShowPlot import Plot
except RuntimeError, ImportError:
    Plot = None
import gc

try:
    import pyfftw
    pyfftw_flag = True
    #print pyfftw.simd_alignment
except ImportError:
    pyfftw_flag = False
    

class Filter(MapEdit):
    def __init__(self, MapInst,copy=False):
        if type(MapInst) is MapEdit:
            if not copy: self.__dict__ = MapInst.__dict__.copy()
            else: self.__dict__ = copy.deepcopy(MapInst.__dict__.copy())
        else:
            super(Filter, self).__init__(MapInst)
        
        #CHECK DEPENDENCIES    
        try:
            import pyfftw
            self.pyfftw_flag = True
            #print pyfftw.simd_alignment
        except ImportError:
            self.pyfftw_flag = False    

    def copy(self,deep=True):
        '''
        Copy contents to a new object
        '''
        #create MapEdit object
        copymap = self.__class__(self.mrc)
        #copy data and header
        copymap.origin = copy.deepcopy(self.origin)
        if deep: copymap.fullMap = self.fullMap.copy()
        else: copymap.fullMap = self.fullMap
        copymap.apix = copy.deepcopy(self.apix)
        copymap.dim = copy.deepcopy(self.dim)
        return copymap

    # useful when a 'safe' contour level is chosen
    # for high resolution maps with fine features on the surface, this might erode part of useful density
    def map_binary_opening(self,contour,it=1,inplace=False):
        """
        Remove isolated densities by erosion
        """
        if not inplace:
            self.data_copy()
        fp = self._grid_footprint()
        # current position can be updated based on neighbors only
        fp[1,1,1] = 0
        if inplace: self.fullMap = self.fullMap*binary_opening(
                                    self.fullMap>float(contour),
                                    structure=fp,iterations=it)
        else:
            newmap = self.copy()
            newmap.fullMap[:] = self.fullMap*binary_opening(
                                    self.fullMap>float(contour),
                                    structure=fp,iterations=it)
            return newmap

    def map_binary_closing(self,contour,it=1,inplace=False):
        """
        Close/Fill 'empty' (zero) voxels with filled (non-zero) neighbors 
        """
        if not inplace:
            self.data_copy()
        fp = self._grid_footprint()
        # current position can be updated based on neighbors only
        fp[1,1,1] = 0
        # threshold can be 1*std() to be safe?
        if inplace: self.fullMap = self.fullMap*binary_closing(
                                    self.fullMap>float(contour),
                                    structure=fp,iterations=it)
        else:
            newmap = self.copy()
            newmap.fullMap[:] = self.fullMap*binary_closing(
                                    self.fullMap>float(contour),
                                    structure=fp,iterations=it)
            return newmap
    
    def make_fourier_shell(self):
        """
        For a given grid, make a grid with sampling 
        frequencies in the range (0:0.5)
        Return:
            grid with sampling frequencies  
        """
        # numpy fftfreq : odd-> 0 to (n-1)/2 & -1 to -(n-1)/2 and even-> 0 to n/2-1 & -1 to -n/2 to check with eman
        # set frequencies in the range -0.5 to 0.5
        rad_z = np.float32(np.arange(np.floor(self.z_size()/2.0)*-1, 
                                     np.ceil(self.z_size()/2.0))/
                                     (np.floor(self.z_size())*1.0))
        rad_y = np.float32(np.arange(np.floor(self.y_size()/2.0)*-1, 
                                     np.ceil(self.y_size()/2.0))/
                                     (np.floor(self.y_size())*1.0))
        rad_x = np.float32(np.arange(np.floor(self.x_size()/2.0)*-1, 
                                     np.ceil(self.x_size()/2.0))/
                                     (np.floor(self.x_size())*1.0))

        rad_x = rad_x**2
        rad_y = rad_y**2
        rad_z = rad_z**2
        
        dist = np.sqrt(rad_z[:,None,None]+rad_y[:,None] + rad_x)
        return dist        

    def fourier_transform(self):
        """
        pythonic FFTs for maps 
        """
        #start = datetime.now()
        if pyfftw_flag:
            input_dtype = str(self.fullMap.dtype)
            output_dtype = 'complex64'
            fftoutput = pyfftw.n_byte_align_empty(self.fullMap.shape, 
                                                  n=16, dtype=output_dtype)
            '''
            if not input_dtype in ['float32','float64','longdouble']: 
                input_dtype = 'float32'
            elif input_dtype == 'float64':
                output_dtype = 'complex128'
            elif input_dtype == 'longdouble':
                output_dtype = 'clongdouble'
                
            #for r2c transforms:
            output_array_shape = \
                    self.fullMap.shape[:len(self.fullMap.shape)-1]+ \
                                        (self.fullMap.shape[-1]//2 + 1,)
            fftoutput = pyfftw.n_byte_align_empty(output_array_shape, 
                                            n=16, dtype=output_dtype)
            #check if array is byte aligned
            #TODO: can we read the map file as byte aligned? 
            if pyfftw.is_byte_aligned(self.fullMap):
                fft = pyfftw.FFTW(self.fullMap,fftoutput,
                                  direction='FFTW_FORWARD',axes=(0,1,2),
                                  flags=['FFTW_ESTIMATE'])
                flag_alignedarray = 0
            else:
            '''
            #real to complex fftw transforms generate output array with last dimension sized n//2 + 1
            #to be consistent with the numpy ffts (output dimensions), copy the input array as a complex type        
            inputarray = pyfftw.empty_aligned(self.fullMap.shape,
                                              n=16,dtype='complex64')
            fft = pyfftw.FFTW(inputarray,fftoutput,
                              direction='FFTW_FORWARD',axes=(0,1,2),
                              flags=['FFTW_ESTIMATE'])
            inputarray[:,:,:] = self.fullMap
            fft()
        else:
            #TODO: warning raises error in tasks
            #warnings.warn("PyFFTw not found!, using numpy fft")
            print "PyFFTw not found!, using numpy fft"
            fftoutput = np.fft.fftn(self.fullMap)   
        #print datetime.now()-start
        #TODO: set header for ftmaps
        ftmap = self.copy(deep=False)
        ftmap.fullMap = fftoutput
        if pyfftw_flag: del inputarray
        return ftmap
    
    def inv_fourier_transform(self,ftmap,output_dtype=None,
                              output_shape=None):
        """
        Calculate inverse fourier transform
        """
        #start = datetime.now()
        if pyfftw_flag:
            input_dtype = str(ftmap.fullMap.dtype)
            '''
            if output_array_dtype is None: output_array_dtype = 'float32'
            if not input_dtype in ['complex64','complex128','clongdouble']: 
                input_dtype = 'complex64'
            elif input_dtype == 'complex128':
                output_array_dtype = 'float64'
            elif input_dtype == 'clongdouble':
                output_array_dtype = 'longdouble'
            
            #for c2r transforms:
            if output_shape is None: output_shape = \
                                    ftmap.fullMap.shape[:len(ftmap.fullMap.shape)-1]+\
                                    ((ftmap.fullMap.shape[-1] - 1)*2,)
            '''
            output_shape = ftmap.fullMap.shape
            output_array_dtype = 'complex64'
            output_array = pyfftw.empty_aligned(output_shape, 
                                                n=16, dtype=output_array_dtype)
            #check if array is byte aligned
            if pyfftw.is_byte_aligned(ftmap.fullMap):
                ifft = pyfftw.FFTW(ftmap.fullMap,output_array,
                                   direction='FFTW_BACKWARD',\
                                   axes=(0,1,2),flags=['FFTW_ESTIMATE'])#planning_timelimit=0.5)
            else:
                
                inputarray = pyfftw.n_byte_align_empty(ftmap.fullMap.shape,
                                                       n=16,
                                                       dtype=input_dtype)
                ifft = pyfftw.FFTW(inputarray,output_array,
                                   direction='FFTW_BACKWARD',\
                               axes=(0,1,2),flags=['FFTW_ESTIMATE'])#planning_timelimit=0.5)
                inputarray[:,:,:] = ftmap.fullMap[:,:,:]
            ifft()
        else:
            #TODO: warnings raises error in tasks
            #warnings.warn("PyFFTw not found!, using numpy fft")
            print "PyFFTw not found!, using numpy ifft"
            output_array = np.real(np.fft.ifftn(ftmap.fullMap))
            #np.fft.fftpack._fft_cache.clear()    
        #print datetime.now()-start
        #TODO: set invftmap header
        invftmap = self.copy(deep=False)
        if output_dtype is None:
            invftmap.fullMap = output_array.real.astype(np.float32,copy=False)
        else:
            try: invftmap.fullMap = output_array.real.astype(output_dtype,copy=False)
            except: invftmap.fullMap = output_array.real.astype(np.float32,
                                                    copy=False)

        del ftmap.fullMap
        return invftmap   
            
    def fourier_filter(self,ftfilter,inplace=False,plot=False,
                       plotfile='plot'):
        """
        Apply lowpass/highpass/bandpass filters in fourier space
        
        ftfilter: filter applied on the fourier grid
        """
            
        ftmap = self.fourier_transform()
        if plot:
            dict_plot = {}
            lfreq,shell_avg = self.get_raps(fftshift(ftmap.fullMap))
            dict_plot['map'] = [lfreq[:],shell_avg[:]]
        #shift zero frequency to the center and apply the filter
        ftmap.fullMap[:] = fftshift(ftmap.fullMap) * ftfilter
        if plot:
            lfreq,shell_avg = self.get_raps(ftmap_filt)
            dict_plot['filtered'] = [lfreq,shell_avg]
            self.plot_raps(dict_plot,plotfile=plotfile)
        ftmap.fullMap[:] = ifftshift(ftmap.fullMap)
        invftmap = self.inv_fourier_transform(ftmap, 
                                        output_shape=self.fullMap.shape)
        if inplace: 
            self.fullMap = invftmap.fullMap
        else:
            newmap = self.copy(deep=False)
            newmap.fullMap = invftmap.fullMap
            return newmap
        
    @staticmethod
    def apply_filter(ftmap,ftfilter,inplace=False):
        #fftshifted ftmap
        if inplace:
            ftmap.fullMap[:] = ftmap.fullMap*ftfilter
            return ftmap
        else:
            filteredmap = ftmap.copy()
            filteredmap.fullMap[:] = ftmap.fullMap*ftfilter
            return filteredmap
        
    def get_raps(self,ftmap,step=0.02):
        '''
        Get rotationally averaged power spectra from fourier (filtered) map
        ftmap : fourier map (e.g. with any filter appied)
        step : frequency shell width [0:0.5]
        '''
        size = max(self.x_size(),self.y_size(),self.z_size())
        #shell values correspond to freq: 0-0.5 (nyquist)
        dist = self.make_fourier_shell()/max(self.apix)
        # select max spatial frequency to iterate to. low resolution map
        maxlevel = 0.5/max(self.apix)
        step = step/max(self.apix)
        x = 0.0
        nc = 0
        shell_avg = []
        lfreq = []
        highlevel = x+step
        while (x<=maxlevel-step):
            #print x,highlevel, maxlevel
            # indices between upper and lower shell bound
            fshells = ((dist < min(maxlevel,highlevel)) & (dist >= x))
            
            # radial average
            
            try:
                #shellvec = self.fancy_index_as_slices(ftmap,fshells)
                shellvec = np.take(ftmap.ravel(),
                                   np.where(fshells.ravel()))
            except:
                shellvec = ftmap[fshells]
            
            #shellvec = ftmap[fshells]
            #print np.histogram(shellvec)
            shellabs = abs(shellvec)
            nshellzero = len(np.flatnonzero(shellabs)) #or count_nonzero
            
            if nshellzero < 5:
                if highlevel == maxlevel: break
                nc += 1
                highlevel = min(maxlevel,x+(nc+1)*step) 
                #x = max(0.0,x-nc*step)
                continue
            else: nc = 0
            ##mft = np.mean(shellabs)
            #if mft == 0.0: continue
            # sq of radial avg amplitude
            shell_avg.append(np.log10(np.mean(np.square(shellabs))))
            lfreq.append((x+(highlevel-x)/2.))
            x = highlevel
            highlevel = x+step
        del fshells,shellvec,shellabs
        return lfreq,shell_avg

    def plot_raps(self,dict_plot,plotfile='plot'):
        if Plot is not None:
            plt = Plot()
            plt.lineplot(dict_plot,plotfile)
