# EMX tags as defind here (Jan 2014)
# http:#i2pc.cnb.csic.es/emx/LoadDictionaryFormat.htm
#
# Tom Burnley / CCP-EM / STFC

data_emx_tag_element_and_attributes

## Attributes

# File Name
_fileName.data              None
_fileName.object            "Micrograph, Particle"
_fileName.required          Yes
_fileName.type              String
_fileName.units             NA
_fileName.definition        "Image file name for a micrograph or particle"
_fileName.example           ;
Micrograph fileName = mic001.mrc index = 1
;

# Index
_index.data                 1
_index.object               "Micrograph, Particle"
_index.required             No
_index.type                 "String" # Should be integer - error in EMX?
_index.units                NA
_index.definition           
;
Index of an image in a multi-image file of micrographs or particles. Defaults 
to 1 if omitted.
;
_index.example              
;
Micrograph fileName = mic001.mrc index = 1
;

# Unit
_unit.data                  None
_unit.object                "Micrograph, Particle"
_unit.required              No
_unit.type                  String
_unit.Units                 NA
_unit.definition            
;
Describes the unit used for the element. This attribute has been designed for 
compatibility with other software and to remind human readers the units assigned 
to a particular magnitude. This attribute will be ignored by computer parsers, 
but should be included when writing to a file. For each magnitude the possible 
value of unit is fixed.
;
_unit.example               
;
defocusU unit = nm; 250
defocusV unit = nm; 280
defocusUAngle unit = deg; 33.34
cs unit = mm; 2.2
Voltage unit = kV; 300
;

# Version
_version.data               None
_version.object             EMX
_version.required           Yes
_version.type               real
_version.units              NA
_version.definition         "EMX version"
_version.example            "1.0"


## Elements

# Accelerating Voltage
_acceleratingVoltage.data       None
_acceleratingVoltage.micrograph None
_acceleratingVoltage.required   No
_acceleratingVoltage.type       Real
_acceleratingVoltage.units      "kiloVolts / kV"
_acceleratingVoltage.definition 
;
Acceleration voltage used to take the electron micrograph.
;

# Active Flag
_activeFlag.data            None
_activeFlag.object          "Micrograph,Particle"
_activeFlag.required        No
_activeFlag.type            Integer
_activeFlag.units           NA
_activeFlag.definition      
;
A selection flag to indicate whether the object used (>0) or not (0).
;

# Amplitude Contrast
_amplitudeContrast.data     None
_amplitudeContrast.object   Micrograph
_amplitudeContrast.required No
_amplitudeContrast.type     Real
_amplitudeContrast.units    NA
_amplitudeContrast.definition 
;
Describes the amplitude contrast contribution for the micrograph as a fraction. 
Range 0-1.
;

# Box Size
_boxSize.data               None
_boxSize.object             Particle
_boxSize.required           No
_boxSize.type               "Tuple of 2 integer numbers labeled with tags X and Y"
_boxSize.units              "pixels / px"
_boxSize.definition         "The size of a particle sub-image in a micrograph image"
_boxSize.example            ;
    X unit = px; 502 
    Y unit = px; 194 
In this context tags X and Y refers to the box x and y dimensions.
;

# Center Coord
_centerCoord.data           None
_centerCoord.object         Particle
_centerCoord.required       No
_centerCoord.type           "Tuple of 2 real numbers labeled with tags X and Y"
_centerCoord.units          "pixels / px"
_centerCoord.definition     
;
The coordinates of the center of a particle sub-image in a micrograph image (
use minly for particle picking). Starts at {0,0}
;
_centerCoord.example       
;
    X unit = px; 251 
    Y unit = px; 97 
In this context tags X and Y refers to the center of the particle x and y 
coordinate.
;

# CS
_cs.data                    None
_cs.object                  Micrograph
_cs.required                No
_cs.type                    Real
_cs.units                   "millimeters / mm"
_cs.definition              
;
Spherical aberration of the objective lens of the microscope on which the data 
was collected. Normally this is a manufacturer-provided value
;

# Defocus U
_defocusU.data              None
_defocusU.object            "Micrograph, Particle"
_defocusU.required          No
_defocusU.type              Real
_defocusU.units             "nanometers / nm"
_defocusU.definition        
;
Value of the maximal defocus. For underfocused micrographs, this is the defocus 
along the direction defined by the minor axis of the Thon rings.  Positive 
numbers indicate underfocus images.
;

# Defocus V
_defocusV.data              None
_defocusV.object            "Micrograph, Particle"
_defocusV.required          No
_defocusV.type              Real
_defocusV.units             "nanometers / nm"
_defocusV.definition
;
Value of the minimal defocus. For underfocused micrographs, this is the defocus 
along the direction defined by the major axis of the Thon rings.  Positive 
numbers indicate underfocus images.
;

# Defocus U Angle
_defocusUAngle.data         None
_defocusUAngle.object       "Micrograph, Particle"
_defocusUAngle.required     No
_defocusUAngle.type         Real
_defocusUAngle.units        "Degrees / deg"
_defocusUAngle.definition   
;
Angle in the range (0, 180) that brings the unit vector (1,0) to coincide with 
the direction of maximum defocus. In other words, the unit vector 
(cos (defocusUAngle), sin(defocusUAngle)) is parallel to the direction of 
maximum defocus.  Note that the Thon ring major axis is perpendicular to the 
direction of maximum defocus.
;

# EMX
_emx.data                   None
_emx.object                 None
_emx.required               Yes
_emx.type                   String
_emx.units                  NA
_emx.definition             "Root element of any EMX file"

# FOM
_fom.data                   None
_fom.object                 "Micrograph, Particle"
_fom.required               No
_fom.type                   Real
_fom.units                  N/A
_fom.definition             "Figure-of-merit quality measure. Range: 0 (worst) - 1 (best)"

# Micrograph parameters
_micrograph.data            None
_micrograph.object          "Micrograph, Particle"
_micrograph.required        "Yes, when exchanging micrographs"
_micrograph.type            "Compound element"
_micrograph.units           NA
_micrograph.definition
;
A container for micrograph parameters. It uses the attributes fileName (string) 
and index (integer). At least one of these attributes must be specified.
;
_micrograph.example         "micrograph fileName=mic001.mrc index=1"

# Particle
_particle.data              None
_particle.object            Particle
_particle.required          "Yes, when exchanging particles"
_particle.type              "Compound element"
_particle.units             NA
_particle.definition        
;
A container for micrograph parameters. It uses the attributes fileName (string) 
and index (integer). At least one of these attributes must be specified.
;
_particle.example           "particle fileName=part001.mrc index=1"


# Pixel Spacing
_pixelSpacing.data          None
_pixelSpacing.object        "Micrograph, Particle"
_pixelSpacing.required      No
_pixelSpacing.type          Real
_pixelSpacing.units         "Ångstrom/pixel, Å/px"
_pixelSpacing.definition    "The sampling rate"
_pixelSpacing.example       
;
    X unit = Å/px; 1.2 
    Y unit = Å/px; 1.4 
In this context tags X and Y refers to the sampling along x and y axis. By 
default sampling along y is equal to sampling along x axis and may be omitted.
;

# Transformation Matrix
_transformationMatrix.data      None
_transformationMatrix.object    Particle
_transformationMatrix.required  No
_transformationMatrix.type      "Tuple of exactly 12 real numbers labeled with tags tij"
_transformationMatrix.units     "Only the last column (that represent displacements) has units, pixels, px"
_transformationMatrix.example   
;
4x3 matrix used for alignment and reconstruction.
    t11 0.1   t12 0.2
    t13 0.3   t14 unit = px; 4.0
    t21 0.01  t22 0.02
    t23 0.03  t24 unit = px; 0.04
    t31 0.11  t32 0.22
    t33 0.33  t34 unit = px; 5.0
When used for alignment the matrix describes rotations, shift, scales and 
mirrors applied to the object. The key equation to transform an image is 
fT(r)=f(T-1r) where T refers to the values in transformationMatrix. When used 
for reconstruction the matrix describes the projection orientation and shift 
with respect to the 3D object.  The key equation to describe the relationship 
between the projection (g(r')) and the volume f(r) is g(r')=f(Tr)dz'. 
Details are available at URL:
http:#i2pc.cnb.csic.es/emx/LoadDictionaryFormat.htm?type=Convention
;

