#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.

import unittest
import os
import shutil
import tempfile
import json
from ccpem_core import test_data
import gemmi

class Test(unittest.TestCase):
    '''
    Unit test for star file i/o (gemmi)
    '''
    def setUp(self):
        print 'Test Relion Pipeline Star I/O with Gemmi'
        self.test_data = os.path.join(
            test_data.get_test_data_path(),
            'star')
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.test_output)

    def test_read_relion_pipeline(self):
        # Find example relion pipeline star file
        filename = os.path.join(
            self.test_data,
            'default_pipeline_3-0b_tutorial.star')
        assert os.path.exists(filename)
        # Read star file
        doc = gemmi.cif.read_file(filename)
        # Convert to json/dictionary 
        pipeline = json.loads(doc.as_json())
        assert 'pipeline_processes' in pipeline.keys()
        assert pipeline['pipeline_processes']['_rlnpipelineprocessname'][1] == 'MotionCorr/job002/'

    def test_read_relion_job_pipeline(self):
        # Test to read job_pipeline.star
        filename = os.path.join(
            self.test_data,
            'job_pipeline_job18_3-0b_tutorial.star')
        assert os.path.exists(filename)
        doc = gemmi.cif.read_file(filename)
        # Convert to json/dictionary 
        pipeline = json.loads(doc.as_json())
        assert pipeline['pipeline_input_edges']['_rlnpipelineedgefromnode'][1] == 'InitialModel/job017/run_it150_class001_symD2.mrc'

    def test_read_relion_job_data(self):
        # Test to read data file
        filename = os.path.join(
            self.test_data,
            'run_it025_data_job18_3-0b_tutorial.star')
        assert os.path.exists(filename)
        doc = gemmi.cif.read_file(filename)
        # Convert to json/dictionary 
        data = json.loads(doc.as_json())
        for k in data['#'].keys():
            print k
        assert data['#']['_rlnclassnumber'][1] == 3
        # Check data read as int (i.e. not string)
        assert isinstance(data['#']['_rlnclassnumber'][1], int)

if __name__ == '__main__':
    unittest.main()
