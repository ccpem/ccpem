#include <iostream>
#include <vector>
#include <string>
#ifndef  __MMDB_Manager__
#include "mmdb_manager.h"
#endif

class MMDBStarObj{
 public:
  mmdb::mmcif::File   file;
  mmdb::cpstr         file_name;
  mmdb::cpstr         data_name;
  mmdb::cpstr         mode;
  int                 rc;
  bool                verbose;
  MMDBStarObj(mmdb::cpstr file_name, mmdb::cpstr mode, bool verbose);

  // i/o
  void ReadMMCIFFile() {
    if (verbose) {
      printf("  Read star file : %s\n", file_name);
    }
    rc = file.ReadMMCIFFile(file_name);
    char errLog[500];
    if (rc < 0) {
      printf ("    Error reading file : %s\n",
              mmdb::mmcif::GetCIFMessage(errLog, rc) );
    } else if (rc > 0) {
      printf ("    Warning reading file : %s\n",
              mmdb::mmcif::GetCIFMessage(errLog, rc) );
    }
  }

  void WriteMMCIFFile() {
    if (verbose) {
      printf("  Writing star file : %s \n", file_name);
    }
    rc = file.WriteMMCIFFile(file_name);
    char errLog[500];
    if (rc < 0) {
      printf ("    Error writing file : %s\n",
              mmdb::mmcif::GetCIFMessage(errLog, rc) );
    } else if (rc > 0) {
      printf ("    Warning writing file : %s\n",
              mmdb::mmcif::GetCIFMessage(errLog, rc) );
    }
  }

  void WriteMMCIFFile(mmdb::cpstr new_file_name) {
    if (verbose) {
      printf("  Writing star file : %s \n", new_file_name);
    }
    rc = file.WriteMMCIFFile(new_file_name);
    char errLog[500];
    if (rc < 0) {
      printf ("    Error writing file : %s\n",
              mmdb::mmcif::GetCIFMessage(errLog, rc) );
    } else if (rc > 0) {
      printf ("    Warning writing file : %s\n",
              mmdb::mmcif::GetCIFMessage(errLog, rc) );
    }
  }

  //// Delete functions
  void DeleteMMCIFData(mmdb::cpstr data_name) {
    file.DeleteCIFData(data_name);
  }

  void DeleteCategory(mmdb::cpstr data_name, mmdb::cpstr cat_name) {
    mmdb::mmcif::Data *data;
    data = file.GetCIFData(data_name);
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    if (!data) {
       printf("\n  Error: star file does not contain defined data block : %s\n", data_name);
    } else {
      data->DeleteCategory(cat_name);
    }
  }

  void DeleteStructField(mmdb::cpstr data_name,
                         mmdb::cpstr cat_name,
                         mmdb::cpstr field_name) {
    mmdb::mmcif::Data *data;
    data = file.GetCIFData(data_name);
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    if (!data) {
      printf("\n  Error: star file does not contain defined data block : %s\n", data_name);
    } else {
      data->DeleteField(cat_name, field_name);
    }
  }

  void DeleteLoopField(mmdb::cpstr data_name,
                       mmdb::cpstr cat_name,
                       mmdb::cpstr field_name,
                       int              row) {
    mmdb::mmcif::Data *data;
    data = file.GetCIFData(data_name);
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    if (!data) {
      printf("\n  Error: star file does not contain defined data block : %s\n", data_name);
    } else {
      data->DeleteLoopField(cat_name, field_name, row);
    }
  }

  void DeleteLoopRows(mmdb::cpstr block_name,
                      int         row = 0,
                      mmdb::cpstr loop_name = "") {
    mmdb::mmcif::Data *data;
    // Create data block if not present already
    data = file.GetCIFData(block_name);
    if (!data) {
      printf("\n  Error: star file does not contain defined data block\n");
    }
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    // Get loop data item
    mmdb::mmcif::Loop *loop = data->GetLoop(loop_name);
    if (!loop) {
      printf("\n  Error: star file does not contain defined data loop name\n");
    }
    loop->DeleteRow(row);
    loop->Optimize();
  }

  void DeleteLoopRows(mmdb::cpstr      block_name,
                      std::vector<int> rows,
                      mmdb::cpstr      loop_name = ""
                      ) {
    mmdb::mmcif::Data *data;
    // Create data block if not present already
    data = file.GetCIFData(block_name);
    if (!data) {
      printf("\n  Error: star file does not contain defined data block\n");
    }
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    // Get loop data item
    mmdb::mmcif::Loop *loop = data->GetLoop(loop_name);
    if (!loop) {
      printf("\n  Error: star file does not contain defined data loop name\n");
    }
    for (unsigned int r = 0; r < rows.size(); r++) {
      loop->DeleteRow(rows[r]);
    }
    loop->Optimize();
  }

  //// Get functions
  // Get data block names
  std::vector<std::string> GetBlockNameList() {
    std::vector<std::string> return_vec;
    for (int n = 0; n < file.GetNofData(); n++) {
      mmdb::mmcif::Data *data;
      data = file.GetCIFData(n);
      return_vec.push_back(data->GetDataName());
    }
    return return_vec;
  }

  // Check if block is a loop (rather than a field list)
  bool IsBlockLoop(mmdb::cpstr block_name) {
    bool return_value = false;
    int cat_num = 0;
    mmdb::mmcif::Data *data;
    data = file.GetCIFData(block_name);
    if (!data) {
      printf("\n  Error: star file does not contain defined data block\n");
      return return_value;
    }
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    mmdb::mmcif::Category *cat = data->GetCategory(cat_num);
    if (cat->GetCategoryID() == mmdb::mmcif::MMCIF_Loop) {
      return_value = true;
    }
    return return_value;
  }

  // Get category list (inc loop or cat)
  std::vector<std::string> GetCatNameList(mmdb::cpstr data_name) {
    std::vector<std::string> return_vec;
    mmdb::mmcif::Data *data;
    data = file.GetCIFData(data_name);
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    if (!data) {
       printf("\n  Error: star file does not contain defined data block : %s\n", data_name);
       return return_vec;
    } else {
      for (int n = 0; n < data->GetNumberOfCategories(); n++) {
        mmdb::mmcif::Category *cat;
        cat = data->GetCategory(n);
        return_vec.push_back(cat->GetCategoryName());
      }
      return return_vec;
    }
  }

  // Get struct tags and fields
  std::vector<std::string> GetStructFieldNameList(mmdb::cpstr block_name,
                                                  mmdb::cpstr struc_name = "",
                                                  int   cat_num = 0) {
    std::vector<std::string> return_vec;
    mmdb::mmcif::Data *data;
    data = file.GetCIFData(block_name);
    if (!data) {
      printf("\n  Error: star file does not contain defined data block\n");
      return return_vec;
    }
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    mmdb::mmcif::Category *cat = data->GetCategory(cat_num);
    if (cat->GetCategoryID() != mmdb::mmcif::MMCIF_Struct) {
      printf("\n  Error: star file does not contain defined data loop\n");
      return return_vec;
    }
    mmdb::mmcif::Struct *struc = data->GetStructure(struc_name);
    if (!struc) {
      printf("\n  Error: star file does not contain defined data structure\n");
    } else {
      for (int n = 0; n < struc->GetNofTags(); n++) {
        return_vec.push_back(struc->GetTag(n));
      }
    }
    return return_vec;
  }

  std::vector<std::string> GetStructFieldDataList(mmdb::cpstr block_name,
                                                  mmdb::cpstr struc_name = "",
                                                  int   cat_num = 0) {
    std::vector<std::string> return_vec;
    mmdb::mmcif::Data *data;
    data = file.GetCIFData(block_name);
    if (!data) {
      printf("\n  Error: star file does not contain defined data block\n");
      return return_vec;
    }
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    mmdb::mmcif::Category *cat = data->GetCategory(cat_num);
    if (cat->GetCategoryID() != mmdb::mmcif::MMCIF_Struct) {
      printf("\n  Error: star file does not contain defined data loop\n");
      return return_vec;
    }
    mmdb::mmcif::Struct *struc = data->GetStructure(struc_name);
    if (!struc) {
      printf("\n  Error: star file does not contain defined data structure\n");
    } else {
      for (int n = 0; n < struc->GetNofTags(); n++) {
        return_vec.push_back(struc->GetField(n));
      }
    }
    return return_vec;
  }

  // Get list of names in loop
  std::vector<std::string> GetLoopNameList(mmdb::cpstr block_name,
                                           mmdb::cpstr loop_name = "",
                                           int   cat_num = 0) {
    std::vector<std::string> return_vec;
    mmdb::mmcif::Data *data;
    data = file.GetCIFData(block_name);
    if (!data) {
      printf("\n  Error: star file does not contain defined data block\n");
      return return_vec;
    }
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    mmdb::mmcif::Category *cat = data->GetCategory(cat_num);
    if (cat->GetCategoryID() != mmdb::mmcif::MMCIF_Loop) {
      printf("\n  Error: star file does not contain defined data loop\n");
      return return_vec;
    }
    mmdb::mmcif::Loop *loop = data->GetLoop(loop_name);
    if (!loop) {
      printf("\n  Error: star file does not contain defined data structure\n");
    } else {
      for (int n = 0; n < loop->GetNofTags(); n++) {
        return_vec.push_back(loop->GetTag(n));
      }
    }
    return return_vec;
  }


  // Get struct values
  double GetRealStructValue(mmdb::cpstr block_name,
                            mmdb::cpstr data_name,
                            int         cat_num = 0) {
    double return_value = 0;
    mmdb::mmcif::Data *data;
    data = file.GetCIFData(block_name);
    if (!data) {
      printf("\n  Error: star file does not contain defined data block\n");
      return return_value;
    }
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    mmdb::mmcif::Category *cat = data->GetCategory(cat_num);
    if (cat->GetCategoryID() != mmdb::mmcif::MMCIF_Struct) {
      printf("\n  Error: star file does not contain defined data loop\n");
      return return_value;
    }
    mmdb::mmcif::Struct *struc = data->GetStructure("");
    struc->GetReal(return_value, "_rlnFinalResolution");
    return return_value;
  }

  int GetIntegerStructValue(mmdb::cpstr block_name,
                            mmdb::cpstr data_name,
                            int         cat_num = 0) {
    int return_value = 0;
    mmdb::mmcif::Data *data;
    data = file.GetCIFData(block_name);
    if (!data) {
      printf("\n  Error: star file does not contain defined data block\n");
      return return_value;
    }
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    mmdb::mmcif::Category *cat = data->GetCategory(cat_num);
    if (cat->GetCategoryID() != mmdb::mmcif::MMCIF_Struct) {
      printf("\n  Error: star file does not contain defined data loop\n");
      return return_value;
    }
    mmdb::mmcif::Struct *struc = data->GetStructure("");
    struc->GetInteger(return_value, "_rlnFinalResolution");
    return return_value;
  }

  mmdb::pstr GetStringStructValue(mmdb::cpstr block_name,
                                  mmdb::cpstr data_name,
                                  int         cat_num = 0) {
    mmdb::pstr return_value = NULL;
    mmdb::mmcif::Data *data;
    data = file.GetCIFData(block_name);
    if (!data) {
      printf("\n  Error: star file does not contain defined data block\n");
      return return_value;
    }
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    mmdb::mmcif::Category *cat = data->GetCategory(cat_num);
    if (cat->GetCategoryID() != mmdb::mmcif::MMCIF_Struct) {
      printf("\n  Error: star file does not contain defined data loop\n");
      return return_value;
    }
    mmdb::mmcif::Struct *struc = data->GetStructure("");
    struc->GetString(return_value, "_rlnFinalResolution");
    return return_value;
  }

  // Get loop value
  double GetLoopRealValue(mmdb::cpstr block_name,
                          mmdb::cpstr loop_name,
                          mmdb::cpstr data_name,
                          int         row,
                          int         cat_num = 0) {
    double return_value = 0;
    mmdb::mmcif::Data *data;
    data = file.GetCIFData(block_name);
    if (!data) {
      printf("\n  Error: star file does not contain defined data block\n");
      return return_value;
    }
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    mmdb::mmcif::Category *cat = data->GetCategory(cat_num);
    if (cat->GetCategoryID() != mmdb::mmcif::MMCIF_Loop) {
      printf("\n  Error: star file does not contain defined data loop\n");
      return return_value;
    }
    // Get loop data item
    mmdb::mmcif::Loop *loop = data->GetLoop(loop_name);
    if (!loop) {
      printf("\n  Error: star file does not contain defined data loop\n");
      return return_value;
    }
    if (row > loop->GetLoopLength()-1) {
      printf("\n  Error: star file loop does not contain defined row number\n");
      return return_value;
    }
    loop->GetReal(return_value, data_name, row);
    return return_value;
  }

  int GetLoopIntegerValue(mmdb::cpstr block_name,
                          mmdb::cpstr loop_name,
                          mmdb::cpstr data_name,
                          int         row,
                          int         cat_num = 0) {
    int return_value = 0;
    mmdb::mmcif::Data *data;
    data = file.GetCIFData(block_name);
    if (!data) {
      printf("\n  Error: star file does not contain defined data block\n");
      return return_value;
    }
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    mmdb::mmcif::Category *cat = data->GetCategory(cat_num);
    if (cat->GetCategoryID() != mmdb::mmcif::MMCIF_Loop) {
      printf("\n  Error: star file does not contain defined data loop\n");
      return return_value;
    }
    // Get loop data item
    mmdb::mmcif::Loop *loop = data->GetLoop(loop_name);
    if (!loop) {
      printf("\n  Error: star file does not contain defined data loop\n");
      return return_value;
    }
    if (row > loop->GetLoopLength()-1) {
      printf("\n  Error: star file loop does not contain defined row number\n");
      return return_value;
    }
    loop->GetInteger(return_value, data_name, row);
    return return_value;
  }

  mmdb::pstr GetLoopStringValue(mmdb::cpstr block_name,
                                mmdb::cpstr loop_name,
                                mmdb::cpstr data_name,
                                int         row,
                                int         cat_num = 0) {
    mmdb::pstr return_value = NULL;
    mmdb::mmcif::Data *data;
    data = file.GetCIFData(block_name);
    if (!data) {
      printf("\n  Error: star file does not contain defined data block\n");
      return return_value;
    }
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    mmdb::mmcif::Category *cat = data->GetCategory(cat_num);
    if (cat->GetCategoryID() != mmdb::mmcif::MMCIF_Loop) {
      printf("\nError: star file does not contain defined data loop\n");
      return return_value;
    }
    // Get loop data item
    mmdb::mmcif::Loop *loop = data->GetLoop(loop_name);
    if (!loop) {
      printf("\n  Error: star file does not contain defined data loop\n");
      return return_value;
    }
    if (row > loop->GetLoopLength()-1) {
      printf("\n  Error: star file loop does not contain defined row number : %d\n", row);
      return return_value;
    }
    loop->GetString(return_value, data_name, row);
    return return_value;
  }

  bool IsLoopVectorReal(mmdb::cpstr block_name,
                        mmdb::cpstr data_name,
                        mmdb::cpstr loop_name = "",
                        int   cat_num = 0) {
    mmdb::mmcif::Data *data;
    // Create data block if not present already
    data = file.GetCIFData(block_name);
    if (!data) {
      printf("\n  Error: star file does not contain defined data block\n");
      return false;
    }
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    mmdb::mmcif::Category *cat = data->GetCategory(cat_num);
    if (cat->GetCategoryID() != mmdb::mmcif::MMCIF_Loop ) {
      printf("\n  Error: star file does not contain defined data loop category\n");
      return false;
    }
    // Get loop data item
    mmdb::mmcif::Loop *loop = data->GetLoop(loop_name);
    if (!loop) {
      printf("\n  Error: star file does not contain defined data loop name\n");
      return false;
    }
    // Determine loop data type
    double rval;
    if (loop->GetReal(rval, data_name, 0) == 0){
       return true;
    }
    return false;
  }

  bool IsLoopVectorInt(mmdb::cpstr block_name,
                       mmdb::cpstr data_name,
                       mmdb::cpstr loop_name = "",
                       int   cat_num = 0) {
    mmdb::mmcif::Data *data;
    // Create data block if not present already
    data = file.GetCIFData(block_name);
    if (!data) {
      printf("\n  Error: star file does not contain defined data block\n");
      return false;
    }
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    mmdb::mmcif::Category *cat = data->GetCategory(cat_num);
    if (cat->GetCategoryID() != mmdb::mmcif::MMCIF_Loop ) {
      printf("\n  Error: star file does not contain defined data loop category\n");
      return false;
    }
    // Get loop data item
    mmdb::mmcif::Loop *loop = data->GetLoop(loop_name);
    if (!loop) {
      printf("\n  Error: star file does not contain defined data loop name\n");
      return false;
    }
    // Determine loop data type
    int ival;
    if (loop->GetInteger(ival, data_name, 0) == 0 ){
       return true;
    }
    return false;
  }

  bool IsLoopVectorStr(mmdb::cpstr block_name,
                       mmdb::cpstr data_name,
                       mmdb::cpstr loop_name = "",
                       int   cat_num = 0) {
    mmdb::mmcif::Data *data;
    // Create data block if not present already
    data = file.GetCIFData(block_name);
    if (!data) {
      printf("\n  Error: star file does not contain defined data block\n");
      return false;
    }
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    mmdb::mmcif::Category *cat = data->GetCategory(cat_num);
    if (cat->GetCategoryID() != mmdb::mmcif::MMCIF_Loop ) {
      printf("\n  Error: star file does not contain defined data loop category\n");
      return false;
    }
    // Get loop data item
    mmdb::mmcif::Loop *loop = data->GetLoop(loop_name);
    if (!loop) {
      printf("\n  Error: star file does not contain defined data loop name\n");
      return false;
    }
    // Determine loop data type
    mmdb::pstr psval = NULL;
    if (loop->GetString(psval, data_name, 0) == 0); {
       return true;
    }
    return false;
  }

  // Get loop vectors
  std::vector<double> GetLoopRealVector(mmdb::cpstr block_name,
                                        mmdb::cpstr data_name,
                                        mmdb::cpstr loop_name = "",
                                        int   cat_num = 0) {
    std::vector<double> return_vec;
    mmdb::mmcif::Data *data;
    // Create data block if not present already
    data = file.GetCIFData(block_name);
    if (!data) {
      printf("\n  Error: star file does not contain defined data block\n");
      return return_vec;
    }
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    mmdb::mmcif::Category *cat = data->GetCategory(cat_num);
    if (cat->GetCategoryID() != mmdb::mmcif::MMCIF_Loop ) {
      printf("\n  Error: star file does not contain defined data loop category\n");
      return return_vec;
    }
    // Get loop data item
    mmdb::mmcif::Loop *loop = data->GetLoop(loop_name);
    if (!loop) {
      printf("\n  Error: star file does not contain defined data loop name\n");
      return return_vec;
    }
    mmdb::rvector rvec;
    int i1,i2,n;
    n = loop->GetLoopLength();
    i1 = 0;
    i2 = n - i1;
    mmdb::GetVectorMemory(rvec, n, 0);
    loop->GetRVector (rvec, data_name, i1, i2);
    //Return as std vector for python and relion support
    return_vec.insert(return_vec.end(), &rvec[0], &rvec[n]);
    return return_vec;
  }

  std::vector<int> GetLoopIntVector(mmdb::cpstr block_name, mmdb::cpstr data_name, int cat_num = 0){
    std::vector<int> return_vec;
    mmdb::mmcif::Data *data;
    // Create data block if not present already
    data = file.GetCIFData(block_name);
    if (!data) {
      printf("\n  Error: star file does not contain defined data block\n");
      return return_vec;
    }
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    mmdb::mmcif::Category *cat = data->GetCategory(cat_num);
    if (cat->GetCategoryID() != mmdb::mmcif::MMCIF_Loop ) {
      printf("\n  Error: star file does not contain defined data loop\n");
      return return_vec;
    }
    // Get loop data item
    mmdb::mmcif::Loop *loop = data->GetLoop("");
    if (!loop) {
      printf("\n  Error: star file does not contain defined data loop\n");
      return return_vec;
    }
    mmdb::ivector ivec;
    int i1,i2,n;
    n = loop->GetLoopLength();
    i1 = 0;
    i2 = n - i1;
    mmdb::GetVectorMemory(ivec, n, 0);
    loop->GetIVector (ivec, data_name, i1, i2);
    //Return as std vector for python and relion support
    return_vec.insert(return_vec.end(), &ivec[0], &ivec[n]);
    return return_vec;
  }

  std::vector<std::string> GetLoopStringVector(mmdb::cpstr block_name, mmdb::cpstr data_name, int cat_num = 0){
    std::vector<std::string> return_vec;
    mmdb::mmcif::Data *data;
    // Create data block if not present already
    data = file.GetCIFData(block_name);
    if (!data) {
      printf("\n  Error: star file does not contain defined data block\n");
      return return_vec;
    }
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestCategories); // Required for non-mmcif files (e.g. STAR)
    data->SetFlag(mmdb::mmcif::CIFFL_SuggestTags);       // Required for non-mmcif files (e.g. STAR)
    mmdb::mmcif::Category *cat = data->GetCategory(cat_num);
    if (cat->GetCategoryID() != mmdb::mmcif::MMCIF_Loop ) {
      printf("\n  Error: star file does not define data as loop : %s", block_name);
      return return_vec;
    }
    // Get loop data item
    mmdb::mmcif::Loop *loop = data->GetLoop("");
    if (!loop) {
      printf("\n  Error: star file does not contain defined data loop : %s\n", data_name);
      return return_vec;
    }
    mmdb::psvector psvec;
    int i1,i2,n;
    n = loop->GetLoopLength();
    i1 = 0;
    i2 = n - i1;
    mmdb::GetVectorMemory(psvec, n, 0);
    loop->GetSVector (psvec, data_name, i1, i2);
    //Return as std vector for python and relion support
    return_vec.insert(return_vec.end(), &psvec[0], &psvec[n]);
    return return_vec;
  }

  //// Put functions
  // Add data block
  void AddCIFData(mmdb::cpstr data_name) {
    file.AddCIFData(data_name);
  }

  // Put data struct
  void PutDataField(mmdb::cpstr data_name,
                    mmdb::cpstr cat_name,
                    mmdb::cpstr tag_name,
                    mmdb::cpstr field_data) {
    mmdb::mmcif::Data *data;
    // Create data block if not present already
    data = file.GetCIFData(data_name);
    if (!data) {
      this->AddCIFData(data_name);
      data = file.GetCIFData(data_name);
    }
    data->PutString(field_data, cat_name, tag_name);
  }

  void PutDataField(mmdb::cpstr data_name,
                    mmdb::cpstr cat_name,
                    mmdb::cpstr tag_name,
                    int   field_data) {
    mmdb::mmcif::Data *data;
    // Create data block if not present already
    data = file.GetCIFData(data_name);
    if (!data) {
      this->AddCIFData(data_name);
      data = file.GetCIFData(data_name);
    }
    data->PutInteger(field_data, cat_name, tag_name);
  }

  void PutDataField(mmdb::cpstr  data_name,
                    mmdb::cpstr  cat_name,
                    mmdb::cpstr  tag_name,
                    double       field_data) {
    mmdb::mmcif::Data *data;
    // Create data block if not present already
    data = file.GetCIFData(data_name);
    if (!data) {
      this->AddCIFData(data_name);
      data = file.GetCIFData(data_name);
    }
    data->PutReal(field_data, cat_name, tag_name);
  }

  // Put loop value
  void PutLoopData(mmdb::cpstr data_name,
                   mmdb::cpstr loop_name,
                   mmdb::cpstr cat_name,
                   mmdb::cpstr tag_name,
                   int         row,
                   mmdb::cpstr loop_data) {
    mmdb::mmcif::Data *data;
    // Create data block if not present already
    data = file.GetCIFData(data_name);
    if (!data) {
      this->AddCIFData(data_name);
      data = file.GetCIFData(data_name);
    }
    data->PutLoopString(loop_data, cat_name, tag_name, row);
  }

  void PutLoopData(mmdb::cpstr data_name,
                   mmdb::cpstr loop_name,
                   mmdb::cpstr cat_name,
                   mmdb::cpstr tag_name,
                   int         row,
                   int         loop_data) {
    mmdb::mmcif::Data *data;
    // Create data block if not present already
    data = file.GetCIFData(data_name);
    if (!data) {
      this->AddCIFData(data_name);
      data = file.GetCIFData(data_name);
    }
    data->PutLoopInteger(loop_data, cat_name, tag_name, row);
  }

  void PutLoopData(mmdb::cpstr  data_name,
                   mmdb::cpstr  loop_name,
                   mmdb::cpstr  cat_name,
                   mmdb::cpstr  tag_name,
                   int          row,
                   double       loop_data) {
    mmdb::mmcif::Data *data;
    // Create data block if not present already
    data = file.GetCIFData(data_name);
    if (!data) {
      this->AddCIFData(data_name);
      data = file.GetCIFData(data_name);
    }
    data->PutLoopReal(loop_data, cat_name, tag_name, row);
  }


  void PutLoopData(mmdb::cpstr         data_name,
                   mmdb::cpstr         loop_name,
                   mmdb::cpstr         cat_name,
                   mmdb::cpstr         tag_name,
                   std::vector<double> put_loop_data,
                   bool                overwrite = false) {
    mmdb::mmcif::Data *data;
    // Create data block if not present already
    data = file.GetCIFData(data_name);
    if (!data) {
      this->AddCIFData(data_name);
      data = file.GetCIFData(data_name);
    }
    mmdb::mmcif::Loop *loop;
    loop = data->GetLoop(loop_name);
    if (!loop) {
      data->AddLoop(loop_name, loop);
      loop = data->GetLoop(loop_name);
    }
    int row_offset;
    if (overwrite) {
      row_offset = 0;
    } else {
      row_offset = loop->GetLoopLength();
    }
    for (unsigned int n = 0; n < put_loop_data.size(); n++) {
      loop->PutReal(put_loop_data[n], tag_name, n + row_offset);
    }
  }
};
// End class MMDBStrObj

MMDBStarObj::MMDBStarObj(mmdb::cpstr file_name_, mmdb::cpstr mode_, bool verbose_ = false)
    : file_name(file_name_),
      mode(mode_),
      verbose(verbose_) {
  mmdb::cpstr read = "r";
  mmdb::cpstr write = "w";
  if (mode[0] == read[0]) {
    ReadMMCIFFile();
  } else if (mode[0] == write[0]) {
  } else printf("\n  Error: read/write mode not specified\n");
}
