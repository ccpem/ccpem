try:
    from ext import star_io_mmdb as star_io_mmdb_ext
    star_available = True
except ImportError:
    star_available = False

import os

def emx_star_dict():
    emx_tag_defs_file = os.path.split(__file__)[0]+'/emx_tag_defs.star'
    return star_io_mmdb_ext.MMDBStarObj(emx_tag_defs_file, 'r', False)
