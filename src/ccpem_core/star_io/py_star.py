#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Proto type python parser for star files.

To do: generate pandas metadata object from star file

# XXX fix multiline data_names
'''

import os
import re
from collections import OrderedDict

class MetaData(object):
    def __init__(self,
                 filename=None):
        # Set data
        self.clear_metadata()
        self.reset_status()
        self.filename = filename
        if self.filename is not None:
            if os.path.exists(self.filename):
                self.parse_star(filename=self.filename)

    def get_data_block(self, line):
        '''
        Check if line contains data block, return block name if true else return
        false.
        Block starts with 'data_<name>'
        '''
        
        if line.lstrip()[0:5] == 'data_':
            block_name = line.split()[0][5:]
            line = line.split('data_' + block_name, 1)[1].lstrip()
            return block_name, line
        else:
            return None, line

    def get_data_name(self, line):
        '''
        Data keys can be in the form of a data name '_<name>' or a data loop 
        'loop_'.
        Return data name (without preceding <_>), None.
        '''
        line = line.lstrip()
        try:
            if line[0] == '_':
                data_name = line[1:].split()[0]
                line = line.split('_' + data_name, 1)[1].lstrip()
                return data_name, line
            else:
                return None, line
        except IndexError:
            return None, line

    def get_loop(self, line):
        '''
        Identify if line contains start of loop: 'loop_'
        '''
        line = line.lstrip()
        try:
            if line[0:5] == 'loop_':
                return True, line.split('loop_', 1)[1].lstrip()

            else:
                return False, line
        except IndexError:
            return False

    def get_data_item(self, line):
        '''
        Get data items from line.
        '''
        quotes = re.findall(r"['\"](.*?)['\"]", line)
        # Check for text string in quotes
        if len(quotes) > 0:
            return quotes[0]
        else:
            line = line.split()
            try:
                return line[0]
            except IndexError:
                return None

    def remove_comment(self, line):
        '''
        Remove comments from line.  Comments preceded by '<'
        '''
        if '#' not in line:
            return line
        else:
            return line.split('#', 1)[0]

    def clear_metadata(self):
        '''
        Clear metadata
        '''
        self.metadata = OrderedDict()

    def reset_status(self):
        '''
        Interpretation of star file context dependent
        '''
        self.in_block = False
        self.in_multiline_string = False
        self.in_data_pair = False
        self.in_loop_get_names = False
        self.in_loop_get_items = False
        self.in_loop_key_index = None
        self.loops = []

    def set_parser_in_block(self):
        self.reset_status()
        self.in_block = True

    def parse_star(self, filename):
        '''
            data block top level
                block name, dictionary
                data_label, data_item = key, value
                    or
                loop table = nested dictionary (key = loop_label)
                    loop key = loop number (loops don't have names)
        
        # Text string: bound by:
        #    [ ], ['], ["], [;]
        # Comments are preceded by [#]
        # Data name is a text string starting with an underline
        # Data item is a text string not starting with underline, preceded by
        # Data loop is a list of data names, preceded by 'loop' and followed
        # Data block is a collection of data, preceded by 'data-code'
        # Data file may contain any number of data blocks
        '''
        with open(filename) as starfile:
            #### Status
            self.reset_status()
            ### Current objects
            self.data_block = None
            self.multiline_string = None
            self.data_name = None
            self.data_item = None
            self.data_loop = None
            self.loops = []

            # Parse file
            n = 0
            for line in starfile:
                #### (1) Block
                n+=1
                block_name, line = self.get_data_block(line)
                if block_name is not None:
                    self.data_block = block_name
                    self.add_block(self.data_block)
                    self.set_parser_in_block()
                # If no block continue to next line
                if not self.in_block:
                    continue
 
                if not self.in_multiline_string:
                    # Remove comments
                    line = self.remove_comment(line)
 
                    #### (2) Loop
                    loop, line = self.get_loop(line)
                    if loop:
                        self.data_loop = self.add_loop(
                            data_block=self.data_block)
                        self.in_loop_get_names = True
 
                    #### (3) Data name
                    data_name, line = self.get_data_name(line)
                    if data_name is not None:
                        self.data_name = data_name
                        if self.in_loop_get_items:
                            # Leave loop
                            self.in_loop_get_items = False
                            self.in_loop_get_names = False
                            self.in_loop_key_index = None
                        if self.in_loop_get_names:
                            # Add data name to loop
                            self.add_data_name(data_block=self.data_block,
                                               data_name=self.data_name,
                                               data_loop=self.data_loop)
                            # XXX test gen
                            self.loop_gen = get_dictionary_iterator(
                                dictionary=self.metadata[
                                    self.data_block][self.data_loop])
                        else:
                            # Add data name to pair
                            self.add_data_name(data_block=self.data_block,
                                               data_name=self.data_name)
                            self.in_data_pair = True
 
                #### (4) Check multiline string
                if len(line) > 0 and self.in_block and self.in_data_pair:
                    if line[0] == ';':
                        # Save multiline string to paired data_name
                        if not self.in_multiline_string:
                            # Start muliline string
                            self.multiline_string = line[1:]
                            self.in_multiline_string = True
                        else:
                            # Save multiline string
                            self.data_item = self.multiline_string
                            self.add_pair_value(data_block=self.data_block,
                                                data_name=self.data_name,
                                                data_item=self.data_item)
                            self.in_data_pair = False
                            self.in_multiline_string = False
                    else:
                        if self.in_multiline_string:
                            self.multiline_string += line
 
                #### (5) Data items
                if self.in_block and not self.in_multiline_string:
                    # Pair data
                    if self.in_data_pair:
                        self.data_item = self.get_data_item(line)
                        if self.data_item is not None:
                            self.add_pair_value(data_block=self.data_block,
                                                data_name=self.data_name,
                                                data_item=self.data_item)
                            self.in_data_pair = False
                    # Loop data
                    if self.in_loop_get_names or self.in_loop_get_items:
                        # Faster than performing regex on every line
                        if '"' in line or "'" in line:
                            line = [p for p in re.split("( |\\\".*?\\\"|'.*?')", line) if p.strip()]
                        else:
                            line = line.split()
                        for item in line:
                            self.data_item = item
                            if self.data_item is not None:
                                if self.in_loop_get_names:
                                    self.in_loop_get_names = False
                                    self.in_loop_get_items = True
                                if self.in_loop_get_items:
                                    self.loop_gen.next().append(self.data_item)

    def add_block(self, block_name):
        # N.B. block name must be unique, will overwrite existing block if key 
        # is duplicated
        self.metadata[block_name] = OrderedDict()

    def add_pair_value(self, data_block, data_name, data_item):
        if [data_block, data_name, data_item].count(None) == 0:
            self.metadata[data_block][data_name] = data_item

    def add_loop_value(self,
                       data_block,
                       data_item,
                       data_loop,
                       data_name):
        '''
        Add loop value by value
        '''
        self.metadata[data_block][data_loop][data_name].append(data_item)

    def set_loop(self, data_block, data_loop, loop_dict):
        '''
        Set loop as ordered dictionary
        '''
        self.metadata[data_block][data_loop] = loop_dict

    def add_data_name(self, data_block, data_name, data_loop=None):
        if data_loop is None:
            if [data_block, data_name].count(None) == 0:
                self.metadata[data_block][data_name] = ''
        else:
            if [data_block, data_name, data_loop].count(None) == 0:
                self.metadata[data_block][data_loop][data_name] = []

    def add_loop(self, data_block):
        loop_name = 'loop_' + str(len(self.loops)+1)
        self.loops.append(loop_name)
        self.metadata[data_block][loop_name] = OrderedDict()
        return loop_name

    def write_star(self, filename=None, dp=3):
        starfile = open(filename, 'w')
        for block_name, block in self.metadata.items():
            block_line = '\n\ndata_' + block_name + '\n\n'
            starfile.write(block_line)
            for data_name, data in block.items():
                if data_name[0:5] == 'loop_':
                    # Loop header
                    line = 'loop_\n'
                    starfile.write(line)
                    for n, data_name in enumerate(data.keys()):
                        line = '\n_' + data_name + ' #' + str(n+1)
                        starfile.write(line)
                    cols = len(data.keys())
                    rows = len(data[data.keys()[0]])
                    
                    # Set indent
                    indent = 4
                    # Formt line
                    tab = (80 - indent) // (cols + 1)
                    for row in xrange(rows):
                        line = '\n'
                        line += ' ' * indent
                        for key, value in data.iteritems():
                            row_val = value[row]
                            # Ensure whitespace between cols
                            if type(row_val) is str:
                                col_value = ' ' + row_val.rjust(tab-1)
                                line += col_value
                            elif type(row_val) is int:
                                col_value = ' {0:>' + str(tab-1) + 'd}'
                                line += col_value.format(row_val)
                            elif type(row_val) is float:
                                col_value = ' {0:>' + str(tab-1)+ '.' + str(dp) + 'f}'
                                line += col_value.format(row_val)
                        starfile.write(line)
                    starfile.write('\n')
                else:
                    # Data pair
                    data_name = '_' + data_name
                    if type(data) is str:
                        if '\n' in data:
                            data = '\n;' + data
                            if data[-1] != '\n':
                                data += '\n'
                            data += ';\n'
                    elif type(data) is int:
                        data = str(data)
                    elif type(data) is float:
                        set_dp = '{0:.' + str(dp) + 'f}'
                        data = set_dp.format(data)
                    line = '\n' + data_name + data.rjust(80-len(data_name))
                    starfile.write(line)


def get_dictionary_iterator(dictionary):
    '''
    Return generator for dictionary values
    '''
    while True:
        for value in dictionary.itervalues():
            yield value

def main():
    if False:
        test1 = 'this is "a test"'  # or "this is 'a test'"
        test2 = "this is 'a test'"
        # pieces = [p for p in re.split("( |[\\\"'].*[\\\"'])", test) if p.strip()]
        # From comments, use this:
        pieces = [p for p in re.split("( |\\\".*?\\\"|'.*?')", test1) if p.strip()]
        print test1
        print pieces

    if False:
        d = OrderedDict()
        d['a'] = [1,2,2]
        d['b'] = [2]
        d['c'] = [3]
        d['d'] = [4]
        print d
        print d['d']
        gen = get_dictionary_iterator(dictionary=d)
        print gen.next()
        print gen.next()
        print gen.next()
        print dir(gen)
    
    if True:
        test_star = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'test_data/rln_postprocess_run1.star')
        md = MetaData(filename=test_star)
        md.write_star('test.star')

if __name__ == '__main__':
    main()
