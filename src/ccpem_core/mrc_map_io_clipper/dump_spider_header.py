
# This reads the spider image file header information, and dumps it in
# a reasonably friendly way.

# Note that Spider can do the conversion to MRC format, using .OPERATION: CP TO MRC
# Need to know pixel size. May be in PIXSIZ in Spider header, but that was 0.0 in example file.

# Martyn Winn June 2013


import sys
from struct import *
import ccpem_core.ccpem_utils


def main(filename,
         verbose = True,
         endian_format = "@"):
  #endian_format="@"  # native
  #endian_format="<"  # little endian
  #endian_format=">"  # big endian
  assert endian_format in ['@', '<', '>']
  header=file(filename).read(1344)

  # Header records contain NX 4-byte words, stored as floating point
  if (verbose):
    print 'NZ, NY = ',unpack_from(endian_format+'ff',header,0)
    print 'IREC = ',unpack_from(endian_format+'f',header,2*4)
    iform = unpack_from(endian_format+'f',header,4*4)
    print 'IFORM = ',iform

  if (iform[0]>0.0 and int(iform[0]+0.1) == 1):
    print 'File contains 2D image'
  elif (iform[0]>0.0 and int(iform[0]+0.1) == 3):
    print 'File contains 3D image'
  elif (iform[0]<0.0 and int(iform[0]-0.1) == -11):
    print 'File contains 2D Fourier, odd'
  elif (iform[0]<0.0 and int(iform[0]-0.1) == -12):
    print 'File contains 2D Fourier, even'
  elif (iform[0]<0.0 and int(iform[0]-0.1) == -21):
    print 'File contains 3D Fourier, odd'
  elif (iform[0]<0.0 and int(iform[0]-0.1) == -22):
    print 'File contains 3D Fourier, even'

  if (verbose):
    print 'IMAMI,FMAX,FMIN,AV,SIG = ',unpack_from(endian_format+'fffff',header,5*4)
    print 'NX,LABREC = ',unpack_from(endian_format+'ff',header,11*4)
    print 'IANGLE,PHI,THETA,GAMMA = ',unpack_from(endian_format+'ffff',header,13*4)
    print 'XOFF,YOFF,ZOFF = ',unpack_from(endian_format+'fff',header,17*4)
    print 'SCALE = ',unpack_from(endian_format+'f',header,20*4)
    print 'LABBYT,LENBYT = ',unpack_from(endian_format+'ff',header,21*4)
    print 'ISTACK/MAXINDX = ',unpack_from(endian_format+'f',header,23*4)
    print 'MAXIM,IMGNUM,LASTINDX = ',unpack_from(endian_format+'fff',header,25*4)
    print 'Pixel size (A) = ',unpack_from(endian_format+'f',header,37*4)

  if (verbose):
    print 'Creation date = ',unpack_from(endian_format+'11s',header,211*4)
    print 'Creation time = ',unpack_from(endian_format+'8s',header,214*4)
    print 'Title = ',unpack_from(endian_format+'160s',header,216*4)

if (__name__ == "__main__"):
  utils.print_header(message = 'Spider Map dump header')
  if len(sys.argv) < 2:
    print "\nUsage: python dump_spider_header.py my_map.vol"
    utils.print_footer()
  else:
    map_filename = sys.argv[1]
    main(filename = map_filename)
