'''
Utility for checking header of MRC files
'''
# This reads the map/mrc header information, and dumps it in
# a reasonably friendly way.

# This is an expanded version of a script posted on the BB 
# by Christoph Best of EBI

# Martyn Winn Sept 2012

import sys
from struct import unpack_from
from ccpem_core.ccpem_utils import print_footer
from ccpem_core.ccpem_utils import print_header

def verbose_print(output_line, verbose = True):
    '''
    Control level of output
    Set global level here, but can override on specific lines
    '''
    if (verbose):
        print output_line


def main(filename,
         imod_SerialEM = False,
         imod_Agard = False):
    '''
    extract header information from MRC format file
    '''
    print ' '
    print 'Summary of header information from file %s' % filename
    print ' '

    header = file(filename).read(1344)
    verbose_print( '   NX,NY,NZ = ' + str(unpack_from('iii', header, 0)) )

    mode = unpack_from('i', header, 3*4)[0]
    verbose_print( '   MODE = ' + str(mode) )
    if (mode == 0):
        print 'File contains signed 8-bit bytes (range -128 to 127)'
    elif (mode == 1):
        print 'File contains signed 2-byte integers'
    elif (mode == 2):
        print 'File contains 4-byte reals'
    elif (mode == 3):
        print 'File contains complex 16-bit integers'
    elif (mode == 4):
        print 'File contains complex 32-bit reals'
    elif (mode == 5):
        print 'File is mode 5: we thought this was never used!'
    elif (mode == 6):
        print 'File is mode 6 (non-standard): contains unsigned 2-byte integers'
    elif (mode == 7):
        print 'File is mode 7 (non-standard): contains signed 4-byte integer'

    verbose_print( '   NXSTART,NYSTART,NZSTART = ' + str(unpack_from('iii', header, 4*4)) )
    verbose_print( '   MX,MY,MZ = ' + str(unpack_from('iii', header, 7*4)) )
    verbose_print( '   A,B,C = ' + str(unpack_from('fff', header, 10*4)) )
    verbose_print( '   AL,BE,GA = ' + str(unpack_from('fff', header, 13*4)) )
    verbose_print( '   MAPC,MAPR,MAPS = ' + str(unpack_from('iii', header, 16*4)) )
    verbose_print( '   DMIN,DMAX,DMEAN = ' + str(unpack_from('fff', header, 19*4)) )

    ispg = unpack_from('i', header, 22*4)[0]
    nsymbt = unpack_from('i', header, 23*4)[0]
    verbose_print( '   ISPG = ' + str(ispg) )
    verbose_print( '   NSYMBT = ' + str(nsymbt) )
    if (nsymbt>0):
        print 'There is an extended header of %d bytes' % nsymbt
        print 'This is expected to consist of %d symmetry record(s)' % (nsymbt/80)

    if (imod_SerialEM or imod_Agard):
        print 'IMOD specific header items:'
        (nint, nreal) = unpack_from('hh', header, 32*4)
        print '   nint,nreal = ', nint, nreal
        print '   imodStamp = ', unpack_from('i', header, 38*4)
        print '   imodFlags = ', unpack_from('i', header, 39*4)
        print '   idtype,lens = ', unpack_from('hh', header, 40*4)
        print '   nd1,nd2 = ', unpack_from('hh', header, 41*4)
        print '   vd1,vd2 = ', unpack_from('hh', header, 42*4)
        print '   tiltangles = ', unpack_from('ffffff', header, 43*4)
        print 'End of IMOD section'

    else:
        verbose_print( '   (extra) = ' + str(unpack_from('25i', header, 24*4)) )

    verbose_print( '   ORIGIN = ' + str(unpack_from('fff', header, 49*4)) )
    verbose_print( '   MAP = ' + str(unpack_from('cccc', header, 52*4)) + ' [should be (\'M\',\'A\',\'P\',\' \')]' )
    verbose_print( '   MACHST = ' + '%x' % unpack_from('i', header, 53*4) + ' [should be 4144 or 4444]' )
    verbose_print( '   RMS = ' + str(unpack_from('f', header, 54*4)) )

    nlabel = unpack_from('i', header, 55*4)[0]
    verbose_print( '   NLABL = ' + str(unpack_from('i', header, 55*4)) )
    for i in range(nlabel):
        verbose_print( '   label %d = ' % i + str(unpack_from('80s', header, 56*4 + i*80)) )

    if (nsymbt>0):
        if (imod_SerialEM):
            print 'IMOD extended header:'
            for i in range(nsymbt/nint):
                offset = 256*4 + i*nint
                print unpack_from('h', header, offset)

        else:
            print 'The following symmetry records are present:'
            for i in range(nsymbt/80):
                offset = 256*4 + i*80
                print unpack_from('80s', header, offset)[0]
    else:
        print 'The following should be the first few image/map values:'
        print unpack_from('20f', header, 256*4)

if (__name__ == "__main__"):
    print_header(message = 'MRC Map dump header')
    if len(sys.argv) < 2:
        print "\nUsage: python dump_mrc_header.py my_map.mrc"
        print_footer()
    else:
        map_filename = sys.argv[1]
        main(filename = map_filename)
