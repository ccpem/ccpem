from Bio.PDB import PDBParser, Selection
from Bio import SeqIO, pairwise2
from dict_aa import dict3_1
import os,re
import types

class pdb2seq():
    '''
    Functions to parse pdbfile and extract sequences
    '''
    def __init__(self,pdbfile):
        self.pdbfile = pdbfile

    def get_pdbatomseq(self):
        '''
        get seq based on atom records
        '''
        dict_seq = {}
        dict_resdet = {}
        pdbname = os.path.split(self.pdbfile)[1].split('.')[0]
        pdbobj = PDBParser(QUIET=True)
        structure_instance = pdbobj.get_structure(pdbname,self.pdbfile)
        for model in structure_instance:
            for chain in model:
                chid = chain.get_id()
                if chid in ['',' ']: chid_fix = ' '
                else: chid_fix = chid
                residues = Selection.unfold_entities(chain, 'R')
                seq = ''
                list_resdet = []
                for res in residues:
                    hetatm_flag, resnum, icode = res.get_id()
                    resname = res.get_resname()
                    if resname not in dict3_1: continue
                    seq += dict3_1[resname]
                    list_resdet.append(str(resnum)+' '+dict3_1[resname])
                
                dict_seq[pdbname+'_'+chid_fix] = seq
                dict_resdet[pdbname+'_'+chid_fix] = list_resdet[:]
        return dict_seq, dict_resdet
    
    def get_pdbseqres(self,outfile=None,chain_id=None,
                   flagout = False,warnings=True):
        '''
        get seq based on seqres if available
        return '' otherwise
        '''
        dict_seq = {}
        pdbname = os.path.split(self.pdbfile)[1].split('.')[0]
        list_records = SeqIO.parse(self.pdbfile,"pdb-seqres")
        if list_records == None or not isinstance(list_records,types.GeneratorType):
            print 'Warning: seqres record not found (biopython get sequence)'
            return dict_seq
        for record in list_records:
            # chain ID?
            if chain_id != None:
                if record.annotations['chain'] != chain_id: continue
            else: chain_id = record.annotations['chain']
            if flagout: 
                outfile = os.path.splitext(self.pdbfile)[0]+'_'+chain_id+'.fasta' 
                ou = open(os.path.abspath(outfile),'w')
                ou.write(">"+pdbname+'_'+chain_id+"\n")
            str_seq = ''
            if re.search('X',str(record.seq)) != None:
                for s in str(record.seq):
                    if s != 'X': str_seq += s
            else: str_seq = str(record.seq)
            if warnings:
                if len(str_seq) > 800:
                    print 'Sequence too long for Jpred: ', chain_id
                    continue
                elif len(str_seq) < 30: 
                    print 'Sequence too short for Jpred: ', chain_id
                    continue
            if flagout: 
                ou.write(str_seq+"\n")
                ou.close()
                try:
                    self.submit_seq(outfile)
                except IOError:
                    print 'Sequence submission for Jpred failed: ', \
                        pdbname+'_'+chain_id
                    continue
            dict_seq[pdbname+'_'+record.annotations['chain']] = str_seq
        return dict_seq
    
    def match_atom_seqres(self, dict_atomseq, dict_seqres):
        dict_atomseq_aln = {}
        dict_seqres_aln = {}
        for pdbch in dict_atomseq:
            if not pdbch in dict_seqres:
                continue
            aligned_seq1,aligned_seq2 = \
                            alignseq(dict_atomseq[pdbch],dict_seqres[pdbch])
            #skip non similar sequences
            if float(aligned_seq1.count('-'))/len(aligned_seq1) < 0.5:
                continue
            if float(aligned_seq2.count('-'))/len(aligned_seq2) < 0.5:
                continue
            dict_atomseq_aln[pdbch] = aligned_seq1[:]
            dict_seqres_aln[pdbch] = aligned_seq2[:]
            
        return dict_atomseq_aln, dict_seqres_aln
    
def alignseq(seq1,seq2):
    if isinstance(seq1,list):
        seq1_str = ''.join(seq1)
    elif isinstance(seq1,basestring):
        seq1_str = seq1
    else: 
        print 'Could not align sequences:'
        print seq1
        print seq2
        return None,None
    if isinstance(seq2,list):
        seq2_str = ''.join(seq2)
    elif isinstance(seq2,basestring):
        seq2_str = seq2
    else: 
        print 'Could not align sequences:'
        print seq1
        print seq2
        return None,None
    aligned = pairwise2.align.globalms(seq1_str,seq2_str,1,-1,-0.5,-0.1)[0]
    aligned_seq1,aligned_seq2 = list(aligned[0]),list(aligned[1])
    return aligned_seq1,aligned_seq2

def match_lists_add(list1,list2):
    if isinstance(list1,str):
        list1 = [e for e in list1]
    if isinstance(list2,str):
        list2 = [e for e in list2]
    #add gaps to list1 based on list2
    indices = [i for i, x in enumerate(list2) if x == "-"]
    ct = 0
    for i in indices: 
        list1.insert(i,'-')
        ct += 1
    return list1

def match_lists_del(list1,list2):
    if isinstance(list1,str):
        list1 = [e for e in list1]
    if isinstance(list2,str):
        list2 = [e for e in list2]
    #remove indices in list1 corresponding to gaps in list2
    indices = [i for i, x in enumerate(list2) if x == "-"]
    ct = 0
    for i in indices: 
        list1.pop(i-ct)
        ct += 1
    return list1

class pdbatom():
    def __init__(self, pdbfile):
        self.pdbfile = pdbfile
        
    def get_ca_coord():
        dict_coord = OrderedDict()
        pdb_basename = os.path.basename(self.pdbfile).split('.')[0]
        p=PDBParser(QUIET=True)
        structure=p.get_structure(pdb_basename, self.pdbfile)
        for mod in structure:
            mod_id = mod.get_id()
            if not mod_id in dict_coord: dict_coord[mod_id] = {}
            for ch in mod:
                ct += 1
                chid = ch.get_id()
                if not chid in dict_coord[mod_id]: dict_coord[mod_id][chid] = {}
                residues = Selection.unfold_entities(ch, 'R')
                for res in residues:
                    hetatm_flag, resnumi, icode = res.get_id()
                    res_atm_list = Selection.unfold_entities(res, 'A')
                    for at in res_atm_list:
                        atid = at.get_id()
                        if atid != 'CA': continue #TODO: ignore non-CA?
                        bfac = at.get_bfactor()
                        elem = at.element
                        coord = at.get_coord()
                        dict_coord[mod_id][chid][res] = [coord[0],coord[1],coord[2]] 
        return dict_coord