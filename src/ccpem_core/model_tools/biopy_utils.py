
import numpy as np
from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.MMCIFParser import MMCIFParser
from Bio.PDB.PDBIO import PDBIO
from Bio.PDB.mmcifio import MMCIFIO


def shift_coordinates(in_model_path, out_model_path, trans_matrix):
    if in_model_path.split('.')[-1] == 'pdb' or in_model_path.split('.')[-1] == 'ent':
        file_type = 'pdb'
        parser = PDBParser(PERMISSIVE=1)
        structure = parser.get_structure('in_model', in_model_path)
    elif in_model_path.split('.')[-1] == 'cif':
        file_type = 'cif'
        parser = MMCIFParser()
        structure = parser.get_structure('in_model', in_model_path)
    else:
        raise Exception(
            'Please provide a the input model in PDB or CIF format')
    for atom in structure.get_atoms():
        atom.get_coord()[0] = atom.get_coord()[0] - trans_matrix[0]
        atom.get_coord()[1] = atom.get_coord()[1] - trans_matrix[1]
        atom.get_coord()[2] = atom.get_coord()[2] - trans_matrix[2]

    if file_type == 'pdb':
        io = PDBIO()
    else:
        io = MMCIFIO()
    io.set_structure(structure)
    io.save(out_model_path)
