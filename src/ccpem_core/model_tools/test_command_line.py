#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.

import unittest
import os
import shutil
import tempfile

import gemmi

from ccpem_core import test_data
import command_line


class Test(unittest.TestCase):
    '''
    Unit test for model tools command line interface
    '''
    def setUp(self):
        self.test_data = test_data.get_test_data_path()
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.test_output)

    def test_convert_pdb_to_mmcif(self):
        pdb_path = os.path.join(self.test_data, 'pdb/5me2.pdb')
        output_basename = os.path.join(self.test_output, '5me2')
        output_cif_name = output_basename + '.cif'
        assert not os.path.exists(output_cif_name)

        args = ['--model', pdb_path, '--output_cif', '--output_name', output_basename]
        command_line.main(args)

        assert os.path.exists(output_cif_name)
        cif_doc = gemmi.cif.read(output_cif_name)
        assert isinstance(cif_doc, gemmi.cif.Document)

    def test_convert_mmcif_to_pdb(self):
        cif_path = os.path.join(self.test_data, 'pdb/5ni1.cif')
        output_basename = os.path.join(self.test_output, '5ni1')
        output_pdb_name = output_basename + '.pdb'
        assert not os.path.exists(output_pdb_name)

        args = ['--model', cif_path, '--output_pdb', '--output_name', output_basename]
        command_line.main(args)

        assert os.path.exists(output_pdb_name)
        structure = gemmi.read_pdb(output_pdb_name)
        assert isinstance(structure, gemmi.Structure)

    def test_set_cell_from_map(self):
        pdb_path = os.path.join(self.test_data, 'pdb/5me2.pdb')
        orig_pdb_cell = gemmi.read_structure(pdb_path).cell
        assert orig_pdb_cell.a == orig_pdb_cell.b == orig_pdb_cell.c == 1.0
        assert orig_pdb_cell.alpha == orig_pdb_cell.beta == orig_pdb_cell.gamma == 90.0

        output_basename = os.path.join(self.test_output, '5ni1')
        output_pdb_name = output_basename + '.pdb'
        output_cif_name = output_basename + '.cif'
        assert not os.path.exists(output_pdb_name)
        assert not os.path.exists(output_cif_name)

        map_path = os.path.join(self.test_data, 'map/mrc/5me2.map')
        map_cell = gemmi.read_ccp4_map(map_path).grid.unit_cell
        assert map_cell.a != 1.0

        args = ['--model', pdb_path,
                '--set_cell_from_map', map_path,
                '--output_pdb',
                '--output_cif',
                '--output_name', output_basename]
        command_line.main(args)

        # Create helper function to check output against map
        def check_cell(structure):
            assert isinstance(structure, gemmi.Structure)
            out_cell = structure.cell
            self.assertAlmostEqual(out_cell.a, map_cell.a, 3)
            self.assertAlmostEqual(out_cell.b, map_cell.b, 3)
            self.assertAlmostEqual(out_cell.c, map_cell.c, 3)
            assert out_cell.alpha == out_cell.beta == out_cell.gamma == 90.0

        assert os.path.exists(output_pdb_name)
        pdb_structure = gemmi.read_pdb(output_pdb_name)
        check_cell(pdb_structure)

        assert os.path.exists(output_cif_name)
        cif_doc = gemmi.cif.read(output_cif_name)
        cif_structure = gemmi.make_structure_from_block(cif_doc[0])
        check_cell(cif_structure)


if __name__ == '__main__':
    unittest.main()

