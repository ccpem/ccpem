#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.

from __future__ import print_function

import argparse
import sys

import gemmi

from ccpem_core import ccpem_utils
import gemmi_utils


def parse_args(arg_list):
    parser = argparse.ArgumentParser(description='CCP-EM model tools')
    parser.add_argument(
        '-model', '--model',
        required=True,
        help='Input atomic model file (PDB or mmCIF/PDBx')
    parser.add_argument(
        '-set_cell_from_map', '--set_cell_from_map',
        help="Set the output model's unit cell to match this map (MRC format)")
    parser.add_argument(
        '-cella', '--cella', dest='cella',
        required=False, default=None, 
        help='Input cell dimensions')
    parser.add_argument(
        '-shift_coordinates', '--shift_coordinates', dest='shift_coordinates',
        default=False,action='store_true', 
        help='Translate atomic coordinates?')
    parser.add_argument(
        '-trans_vec', '--trans_vec', dest='trans_vec',
        required=False, default='0.0,0.0,0.0', 
        help='Vector to translate atomic coordinates')
    parser.add_argument(
        '-output_cif', '--output_cif',
        action='store_true',
        help='Write an output file in mmCIF/PDBx format')
    parser.add_argument(
        '-output_pdb', '--output_pdb', 
        action='store_true',
        help='Write an output file in PDB format')
    parser.add_argument(
        '-output_name', '--output_name', dest='output_name',
        required=False, default='model', 
        help='Output file name (without extension). ".pdb" or ".cif" will be added '
             'according to the chosen output format.')
    
    return parser.parse_args(arg_list)


def main(arg_list):
    args = parse_args(arg_list)
    ccpem_utils.print_header('CCP-EM Model Tools')

    # Read input
    print('Reading input model file:', args.model)
    structure = gemmi.read_structure(args.model)

    # Process structure
    if args.set_cell_from_map:
        print('Reading input map file:', args.model)
        map = gemmi.read_ccp4_map(args.set_cell_from_map)
        cell = map.grid.unit_cell
        print('Setting model cell:',
              cell.a, cell.b, cell.c,
              cell.alpha, cell.beta, cell.gamma)
        structure.cell = cell
        structure.spacegroup_hm = "P 1"
    elif args.cella is not None:
        cella = args.cella[1:-1].split(',')
        cella = [float(c) for c in cella]
        cell = gemmi.UnitCell(cella[0],cella[1],cella[2],90.,90.,90.)
        structure.cell = cell
        structure.spacegroup_hm = "P 1"

    if args.shift_coordinates:
        gemmi_utils.shift_coordinates(structure,'shifted_model.pdb',
                                      [float(t) for t in args.trans_vec.split(',')],
                                      input_obj=True)
    # Write output
    if args.output_cif:
        out_cif_name = args.output_name + '.cif'
        print('Writing output mmCIF file:', out_cif_name)
        gemmi_utils.write_structure_as_mmcif(structure, out_cif_name)
    if args.output_pdb:
        out_pdb_name = args.output_name + '.pdb'
        print('Writing output PDB file:', out_pdb_name)
        gemmi_utils.write_structure_as_pdb(structure, out_pdb_name)

    # End
    ccpem_utils.print_footer()


if __name__ == '__main__':
    main(sys.argv[1:])
