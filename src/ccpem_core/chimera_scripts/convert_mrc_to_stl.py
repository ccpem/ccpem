#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.

from chimera import runCommand as rc
from sys import argv
import chimera, os

"""Script to convert 3D volumes from mrc to stl format

run in headless mode as:

chimera --script="convert_mrc_to_stl.py foo.mrc"  --nogui
 
N.B. this only works in headless mode on Linux due to graphics 
dependencies"""

print 'Convert mrc map to stl format'

input_map = None
if len(argv) > 1:
    input_map = argv[1]

if input_map is not None:
    print 'Input map (mrc format): ', input_map
    if os.path.isfile(input_map):
        # Open map in mrc format
        rc("open %s"%(input_map))
        # Save map in STL format
        rc("export format stl %s"%('test.stl'))
    else:
        print 'Map not found'
