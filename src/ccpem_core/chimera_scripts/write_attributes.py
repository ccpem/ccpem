
import glob, os, re
import numpy as np



def write_attribute_rigid_body(rigid_body_file,outfp,rigidbody_file_tag=None):
    #TODO : add check and exit
    #if outfp.closed:
    # if not rigidbody_file_tag is None:
    outfp.write("attribute: ribbonColor\n")
    outfp.write("match mode: 1-to-1\n") #any match
    outfp.write("recipient: residues\n")
    
    #read from rigid body file and assign color
    assert os.path.isfile(rigid_body_file)
    rf = open(rigid_body_file,'r')
    
    #number of lines in rigid_body_file
    #is there a better way to get an approximate count?
    num_lines = 0
    for l in rf:
        if not l[0] == '#': num_lines += 1    
    #make list of colors
    col_len = np.ceil(np.power(num_lines,1./3))
    
    G = np.arange(0.0,0.95,0.9/col_len)
    B = np.arange(0.0,0.9,0.9/col_len)
    if not col_len == 1: col_len -= 1
    R = np.append(np.arange(0.9,0.0,-0.9/col_len),0.0)
    RGB = np.meshgrid(R,G,B)[0].ravel(),np.meshgrid(R,G,B)[1].ravel(),\
            np.meshgrid(R,G,B)[2].ravel()
    dict_rb = {}
    ct_rb = 0
    rf.seek(0)
    for line in rf:
        if line.startswith("#"):
            pass
        else:
            if len(line) < 3: continue
            tokens = line.split()
            if len(tokens) < 2: continue
            chainID = ''
            list_rb = []
            for i in range(int(len(tokens)/2)):
                start = int(tokens[i*2].split(':')[0])
                end = int(tokens[i*2+1].split(':')[0])
                if ':' in tokens[i*2]:
                    chainID = tokens[i*2].split(':')[1]
                    if not tokens[i*2+1].split(':')[1] == chainID:
                        print 'Check chain IDs in rigid body file', tokens[i*2]
                        continue
                for j in range(start,end+1):
                    #If just one model is opened in chimera
                    if len(chainID) != 0: atom_spec_chimera = "#0:{}.{}".format(j,chainID)
                    else: atom_spec_chimera = "#0:{}".format(j)
                    chainID = ''
                    col_assign = "\t{}\t{} {} {}". \
                                format(atom_spec_chimera,RGB[0][ct_rb],RGB[1][ct_rb],RGB[2][ct_rb])
                    outfp.write("{}\n".format(col_assign))
                list_rb.extend([str(start),str(end)])
            str_rb = ''
            str_len = 0
            for nr in range(1,len(list_rb)+1):
                str_rb += list_rb[nr-1]
                if nr >= 10 and nr % 10 == 0 and nr != len(list_rb): 
                    str_rb += '\n'
                elif nr >= 2 and nr % 2 == 0:
                    str_rb += '\t'
                else: str_rb += ' '
                
            dict_rb[(int(255*RGB[0][ct_rb]),int(255*RGB[1][ct_rb]),int(255*RGB[2][ct_rb]))] = str_rb
            if len(line) > 3: ct_rb += 1
            
    return dict_rb

def rgb_rainbow_gradient(num):
    
    sizes = [np.ceil(num/4.0),np.round(num/4.0),np.round(num/4.0),np.floor(num/4.0)]
    #red
    R1 = int(sizes[0])*[1.0]
    R2 = np.arange(1.0,0.0,-1.0/(sizes[1]))
    R3 = int(round(sizes[2]))*[0.0]
    R4 = int(np.floor(num/4.0))*[0.0]
    #green
    G1 = np.arange(0.0,1.0,1.0/(num/4.0))
    G2 = int(round(num/4.0))*[1.0]
    G3 = int(round(num/4.0))*[1.0]
    G4 = np.arange(1.0,0.0,-1.0/(num/4.0))
    #blue
    B1 = int(round(num/4.0))*[0.0]
    B2 = int(round(num/4.0))*[0.0]
    B3 = np.arange(0.0,1.0,1.0/(num/4.0))
    B4 = int(round(num/4.0))*[1.0]
    

def write_attribute_rigid_body_scores(rigid_body_file,dict_scores,outfp,rigidbody_file_tag=None):
    #TODO : add check and exit
    #if outfp.closed:
    # if not rigidbody_file_tag is None:
    outfp.write("attribute: ribbonColor\n")
    outfp.write("match mode: 1-to-1\n") #any match
    outfp.write("recipient: residues\n")
    
    #read from rigid body file and assign color
    assert os.path.isfile(rigid_body_file)
    rf = open(rigid_body_file,'r')
    
    #number of lines in rigid_body_file
    #is there a better way to get an approximate count?
    num_lines = 0
    for l in rf:
        if not l[0] == '#': num_lines += 1    
    #make list of colors
    col_len = np.ceil(np.power(num_lines,1./3))
    
    G = np.arange(0.0,0.95,0.9/col_len)
    B = np.arange(0.0,0.9,0.9/col_len)
    if not col_len == 1: col_len -= 1
    R = np.append(np.arange(0.9,0.0,-0.9/col_len),0.0)
    RGB = np.meshgrid(R,G,B)[0].ravel(),np.meshgrid(R,G,B)[1].ravel(),\
            np.meshgrid(R,G,B)[2].ravel()
    dict_rb = {}
    ct_rb = 0
    rf.seek(0)
    for line in rf:
        if line.startswith("#"):
            pass
        else:
            if len(line) < 3: continue
            tokens = line.split()
            if len(tokens) < 2: continue
            chainID = ''
            list_rb = []
            for i in range(int(len(tokens)/2)):
                start = int(tokens[i*2].split(':')[0])
                end = int(tokens[i*2+1].split(':')[0])
                if ':' in tokens[i*2]:
                    chainID = tokens[i*2].split(':')[1]
                    if not tokens[i*2+1].split(':')[1] == chainID:
                        print 'Check chain IDs in rigid body file', tokens[i*2]
                        continue
                for j in range(start,end+1):
                    #If just one model is opened in chimera
                    if len(chainID) != 0: atom_spec_chimera = "#0:{}.{}".format(j,chainID)
                    else: atom_spec_chimera = "#0:{}".format(j)
                    chainID = ''
                    col_assign = "\t{}\t{} {} {}". \
                                format(atom_spec_chimera,RGB[0][ct_rb],RGB[1][ct_rb],RGB[2][ct_rb])
                    outfp.write("{}\n".format(col_assign))
                list_rb.extend([str(start),str(end)])
            str_rb = ''
            str_len = 0
            for nr in range(1,len(list_rb)+1):
                str_rb += list_rb[nr-1]
                if nr >= 10 and nr % 10 == 0 and nr != len(list_rb): 
                    str_rb += '\n'
                elif nr >= 2 and nr % 2 == 0:
                    str_rb += '\t'
                else: str_rb += ' '
                
            dict_rb[(int(255*RGB[0][ct_rb]),int(255*RGB[1][ct_rb]),int(255*RGB[2][ct_rb]))] = str_rb
            ct_rb += 1
            
    return dict_rb
