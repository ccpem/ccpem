from chimera import runCommand as rc
import numpy as np
from sys import argv

rc("open {}".format(argv[1]))
rc("color white #0")
rc("defattr {}".format(argv[2]))