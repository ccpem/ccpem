from chimera import runCommand as rc
from sys import argv
import chimera, os

#curdir = getcwd()
if len(argv) == 1:
    attribute = 'bfactor'
else: attribute = argv[-1]

if len(argv) > 2:
    for p in range(1,len(argv)-1):
        if os.path.isfile(argv[p]): rc("open %s"%(argv[p]))
mList = chimera.openModels.list()
rc("rangecolor {},r min red mid white max blue".format(attribute))
rc("volume #{} transparency 0.6".format(p-1))