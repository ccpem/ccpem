from chimera import runCommand as rc, selection
import chimera
import sys
import os
import numpy as np

dirname1 = ''
dirname2 = ''
#map1
if os.path.isfile(sys.argv[1]): 
    rc("open %s"%(sys.argv[1]))
    path1 = os.path.abspath(sys.argv[1])
    dirname1 = os.path.split(path1)[0]
    name1 = os.path.splitext(os.path.split(path1)[-1])[0]
    print path1, name1
else: rc("open emdbID:%s"%(sys.argv[1]))
#map2
if os.path.isfile(sys.argv[2]): 
    rc("open %s"%(sys.argv[2]))
    path2 = os.path.abspath(sys.argv[2])
    dirname2 = os.path.split(path2)[0]
    name2 = os.path.splitext(os.path.split(path2)[-1])[0]
    rc("molmap #1 %f modelId 2"%(float(sys.argv[3])))  
    print path2,name2
else: rc("open emdbID:%s"%(sys.argv[2]))
'''
#model copy
if os.path.isfile(sys.argv[2]): rc("open %s"%(sys.argv[2]))
else: rc("open emdbID:%s"%(sys.argv[2]))
'''
mList = chimera.openModels.list()
m1 = mList[0]
m2 = mList[2]
m3 = mList[1]
a1 = m1.data.full_matrix()
a2 = m2.data.full_matrix()
mean1 = np.mean(a1)
std1 = np.std(a1)
mean2 = np.mean(a2)
std2 = np.std(a2)
rc("volume #0 step 1 level %f"%(mean1+2.0*std1))
#rc("volume #1 step 1 level %f"%(std2))
rc("volume #2 step 1 level %f"%(std2))
tr = np.zeros((3,4))
s2 = selection.ItemizedSelection([m2])
from FitMap.fitcmd import fitmap
fit_list = fitmap(s2, m1, search = 100, listFits = False, envelope = True, metric="correlation", inside=0.2,maxSteps=500,gridStepMin=0.05)#default: maxSteps=2000,gridStepMin=0.02

for fit in fit_list:
    for r in range(3):
        for c in range(4):
            tr[r][c] = fit.transforms[0][r][c]
    break


xf2 = chimera.Xform.xform(tr[0][0], tr[0][1], tr[0][2], tr[0][3],
tr[1][0], tr[1][1], tr[1][2], tr[1][3],
tr[2][0], tr[2][1], tr[2][2], tr[2][3], True)

matfile = name2+'_'+name1+'.mat'
outmat = open(matfile,'w')
outmat.write("Model 1.0\n")
for i in range(3):
    for j in range(4):
        outmat.write(" %f "%(tr[i][j]))
    outmat.write("\n")
outmat.close()

#m3.openState.xform = xf2
savename = name2+'_'+name1+'.pdb'
rc("matrixset %s"%(matfile))
rc("write relative 0 #1 %s"%(savename))
rc("stop")

