#
#     Copyright (C) 2018 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
from ccpem_core import settings


class Test(unittest.TestCase):
    '''
    Unit tests for settings.
    '''
    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        '''
        Remove temporary data.
        '''
        if os.path.exists(path=self.test_output):
            shutil.rmtree(self.test_output)

    def test_find_coot(self):
        program = 'coot'
        command = settings.which(program=program)
        assert command is not None

if __name__ == '__main__':
    unittest.main()
