// File : mmdb_coord_tools.i
%module mmdb_coord_tools

%include "std_vector.i"
%include "std_string.i"

%{
  // the resulting C file should be built as a python extension
  #define SWIG_FILE_WITH_INIT
  //  Includes the header in the wrapper code
  #include "mmdb_coord_tools.h"
%}

// std::vector support
namespace std {
    %template(DoubleVector) vector<double>;
    //
    %template(FloatVector) vector<float>;
    %template(FloatFloatVector) std::vector<std::vector<float> >;
    %template(FloatFloatFloatVector) std::vector<std::vector<std::vector<float> > >;
    //
    %template(IntVector) vector<int>;
    %template(StringVector) vector<std::string>;
}
// End std::vector support

// mmdb typedef
namespace mmdb {
  typedef  char*             pstr;
  typedef  const char*       cpstr;
  extern const int ANY_RES;
}

// Get Multi func for inheritence class NewBox
%include mmdb_coord_tools.h
