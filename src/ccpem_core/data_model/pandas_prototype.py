#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import time, sys
import pandas as pd
import numpy as np
from pandas import DataFrame, Series
from star_io_mmdb.ext import star_io_mmdb as star_io_mmdb_ext
import matplotlib.pyplot as plt

def test_data_frames():
    # For nDim metadata
    print 'Tst pandas'
    if True:
        # Type setting test
        df = DataFrame({'col_name' : [1,2,6,8,-1], 
                        'float_col' : [0.1, 0.2,0.2,10.1,None],
                        'str_col' : ['a','b',None,'c','a']})
        print df
        
        # Add column
        # N.B. column will be assigned to most appropriate type
        # New col will be int
        df['new_col'] = [1,2,3,4,5]
        for n in df['new_col']:
            print n
            print type(n)
        # New col will be float
        df['new_col'] = [1,2.0,3,4,5]
        for n in df['new_col']:
            print n
            print type(n)
        # New col will be python object, each item different
        df['new_col'] = [1,'2.0',3,4,5]
        for n in df['new_col']:
            print n
            print type(n)
        print df.columns
        df.rename(columns={'float_col':'RENAME_float_col'}, inplace=True)
        print df.columns
        print df.to_json()

    if True:
        # MultiIndexing
        arrays = [np.array(['name_a', 'name_b', 'name_c', 'name_d']),
                  np.array(['label', 'label', 'label2', 'label2'])]
        df = DataFrame(np.random.randn(8, 4), columns=arrays)
        print df
        print df['name_a']
        print df.columns.get_level_values(0)
        print df.columns.get_level_values(1)
        # I/O output
        print df.to_csv(sys.stdout, ',')
        # Update column
        df['name_a']=[1,2,3,4,5,6,7,8]
        # Update all values in a table
        df['name_a', 'label']=1000
        # Update single value in a table
        df['name_b', 'label'][0]=-1000
        # Add column
        df['new', 'tst']=[1,2,2,1,1,1,1,2]
        # Math
        # returns series
        mean = df['new'].mean()
        print mean
        print type(mean)
        # returns float
        mean = df['new', 'tst'].mean()
        print mean
        print type(mean)

        # Rename multiIndex cols
        
        print '\n\nRename cols'
        print df
        print df.columns
        # Rename jobname instance
        df.rename(columns={'name_a':'RENAME_float_col'}, inplace=True)
        print df.columns
        
        # Rename label
        df.rename(columns={'label':'RENAME_label'}, inplace=True)
        print df.columns
        # Initiate empty multiIndex table
        df_new = DataFrame(np.random.randn(4, 2),
                           columns=[ ['name_a', 'name_b',],
                                     ['label_a', 'label_b',] ] 
                           )
        print df_new
        df_init = DataFrame(columns=[ [], [] ] )
        df_init['new1', 'tst']=[1,2,2,3]
        print df_init
        print df_init.columns.get_level_values(0)
        print df_init.columns.get_level_values(1)
        df_init['new2', 'tst']=[9,8,7,6]
        df_init['new3', 'tst']=[0,0,1,4]
        print df_init
        print df_init.columns
        df_init.rename(columns={'new1':'RENAME'},
                       inplace=True
                       )
        print df_init

    if False:
        # Speed test, 10 x 1000K double float
        m_list_list = []
        list_len = 1000000
        for n in range(10):
            m_list_list.append(np.random.randn(list_len))
        start_time = time.clock()
        df = DataFrame({'rnd_1' : m_list_list[0],
                        'rnd_2' : m_list_list[1],
                        'rnd_3' : m_list_list[2],
                        'rnd_4' : m_list_list[3],
                        'rnd_5' : m_list_list[4],
                        'rnd_6' : m_list_list[5],
                        'rnd_7' : m_list_list[6],
                        'rnd_8' : m_list_list[7],
                        'rnd_9' : m_list_list[8],
                        'rnd_10' : m_list_list[9],
                        })
        print 'Done (secs) : ', time.clock() - start_time
        print df['rnd_1'].mean()
        print df['rnd_2'].mean()
        df.ix[:,['int_col','float_col']].apply(np.sqrt)
        df[df['rnd_1'] > 0.15]
        print 'Done (secs) : ', time.clock() - start_time


def test_series():
    if True:
        # 1D list storage (instance, label, value)
        data = [{'label':1},
                {'label':0.1},
                {'label':0.1},
                {'label':'0.1'}
                ]
        index_names = ['instance_name_a',
                       'instance_name_b',
                       'instance_name_c',
                       'instance_name_d',
                       ]
        index_name_unique = []
        for name in index_names:
            assert name not in index_name_unique
            index_name_unique.append(name)
        ser = Series(data,
                     index=index_names
                     )
        # Set series jobname
        ser.jobname = 'Test series'
        
        # Iterate through series
        for k, v in ser.iteritems():
            print k, v
        # Set value
        print ser['instance_name_a']
        ser['instance_name_a']['label'] = 100.0
        print ser['instance_name_a']
        # Set key
        ser['instance_name_a']['newlabel'] = ser['instance_name_a'].pop('label')
        print ser['instance_name_a']
        print ser

    if True:
        # 1D list storage (label, definition)
        data = ['definition',
                'definition',
                'definition',
                ]
        index_names = ['label_name_a',
                       'label_name_b',
                       'label_name_c',
                       ]
        ser = Series(data,
                     index=index_names
                     )
        ser.jobname = 'Label container'
        print ser
        print ser.index
        print ser['label_name_a']
        for n in ser.index:
            print n
            print ser[n]
    if True:
        # Change index jobname
        data = ['definition_a',
                'definition_b',
                'definition_c',
                ]
        index_names = ['label_name_a',
                       'label_name_b',
                       'label_name_c',
                       ]
        series = Series(data,
                        index=index_names
                        )
        
        print series.index
        print series.values
        
        print series['label_name_a']
        series.rename({'label_name_a':'tst'}, inplace=True)
        print series['tst']
        
#         print dir(series.index)
#         print type(series.index)
#         series.index.rename({'label_name_a':'tst'}, inplace=True)

def test_star_import():
    '''
    XXX To do :
    Add is loop or list function to star_io_mmdb
    '''
    if True:
        star_test_obj = star_io_mmdb_ext.MMDBStarObj('./test_data/rln_postprocess_run1.star', 'r', False)
        # Return code !0 value for read error
        assert star_test_obj.rc == 0
        series_list = []
        frame_list = []
        block_list = star_test_obj.GetBlockNameList()
        
        for block in block_list:
            # Get field data
            if not star_test_obj.IsBlockLoop(block):
                # Field data
                name_list = star_test_obj.GetStructFieldNameList(block)
                data_list = star_test_obj.GetStructFieldDataList(block)
                series = Series(data_list,
                                index=name_list)
                series.jobname = block
                series_list.append(series)
                continue
            else:
                loop_names = star_test_obj.GetLoopNameList(block)
                loop_dict = {}
                for name in loop_names:
                    if star_test_obj.IsLoopVectorReal(block, name):
                        data = star_test_obj.GetLoopRealVector(block, name)
                        loop_dict[name]=data
                    elif star_test_obj.IsLoopVectorInt(block, name):
                        loop_dict[name]=data
                    elif star_test_obj.IsLoopVectorStr(block, name):
                        loop_dict[name]=data
                print loop_names
                df = DataFrame(loop_dict)
                frame_list.append(df)
        
        # Get list of field names (aka tags)
        name_list = star_test_obj.GetStructFieldNameList('general')
        assert name_list[0] == '_rlnFinalResolution'
        print name_list
        # Get list of field data (aka fields)
        data_list = star_test_obj.GetStructFieldDataList('general')
        assert data_list[0] == '14.160000'

def test_hdf5_export():
    m_list_list = []
    list_len = 1000000 # 10 million
    for n in range(10):
        m_list_list.append(np.random.randn(list_len))
    df = DataFrame({'rnd_1' : m_list_list[0],
                    'rnd_2' : m_list_list[1],
                    'rnd_3' : m_list_list[2],
                    'rnd_4' : m_list_list[3],
                    'rnd_5' : m_list_list[4],
                    'rnd_6' : m_list_list[5],
                    'rnd_7' : m_list_list[6],
                    'rnd_8' : m_list_list[7],
                    'rnd_9' : m_list_list[8],
                    'rnd_10' : m_list_list[9],
                    })
    # Store dataframe as hdf5
    store = pd.HDFStore('tstdata.h5')
    store['obj1'] = df
    print store
    # Retrieve object
    print store['obj1']

def test_hdf5_import():
    start_time = time.clock()
    hdf_import = pd.HDFStore('tstdata.h5')
    print hdf_import
    df = hdf_import['obj1']
    print df['rnd_1'].mean()
    # Above takes
    #   10 *  1 million floats < 0.05 sec
    #   10 * 10 million floats < 0.5  sec
    print 'Done (secs) : ', time.clock() - start_time
    
    print dir(hdf_import)
    print hdf_import.keys()
    for key in hdf_import.keys():
        print hdf_import[key]

def test_maplotlib():
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    
    data = DataFrame({'rnd_1' : np.random.randn(50),
                      'rnd_2' : np.random.randn(50),
                      })

    data['rnd_1'].plot(ax=ax, style='b--')
    data['rnd_2'].plot(ax=ax, style='r')
    ax.set_title('random trace')
    ax.set_xlabel('n')
    ax.set_ylabel('randn')
    plt.savefig('tst.png')
    # Show plot as pop-up.
    plt.show()
    print 'End plot test'

def test_sqlite():
    import sqlite3
    # Export to sqlite
    query = """
CREATE TABLE test
(city VARCHAR(20), state VARCHAR(20),
 real REAL,        int   INTEGER
);"""
    con = sqlite3.connect(':memory:')
    con.execute(query)
    con.commit()
    data = [('Atlanta', 'Georgia', 1.25, 6),
            ('Tallahassee', 'Florida', 2.6, 3),
            ('Sacramento', 'California', 1.7, 5)]
    stmt = "INSERT INTO test VALUES(?, ?, ?, ?)"
    con.executemany(stmt, data)
    con.commit()
    # Import from sqlite
    cursor = con.execute('select * from test')
    rows = cursor.fetchall()
    df = DataFrame(rows, columns=zip(*cursor.description)[0])
    print df

def main():
#     test_data_frames()
#     test_series()
#     test_star_import()
#     test_hdf5_export()
#     test_hdf5_import()
    test_maplotlib()
#     test_sqlite()

if __name__ == '__main__':
    main()