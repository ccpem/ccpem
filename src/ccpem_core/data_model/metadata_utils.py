#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
ccpem metadata utils
'''
import sys
import json
import math
import pandas as pd
from pandas import DataFrame, Series
import numpy as np
import pyrvapi
# XXX Temp. fix for OS X build
try:
    from ccpem_core.star_io.ext import star_io_mmdb as star_io_mmdb_ext
except:
    ImportError
import metadata_default_labels

class MetaDataLabels(Series):
    def __init__(self, *args, **kwargs):
        super(MetaDataLabels, self).__init__(*args, **kwargs)

    def add_metadata_label(self, name, definition):
        '''
        Add metadata label, if already present new label will replace old.
        '''
        self[name]=definition

    def get_metadata_labels(self):
        '''
        Convenience function to return metadata labels stored in index.
        '''
        return self.index

    def print_metadata_labels(self):
        '''
        Pretty print label name and definition.
        '''
        print '{0: <20} :  {1}'.format('\nCCPEM Label', 'Definition')
        for name in self.index:
            print '{0: <20} :  {1}'.format(name, self[name])

    def delete_metadata_label(self, name):
        '''
        Remove named item from series.
        '''
        assert name in self.index
        self.pop(item=name)

class CCPEMMetaDataLabels(MetaDataLabels):
    def __init__(self, *args, **kwargs):
        data = kwargs.pop('data', metadata_default_labels.default_labels())
        super(CCPEMMetaDataLabels, self).__init__(data=data, *args, **kwargs)

class MetaDataSeries(Series):
    def __init__(self, *args, **kwargs):
        name = kwargs.pop('name', 'CCPEM Series')
        super(MetaDataSeries, self).__init__(name=name,  *args, **kwargs)

    def add_item(self, name, value, meta_data_label, ccpem_labels,
                 verbose=False):
        '''
        Add (name, label, value) item.
        N.B. name entries must be unique, adding new item with same name as
        old item overwrite old item.
        '''
        if meta_data_label in ccpem_labels.index:
            pass
        else:
            if verbose:
                print 'Warning: metadata {0} label undefined'.format(meta_data_label)
            meta_data_label = 'undefined'
        data = (meta_data_label, value)
        self[name] = data

    def delete_item(self, name):
        '''
        Remove named item from series.
        '''
        assert name in self.index
        self.pop(name)

    def validate_index_names(self, ccpem_labels, verbose=False):
        '''
        Check index name exists in ccpem_labels, give undefined prefix if
        missing.
        '''
        for label in self.index:
            if label in ccpem_labels.get_metadata_labels():
                pass
            else:
                if label[0:4]=='_rln':
                    new_label = label[4:]
                else:
                    if verbose:
                        print 'Warning: metadata {0} label undefined'.format(label)
                    new_label = 'undefined_'+label
                self.rename_label(old_label=label,
                                  new_label=new_label)

    def rename_label(self, old_label, new_label):
        '''
        Rename label.
        '''
        self.rename({old_label : new_label}, inplace=True)


class MetaDataTable(DataFrame):
    '''
    nD table of metadata.  Based on pandas DataFrame.
    Column used to store name and ccpem label:
        column = meta data label
    Index can be set if necessary.
    '''
    def __init__(self, *args, **kwargs):
        super(MetaDataTable, self).__init__(*args, **kwargs)

    def validate_column_names(self, ccpem_labels, verbose=False):
        '''
        Check column name exists in ccpem_labels, rename as undefined if
        missing.
        '''
        column_labels = self.get_column_labels()
        for label in column_labels:
            if label in ccpem_labels.get_metadata_labels():
                pass
            else:
                if label[0:4]=='_rln':
                    new_label = label[4:]
                else:
                    if verbose:
                        print 'Warning: metadata {0} label undefined'.format(
                            label)
                    new_label = 'undefined_' + label
                self.rename_column(old_label=label,
                                   new_label=new_label)

    def add_column(self, label, ccpem_labels, data):
        '''
        Convenience function to add column.  Validates column name is in known
        metadata labels.
        '''
        if ccpem_labels is not None:
            assert label in ccpem_labels.get_metadata_labels()
        self[label]=data

    def rename_column(self, old_label, new_label):
        '''
        Renames column label or name.
        '''
        assert old_label in self.get_column_labels()
        self.rename(columns={old_label:new_label},
                    inplace=True)

    def delete_column(self, label):
        '''
        Convenience function to remove column with supplied label.
        '''
        assert label in self.get_column_labels()
        del self[label]

    def get_column_labels(self):
        '''
        Convenience function to get column labels.
        '''
        return self.columns.values

    def import_json(self, json):
        '''
        Imports json object or filepath
        '''
        data_frame = pd.read_json(json)
        self.update(data_frame)

    def import_csv(self, csv):
        '''
        Imports comma seperate value string or text filepath.
        Expects first row to be col header information.
        '''
        data_frame = pd.read_csv(csv)
        self.update(data_frame)

    def export_json(self, filename=sys.stdout):
        '''
        Convenience function to export as json.
        '''
        self.to_json(filename)

    def export_csv(self, filename=sys.stdout, separator=','):
        '''
        Convenience function to export as CSV text.
        '''
        self.to_csv(filename, sep=separator)

    def convert_to_resolution_angstrom(self):
        '''
        Convert from <4SSQ/LL> or 2sin(th)/l to Angstrom.

        1/d = 2sin(th)/l
        1/d^2 = 4sin^2(th)/l^2 = <4SSQ/LL>

        (Refmac uses <4SSQ/LL> in some places and 2sin(th)/l in others.)
        '''
        # TODO: why are the resolution values stored as strings, not numbers?
        angstrom_label = (ur'Resolution (\u00c5)').encode('utf-8')
        res_conversions = {
            '<4SSQ/LL>': lambda x: str(1.0 / math.sqrt(float(x))),
            '2sin(th)/l': lambda x: str(1.0 / float(x))
        }
        for column_name in res_conversions:
            if column_name in self.columns.values:
                # to prevent division by zero as REFMAC5 sometimes outputs
                # 0.000 for very low resolution shells (large structures)
                self[column_name] = self[column_name].apply(
                    lambda x: x if float(x)>0 else 'NaN')
                self[column_name] = self[column_name].apply(
                    res_conversions[column_name])
                self.rename(columns={column_name: angstrom_label},
                                     inplace=True)

    def replace_infinity(self):
        '''
        Refmac print '+Infinity', convert to NaN.
        '''
        self.replace(
            to_replace='+Infinity',
            value='NaN',
            inplace=True)
        self.replace(
            to_replace='Infinity',
            value='NaN',
            inplace=True)

    def set_pyrvapi_table(self, table_id):
        '''
        Put dataframe info into pyrvapi table.
        '''
        self.replace_infinity()
        # Set labels and values
        for i, ind in enumerate(self.index):
            # Set vertical labels
            pyrvapi.rvapi_put_vert_theader(
                table_id, ind, '', i)
            for j, col in enumerate(self.columns):
                # Set horizontal labels
                if i == 0:
                    pyrvapi.rvapi_put_horz_theader(
                        table_id, col, '', j)
                # Set cell values
                try:
                    val = str(self[col][ind])
                    pyrvapi.rvapi_put_table_string(
                        table_id, val, i, j)
                except ValueError:
                    pass
        pyrvapi.rvapi_flush()

    def set_pyrvapi_graph(self, graph_id, data_id, plot_id_list=None, overlay_plot_id=None, originalXAxis=False, step=1, ymin = None):
        '''
        Put dataframe info into pyrvapi graph.
            plot_id_list = list of names for plot id.  If overlay_plot_id 
            is set fields plotted on same plot rather than individual 
            plots.
        '''
        # Add graph data
        #
        # Add df index
        pyrvapi.rvapi_add_graph_dataset('index',               # setId
                                        data_id,               # gdtId
                                        graph_id,              # gwdId
                                        'index',               # setName
                                        str(self.index.name))  # setHeader
        for i in range(self.shape[0]):
            if originalXAxis:
                if np.issubdtype(self.index.dtype, np.int_):
                    pyrvapi.rvapi_add_graph_int(
                        'index',  # setId
                        data_id,  # gdtId
                        graph_id, # gwdId
                        int(i)) # int(self.index[i]))
                else:
                    pyrvapi.rvapi_add_graph_real(
                        'index',
                        data_id,
                        graph_id,
                        float(i), # float(self.index[i]),
                        '')
            else:
                if np.issubdtype(self.index.dtype, np.int_):
                    pyrvapi.rvapi_add_graph_int(
                        'index',  # setId
                        data_id,  # gdtId
                        graph_id, # gwdId
                        int(self.index[i])) # int(self.index[i]))
                else:
                    pyrvapi.rvapi_add_graph_real(
                        'index',
                        data_id,
                        graph_id,
                        float(self.index[i]), # float(self.index[i]),
                        '')

        # Add columns
        if plot_id_list is None:
            plot_id_list = self.columns

        for n, col in enumerate(plot_id_list):
            if overlay_plot_id is not None:
                plot_id = overlay_plot_id
            elif len(plot_id_list) == 1:
                plot_id = plot_id_list[0]
            else:
                plot_id = plot_id_list[n]

            # Add dataset
            pyrvapi.rvapi_add_graph_dataset(col,
                                            data_id,
                                            graph_id,
                                            col,
                                            col)
            # Add data
            for i in range(self.shape[0]):
                if self.iloc[i][col] != 'NaN':
                    if np.issubdtype(self[col].dtype, np.int_):
                        pyrvapi.rvapi_add_graph_int(
                            col,
                            data_id,
                            graph_id,
                            int(self.iloc[i][col])) # int(self[col][i])

                    else:
                        pyrvapi.rvapi_add_graph_real(
                            col,
                            data_id,
                            graph_id,
                            float(self.iloc[i][col]), # float(self[col][i])
                            '')

            # Add plot and line
            pyrvapi.rvapi_add_graph_plot(plot_id,
                                         graph_id,
                                         col,          # graph widget id
                                         str(self.index.name),
                                         col)                # yName
            pyrvapi.rvapi_add_plot_line(plot_id,
                                        data_id, 
                                        graph_id,
                                        'index',
                                        col)                 # ysetID

            if ymin is not None:
                pyrvapi.rvapi_set_plot_ymin(plot_id, graph_id, ymin)

            if originalXAxis:
                pyrvapi.rvapi_reset_plot_xticks(plot_id, graph_id) # Reset first

                # Add blank ticks at the start (and end, below) of the axis to ensure
                # all points are plotted, otherwise they can escape off the sides of
                # the graph
                pyrvapi.rvapi_add_plot_xtick(plot_id, graph_id, -1, ' ')

                # Now add the real ticks
                for i in range(0, len(self), step):
                    if self.iloc[i][col] != 'NaN':
                        if np.issubdtype(self.index.dtype, np.int_):
                            pyrvapi.rvapi_add_plot_xtick (
                                plot_id,
                                graph_id,
                                i,
                                '%d' % self.index[i])
                        else:
                            pyrvapi.rvapi_add_plot_xtick (
                                plot_id,
                                graph_id,
                                float(i),
                                '%0.1f' % float(self.index[i]))

                # Add a final blank tick for the end of the axis
                pyrvapi.rvapi_add_plot_xtick(plot_id, graph_id, i + 1, ' ')
        #
        pyrvapi.rvapi_flush()


class MetaDataItems(object):
    '''
    Storage class for metadata items.
    '''
    def _add_meta_data_item(self, name, item):
        '''
        Add meta data item to meta data item container.
        '''
        meta_list = {name : item}
        self.__dict__.update(meta_list)

    def _remove_meta_data_item(self, name):
        '''
        Remove meta data item (table or list).
        '''
        del self.__dict__[name]

class MetaDataContainer(object):
    '''
    Metadata container for lists/series and tables/dataframes.
    '''
    def __init__(self):
        self.__dict__.update({'metadata_items' : MetaDataItems(),
                              'metadata_labels' : MetaDataLabels()})

    def __getitem__(self, key):
        return self.metadata_items.__dict__[key]

    def import_hdf5_file(self, filename):
        '''
        Import from hdf5 file.
        '''
        hdf_import = pd.HDFStore(filename)
        for key in hdf_import.keys():
            if '_MetaDataTable' in key:
                data_frame = hdf_import[key]
                md_table = MetaDataTable(data_frame)
                name = key.replace('_MetaDataTable', '').replace('/', '')
                self.metadata_items._add_meta_data_item(name=name,
                                                        item=md_table)
            elif '_MetaDataSeries' in key:
                series = hdf_import[key]
                md_series = MetaDataSeries(series)
                name = key.replace('_MetaDataSeries', '').replace('/', '')
                self.metadata_items._add_meta_data_item(name=name,
                                                        item=md_series)

    def export_hdf5_file(self, filename):
        '''
        Export to hdf5 file.
        '''
        hdf_export = pd.HDFStore(filename)
        for key in self.metadata_items.__dict__.keys():
            if key[0] != '_':
                class_str = self.metadata_items.__dict__[key].__class__.__name__
                if class_str is 'MetaDataSeries':
                    hdf_export[key+'_MetaDataSeries'] = self.metadata_items.__dict__[key]
                elif class_str is 'MetaDataTable':
                    hdf_export[key+'_MetaDataTable'] = self.metadata_items.__dict__[key]

    def import_json_file(self, filename):
        '''
        Import from json file.
        '''
        json_file=open(filename, 'r')
        data = json.load(fp=json_file)
        json_file.close()
        for key in data.keys():
            if '_MetaDataTable' in key:
                data_frame = pd.io.json.read_json(
                                        path_or_buf=data[key],
                                        typ='frame')
                md_table = MetaDataTable(data_frame)
                name = key.replace('_MetaDataTable', '').replace('/', '')
                self.metadata_items._add_meta_data_item(name=name,
                                                        item=md_table)
            elif '_MetaDataSeries' in key:
                series = pd.io.json.read_json(
                                        path_or_buf=data[key],
                                        typ='series')
                md_series = MetaDataSeries(series)
                name = key.replace('_MetaDataSeries', '').replace('/', '')
                self.metadata_items._add_meta_data_item(name=name,
                                                        item=md_series)

    def export_json_file(self, filename):
        '''
        Export to json file.
        '''
        json_dict = {}
        for key in self.metadata_items.__dict__.keys():
            if key[0] != '_':
                class_str = self.metadata_items.__dict__[key].__class__.__name__
                if class_str is 'MetaDataSeries':
                    json_dict[key+'_MetaDataSeries'] = \
                        self.metadata_items.__dict__[key].to_json()
                elif class_str is 'MetaDataTable':
                    json_dict[key+'_MetaDataTable'] = \
                        self.metadata_items.__dict__[key].to_json()
        json_file = open(filename,'w')
        json.dump(obj=json_dict, fp=json_file)
        json_file.close()

    def import_star_file(self, filename, block_names=None):
        '''
        Import star file.  Block names is list of blocks to import, if none all
        blocks imported.
        '''
        if sys.platform != 'darwin':
            mmdb_star = star_io_mmdb_ext.MMDBStarObj(filename, 'r', False)
            assert mmdb_star.rc == 0
            if block_names is None:
                block_names = mmdb_star.GetBlockNameList()
            for block in block_names:
                # Field/struct -> series
                if not mmdb_star.IsBlockLoop(block):
                    name_list = mmdb_star.GetStructFieldNameList(block)
                    data_list = mmdb_star.GetStructFieldDataList(block)
                    series = MetaDataSeries(data_list,
                                            index=name_list,
                                            name=block)
                    series.validate_index_names(ccpem_labels=self.metadata_labels)
                    self.metadata_items._add_meta_data_item(name=block, item=series)
                # Loops -> data frame table
                else:
                    loop_names = mmdb_star.GetLoopNameList(block)
                    loop_dict = {}
                    for name in loop_names:
                        if mmdb_star.IsLoopVectorReal(block, name):
                            data = mmdb_star.GetLoopRealVector(block, name)
                            loop_dict[name]=data
                        elif mmdb_star.IsLoopVectorInt(block, name):
                            loop_dict[name]=data
                        elif mmdb_star.IsLoopVectorStr(block, name):
                            loop_dict[name]=data
                    table = MetaDataTable(loop_dict)
                    table.validate_column_names(ccpem_labels=self.metadata_labels)
                    self.metadata_items._add_meta_data_item(name=block, item=table)

    def export_star_file(self, filename):
        '''
        Output in star format.
        '''
        if sys.platform != 'darwin':
            mmdb_star = star_io_mmdb_ext.MMDBStarObj(filename, 'w', False)
            for key in self.metadata_items.__dict__.keys():
                if key[0] != '_':
                    md_class = self.metadata_items.__dict__[key]
                    class_str = md_class.__class__.__name__
                    if class_str is 'MetaDataSeries':
                        for n, rec in enumerate(md_class):
                            mmdb_star.PutDataField(key,
                                                   '',
                                                   md_class.index[n],
                                                   rec)
                    elif class_str is 'MetaDataTable':
                        md_class = self.metadata_items.__dict__[key]
                        for n, col in enumerate(md_class):
                            mmdb_star.PutLoopData(key,
                                                  '',
                                                  '',
                                                  col,
                                                  md_class[col],
                                                  True)
            mmdb_star.WriteMMCIFFile()
            assert mmdb_star.rc == 0

def main():
    pass

if __name__ == '__main__':
    main()
