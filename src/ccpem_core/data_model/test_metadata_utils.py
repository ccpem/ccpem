#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Unit tests for metadata_utils
'''
import sys
import time
import os
import shutil
import json
import unittest
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sqlite3
import tempfile
from ccpem_core.data_model import metadata_utils

class Test(unittest.TestCase):
    '''
    Unit test
    '''
    def setUp(self):
        self.test_data = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'test_data')
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        if os.path.exists(self.test_output):
            shutil.rmtree(self.test_output)

    def test_metadata_labels(self):
        '''
        Test metadata list class for storing ccpem metadata label, def pairs.
        '''
        print '\n\n', sys._getframe().f_code.co_name

        ccpem_labels = metadata_utils.CCPEMMetaDataLabels()
        assert ccpem_labels['undefined'] == 'Undefined metadata label definition'

        # Add new label
        name, val = ('new_label', 'a new def')
        ccpem_labels.add_metadata_label(name='new_label',
                                        definition='a new def')
        assert ccpem_labels[name] == val
        new_val = 'a different def'
        ccpem_labels.add_metadata_label(name=name,
                                        definition=new_val)
        assert ccpem_labels[name] == new_val
        ccpem_labels.delete_metadata_label(name=name)
        assert 'new_label' not in ccpem_labels.index

    def test_metadata_series(self):
        '''
        Test metadata series for storing 1d info.
        '''
        print '\n\n', sys._getframe().f_code.co_name
        # Setup labels
        ccpem_labels = metadata_utils.CCPEMMetaDataLabels()

        # Check definetion present
        assert ccpem_labels['Resolution'] == 'Resolution (in 1/Angstroms)'

        # CCPEM Series
        ccpem_series = metadata_utils.MetaDataSeries(name='Test series')
        ccpem_series.add_item(name='new data',
                              meta_data_label='',
                              value=10.0,
                              ccpem_labels=ccpem_labels)
        assert ccpem_series['new data'][1] == 10.0
        ccpem_series.add_item(name='new data2',
                              meta_data_label='',
                              value='foo',
                              ccpem_labels=ccpem_labels)
        assert ccpem_series['new data2'][1] == 'foo'
        ccpem_series.add_item(name='new data2',
                              meta_data_label='',
                              value=99.9,
                              ccpem_labels=ccpem_labels)
        ccpem_series.add_item(name='new data3',
                              meta_data_label='',
                              value=2.1,
                              ccpem_labels=ccpem_labels)
        assert ccpem_series['new data2'][1] == 99.9
        ccpem_series.delete_item(name='new data')
        assert 'new data' not in ccpem_series.index

    def test_metadata_table(self, verbose=False):
        '''
        Test metadata table for storing 1d info.
        '''
        print '\n\n', sys._getframe().f_code.co_name
        ccpem_data_table = metadata_utils.MetaDataTable()
        ccpem_labels = metadata_utils.CCPEMMetaDataLabels()

        # Add data to table directly
        ccpem_data_table['col_labelA'] = [1,2,3,3]
        ccpem_data_table['col_labelB'] = [8,8,8,8]

        # Add data using convience function
        ccpem_data_table.add_column(label='undefined',
                                    ccpem_labels=ccpem_labels,
                                    data=[2,3,4,5])
        ccpem_data_table.validate_column_names(ccpem_labels=ccpem_labels)
        assert 'undefined' in ccpem_data_table.get_column_labels()
        assert 'undefined_col_labelB' in ccpem_data_table.get_column_labels()
        ccpem_data_table.rename_column(old_label='undefined',
                                       new_label='undefined_new')
        ccpem_data_table.delete_column(label='undefined_col_labelB')
        assert 'col_nameB' not in ccpem_data_table.get_column_labels()

        # Output data table as json string and read in.
        json_df_str = ccpem_data_table.to_json()
        assert json.loads(json_df_str)

        # Output data table as json file.
        json_path = os.path.join(self.test_output,
                                'out_table.json')
        ccpem_data_table.export_json(filename=json_path)

        # Overwrite a column values.
        ccpem_data_table['undefined_new'] = [8,8,8,8]

        # Export json convenience function.
        ccpem_data_table.import_json(json=json_path)
        
        # Check reverted to saved value
        assert ccpem_data_table['undefined_new'][0] == 2
        # Export CSV convenience function
        csv_path = os.path.join(self.test_output,
                                'out_table.csv')
        
        ccpem_data_table.export_csv(csv_path, separator=',')
        ccpem_data_table.import_csv(csv=csv_path)
        if verbose:
            # Print data frame
            print ccpem_data_table.to_string()
            print ccpem_data_table.describe()

    def test_resolution_conversion_2SL(self):
        '''
        Test conversion from 2sin(th)/l to resolution in angstroms
        '''
        # Make table with 2sin(th)/l columnn
        table = metadata_utils.MetaDataTable()
        table['2sin(th)/l'] = [0.1, 0.2, 0.4, 0.5, 1.0]

        table.convert_to_resolution_angstrom()

        res_label = ur'Resolution (\u00c5)'.encode('utf-8')
        expected = pd.DataFrame({res_label: [str(x) for x in [10.0, 5.0, 2.5, 2.0, 1.0]]})
        pd.testing.assert_frame_equal(table, expected)

    def test_resolution_conversion_4SSLL(self):
        '''
        Test conversion from <4SSQ/LL> to resolution in angstroms
        '''
        # Make table with <4SSQ/LL> columnn
        table = metadata_utils.MetaDataTable()
        table['<4SSQ/LL>'] = [0.01, 0.04, 0.16, 0.25, 1.0]

        table.convert_to_resolution_angstrom()

        res_label = ur'Resolution (\u00c5)'.encode('utf-8')
        expected = pd.DataFrame({res_label: [str(x) for x in [10.0, 5.0, 2.5, 2.0, 1.0]]})
        pd.testing.assert_frame_equal(table, expected)


    @unittest.skipIf(sys.platform == 'darwin',
                    "does not run on mac")
    def test_metadata_container_class(self):
        '''
        Test star metadata import / export and hdf5 import / export
        '''
        print '\n\n', sys._getframe().f_code.co_name

        # Star import
        metadata = metadata_utils.MetaDataContainer()
        metadata.import_star_file(
            filename=self.test_data + '/rln_postprocess_run1.star',
            block_names=None)
        # Can get item as object
        assert metadata.metadata_items.general.index[0] == 'FinalResolution'
        # Or can get item using get_item
        assert metadata['general'].index[0] == 'FinalResolution'
        assert metadata.metadata_items.fsc.shape == (31,7)

        # Star export
        star_out = os.path.join(self.test_output, 'cont_out.star')
        metadata.export_star_file(filename=star_out)

        # Json export
        json_path = os.path.join(self.test_output,
                                 'out_cont.json')

        metadata.export_json_file(filename=json_path)

        # hdf5 export
        # Requires pytables
        if False:
            metadata.export_hdf5_file(filename=self.test_output + '/out.hd5')

        # json import
        del metadata
        assert 'metadata' not in locals()
        metadata = metadata_utils.MetaDataContainer()
        metadata.import_json_file(filename=json_path)

        # hdf5 import (assert old metadata local is deleted
        # Requires pytables
        if False:
            del metadata
            assert 'metadata' not in locals()
            metadata = metadata_utils.MetaDataContainer()
            metadata.import_hdf5_file(filename=self.test_output + '/out.hd5')
            assert metadata.metadata_items.general.index[0] == 'FinalResolution'
            assert metadata['general'].index[0] == 'FinalResolution'
            assert metadata.metadata_items.fsc.shape == (31,7)

    def test_benchmark_outputs(self):
        '''
        Speed test for different i/o methods.
        '''
        print '\n\n', sys._getframe().f_code.co_name

        m_list_list = []
        list_len = 100
        for n in range(10):
            m_list_list.append(np.random.randn(list_len))
        start_time = time.clock()
        md_table = metadata_utils.MetaDataTable(
                                {'rnd_1' : m_list_list[0],
                                 'rnd_2' : m_list_list[1],
                                 'rnd_3' : m_list_list[2],
                                 'rnd_4' : m_list_list[3],
                                 'rnd_5' : m_list_list[4],
                                 'rnd_6' : m_list_list[5],
                                 'rnd_7' : m_list_list[6],
                                 'rnd_8' : m_list_list[7],
                                 'rnd_9' : m_list_list[8],
                                 'rnd_10' : m_list_list[9],
                                 })
        metadata = metadata_utils.MetaDataContainer()
        metadata.metadata_items._add_meta_data_item(name='tst_random_data',
                                                    item=md_table)
        # Export star
        # Star export
        start_time = time.clock()
        metadata.export_star_file(filename=self.test_output + '/bm.star')
        print 'Star export (secs) : ', time.clock() - start_time

        # hdf5 export
        # Requires pytables
        if False:
            start_time = time.clock()
            metadata.export_hdf5_file(filename=self.test_output + '/bm.hd5')
            print 'hdf5 export (secs) : ', time.clock() - start_time

        # json export
        start_time = time.clock()
        metadata.metadata_items.tst_random_data.to_json(self.test_output + '/bm.json')
        print 'json export (secs) : ', time.clock() - start_time

        start_time = time.clock()
        metadata.export_json_file(filename=self.test_output + '/bm2.json')
        print 'json export (secs) : ', time.clock() - start_time


    @unittest.skipIf(sys.platform == 'darwin',
                    "does not run on mac")
    def test_matplotlib(self, show=False):
        '''
        Test matplotlib usage with pandas.
        '''
        print '\n\n', sys._getframe().f_code.co_name
        metadata = metadata_utils.MetaDataContainer()
        metadata.import_star_file(
            filename=self.test_data + '/rln_postprocess_run1.star',
            block_names=None)
        guinier_data = metadata['guinier']
        guinier_data.set_index('ResolutionSquared', inplace=True)
        guinier_data.plot()
        if show:
            # show as pop-up
            plt.style.use('ggplot')
            plt.show()

    def test_sqlite(self):
        '''
        Test sqlite usage with pandas.  N.B. this is with native python sqlite 
        bindings.
        '''
        print '\n\n', sys._getframe().f_code.co_name

        query = '''
CREATE TABLE test
(city VARCHAR(20), state VARCHAR(20),
 real REAL,        int   INTEGER
);'''
        con = sqlite3.connect(':memory:')
        con.execute(query)
        con.commit()
        data = [('Atlanta', 'Georgia', 1.25, 6),
                ('Tallahassee', 'Florida', 2.6, 3),
                ('Sacramento', 'California', 1.7, 5)]
        stmt = "INSERT INTO test VALUES(?, ?, ?, ?)"
        con.executemany(stmt, data)
        con.commit()
        # Import from sqlite
        cursor = con.execute('select * from test')
        rows = cursor.fetchall()
        df = pd.DataFrame(rows, columns=zip(*cursor.description)[0])
        assert df['city'][0] == 'Atlanta'

if __name__ == '__main__':
    unittest.main()
