#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
from ccpem_core.ccpem_utils import ccpem_coord_tools
from ccpem_core.test_data import get_test_data_path

class Test(unittest.TestCase):
    '''
    Unit test for argparser
    '''

    def setUp(self):
        self.test_output = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'test_output')
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        if os.path.exists(self.test_output):
            shutil.rmtree(self.test_output)

    def test_coord_tools(self):
        '''
        Test functions
        '''
        path = os.path.join(get_test_data_path(),
                            'pdb/1fvm.pdb')
        assert os.path.exists(path=path)
        pdb = ccpem_coord_tools.CCPEMPDB(path=path)
        chains = pdb.get_chain_ids()
        assert chains == \
            ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L']



if __name__ == "__main__":
    unittest.main()
