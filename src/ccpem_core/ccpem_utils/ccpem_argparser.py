#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import argparse
import json
import os
import collections
from ccpem_core import ccpem_utils
from ccpem_core.test_data import get_test_data_path


class ccpemArguments(object):
    '''
    Container for all arguments.
    '''
    def __init__(self, arg_dict, jsonfile=None, json_string=None):
        '''
        Initialise object.  Use ordered dictionaries to maintain output order.
        Supplied dictionary added to __dict__.
        '''
        self.__dict__ = collections.OrderedDict(self.__dict__)
        self.__dict__.update(arg_dict)
        self.jsonfile = jsonfile
        self.import_error = None
        self.import_error_message = None
        if jsonfile is not None:
            self.import_args_from_json(filename=jsonfile)
        if json_string is not None:
            self.import_args_from_json(json_string=json_string)

    def __call__(self):
        self.output_args_as_text()

    def output_args_as_text(self):
        '''
        Pretty print arguments
        '''
        out_string = str()
        for key, value in self.__dict__.items():
            if not key.startswith('__'):
                if hasattr(value, 'value'):
                    set_val = str(value.value)
                    out_string += '  --' + key + '  ' + set_val + '\n'
        return out_string

    def output_args_as_json(self,
                            filename='args.json',
                            return_string=False,
                            set_abs_path=True):
        '''
        Save all parameters as json file.  Ordered dictionary used to preserve
        order in output.
        '''
        json_dict = collections.OrderedDict()
        for key, value in self.__dict__.items():
            if not key.startswith('__'):
                if hasattr(value, 'value'):
                    if set_abs_path:
                        if isinstance(value.value, str):
                            if os.path.exists(value.value):
                                value.value = os.path.abspath(value.value)
                    json_dict[key] = value.value
        if return_string:
            return json.dumps(json_dict)
        else:
            ccpem_utils.atomic_write_json(json_dict,
                                          filename,
                                          indent=4,
                                          separators=(',', ': '))
            self.jsonfile = filename

    def import_args_from_json(self, filename=None, json_string=None):
        '''
        Sets arguments from json file
        '''
        try:
            # Recursively convert JSON file from unicode to  strings
            if filename is not None:
                json_args = json_load_byteified(open(filename, 'r'))
            elif json_string is not None:
                json_args = json_loads_byteified(string_handle=json_string)
            test_data_tag = '@TESTDATA'
            test_data_path = get_test_data_path()
            for key, val in json_args.iteritems():
                if isinstance(val, str):
                    if test_data_tag in val:
                        json_args[key] = val.replace(test_data_tag,
                                                     test_data_path)
                elif isinstance(val, list):
                    for n, item in enumerate(val):
                        if isinstance(item, str):
                            if test_data_tag in item:
                                val[n] = item.replace(test_data_tag,
                                                      test_data_path)
            self.set_values(**json_args)
            self.import_error = None
            self.import_error_message = None
        except ValueError as e:
            self.import_error_message = 'Error reading arguments json file: {0}\n'.format(filename)
            self.import_error_message += '\n  Please check syntax of input json file:'
            self.import_error_message += '\n  ' + str(e)
            ccpem_utils.print_warning(
                message=self.import_error_message)
            self.import_error = str(e)

    def get_value(self, key):
        '''
        As values stored in __dict__ values can also be accessed
        from params object:
        p_obj.key.value
        '''
        val = self.__dict__[key]
        return val.value

    def set_values(self, **kwargs):
        '''
        Sets argument value.  Checks name, type and choice.
        '''
        dict_keys = self.__dict__.keys()
        for key, val in kwargs.iteritems():
            if key in dict_keys:
                # Check type match or None
                if type(val) == self.__dict__[key].type \
                        or val is None \
                        or isinstance(val, list):
                    if self.__dict__[key].choices is None\
                       or (val in self.__dict__[key].choices):
                        self.__dict__[key].value = val
                    else:
                        message = (
                            'Parameter choice invalid: '
                            '{0}: {1} \nChoices: {2}'.format(
                                key,
                                val,
                                self.__dict__[key].choices))
                        print message
                else:
                    message = (
                        '\nParameter type invalid: {0}, {1} ({2})'
                        '\nExpected type: {3}'.format(
                            key,
                            val,
                            type(val),
                            self.__dict__[key].type))
                    print message
            else:
                print '\nParameter invalid: {0}'.format(key)

    def add_args(self, args):
        '''
        Add merge new args into current.
        '''
        self.__dict__.update(args.__dict__)

    def deep_copy(self):
        '''
        Return deep copy of self.
        '''
        copy_dict = collections.OrderedDict()
        for key, value in self.__dict__.items():
            if not key.startswith('__'):
                if hasattr(value, 'value'):
                    copy_dict[key] = ccpemStoreAction(self.__dict__[key])
        return self.__class__(arg_dict=copy_dict)


class ccpemStoreAction(object):
    '''
    Wrapper to allow arg values to be accessed with a function call instead of
    `.value` (i.e. `val = arg.name()` rather than `val = arg.name.value`)
    '''
    # TODO: improve the way this is done. This class is quite cryptic!
    # Maybe better to use the `namespace=` arg to parse_args(), which allows
    # args to be accessed by name alone (i.e. `val = arg.name` - but this
    # would probably require changes to ccpemArguments to work properly.

    def __init__(self, action):
        self.__dict__.update(action.__dict__)

    def __call__(self):
        return self.value


class ccpemArgParser(argparse.ArgumentParser):
    '''
    Allows '#' or '/' to be used as comments in parameters file and allows args
    to be read from single line text file
    Example code:
    parser = ccpemArgParser(
             fromfile_prefix_chars = '@',
             description = 'Example argument parser',
             epilog = textwrap.dedent('\
    To import parameters from argument text file use :\
      python eg_argparse.py @args.txt'))
    # N.B. by default the description and epilog remove white spaces and "\n"
    #      hence use of textwrap
    '''

    def __init__(self, *args, **kwargs):
        super(ccpemArgParser, self).__init__(*args, **kwargs)
        self.argument_info = None

    def generate_arguments(self,
                           filename=None,
                           jsonfile=None,
                           json_string=None):
        '''
        Generate arguments from input file and default values
        '''
        if filename is not None:
            args, unknown = self.parse_known_args(args=['@'+filename])
        else:
            args, unknown = self.parse_known_args(args=None)
        action_dict = collections.OrderedDict()
        for action in self._actions:
            try:
                action.value = getattr(args, action.dest)
            except AttributeError:
                pass
            action_dict[action.dest] = ccpemStoreAction(action)
        ccpem_args = ccpemArguments(action_dict, jsonfile, json_string)
        return ccpem_args

    def convert_arg_line_to_args(self, line):
        '''
        Override function std function.
        '''
        for arg in line.split():
            if not arg.strip():
                continue
            if arg[0] in ['#', '/']:
                break
            yield arg

    def print_argument_current_status(self):
        '''
        Print argument current values for debugging use.
        '''
        ccpem_utils.print_sub_header(message='Arguments')
        args = self.parse_args()
        args_dict = args.__dict__
        for item in args_dict.items():
            print '{0:>30}   {1:<40}'.format('--' + item[0], str(item[1]))


def json_load_byteified(file_handle):
    return _byteify(
        json.load(file_handle, object_hook=_byteify),
        ignore_dicts=True
    )

def json_loads_byteified(string_handle):
    return _byteify(
        json.loads(string_handle, object_hook=_byteify),
        ignore_dicts=True
    )

def _byteify(data, ignore_dicts = False):
    # if this is a unicode string, return its string representation
    if isinstance(data, unicode):
        return data.encode('utf-8')
    # if this is a list of values, return list of byteified values
    if isinstance(data, list):
        return [ _byteify(item, ignore_dicts=True) for item in data ]
    # if this is a dictionary, return dictionary of byteified keys and values
    # but only if we haven't already byteified it
    if isinstance(data, dict) and not ignore_dicts:
        return {
            _byteify(key, ignore_dicts=True): _byteify(value, ignore_dicts=True)
            for key, value in data.iteritems()
        }
    # if it's anything else, return it in its original form
    return data


if __name__ == '__main__':
    pass
