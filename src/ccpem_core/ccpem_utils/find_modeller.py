#
#     Copyright (C) 2018 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import sys
import ast
import subprocess

def import_modeller():
    '''
    Use system python to try and find modeller
    '''
    # Look in standard path first
    try:
        import modeller
        return True
    except ImportError:
        pass

    # Else look for system version
    paths = ast.literal_eval(
        subprocess.check_output(["python",
                                 "-c",
                                 "import sys;print sys.path"]))
    for p in paths:
        if 'modeller' in p:
            print p
            sys.path.append(p)
    try:
        import modeller
        return True
    except ImportError:
        return False
