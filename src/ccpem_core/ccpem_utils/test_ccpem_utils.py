#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import json
import os
import shutil
import tempfile
import unittest

from ccpem_core import ccpem_utils

class Test(unittest.TestCase):
    '''
    Unit tests for ccpem_utils
    '''

    def setUp(self):
        '''
        Setup test directory
        '''
        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        '''
        Remove temporary data.
        '''
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_atomic_write_json(self):
        '''
        Test atomic_write_json() writes JSON correctly and does not leave
        stray temporary files behind.
        '''
        # Prepare test objects
        test_json = ('{"bool_field": true, "float_field": 1.23e-45, "int_array": [1, 2, 3], '
                     '"int_field": 123, "string_field": "example"}')
        test_obj = json.loads(test_json)
        test_file = os.path.join(self.test_dir, 'test.json')

        # Write the JSON file
        ccpem_utils.atomic_write_json(test_obj, test_file, sort_keys=True)

        # Check the only file in the directory is the intended test file
        file_list = os.listdir(self.test_dir)
        assert len(file_list) == 1
        assert file_list[0] == 'test.json'

        # Check the JSON is correct
        with open(test_file) as tf:
            written_json = tf.read()
            assert written_json == test_json


if __name__ == '__main__':
    unittest.main()
