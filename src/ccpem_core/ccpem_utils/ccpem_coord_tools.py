#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import os
import clipper

class CCPEMPDB(object):
    def __init__(self,
                 path):
        assert os.path.exists(path)
        # Set minimol
        f = clipper.MMDBfile()
        f.read_file(path)
        self.mmol = clipper.MiniMol()
        f.import_minimol(self.mmol)

    def get_chain_ids(self):
        '''
        Return list of chain ids
        '''
        chains = []
        model = self.mmol.model()
        for poly in model:
            chains.append(str(poly.id()))
        import time
#         time.sleep(2)
        return chains
