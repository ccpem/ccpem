#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import tempfile
import shutil
import os
from ccpem_core.ccpem_utils import centre_model_in_map
from ccpem_core import test_data

class Test(unittest.TestCase):
    '''
    Test distribution of angles on a sphere (in-house angles generation module
    for dockem and shapem)
    '''
    def setUp(self):
        '''
        Always run at start of test, e.g. for creating directory to store
        temporary test data producing during unit test.
        '''
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        '''
        Remove temporary data.
        '''
        if os.path.exists(path=self.test_output):
            shutil.rmtree(self.test_output)

    def test_centre_map_in_model(self):
        '''
        Tests if map is place in centre of model
        '''
        map_path = os.path.join(
            test_data.get_test_data_path(),
            'map/mrc/1ake_4-5A.mrc')
        assert os.path.exists(map_path)
        #
        model_path = os.path.join(
            test_data.get_test_data_path(),
            'pdb/1AKE_cha_molrep.pdb')
        assert os.path.exists(model_path)

        cmm = centre_model_in_map.AlignMapModel(
            map_path=map_path,
            model_path=model_path,
            contour=1.0,
            verbose=False)

        # Check COM of model
        self.assertAlmostEqual(cmm.model_com[0],
                               30.642,
                               2)

        # Check shifted pdb is generated
        assert os.path.exists(cmm.output_model_path)

if __name__ == '__main__':
    unittest.main()
