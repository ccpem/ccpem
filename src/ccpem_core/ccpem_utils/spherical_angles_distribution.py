#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Generates angles file as in spider (VO EA command) for running ShapeM
'''
import math
import sys
import os
from time import strftime


def angle_distn(theta_step,
                theta_limit=180,
                write_output=True,
                output_path=os.path.dirname(os.path.realpath(__file__))):
    if write_output:
        outfile = open(output_path+'/angles_file.spi', 'w')
    tm = strftime('%d-%b-%Y AT %H:%M:%S')
    if write_output:
        outfile.write('{0}{1}{2}\n'.format(' ;amr/spi   ',
                                           tm,
                                           '   angles_file.spi'))
    linenumber = 1
    assert (theta_step > 0 and theta_step < 180), \
        ('Usage of the script is python script.py theta_step and theta step ',
         'is angular step and values >0 and <180 are allowed!')
    phi_start = 0.00
    phi_final = 359.9
    theta_start = 0.00
    theta_limit = theta_limit
    # Numerator of this equation is range of theta
    theta_ite = (theta_limit-theta_start)/theta_step
    dict_look_up = {}
    for y in range(int(theta_ite+1)):
        if y*theta_step == 0 or y*theta_step == 180:
            stepphi = 1
            phi_incr = 0
        else:
            # http://spider.wadsworth.org/spider_doc/spider/docs/man/voea.html
            delphi = float(theta_step) / math.sin(
                math.radians(float(y*theta_step)))
            stepphi = max(int((phi_final-phi_start)/delphi)-1, 1)
            phi_incr = (phi_final-phi_start)/stepphi
        for i in range(stepphi):
            psi_value = 0.0
            phi_value = (i*phi_incr)
            theta_value = (y*theta_step)
            phi_bd = len(str(int(phi_value // 1)))
            theta_bd = len(str(int(theta_value // 1)))
            if write_output:
                outfile.write(
                    ('{0:5} 3   {psi:<13.4f}{theta:<13.{th}f}'
                     '{phi:<11.{ph}f}\n'.format(linenumber,
                                                psi=psi_value,
                                                theta=theta_value,
                                                phi=phi_value,
                                                th=6-(theta_bd+1),
                                                ph=6-(phi_bd+1))))
            dict_look_up[linenumber] = [psi_value, theta_value, phi_value]
            linenumber += 1
    if write_output:
        outfile.close()
    return dict_look_up


def main(theta_step):
    # Theta step is angular step and values >0 and <180 are allowed!
    angle_distn(theta_step)

if __name__ == '__main__':
    if len(sys.argv) > 1:
        main(float(sys.argv[1]))
