#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import mimetypes

'''
Central place to define allowed file extensions for file dialogs.
    File types format = 'Type name (*.foo *.bar)'
'''

# Allowed format extensions

# All ext
all_ext = 'All files (*.*)'
# MRC format
mrc_ext = 'MRC maps (*.mrc *.mrcs *.map *.ccp4)'
# Text format
text_ext = 'Text files (*.txt)'
# PDB formats
pdb_ext = 'PDB files (*.pdb *.ent *.cif *.mmcif)'
# Ligand CIF library (Refmac)
lib_ext = 'Ligand files (*.cif *.lib)'


def check_text_file(filename):
    '''
    Check if filename is text file, return True if is else False.
    '''
    if filename is None:
        return False
    mime = mimetypes.guess_type(filename)
    if 'text' in mime[0]:
        return True
    else:
        return False


def concatonate_types(files_types):
    '''
    Concatonate file types for QDialog.  Must use ;; seperator
    '''
    ext = None
    for types in files_types:
        if ext is None:
            ext = types
        else:
            ext += ';;' + types
    return ext
