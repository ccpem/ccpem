#
#     Copyright (C) 2014 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import sys, json
from urllib import urlopen
import utils

def fetchJSONeid(eid):
    s = "http://www.ebi.ac.uk/pdbe/emdb/search/?q=emd-"+eid+"&rows=-1"
    return json.loads(urlopen(s).read())

def main():
    eid = '2638'
    JSON_eid = fetchJSONeid(eid=eid)
    print JSON_eid['ResultSet']['Result'][0].keys()
    resolution = JSON_eid['ResultSet']['Result'][0]['Resolution']
    print "Resolution : ", resolution
    utils.print_footer()

if (__name__ == "__main__"):
    utils.print_header(message='EMDB JSON meta data parser')
    main()
