#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import sys
import json
import os
from ccpem_core.TEMPy.MapParser import MapParser
from ccpem_core.TEMPy.StructureParser import PDBParser
from ccpem_core import ccpem_utils


class AlignMapModel(object):
    def __init__(self,
                 map_path,
                 model_path,
                 output_model_path=None,
                 contour=1.0,
                 verbose=True):
        self.map_path = map_path
        self.model_path = model_path
        self.contour = contour

        # Get map com
        self.em_map = MapParser.readMRC(map_path)
        self.map_com = self.em_map._get_com_threshold(contour)

        # Get model com
        structure_instance = PDBParser.read_PDB_file(
            structure_id='pdb_input',
            filename=model_path)
        self.model_com = structure_instance.CoM

        # Shift PDB
        shift = self.map_com - self.model_com
        structure_instance.translate(shift[0], shift[1], shift[2])

        # Save
        if output_model_path is None:
            self.output_model_path = self.set_output_path(model_path)
        else:
            self.output_model_path=output_model_path
        structure_instance.write_to_PDB(self.output_model_path)

        if verbose:
            ccpem_utils.print_header(
                message='Centre model in map')
            ccpem_utils.print_sub_header(
                message='Shift model: {0}, {1}, {2}'.format(
                    shift[0],shift[1],shift[2]))
            ccpem_utils.print_sub_header(
                message='Output model: {0}'.format(self.output_model_path))

    @staticmethod
    def set_output_path(input_path):
        output_path = 'cen_' + os.path.basename(input_path)
        output_path = os.path.join(
           os.path.dirname(input_path),
           output_path)
        return output_path

def main():
    '''
    Align model COM to map COM.  JSON file expected to contain:
        'model_path' : path to model (pdb)
        'map_path'   : path to map (mrc)
    '''
    try:
        json_path = sys.argv(1)
        args = json.load(json_path)
        map_path = args['map_path']
        model_path = args['model_path']
        if 'contour' in args.keys():
            contour = args['contour']
        else:
            contour = 1.0
        cmm = AlignMapModel(
            map_path=args['map_path'],
            model_path=args['model_path'],
            contour=contour)
    except IOError:
        print 'Can not read JSON input'

if __name__ == '__main__':
    main()
