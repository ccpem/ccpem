#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import errno
import inspect
import os
import subprocess
import platform
import json
import re
import threading


class CCPEMVersion(object):
    def __init__(self):
        # Import version tag
        self.build_version = False
        try:
            from ccpem_core import version
            self.build_version = True
        except ImportError:
            pass
        if self.build_version:
            self.version = version.version
            self.git_revision = version.git_revision
            self.build_time = version.build_time
            self.name = self.version
        else:
            self.version = 'dev'
            self.git_revision = get_git_revision()
            self.build_time = 'N/A'
            self.name = self.version + '_' + self.git_revision


class DictToClass(object):
    def __init__(self, dct):
        self.__dict__.update(dct)


def get_home_directory():
    '''
    Return home directory dependent on platform
    '''
    user_platform = platform.system()
    # Linux
    if user_platform == 'Linux':
        path = os.getenv('XDG_DATA_HOME',
                         os.path.expanduser('~'))
    # Mac
    elif user_platform == 'Darwin':
        path = os.path.expanduser('~')
    else:
        # Fall back to current directory
        path = os.getcwd()

    # Windows
    # XXX needs testing
#     else:
#         try:
#             from win32com.shell import shell, shellcon
#             path = shell.SHGetFolderPath(0, shellcon.CSIDL_DESKTOP, None, 0)
#             if path is None:
#                 path = shell.SHGetFolderPath(0, shellcon.CSIDL_LOCAL_APPDATA, None, 0)
#             path = os.path.normpath(_get_win_folder(const))
#         except:
#             ImportError
    return path


def check_directory_and_make(directory,
                             verbose=False,
                             auto_suffix=False,
                             make=True):
    '''
    Makes directory if not already present. Auto suffix appends "_n" suffix.
    '''
    make_dir = False
    # Check directory already present or present and empty
    if not os.path.exists(directory):
        make_dir = True
    elif os.listdir(directory) == []:
        make_dir = True
    #
    if make_dir:
        if make:
            try:
                os.makedirs(directory)
                if verbose:
                    print '  Create directory : ', directory
                return directory
            except OSError:
                return None
        else:
            return directory
    elif auto_suffix:
        suffixSearch = re.compile(r'(_\d+)$').search(directory)
        if suffixSearch is None:
            new_directory = directory + '_1'
        else:
            suffix = suffixSearch.group(1)
            i = int(suffix.replace('_', '')) + 1
            new_directory = directory.replace(suffix, '_'+str(i))
        new_directory = check_directory_and_make(directory=new_directory,
                                                 verbose=verbose,
                                                 auto_suffix=auto_suffix,
                                                 make=make)
        return new_directory
    else:
        return None


def check_file_exists(filename):
    '''
    Returns bool value for presence of specified file
    '''
    return os.path.isfile(filename)


def atomic_write_json(object, filename, **json_dump_kwargs):
    '''
    Save an object to JSON as an atomic operation.

    This ensures any processes reading the JSON file will always see a valid
    version (old or new) and not a half-written new file.

    This function writes the JSON to a temporary file first, then renames it
    after writing is complete and flushed to disk. The temporary file will
    always be removed even if the write-and-rename is unsuccessful.

    Pass additional keyword arguments for the json.dump() command as keyword
    arguments to this function.

    Note: using this function avoids race conditions caused by processes
    reading the file when it is partly-written by another process. It does not
    avoid possible problems caused by two processes writing to the same file
    at the same time. In that case, one version will be kept and the other
    will not, but there is no guarantee about which one will be kept. Avoiding
    that problem would require some kind of file locking, versioning or time
    stamping to identify conflicting or simultaneous edits.
    '''
    # Get the directory for the target file to write
    dir = os.path.dirname(os.path.abspath(filename))
    if not os.path.exists(dir):
        os.makedirs(dir)
    # Create a unique name for the temporary file using the PID and current thread ID
    pid = os.getpid()
    thread_id = threading.current_thread().ident
    temp_name = "{}.tmp{}-{}".format(filename, pid, thread_id)
    try:
        # Create a temporary file, write the JSON to it and flush to disk
        with open(temp_name, 'w') as temp_file:
            # Write the JSON to the temporary file, flush it to disk and close it
            json.dump(object, temp_file, **json_dump_kwargs)
            temp_file.flush()
            os.fsync(temp_file.fileno())
        # Rename the temporary file to the target filename
        os.rename(temp_name, filename)
    finally:
        try:
            # Unconditionally remove the temporary file, ignoring the error if
            # the file no longer exists
            os.remove(temp_name)
        except OSError as e:
            if e.errno == errno.ENOENT:
                pass
            else:
                raise


def tail(filename, maxlen=100):
    from collections import deque
    tail_str = ''
    with open(filename) as openfile:
        for line in deque(openfile, maxlen=maxlen):
            tail_str += line
    return tail_str


def print_warning(message):
    '''
    Generic ccpem warning message.
    '''
    print '\nCCP-EM Warning:'
    print '  {0}'.format(message)


def print_error(message):
    '''
    Generic ccpem error message.
    '''
    print '\nCCP-EM Error:'
    print '  {0}'.format(message)


def print_header(message=None, return_str=False, total_len=80):
    mess_len = len(message)
    fill_len = total_len - mess_len + 1
    fill_rl = fill_len // 2
    fill_l = fill_rl
    fill_r = fill_rl
    if (fill_rl * 2 != fill_len):
        fill_r +=1
    #
    header = '\n' + '_' * total_len + '\n'
    if len(header) > 80:
        header += '\n' + '_' * (fill_l-1) + ' ' + message + ' ' + '_' * (fill_r-2) + '\n'
    else:
        header += '\n' + '_' * (fill_l-1) + ' ' + message + ' ' + '_' * (fill_r-1) + '\n'
    #
    if return_str:
        return header
    else:
        print header


def print_sub_header(message=None, return_str=False, total_len=80):
    mess_len = len(message)
    fill_len = total_len - mess_len +1 
    sub_header = '\n' + message + ' ' + '_' * (fill_len-2) + '\n'
    if return_str:
        return sub_header
    else:
        print sub_header


def print_sub_sub_header(message=None, return_str=False, total_len=80):
    message = '__ ' + message
    mess_len = len(message)
    fill_len = total_len - mess_len + 1
    sub_header = '\n' + message + ' ' + '_' * (fill_len-2) + '\n'
    if return_str:
        return sub_header
    else:
        print sub_header


def print_dictionary(dictionary):
    for key, value in dictionary.items():
        print '  {0:>25} : {1:<20}'.format(key, value)


def print_footer():
    print '_' * 80 + '\n'


def return_footer():
    return '_' * 80 + '\n'


def print_pass_regression_test_footer(message=None, total_len=80):
    mess_len = len(message)
    fill_len = total_len - mess_len
    sub_sub_header = '\n' + message + ' ' + '_' * (fill_len-2) + '\n'
    print sub_sub_header
    print '_' * total_len


def get_image_list_from_directory(image_file_directory=None):
    print_sub_header(message='Get images from directory')
    images = []
    print "  From directory :\n", image_file_directory
    extension = [".png", ".jpg", ".tif", ".bmp"]
    try:
        for filename in sorted(os.listdir(image_file_directory)):
            for ext in extension:
                if filename.lower().endswith(ext):
                    print "    Get image : ", filename
                    images.append(image_file_directory + filename)
    except OSError:
        msg = "\n\nError. Directory not found : \n" + image_file_directory
        raise Exception(msg)
    print "\n  Total number of images : ", len(images)
    return images


def convert_unicode_to_string(input):
    if isinstance(input, dict):
        return {
            convert_unicode_to_string(key):
                convert_unicode_to_string(
                    value) for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [convert_unicode_to_string(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input


def get_git_revision():
    path = os.path.dirname(__file__)
    try:
        revision = subprocess.check_output(
            "git describe --tags --always --long HEAD",
            shell=True,
            cwd=path).strip()
        if revision == '':
            revision = None
    except subprocess.CalledProcessError as e:
        print e
    return revision


def get_path_abs(path):
    '''
    Return absolute path.
    '''
    path = os.path.abspath(path)
    return path


def get_test_data_path(test_data_module, file_name):
    '''
    Get the path to a test data file in the same directory as a given module
    '''
    return os.path.join(os.path.dirname(inspect.getfile(test_data_module)), file_name)


def main():
    '''
    For testing
    '''
    version = CCPEMVersion()
    print version.version
    print version.build_time
    print version.name


if __name__ == '__main__':
    main()
