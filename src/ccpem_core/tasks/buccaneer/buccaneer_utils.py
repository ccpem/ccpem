#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import os
import gemmi
import json
from ccpem_core.model_tools import gemmi_utils


def read_structure_from_pdb_and_mmcif(xyz_in):
    st = gemmi.read_structure(xyz_in)
    st_cif = None
    dirpath, filename = os.path.split(os.path.abspath(xyz_in))
    spext = filename.split('.')
    if spext[1] in ('pdb', 'ent'):
        cif_in = spext[0] + '.mmcif'
        if os.path.isfile(os.path.join(dirpath, cif_in)):
            st_cif = gemmi.read_structure(os.path.join(dirpath, cif_in))

    return st, st_cif


def shiftback_models(shifts_json, pipeline_ncycles, job_location):
    shiftback_folder = os.path.join(job_location, 'shiftback_models')
    if not os.path.isdir(shiftback_folder):
        os.mkdir(shiftback_folder)
    info = json.load(open(shifts_json))
    org_cell = gemmi.UnitCell(*info["cell"])
    shifts = gemmi.Position(*info["shifts"])

    print("Start shifting back output models.")
    for i in range(1, pipeline_ncycles+1):
        xyz_in = os.path.join(job_location, 'build{0}.pdb'.format(i))
        refxyz_in = os.path.join(job_location, 'refined{0}.pdb'.format(i))
        st, st_cif = read_structure_from_pdb_and_mmcif(xyz_in)
        st.cell = org_cell
        fname_out = os.path.join(
            shiftback_folder, 'build{0}_shiftback'.format(i))
        ref_fname_out = os.path.join(
            shiftback_folder, 'refined{0}_shiftback'.format(i))

        # Need to modify write_structure_as_mmcif to write out the cif blocks
        # written from refmac
        gemmi_utils.shift_coordinates(
            in_model_path=st, out_model_path=None, trans_matrix=-shifts, input_obj=True)
        gemmi_utils.write_structure_as_pdb(st, fname_out+'.pdb')
        if st_cif is not None:
            gemmi_utils.shift_coordinates(
                in_model_path=st_cif, out_model_path=None, trans_matrix=-shifts, input_obj=True)
            gemmi_utils.write_structure_as_mmcif(st_cif, fname_out+'.mmcif')

        st, st_cif = read_structure_from_pdb_and_mmcif(refxyz_in)
        gemmi_utils.shift_coordinates(
            in_model_path=st_cif, out_model_path=None, trans_matrix=-shifts, input_obj=True)
        gemmi_utils.shift_coordinates(
            in_model_path=st, out_model_path=None, trans_matrix=-shifts, input_obj=True)

        gemmi_utils.write_structure_as_mmcif(st_cif, ref_fname_out+'.mmcif')
        gemmi_utils.write_structure_as_pdb(st, ref_fname_out+'.pdb')

    print("Shifted back output models saved to:\n {0}".format(
        shiftback_folder))


if __name__ == '__main__':
    import sys
    shift_json = str(sys.argv[1])
    pipeline_ncycles = int(sys.argv[2])
    job_location = str(sys.argv[3])
    shiftback_models(shift_json, pipeline_ncycles, job_location)
