import sys
import re
import os
import time
import pyrvapi
import fileinput
import collections
import shutil, copy
import numpy as np
import pandas as pd
import math
try:
    from scipy.spatial import cKDTree as kdtree
except ImportError:
    from scipy.spatial import KDTree as kdtree
from lxml import etree
from ccpem_core import process_manager
from ccpem_core import ccpem_utils
from ccpem_core.tasks.refmac import refmac_results
from ccpem_core.ccpem_utils.ccp4_log_parser import smartie
from ccpem_core.data_model import metadata_utils
from __builtin__ import True, False
from collections import OrderedDict
from pyrvapi_utils import *
from smoc.set_smoc_results import SetSMOCResults, SetSCCCResults
from molprobity.set_molprobity_results import SetMolprobityResults
from pi_score.set_piscore_results import SetPIscoreResults
from cablam.set_cablam_results import SetCaBLAMResults
from refmac.set_refmac_results import SetRefmacResults
from global_scores.set_scores_results import SetScoresResults
from dssp.dssp_log_parser import DSSP_LogParser
from jpred.set_jpred_results import SetJpredResults
from bfactor_analysis.analyse_bfactors_results import SetBfactResults
from cootscript_edit import SetCootScript
import json
import gc
from validate_results_settings import residue_summary_table_settings
from outlier_utils import cluster_residues

# Resolution label (equivalent to <4SSQ/LL>
ang_min_one = (ur'Resolution (\u00c5-\u00B9)').encode('utf-8')
angstrom_label = (ur'Resolution (\u00c5)').encode('utf-8')

global_tab = 'global_tab'
local_tab = 'local_tab'
global_tab_name = 'Results (Global)'
local_tab_name = 'Results (Local)'
global_reference_tab = 'global_reference_tab'
global_reference_tab_name = 'References (Global)'
local_reference_tab = 'local_reference_tab'
local_reference_tab_name = 'References (Local)'
    
class PipelineResultsViewer(object):
    '''
    Results viewer for job pipeline
    '''
    
    def __init__(self,
                 pipeline=None,
                 pipeline_path=None):
        if pipeline_path is not None:
            self.pipeline = process_manager.CCPEMPipeline(
                pipeline=None,
                import_json=pipeline_path)
        else:
            self.pipeline = pipeline
        # setup doc
        ccp4 = os.environ['CCPEM']
        share_jsrview = os.path.join(ccp4, 'share', 'jsrview')
        self.directory = os.path.join(self.pipeline.location, 'report')
        self.index = os.path.join(self.directory, 'index.html')
         
        if os.path.exists(self.directory):
            shutil.rmtree(self.directory)
        ccpem_utils.check_directory_and_make(self.directory)
        #setup pages
        pyrvapi.rvapi_init_document(self.pipeline.location, self.directory,
                                    self.pipeline.location, 1, 4, share_jsrview,
                                    None, None, None, None)
        pyrvapi.rvapi_flush()
        #lists of job processes
        self.bfactor_process = []
        self.molprobity_process = []
        self.piscore_process = []
        self.cablam_process = []
        self.refmac_process = []
        self.scores_process = []
        self.smoc_process = []
        self.fdr_process = []
        self.fsc_process = []
        self.diffmap_process = []
        self.sccc_process = []
        self.dssp_process = []
        self.jpred_process = []
        dict_local_processes = {}
        self.list_jobs = []
        #count job types
        count_total_jobs, dict_num_jobs = self.count_job_types()
        #print count_total_jobs
        num_local_process_complete = 0
        if 'SMOC' in dict_num_jobs and dict_num_jobs['SMOC'] > 0 : num_local_process_complete += 1
        if 'FDR' in dict_num_jobs and dict_num_jobs['FDR'] > 0 : num_local_process_complete += 1
        if 'FSC' in dict_num_jobs and dict_num_jobs['FSC'] > 0 : num_local_process_complete += 1
        if 'Diffmap' in dict_num_jobs and dict_num_jobs['Diffmap'] > 0 : num_local_process_complete += 1
        #print 'Total number of jobs: ', count_total_jobs, dict_num_jobs
        #set coot script
        cs = SetCootScript(self.pipeline.location)
        #list of chains with outliers
        self.list_chains = []
        #list_summary_methods: methods for summary table
        self.list_summary_methods = []
        #dictionary with residue outlier details for each method 
        #self.residue_outliers[method][mid][chain][res] = outlier type
        self.residue_outliers = OrderedDict()
        #dictionary with residue coordinates for each model
        self.residue_coordinates = {}
        #output report for global results
        gr = open('global_report.txt','w')
        
        flag_process_complete= 0#local processes
        self.list_failed_jobs = []
        self.list_finished_jobs = []
        self.list_complete = []
        dssp_dict=None
        #set timeout for results
        timeout = time.time() + 15*60
        flag_add_tabs = True
        flag_complete = False
        dict_local_fail = {}
        # get the process that finished and generate results
        while len(self.list_failed_jobs)+len(self.list_finished_jobs) < count_total_jobs:
            if time.time() > timeout: break
            time.sleep(2)
            for jobs in self.pipeline.pipeline:
                for job in jobs:
                    status = job.get_status()
                    #skip failed jobs
                    if status not in ['finished','running','ready']: 
                        if not job.name in self.list_failed_jobs and \
                            not 'Refmac (masked)' in job.name:
                            self.list_failed_jobs.append(job.name)
                            if 'SMOC' in job.name:
                                try: dict_local_fail['SMOC'] += 1
                                except KeyError:
                                    dict_local_fail['SMOC'] = 1
                                if dict_num_jobs['SMOC'] == dict_local_fail['SMOC'] : num_local_process_complete -= 1
                            if 'FDR' in job.name:
                                try: dict_local_fail['FDR'] += 1
                                except KeyError:
                                    dict_local_fail['FDR'] = 1
                                if dict_num_jobs['FDR'] == dict_local_fail['FDR'] : num_local_process_complete -= 1
                        print 'Job failed ',job.name
                        if 'SMOC' in job.name or 'FDR' in job.name:
                            if flag_process_complete < num_local_process_complete:
                                continue
                        else: continue
                    #skip running jobs
                    if status in ['running','ready']:
                        continue
                    #skip results job if part of pipeline
                    if 'Results' in job.name:
                        continue
                    #add finished jobs to list
                    #print job.name, 'job loop'
                    if not job.name in self.list_finished_jobs and \
                        not 'Refmac (masked)' in job.name:
                        self.list_finished_jobs.append(job.name)
                    else: continue
                    time.sleep(2)
                    
                    if 'Bfactor' in job.name and \
                        'Bfactor' not in self.list_complete:
                        self.bfactor_process.append(job)
                        # all jobs of this type are completed?
                        flag_complete = self.check_job_set_complete('Bfactor',
                                                    dict_num_jobs['Bfactor'])
                        if flag_complete == 1:
                            if flag_add_tabs:
                                #add tabs
                                self.add_global_local_tabs()
                                flag_add_tabs = False
                            bfa = SetBfactResults(self.bfactor_process,
                                                      self.list_chains,
                                                      cootset_instance=cs,
                                                      glob_reportfile=gr)
                            self.update_list_chains(bfa.list_chains)
                            gc.collect()
                            
                            
                    if 'Molprobity' in job.name and \
                            'Molprobity' not in self.list_complete:
                        self.molprobity_process.append(job)
                        # all jobs of this type are completed?
                        flag_complete = self.check_job_set_complete('Molprobity',
                                                        dict_num_jobs['Molprobity'])
                        if flag_complete == 1:
                            if flag_add_tabs:
                                #add tabs
                                self.add_global_local_tabs()
                                flag_add_tabs = False
                            mp = SetMolprobityResults(self.molprobity_process,
                                                      self.list_chains,
                                                      cootset_instance=cs,
                                                      glob_reportfile=gr)
                            self.update_list_chains(mp.list_chains)
                            if len(mp.residue_outliers) != 0: 
                                self.list_summary_methods.append('molprobity')
                                self.residue_outliers['molprobity'] = mp.residue_outliers
                                self.update_coordinates(mp.residue_coordinates)
                            #collect non-ref/deleted items from the object
                            gc.collect()
             
                    if 'Refmac' in job.name and \
                        'Refmac' not in self.list_complete and\
                        'Refmac (masked)' not in job.name:
                        #print job.name, dict_num_jobs['Refmac']
                        self.refmac_process.append(job)
                        flag_complete = self.check_job_set_complete('Refmac',
                                                        dict_num_jobs['Refmac'])
                        if flag_complete == 1:
                            if flag_add_tabs:
                                #add tabs
                                self.add_global_local_tabs()
                                flag_add_tabs = False
                            SetRefmacResults(self.refmac_process,
                                             glob_reportfile=gr)
    #                 if 'Refmac refine (local)' in job.name:
    #                     self.refmac_process.append(job)
                    if 'TEMPy scores' in job.name and \
                        'TEMPy scores' not in self.list_complete:
                        #print job.name, status
                        self.scores_process.append(job)
                        flag_complete = self.check_job_set_complete('TEMPy scores',
                                                        dict_num_jobs['TEMPy scores'])
                        if flag_complete == 1:
                            if flag_add_tabs:
                                #add tabs
                                self.add_global_local_tabs()
                                flag_add_tabs = False
                            tsc = SetScoresResults(self.scores_process, 
                                                self.list_chains,
                                                glob_reportfile=gr)
                                 
                            gc.collect()
                    
                    if ('SMOC' in job.name and \
                        'SMOC' not in self.list_complete) or \
                        ('FDR' in job.name and \
                        'FDR' not in self.list_complete):
                        #print job.name, status
                        
                        
                        if 'SMOC' in job.name: 
                            self.smoc_process.append(job)
                            flag_complete = self.check_job_set_complete('SMOC',
                                                        dict_num_jobs['SMOC'])
                            #print 'smoc',flag_complete, dict_num_jobs['SMOC']
                            flag_process_complete += flag_complete
                            dict_local_processes['smoc'] = self.smoc_process
                        elif 'FDR' in job.name: 
                            self.fdr_process.append(job)
                            flag_complete = self.check_job_set_complete('FDR',
                                                        dict_num_jobs['FDR'])
                            #print 'fdr',flag_complete, dict_num_jobs['FDR']
                            flag_process_complete += flag_complete
                            dict_local_processes['fdr'] = self.fdr_process
                        elif 'FSC' in job.name: 
                            self.fsc_process.append(job)
                            dict_local_processes['fsc'] = self.fsc_process
                        elif 'Diffmap' in job.name: 
                            self.diffmap_process.append(job)
                            dict_local_processes['diffmap'] = self.diffmap_process
                        
                        if flag_process_complete == num_local_process_complete:
                            if flag_add_tabs:
                                #add tabs
                                self.add_global_local_tabs()
                                flag_add_tabs = False
                            sm = SetSMOCResults(dict_local_processes = dict_local_processes,
                                                list_chains=self.list_chains,
                                                pipeline_location=self.pipeline.location,
                                                cootset_instance=cs,
                                                glob_reportfile=gr)
                            for process_name in sm.residue_outliers:
                                if len(sm.residue_outliers[process_name]) != 0: 
                                    self.list_summary_methods.append(process_name)
                                    for mapid in sm.residue_outliers[process_name]:
                                        if mapid[-1] == '0':#main map
                                            self.residue_outliers[process_name] = sm.residue_outliers[process_name][mapid]
                            #print 'update coordinates', time.time()
                            for mapid in sm.residue_coordinates:
                                if mapid[-1] == '0':#main map
                                    self.update_coordinates(sm.residue_coordinates[mapid])
                            #print 'update chain list', time.time()
                            self.update_list_chains(sm.list_chains)
                                 
                            gc.collect()
                            
                    if 'SCCC' in job.name and \
                            'SCCC' not in self.list_complete:
                        self.sccc_process = job
                        #self.smoc_process.append(job)
                        flag_complete = self.check_job_set_complete('SCCC',
                                                        dict_num_jobs['SCCC'])
                    
                    if 'PI-score' in job.name and \
                            'PI-score' not in self.list_complete:
                        self.piscore_process.append(job)
                        # all jobs of this type are completed?
                        flag_complete = self.check_job_set_complete('PI-score',
                                                        dict_num_jobs['PI-score'])
                        if flag_complete == 1:
                            if flag_add_tabs:
                                #add tabs
                                self.add_global_local_tabs()
                                flag_add_tabs = False
                            p = SetPIscoreResults(self.piscore_process,
                                                      self.list_chains,
                                                      cootset_instance=cs,
                                                      glob_reportfile=gr)
                            self.update_list_chains(p.list_chains)
#                             if len(p.residue_outliers) != 0: 
#                                 self.list_summary_methods.append('piscore')
#                                 self.residue_outliers['piscore'] = mp.residue_outliers
#                                 self.update_coordinates(mp.residue_coordinates)
                            #collect non-ref/deleted items from the object
                            gc.collect()

                    if 'CABLAM' in job.name and \
                            'CABLAM' not in self.list_complete:
                        self.cablam_process.append(job)
                        flag_complete = self.check_job_set_complete('CABLAM',
                                                        dict_num_jobs['CABLAM'])
                        if flag_complete == 1:
                            if flag_add_tabs:
                                #add tabs
                                self.add_global_local_tabs()
                                flag_add_tabs = False
                            cm = SetCaBLAMResults(self.cablam_process,
                                                  list_chains=self.list_chains,
                                                  dssp_dict=dssp_dict,
                                                    cootset_instance=cs,
                                                    glob_reportfile=gr)
             
                            self.update_list_chains(cm.list_chains)
                             
                            if len(cm.residue_outliers) != 0: 
                                self.list_summary_methods.append('cablam')
                                self.residue_outliers['cablam'] = \
                                            copy.deepcopy(cm.residue_outliers)
                                del cm.residue_outliers
                                self.update_coordinates(cm.residue_coordinates)
                            gc.collect()
             
                    if 'DSSP' in job.name and \
                        'DSSP' not in self.list_complete:
                        self.dssp_process.append(job)
                        flag_complete = self.check_job_set_complete('DSSP',
                                                        dict_num_jobs['DSSP'])
                        if flag_complete == 1:
                            dp = DSSP_LogParser(self.dssp_process)
                            #if self.list_chains == None: self.list_chains = dp.list_chains
                            dssp_dict = dp.dict_sses
                    if 'Jpred Download' in job.name and \
                        'Jpred Download' not in self.list_complete:
                        self.jpred_process.append(job)
                        flag_complete = self.check_job_set_complete('Jpred Download',
                                                        dict_num_jobs['Jpred'])
                        if flag_complete == 1:
                            if flag_add_tabs:
                                #add tabs
                                self.add_global_local_tabs()
                                flag_add_tabs = False
                            jp = SetJpredResults(self.jpred_process,
                                            list_chains=self.list_chains,
                                                  dssp_dict=dssp_dict,
                                                    cootset_instance=cs)
                            if len(jp.residue_outliers) != 0: 
                                self.list_summary_methods.append('jpred')
                                self.residue_outliers['jpred'] = jp.residue_outliers
                                self.update_coordinates(jp.residue_coordinates)
                            gc.collect()
                    pyrvapi.rvapi_flush()
                    
        gr.flush()
        gr.close()
        #print self.residue_coordinates
        #print self.list_complete
        if len(self.list_finished_jobs) == 0 or len(self.list_complete) == 0:
            pyrvapi.rvapi_add_tab( global_tab, "", True)
            pyrvapi.rvapi_add_section("results_section","",global_tab,0,0,1,1,True )
            pyrvapi.rvapi_add_text("Jobs failed", "results_section", 1, 0, 1, 1)
            pyrvapi.rvapi_flush()
        #if no local outliers
        elif len(self.residue_outliers) == 0 and not 'PI-score' in self.list_complete:
            pyrvapi.rvapi_add_section("localresults_section","",local_tab,0,0,1,1,True )
            pyrvapi.rvapi_add_text("No local outliers", "localresults_section", 1, 0, 1, 1)
            pyrvapi.rvapi_flush()
        elif not 'PI-score' in self.list_complete or len(self.list_complete) > 2:
             
            #cluster functions class
            self.cl_out = cluster_residues()
            #cluster outliers spatially
            self.cluster_outliers()
            pyrvapi.rvapi_flush()
            #set outlier summary table
            self.rsumm = residue_summary_table_settings()
            
            if len(self.list_summary_methods) > 0:
                self.list_summary_methods.insert(0,'residue')
                self.convert_outlierdict_to_dataframe()
                self.get_outlier_summary()
             
            self.cootset = cs
            self.add_cootdata()
            pyrvapi.rvapi_flush()

    def count_job_types(self):
        '''
        Count total number of jobs of each type
        '''
        #dict of job type counts
        dict_num_jobs = {}
        count_total_jobs = 0
        for jobs in self.pipeline.pipeline:
            for job in jobs:
                if 'Molprobity' in job.name:
                    try: dict_num_jobs['Molprobity'] += 1
                    except KeyError:
                        dict_num_jobs['Molprobity'] = 1
                elif 'Bfactor' in job.name:
                    try: dict_num_jobs['Bfactor'] += 1
                    except KeyError:
                        dict_num_jobs['Bfactor'] = 1
                elif 'Refmac' in job.name and 'Refmac (masked)' not in job.name:
                    try: dict_num_jobs['Refmac'] += 1
                    except KeyError:
                        dict_num_jobs['Refmac'] = 1
                elif 'TEMPy scores' in job.name:
                    try: dict_num_jobs['TEMPy scores'] += 1
                    except KeyError:
                        dict_num_jobs['TEMPy scores'] = 1
                elif 'SMOC' in job.name:
                    try: dict_num_jobs['SMOC'] += 1
                    except KeyError:
                        dict_num_jobs['SMOC'] = 1
                elif 'FDR' in job.name:
                    try: dict_num_jobs['FDR'] += 1
                    except KeyError:
                        dict_num_jobs['FDR'] = 1
                elif 'FSC' in job.name:
                    try: dict_num_jobs['FSC'] += 1
                    except KeyError:
                        dict_num_jobs['FSC'] = 1
                elif 'Diffmap' in job.name:
                    try: dict_num_jobs['Diffmap'] += 1
                    except KeyError:
                        dict_num_jobs['Diffmap'] = 1
                elif 'CABLAM' in job.name:
                    try: dict_num_jobs['CABLAM'] += 1
                    except KeyError:
                        dict_num_jobs['CABLAM'] = 1
                elif 'DSSP' in job.name:
                    try: dict_num_jobs['DSSP'] += 1
                    except KeyError:
                        dict_num_jobs['DSSP'] = 1
                elif 'Jpred Download' in job.name:
                    try: dict_num_jobs['Jpred'] += 1
                    except KeyError:
                        dict_num_jobs['Jpred'] = 1
                elif 'PI-score' in job.name:
                    try: dict_num_jobs['PI-score'] += 1
                    except KeyError:
                        dict_num_jobs['PI-score'] = 1
                if not 'Results' in job.name and not job.name in self.list_jobs \
                and not 'Refmac (masked)' in job.name: 
                    count_total_jobs += 1
                    self.list_jobs.append(job.name)
        return count_total_jobs, dict_num_jobs

    def check_job_set_complete(self,job_name, num_jobs):
        '''
        Check if all jobs of certain type are finished/failed
        '''
        count_finished_jobs = 0
        for name in self.list_finished_jobs:
            if job_name in name:
                count_finished_jobs += 1
        count_failed_jobs = 0
        for name in self.list_failed_jobs:
            if job_name in name:
                count_failed_jobs += 1
        count_jobs = count_finished_jobs + count_failed_jobs
        
        if count_jobs < num_jobs:
            return 0
        if count_jobs == count_failed_jobs: return -1
        self.list_complete.append(job_name)
        #print job_name, count_jobs, num_jobs
        #print self.list_complete
        return 1
    
    def add_global_local_tabs(self):
        '''
        Add global and local tabs in validation Results
        '''
        pyrvapi.rvapi_add_tab( global_tab, global_tab_name, True)
        pyrvapi.rvapi_add_tab( local_tab, local_tab_name, True)
#         pyrvapi.rvapi_add_tab( global_reference_tab, global_reference_tab_name, True)
#         pyrvapi.rvapi_add_tab( local_reference_tab, local_reference_tab_name, True)
        pyrvapi.rvapi_flush()

    def update_list_chains(self,list_chains):
        '''
        Update list of chains based on outliers from each method
        '''
        for chain in list_chains:
            if not chain in self.list_chains:
                self.list_chains.append(chain)
        
    def add_chain_sections(self):
        '''
        Add result sections for chains
        '''
        for chain in self.list_chains:
            chain_section = chain
            chain_name = 'chain '+chain
            pyrvapi.rvapi_add_section(chain_section, chain_name, 
                                          local_tab, 0, 0, 1, 1, False)
    
    def get_outlier_summary(self):
        '''
        Merge outlier types for each residue
        '''
        self.dict_cluster_number = {}
        self.dict_coot_outlier_clusters = {}
        #chains
        for chain in self.list_chains:
            dict_model_outlier = OrderedDict()
            chain_table_flag = False
            #methods
            for met in self.list_summary_methods[1:]:
#                 for mapid in self.residue_outliers[met]:
#                     if not mapid in dict_model_outlier: 
#                             dict_model_outlier[mapid] = {}
#                     if not mapid in self.dict_cluster_number: 
#                             self.dict_cluster_number[mapid] = {}
                for pdbid in self.residue_outliers[met]:
                    if not chain in self.residue_outliers[met][pdbid]: continue
                    if not pdbid in dict_model_outlier: 
                        dict_model_outlier[pdbid] = {}
                    if not pdbid in self.dict_cluster_number:
                        self.dict_cluster_number[pdbid] = [1,1]
                    #add outlier set to dict_model_outlier
                    self.add_new_residue_outlier_set(self.residue_outliers[met][pdbid][chain],
                                                 dict_model_outlier[pdbid],pdbid,
                                                 method=met
                                                 )
                    if len(dict_model_outlier[pdbid]) > 0:
                        chain_table_flag = True
                        
            if chain_table_flag:
                self.set_summary_tables(dict_model_outlier,chain)
                self.dict_cluster_number[pdbid][1] = 1
                self.set_coot_dict_clusters(dict_model_outlier, chain)
        
    
    def set_summary_tables(self,dict_model_outlier,chain):
        '''
        Set results tables under each chain section 
        with list of residues and outlier types
        '''
        
        summary_table_subsection = "ressummary"+chain
        self.add_table_subsection(chain,summary_table_subsection)
        self.set_summary_table_chain(dict_model_outlier,chain,summary_table_subsection)

    def set_summary_table_chain(self,dict_model_outlier,chain,summary_table_subsection):
        '''
        Add outlier data to summary table for each chain
        '''
        list_method_indices = [self.rsumm.data_titles.index(met) for met in self.list_summary_methods]
        
#         for mapid in dict_model_outlier:
        for pdbid in dict_model_outlier:
#                 if mapid == 'nomap':
            tid = pdbid
#                 else:
#                     tid = mapid+'_'+pdbid
            dict_outlier = dict_model_outlier[pdbid]
            outlier_res = dict_outlier.keys()
            outlier_res.sort()
            data_array = []
            list_clust_sep = []
            if pdbid in self.dict_residue_clusters:
                #check if outliers form clusters
                try: 
                    self.cl_out.check_seq_neigh_cluster(outlier_res, 
                                                  self.dict_residue_clusters[pdbid][chain])
                    self.cl_out.merge_list_clusters(self.dict_residue_clusters[pdbid][chain])
                    ct_cluster = 0
                    for res_list in self.dict_residue_clusters[pdbid][chain]:
                        flag_add_cluster = 0
                        for res in res_list:
                            if res in dict_outlier:
                                len_row = len(dict_outlier[res])
                                if len_row > 0:
                                    #for each cluster, first row includes cluster ID
                                    if flag_add_cluster == 0:
                                        ct_cluster += 1
                                        if ct_cluster < len(self.dict_residue_clusters[pdbid][chain]):
                                            row_data = [str(ct_cluster)]
                                        #last cluster has individual outliers
                                        elif ct_cluster == len(self.dict_residue_clusters[pdbid][chain]):
                                            row_data = ['-']
                                        else: continue
                                        row_data.extend(dict_outlier[res])
                                        data_array.append(row_data)
                                    else: data_array.append(dict_outlier[res])
                                    flag_add_cluster += 1
                        if flag_add_cluster == 0: continue
                        list_clust_sep.append(len(data_array))
#                         #cluster separator
#                         if ct_cluster < len(self.dict_residue_clusters[pdbid][chain]):
#                             data_array.append([' ']*len_row)
#                             data_array.append([' ']*len_row)
                except KeyError: 
                    for res in outlier_res:
                        data_array.append(dict_outlier[res])
            else:
                for res in outlier_res:
                    data_array.append(dict_outlier[res])
            
            table_id = "summarytable"+chain+pdbid
            table_name = "Outlier summary table: "+tid
            list_horz_headers = [self.rsumm.list_horz_headers[index] for index in list_method_indices]
            list_horz_headers = ['Cluster']+list_horz_headers
            list_horz_headers_tips = [self.rsumm.list_horz_header_tooltips[index] for index in list_method_indices]
            list_horz_headers_tips = ['Cluster ID']+list_horz_headers_tips
            add_table_to_section(summary_table_subsection,table_id, table_name)
            #adjust row span for cluster ID
            y=0
#             list_colors = list_cell_colors[:]
#             while len(list_clust_sep) > len(list_colors):
#                 list_colors.extend(list_colors)
            count_rows = 0
            for l in range(len(list_clust_sep)):
                if l == 0: 
                    start_row = 0
                    row_span = list_clust_sep[l]
                else:
                    start_row = list_clust_sep[l-1]
                    row_span = list_clust_sep[l]
                row_span = row_span - start_row
                ##cell_color = "table-"+list_colors[l]
                #set row span
                customize_table_cell(table_id,count_rows,y,
                         rowspan=row_span,colspan=1)
                count_rows += row_span
            #add headers and data
            add_horz_headers_to_table(table_id,list_horz_headers,list_horz_headers_tips)
            add_data_to_table(table_id,data_array)
            textfile = os.path.join(self.pipeline.location,
                                pdbid+'_'+chain+'_cluster_outliers.csv')
            self.add_data_to_textfile(textfile,list_horz_headers,data_array)
            pyrvapi.rvapi_flush()
    
    def add_data_to_textfile(self,textfile,list_horz_headers,data_array):
        with open(textfile,'w') as c:
            c.write('#Molprobity: geometry outliers reported by molprobity.\n'
                    '#CaBLAM: Backbone geometry outliers reported by molprobity.\n'
                    '#SMOC: local fit to map outliers reported by TEMPy SMOC.\n'
                    '#Jpred: Secondary structure prediction from sequence inconsistent with model.\n'
                    )
            list_header = []
            ct_str = 0
            for header in list_horz_headers:
                formatted_header = ' '.join(header.split('<br>'))
                formatted_header = '_'.join(formatted_header.split())
                if ct_str < 2:
                    list_header.append('{}'.format(formatted_header))
                else:
                    list_header.append('{}'.format(formatted_header))
                ct_str += 1
            c.write(','.join(list_header)+'\n')
            len_prev = 0
            for row in data_array:
                list_outl = []
                ct_str = 0
                if len_prev != 0 and len_prev - len(row) == 1:#cluster number missing
                    list_outl.append('{}'.format(prev_row[0]))
                    ct_str += 1
                
                for outl in row:
                    formatted_outl = '*'.join(outl.split('<br>'))
                    if ct_str < 2:
                        list_outl.append('{}'.format(formatted_outl))
                    else:
                        list_outl.append('{}'.format(formatted_outl))
                    ct_str += 1
                c.write(','.join(list_outl)+'\n')
                if len_prev <= len(row):
                    len_prev = len(row)
                    prev_row = row
                
    def set_df_csv(self,dict_data,data_id):
        df = pd.DataFrame.from_dict(dict_data, orient='index')
        #df1 = df.replace(np.nan,' ')
        df.to_csv(os.path.join(self.pipeline.location,
                                data_id+'_outliersummary.txt'),sep=',')
    def update_residue_outliers(self,res_outliers):
        '''
        update dictionary of outliers with new outlier set
        self.dict_res_outliers[pdbid][chain] = 
                list_outlier_res_num
        '''
        for pdbid in res_outliers:
            if not pdbid in self.dict_res_outliers:
                self.dict_res_outliers[pdbid] = {}
            for chain in res_outliers[pdbid]:
                if not chain in self.dict_res_outliers[pdbid]:
                    self.dict_res_outliers[pdbid][chain] = res_outliers[pdbid][chain].keys()
                else:
                    self.dict_res_outliers[pdbid][chain].extend(res_outliers[pdbid][chain].keys())
                
    def update_coordinates(self,dict_coordinates):
        '''
        Update residue CA coordinates for outliers
        '''
        for pdbid in dict_coordinates:
            if not pdbid in self.residue_coordinates:
                self.residue_coordinates[pdbid] = dict_coordinates[pdbid]
            else:
                for chain in dict_coordinates[pdbid]:
                    if not chain in self.residue_coordinates[pdbid]:
                        self.residue_coordinates[pdbid][chain] = dict_coordinates[pdbid][chain]
                    else:
                        for resnum in dict_coordinates[pdbid][chain]:
                            res_coordinate = dict_coordinates[pdbid][chain][resnum]
                            if len(res_coordinate) != 3: continue
                            if not resnum in self.residue_coordinates[pdbid][chain]:
                                self.residue_coordinates[pdbid][chain][resnum] = res_coordinate
                            elif len(self.residue_coordinates[pdbid][chain][resnum]) != 3:
                                self.residue_coordinates[pdbid][chain][resnum] = res_coordinate
    
    def merge_coordinates(self):
        #merge geometry outlier coordinates with map specific outliers
        for mapid in self.residue_coordinates:
            if mapid != 'nomap' and 'nomap' in self.residue_coordinates:
                self.update_coordinates(self.residue_coordinates['nomap'],mapid)
        if 'nomap' in self.residue_coordinates:
            del self.residue_coordinates['nomap']
                                         
    def cluster_outliers(self):
        '''
        Cluster outlier residues based on spatial coordinates
        Uses scipy kdtree
        '''
        list_res_clusters = []
        self.dict_residue_clusters = {}
        #get outlier residues from residue_outliers dict
#         for chain in self.list_chains:
#             for met in self.list_summary_methods:
#                 for pdbid in self.residue_outliers[met]:
#                     if not chain in self.residue_outliers[met][pdbid]: continue
#                     if not pdbid in self.dict_residue_clusters:
#                         self.dict_residue_clusters[pdbid] = {}
#                     #check if coordinate data is available
#                     if not pdbid in self.residue_coordinates:
#                         continue
#                     if not chain in chain in self.residue_coordinates[pdbid]:
#                         continue
#                     list_coord = []
#                     list_res = []
#                     for res in self.residue_outliers[met][pdbid][chain]:
#                         list_coord.append(self.residue_outliers[met][pdbid][chain][res])
#                         list_res.append(str(res))
#                     
#         for mapid in self.residue_coordinates:
#             if not mapid in self.dict_residue_clusters:
#                     self.dict_residue_clusters[mapid] = {}
        for pdbid in self.residue_coordinates:
            if not pdbid in self.dict_residue_clusters:
                self.dict_residue_clusters[pdbid] = {}
            for chain in self.residue_coordinates[pdbid]:
                list_coord = []
                list_res = []
                for res in self.residue_coordinates[pdbid][chain]:
                    if str(res) in list_res: continue
                    list_coord.append(self.residue_coordinates[pdbid][chain][res])
                    list_res.append(str(res)) #resid chain
                #print pdbid, chain, list_res
                if len(list_coord) > 0:
                    list_res_clusters = self.cl_out.get_cluster_outliers(list_res, list_coord)
                    list_res_clusters.sort(key=len,reverse=True)
                
                self.dict_residue_clusters[pdbid][chain] = copy.deepcopy(list_res_clusters)
    
    def set_coot_dict_clusters(self,dict_model_outlier,chain):
        '''
        Save outlier clusters in a dictionary for coot fixing
        '''
        for pdbid in self.dict_residue_clusters:
#                 cl_id = mapid+':'+pdbid
#                 if mapid == 'nomap': cl_id = pdbid
            if not pdbid in self.dict_coot_outlier_clusters:
                self.dict_coot_outlier_clusters[pdbid] = {}
            try:
                dict_outliers = dict_model_outlier[pdbid]
            except KeyError: continue
            try: list_clusters = self.dict_residue_clusters[pdbid][chain]
            except KeyError: continue
#             try:
#                 cl_number = self.dict_cluster_number[pdbid][0]
#             except KeyError: continue
            for l in xrange(len(list_clusters)):
                if len(list_clusters[l]) == 0: continue
                #sorted by cluster size until individuals
                if l > 0 and len(list_clusters[l-1]) < len(list_clusters[l]):
                    break
                list_res = list_clusters[l]
                
                flag_cluster = False
                for res in list_res:
                    try:
                        residue_coordinate = \
                            self.residue_coordinates[pdbid][chain][res]
                    except KeyError: continue
                    if res in dict_outliers:
                        #res,molprobity outlier string, cablam outlier string, smoc
                        outlier_types = dict_outliers[res][:]
                        #rename selected outliers with method name
                        #and merge with '\n'
                        for met_index in xrange(1,len(outlier_types)):
                            #different types within a method
                            split_type = outlier_types[met_index].split('<br>')
                            outlier_types[met_index] = '\n'.join(split_type)
                            if outlier_types[met_index] == '-':
                                outlier_types[met_index] = ''
                                continue
                            #add method name to outlier type
                            if self.list_summary_methods[met_index] in \
                                        ['cablam','smoc','fdr','jpred']:
                                outlier_types[met_index] = \
                                    self.list_summary_methods[met_index] + \
                                    ' '+ outlier_types[met_index]
                        #remove no outlier methods
                        ct_pop = 0
                        for met_index in xrange(len(outlier_types)):
                            if met_index > len(outlier_types): break
                            if len(outlier_types[met_index-ct_pop]) == 0:
                                outlier_types.pop(met_index-ct_pop)
                                ct_pop += 1
                        
                        outlier_types_string = '\n'.join(outlier_types[1:]) #0: 'Residue'
                        
                        flag_cluster = True
                        try:
                            self.dict_coot_outlier_clusters[pdbid]['clusters'].append(
                            (chain,res,self.dict_cluster_number[pdbid][1],outlier_types_string,residue_coordinate))
                        except KeyError:
                            self.dict_coot_outlier_clusters[pdbid]['clusters'] = \
                            [(chain,res,self.dict_cluster_number[pdbid][1],outlier_types_string,residue_coordinate)]
                if flag_cluster: 
                    self.dict_cluster_number[pdbid][0] += 1 #model
                    self.dict_cluster_number[pdbid][1] += 1 #chain
            #break # compute coot data for the first map based outliers only

    def add_cootdata(self):
        '''
        Add cluster outliers for fixing in Coot
        '''
        if self.cootset is not None:
            for pdbid in self.dict_coot_outlier_clusters:
                self.cootset.add_data_to_cootfile(self.dict_coot_outlier_clusters[pdbid],
                    modelid=pdbid,method='clusters')

    def convert_outlierdict_to_dataframe(self):
        '''
        Generate a dataframe of residue outliers
        '''
        dict_df = {}
        for met in self.list_summary_methods[1:]:
#             for mapid in self.residue_outliers[met]:
#                 self.residue_outliers[met][mapid] = {}
            for pdbid in self.residue_outliers[met]:
#                 if mapid == 'nomap':
                df_id = pdbid
#                 else:
#                     df_id = mapid+':'+pdbid
                if not df_id in dict_df:
                    dict_df[df_id] = {}
                for chain in self.residue_outliers[met][pdbid]:
                    for res in self.residue_outliers[met][pdbid][chain]:
                        outlier_det = self.residue_outliers[met][pdbid][chain][res]
                        if isinstance(outlier_det,list):
                            outlier_string = ':'.join(outlier_det)
                        else: outlier_string = outlier_det
                        try: 
                            res_key = int(res)
                        except ValueError: 
                            res_key = res.strip()
                        
                        try: dict_df[df_id][(chain,res_key)][met] = outlier_string
                        except KeyError:
                            dict_df[df_id][(chain,res_key)] = {}
                            dict_df[df_id][(chain,res_key)][met] = outlier_string
        for df_id in dict_df:
            self.set_df_csv(dict_df[df_id], df_id)

    def add_new_residue_outlier_set(self,residue_outliers,dict_outlier,
                                    pdbid,method='molprobity'):
        '''
        Add outlier set to the dictionary dict_outlier[res][method] = outlier name(s)
        '''
        #index for summary table
        met_index = self.list_summary_methods.index(method)
        len_table = len(self.list_summary_methods)
        for res in residue_outliers:
            outlier_det = residue_outliers[res]
            if isinstance(outlier_det,list):
                outlier_string = '<br>'.join(outlier_det)
            else: outlier_string = outlier_det
            res_key = res.strip()
#             try: 
#                 res_key = int(res)
#             except ValueError: 
#                 res_key = res.strip()
                #print res, self.list_summary_methods[met_index], outlier_string
            #set outlier dictionary:
            #[res,outlier type1,outlier type 2,..] 
            try: 
                dict_outlier[res_key][met_index] = outlier_string
            except (KeyError, IndexError) as exc:
                dict_outlier[res_key] = [res]
                dict_outlier[res_key].extend(['-']*(len_table-1))
                dict_outlier[res_key][met_index] = outlier_string
    
    def add_table_subsection(self,chain,summary_table_subsection):
        '''
        Add summary table section
        '''
        pyrvapi.rvapi_add_section1(chain+'/'+summary_table_subsection, 
                               'Summary table', 
                               0, 0, 1, 1, True)
        
    def get_gemmi_model(self,structure_filename):
        '''
        Get gemmi atomic model structure object
        '''
        import gemmi
        model = gemmi.read_structure(structure_filename)[0]
        return model
    def get_residue_ca_coord(self,chain,resnum,resname):
        '''
        Get coordinate given res num and name
        '''
        chain_model = model[chain].get_polymer()
        atom = chain_model[str(resnum)][str(resname)]['CA'][0]
        atom_coord = atom.pos
        return atom_coord.x,atom_coord.y,atom_coord.z


def main():
    PipelineResultsViewer(pipeline_path=sys.argv[1])
if __name__ == '__main__':
    main()


