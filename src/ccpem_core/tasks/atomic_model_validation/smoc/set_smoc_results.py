import sys
import re
import os
import pyrvapi
import shutil
import numpy as np
import pandas as pd
import math, glob
from collections import OrderedDict
from ccpem_core.tasks.atomic_model_validation.pyrvapi_utils import *
from copy import deepcopy
import json

class SetSMOCResults():
    def __init__(self, dict_local_processes={},
                 list_chains=[],
                 pipeline_location=None, cootset_instance=None,
                 glob_reportfile=None,fdr_process=None):
        self.list_process_names = dict_local_processes.keys()
        self.cootset=cootset_instance
        self.smoc_results = OrderedDict()
        self.smoc_csvs = OrderedDict()#smoc scores per model
        self.dict_dataframes = OrderedDict()
        #outlier details
        self.dict_outlier_details = OrderedDict()
        #output json files for each model
        self.dict_jsonfiles = OrderedDict()
        self.list_modelids = []
        self.list_pdbids = []
        self.list_mapids = []
        self.residue_outliers = OrderedDict()
        self.residue_z = OrderedDict()
        self.residue_coordinates = OrderedDict()
        self.dict_cootdata = OrderedDict()
        self.dict_ca_coord = OrderedDict()
        self.list_modelids = []
        #print dict_local_processes.keys()
        for n in dict_local_processes:
            if len(dict_local_processes[n]) > 0:
                process_name = n
                dict_local_processes[n] = sorted(dict_local_processes[n], 
                                     key=lambda j: j.name.split(' ')[-1][-1]+
                                                    j.name.split(' ')[0][-1])
                ct_job = 0
                for job in dict_local_processes[n]:
                    if process_name == 'smoc':
                        process_result = os.path.join(job.location,'smoc_score.csv')
                        modelid = job.name.split(' ')[-1]
                        if not process_name in self.dict_dataframes:
                            self.dict_dataframes[process_name] = {}
                        self.dict_dataframes[process_name][modelid] = pd.read_csv(process_result,
                                              skipinitialspace=True,
                                              skip_blank_lines=True)
                        if not process_name in self.dict_jsonfiles:
                            self.dict_jsonfiles[process_name] = {}
                        self.dict_jsonfiles[process_name][modelid] = os.path.join(job.location,
                                                                modelid+"_smoc.json")
                        if not process_name in self.smoc_csvs:
                            self.smoc_csvs[process_name] = []
                        self.smoc_csvs[process_name].append(
                                os.path.join(job.location,modelid+'_smoc.csv'))
                        
                        try: self.smoc_results['smoc'].append(process_result)
                        except KeyError:
                            self.smoc_results['smoc'] = [process_result]
                            
                    elif process_name == 'fdr':
                        try: process_result = glob.glob(os.path.join(job.location, '*_FDR_ranked_residues.csv'))[0]
                        except: process_result = None
                        modelid = job.name.split(' ')[-1]
                        if not process_name in self.dict_dataframes:
                            self.dict_dataframes[process_name] = {}
                        if os.path.isfile(process_result):
                            self.dict_dataframes[process_name][modelid] = pd.read_csv(process_result)
                        
                        if not process_name in self.dict_jsonfiles:
                            self.dict_jsonfiles[process_name] = {}
                        self.dict_jsonfiles[process_name][modelid] = os.path.join(job.location,
                                                                modelid+"_fdr.json")
                        
                        if not process_name in self.smoc_csvs:
                            self.smoc_csvs[process_name] = []
                        self.smoc_csvs[process_name].append(
                                os.path.join(job.location,modelid+'_fdr.csv'))
                        
                        try: self.smoc_results['fdr'].append(process_result)
                        except KeyError:
                            self.smoc_results['fdr'] = [process_result]
                    
                    
                    if not modelid in self.list_modelids:
                        self.list_modelids.append(modelid)
                
                    pdbid = modelid.split(':')[-1]
                    if not pdbid in self.list_pdbids: 
                        self.list_pdbids.append(pdbid)
                        if pipeline_location is not None:
                            ca_coordfile = os.path.join(pipeline_location,pdbid,'ca_coord.json')
                            if os.path.isfile(ca_coordfile):
                                self.dict_ca_coord[pdbid] = ca_coordfile
                    mapid = modelid.split(':')[0]
                    if not mapid in self.list_mapids: 
                        self.list_mapids.append(mapid)
                    if ct_job == 0: # first map is used as main map, TODO: check this
                        self.main_map = mapid
                    ct_job += 1
                    #self.residue_outliers[pdbid] = {}
                    self.residue_coordinates[mapid] = {}
        #print self.smoc_csvs
        self.list_chains = list_chains
        self.add_chain_sections = False
        if len(self.list_chains) == 0:
            self.add_chain_sections = True
        
        self.min_val = None
        
        #set data for local outliers
        self.set_local_data()
        for process_name in self.list_process_names:
            self.check_chain_order_cootdata(process_name)
        self.add_cootdata(process_name)
#         self.delete_cootdata()
        self.save_outlier_json()
#         #set smoc plots
        self.set_local_results()
        #print 'del results'
        self.delete_dictresults()
        
    def add_cootdata(self,process_name):
        if self.cootset is not None:
            for pdbid in self.dict_cootdata:
                self.cootset.add_data_to_cootfile(self.dict_cootdata[pdbid],
                                                  modelid=pdbid,method=process_name)
                
    def delete_cootdata(self):
        self.dict_cootdata.clear()
        del self.dict_cootdata
    
    def delete_dictplots(self):
        #print 'del dictplots'
        self.dict_pdbdata.clear()
        del self.dict_pdbdata
        self.dict_mapdata.clear()
        del self.dict_mapdata
        
    def delete_dictresults(self):
        self.dict_outlier_details.clear()
        del self.dict_outlier_details
        self.residue_z.clear()
        del self.residue_z

    def set_local_data(self):
#         if not self.add_chain_sections:
#             for chain in self.list_chains:
#                 smoc_subsection = 'smoc'+chain
#                 self.add_subsection_to_chain(chain,smoc_subsection)
        self.dict_pdbdata = OrderedDict()
        self.dict_mapdata = OrderedDict()
        
        for process_name in self.dict_dataframes:
            ct_model = 0
            for mid in self.list_modelids:
                if mid in self.dict_dataframes[process_name]:
                    df = self.dict_dataframes[process_name][mid]
                    #print process, mid, self.smoc_csvs[process_name], ct_model
                    self.set_results_data(df, mid,self.smoc_csvs[process_name][ct_model],process_name)
                    ct_model += 1
    
    def save_outlier_json(self):
        for mid in self.dict_outlier_details:
            try:
                jsonfile = self.dict_jsonfiles[mid]
            except KeyError: continue
            with open(jsonfile,'w') as jf:
                json.dump(self.dict_outlier_details[mid],jf)


    def set_local_results(self):
        self.set_results_graphs()
        self.delete_dictplots()
    
    def add_section_chain(self, chain_section, chain_name):
        pyrvapi.rvapi_add_section(chain_section, chain_name, 
                                          local_tab, 0, 0, 1, 1, False)
    def add_subsection_to_chain(self,chain,smoc_subsection):
        pyrvapi.rvapi_add_section1(chain+'/'+smoc_subsection, 
                               'Per-residue scores', 
                               0, 0, 1, 1, False)
                        
    def set_results_data(self,df,mid,mid_csv=None,process_name='smoc'):
        mapid = mid.split(':')[0]
        pdbid = mid.split(':')[-1]
        list_chains = []
        ct_plot = 0
        list_chain_model = []
#         if not process_name in self.dict_outlier_details:
#             self.dict_outlier_details[process_name] = {}
        if not mid in self.dict_outlier_details:
            self.dict_outlier_details[mid] = {}
        if not pdbid in self.dict_cootdata:
            self.dict_cootdata[pdbid] = {}
        if not process_name in self.dict_cootdata[pdbid]:
            self.dict_cootdata[pdbid][process_name] = []
        if not process_name in self.residue_outliers:
            self.residue_outliers[process_name] = {}
            self.residue_z[process_name] = {}
                
        if not mapid in self.residue_outliers[process_name]:
            self.residue_outliers[process_name][mapid] = {}
            self.residue_z[process_name][mapid] = {}
            self.residue_coordinates[mapid] = {}
        if not pdbid in self.residue_outliers[process_name][mapid]:
            self.residue_outliers[process_name][mapid][pdbid] = {}
            self.residue_z[process_name][mapid][pdbid] = {}
            self.residue_coordinates[mapid][pdbid] = {}
        if mid_csv is not None:
            scoreout = mid_csv
            sco = open(scoreout,'w')
            sco.write('#chain,residue,score,z_local,z_global\n')
        
        #check if ca coordinates are available
        flag_coord_exists = False
        if pdbid in self.dict_ca_coord:
            ca_coord_json = self.dict_ca_coord[pdbid]
            with open(ca_coord_json,'r') as j:
                dict_pdb_ca_coord = json.load(j)
            flag_coord_exists = True
        else:
            dict_pdb_ca_coord = {}
        
        if process_name == 'smoc':
            # Get data
            for pdb in df:
                if not '_' in pdb: continue
                #add plots for each chain
                chain = pdb.split('_')[-1]
                ct_data_index = 0
                if '.' in chain: 
                    chain = chain.split('.')[0]
                    pdbname = '.'.join(pdb.split('.')[:-1])
                if chain == '': chain = ' '
                added_smoc_text = False
                
                #add chain sections
                if self.add_chain_sections:
                    if not chain in self.list_chains:
                        self.list_chains.append(chain)
                    chain_section = chain
                    chain_name = 'chain '+chain
                    self.add_section_chain(chain_section, chain_name)
                
                if chain in self.list_chains:
                    if not chain in self.residue_outliers[process_name][mapid][pdbid]:
                        self.residue_outliers[process_name][mapid][pdbid][chain] = {}
                        self.residue_z[process_name][mapid][pdbid][chain] = {}
                        self.residue_coordinates[mapid][pdbid][chain] = {}
                    if not chain in list_chain_model:
                        dict_results = {}
                        list_chain_model.append(chain)
                        
                    ct_res_chain = 0
                    pdb_df = df[pdb].dropna(axis=0, how='any')
                    #print pdb_df
                    if pdb_df[0] == 'resnum': 
                        dict_results['residue'] = []
                        for val in pdb_df[1:]:
                            residue_num = int(float(val))
                            dict_results['residue'].append(residue_num)
                            try:
                                ca_coord = dict_pdb_ca_coord["1"][chain][str(residue_num)][0]
                                res_name = dict_pdb_ca_coord["1"][chain][str(residue_num)][1]
                                try:
                                    dict_results['CAx'].append(ca_coord[0])
                                    dict_results['CAy'].append(ca_coord[1])
                                    dict_results['CAz'].append(ca_coord[2])
                                    dict_results['resname'].append(res_name)
                                except KeyError:
                                    dict_results['CAx'] = [ca_coord[0]]
                                    dict_results['CAy'] = [ca_coord[1]]
                                    dict_results['CAz'] = [ca_coord[2]]
                                    dict_results['resname'] = [res_name]
                            except: 
                                flag_coord_exists = False
                    if not flag_coord_exists:
                        if 'CAx' in dict_results: del dict_results['CAx']
                        if 'CAy' in dict_results: del dict_results['CAy']
                        if 'CAz' in dict_results: del dict_results['CAz']
                
                    if pdb_df[0] == 'smoc':
                        dict_results['smoc'] = []
                        for val in pdb_df[1:]:
                            dict_results['smoc'].append(round(float(val),3))
                    elif pdb_df[0] == 'z_local':
                        dict_results['z_local'] = []
                        for val in pdb_df[1:]:
                            dict_results['z_local'].append(round(float(val),3))
                    elif pdb_df[0] == 'z_global':
                        dict_results['z_global'] = []
                        for val in pdb_df[1:]:
                            dict_results['z_global'].append(round(float(val),3))
                    #print dict_results
                    self.set_results_chain(dict_results,sco,process_name,chain,pdbid,mapid,mid)
                
        elif process_name == 'fdr':
            list_chains = df.Chain_name.unique()
            # Get data
            for chain in list_chains:
                #add chain sections
                if self.add_chain_sections:
                    if not chain in self.list_chains:
                        self.list_chains.append(chain)
                    chain_section = chain
                    chain_name = 'chain '+chain
                    self.add_section_chain(chain_section, chain_name)
                
                if chain in self.list_chains:
                    if not chain in self.residue_outliers[process_name][mapid][pdbid]:
                        self.residue_outliers[process_name][mapid][pdbid][chain] = {}
                        self.residue_z[process_name][mapid][pdbid][chain] = {}
                        self.residue_coordinates[mapid][pdbid][chain] = {}
                    if not chain in list_chain_model:
                        dict_results = {}
                        list_chain_model.append(chain)
                        
                df_chain = df.loc[df['Chain_name'] == chain]
                list_resnum = []
                list_scores = []
                dict_results = {}
                df_res = df_chain['residue_id'].tolist()
                df_score = df_chain['conf_score'].tolist()
                for l in xrange(len(df_chain['residue_id'])):
                    res = df_res[l]
                    sc = df_score[l]
                    try: 
                        resnum = int(res)
                    except ValueError: continue#skip insertion codes
                    list_resnum.append(resnum)
                    list_scores.append(float(sc))
                    try:
                        ca_coord = dict_pdb_ca_coord["1"][chain][str(resnum)][0]
                        res_name = dict_pdb_ca_coord["1"][chain][str(resnum)][1]
                        try:
                            dict_results['CAx'].append(ca_coord[0])
                            dict_results['CAy'].append(ca_coord[1])
                            dict_results['CAz'].append(ca_coord[2])
                            dict_results['resname'].append(res_name)
                        except KeyError:
                            dict_results['CAx'] = [ca_coord[0]]
                            dict_results['CAy'] = [ca_coord[1]]
                            dict_results['CAz'] = [ca_coord[2]]
                            dict_results['resname'] = [res_name]
                    except IOError: 
                        flag_coord_exists = False
                if not flag_coord_exists:
                    if 'CAx' in dict_results: del dict_results['CAx']
                    if 'CAy' in dict_results: del dict_results['CAy']
                    if 'CAz' in dict_results: del dict_results['CAz']
                        
                dict_results['residue'] = list_resnum
                dict_results[process_name] = list_scores
                
                self.set_results_chain(dict_results,sco,process_name,chain,pdbid,mapid,mid)
    #         pyrvapi.rvapi_set_plot_xmin('plot1', 'graphWidget1', 0.0)
    #         pyrvapi.rvapi_set_plot_ymin('plot1', 'graphWidget1', 0.0)
        
        pyrvapi.rvapi_flush()
        sco.close()
        #print self.dict_outlier_details
    def set_results_chain(self,dict_results,sco,process_name,chain,pdbid,mapid,mid):
        z_cutoff = -1.5
        if 'residue' in dict_results and process_name in dict_results:
            try:
                assert len(dict_results['residue']) == len(dict_results[process_name])
            except AssertionError:
                print pdbid
                print len(dict_results['residue']), len(dict_results[process_name])
                print dict_results['residue'], dict_results[process_name]
                
            if self.min_val == None: self.min_val = min(dict_results[process_name])
            else: self.min_val = min(self.min_val,min(dict_results[process_name]))
            added_smoc_text = True
            #store plot data for each pdb and map
            if not process_name in self.dict_pdbdata:
                self.dict_pdbdata[process_name] = {}
            if not chain in self.dict_pdbdata[process_name]: 
                self.dict_pdbdata[process_name][chain] = OrderedDict()
            if not pdbid in self.dict_pdbdata[process_name][chain]:
                self.dict_pdbdata[process_name][chain][pdbid] = OrderedDict()
            if not mapid in self.dict_pdbdata[process_name][chain][pdbid]:
                self.dict_pdbdata[process_name][chain][pdbid][mapid] = OrderedDict()

            self.dict_pdbdata[process_name][chain][pdbid][mapid] = [
                                        dict_results['residue'][:],
                                        dict_results[process_name][:]]
            if 'resname' in dict_results:
                if not chain in self.dict_outlier_details[mid]:
                    self.dict_outlier_details[mid][chain] = {}
                #save outliers in dict
                for l in xrange(len(dict_results['residue'])):
                    try:
                        self.dict_outlier_details[mid][chain][process_name].append([
                                                    str(dict_results['residue'][l]) +' '+
                                                    dict_results['resname'][l],
                                                    dict_results[process_name][l]])
                    except KeyError:
                        self.dict_outlier_details[mid][chain][process_name] = [[
                                                    str(dict_results['residue'][l]) +' '+
                                                    dict_results['resname'][l], 
                                                    dict_results[process_name][l]]]
            if not process_name in self.dict_mapdata:
                self.dict_mapdata[process_name] = {}
            if not chain in self.dict_mapdata[process_name]: 
                self.dict_mapdata[process_name][chain] = OrderedDict()
            if not mapid in self.dict_mapdata[process_name][chain]:
                self.dict_mapdata[process_name][chain][mapid] = OrderedDict()
            if not pdbid in self.dict_mapdata[process_name][chain][mapid]:
                self.dict_mapdata[process_name][chain][mapid][pdbid] = OrderedDict()
            
            self.dict_mapdata[process_name][chain][mapid][pdbid] = [
                                        dict_results['residue'][:],
                                        dict_results[process_name][:]]
            
            if 'resname' in dict_results and 'CAx' in dict_results and \
                'CAy' in dict_results and 'CAz' in dict_results:
                if process_name == 'fdr':
                    smoc_z, list_z_local, list_z_global = self.select_outliers_by_score(dict_results, process_name, z_cutoff)
                elif process_name == 'smoc':
                    if 'z_local' in dict_results:
                        smoc_z = np.array(dict_results['z_local']) < z_cutoff
                        list_z_local = dict_results['z_local']
                        list_z_global = dict_results['z_global']
                        #print 'z', chain, smoc_z, list_z_local
                    else:
                        smoc_z, list_z_local, list_z_global = self.select_outliers_by_z(dict_results, process_name, z_cutoff)
                #print len(dict_results['smoc']),len(list_z_local)
                #print smoc_z, list_z_local, list_z_global, dict_results[process_name]
                #print chain, smoc_z, list_z_local
                for l in xrange(len(dict_results['residue'])):
                    #write z scores
                    residue_scores = ','.join([
                        chain,str(dict_results['resname'][l])+
                        '_'+str(dict_results['residue'][l]),
                        "{0:.2f}".format(float(dict_results[process_name][l])),
                        "{0:.2f}".format(list_z_local[l]),
                        "{0:.2f}".format(list_z_global[l])
                                       ])
                    sco.write(residue_scores+"\n")
                    #print residue_scores, smoc_z[l]
                    if process_name == 'fdr':
                        self.residue_z[process_name][mapid][pdbid][chain][str(dict_results['residue'][l])] = float(dict_results[process_name][l])
                    else:
                        self.residue_z[process_name][mapid][pdbid][chain][str(dict_results['residue'][l])] = list_z_local[l]

                    if smoc_z[l]:
                        self.residue_outliers[process_name][mapid][pdbid][chain][str(dict_results['residue'][l])] = 'Outlier'
                        self.residue_coordinates[mapid][pdbid][chain][str(dict_results['residue'][l])] = \
                                                                        (dict_results['CAx'][l],
                                                                         dict_results['CAy'][l],
                                                                         dict_results['CAz'][l])
                        #print dict_results['residue'][l], dict_results[process_name][l]
                        #TODO: use only the full map (map0) for coot outliers
                        if mapid not in self.list_mapids or \
                            self.list_mapids.index(mapid) > 0: continue
                        try:
                            self.dict_cootdata[pdbid][process_name].append((chain,
                                                    dict_results['residue'][l],
                                                    dict_results['resname'][l],
                                                    dict_results[process_name][l],
                                                    (dict_results['CAx'][l],
                                                    dict_results['CAy'][l],
                                                    dict_results['CAz'][l])
                                                        ))
                        except IndexError, msg:
                            print 'Could not set coot data for', pdbid, chain, msg
                            self.dict_cootdata[pdbid][process_name].append((chain,
                                                    dict_results['residue'][l],
                                                    '',
                                                    dict_results[process_name][l],
                                                    ('',
                                                    '',
                                                    '')
                                                        ))

    
    def check_chain_outliers(self,chain,process_name):
        for mid in self.dict_outlier_details:
            if chain in self.dict_outlier_details[mid]:
                if process_name in self.dict_outlier_details[mid][chain]:
                    return True
        return False

    def check_chain_order_cootdata(self,process_name):
        #print 'Chains:', self.list_chains
        for pdbid in self.dict_cootdata:
            list_chain_sort = []
            for chain in self.list_chains:
                for resdata in self.dict_cootdata[pdbid][process_name]:
                    if resdata[0] in self.list_chains:
                        if resdata[0] == chain:
                            list_chain_sort.append(resdata)
                    else:
                        list_chain_sort.append(resdata)
            self.dict_cootdata[pdbid][process_name] = list_chain_sort[:]
            
    def set_results_graphs(self):
        x_label = 'Residue'
        y_label = 'Score'
        for chain in self.list_chains:
            section_name = 'scores'+chain
            
            
            self.add_subsection_to_chain(chain,section_name)
            #self.add_smoc_section_text(section_name)
            ct_graph = 0 # graph widgets
            
            num_score_types = len(self.dict_mapdata) #number of local scores
            #map -> widget, multiple pdb -> plots, processes -> lines
            for mapid in self.list_mapids:
                #Add a line with mapid
                pyrvapi.rvapi_add_text("<br>"+mapid+":<br>", section_name, ct_graph, 0, 1, 1)
                for process_name in self.dict_mapdata:
                    if not chain in self.dict_mapdata[process_name]: continue
                    #skip chain tab if outliers not found
                    if hasattr(self, 'dict_outlier_details'):
                        if len(self.dict_outlier_details) > 0 and \
                            not self.check_chain_outliers(chain,process_name): continue
                    if not mapid in self.dict_mapdata[process_name][chain]: continue
                    skip_map = False
                    #skip map plot if mult map vs single pdb
                    if len(self.dict_mapdata[process_name][chain][mapid]) == 1:
                        for pdbid in self.dict_pdbdata[process_name][chain]:
                            if mapid in self.dict_pdbdata[process_name][chain][pdbid] and \
                                    len(self.dict_pdbdata[process_name][chain][pdbid]) > 1: 
                                skip_map = True
                                break
                    if skip_map: continue
                #graph for each map
                graphwidget = 'graphWidget'+str(ct_graph)+section_name
                pyrvapi.rvapi_add_graph(graphwidget, section_name, ct_graph, 0, 1, 1)
                #pyrvapi.rvapi_add_loggraph(graphwidget, section, 0, 0, 1, 1)
                
                #pdbs -> plots, scores -> lines
                if num_score_types > 1:
                    ct_pdb = 0
                    if len(self.list_pdbids) > 1:
                        pyrvapi.rvapi_add_text("toggle models above to switch plots<br>", section_name, ct_graph+1, 0, 1, 1)
                    #plots for each pdb and multiple scores
                    for pdbid in self.list_pdbids:
                        ct_line = 0
                        data = 'smocdata1'+str(ct_pdb)
                        dataname = 'Local score'
                        pyrvapi.rvapi_add_graph_data1(graphwidget+'/'+data,
                                                    dataname)
                        plotname = pdbid
                        plot_id = 'plot'+str(ct_pdb)
                        ct_process = 0
                        for process_name in self.dict_mapdata:
                            if not chain in self.dict_mapdata[process_name]: continue
                            if not pdbid in self.dict_mapdata[process_name][chain][mapid]: continue
                            list_x, list_y = \
                                self.dict_mapdata[process_name][chain][mapid][pdbid]
                            plotlinename = process_name
                            if ct_process == 0:
                                add_plot_to_graph(graphwidget,data,plotname,
                                                  plotlinename,list_x,list_y,
                                              x_label, y_label,
                                              plot_id=plot_id,
                                              xtype='int',lnum=ct_line+1,
                                              marker_style=pyrvapi.RVAPI_MARKER_Off)
                            else:
                                line_color = getattr(pyrvapi, 
                                                     'RVAPI_COLOR_'+list_line_colors[ct_line-1])
                                add_line_to_plot(graphwidget,data,plotlinename,list_y,
                                                 list_x=list_x,x_label=x_label,
                                  ytype='float',
                                  plot_id=plot_id,
                                  lnum=ct_line+1,
                                  line_color=line_color,
                                  marker_style=pyrvapi.RVAPI_MARKER_Off)
                            
                            list_potential_outliers = []
                            list_sel_resnum = []
                            list_sel_scores = []
#                             for res in self.residue_z[process_name][mapid][pdbid][chain]:
#                                 z_outlier = self.residue_z[process_name][mapid][pdbid][chain][res]
#                                 if process_name == 'fdr':
#                                     if z_outlier < 0.95 and z_outlier >= 0.9:
#                                         list_potential_outliers.append(res)
#                                 else:
#                                     if z_outlier <= -1.0 and z_outlier >= -1.5:
#                                         list_potential_outliers.append(res)
#                                         
#                             for s in xrange(len(list_potential_outliers)):
#                                 try: 
#                                     list_sel_resnum.append(int(list_potential_outliers[s]))
#                                 except (ValueError,TypeError) as exc:
#                                     pass
#                             list_sel_resnum.sort()
#                             for s in list_sel_resnum:
#                                 list_sel_scores.append(list_y[list_x.index(s)])
#                                 
#                             if len(list_sel_resnum) > 0:
#                                 ct_line+=1
#                                 line_color = getattr(pyrvapi, 
#                                                  'RVAPI_COLOR_'+list_line_colors[ct_line*-1])
#                                 if process_name == 'fdr':
#                                     plotlinename = pdbid+'_'+process_name+": disfavored (0.9<=score<0.95)"
# #                                     line_color = getattr(pyrvapi, 
# #                                                  'RVAPI_COLOR_Lime')
#                                 else: 
#                                     plotlinename = pdbid+'_'+process_name+": disfavored (-1.5<=z<-1.0)"
# #                                     line_color = getattr(pyrvapi, 
# #                                                  'RVAPI_COLOR_Orange')
#                                 add_line_to_plot(graphwidget,data,plotlinename,list_sel_scores,
#                                                          list_x=list_sel_resnum,x_label=x_label,
#                                           ytype='float',
#                                           plot_id=plot_id,
#                                           lnum=ct_line+1,
#                                           line_color=line_color,
#                                           line_show=pyrvapi.RVAPI_LINE_Off)
                            
                            list_sel_resnum = []
                            list_sel_scores = []
                            list_outlier_residues = self.residue_outliers[process_name][mapid][pdbid][chain].keys()
                                
                            #print list_outlier_residues
                            for s in xrange(len(list_outlier_residues)):
                                try: 
                                    list_sel_resnum.append(int(list_outlier_residues[s]))
                                except (ValueError,TypeError) as exc:
                                    pass
                            list_sel_resnum.sort()
                            for s in list_sel_resnum:
                                list_sel_scores.append(list_y[list_x.index(s)])
                                
                            if len(list_sel_resnum) > 0:
                                ct_line+=1
                                line_color = getattr(pyrvapi, 
                                                 'RVAPI_COLOR_'+list_line_colors[ct_line*-1])
                                if process_name == 'fdr':
                                    plotlinename = process_name+': outliers (score < 0.9)'
                                else: 
                                    plotlinename = process_name+': outliers (z < -1.5)'
                                add_line_to_plot(graphwidget,data,plotlinename,list_sel_scores,
                                                         list_x=list_sel_resnum,x_label=x_label,
                                          ytype='float',
                                          plot_id=plot_id,
                                          lnum=ct_line+1,
                                          line_color=line_color,
                                          line_show=pyrvapi.RVAPI_LINE_Off)
                                                        
                            ct_process += 1
                            ct_line += 1
                        ct_pdb += 1
                #only one score, map -> plot, pdb -> lines
                elif num_score_types == 1:
                    plotname = mapid
                    data = 'smocdata2'+str(ct_graph)
                    dataname = 'Local score'
                    pyrvapi.rvapi_add_graph_data1(graphwidget+'/'+data,
                                                dataname)
                    plot_id = 'plot'+str(ct_graph)
                    ct_pdb = 0
                    ct_line = 0
                    #plot for each map , multiple pdb
                    for pdbid in self.list_pdbids:
                        process_name = self.dict_mapdata.keys()[0]
                        try:
                            if not pdbid in self.dict_mapdata[process_name][chain][mapid]: continue
                        except KeyError: continue
                        list_x, list_y = \
                            self.dict_mapdata[process_name][chain][mapid][pdbid]
                        plotlinename = pdbid+'_'+process_name
                        if ct_pdb == 0:
                            #print chain, plot_id, pdbid, plotlinename, ct_line+1
                            add_plot_to_graph(graphwidget,data,plotname,
                                              plotlinename,list_x,list_y,
                                          x_label, y_label,
                                          plot_id=plot_id,
                                          xtype='int',lnum=ct_line+1,
                                          marker_style=pyrvapi.RVAPI_MARKER_Off)
                        else:
                            #print chain, ct_line, plot_id, pdbid, plotlinename, ct_line+1
                            line_color = getattr(pyrvapi, 
                                                 'RVAPI_COLOR_'+list_line_colors[ct_line])
                            add_line_to_plot(graphwidget,data,plotlinename,list_y,
                                             list_x=list_x,x_label=x_label,
                              ytype='float',
                              plot_id=plot_id,
                              lnum=ct_line+1,
                              line_color=line_color,
                              marker_style=pyrvapi.RVAPI_MARKER_Off)
                        
                        list_potential_outliers = []
                        list_sel_resnum = []
                        list_sel_scores = []
#                         for res in self.residue_z[process_name][mapid][pdbid][chain]:
#                             z_outlier = self.residue_z[process_name][mapid][pdbid][chain][res]
#                             #print res, z_outlier
#                             if process_name == 'fdr':
#                                 if z_outlier < 0.95 and z_outlier >= 0.9:
#                                     list_potential_outliers.append(res)
#                             else:
#                                 if z_outlier < -1.0 and z_outlier >= -1.5:
#                                     list_potential_outliers.append(res)
#                         #print chain, list_potential_outliers
#                         for s in xrange(len(list_potential_outliers)):
#                             try: 
#                                 list_sel_resnum.append(int(list_potential_outliers[s]))
#                             except (ValueError,TypeError) as exc:
#                                 pass
#                         list_sel_resnum.sort()
#                         for s in list_sel_resnum:
#                             list_sel_scores.append(list_y[list_x.index(s)])
#                         #print list_sel_resnum, list_sel_scores
#                         if len(list_sel_resnum) > 0:
#                             ct_line+=1
#                             line_color = getattr(pyrvapi, 
#                                                  'RVAPI_COLOR_'+list_line_colors[ct_line*-1])
#                             if process_name == 'fdr':
#                                 plotlinename = pdbid+'_'+process_name+": disfavored (0.9<=score<0.95)"
# #                                 line_color = getattr(pyrvapi, 
# #                                              'RVAPI_COLOR_Lime')
#                             else: 
#                                 plotlinename = pdbid+'_'+process_name+": disfavored (-1.5<=z<-1.0)"
# #                                 line_color = getattr(pyrvapi, 
# #                                              'RVAPI_COLOR_Orange')
#                             add_line_to_plot(graphwidget,data,plotlinename,list_sel_scores,
#                                                      list_x=list_sel_resnum,x_label=x_label,
#                                       ytype='float',
#                                       plot_id=plot_id,
#                                       lnum=ct_line+1,
#                                       line_color=line_color,
#                                       line_show=pyrvapi.RVAPI_LINE_Off)
                        
                        list_sel_resnum = []
                        list_sel_scores = []
                        list_outlier_residues = self.residue_outliers[process_name][mapid][pdbid][chain].keys()
                        #print list_x, list_y
                        #print list_outlier_residues
                        for s in xrange(len(list_outlier_residues)):
                            try: 
                                list_sel_resnum.append(int(list_outlier_residues[s]))
                            except (ValueError,TypeError) as exc:
                                pass
                        list_sel_resnum.sort()
                        for s in list_sel_resnum:
                            list_sel_scores.append(list_y[list_x.index(s)])
                        #print list_sel_resnum, list_sel_scores
                        if len(list_sel_resnum) > 0:
                            ct_line+=1
                             
                            line_color = getattr(pyrvapi, 
                                                 'RVAPI_COLOR_'+list_line_colors[ct_line*-1])
                            plotlinename = pdbid+'_'+process_name+': outliers'
                            #print chain, plot_id, pdbid, plotlinename, ct_line+1
                            add_line_to_plot(graphwidget,data,plotlinename,list_sel_scores,
                                                     list_x=list_sel_resnum,x_label=x_label,
                                      ytype='float',
                                      plot_id=plot_id,
                                      lnum=ct_line+1,
                                      line_color=line_color,
                                      line_show=pyrvapi.RVAPI_LINE_Off)
                        
                        ct_pdb += 1
                        ct_line += 1

                    pyrvapi.rvapi_flush()
                ct_graph += 1
            ct_graph += 1#break between plot types
            pyrvapi.rvapi_add_text("<br><br><br>", section_name, ct_graph, 0, 1, 1)
            ct_graph += 1
            #pdb -> graph, plot -> processes, map-> lines
            for pdbid in self.list_pdbids:
                #pyrvapi.rvapi_add_loggraph(graphwidget, section, 0, 0, 1, 1)
                
                ct_process = 0 #plots
                for process_name in self.dict_pdbdata:
                    if not chain in self.dict_pdbdata[process_name]: continue
                    if not pdbid in self.dict_pdbdata[process_name][chain]: continue
                    #skip pdb plot if single map vs multiple pdbs
                    if len(self.dict_pdbdata[process_name][chain][pdbid]) == 1: continue
                    
                    if ct_process == 0:
                        pyrvapi.rvapi_add_text("<br>"+pdbid+":<br>", section_name, ct_graph, 0, 1, 1)
                        graphwidget = 'graphWidget'+str(ct_graph)+section_name
                        pyrvapi.rvapi_add_graph(graphwidget, section_name, ct_graph, 0, 1, 1)
                        if len(self.dict_pdbdata) > 1:
                            pyrvapi.rvapi_add_text("toggle scores above to switch plots<br>", section_name, ct_graph+1, 0, 1, 1)

                    data = 'smocdata'+str(ct_process)
                    dataname = 'Local score'
                    pyrvapi.rvapi_add_graph_data1(graphwidget+'/'+data,
                                                    dataname)
                    plotname = process_name
                    plot_id = 'plot'+str(ct_process)
                    ct_map = 0
                    ct_line = 0
                    for mapid in self.list_mapids:
                        if not mapid in self.dict_pdbdata[process_name][chain][pdbid]:
                            continue
                        list_x, list_y = \
                            self.dict_pdbdata[process_name][chain][pdbid][mapid]
                        #print pdbid, mapid, chain, plot_id, list_x, list_y, process_name, ct_line
                        plotlinename = mapid
                        if ct_map == 0:
                            add_plot_to_graph(graphwidget,data,plotname,
                                              plotlinename,list_x,list_y,
                                              x_label, y_label,
                                              plot_id=plot_id,
                                              xtype='int',lnum=ct_line+1,
                                              marker_style=pyrvapi.RVAPI_MARKER_Off)
                        else:
                            line_color = getattr(pyrvapi, 'RVAPI_COLOR_'+list_line_colors[ct_map-1])
                            add_line_to_plot(graphwidget,data,plotlinename,list_y,
                                             list_x=list_x,x_label=x_label,
                              ytype='float',
                              plot_id=plot_id,
                              lnum=ct_line+1,
                              line_color=line_color,
                              marker_style=pyrvapi.RVAPI_MARKER_Off)
                        ct_map += 1
                        ct_line += 1
                        pyrvapi.rvapi_flush()
                    ct_process += 1
                ct_graph += 1

    def select_outliers_by_score(self,dict_results, process_name, z_cutoff = -2.5):
        list_smoc = dict_results[process_name]
        array_smoc = np.array(list_smoc)
        list_z_global = self.calc_z_median(list_smoc)
        #print len(list_smoc),len(list_z_global)
        #if coords are available, set z score locally
        if 'CAx' in dict_results and 'CAy' in dict_results and \
            'CAz' in dict_results:
            indi = zip(dict_results['CAx'], dict_results['CAy'], dict_results['CAz'])
            try: 
                from scipy.spatial import cKDTree
                gridtree = cKDTree(indi)
            except ImportError:
                try:
                    from scipy.spatial import KDTree
                    gridtree = KDTree(indi)
                except ImportError: 
                    gridtree = None
            list_z_local = []
            if gridtree is not None:
                list_z = []
                for l in xrange(len(list_smoc)):
                    ca_coord = indi[l]
                    val = list_smoc[l]
                    list_neigh = self.get_indices_sphere(gridtree,ca_coord,dist=10.0)
                    list_smoc_local = array_smoc[list_neigh]
                    z_local = self.calc_z_median(list_smoc_local,val)
                    #res_index = list_neigh.index(l)
                    list_z_local.append(z_local)
                    #list_z.append(z_local < z_cutoff)
                
        else:
            list_z_local = [0.]*len(list_z_global) #when no local z is calculated
            z = self.calc_z_median(array_smoc)
            #list_z = z < z_cutoff
        if process_name == 'fdr': list_z = array_smoc < 0.9
        #print len(list_z),len(list_z_local),len(list_z_global)
        return list_z, list_z_local, list_z_global

    def select_outliers_by_z(self,dict_results, process_name, z_cutoff = -2.5):
        list_smoc = dict_results[process_name]
        array_smoc = np.array(list_smoc)
        #use z = 0 for a very narrow distribution
        if np.median(list_smoc) > 0.6 and np.std(list_smoc) < 0.05:
            list_z_global = [0.0]*len(list_smoc)
            #print list_z_global
        else:
            list_z_global = self.calc_z_median(list_smoc)
        #print len(list_smoc),len(list_z_global)
        #if coords are available, set z score locally
        if 'CAx' in dict_results and 'CAy' in dict_results and \
            'CAz' in dict_results:
            indi = zip(dict_results['CAx'], dict_results['CAy'], dict_results['CAz'])
            try: 
                from scipy.spatial import cKDTree
                gridtree = cKDTree(indi)
            except ImportError:
                try:
                    from scipy.spatial import KDTree
                    gridtree = KDTree(indi)
                except ImportError: 
                    gridtree = None
            list_z_local = []
            if gridtree is not None:
                list_z = []
                for l in xrange(len(list_smoc)):
                    ca_coord = indi[l]
                    val = list_smoc[l]
                    list_neigh = self.get_indices_sphere(gridtree,ca_coord,dist=12.0)
                    list_smoc_local = array_smoc[list_neigh]
                    if np.median(list_smoc_local) > 0.6 and np.std(list_smoc_local) < 0.05:
                        z_local = 0.0
                    else:
                        z_local = self.calc_z_median(list_smoc_local,val)
                    #res_index = list_neigh.index(l)
                    list_z_local.append(z_local)
                    list_z.append(z_local < z_cutoff)
                
        else:
            list_z_local = [0.]*len(list_z_global) #when no local z is calculated
            z = self.calc_z_median(array_smoc)
            list_z = z < z_cutoff
        #print len(list_z),len(list_z_local),len(list_z_global)
        #print list_z, list_z_local, list_z_global
        return list_z, list_z_local, list_z_global    
    
    def calc_z(self,list_vals,val=None):
        if val is not None:
            try:
                list_vals.remove(val) #remove val from list
            except: pass
        sum_mapval = np.sum(list_vals)
        mean_mapval = sum_mapval/len(list_vals)
        #median_mapval = np.median(list_vals)
        std_mapval = np.std(list_vals)
        if val is None:
            if std_mapval == 0.:
                z = [0.0]*len(list_vals)
            else:
                z = np.round((np.array(list_vals) - mean_mapval)/std_mapval,2)
        else:
            if std_mapval == 0.:
                z = 0.0
            else:
                z = (val - mean_mapval)/std_mapval
        return z
     
    def calc_z_median(self,list_vals,val=None):
        if val is not None:
            try:
                list_vals.remove(val)
            except: pass
        sum_smoc = np.sum(list_vals)
        median_smoc = np.median(list_vals)
        mad_smoc = np.median(np.absolute(np.array(list_vals)-median_smoc))
        if val is None:
            if mad_smoc == 0.:
                z = [0.0]*len(list_vals)
            else:
                z = np.around((np.array(list_vals) - median_smoc)/mad_smoc,2)
        else:
            if mad_smoc == 0.:
                z = 0.0
            else:
                z = round(((val - median_smoc)/mad_smoc),2)
        return z    

    def get_indices_sphere(self,gridtree,coord,dist=5.0):
        list_points = gridtree.query_ball_point(\
                    [coord[0],coord[1],coord[2]], 
                    dist)
        return list_points
    
    def add_smoc_section_text(self,section_name):
        pyrvapi.rvapi_add_text(
                        '<br><br>',
                        section_name, 0, 0, 1, 1)
        pyrvapi.rvapi_add_text(
            'Local fragment Score based on Manders\' Overlap Coefficient (SMOC)',
                        section_name, 0, 0, 1, 1)
        pyrvapi.rvapi_flush()
    
    def add_fdr_section_text(self,section_name):
        pyrvapi.rvapi_add_text(
                        '<br><br>',
                        section_name, 0, 0, 1, 1)
        pyrvapi.rvapi_add_text(
            'False Discovery Rate based per-residue scores',
                        section_name, 0, 0, 1, 1)
        pyrvapi.rvapi_flush()
        
    def set_smoc_graph(self, section, graphwidget, data, dataname,
                       list_x,list_y,x_label, y_label, plotname,
                       plotlinename):
        pyrvapi.rvapi_add_graph(graphwidget, section, 0, 0, 1, 1)
        #pyrvapi.rvapi_add_loggraph(graphwidget, section, 0, 0, 1, 1)
        pyrvapi.rvapi_add_graph_data1(graphwidget+'/'+data,
                                                dataname)
        add_plot_to_graph(graphwidget,data,plotname,plotlinename,list_x,list_y,
                          x_label, y_label,xtype='int')
        
#     def add_outlier_title_for_residues(self, outlier_title, chain, residue_num):
#         try: 
#             if not outlier_title in self.residue_outliers[chain][residue_num]:
#                 self.residue_outliers[chain][residue_num].append(outlier_title)
#         except KeyError:
#             self.residue_outliers[chain][residue_num] = [outlier_title]


class SetSCCCResults():
    def __init__(self, sccc_process):
        self.sccc_process = sccc_process
        self.sccc_results = os.path.join(sccc_process.location,'sccc_score.txt')
        if os.path.isfile(self.sccc_results):
            self.sccc_dataframe = pd.read_csv(self.sccc_results)
            
    def set_results_summary(self,local_tab, sccc_section):
        pyrvapi.rvapi_add_section(sccc_section, 'Local SCCC scores', 'tab2', 0, 0, 1, 1,
                                  True)
        pyrvapi.rvapi_add_text(
            'Rigid Body Segment Cross Correlation Coefficient (SCCC)',
            sccc_section, 0, 0, 1, 1)

        pyrvapi.rvapi_flush()
    
    def set_results_tables(self, sccc_section):
        build_table = sccc_section+'/table1'
        pyrvapi.rvapi_add_table1(build_table,'SCCC scores', 1, 0, 1, 1, False)
        pyrvapi.rvapi_put_horz_theader('table1','Rigid Bodies','',0)
        ct_pdbs = 1
        for col in self.sccc_dataframe.columns:
            pyrvapi.rvapi_put_horz_theader('table1',col,'',ct_pdbs)
            ct_pdbs += 1
        for i in range(len(self.sccc_dataframe.index)):
            index = self.sccc_dataframe.index[i]
            pyrvapi.rvapi_put_table_string ( 'table1',index,i,0 )
            for j in range(1,ct_pdbs):
                col = self.sccc_dataframe.columns[j-1]
                #print self.sccc_dataframe[col][index]
                try: pyrvapi.rvapi_put_table_string ( 'table1','%0.2f' % self.sccc_dataframe[col][index],i,j )
                except TypeError: sys.exit('SCCC calculation failed')
        pyrvapi.rvapi_flush()
