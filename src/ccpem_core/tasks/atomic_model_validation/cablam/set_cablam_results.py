import sys
import re
import os
import time
import pyrvapi
import fileinput
from collections import OrderedDict
import shutil
import numpy as np
import pandas as pd
import math
from cablam_log_parser import CaBLAM_LogParser
from ccpem_core.tasks.atomic_model_validation.pyrvapi_utils import *
import json

class SetCaBLAMResults():
    def __init__(self,cablam_process,list_chains=[],
                 dssp_dict=None, cootset_instance=None,
                 glob_reportfile=None):
        self.cablam_process = cablam_process
        self.cootset = cootset_instance
        self.list_chains = list_chains
        self.dssp_dict = dssp_dict
        self.add_chain_sections = False
        #outlier residue summary
        self.residue_outliers = OrderedDict()
        self.residue_coordinates = OrderedDict()
        #outlier details
        self.dict_outlier_details = OrderedDict()
        #log parse instances
        self.dict_logparse = OrderedDict()
        #outlier data for coot
        self.dict_cootdata = OrderedDict()
        #output json files for each model
        self.dict_jsonfiles = OrderedDict()
        self.list_modelids = []
        for cablam_job in cablam_process:
            modelid = cablam_job.name.split(' ')[-1]
            job_location = cablam_job.location
            self.list_modelids.append(modelid)
            cm = CaBLAM_LogParser(cablam_job.stdout)
            self.dict_logparse[modelid] = cm
            #self.residue_outliers[modelid] = {}
            
            if len(list_chains) == 0:
                for ch in cm.list_chains:
                    if not ch in self.list_chains:
                        self.list_chains.append(ch)
                self.add_chain_sections = True
            self.dict_jsonfiles[modelid] = os.path.join(job_location,
                                                        modelid+"_cablam.json")
        
        self.glob_reportfile = glob_reportfile
        if self.glob_reportfile is not None:
            self.glob_reportfile.write('*CaBLAM* (backbone geometry)\n')
            self.glob_reportfile.write('============================\n')
        self.set_global_results()
        
        if self.glob_reportfile is not None: 
            self.glob_reportfile.write('----------------------------\n\n')
        self.set_local_data()
        self.save_outlier_json()
        self.add_cootdata()
        self.delete_cootdata()
        self.set_local_results()
        
    def add_cootdata(self):
        for mid in self.dict_cootdata:
            if self.cootset is not None:
                self.cootset.add_data_to_cootfile(self.dict_cootdata[mid],
                    modelid=mid, method='cablam')
                
    def set_global_results(self):
        tab_id = 'global_tab'
        section_id = 'cablam_global_sec'
        pyrvapi.rvapi_add_section(section_id, 'CaBLAM summary', tab_id, 
                                  0, 0, 1, 1, False)
        self.set_summary_table(tab_id,section_id,"cablam_summary")

    def set_summary_table(self, tab, section, table_id):
        add_table_to_section(section,table_id)
        gs = global_summary_settings()
        add_vert_headers_to_table(table_id,gs.list_vert_headers,
                                  gs.list_vert_header_tooltips)
        #expand vert header for all models
        list_horz_headers, list_horz_header_tooltips = \
            self.expand_table_header_models(gs.list_horz_headers, 
                                            gs.list_horz_header_tooltips)
        add_horz_headers_to_table(table_id,list_horz_headers,
                                  list_horz_header_tooltips)
        ct_model = 0
        for mid in self.list_modelids:
            if self.glob_reportfile is not None:
                self.glob_reportfile.write('>{}\n'.format(mid))
            dict_summary = self.dict_logparse[mid].dict_summary
            data_array = [[dict_summary[gs.list_vert_headers[l]]] \
                          for l in xrange(len(gs.list_vert_headers))]
            add_data_to_table(table_id,data_array, start_col=ct_model)
            for l in xrange(len(gs.list_vert_headers)):
                if self.glob_reportfile is not None:
                    self.glob_reportfile.write('-{}: {}%\n'.format(gs.list_vert_headers[l],
                                                         data_array[l][0]))
            ct_model += 1
        pyrvapi.rvapi_flush()

    def expand_table_header_models(self, list_headers, list_header_tooltips):
        list_expanded_headers = []
        list_expanded_header_tips = []
        for mid in self.list_modelids:
            ct_h = 0
            for h in xrange(len(list_headers)):
                header = list_headers[h]
                header += "<br>({})".format(mid)
                list_expanded_headers.append(header)
                list_expanded_header_tips.append(list_header_tooltips[h])
        return list_expanded_headers, list_expanded_header_tips

    def set_local_data(self):
        '''
        get outlier data from log parser and store in a dictionary/json
        '''
        outlier_settings = residue_outlier_settings()
        for chain in self.list_chains:
            #check dssp
            dssp_chain_flag = False
            if self.dssp_dict != None:
                for mid in self.dssp_dict:
                    if mid in self.list_modelids:
                        if chain in self.dssp_dict[mid]:
                            dssp_chain_flag=True
                            break
            self.set_outlier_data(chain,outlier_settings,dssp_chain_flag)
    
    def set_outlier_data(self,chain, outlier_settings,dssp_chain_flag):
        list_measures = self.get_measures_chain(chain)
        
        for measure in list_measures:
            for mid in self.list_modelids:
                if 'Disfavored' not in measure:
#                     if not 'nomap' in self.residue_outliers:
#                         self.residue_outliers['nomap'] = {}
                    if not mid in self.residue_outliers:
                        self.residue_outliers[mid] = {}
                        self.residue_coordinates[mid] = {}
                    if not chain in self.residue_outliers[mid]:
                        #save outlier residues
                        self.residue_outliers[mid][chain] = {}
                        self.residue_coordinates[mid][chain] = {}
                lp = self.dict_logparse[mid]
                if not chain in lp.dict_outliers: continue
                dict_outlier_data = lp.dict_outliers[chain]
                if not measure in dict_outlier_data: continue
                self.save_outlier_data(dict_outlier_data, measure, 
                                                 chain,mid, dssp_chain_flag,
                                                 outlier_settings)
    
    def save_outlier_data(self, dict_outlier_data, measure,
                                chain,mid, dssp_chain_flag,
                                    outlier_settings):
        ct_res = 0
        list_data = []
        try: 
            dict_data = dict_outlier_data[measure] #log based outliers
        except KeyError: return
        
        for loutlier in dict_data:
            #7_ILE,sec_str_proposed,alpha_score,beta_score, threeten_score
            outlier = []
            ct_val = 0
            for val in loutlier:
                #round floats
                roundedval = val
                if isinstance(val,float):
                    roundedval = "{:.3f}".format(round(val,3))
                #save residue_outliers
                if ct_val == 0: 
                    #7 ILE
                    val_split = val.split(' ')
                    #save outlier residue and outlier type
                    if 'Disfavored' not in measure:
                        self.residue_outliers[mid][chain][str(val_split[0])] = \
                                    outlier_settings.data_titles[measure] 
                outlier.append(roundedval)
                ct_val += 1

            if len(outlier) == 0: continue
            
            ct_res += 1
            #outlier details
            residue_details = outlier
            if len(residue_details) == 0: continue
            residue_id = residue_details[0]
            residue_number = residue_id.split()[0]
            try: residue_name = residue_id.split()[1]
            except IndexError:
                residue_name = ' '
            #add dssp column
            CAx=CAy=CAz = ''
            dssp_sse = ' '
            if dssp_chain_flag:
                try:
                    dssp_sse = self.dssp_dict[mid][chain][residue_number][0]+\
                                '<br>'+\
                                self.dssp_dict[mid][chain][residue_number][2]

                    CAx,CAy,CAz = [float(val) for val in \
                                    self.dssp_dict[mid][chain][residue_number][-3:] ]
                    if 'Disfavored' not in measure:
                        self.residue_coordinates[mid][chain][str(val_split[0])] = \
                                    (CAx,CAy,CAz)
                except KeyError:
                    pass
                residue_details.append(dssp_sse)
                
            #residue_details[1] = residue_details[1].strip()
            #add recommendation
            if len(residue_details[1]) == 0:
                if 'CA Geom' in measure:
                    residue_details[1] = 'check CA trace'
                else:
                    residue_details[1] = '''check CA trace,carbonyls, peptide'''
            #7 ILE,sec_str_proposed,alpha_score,beta_score, threeten_score, dssp
            list_data.append(residue_details)
            if self.cootset is not None:
                if 'utlier' in measure:
#                     if not 'CA Geom' in measure:
#                         if len(residue_details[1]) == 0:
#                             residue_details[1] = '''check CA trace,carbonyls, peptide'''
#                     else:
#                         residue_details[1] = 'check CA trace'
                    if '<br>' in dssp_sse:
                        dssp_sse_split = dssp_sse.split('<br>')
                        dssp_sse = '\n'.join(dssp_sse_split)
                    coot_data_res = chain,residue_number,residue_name,residue_details[1],\
                                dssp_sse,(CAx,CAy,CAz)
                    try: 
                        self.dict_cootdata[mid]['cablam'].append((coot_data_res))
                    except KeyError:
                        self.dict_cootdata[mid] = {'cablam':[(coot_data_res)]}
        #set dict of outlier details
        if len(list_data) == 0: return
        if not mid in self.dict_outlier_details:
            self.dict_outlier_details[mid] = {}
        if not chain in self.dict_outlier_details[mid]:
            self.dict_outlier_details[mid][chain] = {}
        self.dict_outlier_details[mid][chain][measure] = list_data[:]

    def save_outlier_json(self):
        for mid in self.dict_outlier_details:
            try:
                jsonfile = self.dict_jsonfiles[mid]
            except KeyError: continue
            with open(jsonfile,'w') as jf:
                json.dump(self.dict_outlier_details[mid],jf)
                
    def delete_cootdata(self):
        self.dict_cootdata.clear()
        del self.dict_cootdata
    
    def delete_dictresults(self):
        self.dict_outlier_details.clear()
        del self.dict_outlier_details
    
    def set_local_results(self):
        '''
        Set local sections and tables
        '''
        #local_tab = 'local_tab'
        #local_tab_name = "Local Results"
        outlier_settings = residue_outlier_settings()
        count_chain = 0
        #print self.list_chains
        for chain in self.list_chains:
            chain_section = chain
            #check dssp
            dssp_chain_flag = False
            if self.dssp_dict != None:
                for mid in self.dssp_dict:
                    if mid in self.list_modelids:
                        if chain in self.dssp_dict[mid]:
                            dssp_chain_flag=True
                            break
            #skip chain tab if outliers not found
            if hasattr(self, 'dict_outlier_details'):
                if len(self.dict_outlier_details) > 0 and \
                    not self.check_chain_outliers(chain): continue
            chain_name = 'chain '+chain
            if self.add_chain_sections:
                pyrvapi.rvapi_add_section(chain_section, chain_name, 
                                          local_tab, 0, 0, 1, 1, False)
            
            self.set_outlier_sections_tables(chain_section,
                                                 outlier_settings,
                                                 dssp_chain_flag)
                
            count_chain += 1
            pyrvapi.rvapi_flush()
        self.delete_dictresults()

    def check_chain_outliers(self,chain):
        for mid in self.dict_outlier_details:
            if chain in self.dict_outlier_details[mid]:
                return True
        return False

    def set_outlier_sections_tables(self,chain,outlier_settings,
                                    dssp_chain_flag):
        list_measures = self.get_measures_chain(chain)
        for measure in outlier_settings.data_keys:
            if not measure in list_measures: continue
            #print measure
            outlier_subsection = \
                    "CaBLAM"+''.join(measure.split())+str(chain)
            
            if measure in outlier_settings.data_titles:
                outlier_subsection_name = "CaBLAM: "+ \
                                    outlier_settings.data_titles[measure]
                add_subsection(chain,outlier_subsection,
                               outlier_subsection_name)
#                 add_text_to_section(outlier_subsection,
#                                     [outlier_settings.data_info[measure]])
                pyrvapi.rvapi_flush()
                #add separate tables for each model
                for mid in self.list_modelids:
                     
                    table_id = ''.join(outlier_settings.data_titles[measure].split()) + \
                                str(chain)+mid
                    table_name = mid #+ ':' +outlier_settings.data_titles[measure]
                    add_table_to_section(outlier_subsection,table_id, table_name)
                 
                    list_horz_headers = outlier_settings.data_headers[measure][:]
                    list_horz_headers_tips = outlier_settings.data_tooltips[measure][:]
 
                    if dssp_chain_flag:
                        #add dssp column
                        list_horz_headers.append('DSSP')
                        list_horz_headers_tips.append(
                            'Current sec str by DSSP (and for 2 seq neighbors)') #: ({})'.format(
                                #outlier_settings.data_info[measure]))
                     
                    add_horz_headers_to_table(table_id,list_horz_headers,list_horz_headers_tips)
                    
                    self.add_outlier_data_to_table(table_id,chain,mid,measure)
        pyrvapi.rvapi_flush()


    def get_measures_chain(self,chain):
        '''
        Return outlier types for given chain
        '''
        list_measures = []
        for mid in self.list_modelids:
            lp = self.dict_logparse[mid]
            dict_outlier = lp.dict_outliers
            if chain in dict_outlier:
                for measure in dict_outlier[chain]:
                    if not measure in list_measures:
                        list_measures.append(measure)
        return list_measures
    
    def add_outlier_data_to_table(self,table_id,chain,mid,measure):
        if mid in self.dict_outlier_details:
            if chain in self.dict_outlier_details[mid]:
                if measure in self.dict_outlier_details[mid][chain]:
                    list_data = self.dict_outlier_details[mid][chain][measure]
                    add_data_to_table(table_id,list_data)
        
    def add_local_tabs_chains(self,list_chains):
        for chain in list_chains:
            pyrvapi.rvapi_add_tab(chain, "chain: "+chain, 
                                  True)

class global_summary_settings() :
    data_keys = ['CaBLAM Outlier',
                     'CaBLAM Disfavored',
                     'CA Geom Outlier']
    list_horz_headers = ['Percent']
    list_horz_header_tooltips = ['Outlier counts']
    list_vert_headers = data_keys
    list_vert_header_tooltips = [' ',' ',' ']
    
class residue_outlier_settings() :
    #NOTE: data_keys has same keywords as in log file
    #change this with updates
    data_keys = ['CaBLAM Outlier',
                     'CaBLAM Disfavored',
                     'CA Geom Outlier']
    data_titles = {'CaBLAM Outlier':'Outlier',
                     'CaBLAM Disfavored':'Disfavored',
                     'CA Geom Outlier':'CA Geom Outlier'}
    data_info = {'CaBLAM Outlier': 
                                "DSSP codes: <br>'H': helix, 'E':strand, 'T':turn,\
                                'B': beta bridge, 'G': helix-3, 'I': helix-5,\
                                 'S':bend",
                     'CaBLAM Disfavored':
                                        '''DSSP codes: <br>'H': helix, 'E':strand, 'T':turn, 
                                'B': beta bridge, 'G': helix-3, 'I': helix-5,
                                 'S':bend''',
                     'CA Geom Outlier':'''DSSP codes: <br>'H': helix, 'E':strand, 'T':turn, 
                                'B': beta bridge, 'G': helix-3, 'I': helix-5,
                                 'S':bend'''}
    data_headers = {
                    'CaBLAM Outlier':["residue","recommendation",
                                      "alpha score","beta score","three-ten score"],
                     'CaBLAM Disfavored':["residue","recommendation",
                                      "alpha score","beta score","three-ten score"],
                     'CA Geom Outlier':["residue","recommendation",
                                      "alpha score","beta score","three-ten score"]}
    data_tooltips = {
                    'CaBLAM Outlier':["residue","Recommended secondary structure or checks",
                                      "alpha score","beta score","three-ten score"],
                     'CaBLAM Disfavored':["residue","Recommended secondary structure or checks",
                                      "alpha score","beta score","three-ten score"],
                     'CA Geom Outlier':["residue","Recommended secondary structure or checks",
                                      "alpha score","beta score","three-ten score"]
                  }