import sys
import re
import os
from collections import OrderedDict
import json

list_keywords = ['CaBLAM Outlier',
                     'CaBLAM Disfavored',
                     'CA Geom Outlier']

def fix_chainid(chain):
    if chain in ['',None]:
        chain = '_'
    return chain

class CaBLAM_LogParser(object):
    '''
    Parser for CaBLAM stdout
    '''
    
    def __init__(self,stdout):
        self.stdout = stdout
        self.dict_outliers = OrderedDict()
        self.dict_summary = OrderedDict()
        for key in list_keywords:
            self.dict_summary[key] = 0
        self.list_chains = []
        self.dict_outliers_headers = {}
        stdout_fh = open(self.stdout,'r')
        residue_scores_start = False
        count_total_residues = 0
        for line in stdout_fh:
            if residue_scores_start and ':' in line:
                #':' separated output
                if line.count(':') < 4: continue
                # A
                chain = line[:2]
                if len(line[:2].strip()) == 0:
                    chain = ' '
                else:
                    chain = line[:2].strip()
                chain = fix_chainid(chain)
                if not chain in self.list_chains:
                    self.list_chains.append(chain)
                if not chain in self.dict_outliers:
                    self.dict_outliers[chain] = {}
                try:
                    outlier_details = self.get_outlier(line)
                except: continue
                if 'CaBLAM Disfavored' in line:
                    try:
                        self.dict_outliers[chain]['CaBLAM Disfavored'].append(outlier_details)
                    except KeyError:
                        self.dict_outliers[chain]['CaBLAM Disfavored'] = [outlier_details]
                    try:
                        self.dict_summary['CaBLAM Disfavored'] += 1
                    except KeyError:
                        self.dict_summary['CaBLAM Disfavored'] = 1
                elif 'CaBLAM Outlier' in line:
                    try: 
                        self.dict_outliers[chain]['CaBLAM Outlier'].append(outlier_details)
                    except KeyError:
                        self.dict_outliers[chain]['CaBLAM Outlier'] = [outlier_details]
                    try:
                        self.dict_summary['CaBLAM Outlier'] += 1
                    except KeyError:
                        self.dict_summary['CaBLAM Outlier'] = 1
                elif 'CA Geom Outlier' in line:
                    try: 
                        self.dict_outliers[chain]['CA Geom Outlier'].append(outlier_details)
                    except KeyError:
                        self.dict_outliers[chain]['CA Geom Outlier'] = [outlier_details]
                    try:
                        self.dict_summary['CA Geom Outlier'] += 1
                    except KeyError:
                        self.dict_summary['CA Geom Outlier'] = 1
                count_total_residues += 1
            if 'residue :' in line:
                residue_scores_start = True
        for measure in self.dict_summary:
            self.dict_summary[measure] = round(
                                            (self.dict_summary[measure]*100)/float(count_total_residues),
                                            3)

    def get_outlier(self,line):
        line_split = line[:-1].split(':')
        #chain, resnum, restype
        # A   7  ILE
        residue_details = line_split[0][2:].strip()
        #7 ILE
        residue_details = ' '.join(residue_details.split())
        #try beta sheet
        sec_str_proposed = line_split[4]
        try:
            alpha_score, beta_score, threeten_score = [float(val) for val in line_split[5:]]
        except:
            alpha_score, beta_score, threeten_score = ' ', ' ' ,' '
        return [residue_details.strip(),sec_str_proposed.strip()[3:],alpha_score, 
                beta_score, threeten_score]
            
