import os,sys,re
import subprocess
from ccpem_progs import jpred
import glob
import time
import types
import urllib2
import tarfile
from ccpem_core.pdb_tools.biopy_tools import *
import json
from collections import OrderedDict

class JpredLogParse():
    def __init__(self,job_location):
        #get dirs corresponding to job ids
        self.job_location = job_location
        self.list_chains = []
        cwd = os.getcwd()
        if os.path.isdir(job_location):
            os.chdir(job_location)
            self.parse_output_finished(job_location)
            #self.check_submitted_jobs(job_location)
        os.chdir(cwd)
    
    def check_submitted_jobs(self,jobdir,delay=2):
        '''
        Check submitted jpred jobs, parse and download outputs
        '''
        #count jobs
        list_jobs = glob.glob(jobdir+'/*log_running_job_id.txt')
        nJobsRunning = len(list_jobs)
        #count seq
        list_seqfiles = glob.glob(jobdir+'/*.fasta') #pdb_ch.fasta
        nseq = len(list_seqfiles)
        #save chains
        for seqfile in list_seqfiles:
            chain = os.path.basename(seqfile).split('_')[-1]
            self.list_chains.append(chain)
        self.failed_jobs = []
        self.finished_jobs = []
        self.failed_downloads = []
        self.dict_jobids = {}
        count_delays = 0
        while nJobsRunning <= nseq:
            time.sleep(delay) #delay may not be required in the results parser
            count_new_jobs = 0
            for jobpath in list_jobs:
                logfile = os.path.basename(jobpath)
                seqname = logfile.split('_log_running_job_id')[0]
                if seqname in self.failed_jobs: continue
                if seqname in self.finished_jobs: continue
                count_new_jobs += 1
                #check job status and download
                self.check_job_status(jobpath)
                print 'Failed: ', self.failed_jobs
                print 'Finished: ', self.finished_jobs
            count_delays += 1
            if count_delays >= 100: break
            if count_new_jobs == 0: break
            #if nJobsRunning == self.nseq: break
            #update running jobs
            list_jobs = glob.glob(jobdir+'/*log_running_job_id.txt')
            nJobsRunning = len(list_jobs)
        self.parse_output_finished()

    def check_job_status(self,logfilepath,checkevery=2):
        '''
        Check Jpred job status and download
        '''
        jobid = ''
        logfile = os.path.basename(logfilepath)
        #seqname is prefix of log file
        seqname = logfile.split('_log_running_job_id')[0]
        jobid = self.get_job_id(logfile)
        #save jobid
        self.dict_jobids[seqname] = jobid
        #if no jobid in log file
        if len(jobid) == 0: 
            try: self.failed_jobs.append(seqname)
            except: 
                self.failed_jobs = [seqname]
            return
        args = ['status','jobid={}'.format(jobid), 'getResults=no','silent',
                'checkEvery={}'.format(checkevery)]
        #get job status in a status file
        status_file = os.path.splitext(logfilepath)[0]
        status_file = status_file.split('log_running_job_id')[0] +'log_status.txt'
        #if status file exists
        if os.path.isfile(status_file):
            with open(status_file,'r') as sf:
                for line in sf:
                    #if job is finished, download results
                    if seqname in self.finished_jobs:
                        if 'http://' in line:
                            linesplit = line.split('http')
                            jpred_url = 'http'+\
                                        '/'.join(linesplit[1].split('/')[:-1])+\
                                        '/'+jobid+'.tar.gz'
                            #add a delay to download finished jobs
                            time.sleep(2)
                            download_flag = self.download_job_output(jpred_url)
                            if not download_flag:
                                self.failed_jobs.append(seqname)
                                print 'Failed: ', self.failed_jobs
                            return
                    #check if finished
                    if "finished. Results available at the following" in line:
                        try:
                            if not seqname in self.finished_jobs: 
                                self.finished_jobs.append(seqname)
                        except: self.finished_jobs = [seqname]
                        
        #check status of the job
        else:
            #add delay to check status
            time.sleep(1)
            command_args = commands['jpredapi'][:]
            command_args += args
            #print command_args
            with open(status_file,'w') as sf:
                subprocess.call(command_args,stdout=sf)

    def get_job_id(self,logfile):
        #get job id from logfile
        with open(logfile,'r') as lf:
            for line in lf:
                if ':' in line:
                    jobid = line.split()[-1]
                    break
        return jobid

    def download_job_output(self,job_url):
        return_flag = True
        cwd = os.getcwd()
        jobid = job_url.split('/')[-1]
        jobid = jobid.split('.')[0]
        if not os.path.isdir(jobid):
            os.mkdir(jobid)
            os.chdir(jobid)
        
        jpred_tarfile = job_url.split('/')[-1]
        command_args = commands['jpred_download'][:]
        command_args.append(job_url)
        command_args.append(jpred_tarfile)
        if not os.path.isfile(jpred_tarfile):
            print 'Downloading Jpred results:'
            print job_url
            subprocess.call(command_args)
        
        try:
            assert os.path.isfile(jpred_tarfile)
            tar = tarfile.open(jpred_tarfile, "r:gz")
            tar.extractall()
            tar.close()
        except (AssertionError,TypeError) as exc:
            self.failed_downloads.append(jobid)
            return_flag = False
        os.chdir(cwd)
        return return_flag
        
    def parse_output_finished(self,jobdir):
            
        self.jpred_predictions = {}
        #seq from atom records of pdb file
        pdbseq_json = os.path.join(self.job_location,'chain_atomseq.json')
        if not os.path.isfile(pdbseq_json):
            print 'Parsing pdb atom seq failed for', seqname 
            return
        with open(pdbseq_json,'r') as js:
            dict_residue_details = json.load(js)
            
        
        list_jobs = glob.glob(jobdir+'/*log_running_job_id.txt')
        
        for logfile in list_jobs:
            #seqname is prefix of log file
            logfilename = os.path.basename(logfile)
            seqname = logfilename.split('_log_running_job_id')[0]
            
            jobid = self.get_job_id(logfile)
            predseq, dict_pred = self.parse_output(jobid)
            if len(predseq) == 0 or dict_pred == None: return
            
            #seqname derived from logfilename is key in the atom sequence dict
            if seqname in dict_residue_details:
                try: 
                    atomseq = dict_residue_details[seqname]['atomseq']
                    list_resids = dict_residue_details[seqname]['residue_id']
                except KeyError:
                    print 'Atom sequence could not be found for', seqname
                    continue
                #compare atom record sequence with jpred processed sequence
                self.compare_atomseq_jpredseq(atomseq,predseq,dict_pred)
            href = ''
            status_file = seqname+'_log_status.txt'
            if os.path.isfile(status_file):
                with open(status_file,'r') as sf:
                    for line in sf:
                        if 'http://' in line:
                            linesplit = line.split('http')
                            href = 'http'+\
                                        linesplit[1][:-1]
                                                 
            if 'jnetpred' in dict_pred and len(dict_pred['jnetpred']) > 0:
                if len(list_resids) == len(dict_pred['jnetpred']):
                    chain_id = seqname.split('_')[-1]
                    c = 0
                    for resid in list_resids:
                        try: pred = dict_pred['jnetpred'][c]
                        except KeyError: break
                        if 'JNETCONF' in dict_pred:
                            try: conf = int(dict_pred['JNETCONF'][c])
                            except KeyError: 
                                conf = -1
                                break
                        else: conf = -1
                        
                        try: 
                            self.jpred_predictions[chain_id][resid] = \
                                                            [pred,
                                                            conf,href]
                        except KeyError:
                            self.jpred_predictions[chain_id] = OrderedDict()
                            self.jpred_predictions[chain_id][resid] = \
                                                            [pred,
                                                            conf,href]
                        c += 1
            
    def compare_atomseq_jpredseq(self,atomseq,predseq,dict_pred):
        aligned_seq1,aligned_seq2 = alignseq(atomseq,predseq)
        if None in [aligned_seq1,aligned_seq2]:
            return False
        for predprop in dict_pred:
            #if atom seq matches with predicted lengths
            if len(atomseq) == len(dict_pred[predprop]):
                pass
            #if pred sequence length matches with predicted property
            elif len(predseq) == len(dict_pred[predprop]):
                #add gaps based on the pred sequence aligned to atom sequence
                match_lists_add(dict_pred[predprop],aligned_seq2)
                #delete pred at gaps corresponding to atom sequence
                match_lists_del(dict_pred[predprop],aligned_seq1)
            #delete the property in case of sequence length mismatch
            else: del dict_pred[predprop]
        return True
    
    def parse_output(self,jobid):
        return_flag = True
        cwd = os.getcwd()
        seq = ''
        
        if os.path.isdir(jobid):
            #os.chdir(jobid)
            jnet_file = os.path.join(jobid,jobid+'.jnet')
            fasta_file = os.path.join(jobid,jobid+'.fasta')
            
            if os.path.isfile(fasta_file):
                with open(fasta_file,'r') as seqf:
                    seqstart = False
                    for line in seqf:
                        if seqstart:
                            seq += line[:-1]
                        if line[0] == '>':
                            seqstart = True
            if os.path.isfile(jnet_file):
                dict_pred = {}
                with open(jnet_file,'r') as jf:
                    for line in jf:
                        if ':' in line:
                            linesplit = line.split(':')
                            key = linesplit[0]
                            val = linesplit[1].split(',')[:-1]
                            dict_pred[key] = val
                    
            else: return_flag = False
            if not 'jnetpred' in dict_pred: return_flag = False
            if not return_flag:
                return seq, None
            
            return seq, dict_pred

