import sys
import re
import os
from collections import OrderedDict
import json
import glob
from math import ceil


class PIscoreLogParser(object):
    '''
    Parser for Molprobity stdout
    '''
    def __init__(self,outdir):
        self.outdir = os.path.join(outdir,'pi_output')
        for g in glob.glob(os.path.join(self.outdir,'pi_score_*')):
            #print g
            self.globoutfile = g
            break
        self.dict_summary = OrderedDict()
        self.dict_outliers = OrderedDict()
        self.list_chains = []
        self.dict_outliers_headers = {}
        self.summary_start = False
        self.outlier_start = False
        value = None
        self.get_summary()
        self.get_outliers()
        #print self.dict_outliers
    def get_summary(self):
        '''
        Get global PI-score statistics from output
        '''
        self.dict_summary['PI-score Disfavored'] = []
        self.dict_summary['PI-score Outlier'] = []
        stdout_fh = open(self.globoutfile,'r')
        for line in stdout_fh:
            if line[0] != '#':
                linesplit = line.strip().split(',')
                modelid = linesplit[0]
                chain_pair = linesplit[1]
                for chainID in chain_pair.split('_'):
                    if not chainID in self.list_chains:
                        self.list_chains.append(chainID)
                pi_score = linesplit[-1]
                
                try:
                    if float(pi_score) < -0.5 and float(pi_score) >= -1.0:
                        self.dict_summary['PI-score Disfavored'].append(chain_pair)
                    if float(pi_score) < -1.0:
                        self.dict_summary['PI-score Outlier'].append(chain_pair)
                except ValueError: pass
        stdout_fh.close()
        
        
    def is_summary(self,line):
        if line[0] == '=' and line.split()[1] == 'Summary':
            self.summary_start = True
            self.outlier_start = False
        try:
            if line.split()[0] == 'molprobity' and \
                line.split()[-1] == 'statistics.':
                self.summary_start = True
        except IndexError: pass
        try:
            if line.split()[0] == 'Results' or \
                line.split()[0] == 'Refinement':
                self.summary_start = False
        except IndexError: pass
        
    def is_outlier(self,line):
        if line[0] == '=' and line.split()[1] == 'Geometry':
            self.outlier_start = True
        if '------' in line:
            self.summary_start = False
    
    def get_outliers(self):
        
        for g in glob.glob(os.path.join(self.outdir,'**/*_interface_residues_dict.json')):
            self.residue_interface_file = g
            break
        with open(self.residue_interface_file,'r') as j:
            self.residue_interface = json.load(j)
            
        for cp in self.dict_summary['PI-score Outlier']:
            list_cp = cp.split('_')
            interface_residues1 = '' 
            
            try:
                for m in range(int(ceil(len(self.residue_interface[cp][0])/10))):
                    list_b = []
                    if m != 0: interface_residues1 += '\n'
                    for l in range(10):
                        try: list_b.append(str(self.residue_interface[cp][0][m*10+l]))
                        except IndexError: break
                        
                    interface_residues1 += ','.join(list_b)
            except KeyError: continue
            interface_residues2 = ''
            try:
                for m in range(int(ceil(len(self.residue_interface[cp][1])/10))):
                    list_b = []
                    if m != 0: interface_residues2 += '\n'
                    for l in range(10):
                        try:
                            list_b.append(str(self.residue_interface[cp][1][m*10+l]))
                        except IndexError: break
                    interface_residues2 += ','.join(list_b)
                    
            except KeyError: continue
            try: 
                self.dict_outliers[list_cp[0]][list_cp[1]] = interface_residues1
            except KeyError:
                self.dict_outliers[list_cp[0]] = {list_cp[1]:interface_residues1}
            try: 
                self.dict_outliers[list_cp[1]][list_cp[0]] = interface_residues2
            except KeyError:
                self.dict_outliers[list_cp[1]] = {list_cp[0]:interface_residues2}
        
        for p in self.dict_summary:
            if len(self.dict_summary[p]) == 0:
                self.dict_summary[p] = ['None']
    def get_chains(self,line):
        if "Chain:" in line:
            chainID = line.split()[1]
            if chainID[0] == '"':
                try: 
                    chainID = chainID[1:-1]
                    if not chainID in self.list_chains:
                        self.list_chains.append(chainID)
                except IndexError: pass
