import sys
import re
import os
import time
import pyrvapi
import fileinput
from collections import OrderedDict
import shutil
import numpy as np
import pandas as pd
import math
from piscore_log_parser import PIscoreLogParser
from ccpem_core.tasks.atomic_model_validation.cootscript_edit import SetCootScript
from ccpem_core.tasks.atomic_model_validation.pyrvapi_utils import *
import copy, json

global_tab = 'global_tab'
local_tab = 'local_tab'
global_tab_name = 'Results (Global)'
local_tab_name = 'Results (Local)'
global_reference_tab = 'global_reference_tab'
global_reference_tab_name = 'References (Global)'
local_reference_tab = 'local_reference_tab'
local_reference_tab_name = 'References (Local)'
angstrom_label = (ur'(\u00c5)').encode('utf-8')

class SetPIscoreResults():
    def __init__(self,piscore_process,
                 list_chains=[],
                 cootset_instance=None,
                 glob_reportfile=None):
        self.piscore_process = piscore_process
        self.cootset = cootset_instance
        self.list_chains = list_chains
        #outlier residue summary
        self.residue_outliers = OrderedDict()
        self.residue_coordinates = OrderedDict()
        #list of pdb inputs
        self.list_modelids = []
        self.dict_cootfiles = {}
        self.dict_cootdata = OrderedDict()
        self.dict_logparse = OrderedDict()
        #outlier details
        self.dict_outlier_details = OrderedDict()
        #output json files for each model
        self.dict_jsonfiles = OrderedDict()
        self.dict_json_cacoord = OrderedDict()
        self.model_res = {}
        dict_cootset = {}
        self.add_chain_sections = False
        ct_model = 0
        for piprocess in self.piscore_process:
            model_id = piprocess.name.split(' ')[-1]
            self.list_modelids.append(model_id)
            self.dict_logparse[model_id] = PIscoreLogParser(piprocess.location)

            if len(list_chains) == 0:
                for ch in self.dict_logparse[model_id].list_chains:
                    if not ch in self.list_chains:
                        self.list_chains.append(ch)
                self.add_chain_sections = True
            #print self.list_chains
            self.dict_jsonfiles[model_id] = os.path.join(piprocess.location,
                                                        model_id+"_piscore.json")
        
        self.glob_reportfile = glob_reportfile
        if self.glob_reportfile is not None:
            self.glob_reportfile.write('*PI-score* (subunit interface quality)\n')
            self.glob_reportfile.write('======================================\n')
        self.set_global_results()
        
        if self.glob_reportfile is not None: 
            self.glob_reportfile.write('------------------------------------\n\n')
        self.set_local_data()
        self.save_outlier_json()
        self.add_cootdata()
        self.delete_cootdata()
        self.set_local_results()

    def add_cootdata(self):
        for mid in self.dict_cootdata:
            if self.cootset is not None:
                self.cootset.add_data_to_cootfile(self.dict_cootdata[mid],
                    modelid=mid, method='piscore')
                
    def set_global_results(self):
        tab_id = 'global_tab'
        section_id = 'piscore_global_sec'
        pyrvapi.rvapi_add_section(section_id, 'PI-score summary', tab_id, 
                                  0, 0, 1, 1, False)
        self.set_summary_table(tab_id,section_id,"piscore_summary")

    def set_summary_table(self, tab, section, table_id):
        add_table_to_section(section,table_id)
        gs = global_summary_settings()
        add_vert_headers_to_table(table_id,gs.list_vert_headers,
                                  gs.list_vert_header_tooltips)
        #expand vert header for all models
        list_horz_headers, list_horz_header_tooltips = \
            self.expand_table_header_models(gs.list_horz_headers, 
                                            gs.list_horz_header_tooltips)
        add_horz_headers_to_table(table_id,list_horz_headers,
                                  list_horz_header_tooltips)
        ct_model = 0
        for mid in self.list_modelids:
            if self.glob_reportfile is not None:
                self.glob_reportfile.write('>{}\n'.format(mid))
            dict_summary = self.dict_logparse[mid].dict_summary
            data_array = [[';'.join(dict_summary[gs.list_vert_headers[l]])] \
                          for l in xrange(len(gs.list_vert_headers))]
            add_data_to_table(table_id,data_array, start_col=ct_model)
            for l in xrange(len(gs.list_vert_headers)):
                if self.glob_reportfile is not None:
                    self.glob_reportfile.write('-{}: {}%\n'.format(gs.list_vert_headers[l],
                                                         data_array[l][0]))
            ct_model += 1
        pyrvapi.rvapi_flush()

    def expand_table_header_models(self, list_headers, list_header_tooltips):
        list_expanded_headers = []
        list_expanded_header_tips = []
        for mid in self.list_modelids:
            ct_h = 0
            for h in xrange(len(list_headers)):
                header = list_headers[h]
                header += "<br>({})".format(mid)
                list_expanded_headers.append(header)
                list_expanded_header_tips.append(list_header_tooltips[h])
        return list_expanded_headers, list_expanded_header_tips

    def set_local_data(self):
        '''
        get outlier data from log parser and store in a dictionary/json
        '''
        outlier_settings = residue_outlier_settings()
        for chain in self.list_chains:
            self.set_outlier_data(chain,outlier_settings)
#     
    def set_outlier_data(self,chain, outlier_settings):
         
        for mid in self.list_modelids:
            if not mid in self.residue_outliers:
                self.residue_outliers[mid] = {}
                self.residue_coordinates[mid] = {}
            if not chain in self.residue_outliers[mid]:
                #save outlier residues
                self.residue_outliers[mid][chain] = {}
                self.residue_coordinates[mid][chain] = {}
            lp = self.dict_logparse[mid]
            if not chain in lp.dict_outliers: continue
                
            dict_outlier_data = lp.dict_outliers[chain]
            self.save_outlier_data(dict_outlier_data, 
                                                 chain,mid,
                                                 outlier_settings)
#     
    def save_outlier_data(self, dict_outlier_data,
                                chain,mid,
                                    outlier_settings):
        ct_res = 0
        measure = 'PI-score Outlier'
        list_data = []
        
        for ch in dict_outlier_data:# interacting chain
            outlier = []
            ct_val = 0
            
            for res_str in dict_outlier_data[ch].split('\n'):
                for o in res_str.split(','):
                    residue_details = []
                    residue_number = o[3:]
                    residue_name = o[:3]
                    if self.cootset is not None:
                        coot_data_res = chain,residue_number,residue_name, o
                        try: 
                            self.dict_cootdata[mid]['piscore'].append((coot_data_res))
                        except KeyError:
                            self.dict_cootdata[mid] = {'piscore':[(coot_data_res)]}
                    #print o, residue_number, residue_name
                    #save outlier residue and outlier type
                    if 'Disfavored' not in measure:
                        self.residue_outliers[mid][chain][str(residue_number)] = \
                                    outlier_settings.data_titles[measure] 
             
                    ct_res += 1
            residue_details = [ch,dict_outlier_data[ch]]
            list_data.append(residue_details)
        #print list_data
        #set dict of outlier details
        if len(list_data) == 0: return
        if not mid in self.dict_outlier_details:
            self.dict_outlier_details[mid] = {}
        if not chain in self.dict_outlier_details[mid]:
            self.dict_outlier_details[mid][chain] = {}
        self.dict_outlier_details[mid][chain][measure] = list_data[:]
# 
    def save_outlier_json(self):
        for mid in self.dict_outlier_details:
            try:
                jsonfile = self.dict_jsonfiles[mid]
            except KeyError: continue
            with open(jsonfile,'w') as jf:
                json.dump(self.dict_outlier_details[mid],jf)
#                 
    def delete_cootdata(self):
        self.dict_cootdata.clear()
        del self.dict_cootdata
#     
    def delete_dictresults(self):
        self.dict_outlier_details.clear()
        del self.dict_outlier_details
#     
    def set_local_results(self):
        '''
        Set local sections and tables
        '''
        #local_tab = 'local_tab'
        #local_tab_name = "Local Results"
        outlier_settings = residue_outlier_settings()
        count_chain = 0
        #print self.list_chains
        for chain in self.list_chains:
            chain_section = chain
            #skip chain tab if outliers not found
            if not hasattr(self, 'dict_outlier_details'):
                continue
            if self.check_chain_outliers(chain):
                chain_name = 'chain '+chain
                if self.add_chain_sections:
                    pyrvapi.rvapi_add_section(chain_section, chain_name, 
                                              local_tab, 0, 0, 1, 1, False)
                #print chain_section
                self.set_outlier_sections_tables(chain_section,
                                                     outlier_settings)
                     
                count_chain += 1
                pyrvapi.rvapi_flush()
        self.delete_dictresults()
# 
    def check_chain_outliers(self,chain):
        for mid in self.dict_outlier_details:
            if chain in self.dict_outlier_details[mid]:
                return True
        return False
# 
    def set_outlier_sections_tables(self,chain,outlier_settings):
        
        for measure in outlier_settings.data_keys:
            #print measure
            outlier_subsection = \
                    "PI-score"+''.join(measure.split())+str(chain)
            #print outlier_subsection
            if measure in outlier_settings.data_titles:
                outlier_subsection_name = "PI-score: "+ \
                                    outlier_settings.data_titles[measure]
                add_subsection(chain,outlier_subsection,
                               outlier_subsection_name,show=True)
#                 add_text_to_section(outlier_subsection,
#                                     [outlier_settings.data_info[measure]])
                pyrvapi.rvapi_flush()
                #print self.list_modelids
                #add separate tables for each model
                for mid in self.list_modelids:
                      
                    table_id = 't'+''.join(measure.split()) + \
                                str(chain)+mid
                    #print table_id
                    table_name = mid #+ ':' +outlier_settings.data_titles[measure]
                    add_table_to_section(outlier_subsection,table_id, table_name)
                  
                    list_horz_headers = outlier_settings.data_headers[measure][:]
                    list_horz_headers_tips = outlier_settings.data_tooltips[measure][:]
                    #print list_horz_headers, list_horz_headers_tips
                    #print measure, mid, chain, list_horz_headers
                    add_horz_headers_to_table(table_id,list_horz_headers,list_horz_headers_tips)
                     
                    self.add_outlier_data_to_table(table_id,chain,mid,measure)
        pyrvapi.rvapi_flush()
# 
# 
#     def get_measures_chain(self,chain):
#         '''
#         Return outlier types for given chain
#         '''
#         list_measures = []
#         for mid in self.list_modelids:
#             lp = self.dict_logparse[mid]
#             dict_outlier = lp.dict_outliers
#             if chain in dict_outlier:
#                 for measure in dict_outlier[chain]:
#                     if not measure in list_measures:
#                         list_measures.append(measure)
#         return list_measures
#     
    def add_outlier_data_to_table(self,table_id,chain,mid,measure):
        #print self.dict_outlier_details
        if mid in self.dict_outlier_details:
            if chain in self.dict_outlier_details[mid]:
                if measure in self.dict_outlier_details[mid][chain]:
                    list_data = self.dict_outlier_details[mid][chain][measure]
                    #print list_data
                    add_data_to_table(table_id,list_data)
         
    def add_local_tabs_chains(self,list_chains):
        for chain in list_chains:
            pyrvapi.rvapi_add_tab(chain, "chain: "+chain, 
                                  True)

class global_summary_settings() :
    data_keys = ['PI-score Outlier',
                     'PI-score Disfavored']
    list_horz_headers = ['Subunit interface issues']
    list_horz_header_tooltips = ['Subunit interfaces with potential geometry issues']
    list_vert_headers = data_keys
    list_vert_header_tooltips = ['PIscore < -1.0','-1.0 <= PIscore < -0.5']
    
class residue_outlier_settings() :
    #NOTE: data_keys has same keywords as in log file
    #change this with updates
    data_keys = ['PI-score Outlier']
    data_titles = {'PI-score Outlier':'Outlier'}
    data_info = {'PI-score Outlier': "PI-score < -1.0"}
    data_headers = {
                    'PI-score Outlier':["Interfacing chain","Residues"]}
    data_tooltips = {
                    'PI-score Outlier':["chain it interfaces with", "residues at the interface"]
                  }