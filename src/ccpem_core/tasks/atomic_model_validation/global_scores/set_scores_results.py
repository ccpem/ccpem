import sys
import re
import os
import pyrvapi
import shutil
import numpy as np
import pandas as pd
import math
from collections import OrderedDict
from ccpem_core.tasks.atomic_model_validation.pyrvapi_utils import *
from copy import deepcopy
import json

class SetScoresResults():
    def __init__(self, scores_process, list_chains=[],
                 molprobity_process=None, cootset_instance=None,
                 glob_reportfile=None):
        self.scores_process = sorted(scores_process, 
                                     key=lambda j: j.name.split(' ')[-1][-1]+
                                                    j.name.split(' ')[0][-1])
        self.scores_results = []
        self.scores_dataframes = OrderedDict()
        #output json files for each model
        self.dict_jsonfiles = OrderedDict()
        self.list_modelids = []
        self.list_pdbids = []
        self.list_mapids = []
        self.dict_cootdata = OrderedDict()
        
        self.glob_reportfile = glob_reportfile
        if self.glob_reportfile is not None:
            self.glob_reportfile.write('*TEMPy* global real space scores\n')
            self.glob_reportfile.write('================================\n')
                    
        for scores_job in scores_process:
            scores_result = os.path.join(scores_job.location,'global_scores.csv')
            self.scores_results.append(scores_result)
            #get result from the output text file
            if os.path.isfile(scores_result):
                modelid = scores_job.name.split(' ')[-1]
                self.scores_dataframes[modelid] = pd.read_csv(scores_result,
                                            skipinitialspace=True,
#                                               skip_blank_lines=True,
                                              sep=';')
                if not modelid in self.list_modelids: self.list_modelids.append(modelid)
                self.dict_jsonfiles[modelid] = os.path.join(scores_job.location,
                                                        modelid+"_scores.json")
#                 pdbid = modelid.split(':')[-1]
#                 if not pdbid in self.list_pdbids: 
#                     self.list_pdbids.append(pdbid)
#                 mapid = modelid.split(':')[0]
#                 if not mapid in self.list_mapids: 
#                     self.list_mapids.append(mapid)
        #set global scores
        self.set_global_results()
                
        if self.glob_reportfile is not None:
            self.glob_reportfile.write('--------------------------------\n\n')
        
    def set_global_results(self):
        pipeline_tab = 'global_tab'
        scores_section_id = 'scores_global_section'
        pyrvapi.rvapi_add_section(scores_section_id, 'Global scores (TEMPy)', pipeline_tab, 
                                  0, 0, 1, 1, False)
        section_added = []
        for modelid in self.scores_dataframes:
            #add subsection
            mapid = modelid#.split(':')[0]
            subsection_id = ''.join(mapid.split())
            subsection_name = mapid
            add_subsection(scores_section_id,subsection_id,
                       subsection_name,show=True)
            #add table to section
            table_id = "tempy_scores"+subsection_id
            table_name = ""
            add_table_to_section(subsection_id,table_id,table_name,show=True)
            section_added.append(mapid)
            if self.glob_reportfile is not None:
                self.glob_reportfile.write('>{}\n'.format(modelid))
                
            col = self.scores_dataframes[modelid].columns[0]
            for i in range(len(self.scores_dataframes[modelid].index)):
                index = self.scores_dataframes[modelid].index[i]
                pdb = self.scores_dataframes[modelid][col][index]
                pdbid = os.path.splitext(pdb)[0]
                if not pdbid in self.list_pdbids: self.list_pdbids.append(pdbid)
            #set table headers
            self.set_table_header(subsection_id, table_id)
            
            self.set_results_tables(table_id,self.scores_dataframes[modelid])
    
    def set_table_header(self,section_id, table_id, table_name=''):
        self.list_horz_headers = self.list_pdbids
        list_horz_header_tooltips = ['Model id + model number']*len(self.list_pdbids)
        self.list_vert_headers = ['overlap_map','overlap_model','correlation','local_correlation','local_mi']
        if len(self.list_horz_headers) >2:
            self.list_vert_headers += ['ccc_ov','mi_ov']
        list_vert_header_tooltips = [dict_scores_tooltip[sc] for sc in self.list_vert_headers]
        add_horz_headers_to_table(table_id,self.list_horz_headers,
                                  list_horz_header_tooltips)
        add_vert_headers_to_table(table_id,self.list_vert_headers,
                                  list_vert_header_tooltips)
        
    def set_results_tables(self,table_id,scores_dataframe,start_col=0):
        ct_sc = len(scores_dataframe.columns)
        
        list_scores = []
        for i in range(len(scores_dataframe.index)):
            index = scores_dataframe.index[i]
            ct_it = 0
            for j in range(1,ct_sc):
                if j == 4: continue#skip MI
                if len(self.list_pdbids) < 3:
                    if j > ct_sc-3: continue
                col = scores_dataframe.columns[j]
                try: list_scores[ct_it].append(round(scores_dataframe[col][index],3))
                except IndexError: list_scores.append([round(scores_dataframe[col][index],3)])
                
                if self.glob_reportfile is not None:
                    self.glob_reportfile.write('-{}: {}\n'.format(
                                                self.list_vert_headers[ct_it],
                                                round(scores_dataframe[col][index],3)))
                ct_it += 1
            add_data_to_table(table_id,list_scores)
        pyrvapi.rvapi_flush()
        
dict_scores_tooltip = {'overlap_map':'fraction of overlap w.r.t map',
                       'overlap_model':'fraction of overlap w.r.t model',
                               'correlation': 'cross correlation coefficient',
                               'mi': 'mutual information',
                               'local_correlation': 'cross correlation coefficent'\
                                ' calculated on the volume of overlap',
                                'local_mi': 'mutual information score'\
                                ' calculated on the volume of overlap',
                                'ccc_ov': 'combined local_correlation and overlap score',
                                'mi_ov': 'combined mutual information and overlap score'}