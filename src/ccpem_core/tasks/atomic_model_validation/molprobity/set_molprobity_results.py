import sys
import re
import os
import time
import pyrvapi
import fileinput
from collections import OrderedDict
import shutil
import numpy as np
import pandas as pd
import math
from molprobity_log_parser import MolprobityLogParser
from ccpem_core.tasks.atomic_model_validation.cootscript_edit import SetCootScript
from ccpem_core.tasks.atomic_model_validation.pyrvapi_utils import *
import copy, json

global_tab = 'global_tab'
local_tab = 'local_tab'
global_tab_name = 'Results (Global)'
local_tab_name = 'Results (Local)'
global_reference_tab = 'global_reference_tab'
global_reference_tab_name = 'References (Global)'
local_reference_tab = 'local_reference_tab'
local_reference_tab_name = 'References (Local)'
angstrom_label = (ur'(\u00c5)').encode('utf-8')

class SetMolprobityResults():
    def __init__(self,molprobity_process,
                 list_chains=[],
                 cootset_instance=None,
                 glob_reportfile=None):
        self.molprobity_process = molprobity_process
        self.cootset = cootset_instance
        self.list_chains = list_chains
        #outlier residue summary
        self.residue_outliers = OrderedDict()
        self.residue_coordinates = OrderedDict()
        #list of pdb inputs
        self.list_modelids = []
        self.dict_cootfiles = {}
        self.dict_cootdata = OrderedDict()
        self.dict_logparse = OrderedDict()
        #outlier details
        self.dict_outlier_details = OrderedDict()
        #output json files for each model
        self.dict_jsonfiles = OrderedDict()
        self.dict_json_cacoord = OrderedDict()
        self.list_chains = []
        self.model_res = {}
        dict_cootset = {}
        self.add_chain_sections = False
        ct_model = 0
        for molprocess in self.molprobity_process:
            model_id = molprocess.name.split(' ')[-1]
            self.list_modelids.append(model_id)
            self.dict_logparse[model_id] = MolprobityLogParser(molprocess.stdout)
            #list of residue outliers for each model
            #self.residue_outliers[model_id] = {}
            self.residue_coordinates[model_id] = {}
            model_info_json = os.path.join(molprocess.location,'model_info.json')
            with open(model_info_json,'r') as jf:
                model_info = json.load(jf)
            resolution = model_info['resolution']
            if resolution == 0.0:
                self.model_res[model_id] = 'NA'
            else: self.model_res[model_id] = resolution
            self.molprobity_cootdata = None
            self.dict_cootfiles[model_id] = os.path.join(
                molprocess.location,
                'molprobity_coot.py')
            cootfile = self.dict_cootfiles[model_id]
            self.dict_jsonfiles[model_id] = os.path.join(molprocess.location,
                                                        model_id+"_molprobity.json")
            if os.path.isfile(cootfile):
                #just as a workaround get residue data from coot script
                #mmtbx validation object can be used with ccp4-python but
                #avoiding this step for now
                if not self.cootset is None:
                    self.cootset.set_molprobity_cootscript(cootfile,
                                                              modelid=model_id,
                                                              method='molprobity')
                    self.dict_cootdata[model_id] = \
                        self.cootset.get_data_from_cootscript(cootfile,
                                                              modelid=model_id,
                                                              method='molprobity')
            if len(list_chains) == 0:
                for chain in self.dict_logparse[model_id].list_chains:
                    if not chain in self.list_chains:
                        self.list_chains.append(chain)
                self.add_chain_sections = True
            else: self.list_chains = list_chains[:]
            
            ca_coord_jsonfile = os.path.join(molprocess.location,'ca_coord.json')
            self.dict_json_cacoord[model_id] = ca_coord_jsonfile
        
        self.glob_reportfile = glob_reportfile
        if self.glob_reportfile is not None:
            self.glob_reportfile.write('*Molprobity* model geometry\n')
            self.glob_reportfile.write('===========================\n')
        self.set_global_results()
        if self.glob_reportfile is not None:
            self.glob_reportfile.write('---------------------------\n\n')
        self.set_local_data()
        self.save_outlier_json()
        self.set_local_results()
                    
    def get_data_from_jsonfile(self,jsonfile):
        with open(jsonfile,'r') as jf:
            data = json.loads(jf.read())
        #print data
        
    def set_global_results(self):
        pipeline_tab = 'global_tab'
        build_sec = 'molprobity_global_sec'
        pyrvapi.rvapi_add_section(build_sec, 'Molprobity summary', pipeline_tab, 
                                      0, 0, 1, 1, False)
        gs = global_summary_settings()
        table_id = "molprobity_summary"
        self.set_summary_table(pipeline_tab,build_sec,table_id,gs)
        list_horz_headers = []
        list_horz_header_tooltips = []
        #set header including each model id
        for mid in self.list_modelids:
            for h in xrange(len(gs.list_horz_headers)):
                hheader = gs.list_horz_headers[h]
                list_horz_headers.append(hheader+"<br>({})".format(mid))
                list_horz_header_tooltips.append(gs.list_horz_header_tooltips[h])
                
        list_horz_headers.append('Expected range')
        list_horz_header_tooltips.append('Expected/acceptable range')
        add_horz_headers_to_table(table_id,list_horz_headers,
                                  list_horz_header_tooltips)
        mnum = 0
        for mid in self.list_modelids:
            if self.glob_reportfile is not None:
                self.glob_reportfile.write('>{}\n'.format(mid))
                
            lp = self.dict_logparse[mid]
            dict_summary = lp.dict_summary
            self.add_global_data(table_id,dict_summary,gs,mnum)
            mnum += 1
            #print dict_summary
        ct_id = 0
        for modelid in self.model_res:
            if self.model_res[modelid] == 'NA':
                issue_line = modelid+": Warning, resolution not found!, \
                                    using a general reference distribution for percentiles"
                print_line = "<h3 style='color:orange'>{0}</h3>".format(issue_line)
                
            else:
                issue_line = modelid+": resolution = "+str(self.model_res[modelid])
                print_line = "<h3 style='color:orange'>{0}</h3>".format(issue_line)
            pyrvapi.rvapi_add_text(print_line, build_sec, 3+ct_id, 0, 1, 1)
            ct_id += 1

    def set_summary_table(self, tab, section, table_name,gs):
        add_table_to_section(section,table_name)
        add_vert_headers_to_table(table_name,gs.list_vert_headers,
                                  gs.list_vert_header_tooltips)
    def add_global_data(self,table_name,dict_summary,gs,mnum):
        i = 0
        for statname in gs.data_keys:
            if not statname in dict_summary:
                pyrvapi.rvapi_put_table_string(table_name,' ',i,mnum)
                if mnum == 0:
                    pyrvapi.rvapi_put_table_string(table_name,
                                               gs.dict_referencerange[statname],
                                               i,len(self.list_modelids))
                i += 1
                continue
                
            stat_value = dict_summary[statname][0]
            percentile = ''
            if statname in ['Clashscore','MolProbity score']:
                if '<br>' in stat_value:
                    stat_split = stat_value.split('<br>')
                    stat_value = stat_split[0]
                    percentile = stat_split[1]
            if isinstance(stat_value,float):
                stat_value = "{:.2f}".format(round(stat_value,2))
            if len(self.list_modelids) > 1:#add individual percentiles
                stat_value = stat_value + '<br>' + percentile
            pyrvapi.rvapi_put_table_string(table_name,stat_value,i,mnum)
            
            if mnum == 0:
                if gs.dict_referencerange.has_key(statname):
                    if statname in ['Clashscore','MolProbity score']:
                        if len(self.list_modelids) == 1:
                            pyrvapi.rvapi_put_table_string(table_name,
                                           percentile,
                                           i,len(self.list_modelids))
                    else:
                        pyrvapi.rvapi_put_table_string(table_name,
                                           gs.dict_referencerange[statname],
                                           i,len(self.list_modelids))
            i += 1
            
            if self.glob_reportfile is not None:
                if '<br>' in stat_value:
                    stat_value = " ".join(stat_value.split('<br>'))
                        
                statname_fixed = statname
                if statname == 'favored':
                    statname_fixed = 'Ramachandran '+statname
                if statname in ['Clashscore','MolProbity score'] and \
                                                len(self.list_modelids) == 1:
                    self.glob_reportfile.write('-{}: {} [{}] \n'.format(
                                                        statname_fixed,
                                                         stat_value,
                                                         percentile))
                else:
                    self.glob_reportfile.write('-{}: {} [expected: {}] \n'.format(
                                                        statname_fixed,
                                                         stat_value,
                                                         gs.dict_referencerange[statname]))
                    
        pyrvapi.rvapi_flush()
        
    def set_local_data(self):
        outlier_settings = residue_outlier_settings()
        count_chain = 0
        
        for chain in self.list_chains:
            #if not chain in self.dict_logparse: continue
            #save outlier residues
            mnum = 0
            for mid in self.list_modelids:
#                 self.residue_outliers['nomap'] = {}
                if not mid in self.residue_outliers:
                    self.residue_outliers[mid] = {}
                if not chain in self.residue_outliers[mid]:
                    self.residue_outliers[mid][chain] = {}
                    self.residue_coordinates[mid][chain] = {}
                try:
                    log_data = self.dict_logparse[mid]
                except KeyError: continue
                if mid in self.dict_cootdata:
                    cootdata = self.dict_cootdata[mid]
                    self.save_outlier_data(cootdata, chain,
                                                 outlier_settings, mnum=mnum)
                #check outlier for the chain
                if chain in log_data.dict_outliers:
                    self.save_outlier_data(log_data.dict_outliers[chain], chain,
                                                 outlier_settings, cootdata=False,
                                                 mnum=mnum)
                mnum += 1
            count_chain += 1
                
                    
    def save_outlier_data(self,dict_outlier_data, chain,
                                    outlier_settings, cootdata=True,
                                    mnum=0):
        mid = self.list_modelids[mnum]
        if not mid in self.residue_coordinates:
            self.residue_coordinates[mid] = {}
        if not chain in self.residue_coordinates[mid]:
            self.residue_coordinates[mid][chain] = {}
        try:
            with open(self.dict_json_cacoord[mid],'r') as cj:
                dict_cacoord = json.load(cj)
        except KeyError: dict_cacoord = {}
        
        for measure in dict_outlier_data:
            l = 0
            ct_res = 0
            list_data = []
            #data from coot script
            if not cootdata:
                try: dict_data = dict_outlier_data[measure] #log based outliers
                except KeyError: continue
            else:
                #log data
                dict_data = dict_outlier_data[measure]
            #outlier details
            for loutlier in dict_data:
                outlier = []
                for val in loutlier:
                    roundedval = val
                    if isinstance(val,float):
                        roundedval = "{:.4}".format(round(val,4))
                    elif isinstance(val,str):
                        roundedval = val.strip()
                    outlier.append(roundedval)
                #print loutlier, outlier
                if len(outlier) == 0: continue
                if measure in outlier_settings.data_titles:
                    ct_res += 1
                    if cootdata: 
                        res_coordinate = outlier[-1]
                        
                        if measure == 'probe': 
                            #TODO: check chain in coot data outliers
                            if outlier[0].split()[0] != chain and \
                                outlier[1].split()[0] != chain: continue
                                
                            residue_details = outlier[:-1] # -1: coordinate
                            
                            if outlier[0].split()[0] == chain:
                                atom_id_split = residue_details[0].split()
                                if len(atom_id_split) == 4:
                                    residue_num1 = str(atom_id_split[1])
                                if outlier[1].split()[0] == chain:
                                    atom_id_split = residue_details[1].split()
                                    if len(atom_id_split) == 4:
                                        residue_num2 = str(atom_id_split[1])
                                        if residue_num2 != residue_num1:
                                            residue_num = [residue_num1,
                                                       residue_num2]
                                        else: residue_num = residue_num1
                                        
                            elif outlier[1].split()[0] == chain:
                                atom_id_split = residue_details[1].split()
                                residue_num = str(residue_details[1].split()[1])
                            else: 
                                atom_id_split = []
                                residue_num = ''
                            
                        elif measure == 'cbeta':
                            if outlier[0].split()[0] != chain: continue
                            #ignore Conf from the details
                            residue_details = outlier[1:3]
                            residue_details.append(outlier[-2])
                            residue_num = str(residue_details[0].strip())
                        else: 
                            if outlier[0].split()[0] != chain: continue
                            residue_details = outlier[1:-1] #0: chain, -1: coordinate
                            residue_num = str(residue_details[0].strip())
                        #save outlier residue and outlier type
                        outlier_title = outlier_settings.data_titles[measure]
                        #separate backbone and sidechain clashes
                        if measure == 'probe' and len(atom_id_split) > 0:
                            #A 40 LYS HG2
                            if atom_id_split[-1] in ['N','CA','C','O',
                                                     'P','OP1','OP2',
                                                     "C3'","C4'","C5'",
                                                     "O3'","O5'"]:
                                outlier_title = 'backbone clash'
                            else: outlier_title = 'side-chain clash'
                        
                        try: 
                            if len(residue_num) == 0: continue
                        except UnboundLocalError: continue
                        
                        if isinstance(residue_num,list):
                            for resnum in residue_num:
                                self.residue_coordinates[mid][chain][resnum] = \
                                                    res_coordinate
                                self.add_outlier_title_for_residues(outlier_title, 
                                                            chain, resnum,
                                                            mnum=mnum)
                        else:
                            self.residue_coordinates[mid][chain][residue_num] = \
                                                    res_coordinate
                            self.add_outlier_title_for_residues(outlier_title, 
                                                            chain, residue_num,
                                                            mnum=mnum)
                    else: 
                        #['19 ASN C<br>20 VAL N<br>20 VAL CA', '4.7*sigma']
                        residue_details = outlier
                        resdet_split = residue_details[0].split('<br>')
                        #save outlier residue and outlier type
                        outlier_title = outlier_settings.data_titles[measure]
                        #remove'outliers' from measure
                        if 'outliers' in outlier_title: 
                            outlier_title = ' '.join(outlier_title.split()[:-1])
                        outlier_title_ext = outlier_title
                        flag_pass = 1
                        res_prev = ''
                        
                        #multiple residues
                        for resdet in resdet_split:
                            #G T   8
                            if measure == 'Backbone torsion suites':
                                #print residue_details, resdet_split, resdet
                                if len(resdet.split()) < 3:  continue 
                                residue_num = str(resdet.split()[2])
                                
                                #fix this
                                try: atom_name = str(resdet.split()[3])
                                except IndexError: atom_name = ' '
                            else: 
                                residue_num = str(resdet.split()[0])
                                try: atom_name = str(resdet.split()[2])
                                except IndexError:
                                    atom_name = '-'
                            
                            try:
                                if len(res_prev) > 0 and res_prev != residue_num:
                                    self.add_outlier_title_for_residues(outlier_title_ext, 
                                                                     chain, res_prev,
                                                                     mnum=mnum)
                                    outlier_title_ext = outlier_title
                                res_coordinate = tuple(dict_cacoord['1'][chain][residue_num][0])
                                self.residue_coordinates[mid][chain][residue_num] = \
                                                    res_coordinate
                                
                                outlier_title_ext += ':'+atom_name
                                res_prev = residue_num
                            except (KeyError,TypeError) as exc: 
                                flag_pass = 0
                        if flag_pass == 1:
                            try:
                                self.add_outlier_title_for_residues(outlier_title_ext, 
                                                                     chain, residue_num,
                                                                     mnum=mnum)
                            except UnboundLocalError: pass
                    if len(residue_details) == 0: continue
                    list_data.append(residue_details)
            if len(list_data) == 0: continue
            if not mid in self.dict_outlier_details:
                self.dict_outlier_details[mid] = {}
            if not chain in self.dict_outlier_details[mid]:
                self.dict_outlier_details[mid][chain] = {}
            self.dict_outlier_details[mid][chain][measure] = list_data[:]
            del list_data
    def save_outlier_json(self):
        for mid in self.dict_outlier_details:
            try:
                jsonfile = self.dict_jsonfiles[mid]
            except KeyError: continue
            with open(jsonfile,'w') as jf:
                json.dump(self.dict_outlier_details[mid],jf)

    def convert_cootdata_to_dictchain(self,dict_cootdata):
        dict_chain = {}
        for measure in dict_cootdata:
            dict_chain[measure] = {}
            for o in dict_cootdata[measure]:
                chain = o[0]
                try: dict_chain[measure][chain].append(o)
                except KeyError: 
                    dict_chain[measure][chain] = [o]
        return dict_chain

    def get_measures_chain(self,chain):
        list_measures = []
        for mid in self.list_modelids:
            if mid in self.dict_cootdata:
                cootdata = self.dict_cootdata[mid]
                dict_chain = self.convert_cootdata_to_dictchain(cootdata)
                for measure in cootdata:
                    #avoid empty data fields
                    #TODO: check if chain is present
                    if len(self.dict_cootdata[mid][measure]) == 0 or \
                        not chain in dict_chain[measure]: continue
                    
                    if not measure in list_measures:
                        list_measures.append(measure)
                del dict_chain
            lp = self.dict_logparse[mid]
            
            if chain in lp.dict_outliers:
                for measure in lp.dict_outliers[chain]:
                    if len(lp.dict_outliers[chain][measure]) == 0:
                        continue
                    if not measure in list_measures:
                        list_measures.append(measure)
        return list_measures
    
    def add_outlier_title_for_residues(self, outlier_title, chain, residue_num,
                                       mnum=0):
        model = self.list_modelids[mnum]
        if 'outliers' in outlier_title: 
            outlier_title = ' '.join(outlier_title.split()[:-1])
        try: 
            if not outlier_title in self.residue_outliers[model][chain][str(residue_num)]:
                self.residue_outliers[model][chain][str(residue_num)].append(outlier_title)
        except KeyError:
            self.residue_outliers[model][chain][str(residue_num)] = [outlier_title]
                        

    def set_local_results(self):
        #local_tab = 'local_tab'
        #local_tab_name = "Local Results"
        outlier_settings = residue_outlier_settings()
        #pyrvapi.rvapi_add_tab( local_tab, local_tab_name, True)
        count_chain = 0
        for chain in self.list_chains:
            chain_section = chain
            chain_name = 'chain '+chain
            #skip chain tab if outliers not found
            if hasattr(self, 'dict_outlier_details'):
                if len(self.dict_outlier_details) > 0 and \
                        not self.check_chain_outliers(chain): continue
            if self.add_chain_sections:
                pyrvapi.rvapi_add_section(chain_section, chain_name, 
                                      local_tab, 0, 0, 1, 1, False)
#             molprobity_subsection = "Molprobity"+str(chain)
#             add_subsection(chain_section,molprobity_subsection,'Molprobity')
            self.set_outlier_sections_tables(chain_section,
                                                 outlier_settings)

            count_chain += 1
        pyrvapi.rvapi_flush()
        self.delete_dictresults()
        self.delete_cootdata()

    def check_chain_outliers(self,chain):
        for mid in self.dict_outlier_details:
            if chain in self.dict_outlier_details[mid]:
                return True
        return False

    def delete_cootdata(self):
        self.dict_cootdata.clear()
        del self.dict_cootdata
    
    def delete_dictresults(self):
        self.dict_outlier_details.clear()
        del self.dict_outlier_details
        
    def set_outlier_sections_tables(self,chain,
                                    outlier_settings):
        list_measures = self.get_measures_chain(chain)
        for measure in list_measures:
            outlier_subsection = \
                    "Molprobity"+''.join(measure.split())+str(chain)
            if not measure in outlier_settings.data_titles: continue
            outlier_subsection_name = "Molprobity: "+ \
                                    outlier_settings.data_titles[measure]
            add_subsection(chain,outlier_subsection,
                       outlier_subsection_name)
            for mid in self.list_modelids:
                table_id = ''.join(outlier_settings.data_titles[measure].split()) + \
                                    str(chain)+mid
                table_name = mid
                        
                add_table_to_section(outlier_subsection,table_id,table_name,show=True)
                add_horz_headers_to_table(table_id,outlier_settings.data_headers[measure],
                                                outlier_settings.data_tooltips[measure])
                self.add_outlier_data_to_table(table_id,chain,mid,measure)
#             for mid in self.list_modelids:
#                 table_id = ''.join(outlier_settings.data_titles[measure].split()) + \
#                             str(chain)+mid
#                 table_name = mid
#                 add_table_to_section(outlier_subsection,table_id,table_name,show=True)
#                 add_horz_headers_to_table(table_id,outlier_settings.data_headers[measure],
#                                             outlier_settings.data_tooltips[measure])
#                 list_expanded_headers, list_expanded_header_tips, \
#                         list_expanded_header_colors = \
#                     self.expand_table_header_models(outlier_settings.data_headers[measure],
#                                            outlier_settings.data_tooltips[measure])
#                 add_horz_headers_to_table(table_id,list_expanded_headers, list_expanded_header_tips)
#                 for lc in xrange(len(list_expanded_header_colors)):
#                     color = list_expanded_header_colors[lc]
#                     customize_table_cell(table_id,0,lc,cell_color=color)
    
    def expand_table_header_models(self, list_headers, list_header_tooltips):
        list_expanded_headers = []
        list_expanded_header_tips = []
        list_expanded_header_colors = []
        for mid in self.list_modelids:
            ct_h = 0
            for h in xrange(len(list_headers)):
                header = list_headers[h]
                header += "<br>({})".format(mid)
                list_expanded_headers.append(header)
                list_expanded_header_tips.append(list_header_tooltips[h])
                list_expanded_header_colors.append('table-'+
                                                   list_cell_colors[ct_h%len(list_cell_colors)]+
                                                   '-hh')
            ct_h += 1
        return list_expanded_headers, list_expanded_header_tips, \
                list_expanded_header_colors

    def add_outlier_data_to_table(self,table_id,chain,mid,measure):
        if mid in self.dict_outlier_details:
            if chain in self.dict_outlier_details[mid]:
                if measure in self.dict_outlier_details[mid][chain]:
                    list_data = self.dict_outlier_details[mid][chain][measure]
                    add_data_to_table(table_id,list_data)
                    
        
    def add_local_tabs_chains(self,list_chains):
        for chain in list_chains:
            pyrvapi.rvapi_add_tab(chain, "chain: "+chain, 
                                  True)

class global_summary_settings():
    #NOTE: data_keys has same keywords as in molprobity log
    #change this with log updates
    data_keys = ['Ramachandran outliers','favored','Rotamer outliers',
                'C-beta deviations','Clashscore','MolProbity score',
                'Cis-proline','Cis-general']#,'Twisted Proline','Twisted General']
    dict_referencerange = {
            'Ramachandran outliers': "< 0.05%",
            'favored': "> 98%",
            'Rotamer outliers': "< 0.3%",
            'C-beta deviations': "0",
            'Clashscore' : ' ',
            'MolProbity score' : ' ',
            'Cis-proline' : "0%",
            'Cis-general' : "0%"
            #'Twisted Proline' : "0%",
            #'Twisted General' : "0%"
            }
    data_titles = ['Ramachandran outliers',
                        'Ramachandran favored',
                        'Rotamer outliers',
                        'C-beta deviations',
                        'Clashscore',
                        'Molprobity score',
                        'Cis-proline',
                        'Cis-general']
                        #'Twisted Proline',
                       #'Twisted General']
    list_horz_headers = ['Outliers']
    list_horz_header_tooltips = ['Calculated outlier counts']
    list_vert_headers = ['Ramachandran outliers',
                        'Ramachandran favored',
                        'Rotamer outliers',
                        'C-beta deviations',
                        'Clashscore',
                        'Molprobity score',
                        'Cis-proline',
                        'Cis-general']
                        #'Twisted Proline',
                       #'Twisted General']
    list_vert_header_tooltips = ['Percent of ramachandran phi/psi outliers',
                                 'Percent of ramachandran favoured phi/psi',
                                 '''Percent of side chains outside 
                                        standard rotamer ranges''',
                                '''Number of C-beta deviations''',
                                '''Score for atom clashes''',
                                '''Overall molprobity quality score''',
                                'Cis-proline',
                                'Cis-general',
                                'Twisted Proline',
                                'Twisted General']

def get_gemmi_object(model_path):
    structure = gemmi.read_structure(model_path)
    return structure

def get_coordinates(structure,chainID,resnum):
    list_coord = []
    for model in structure:
        for chain in model:
            if chain.name != chainID: continue
            polymer = chain.get_polymer()
            #skip non polymers
            if not polymer: continue
            for residue in chain:
                if residue.seqid.num != resnum: continue
                residue_id = str(residue.seqid.num)+'_'+residue.name
                for atom in residue:
                    if atom.name == 'CA':
                        coord = atom.pos #gemmi Position
                        return (coord.x,coord.y,coord.z)
                return None


class residue_outlier_settings() :
    #NOTE: data_keys has same keywords as in molprobity coot file
    #change this with updates
    data_keys = [ "rama", "rota", "cbeta", "probe" , "Bond lengths", "Bond angles",
                 "AsnGlnHis flips", "Sugar pucker", "Backbone torsion suites"]
    data_titles = { "rama"  : "Ramachandran outliers",
                  "rota"  : "Rotamer outliers",
                  "cbeta" : "C-beta outliers",
                  "probe" : "Atom clashes",
                  "Bond lengths" : "Bond length outliers",
                  "Bond angles" : "Bond angle outliers",
                  "Dihedral angles" : "Dihedral angle outliers",
                  "Chiral Volumes" : "Chiral volume outliers",
                  "Planar groups" : "Planar group outliers",
                  "AsnGlnHis flips" : "Asn/Gln/His flips",
                  "Sugar pucker" : "Sugar pucker",
                  "Backbone torsion suites":"Backbone torsion suites"}
    data_headers = { "rama"  : ["Residue", "Name", "Score (%)"],
                 "rota"  : ["Residue", "Name", "Score (%)"],
                 "cbeta" : ["Residue", "Name", "Deviation "+angstrom_label],
                 "probe" : ["Atom 1", "Atom 2", "Overlap"] ,
                 "Bond lengths" : ["atoms","deviation"],
                  "Bond angles" : ["atoms", "deviation"],
                  "Dihedral angles" : ["atoms", "deviation"],
                  "Planar groups": ["atoms",
                                    "deviation"],
                  "Sugar pucker" : ["residues","delta","outlier","epsilon",
                                    "outlier"],
                  "Backbone torsion suites": ["suite ID",
                                              "triaged angle"]}
    data_tooltips = {
                    "rama"  : ["Residues outside allowed Ramachandran bounds",
                               "Residue names",
                               "phi/psi probability (%)"],
                  "rota"  : ["Residues not among probable/observed rotamer types",
                             "Residue names",
                             "Rotamer probablility (%)"],
                  "cbeta" : ["C-beta outliers",
                             "Residue names",
                             "Deviation from standard geometry"],
                  "probe" : ["", "", "Steric overlap"],
                  "Bond lengths" : ["atoms", "number of standard deviations from ideal"],
                  "Bond angles" : ["atoms", " number of standard deviations from ideal"],
                  "Dihedral angles" : ["atoms", "deviation from ideal geometry"],
                  "Chiral Volumes" : """Outliers from the expected 
                                        chiral volume around atom ( 
                                        for Ca it is 2.6 or -2.6"""+ \
                                        angstrom_label+"3 for L or D conformer",
                  "Planar groups" : ["atoms",
                                    "deviation from planarity"],
                  "AsnGlnHis flips" : "AsnGlnHis flips",
                  "Sugar pucker": ["residues","delta torsion (C5'-C4'-C3'-C2')",
                                   "outlier?","epsilon torsion (C4'-C3'-O3'-Pi+1)",
                                   "outlier?"],
                  "Backbone torsion suites": ["Suite","triaged torsion angle"]
                  }