import sys
import re
import os
from collections import OrderedDict
import json


class MolprobityLogParser(object):
    '''
    Parser for Molprobity stdout
    '''
    def __init__(self,stdout):
        self.stdout = stdout
    
        self.dict_summary = OrderedDict()
        self.dict_outliers = OrderedDict()
        self.list_chains = []
        self.dict_outliers_headers = {}
        self.summary_start = False
        self.outlier_start = False
        value = None
        stdout_fh = open(self.stdout,'r')
        for line in stdout_fh:
            self.is_summary(line)
            self.is_outlier(line)
            self.get_summary(line[:-1])
            self.get_chains(line[:-1])
#                        ----------Bond lengths----------                       
            if '------' in line:
                value = line.strip()
                value = value.strip('-')

            if self.outlier_start and value is not None:
                if len(line) > 0: 
                    self.get_outliers(line,value)
        stdout_fh.close()
    def get_summary(self, line):
        '''
        Get global molprobity statistics from log
        NOTE that the pattern search is based on the log output
        '''
        self.list_summary_flags = ['Ramachandran outliers','favored','Rotamer outliers',
                                   'C-beta deviations','Clashscore','MolProbity score']
        self.list_others = ['Cis-proline','Cis-general']#,'Twisted Proline','Twisted General']
        for p in self.list_summary_flags:
            if p in line and '=' in line:
                if "(percentile:" in line:
                    try:
                        line_split1 = line.split('=')[1]
                        percentile_val = line.split("(percentile:")[1]
                        percentile_val = percentile_val.split()[0].strip()
                        if percentile_val[-1] == ')': 
                            percentile_val = percentile_val[:-1]
                        self.dict_summary[p] = [line_split1.split()[0]+
                                                "<br>(percentile:{})".format(percentile_val)]
                    except IndexError: pass
                else:
                    try:
                        self.dict_summary[p] = [line.split('=')[-1].strip()]
                    except IndexError: pass
        for p in self.list_others:
            if p in line and ':' in line:
                try:
                    outlier_percent = line.split(':')[-1].strip()
                    self.dict_summary[p] = [float(outlier_percent.split()[0])]
                except IndexError: pass
                  
#         if 'outliers present' in line:
#             try:
#                 self.dict_summary[line.split()[1]+' outliers'] = line.split()[0]
#             except IndexError: pass
    def is_summary(self,line):
        if line[0] == '=' and line.split()[1] == 'Summary':
            self.summary_start = True
            self.outlier_start = False
        try:
            if line.split()[0] == 'molprobity' and \
                line.split()[-1] == 'statistics.':
                self.summary_start = True
        except IndexError: pass
        try:
            if line.split()[0] == 'Results' or \
                line.split()[0] == 'Refinement':
                self.summary_start = False
        except IndexError: pass
        
    def is_outlier(self,line):
        if line[0] == '=' and line.split()[1] == 'Geometry':
            self.outlier_start = True
        if '------' in line:
            self.summary_start = False
    
    def get_outliers(self,line,measure):
        list_headers = ['atoms','residue','Suite']
        try:
            if line.split()[0] in list_headers:
                self.dict_outliers_headers[measure] = \
                                            line.strip().split()
            if line[2] != ' ': return
            try:
                #is chain ID always one letter?
                if 'Suite' in self.dict_outliers_headers[measure]:
                    chainID = line[7]
                else:
                    chainID = line[3]
            except KeyError: return
            
                
            if chainID.isalpha() or chainID.isdigit():
                if not chainID in self.dict_outliers:
                    self.dict_outliers[chainID] = {}
            
                if not self.dict_outliers[chainID].has_key(measure): 
                    self.dict_outliers[chainID][measure] = [[]]
            
                if self.dict_outliers_headers[measure][0] == 'atoms':
                    #   A 108  GLU  N           1.33     1.18     0.15  1.40e-02  1.20e+02  10.9*sigma
                    # 108  GLU  N
                    atom_id = line[:19].strip()[1:] #ignore chainID
                    #108 GLU N
                    atom_id = ' '.join(atom_id.split())
                    try:
                        #first element = atoms part of the outlier
                        self.dict_outliers[chainID][measure][-1][0] += "<br>" + atom_id
                    except IndexError: self.dict_outliers[chainID][measure][-1].append(atom_id)
                    try: 
                        #           1.33     1.18     0.15  1.40e-02  1.20e+02  10.9*sigma
                        rest_line = line[19:-1].split()
                        #NOTE: adding only the sigma details for now
                        self.dict_outliers[chainID][measure][-1].append(rest_line[-1])
                        if 'sigma' in rest_line[-1]: 
                            self.dict_outliers[chainID][measure].append([])
                    except IndexError: 
                        pass
                elif self.dict_outliers_headers[measure][0] == 'residue':
                    if not line[:3] == '   ': return
                    #prot/nuc acid residue outliers
                    try: 
                        residue_id = line[:15].strip()[1:] #ignore chainID
                        residue_id = ' '.join(residue_id.split())
                    except IndexError: return
                    self.dict_outliers[chainID][measure] = [[residue_id]]
                    try: 
                        rest_line_str = line[15:-1].strip()
                        rest_line = rest_line_str.split()
                        self.dict_outliers[chainID][measure][-1].extend(
                            rest_line)
                    except IndexError: 
                        pass
                elif self.dict_outliers_headers[measure][0] == 'Suite':
                    if not line[:3] == '   ': return
                    #nuc acid residue outliers
                    try: 
                        residue_id = line[:15].rstrip()
                        atom_name = residue_id[13:].strip()
                        if len(atom_name) == 0: atom_name = ' '
                        residue_id = ' '.join([residue_id[4:6].strip(),residue_id[6:8].strip(),
                                               residue_id[8:13].strip(),atom_name
                                               ])#residue_id.split())
                    except IndexError: return
                    self.dict_outliers[chainID][measure] = [[residue_id]]
                    try: 
                        rest_line_str = line[15:-1].strip()
                        rest_line = [rest_line_str.split()[-1]]
                        self.dict_outliers[chainID][measure][-1].extend(
                            rest_line)
                    except IndexError: 
                        pass
#             if measure == 'Bond lengths':
#                 residue = line[10:13].strip()
#                 if residue in ['A','T','G','C','U'] and 'sigma' in line:
#                     try: self.dict_summary['bonds_na'] += 1
#                     except KeyError: self.dict_summary['bonds_na'] = 1
#                 elif 'sigma' in line:
#                     try: self.dict_summary['bonds_prot'] += 1
#                     except KeyError: self.dict_summary['bonds_prot'] = 1
#             elif measure == 'Bond angles':
#                 residue = line[10:13].strip()
#                 if residue in ['A','T','G','C','U'] and 'sigma' in line:
#                     try: self.dict_summary['angles_na'] += 1
#                     except KeyError: self.dict_summary['angles_na'] = 1
#                 elif 'sigma' in line:
#                     try: self.dict_summary['angles_prot'] += 1
#                     except KeyError: self.dict_summary['angles_prot'] = 1
        except IndexError: return

    def get_chains(self,line):
        if "Chain:" in line:
            chainID = line.split()[1]
            if chainID[0] == '"':
                try: 
                    chainID = chainID[1:-1]
                    if not chainID in self.list_chains:
                        self.list_chains.append(chainID)
                except IndexError: pass
