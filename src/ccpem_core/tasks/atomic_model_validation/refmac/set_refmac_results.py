import sys
import re
import os
import time
import pyrvapi
import fileinput
from collections import OrderedDict
import shutil
import numpy as np
import pandas as pd
import math
from ccpem_core.tasks.atomic_model_validation.pyrvapi_utils import *
from refmac_results_parser import RefmacRefineResultsParser_Nautilus

global_tab = 'global_tab'
local_tab = 'local_tab'
global_tab_name = 'Results (Global)'
local_tab_name = 'Results (Local)'
global_reference_tab = 'global_reference_tab'
global_reference_tab_name = 'References (Global)'
local_reference_tab = 'local_reference_tab'
local_reference_tab_name = 'References (Local)'

class SetRefmacResults():
    def __init__(self, refmac_process,list_chains=None,
                 map_resolution=None,
                 glob_reportfile=None):
        self.refmac_process = sorted(refmac_process, 
                                     key=lambda j: j.name.split(' ')[-1][-1]+
                                                    j.name.split(' ')[0][-1])
        self.refmac_results = [RefmacRefineResultsParser_Nautilus(refmac_job.stdout)
                               for refmac_job in refmac_process]
        self.list_modelids = [refmac_job.name.split(' ')[-1] for refmac_job in refmac_process]
        self.map_resolution = map_resolution
        self.glob_reportfile = glob_reportfile
        if glob_reportfile is not None:
            glob_reportfile.write('*REFMAC5* model-map FSCavg\n')
            glob_reportfile.write('==========================\n')
        self.set_global_results()
        if glob_reportfile is not None: 
            glob_reportfile.write('--------------------------\n\n')

    def set_global_results(self):
        pipeline_tab = 'global_tab'
        fsc_section_id = 'refmac_global_section'
        #pyrvapi.rvapi_add_tab( pipeline_tab, tab_name, True)
        pyrvapi.rvapi_add_section(fsc_section_id, 'Model-Map FSC', pipeline_tab, 
                                  0, 0, 1, 1, False)
        fscavg_section_id = "refmac_fscavg_section"
        pyrvapi.rvapi_add_section(fscavg_section_id, 'Refmac FSC average', global_tab, 
                                      0, 0, 1, 1, False)
        table_id = "refmac_fscavg"
        table_name = ""
        
        self.set_fscavg_table(fscavg_section_id, table_id)
        
        lists_resolution = []
        lists_fsc = []
        lists_nref = []
        lists_names = []
        dict_modelplots = OrderedDict()
        dict_mapplots = OrderedDict()
        ct_job = 0
        for refmac_result in self.refmac_results:
            list_resolution, list_fsc, list_nref = \
                self.set_individual_results(fsc_section_id,refmac_result)
                
            modelid = self.list_modelids[ct_job]
            if self.glob_reportfile is not None:
                self.glob_reportfile.write('>{}\n'.format(modelid))
            pdbid = ':'.join(modelid.split(':')[1:])
            mapid = modelid.split(':')[0]
            
            list_fscavgs = self.calculate_fscavg(refmac_result, list_resolution, 
                                                 list_fsc, list_nref)
            if self.glob_reportfile is not None:
                self.glob_reportfile.write('-FSCavg: {}\n'.format(list_fscavgs[0]))
            list_fscavgs = [[fscavg] for fscavg in list_fscavgs]
            add_data_to_table(table_id,list_fscavgs,start_col=ct_job)
            
            try:
                dict_mapplots[mapid][0].append(list_resolution)
                dict_mapplots[mapid][1].append(list_fsc)
                dict_mapplots[mapid][2].append(modelid)
            except KeyError: 
                dict_mapplots[mapid] = [[list_resolution], [list_fsc], [modelid]]
            try:
                dict_modelplots[pdbid][0].append(list_resolution)
                dict_modelplots[pdbid][1].append(list_fsc)
                dict_modelplots[pdbid][2].append(modelid)
            except KeyError: 
                dict_modelplots[pdbid] = [[list_resolution], [list_fsc], [modelid]]
            ct_job += 1
            
        self.set_fsc_plot(fsc_section_id,'refmacfsc',dict_modelplots, dict_mapplots)

    
    def set_fscavg_table(self,section_id, table_id, table_name=''):
        add_table_to_section(section_id,table_id, table_name)
        list_horz_headers = self.list_modelids
        list_horz_header_tooltips = ['Model id + model number']*len(self.list_modelids)
        list_vert_headers = ['FSC average','FSC average (FSC > 0.5)']
        list_vert_header_tooltips = ['','','']
        add_horz_headers_to_table(table_id,list_horz_headers,
                                  list_horz_header_tooltips)
        add_vert_headers_to_table(table_id,list_vert_headers,
                                  list_vert_header_tooltips)
    def calculate_fscavg(self,refmac_results,list_resolution, 
                         list_fsc, list_nref):
        fsc_avg = refmac_results.refmac_stats['FSC average']
        fscavg_0_5 = self.calculate_fscavg_resrange([list_resolution],[list_fsc],
                                                    [list_nref],
                                  maxfsc=1.0,minfsc=0.5,minres=10.0)[0]
#         fscavg_0_1 = self.calculate_fscavg_resrange([list_resolution],[list_fsc],
#                                                     [list_nref],
#                                   maxfsc=1.0,minfsc=0.143,minres=10.0)[0]
        fscavg_0_5 = "{:.4}".format(round(fscavg_0_5,4))
#         fscavg_0_1 = "{:.4}".format(round(fscavg_0_1,4))
        list_fscavgs = [fsc_avg,fscavg_0_5]
        
#         maxRes = np.amax(list_resolution)
#         minRes = np.amin(list_resolution)
#         frequency_factor = np.power(1/minRes,3)/3.0
#         area_fsc_curve = frequency_factor*float(fsc_avg)
#         effective_resolution = np.power(area_fsc_curve,1/3.)*np.power(np.sqrt(3.),1/3)
#         print 'Effective resolution: ', effective_resolution
        
        return list_fscavgs
    
    def set_individual_results(self,section_id,refmac_results, map_flag ='map'):
        refmac_fsc_table_df = refmac_results.fsc_fom_table
        list_fsc = []
        list_resolution = []
        list_nref = []
        for i in xrange(len(refmac_fsc_table_df['FSCwork'].tolist())):
            nref = refmac_fsc_table_df['NREFall'].tolist()[i]
            try:
                nref = int(nref) # *** seems to occur in this field sometimes at high freq
            except (ValueError,TypeError) as e: continue
            fsc = float(refmac_fsc_table_df['FSCwork'].tolist()[i])
            res = float(refmac_fsc_table_df[angstrom_label].tolist()[i])
            list_fsc.append(fsc)
            list_resolution.append(res)
            list_nref.append(nref)
#         list_fsc = [float(fsc) for fsc in refmac_fsc_table_df['FSCwork'].tolist()]
#         list_resolution = [ float(res) for res in refmac_fsc_table_df[angstrom_label].tolist()]
#         list_nref = [ int(nref) for nref in refmac_fsc_table_df['NREFall'].tolist()]
#         add_text_to_section(section_id,[map_flag+"<br>", 'FSC average: '+str(fsc_avg)+"<br>"])
        return list_resolution, list_fsc, list_nref
    
    def calculate_fscavg_resrange(self,lists_resolution,lists_fsc,lists_nref,
                                  maxfsc=1.0,minfsc=0.0,minres=10.0):
        ct_fsc = 0
        list_fscavg = []
        for list_resln in lists_resolution:
            min_index = 0
            max_index = 0
            list_fsc = lists_fsc[ct_fsc]
            list_nref = lists_nref[ct_fsc]
            for resln in list_resln:
                fsc_val = list_fsc[max_index]
                nref_val = list_nref[max_index]
                if fsc_val > maxfsc: 
                    min_index += 1
                    max_index += 1
                    continue
                if fsc_val < minfsc and resln < minres:
                    break
                max_index += 1
            ct_fsc += 1
            sum_shells = 0
            weighted_fsc_sum = 0.0
            for ind in xrange(min_index,max_index):
                weighted_fsc_sum += float(list_fsc[ind]*list_nref[ind])
                sum_shells += list_nref[ind]
                #print ind+1, list_fsc[ind]
            FSCavg = weighted_fsc_sum/sum_shells
            list_fscavg.append(FSCavg)
        return list_fscavg

    def set_fsc_plot(self,section,section_name,dict_modelplots, dict_mapplots,
                     graphwidgetnum=1):
        graphwidget = 'graphwidget'+`graphwidgetnum`+section_name
        data = 'modelmapfsc'
        dataname = 'FSC curve'
        plotname = 'modelmap fsc'
        pyrvapi.rvapi_add_loggraph(graphwidget, section, 2,0,1,1)
        pyrvapi.rvapi_add_graph_data1(graphwidget+'/'+data,
                                                dataname)
        x_label = 'Resolution'
        y_label = 'FSC'

        ct_plot = 0
        ct_line = 0
        for pdbid in dict_modelplots:
            plot_vals = dict_modelplots[pdbid]
            #print plot_vals
            plotname = pdbid
            self.add_fsc_plot(graphwidget,data,plotname,plot_vals[0],plot_vals[1],
                          lists_names=plot_vals[2], plot_num= ct_plot+1,
                          lnum=ct_line)
            for l in xrange(len(plot_vals[0])):
                self.save_fsc_plot(pdbid,plot_vals[0][l][:],plot_vals[1][l])
                self.save_csv_fsc(pdbid,plot_vals[0][l][:],plot_vals[1][l])
                break
            ct_plot += 1
            ct_line += len(plot_vals[0])
        for mapid in dict_mapplots:
            if len(dict_modelplots) == 1: break
            plot_vals = dict_mapplots[mapid]
            plotname = mapid
            self.add_fsc_plot(graphwidget,data,plotname,plot_vals[0],plot_vals[1],
                          lists_names=plot_vals[2], plot_num= ct_plot+1,
                          lnum=ct_line)
            ct_plot += 1
            ct_line += len(plot_vals[0])

    def add_fsc_plot(self,graphwidget,data,plotname,lists_x,lists_y,
                     lists_names=[], plot_num=1,lnum=0):
        #print 'Add FSC plot line', plotname, plot_num
        x_label = 'Resolution'
        y_label = 'FSC'
        plot_id = 'plot'+str(plot_num)
        #TODO: multiple pdb or map inputs have to be accounted
        for l in xrange(len(lists_x)):
            list_x = lists_x[l]
            list_y = lists_y[l]
            plotlinename = lists_names[l]
            if l == 0:
                add_plot_to_graph(graphwidget,data,plotname,plotlinename,list_x,list_y,
                          x_label, y_label,OriginalXaxisOrder=True,line_fill=False,
                          plot_id=plot_id,lnum=lnum+(l+1))
            else:
                line_color = getattr(pyrvapi, 'RVAPI_COLOR_'+list_line_colors[l-1])
                #fill_color = getattr(pyrvapi, 'RVAPI_COLOR_'+list_fill_colors[l-1])
                add_line_to_plot(graphwidget,data,plotlinename,list_y,
                          ytype='float',lnum=lnum+(l+1),
                          line_color=line_color,
                          line_show='', #RVAPI_LINE_Off
                          line_fill=False,
                          #fill_color=fill_color,
                          plot_id=plot_id)
            pyrvapi.rvapi_flush()

    def save_csv_fsc(self,pdbid, list_res,list_fsc):
        #list_res.reverse()
        try: csvfile = '_'.join(pdbid.split('_')[:-1])+'_fsc.csv'
        except IndexError: csvfile = pdbfile+'_fsc.csv'
        with open(csvfile,'w') as csv:
            csv.write('#resolution,fsc\n')
            for l in xrange(len(list_res)):
                csv.write("{},{}\n".format(list_res[l],list_fsc[l]))
                
    def save_fsc_plot(self,pdbid, list_res,list_fsc):
        #list_res.reverse()
        list_res_str = [str(round(res,3)) for res in list_res]
        #print list_res_str, list_fsc
        try: plot_filename = '_'.join(pdbid.split('_')[:-1])+'_fsc.png'
        except IndexError: plot_filename = pdbfile+'_fsc.png'
        try:
            import matplotlib.pyplot as plot
        except ImportError: return
        try: plot.style.use('ggplot')
        except AttributeError: pass
        try:
            plot.rcParams.update({'font.size': 12})
            plot.rcParams.update({'legend.fontsize': 14})
            #plot.rcParams.update({'xtick.major.pad':25})
        except: pass
#         fig, ax = plot.subplots()
#         fig.tight_layout()
        plot.xlabel('Resolution', fontsize=15)
        plot.ylabel('FSC',fontsize=15)
        plot.plot(list_res_str,list_fsc,'g-',linewidth=3.0,linestyle='-')
        plot.axhline(y=0.5, color='black',linestyle='--') #fsc 0.5
#         # Set the ticks and labels...
#         locs,labs = plot.xticks()
#         step = (max(locs)-min(locs))/10.
#         locs = np.arange(min(locs),max(locs)+step,step)
#         labels = np.round(locs[1:],1)
#         print labels
        plot.xticks(rotation='vertical')
        
        plot.savefig(plot_filename,bbox_inches='tight')
        plot.close()
        
        