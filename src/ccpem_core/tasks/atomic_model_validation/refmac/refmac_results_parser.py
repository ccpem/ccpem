import sys
import re
import os
from collections import OrderedDict
import json
from ccpem_core.ccpem_utils.ccp4_log_parser import smartie
from ccpem_core.data_model import metadata_utils
from ccpem_core.tasks.refmac import refmac_results
import json

class RefmacRefineResultsParser_Nautilus(object):
    '''
    Class to parse refinement results in Buccaneer/Nautilus Pipeline
    Modified version of RefmacRefineResultsParser from refmac_results to include free Rfactor row in summary
    '''
    def __init__(self, stdout):
        self.stdout = stdout
        self.log = smartie.parselog(self.stdout)
        self.refine_results = None
        self.ncyc_table = None
        self.fsc_fom_table = None
        self.weightText = ''

        stdoutIN = open(self.stdout, 'r')

        for line in stdoutIN:
            match = re.search('Weight matrix\s*(.*)', line)
            if match:
                self.weightText = match.group(1)

        # Get refinement prog
        refine_prog = self.log.program(0)
        # Get results summary
        if refine_prog.keytext(-1).name() == 'Result':
            self.refine_results = refine_prog.keytext(-1)
        if self.refine_results is not None:
            self.set_results_summary()

            # Parse table list in reverse order to find final FSC and ncyc table
            for cntr in reversed(xrange(len(refine_prog.tables()))):
                table = refine_prog.tables()[cntr]
                if table.title().find('FSC and  Fom') != -1:
                    if self.fsc_fom_table is None:
                        self.fsc_fom_table = refmac_results.smartie_table_to_meta_data_table(table=table)
                        self.fsc_fom_table.convert_to_resolution_angstrom()
                if table.title().find('Rfactor analysis, stats vs cycle') != -1:
                    if self.ncyc_table is None:
                        self.ncyc_table = refmac_results.smartie_table_to_meta_data_table(table=table)
                # Stop if both table found
                if self.ncyc_table is not None \
                        and self.fsc_fom_table is not None:
                    break
        #self.print_summary()

    def set_results_summary(self):
        self.refmac_stats = self.get_initial_full_refine_stats()

    def get_initial_full_refine_stats(self):
        res = self.refine_results.message().split()
        i_res = OrderedDict()
        i_res['FSC average'] = '-'
        i_res['R factor'] = res[4]
        i_res['R free'] = ''
        i_res['Rms bond'] = res[8]
        i_res['Rms angle'] = res[12]
        i_res['Rms chiral'] = res[16]
        # get overall FSC
        i_res['FSC average'] = self.find_fsc_average_at_freerfactor(r_string=i_res['R free'])
        return i_res

    def find_fsc_average_at_freerfactor(self, r_string, reverse=False):
        #find starting fsc
        search_file = open(self.stdout, 'r').read()
        r_line = 'Average Fourier shell correlation    ='
        if reverse:
            find = search_file.rfind(r_line)
        else:
            find = search_file.find(r_line)
        fsc_line = search_file[find:find+49]
        fsc_line_split = fsc_line.split()
        
        fsc_average = fsc_line_split[fsc_line_split.index('=')+1]
        return fsc_average

    def print_summary(self):
        print self.refine_results.message()
        print self.fsc_fom_table.to_string()
        print self.ncyc_table.to_string()
