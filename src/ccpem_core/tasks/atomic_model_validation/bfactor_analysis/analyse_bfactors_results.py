import sys
import re
import os
import time
import pyrvapi
import fileinput
from collections import OrderedDict
import shutil
import numpy as np
import math
import json
from ccpem_core.tasks.atomic_model_validation.pyrvapi_utils import *
from scipy.signal import argrelextrema

global_tab = 'global_tab'
local_tab = 'local_tab'
global_tab_name = 'Results (Global)'
local_tab_name = 'Results (Local)'
global_reference_tab = 'global_reference_tab'
global_reference_tab_name = 'References (Global)'
local_reference_tab = 'local_reference_tab'
local_reference_tab_name = 'References (Local)'

class SetBfactResults():
    def __init__(self, bfact_process,list_chains=None,
                 cootset_instance=None,
                 glob_reportfile=None):
        self.bfact_process = sorted(bfact_process, 
                                     key=lambda j: j.name.split(' ')[-1][-1]+
                                                    j.name.split(' ')[0][-1])
        self.list_modelids = [] 
        self.list_chains = []
        self.dict_json_dist = OrderedDict()
        self.dict_json_dev = OrderedDict()
        for bfact_job in bfact_process:
            bfact_dist = os.path.join(bfact_job.location,'bfact.json')
            #bfact_dev = os.path.join(bfact_job.location,'bfact_deviation.json')

            modelid = bfact_job.name.split(' ')[-1]
            self.dict_json_dist[modelid] = bfact_dist
            #self.dict_json_dev[modelid] = bfact_dev
            self.list_modelids.append(modelid)
        self.glob_reportfile = glob_reportfile
        if glob_reportfile is not None:
            glob_reportfile.write('Atomic B-factor distribution\n')
            glob_reportfile.write('============================\n')
        self.set_global_results()
        if glob_reportfile is not None: 
            glob_reportfile.write('----------------------------\n\n')
        self.add_chain_sections = False
        if len(self.list_chains) == 0:
            self.add_chain_sections = True
        #self.set_local_results()

    def set_global_results(self):
        pipeline_tab = 'global_tab'
        bfact_section_id = 'bfact_global_section'
        #pyrvapi.rvapi_add_tab( pipeline_tab, tab_name, True)
        pyrvapi.rvapi_add_section(bfact_section_id, 'Atomic B-factor distribution', pipeline_tab, 
                                  0, 0, 1, 1, False)
        graphwidget = 'graphwidget'+`1`
        data = 'bfactdist'
        dataname = 'B-factor distribution'
        plotname = 'B-factor distribution'
        pyrvapi.rvapi_add_loggraph(graphwidget, bfact_section_id, 1,0,1,1)
        pyrvapi.rvapi_add_graph_data1(graphwidget+'/'+data,
                                                dataname)
        x_label = 'Atomic B-factor'
        y_label = 'Number of residues'

        dict_highlight_issues = OrderedDict()
        ct_plot = 0
        for modelid in self.dict_json_dist:
            jsonfile = self.dict_json_dist[modelid]
            with open(jsonfile,'r') as jf:
                dict_dist = json.load(jf)
            list_bfact = []
            for chain in dict_dist:
                for res_id in dict_dist[chain]:
                    list_bfact.append(dict_dist[chain][res_id])
            try:
                bfact_freq,bfact_val = np.histogram(list_bfact,min(len(list_bfact),10))
                
            except: continue
                        
            if self.glob_reportfile is not None:
                self.glob_reportfile.write('>{}\n'.format(modelid))
            #print bfact_freq,bfact_val
            #check for multiple peaks
            local_peaks = argrelextrema(bfact_freq, np.greater,order=3)
            list_peaks = []
            single_bval_frac = float(np.amax(bfact_freq))/np.sum(bfact_freq)
            #print np.amax(bfact_freq), single_bval_frac
            #print local_peaks
            for peak in local_peaks[0]: 
                if single_bval_frac < 0.95 and peak > 10:
                    list_peaks.append(peak)
            bfact_val_centred = [round((bfact_val[l]+bfact_val[l+1])/2.,2) for l in xrange(len(bfact_val)-1)]
            
            if single_bval_frac > 0.95:
                bfact_val_centred = bfact_val[:-2]+[round((bfact_val[-2]+bfact_val[-1])/2.,2)]
                list_peaks = []
                issue_line = modelid+": Warning, single value or a narrow peak!.<br> Check if atomic b-factors are refined?"
                print_line = "<h3 style='color:orange'>{0}</h3>".format(issue_line)
                dict_highlight_issues[modelid] = print_line
                
                if self.glob_reportfile is not None:
                    self.glob_reportfile.write('-Warning, single value or a narrow peak!\n')
            elif bfact_freq[0] > bfact_freq[1] and bfact_freq[0] > bfact_freq[2]:
                if not 0 in list_peaks and bfact_freq[0] > 10: list_peaks.append(0)
            elif bfact_freq[1] > bfact_freq[0] and \
                bfact_freq[1] > bfact_freq[2] and bfact_freq[1] > bfact_freq[3]:
                if not 1 in list_peaks and bfact_freq[1] > 10: list_peaks.append(1)
            if single_bval_frac > 0.95:
                list_peaks = []
            elif bfact_freq[-1] > bfact_freq[-2] and bfact_freq[-1] > bfact_freq[-3]:
                if not len(bfact_freq)-1 in list_peaks and bfact_freq[-1] > 10: list_peaks.append(len(bfact_freq)-1)
            elif bfact_freq[-2] > bfact_freq[-1] and \
                bfact_freq[-2] > bfact_freq[-3] and bfact_freq[-2] > bfact_freq[-4]:
                if not len(bfact_freq)-2 in list_peaks and bfact_freq[-2] > 10:list_peaks.append(len(bfact_freq)-2)
            #print list_peaks
            
            if len(list_peaks) > 1:
                issue_line = modelid+": Warning, multiple peaks detected!"
                print_line = "<h3 style='color:orange'>{0}</h3>".format(issue_line)
                dict_highlight_issues[modelid] = print_line
                
                if self.glob_reportfile is not None:
                    self.glob_reportfile.write('-Warning, multiple peaks detected!\n')
                    
            plotname = modelid
            plotlinename = modelid
            ct_plot += 1
            plot_id = 'plot'+modelid+str(ct_plot)
            add_plot_to_graph(graphwidget,data,plotname,plotlinename,bfact_val_centred,bfact_freq,
                          x_label, y_label,OriginalXaxisOrder=True,line_fill=False,
                          plot_id=plot_id,lnum=ct_plot)
            pyrvapi.rvapi_flush()
        #highlight potential issues
        ct_id = 0
        for modelid in dict_highlight_issues:
            pyrvapi.rvapi_add_text(dict_highlight_issues[modelid], bfact_section_id, 3+ct_id, 0, 1, 1)
            ct_id += 1
    
    def set_local_results(self):
        x_label = 'Residue'
        y_label = 'Bfactor deviation'
        self.list_local_subsection = []
        ct_plot = 0
        for modelid in self.dict_json_dev:
            jsonfile = self.dict_json_dev[modelid]
            with open(jsonfile,'r') as jf:
                dict_dist = json.load(jf)
            ct_line = 0
            for dist in dict_dist:
                list_bfact_dev = []
                list_res = []
                print dist
                for chain in dict_dist[dist]:
                    #add chain sections
                    if self.add_chain_sections:
                        if not chain in self.list_chains:
                            self.list_chains.append(chain)
                            chain_section = chain
                            chain_name = 'chain '+chain
                            self.add_section_chain(chain_section, chain_name)
                            
                    subsection_name = 'bfactor'+chain
                    #add section for plot
                    if not chain in self.list_local_subsection:
                        self.add_subsection_to_chain(chain,subsection_name)
                        self.list_local_subsection.append(chain)
                    for res_id in dict_dist[dist][chain]:
                        bfact_dev = dict_dist[dist][chain][res_id]
                        list_bfact_dev.append(bfact_dev)
                        try: res_num = int(res_id.split('_')[0])
                        except TypeError: continue
                        list_res.append(res_num)
                    if ct_line == 0:
                        print 'Adding plot', modelid, chain, dist
                        pyrvapi.rvapi_add_text(modelid+":<br>", subsection_name, ct_plot, 0, 1, 1)
                        graphwidget = 'graphWidget'+str(ct_plot)+subsection_name
                        data = 'bfactdata'+str(ct_plot)+subsection_name
                        dataname = 'Bfact deviation'+str(ct_plot)+subsection_name
                        pyrvapi.rvapi_add_graph(graphwidget, subsection_name, ct_plot, 0, 1, 1)
                        #pyrvapi.rvapi_add_loggraph(graphwidget, section, 0, 0, 1, 1)
                        pyrvapi.rvapi_add_graph_data1(graphwidget+'/'+data,
                                                            dataname)
                        plotname = modelid
                        plot_id = 'plot'+str(ct_plot)+subsection_name
                        plotlinename = str(dist)
                        add_plot_to_graph(graphwidget,data,plotname,
                                          plotlinename,list_res,list_bfact_dev,
                                          x_label, y_label,
                                          plot_id=plot_id,
                                          xtype='int',lnum=ct_line+1)
                    else:
                        plotlinename = str(dist)
                        print 'add line ', plotlinename, modelid, chain
                        line_color = getattr(pyrvapi, 'RVAPI_COLOR_'+list_line_colors[ct_line-1])
                        add_line_to_plot(graphwidget,data,plotlinename,list_bfact_dev,
                                         list_x=list_res,x_label=x_label,
                                          ytype='float',
                                          plot_id=plot_id,
                                          lnum=ct_line+1,
                                          line_color=line_color)
                ct_line += 1
            ct_plot += 1
            pyrvapi.rvapi_flush()

    def add_section_chain(self, chain_section, chain_name):
        pyrvapi.rvapi_add_section(chain_section, chain_name, 
                                          local_tab, 0, 0, 1, 1, False)
    def add_subsection_to_chain(self,chain,subsection):
        pyrvapi.rvapi_add_section1(chain+'/'+subsection, 
                               'Bfactor deviation from neighboring residues', 
                               0, 0, 1, 1, False)

    