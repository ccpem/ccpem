#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
import json, shutil
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.tasks.tempy.smoc.smoc_task import SMOC
from ccpem_core.tasks.tempy.difference_map import difference_map_task
from ccpem_core.tasks.tempy.difference_map import difference_map
from ccpem_core.tasks.tempy.scores.scores_task import GlobScore
from ccpem_core.tasks.refmac.refmac_task import RefmacMapToMtz, RefmacRefine, \
    RefmacSfcalcCrd, SetModelCell, RefmacRefineServal, LibgRestraints
from ccpem_core.tasks.bin_wrappers import fft
from ccpem_core.tasks.tempy.smoc import smoc_process
from ccpem_core.tasks.tempy.scores import scores_process
import FDRcontrol
from ccpem_progs.fdr_validation import val_pdb_python
from ccpem_core.tasks.atomic_model_validation.bfactor_analysis import analyse_bfactors
from ccpem_core.tasks.model2map.model2map_task import mapprocess_wrapper, \
                sf2mapWrapper, fix_map_model_refmac
from ccpem_core.tasks.jpred.jpred_task import JPredRunWrapper, JPredDownloadWrapper
from ccpem_core.tasks.atomic_model_validation import validate_results
from ccpem_core.process_manager import job_register
from ccpem_core import settings
from run_validation_tasks import *
from ccpem_progs.jpred import jpred_submit
from ccpem_core.tasks.jpred import jpred_download
import mrcfile, numpy as np
from ccpem_core.model_tools import command_line
from ccpem_core.model_tools.gemmi_utils import shift_coordinates, remove_atomic_charges, \
                            convert_mmcif_to_pdb
from ccpem_core.map_tools.TEMPy import map_preprocess \
                            , attribute_map_values
from piscore import run_piscore_wc

# Conversions between GUI labels and Max's argument values
METHODS = {
    'FDR-BY': 'BY',
    'FDR-BH': 'BH',
    'FWER-Holm': 'Holm',
    'FWER-Hochberg': 'Hochberg'
}
TEST_PROCS = {
    'Left-sided': 'leftSided',
    'Right-sided': 'rightSided',
    'Two-sided': 'twoSided'
}
class ValidateTask(task_utils.CCPEMTask):
    '''
    Atomic model validation
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='Model validation',
        author='Williams C et al., Brown A et al., Nicholls R et al., Cragnolini T et al., Joseph et al',
        version='',
        description=(
            '''Validate protein atomic model quality and agreement with map data.<br><br>
            Reports outliers based on 
            Stereochemistry:  (Molprobity),<br>
            C-alpha geometry/secondary structure: (CaBLAM),<br>
            Subunit interface quality: PI-score, <br>
            Local fit of atomic model in density: (TEMPy SMOC),<br>
            Backbone trace: (FDR validation), <br>
            Global density fit (Refmac Model-map FSC, TEMPy Scores)<br>
            References:<br>
            <a href="https://onlinelibrary.wiley.com/doi/10.1002/pro.3330"> 
            Williams et al, 2017 </a>:Molprobity<br>
            <a href="http://phenix-online.org/phenixwebsite_static/mainsite/files/newsletter/CCN_2018_07.pdf"> 
            Williams et al, 2018</a>:CaBLAM<br>
            <a href="https://www.nature.com/articles/s41467-021-23692-x?proof=t+target%3D"> 
            Malhotra et al, 2021</a>: PI-score<br>
            <a href="http://scripts.iucr.org/cgi-bin/paper?S2059798320014928"> 
            Cragnolini et al, 2021</a>:TEMPy<br>
            <a href="https://www.sciencedirect.com/science/article/pii/S104620231630041X?via%3Dihub"> 
            Joseph et al, 2016</a>: SMOC<br>
            <a href="http://scripts.iucr.org/cgi-bin/paper?S1399004714021683"> 
            Brown et al, 2015</a>:Refmac<br>
            <br>
            '''),
        short_description=(
            'Evaluate atomic model geometry and density fit'),
        documentation_link=('http://molprobity.biochem.duke.edu/ ',),
                        
        references=('https://onlinelibrary.wiley.com/doi/10.1002/pro.3330',
                    'http://phenix-online.org/phenixwebsite_static/mainsite/files/newsletter/CCN_2018_07.pdf',
                    'https://www.nature.com/articles/s41467-021-23692-x?proof=t+target%3D',
                    'http://scripts.iucr.org/cgi-bin/paper?S2059798320014928',
                    'http://scripts.iucr.org/cgi-bin/paper?S1399004714021683'))

    commands = {'molprobity': settings.which(program='molprobity.molprobity'),
                'reduce': settings.which(program='molprobity.reduce'),
                'cablam': settings.which(program='molprobity.cablam'),
                'ccpem-smoc': ['ccpem-python', os.path.realpath(smoc_process.__file__)],
                'ccpem-scores': ['ccpem-python', os.path.realpath(scores_process.__file__)],
                'ccpem-ribfind': settings.which('ccpem-ribfind'),
                'pdbset':  settings.which(program='pdbset'),
                'ccpem-gemmi':settings.which(program='gemmi'),
                'refmac': settings.which(program='refmac5'),
                'dssp': settings.which('mkdssp'),
                'ccpem-python':  settings.which(program='ccpem-python'),
                'libg': settings.which(program='libg'),
                'ccpem-jpred':
                ['ccpem-python', os.path.realpath(jpred_submit.__file__)],
                'jpred-download':
                ['ccpem-python', os.path.realpath(jpred_download.__file__)],
                'ccpem-mapprocess':
                ['ccpem-python', os.path.realpath(map_preprocess.__file__)],
                'pdbset':  settings.which(program='pdbset'),
                'bfactor':
                ['ccpem-python',os.path.realpath(analyse_bfactors.__file__)],
                'model-tools':
                ['ccpem-python',os.path.realpath(command_line.__file__)],
                'ccpem-diffmap':
                 ['ccpem-python', os.path.realpath(difference_map.__file__)],
                 'ccpem-mapval':
                 ['ccpem-python', 
                        os.path.realpath(attribute_map_values.__file__)],
                 'confidence_maps_python': ['ccpem-python', FDRcontrol.__file__],
                 'fdr_validation_python' : ['ccpem-python', val_pdb_python.__file__],
                'piscore' : ['ccpem-python',run_piscore_wc.__file__]
                }

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        #
        super(ValidateTask, self).__init__(
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            parent=parent)
        
        #self.smoc_task = SMOC()
    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-pdb_path',
            '--pdb_path',
            help=('Input pdb'),
            metavar='Input atomic model',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-input_pdbs',
            '--input_pdbs',
            help=('Input atomic model(s). \n'
                  'Ensure same residue numbering in all. \n'
                  'Ensure atomic B-factors are refined/set (for Refmac) \n'
                  'Currently supports single model only (per file)'),
            metavar='Input atomic model',
            type=str,
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-pdbs_zero',
            '--pdbs_zero',
            help=('Models moved to zero origin, for Refmac'),
            metavar='Fixed atomic models',
            type=str,
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-input_pdb_chains',
            '--input_pdb_chains',
            help='Input PDB chain(s)',
            metavar='Input PDB chain selections',
            type=str,
            nargs='*',
            default=None)
        
        # Refine B-factors
        parser.add_argument(
            '-refine_bfactor',
            '--refine_bfactor',
            help=('Refine atomic B-factors'),
            metavar='Refine atomic B-factors?',
            type=bool,
            default=True)

        # use Refmac to synthesize maps for SMOC and global scores
        parser.add_argument(
            '-use_refmac',
            '--use_refmac',
            help=('Use Refmac to make map from model. \
Ensure atomic B-factors are refined. \
Input map and resolution to activate'),
            metavar='Use Refmac to simulate map from model?',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-sim_maps',
            '--sim_maps',
            help='Synthetic maps from models, to score against map',
            metavar='Input synthetic maps',
            type=str,
            nargs='*',
            default=None)
        #ligand dictionary for Refmac
        parser.add_argument(
            '-lib_in',
            '--lib_in',
            help='Ligand dictionary for Refmac (cif format)',
            metavar='Ligand dictionary',
            type=str,
            default=None)
        #input map
        map_path = parser.add_argument_group()
        map_path.add_argument(
            '-map_path',
            '--map_path',
            help=('Input map (mrc format). \n'
                  'Ensure model is fitted inside map. \n'
                  'Required for evaluating density fits'),
            metavar='Input map',
            type=str,
            default=None)
        map_path.add_argument(
            '-map_path_edit',
            '--map_path_edit',
            help='Edited map (mrc format)',
            metavar='Edited input map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-map_resolution',
            '--map_resolution',
            help=('Resolution of map (Angstrom) \n'
                  'Required for evaluating density fit'),
            metavar='Map resolution',
            type=float,
            default=None)
        
        num_proc = 1
        num_proc = max(self.get_ncpu()-1,num_proc)
        parser.add_argument(
            '-ncpu',
            '--ncpu',
            help= ('Number of cpus to use \n'
                   '(max cpus - 1, by default)'),
            metavar='Number of cpus',
            type=int,
            default=num_proc)
        
        parser.add_argument(
            '-results_parallel',
            '--results_parallel',
            help= ('''Generate results in parallel.\n'''),
            metavar='Generate results in parallel',
            type=bool,
            default=False)
        
        parser.add_argument(
            '-half1',
            '--half_map_1',
            help=('Additional map 1 (mrc format). Can be another map to compare fit against.\
Same dimensions and voxel size as the fullmap recommended'),
            metavar='Additional map 1',
            type=str,
            default=None)
        
        parser.add_argument(
            '-half_map1_edit',
            '--half_map1_edit',
            help=('Half map 1 fixed for origin'),
            metavar='Half map1 fixed',
            type=str,
            default=None)
        
        parser.add_argument(
            '-half2',
            '--half_map_2',
            help=('Additional map 2 (mrc format).Can be another map to compare fit against.\
Same dimensions and voxel size as the fullmap recommended'),
            type=str,
            metavar='Additional map 2',
            default=None)
        
        parser.add_argument(
            '-half_map2_edit',
            '--half_map2_edit',
            help=('Half map 2 fixed for origin'),
            type=str,
            metavar='Half map2 fixed',
            default=None)
        
        parser.add_argument(
            '-modelmap',
            '--modelmap',
            help='Map simulated from model (mrc format)',
            type=str,
            default=None)
        parser.add_argument(
            '-run_molprobity',
            '--run_molprobity',
            help= ('''Check geometry using molprobity'''),
            metavar='Geometry (Molprobity)',
            type=bool,
            default=True)
        parser.add_argument(
            '-run_cablam',
            '--run_cablam',
            help= ('''Check CA geometry using CaBLAM'''),
            metavar='Ca geometry(CaBLAM)',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-run_smoc',
            '--run_smoc',
            help= ('''SMOC: calculate Segment Manders\' Overlap Coefficient  \n''' \
                   ''' Scores residues and outputs per residue plots'''),
            metavar='Local density fit (TEMPy)',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-run_scores',
            '--run_scores',
            help= ('''TEMPy scores: calculate global scores for model fit  \n'''),
            metavar='Global density fit (TEMPy)',
            type=bool,
            default=False)
        
        parser.add_argument(
            '-run_fdr',
            '--run_fdr',
            help= ('''FDR validation: score residue backbone positions based on False Discovery Rate analysis of the map  \n''' \
                   ''' Use unmasked map as input '''),
            metavar='Backbone position (FDR validation)',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-run_piscore',
            '--run_piscore',
            help= ('''PI-score: score interfaces between subunits (chains) of a model'''),
            metavar='Subunit interface (PI-score)',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-run_jpred',
            '--run_jpred',
            help= ('''predict secondary structure from sequence.  \n''' \
                   '''Task run on the Jpred server, may take several minutes'''),
            metavar='SSE prediction (JPred)',
            type=bool,
            default=False)
        parser.add_argument(
            '-run_refmac',
            '--run_refmac',
            help= ('''REFMAC: calculate model-map FSC and FSCavg'''),
            metavar='Model-map FSC (Refmac)',
            type=bool,
            default=False)
        parser.add_argument(
            '-run_refmac_refine',
            '--run_refmac_refine',
            help= ('''refine model using REFMAC prior to validation'''),
            metavar='refine model using Refmac)',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-run_diffmap',
            '--run_diffmap',
            help= ('''TEMPy diffmap: calculate model-map difference\n'''),
            metavar='Difference map (TEMPy)',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-output_dssp',
            '--output_dssp',
            help='File name for dssp output',
            metavar='Output dssp',
            type=str,
            default='output.dssp')
        #set contour
        parser.add_argument(
            '-auto_contour_level',
            '--auto_contour_level',
            help='''Automatically set map contour level. 
                    Unselect to set manually (recommended for masked/segmented maps).''',
            metavar='Auto set contour',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-map_contour_level',
            '--map_contour_level',
            help='''Set map contour level''',
            metavar='Contour level',
            type=float,
            default=None)
        
        parser.add_argument(
            '-prune',
            '--prune',
            help="Prune low confidence areas of the model",
            metavar='Prune model?',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-valCA',
            '--valCA',
            help="Validate model based on the CA positions",
            metavar='Validate CA?',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-fdr_map',
            '--fdr_map',
            help='Input fdr map (mrc format)',
            metavar='Input fdr map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-apix',
            '--apix',
            help='Override pixel size of input map.',
            metavar='Pixel size (angstroms)',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-locResMap',
            '--locResMap',
            help='Input local resolution map (optional).',
            metavar='Local resolution map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-method',
            '--method',
            help=("Method for multiple testing correction. FDR for False Discovery Rate or FWER for Family-Wise Error "
                  "Rate. 'BY' for Benjamini-Yekutieli, 'BH' for Benjamini-Hochberg."),
            metavar='Correction method',
            type=str,
            choices=['FDR-BY',
                     'FDR-BH',
                     'FWER-Holm',
                     'FWER-Hochberg'],
            default='FDR-BY')
        #
        parser.add_argument(
            '-window_size',
            '--window_size',
            help='Size of box for background noise estimation (in pixels).',
            metavar='Noise box size',
            type=int,
            default=None)
        #
        parser.add_argument(
            '-noise_box',
            '--noise_box',
            help='Coordinates of centre of box for noise estimation (in pixels, x y z). Leave blank for default.',
            metavar='Noise box coordinates',
            type=list,
            default=None)
        #
        parser.add_argument(
            '-meanMap',
            '--meanMap',
            help='3D map of noise means to be used for FDR control.',
            metavar='Mean map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-varianceMap',
            '--varianceMap',
            help='3D map of noise variances to be used for FDR control.',
            metavar='Variance map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-test_proc',
            '--test_proc',
            help="Choose between right, left and two-sided testing.",
            metavar='Test procedure',
            type=str,
            choices=['Left-sided',
                     'Right-sided',
                     'Two-sided'],
            default='Right-sided')
        #
        parser.add_argument(
            '-lowPassFilter',
            '--lowPassFilter',
            help="Low-pass filter the map at the given resoultion prior to FDR control.",
            metavar='Low-pass filter',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-ecdf',
            '--ecdf',
            help="Use empirical cumulative distribution function instead of the standard normal distribution.",
            metavar='Use ECDF',
            type=bool,
            default=None)
        #SMOC srguments
        SMOC().add_basic_arguments(parser)
        SMOC().add_advanced_arguments(parser)
        return parser

    def get_ncpu(self):
        try:
            import multiprocessing
            return multiprocessing.cpu_count()
        except (ImportError, NotImplementedError):
            return 1

    def ribfind_job(self, db_inject=None, job_title=None):
        # Set 
        if self.database_path is None:
            path = os.path.dirname(self.job_location)
        else:
            path = os.path.dirname(self.database_path)

        job_id, job_location = job_register.job_register(
            db_inject=db_inject,
            path=path,
            task_name=ribfind_task.Ribfind.task_info.name)

        # Set task args
        args = ribfind_task.Ribfind().args
        args.job_title.value = job_title
        pdbs = self.args.input_pdbs()
        if not isinstance(pdbs, list):
            pdbs = [pdbs]
        args.input_pdb.value = pdbs[0]
        args_json_string = args.output_args_as_json(
            return_string=True)

        # Set process args
        process_args = ['--no-gui']
        process_args += ["--args_string='{0}'".format(
            args_json_string)]
        process_args += ['--job_location={0}'.format(
            job_location)]
        if self.database_path is not None:
            process_args += ['--project_location={0}'.format(
                os.path.dirname(self.database_path))]
        if job_id is not None:
            process_args += ['--job_id={0}'.format(
                job_id)]

        # Create process
        self.ribfind_process = process_manager.CCPEMProcess(
            name='Ribfind auto run task',
            command=self.commands['ccpem-ribfind'],
            args=process_args,
            location=job_location,
            stdin=None)

    def copy_argsfile(self,argsfile, new_argsfile, new_map_path=None,
                      new_pdb_path=None):
        
        with open(argsfile,'r') as inpf:
            json_args = json.load(inpf)
        with open(new_argsfile,'w') as outf:
            json_args['job_location'] = os.path.dirname(new_argsfile)
            #set ribfind task output
            json_args['rigid_body_path'] = self.args.rigid_body_path.value
            if new_map_path is not None:
                json_args['map_path'] = new_map_path
                json_args['map_path_edit'] = new_map_path
            if new_pdb_path is not None:
                json_args['input_pdbs'] = new_pdb_path
            json.dump(json_args,outf)
            
    def edit_argsfile(self,args_file, new_map_path=None,
                      contour_level=None):
        with open(args_file,'r') as f:
            json_args = json.load(f)
            #set ribfind task output
            json_args['rigid_body_path'] = self.args.rigid_body_path.value
            #set mapprocess task output
            if self.mapprocess_task is not None:
                mapprocess_job_location = self.mapprocess_task.job_location
                #the processed map is written to the same dir as input map
                #the job location has other output files
                if mapprocess_job_location is not None:
                    processed_map_path = os.path.splitext(
                                        os.path.abspath(self.args.map_path.value))[0] \
                                        +'_processed.mrc'
                    #print processed_map_path
                    if os.path.isfile(processed_map_path):
                        json_args['map_path'] = processed_map_path
                        self.unprocessed_map = self.args.map_path.value
                        self.args.map_path.value = processed_map_path
            if new_map_path is not None:
                json_args['map_path'] = new_map_path
            #add synthetic maps as an argument
            if self.args.use_refmac.value:
                json_args['map_path_edit'] = self.args.map_path_edit.value
                if new_map_path is not None:
                    json_args['map_path_edit'] = new_map_path
                if len(self.sim_maps) == len(self.list_pdbs):
                    json_args['sim_maps'] = self.sim_maps
            #add user input contour
            #print contour_level
            if contour_level is not None:
                json_args['map_contour_level'] = contour_level
                json_args['auto_contour_level'] = False
            
        with open(args_file,'w') as f:
            json.dump(json_args,f)
    #translate model to fit to shifted map
    def fix_model_for_refmac(self,pdb_path,pdb_edit_path,trans_vector,
                             remove_charges=False):
        #print pdb_path,pdb_edit_path
        shift_coordinates(pdb_path,pdb_edit_path,trans_vector,remove_charges)

    def check_processed_map(self):
        processed_map_path = os.path.splitext(
                            os.path.abspath(self.args.map_path.value))[0] \
                            +'_processed.mrc'
        #print processed_map_path
        if os.path.isfile(processed_map_path) and \
            self.mapprocess_task is not None:
            self.map_input.value_line.setText(processed_map_path)

    def set_model_lists(self):
        #set input pdbs
        self.list_pdbs = self.args.input_pdbs()
        
        if self.list_pdbs == None or len(self.list_pdbs) == 0:
            if self.args.pdb_path.value != None:
                self.list_pdbs = [self.args.pdb_path.value]
        elif not isinstance(self.list_pdbs, list):
            self.list_pdbs = [self.list_pdbs]
        
        #set input maps
        self.list_maps = []
        if self.args.map_path.value != None and not self.args.map_path.value in self.list_maps:
            self.list_maps.append(self.args.map_path.value)
        if self.args.half_map_1.value != None and not self.args.half_map_1.value in self.list_maps:
            self.list_maps.append(self.args.half_map_1.value)
        if self.args.half_map_2.value != None and not self.args.half_map_2.value in self.list_maps:
            self.list_maps.append(self.args.half_map_2.value)
            
    def set_model_ids(self):
        self.set_model_lists()
        #TODO: set mapids from basenames
        #self.list_mapids = ['map0','hm1','hm2']
#         #set map ids
        ct_map = 0
        self.list_mapids = []
        for map in self.list_maps:
            map_basename = os.path.basename(map)
            try: map_basename = '.'.join(map_basename.split('.')[:-1])
            except IndexError: 
                map_basename = map_basename.split('.')[0]
            if len(map_basename) <= 10:
                mapid = map_basename+'_'+\
                    str(ct_map)
            else:
                mapid = map_basename[:5]+'_'+\
                    map_basename[-5:]+'_'+\
                    str(ct_map)
            if not mapid in self.list_mapids:
                self.list_mapids.append(mapid)
            ct_map += 1
        
        self.list_pdbids = []
        #set model ids
        ct_pdb = 0
        for pdb in self.list_pdbs:
            pdb_basename = os.path.basename(pdb)
#             try: pdb_basename = '.'.join(pdb_basename.split('.')[:-1])
#             except IndexError: 
#                 pdb_basename = pdb_basename.split('.')[0]
            pdb_basename = os.path.splitext(pdb_basename)[0]
            if len(pdb_basename) <= 10:
                pdbid = pdb_basename+'_'+\
                    str(ct_pdb)
            else:
                pdbid = pdb_basename[:5]+'_'+\
                    pdb_basename[-5:]+'_'+\
                    str(ct_pdb)
            if not pdbid in self.list_pdbids:
                self.list_pdbids.append(pdbid)
            ct_pdb += 1

#     def fix_map_model_refmac(self,mapfile):
#         ori_x = ori_y = ori_z = 0.
#         fix_map_model = True
#         with mrcfile.open(mapfile, mode='r',permissive=True) as mrc:
#             map_nx = mrc.header.nx
#             map_ny = mrc.header.ny
#             map_nz = mrc.header.nz
#             map_nxstart = mrc.header.nxstart
#             map_nystart = mrc.header.nystart
#             map_nzstart = mrc.header.nzstart
#             map_mapc = mrc.header.mapc
#             map_mapr = mrc.header.mapr
#             map_maps = mrc.header.maps
#             ori_x = mrc.header.origin.x
#             ori_y = mrc.header.origin.y
#             ori_z = mrc.header.origin.z
#             cella = (mrc.header.cella.x,mrc.header.cella.y,
#                      mrc.header.cella.z)
#             self.apix = mrc.voxel_size.item()
#             self.map_dim = (map_nx,map_ny,map_nz)
# #         print 'initial origin'
# #         print mrc.header.nxstart, mrc.header.nystart, mrc.header.nzstart
# #         print mrc.header.origin
#         #fix map and model when nstart is non-zero
#         if ori_x == 0. and ori_y == 0. and ori_z == 0.:
#             if map_nxstart != 0 or map_nystart != 0 or map_nzstart != 0:
#                 #fix origin to match nstart
#                 map_edit = os.path.splitext(
#                             os.path.basename(mapfile))[0]+'_fix.mrc'
#                 self.args.map_path_edit.value = os.path.join(
#                                         self.job_location,map_edit)
#                 shutil.copyfile(mapfile,self.args.map_path_edit.value)
#                 try: assert os.path.isfile(self.args.map_path_edit.value)
#                 except AssertionError:
#                     return fix_map_model, (ori_x,ori_y,ori_z), (map_nx,map_ny,map_nz)
#                 #edit float origin
#                 with mrcfile.open(self.args.map_path_edit.value,'r+') as mrc:
#                     mrc.header.origin.x = map_nxstart*self.apix[0]
#                     mrc.header.origin.y = map_nystart*self.apix[1]
#                     mrc.header.origin.z = map_nzstart*self.apix[2]
#                 #refmac uses existing nstart values, no need to translate models
# #                 ori_x = map_nxstart*apix[0]
# #                 ori_y = map_nystart*apix[1]
# #                 ori_z = map_nzstart*apix[2]
#                 fix_map_model = False
#         return fix_map_model, (ori_x,ori_y,ori_z), (map_nx,map_ny,map_nz)
    
    def calculate_cropped_map_header(self,dim,new_dim):
        map_nx,map_ny,map_nz = dim
        crop_x = int(map_nx - new_dim[0])
        xs = max(0,int(np.ceil(float(crop_x)/2)))
        xe = min(map_nx,map_nx-int(np.floor(float(crop_x)/2)))
        
        crop_y = int(map_ny - new_dim[1])
        ys = max(0,int(np.ceil(float(crop_y)/2)))
        ye = min(map_ny,map_ny-int(np.floor(float(crop_y)/2)))
        
        crop_z = int(map_nz - new_dim[2])
        zs = max(0,int(np.ceil(float(crop_z)/2)))
        ze = min(map_nz,map_nz-int(np.floor(float(crop_z)/2)))
        #print xs,xe,ys,ye,zs,ze
        self.origin_vector_crop[0] = self.origin_vector[0] + xs*self.apix[0]
        self.origin_vector_crop[1] = self.origin_vector[1] + ys*self.apix[1]
        self.origin_vector_crop[2] = self.origin_vector[2] + zs*self.apix[2]
        self.map_origin_shift[0] = self.map_origin_shift[0] + xs*self.apix[0]
        self.map_origin_shift[1] = self.map_origin_shift[1] + xs*self.apix[1]
        self.map_origin_shift[2] = self.map_origin_shift[2] + xs*self.apix[2]
        self.nxstart = int(self.origin_vector_crop[0]/self.apix[0])
        self.nystart = int(self.origin_vector_crop[1]/self.apix[1])
        self.nzstart = int(self.origin_vector_crop[2]/self.apix[2])
        cella = (new_dim[0]*self.apix[0],new_dim[1]*self.apix[1],new_dim[2]*self.apix[2])
        return cella

    def run_pipeline(self, job_id=None, db_inject=None):
        #set input model and map ids
        self.set_model_ids()
        # if self.args.run_refmac_refine.value:
        #     self.list_maps_zero = []
        self.sim_maps = []#self.args.sim_maps.value
        self.pdbs_zero = []
        if self.args.map_contour_level.value is not None:
            self.args.auto_contour_level.value = False
        #make dirs for maps and models
        for mid in self.list_pdbids:
            mdir = os.path.join(self.job_location,mid)
            if not os.path.isdir(mdir):
                os.mkdir(mdir)
        #initialize edited data
        self.mapprocess_task = None
        if self.args.map_path.value != None:
            self.args.map_path_edit.value = self.args.map_path.value
            self.args.half_map1_edit.value = self.args.half_map_1.value
            self.args.half_map2_edit.value = self.args.half_map_2.value
            self.args.pdbs_zero.value = self.list_pdbs[:]
            self.bfact_refined = self.list_pdbs[:]
            fix_map_model = True
            self.list_new_dims = []
            #fix map origin
            self.args.map_path_edit.value, fix_map_model, cella, self.map_origin_shift, \
                self.map_dim, self.origin_vector = \
                fix_map_model_refmac(self.args.map_path.value,self.args.map_path_edit.value,
                                     self.job_location)
#             print self.args.map_path_edit.value, fix_map_model, cella, self.map_origin_shift, \
#                 self.map_dim, self.origin_vector
            self.origin_vector_crop = self.origin_vector
            self.apix = (float(cella[0])/self.map_dim[0],float(cella[1])/self.map_dim[1],
                         float(cella[2])/self.map_dim[2])
            
            self.list_maps[0] = self.args.map_path_edit.value
            #large maps are cropped by default
            if min(self.map_dim) > 350:
                self.new_dim = (self.map_dim[0]-int(0.25*self.map_dim[0]),
                           self.map_dim[1]-int(0.25*self.map_dim[1]),
                           self.map_dim[2]-int(0.25*self.map_dim[2]))
                self.new_dim = (self.new_dim[0]+(self.new_dim[0]%2),
                                self.new_dim[1]+(self.new_dim[1]%2),
                                self.new_dim[2]+(self.new_dim[2]%2)
                                )
                #print(self.map_dim, self.new_dim)
                self.list_new_dims.append(self.new_dim)
            
            if self.args.half_map_1.value != None:
                self.args.half_map1_edit.value, fix_map_model, cella_h1, origin_shift_vector_h1, \
                    self.map_dim_h1, origin_vector_h1 = \
                fix_map_model_refmac(self.args.half_map_1.value,self.args.half_map1_edit.value,
                                     self.job_location)
                self.list_maps[1] = self.args.half_map1_edit.value
                if compare_tuple(self.map_dim, self.map_dim_h1): #crop only if dimensions of input maps are same
                    self.list_new_dims.append(self.new_dim)
                # if min(self.map_dim_h1) > 350:
                #     new_dim = (self.map_dim_h1[0]-int(0.2*self.map_dim_h1[0]),
                #                self.map_dim_h1[1]-int(0.2*self.map_dim_h1[1]),
                #                self.map_dim_h1[2]-int(0.2*self.map_dim_h1[2]))
                
            if self.args.half_map_2.value != None:
                self.args.half_map2_edit.value, fix_map_model, cella_h2, origin_shift_vector_h2, \
                    self.map_dim_h2, origin_vector_h2 = \
                fix_map_model_refmac(self.args.half_map_2.value,self.args.half_map2_edit.value,
                                     self.job_location)
                self.list_maps[2] = self.args.half_map2_edit.value
                if compare_tuple(self.map_dim, self.map_dim_h2): #crop only if dimensions of input maps are same
                    self.list_new_dims.append(self.new_dim)
                # if min(self.map_dim_h2) > 350:
                #     new_dim = (self.map_dim_h2[0]-int(0.2*self.map_dim_h2[0]),
                #                self.map_dim_h2[1]-int(0.2*self.map_dim_h2[1]),
                #                self.map_dim_h2[2]-int(0.2*self.map_dim_h2[2]))
        #self.set_model_ids()
        
        pl2 = []
        if self.args.map_path.value != None:
            #crop big maps
            if len(self.list_maps) == len(self.list_new_dims): # if all maps can be cropped to same dimensions
                cella = self.calculate_cropped_map_header(self.map_dim, self.new_dim)
                self.map_dim = self.new_dim
                #print self.map_dim
                self.crop_map_process(pl2)
                map_cropped = os.path.splitext(os.path.basename(self.args.map_path.value))[0]+'_crop.mrc'
                
                #self.origin_vector = (0.,0.,0.)
                
            self.trans_vector = (-self.map_origin_shift[0],
                            -self.map_origin_shift[1],
                            -self.map_origin_shift[2])
            #print trans_vector
            #fix map and models for refmac
            if self.args.use_refmac.value or self.args.run_refmac.value:
                ##
                self.pdbs_zero = []
                #fix models if required
                ct_pdb = 0
                for pdb_path in self.list_pdbs:
                    pdbid = self.list_pdbids[ct_pdb]
                    mdir = os.path.join(self.job_location,pdbid)
                    
                    if os.path.splitext(pdb_path)[-1].lower() in ['.cif','.mmcif']:
                        pdb_edit_path = os.path.join(mdir,
                                            os.path.splitext(os.path.basename(
                                                pdb_path))[0]+'_fix.cif')
                    else:
                        pdb_edit_path = os.path.join(mdir,
                                            os.path.splitext(os.path.basename(
                                                pdb_path))[0]+'_fix.pdb')
                    #translate model to  nstart
                    if self.map_origin_shift[0] != 0. or self.map_origin_shift[1] != 0. \
                            or self.map_origin_shift[2] != 0.:#if nstart non-zero
                        
                        self.fix_model_for_refmac(pdb_path,pdb_edit_path,self.trans_vector,
                                                  remove_charges=True)
                    else:
                        #remove charges
                        remove_atomic_charges(pdb_path,
                                                  pdb_edit_path)
                    ct_pdb += 1
                    
                    try:
                        self.pdbs_zero.append(pdb_edit_path)
                    except: break
                                                  
                #store pdbs fixed for refmac (origin 0)
                if len(self.pdbs_zero) == len(self.list_pdbs):
                    self.args.pdbs_zero.value = self.pdbs_zero[:]
                    self.bfact_refined = self.pdbs_zero[:] #initialize to fixed pdbs
                #self.pdbs_zero = self.list_pdbs
    #         for mid in self.list_mapids:
    #             mdir = os.path.join(self.job_location,mid)
    #             if not os.path.isdir(mdir):
    #                 os.mkdir(mdir)
            self.origin_vector = self.origin_vector_crop
        #override any sccc selection
        self.args.use_smoc.value = True
        self.args.use_sccc.value = False
        if self.args.results_parallel.value:
            self.args.ncpu.value = max(1,self.args.ncpu.value-1)
        
        #set order in which job types are added to the pipeline
        pl1 = []
        #bfactor
        self.add_bfactor_process(pl1)
        #run refmac and refine b factors
        if self.args.run_refmac.value:
            self.add_refmac_process(pl1)
        #reduce,molprobity
        if not self.args.run_refmac_refine.value and self.args.run_molprobity.value:
            self.add_molprobity_process(pl1)
        #cablam
        if not self.args.run_refmac_refine.value and self.args.run_cablam.value:
            self.add_cablam_process(pl1)
        
        if len(pl2) > 0: #crop map
            pl1 = pl2+pl1
                
        if self.args.refine_bfactor.value:
            pl = []
        else:
            pl = pl1
        if self.args.run_refmac_refine.value:
            if self.args.run_molprobity.value:
                self.add_molprobity_process(pl)
            if self.args.run_cablam.value:
                self.add_cablam_process(pl)
        #refmac synthetic maps
        if self.args.run_scores.value or self.args.run_smoc.value or self.args.run_diffmap.value:
        #if self.args.use_refmac.value:
            self.add_mapsyn_process(pl)
        #fdr validation
        if self.args.run_fdr.value:
            self.add_fdr_process(pl)
        #PI-score
        if self.args.run_piscore.value:
            self.add_piscore_process(pl)
        #scores
        if self.args.run_scores.value:
            self.add_scores_process(pl)
        #smoc
        if self.args.run_smoc.value:
            self.add_smoc_process(pl,job_id=job_id,db_inject=db_inject)
        #tempy diffmap
        if self.args.run_diffmap.value:
            self.add_diffmap_process(pl)
        #jpred
        if self.args.run_jpred.value:
            self.add_jpred_process(pl)
        
        if self.args.refine_bfactor.value:
            pl1.extend(pl)
            pl = pl1
        #set Results
        if not self.args.results_parallel.value:
            if len(pl) > 0:
                self.add_results_process(pl,len(pl))
        
        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
            #on_running_custom=custom_running)
        self.pipeline.start()

    def add_results_process(self,pl,index_pipeline=0):
        results_scriptfile = validate_results.__file__
        run_command = ['ccpem-python',results_scriptfile]
        run_command.append(os.path.join(self.job_location,'task.ccpem'))
        self.process_results = process_manager.CCPEMProcess(
            command=run_command,
            location=self.job_location,
            name='Results')
        #self.add_parallel_process(pl,[self.process_results])
        pl.insert(index_pipeline,[self.process_results])
    
    def add_bfactor_process(self,pl):
        ct_pdb = 0
        list_pdbs = self.list_pdbs
        if hasattr(self,'list_refined') and len(self.list_pdbs) == len(self.list_refined):
            list_pdbs = self.list_refined
        for pdb in list_pdbs:
            pdbid = self.list_pdbids[ct_pdb]
            mdir = os.path.join(self.job_location,pdbid)
            self.process_bfactor = BfactWrapper(
                self.commands['bfactor'],
                        pdb,
                        job_location=mdir,
                        name='Bfactor '+pdbid,
                        )
            self.add_parallel_process(pl, [self.process_bfactor.process])
            ct_pdb += 1
    
    def add_molprobity_process(self,pl):
        ct_pdb = 0
        list_pdbs = self.list_pdbs
        if hasattr(self,'list_refined') and len(self.list_pdbs) == len(self.list_refined):
            list_pdbs = self.list_refined
        for pdb in list_pdbs:
            pdbid = self.list_pdbids[ct_pdb]
            mdir = os.path.join(self.job_location,pdbid)
            cif_pdb = pdb
            if os.path.splitext(pdb)[-1].lower() in ['.cif','.mmcif']:
                cif_pdb = os.path.splitext(pdb)[0]+'.pdb'
                if not os.path.isfile(cif_pdb):
                    convert_mmcif_to_pdb(pdb,cif_pdb)
                    
            self.process_reduce = ReduceRun(
                command=self.commands['reduce'],
                        job_location=mdir,
                        pdb_path=cif_pdb,
                        name='Reduce '+pdbid)
#             if os.path.splitext(pdb)[-1].lower() == '.cif' or \
#             os.path.splitext(pdb)[-1].lower() == '.mmcif':
#                 reduce_outpdb = os.path.join(mdir,'reduce_out.cif')
#             else:
            reduce_outpdb = os.path.join(mdir,'reduce_out.pdb')
#             try: 
#                 shutil.copyfile(pdb,reduce_outpdb)
#             except: 
#                 molprobity_inputpdb = pdb
            #also check for reduce error
            #if os.path.isfile(reduce_outpdb):
            molprobity_inputpdb = reduce_outpdb
            self.process_molprobity = MolprobityRun(
                command=self.commands['molprobity'],
                        job_location=mdir,
                        name='Molprobity '+pdbid,
                        pdb_path=molprobity_inputpdb)
            
            self.add_parallel_process(pl, [self.process_reduce.process,
                                           self.process_molprobity.process])
            ct_pdb += 1
    
    def add_cablam_process(self,pl):
        list_pdbs = self.list_pdbs
        if hasattr(self,'list_refined') and len(self.list_pdbs) == len(self.list_refined):
            list_pdbs = self.list_refined
        ct_pdb = 0
        for pdb in list_pdbs:
            pdbid = self.list_pdbids[ct_pdb]
            mdir = os.path.join(self.job_location,pdbid)
            self.process_cablam = CablamRun(
                command=self.commands['cablam'],
                        job_location=mdir,
                        name='CABLAM '+pdbid,
                        pdb_path=pdb)
            self.process_dssp = DSSPRun(
                command=self.commands['dssp'],
                        job_location=mdir,
                        name='DSSP '+pdbid,
                        pdb_path=pdb,
                        output_dssp=self.args.output_dssp.value)
            self.add_parallel_process(pl, [self.process_dssp.process,
                                           self.process_cablam.process])
            ct_pdb += 1
            
    def add_jpred_process(self,pl):
        list_pdbs = self.list_pdbs
        if hasattr(self,'list_refined') and len(self.list_pdbs) == len(self.list_refined):
            list_pdbs = self.list_refined
        ct_pdb = 0
        list_processes = []
        for pdb in list_pdbs:
            pdbid = self.list_pdbids[ct_pdb]
            mdir = os.path.join(self.job_location,pdbid)
            self.jpred_wrapper = JPredRunWrapper(
                self.commands['ccpem-jpred'],
                pdb,
                job_location=mdir,
                name='Jpred: '+pdbid,
                )
            self.download_wrapper = JPredDownloadWrapper(
                self.commands['jpred-download'],
                job_location=mdir,
                name='Jpred Download: '+pdbid,
                )
            if not self.check_process_type_exists(pl,'DSSP'):
                self.process_dssp = DSSPRun(
                    command=self.commands['dssp'],
                            job_location=mdir,
                            name='DSSP '+pdbid,
                            pdb_path=pdb,
                            output_dssp=self.args.output_dssp.value)
                list_processes.append(self.process_dssp.process)
            #add jpred process
            list_processes.append(self.jpred_wrapper.process)
            #list_processes.append(self.download_wrapper.process)
            self.add_parallel_process(pl,list_processes[:])
            pl.append([self.download_wrapper.process])
            ct_pdb += 1
    
    def check_process_type_exists(self,pl,process_name):
        for st in pl:
            for process in st:
                if process_name in process.name:
                    return True
        return False
    
    def add_parallel_process(self,pl, list_process):
        num_process = len(list_process)
        ct_added_process = 0
        pl_stages = len(pl)
        #pl_stages = min(len(pl),num_process)
        for l in xrange(pl_stages,0,-1):
            if len(pl[-l]) < self.args.ncpu.value and ct_added_process < num_process:
                pl[-l].append(list_process[ct_added_process])
                ct_added_process += 1

        for l in xrange(ct_added_process,num_process):
            pl.append([list_process[l]])
    
    def crop_map_process(self,pl):
        list_processes = []
        ct_map = 0
        for map in self.list_maps:
            mapid = self.list_mapids[ct_map]
            job_dir = os.path.join(self.job_location,mapid)
            map_cropped = os.path.splitext(os.path.basename(map))[0]+'_crop.mrc'
            if not os.path.isdir(job_dir):
                os.mkdir(job_dir)
                shutil.copyfile(map,os.path.join(job_dir,map_cropped))
            self.list_maps[ct_map] = os.path.join(job_dir,map_cropped) #store cropped maps to score
            new_dim = list(self.list_new_dims[ct_map])
            
            ct_map += 1
            self.mapprocess_wrapper = mapprocess_wrapper(
                                job_location=job_dir,
                                command=self.commands['ccpem-mapprocess'],
                                map_path=map,
                                list_process=['crop'],
                                cropped_dim=new_dim,
                                out_map=map_cropped,
                                name='Crop '+mapid
                                )
            list_processes.append(self.mapprocess_wrapper.process)
            pl.append(list_processes)
        #self.add_parallel_process(pl, list_processes)
        
    def add_mapsyn_process(self,pl):
        ct_pdb = 0
        fix_map_model = False
        self.sim_maps = []
        if not hasattr(self,'origin_vector'):
            #fix map for refmac
            self.args.map_path_edit.value, fix_map_model, cella, self.origin_vector, \
            self.map_dim, self.map_origin_vector= \
                fix_map_model_refmac(self.args.map_path.value,self.args.map_edit_path.value,
                                     self.job_location)
            trans_vector = (-self.origin_vector[0],
                            -self.origin_vector[1],
                            -self.origin_vector[2])
            ##if fix_map_model: refmac generates maps with origin 0
            self.pdbs_zero = []
            #fix models if required
            ct_pdb = 0
            for pdb_path in self.list_pdbs:
                pdbid = self.list_pdbids[ct_pdb]
                mdir = os.path.join(self.job_location,pdbid)
                #pdb_edit_path = pdb_path
                if os.path.splitext(pdb_path)[-1].lower() in ['.cif','.mmcif']:
                    pdb_edit_path = os.path.join(mdir,
                                        os.path.splitext(os.path.basename(
                                            pdb_path))[0]+'_fix.cif')
                else:
                    pdb_edit_path = os.path.join(mdir,
                                        os.path.splitext(os.path.basename(
                                            pdb_path))[0]+'_fix.pdb')

                #translate model to nstart 0
                if self.origin_vector[0] != 0. or self.origin_vector[1] != 0. \
                        or self.origin_vector[2] != 0.:
                    
                    self.fix_model_for_refmac(pdb_path,pdb_edit_path,trans_vector,
                                              remove_charges=True)
                else:
                    #remove charges
                    remove_atomic_charges(pdb_path,
                                              pdb_edit_path)
                try:
                    self.pdbs_zero.append(pdb_edit_path)
                except: break
            #store pdbs fixed for refmac (origin 0)
            if len(self.pdbs_zero) == len(self.list_pdbs):
                self.args.pdbs_zero.value = self.pdbs_zero[:]
            
        #check whether to use fixed and bfact refined pdbs
        list_pdbs = self.list_pdbs
        if hasattr(self,'list_refined_zero') and len(self.list_pdbs) == len(self.list_refined_zero):
            list_pdbs = self.list_refined_zero
        elif len(self.bfact_refined) == len(self.list_pdbs) and self.args.run_refmac.value:
            list_pdbs = self.bfact_refined
        elif len(self.args.pdbs_zero.value) == len(self.list_pdbs):
            list_pdbs = self.args.pdbs_zero.value
        
        ct_pdb = 0
        for pdb_path in list_pdbs:
            pdbid = self.list_pdbids[ct_pdb]
            mdir = os.path.join(self.job_location,pdbid)
            list_process = []
            pdb_edit_path = pdb_path
            model_map_filename = os.path.splitext(
                    os.path.basename(pdb_path))[0]
            reference_map = os.path.join(
                mdir,
                model_map_filename+'_refmac.mrc')
            modelid = pdbid
#             if len(model_map_filename) > 10:
#                 modelid = model_map_filename[:5]+'_'+model_map_filename[-5:]
#             else:
#                 modelid = model_map_filename
            # Set PDB cryst from mtz
            self.process_set_unit_cell = SetModelCell(
                job_location=mdir,
                name='Set model unit cell '+modelid,
                pdb_path=pdb_path,
                map_path=self.list_maps[0])
            
            list_process.append(self.process_set_unit_cell.process)
            pdb = self.list_pdbs[ct_pdb]
            if os.path.splitext(pdb)[-1].lower() in ['.cif','.mmcif'] or \
                 os.stat(pdb).st_size > 10000000:
                self.pdb_set_path = self.process_set_unit_cell.cifout_path
            else:
                self.pdb_set_path = self.process_set_unit_cell.pdbout_path

            # Calculate mtz from pdb 
            self.refmac_sfcalc_crd_process = RefmacSfcalcCrd(
                job_location=mdir,
                pdb_path=self.pdb_set_path,
                name='SfcalcCrd '+modelid,
                lib_in=self.args.lib_in(),
                resolution=self.args.map_resolution.value,
                stdin='MAKE NEWLigand Noexit')
            self.refmac_sfcalc_mtz = os.path.join(
                mdir,
                'sfcalc_from_crd.mtz')
            list_process.append(self.refmac_sfcalc_crd_process.process)

            # Convert calc mtz to map
            #gemmi sf2map
            self.sf2map_process = sf2mapWrapper(
                job_location=mdir,
                command=self.commands['ccpem-gemmi'],
                map_nx=self.map_dim[0],
                map_ny=self.map_dim[1],
                map_nz=self.map_dim[2],
                # For now only use x,y,z
    #                 fast=mrc_axis[int(map_mapc)],
    #                 medium=mrc_axis[int(map_mapr)],
    #                 slow=mrc_axis[int(map_maps)],
                fast='X',
                medium='Y',
                slow='Z',
                mtz_path=self.refmac_sfcalc_mtz,
                map_path=reference_map,
                name = 'sf2map '+ modelid)
            list_process.append(self.sf2map_process.process)
             
            #set origin and remove background
            reference_map_processed = os.path.join(
                    mdir,
                    model_map_filename+'_syn.mrc')
            if not all(o == 0 for o in self.origin_vector):
                self.mapprocess_wrapper = mapprocess_wrapper(
                                job_location=mdir,
                                command=self.commands['ccpem-mapprocess'],
                                map_path=reference_map,
                                list_process=['shift_origin','threshold'],
                                map_origin=self.origin_vector,
                                map_contour=0.02,
                                #map_resolution=self.args.map_resolution.value,
                                out_map=reference_map_processed,
                                name='MapProcess '+modelid
                                )
            else:
                self.mapprocess_wrapper = mapprocess_wrapper(
                                job_location=mdir,
                                command=self.commands['ccpem-mapprocess'],
                                map_path=reference_map,
                                list_process=['threshold'],
                                map_origin=self.origin_vector,
                                map_contour=0.02,
                                #map_resolution=self.args.map_resolution.value,
                                out_map=reference_map_processed,
                                name='MapProcess '+modelid
                                )
            list_process.append(self.mapprocess_wrapper.process)
            
            self.sim_maps.append(reference_map_processed)
            
            self.add_parallel_process(pl, list_process[:])
            ct_pdb += 1
        # Set args
        args_file = os.path.join(self.job_location,
                                         'args.json')
        self.edit_argsfile(args_file)
        
    def add_smoc_process(self,pl,job_id=None, db_inject=None):
        ct_pdb = 0
        self.mapprocess_task = None
        #using these map ids for now
        #TODO: replace with self.list_mapids
        #list_mapids = ['map0','hm1','hm2']
        list_pdbs = self.list_pdbs
        #print self.list_pdbs
        list_maps = self.list_maps
        if hasattr(self,'list_refined') and len(self.list_pdbs) == len(self.list_refined):
            list_pdbs = self.list_refined
            #list_maps = self.list_maps_zero
#         elif len(self.args.pdbs_zero.value) == len(self.list_pdbs):
#             list_pdbs = self.args.pdbs_zero.value
#         else:
#             list_pdbs = self.list_pdbs
        for pdb in list_pdbs:
            pdbid = self.list_pdbids[ct_pdb]
            mdir = os.path.join(self.job_location,pdbid)
            ct_map = 0
            list_process = []
            for map in list_maps:
                mapid = self.list_mapids[ct_map]
                job_dir = os.path.join(mdir,mapid)
                if not os.path.isdir(job_dir):
                    os.mkdir(job_dir)
                #SMOC pipeline
#                 if (self.args.map_resolution.value > 7.5 or self.args.use_sccc.value) \
#                     and self.args.rigid_body_path.value is None :
#                     ribfind_job_title = 'Auto ribfind run'
#                     if job_id is not None:
#                         ribfind_job_title += ' {0}'.format(
#                             job_id)
#                     self.ribfind_job(db_inject=db_inject,
#                                      job_title=ribfind_job_title)
#                     list_process.append(self.ribfind_process)
#                 
#                     # Set rigid body path
#                     pdb_outdir = '.'.join(os.path.basename(pdb).split('.')[:-1])
#                     self.args.rigid_body_path.value = \
#                         os.path.join(self.ribfind_process.location,
#                                      pdb_outdir,'protein')
                # Set args
                args_file = os.path.join(self.job_location,
                                         'args.json')
                new_argsfile = os.path.join(job_dir,
                                         'args.json')
                #self.check_processed_map()
                if not os.path.isfile(new_argsfile):
                    self.copy_argsfile(args_file,new_argsfile, new_map_path=map,
                                   new_pdb_path=[pdb])
                    self.edit_argsfile(new_argsfile, new_map_path=map)
                # Generate process
                if not self.args.use_sccc.value:
                    self.smoc_wrapper = SMOCWrapper(
                        command=self.commands['ccpem-smoc'],
                        job_location=job_dir,
                        name='SMOC '+mapid+':'+pdbid)
#                 else:
#                     self.smoc_wrapper = SMOCWrapper(
#                         command=self.commands['ccpem-smoc'],
#                         job_location=job_dir,
#                         name='SCCC '+mapid+':'+pdbid)
                list_process.append(self.smoc_wrapper.process)
                ct_map += 1
            if self.args.use_refmac.value:
                for p in list_process:
                    pl.append([p])
            else:
                self.add_parallel_process(pl, list_process[:])
                
            ct_pdb += 1
    
    def add_diffmap_process(self,pl):
        ct_pdb = 0
        self.mapprocess_task = None
        if self.args.use_refmac.value:
            self.map_or_pdb_selection_edit = 'Map'
        else:
            self.map_or_pdb_selection_edit = 'Model'
        
        list_pdbs = self.list_pdbs
        list_maps = self.list_maps
        if hasattr(self,'list_refined') and len(self.list_pdbs) == len(self.list_refined):
            list_pdbs = self.list_refined
            #list_maps = self.list_maps_zero
        
        for pdb in list_pdbs:
            pdbid = self.list_pdbids[ct_pdb]
            mdir = os.path.join(self.job_location,pdbid)
            #check for refmac syn maps
            sim_map = None
            if len(self.sim_maps) != 0 and \
                            len(self.list_pdbs) == len(self.sim_maps):
                sim_map = self.sim_maps[ct_pdb]
            ct_map = 0
            list_process = []
            for map in list_maps:
                mapid = self.list_mapids[ct_map]
                job_dir = os.path.join(mdir,mapid)
                if not os.path.isdir(job_dir):
                    os.mkdir(job_dir)
                # Generate diffmap process
                self.tempy_difference_map = difference_map_task.TEMPyDifferenceMap(
                    job_location=job_dir,
                    command=self.commands['ccpem-diffmap'],
                    map_path_1=map,
                    map_or_pdb=self.map_or_pdb_selection_edit,
                    map_path_2=sim_map,#synthetic map
                    map_resolution_1=self.args.map_resolution.value,
                    map_resolution_2=self.args.map_resolution.value,
                    pdb_path = pdb,
                    mode='local',
                    noscale=False,
                    nofilt=False,
                    maskfile=None,
                    mpi=False,
                    n_mpi = 2,
                    window_size=None,
                    threshold_fraction=None,
                    dust_filter=False,
                    fracmap=True,
                    dust_prob = 0.1,
                    ref_scale = True)
                list_process.append(self.tempy_difference_map.process)
                # Generate getmapval process
                #model-map
                diff_frac2_map = os.path.join(
                        job_dir,'diff_frac2.mrc')
                self.get_map_val = difference_map_task.GetMapVal(
                    job_location=job_dir,
                    command=self.commands['ccpem-mapval'],
                    map_path_1=diff_frac2_map,
                    map_resolution_1=self.args.map_resolution.value,
                    pdb_path = pdb,
                    name = 'Get difference '+mapid+':'+pdbid)
                 
                list_process.append(self.get_map_val.process)
                ct_map += 1
            if self.args.use_refmac.value:
                for p in list_process:
                    pl.append([p])
            else:
                self.add_parallel_process(pl, list_process[:])
                 
            ct_pdb += 1
            
    def add_scores_process(self,pl):
        self.mapprocess_task = None
        #using these map ids for now
        #TODO: replace with self.list_mapids
        #list_mapids = ['map0','hm1','hm2']
        ct_map = 0
        list_process = []
        list_pdbs = self.list_pdbs
        list_maps = self.list_maps
        if hasattr(self,'list_refined') and len(self.list_pdbs) == len(self.list_refined):
            list_pdbs = self.list_refined
            #list_maps = self.list_maps_zero
#         if len(self.args.pdbs_zero.value) == len(self.list_pdbs):
#             list_pdbs = self.args.pdbs_zero.value
            
        for map in list_maps:
            mapid = self.list_mapids[ct_map]
            job_dir = os.path.join(self.job_location,mapid)
            if not os.path.isdir(job_dir):
                os.mkdir(job_dir)
            # Set args
            args_file = os.path.join(self.job_location,
                                     'args.json')
            new_argsfile = os.path.join(job_dir,
                                     'args.json')
            #print new_argsfile
            #copy args json
            #if not os.path.isfile(new_argsfile):
            self.copy_argsfile(args_file,new_argsfile, new_map_path=map,
                               new_pdb_path=list_pdbs)
            #print self.args.map_contour_level.value
            if self.args.map_contour_level.value is not None:
                self.edit_argsfile(new_argsfile, new_map_path=map,
                                   contour_level=self.args.map_contour_level.value)
            else:
                self.edit_argsfile(new_argsfile, new_map_path=map)
            # Generate process
            self.scores_wrapper = GlobScoreWrapper(
                command=self.commands['ccpem-scores'],
                job_location=job_dir,
                name='TEMPy scores '+mapid)
            ct_map += 1
            if self.args.use_refmac.value:
                pl.append([self.scores_wrapper.process])
            else:
                self.add_parallel_process(pl, [self.scores_wrapper.process])
    
    def prepare_cmdline_args_fdr(self,map):
        # Required args
        args = [
            '--em_map', str(map),
            '-method', METHODS[self.args.method.value],
            '--testProc', TEST_PROCS[self.args.test_proc.value]
        ]

        noise_box_coords = self.args.noise_box.value

        if noise_box_coords is not None and len(noise_box_coords) > 0:
            args.extend(['-noiseBox'] + noise_box_coords)

        def add_arg_if_not_none(name):
            arg = getattr(self.args, name.strip('-'))
            if arg is not None and str(arg.value) != 'None':
                args.extend([name, str(arg.value)])

        add_arg_if_not_none('--apix')
        add_arg_if_not_none('-locResMap')
        add_arg_if_not_none('--window_size')
        add_arg_if_not_none('--meanMap')
        add_arg_if_not_none('--varianceMap')
        add_arg_if_not_none('--lowPassFilter')

        if self.args.ecdf.value:
            args.append('-ecdf')

        return args
    def prepare_cmdline_args_validation(self,pdb):
                # Required args
        args = [
            '-input_map', str(self.args.fdr_map.value),
            '-input_pdb', str(pdb),
            #'-prune', str(self.args.prune.value),
            #'-valCA', str(self.args.valCA.value)
        ]
        #if self.args.fdr_map.value != None:
        #     args.append('-fdr_map')

        if self.args.prune.value:
            args.append('-prune')
        if self.args.valCA.value:
            args.append('-valCA')
        return args
    
    def add_piscore_process(self,pl):
        ct_pdb = 0
        for pdb in self.list_pdbs:
            pdbid = self.list_pdbids[ct_pdb]
            mdir = os.path.join(self.job_location,pdbid)
            if not os.path.isdir(mdir):
                os.mkdir(mdir)
                
            cif_pdb = pdb
            if os.path.splitext(pdb)[-1].lower() in ['.cif','.mmcif']:
                cif_pdb = os.path.splitext(pdb)[0]+'.pdb'
                if not os.path.isfile(cif_pdb):
                    convert_mmcif_to_pdb(pdb,cif_pdb)
            # PI-score process
            args=['-p',cif_pdb]
            #print args
            piscore_process = process_manager.CCPEMProcess(
                name='PI-score '+pdbid,
                command=self.commands['piscore'],
                args=args,
                location=mdir)
            pl.append([piscore_process])
            ct_pdb += 1

    def add_fdr_process(self,pl):
        
        list_pdbs = self.list_pdbs
        if hasattr(self,'list_refined') and len(self.list_pdbs) == len(self.list_refined):
            list_pdbs = self.list_refined
        elif len(self.args.pdbs_zero.value) == len(list_pdbs):
            list_pdbs = self.args.pdbs_zero.value
        ct_map = 0
        for map in self.list_maps:
            list_process = []
            mapid = self.list_mapids[ct_map]
            job_dir = os.path.join(self.job_location,mapid)
            if not os.path.isdir(job_dir):
                os.mkdir(job_dir)
            #generate confidence maps
            args = self.prepare_cmdline_args_fdr(map)
            confidence_maps_process = process_manager.CCPEMProcess(
            name='Confidence map '+mapid,
            command=self.commands['confidence_maps_python'],
            args=args,
            location=job_dir)
            map_basename = os.path.splitext(os.path.basename(map))[0]
            self.args.fdr_map.value = os.path.join(job_dir,map_basename + '_confidenceMap.mrc')
            ct_pdb = 0
            for pdb in list_pdbs:
                pdbid = self.list_pdbids[ct_pdb]
                mdir = os.path.join(job_dir,pdbid)
                if not os.path.isdir(mdir):
                    os.mkdir(mdir)
                # FDR Validation process
                args=self.prepare_cmdline_args_validation(pdb)
                fdr_validation_process = process_manager.CCPEMProcess(
                    name='FDR validation '+mapid+':'+pdbid,
                    command=self.commands['fdr_validation_python'],
                    args=args,
                    location=mdir)
                self.add_parallel_process(pl, [confidence_maps_process,fdr_validation_process])
                ct_pdb += 1
            ct_map += 1

    def add_refmac_process(self,pl):
#         self.refmac_mode = 'Global'
#         self.refmac_ncyc = 10
#         if not self.args.refine_bfactor.value:
#             self.refmac_mode = 'Local'
#             self.refmac_ncyc = 0
#         elif self.args.run_refmac_refine.value:
#             self.refmac_mode = 'Local'
#             self.refmac_ncyc = 20
#         else:
#             self.refmac_mode = 'Local'
#             self.refmac_ncyc = 10
        self.list_refined = []
        self.list_refined_zero = []
#         #check whether to use fixed pdbs
        if len(self.args.pdbs_zero.value) == len(self.list_pdbs):
            list_pdbs = self.args.pdbs_zero.value
        else:
            list_pdbs = self.list_pdbs
        #print list_pdbs
        ct_pdb = 0
        #print self.list_maps
        for pdb in list_pdbs:
            pdbid = self.list_pdbids[ct_pdb]
            mdir = os.path.join(self.job_location,pdbid)
            ct_map = 0
            for map in self.list_maps:
                list_refmac_process = []
                mapid = self.list_mapids[ct_map]
                
                # Convert map to mtz
                self.process_maptomtz = RefmacMapToMtz(
                    command=self.commands['refmac'],
                    resolution=self.args.map_resolution.value,
                    mode='Global',
                    name='Map to MTZ (global) '+mapid+':'+pdbid,
                    job_location=mdir,
                    map_path=map,
                    lib_path=self.args.lib_in.value,
                    hklout_path=os.path.join(self.job_location,
                                                     mapid+'.mtz')
                    )
                list_refmac_process.append(self.process_maptomtz.process)
                # Set cell parameters and scale of model
                self.process_set_unit_cell = SetModelCell(
                    job_location=mdir,
                    name='Set model unit cell '+mapid+':'+pdbid,
                    pdb_path=pdb,
                    map_path=map)
                list_refmac_process.append(self.process_set_unit_cell.process)
                refine_global = False
                if os.path.splitext(pdb)[-1].lower() in ['.cif','.mmcif'] or os.stat(pdb).st_size > 10000000:
                    start_refmac_pdb_path = self.process_set_unit_cell.cifout_path
                    self.refmac_refined = self.process_set_unit_cell.cifout_path
                else:
                    start_refmac_pdb_path = self.process_set_unit_cell.pdbout_path
                    self.refmac_refined = self.process_set_unit_cell.pdbout_path
#                 # Masked refine
#                 if self.args.run_refmac_refine.value:
#                     self.process_maptomtz_local = RefmacMapToMtz(
#                         command=self.commands['refmac'],
#                         resolution=self.args.map_resolution.value,
#                         mode='Masked',
#                         name='Map to MTZ (masked) '+mapid+':'+pdbid,
#                         job_location=mdir,
#                         map_path=map,
#                         pdb_path=start_refmac_pdb_path,
#                         lib_path=self.args.lib_in.value,
#                         pdbout_path='shifted_local.cif',
#                         stdin = 'MAKE NEWLigand Noexit'
# #                         hklout_path=os.path.join(self.job_location,
# #                                                          mapid+'_local.mtz')
#                         )
                    #print self.process_maptomtz_local.stdin
                if self.args.run_refmac_refine.value:
                    
                    # self.process_libg = LibgRestraints(
                    #     command=self.commands['libg'],
                    #     job_location=mdir,
                    #     name='DNA or RNA basepair restraints',
                    #     pdb_path=pdb)
                    
                    self.libg_restraints = False
                    # self.libg_restraints_path = [self.process_libg.libg_restraints_path]
                    # list_refmac_process.append(self.process_libg.process)
                    
                    
                    self.process_refine_local = RefmacRefineServal(
                        command=self.commands['ccpem-python'],
                        job_location=mdir,
                        pdb_path=pdb,
                        resolution=self.args.map_resolution.value,
                        halfmap_paths=[None,None],
                        map_path=map,
                        mask_for_fofc=None,
                        mode='Masked',
                        mask_radius=3.0,
                        jelly_body_on=True,
                        hydrogens='Recreate',
                        name='ServalCat (masked) '+mapid+':'+pdbid,
                        ncycle=20,
                        lib_path=self.args.lib_in.value,
                        libg_restraints=self.libg_restraints,
                        keywords='MAKE NEWLigand Noexit')#+'\nMAKE CHECk NONE')#,
                        #libg_restraints_path=self.libg_restraints_path)
                        
                        
#                         command=self.commands['refmac'],
#                         job_location=mdir,
#                         pdb_path=self.process_maptomtz_local.pdbout_path,
#                         resolution=self.args.map_resolution.value,
#                         lib_path=self.args.lib_in.value,
#                         mtz_path=hklout_path,
#                         mode='Masked',
#                         name='Refmac (masked) '+mapid+':'+pdbid,
#                         weight=None,
#                         keywords='weight auto 3'+'\nMAKE NEWLigand Noexit',
#                         ncycle=20)
                    if os.stat(pdb).st_size > 10000000:#os.path.splitext(pdb)[-1].lower() in ['.cif','.mmcif']
                        self.refmac_refined = os.path.join(mdir,'refined.mmcif')
                    else:
                        self.refmac_refined = os.path.join(mdir,'refined.pdb')
                    #list_refmac_process.append(self.process_maptomtz_local.process)
                    list_refmac_process.append(self.process_refine_local.process)
                    refine_global = False
                elif self.args.refine_bfactor.value:
                    # Masked refinement
                    self.process_maptomtz_local = RefmacMapToMtz(
                        command=self.commands['refmac'],
                        resolution=self.args.map_resolution.value,
                        mode='Masked',
                        name='Map to MTZ (masked) '+mapid+':'+pdbid,
                        job_location=mdir,
                        map_path=map,
                        pdb_path=start_refmac_pdb_path,
                        pdbout_path='shifted_local.cif',
                        stdin = 'MAKE NEWLigand Noexit'
#                         hklout_path=os.path.join(self.job_location,
#                                                          mapid+'_local.mtz')
                        )
                    #print self.process_maptomtz_local.stdin
                    hklout_path = os.path.join(mdir,'masked_fs.mtz')
                    self.process_refine_local = RefmacRefine(
                        command=self.commands['refmac'],
                        job_location=mdir,
                        pdb_path=self.process_maptomtz_local.pdbout_path,
                        resolution=self.args.map_resolution.value,
                        lib_path=self.args.lib_in.value,
                        mtz_path=hklout_path,
                        mode='Masked',
                        name='Refmac (masked) '+mapid+':'+pdbid,
                        weight=None,
                        keywords='weight auto 3'+'\nrefi bonly',#+'\nMAKE NEWLigand Noexit',
                        ncycle=8)
                    
                    if os.stat(pdb).st_size > 10000000: #os.path.splitext(pdb)[-1].lower() in ['.cif','.mmcif']
                        self.refmac_refined = os.path.join(mdir,'refined.mmcif')
                        self.bfact_refined[ct_pdb] = os.path.join(mdir,'refined.mmcif')
                    else:
                        self.refmac_refined = os.path.join(mdir,'refined.pdb')
                        self.bfact_refined[ct_pdb] = os.path.join(mdir,'refined.pdb')
                    
                    list_refmac_process.append(self.process_maptomtz_local.process)
                    list_refmac_process.append(self.process_refine_local.process)
                    refine_global = True
                else:
                    refine_global = True
                #global stats
                self.process_refine = RefmacRefine(
                        command=self.commands['refmac'],
                        job_location=mdir,
                        pdb_path=self.refmac_refined,
                        resolution=self.args.map_resolution.value,
                        mtz_path=self.process_maptomtz.hklout_path,
                        lib_path=self.args.lib_in.value,
                        mode='Global',
                        name='Refmac '+mapid+':'+pdbid,
                        ncycle=0,
                        keywords='MAKE NEWLigand Noexit')#+'\nMAKE CHECk NONE')
                list_refmac_process.append(self.process_refine.process)
                
                #self.add_parallel_process(pl, list_refmac_process)
                
                if refine_global: self.refmac_refined = pdb
                if ct_map == 0 and not refine_global: 
                    
                    self.modelshift_wrapper = ModelShift(
                                        job_location=mdir,
                                        command=self.commands['model-tools'],
                                        pdb_path=self.refmac_refined,
                                        transvec=[-t for t in self.trans_vector],
                                        name='Shift refined model '+pdbid
                                        )
                    self.list_refined_zero.append(self.refmac_refined)
                    refmac_refined_shifted = os.path.splitext(self.refmac_refined)[0] + \
                                                '_shifted'+os.path.splitext(self.refmac_refined)[1]
                    #shutil.copyfile(self.refmac_refined,refmac_refined_shifted)
                    self.list_refined.append(refmac_refined_shifted)
                    list_refmac_process.append(self.modelshift_wrapper.process)
                    
                # if ct_pdb == 0 and not refine_global:
                #     mapid = self.list_mapids[ct_map]
                #     job_dir = os.path.join(self.job_location,mapid)
                #     map_zero = os.path.splitext(os.path.basename(map))[0]+'_zero.mrc'
                #     if not os.path.isdir(job_dir):
                #         os.mkdir(job_dir)
                #         shutil.copyfile(map,os.path.join(job_dir,map_zero))
                #     self.list_maps_zero.append(os.path.join(job_dir,map_zero)) #store cropped maps to score
                #
                #     self.mapprocess_wrapper = mapprocess_wrapper(
                #                         job_location=job_dir,
                #                         command=self.commands['ccpem-mapprocess'],
                #                         map_path=map,
                #                         list_process=['shift_origin'],
                #                         map_origin=(0.,0.,0.),
                #                         out_map=map_zero,
                #                         name='Shift to zero '+mapid
                #                         )
                #     list_refmac_process.append(self.mapprocess_wrapper.process)
                self.add_parallel_process(pl, list_refmac_process)
                    
                ct_map += 1
# 
#             if self.args.half_map_1.value != None:
#                 # Convert half map 1 to mtz; stage
#                 self.process_maptomtz_hm1 = RefmacMapToMtz(
#                     command=self.commands['refmac'],
#                     resolution=self.args.map_resolution.value,
#                     mode='Global',
#                     name='Map to MTZ hm1:'+pdbid,
#                     job_location=mdir,
#                     map_path=self.args.half_map_1.value,
#                     hklout_path=os.path.join(self.job_location,
#                                                  'starting_map_hm1.mtz'))
#                 #set PDB cell
#                 self.process_pdb_set = PDBSetCell(
#                     command=self.commands['pdbset'],
#                     job_location=mdir,
#                     name='Set PDB cell hm1:'+pdbid,
#                     pdb_path=pdb,
#                     map_path=self.args.half_map_1.value)
#                 start_refmac_pdb_path = self.process_pdb_set.pdbout_path
#                 self.process_refine_hm1 = RefmacRefine(
#                     command=self.commands['refmac'],
#                     job_location=mdir,
#                     pdb_path=start_refmac_pdb_path,
#                     resolution=self.args.map_resolution.value,
#                     mode='Global',
#                     name='Refmac hm1:'+pdbid,
#                     ncycle=0,
#                     mtz_path=self.process_maptomtz_hm1.hklout_path)
#                 self.add_parallel_process(pl, [self.process_maptomtz_hm1.process,
#                                      self.process_pdb_set.process,
#                                      self.process_refine_hm1.process])
#             
#             if self.args.half_map_2.value != None:
#                 # Convert half map 1 to mtz; stage
#                 self.process_maptomtz_hm2 = RefmacMapToMtz(
#                     command=self.commands['refmac'],
#                     resolution=self.args.map_resolution.value,
#                     mode='Global',
#                     name='Map to MTZ hm2:'+pdbid,
#                     job_location=mdir,
#                     map_path=self.args.half_map_2.value,
#                     hklout_path=os.path.join(self.job_location,
#                                                  'starting_map_hm2.mtz'))
#                 #set PDB cell
#                 self.process_pdb_set = PDBSetCell(
#                     command=self.commands['pdbset'],
#                     job_location=mdir,
#                     name='Set PDB cell hm2:'+pdbid,
#                     pdb_path=pdb,
#                     map_path=self.args.half_map_2.value)
#                 start_refmac_pdb_path = self.process_pdb_set.pdbout_path
#                 self.process_refine_hm2 = RefmacRefine(
#                     command=self.commands['refmac'],
#                     job_location=mdir,
#                     pdb_path=start_refmac_pdb_path,
#                     resolution=self.args.map_resolution.value,
#                     mode='Global',
#                     name='Refmac hm2:'+pdbid,
#                     ncycle=0,
#                     mtz_path=self.process_maptomtz_hm2.hklout_path)
#                 self.add_parallel_process(pl, [self.process_maptomtz_hm2.process,
#                                      self.process_pdb_set.process,
#                                      self.process_refine_hm2.process])
            ct_pdb += 1

def compare_tuple(tuple1,tuple2):
    for val1, val2 in zip(tuple1, tuple2):
        if type(val2) is float:
            if round(val1,2) != round(val2,2):
                return False
        else:
            if val1 != val2:
                return False
    return True
