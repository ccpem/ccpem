import os,sys
from collections import OrderedDict
import shutil

coot_data_keys = [ "clusters","rama", "rota", "cbeta", "probe", "smoc", 
                  "fdr","fsc","diffmap","cablam","jpred"]

class SetCootScript(object):
    
    def __init__(self,job_location,method='molprobity',
                 molprobity_job_location=None, modelid=''):
        #set cootdata dir
        if os.path.isdir(job_location):
            self.coot_validation_dir = os.path.join(
                                            job_location,
                                            'validation_cootdata')
            if not os.path.isdir(self.coot_validation_dir):
                os.mkdir(self.coot_validation_dir)
                with open(os.path.join(self.coot_validation_dir,"__init__.py"),'w') as initf:
                    pass

        self.ref_cootscript = os.path.join(os.path.dirname(__file__),"coot_validation_list.py")
            #just as a workaround get residue data from coot script
            #mmtbx validation object can be used with ccp4-python but
            #avoiding this for now
    def set_molprobity_cootscript(self,cootfile,modelid='',method='molprobity'):
        molprobity_probe_txt = os.path.join(
                        os.path.dirname(cootfile),
                        'molprobity_probe.txt')
        if os.path.isfile(molprobity_probe_txt):
            try: shutil.copyfile(molprobity_probe_txt, os.path.join(
                    self.coot_validation_dir,
                    'molprobity_probe'+modelid+'.txt'))
            except: pass
        self.new_cootscript = os.path.join(self.coot_validation_dir,
                                    'outliers_cootscript_'+modelid+'.py')
        if os.path.isfile(cootfile):
            if not os.path.isfile(self.new_cootscript):
                try: 
                    shutil.copyfile(cootfile, self.new_cootscript)
                except: pass
            else:
                cootdata = self.get_data_from_cootscript(cootfile,
                                                         method='molprobity',
                                                         modelid=modelid)
                self.add_data_to_cootfile(cootdata,method='molprobity',
                                                         modelid=modelid)
                
    def get_data_from_existing_cootscript(self,cootfile, modelid='',method=''):
        datafile_basename = 'outliers_prevdata_'+modelid+'_'+method
        datafile = os.path.join(self.coot_validation_dir,
                                datafile_basename+'.py')
        if os.path.isfile(datafile):
            os.remove(datafile)
        cootf = open(cootfile,'r')
        dataf = open(datafile,'w')
        dataf.write("data = {}\n")
        list_metrics = []
         
        for line in cootf:
            linesplit = line.split()
            if line[:5] == 'data[' and linesplit[1] == '=':
                dataf.write(line)
        cootf.close()
        dataf.close()
         
        if not self.coot_validation_dir in sys.path:
            sys.path.append(self.coot_validation_dir)
             
        #from self.coot_validation_dir import outliers_cootdata
        outliers_cootdata = getattr(__import__(datafile_basename,fromlist=['data']),'data')
        #data = outliers_cootdata.data
        return outliers_cootdata

    def get_data_from_cootscript(self,cootfile, modelid='',method=''):
        datafile_basename = 'outliers_cootdata_'+modelid+'_'+method
        datafile = os.path.join(self.coot_validation_dir,
                                datafile_basename+'.py')
        #read from datafile
        if os.path.isfile(datafile):
            if not self.coot_validation_dir in sys.path:
                sys.path.append(self.coot_validation_dir)
            outliers_cootdata = getattr(
                                __import__(datafile_basename,
                                           fromlist=['data']),'data')
            
            if len(outliers_cootdata) > 0:
                read_cootscript = False
            else:
                read_cootscript = True
        else: read_cootscript = True
        #read from coot script
        if read_cootscript:
            cootf = open(cootfile,'r')
            dataf = open(datafile,'w')
            dataf.write("data = {}\n")
            list_metrics = []
            
            for line in cootf:
                linesplit = line.split()
                if line[:5] == 'data[' and linesplit[1] == '=':
                    dataf.write(line)
            cootf.close()
            dataf.close()
            
            if not self.coot_validation_dir in sys.path:
                sys.path.append(self.coot_validation_dir)
                
            #from self.coot_validation_dir import outliers_cootdata
            outliers_cootdata = getattr(__import__(datafile_basename,fromlist=['data']),'data')
            #data = outliers_cootdata.data
        return outliers_cootdata

    def add_data_to_datafile(self,datafile,newdata):
        with open(datafile,'a') as dataf:
            for k in newdata:
                dataf.write("\n")
                dataf.write("data[{}] = {}".format(k,newdata[k]))
    
    def add_data_to_cootfile(self,newdata,modelid='',method=''):
        self.new_cootscript = os.path.join(self.coot_validation_dir,
                                    'outliers_cootscript_'+modelid+'.py')
        #check coot script template
        if not os.path.isfile(self.ref_cootscript):
            return
        cf = open(self.ref_cootscript,'r')
        if os.path.isfile(self.new_cootscript):
            current_data = self.get_data_from_existing_cootscript(self.new_cootscript,
                                                         modelid=modelid,
                                                         method=method)
            for data_key in current_data:
                if not len(current_data[data_key]) == 0:
                    newdata[data_key] = current_data[data_key]
            #delete existing cootscript
            os.remove(self.new_cootscript)
            
        newcf = open(self.new_cootscript,'w')
        list_newkeys = newdata.keys()
        set_data = False
        for line in cf:
            #avoid shorter lines
            linesplit = line.split()
            newcf.write(line)
        newcf.write("data = {}\n")
        for data_key in coot_data_keys:
            if not data_key in newdata:
                newcf.write("data['{}'] = []\n".format(data_key))
        for data_key in list_newkeys:
            newcf.write("data['{}'] = {}\n".format(data_key,newdata[data_key]))

        if 'probe' in newdata.keys():
            newcf.write("handle_read_draw_probe_dots_unformatted(\"{}\", 0, 0)\n".format(
                os.path.join(
                    self.coot_validation_dir,
                    'molprobity_probe'+modelid+'.txt')))
            newcf.write("show_probe_dots(True, True)\n")
        newcf.write("gui = coot_molprobity_todo_list_gui(data=data)\n")
        newcf.close()
        cf.close()
    
    def get_data_from_jsonfile(self,jsonfile):
        with open(jsonfile,'r') as jf:
            data = json.loads(jf.read())
        print data
