#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
import json
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.process_manager import job_register
from ccpem_core import settings
import mrcfile
from ccpem_core.model_tools.gemmi_utils import shift_coordinates, remove_atomic_charges

class Strudel(task_utils.CCPEMTask):
    '''
    CCPEM / Strudel score task.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='Strudel Score',
        author='A. Istrate, G. Murshudov, A. Patwardhan, G. Kleywegt',
        version='',
        description=(
            '''
            Strudel Score is a map-model validation method which entails calculating the linear correlation coefficient between an amino-acid map-motif from the Strudel library and the cryo-EM map values around a target residue.  
The validation results can be visualised using the StrudelScore plugin for ChimeraX (link: https://cxtoolshed.rbvi.ucsf.edu/apps/chimeraxstrudelscore )
            '''),
        short_description=(
            'Identify sequence mis-fits and propose potential residues for replacement'),
        documentation_link='https://github.com/emdb-empiar/3dstrudel/blob/master/threed_strudel/tutorials/Introduction_to_strudel_validation.pdf',
        references=None)

    commands = {'ccpem-strudel':
        ['ccpem-python', settings.which('strudel_mapMotifValidation.py')]}


    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        #
        super(Strudel, self).__init__(
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        map_path = parser.add_argument_group()
        map_path.add_argument(
            '-map_path',
            '--map_path',
            help='Input map (mrc format)',
            metavar='Input map',
            type=str,
            default=None)
        
        pdb_path = parser.add_argument_group()
        pdb_path.add_argument(
            '-pdb_path',
            '--pdb_path',
            help=('Atomic model to be evaluated'),
            metavar='Input atomic model',
            type=str,
            default=None)

        #motif library for strudel
        parser.add_argument(
            '-library_selection',
            '--library_selection',
            help='Strudel motif library for scoring',
            metavar='Strudel motif library',
            type=str,
            choices=['2.0-2.3','2.3-2.5','2.5-2.8','2.8-3.0','3.0-3.2','3.2-3.5','3.5-4.0'],
            default='2.8-3.0')
        
        num_proc = 1
        num_proc = max(self.get_ncpu()-1,num_proc)
        parser.add_argument(
            '-ncpu',
            '--ncpu',
            help= ('Number of cpus to use \n'
                   '(max cpus - 1, by default)'),
            metavar='Number of cpus',
            type=int,
            default=num_proc)
        
        return parser
    
    
    def get_ncpu(self):
        try:
            import multiprocessing
            return multiprocessing.cpu_count()
        except (ImportError, NotImplementedError):
            return 1
    
    def run_pipeline(self, job_id=None, db_inject=None):
        
        pl = []
        self.library_path = os.path.join(os.environ['CCPEM'],'lib','strudel',
                                         'strudel-libs_ver-4.0_voxel-0.5',
                                         'motifs_'+self.args.library_selection.value)
        #print 'Library path: ', self.library_path
        try:
            assert os.path.exists(self.library_path)
        except AssertionError:
            raise RuntimeError('Strudel library not found, please download from \
            https://ftp.ebi.ac.uk/pub/databases/emdb_vault/strudel_libs to lib/strudel \
            under CCP-EM installation')


        self.out_path = os.path.join(self.job_location,'strudel_out')
            
        self.strudel_process = strudel_wrapper(
                            job_location=self.job_location,
                            command=self.commands['ccpem-strudel'],
                            map_path=self.args.map_path.value,
                            pdb_path=self.args.pdb_path.value,
                            library_path=self.library_path,
                            out_path=self.out_path,
                            name='Strudel Score'
                            )
        pl = [[self.strudel_process.process]]

        # Generate process
        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

class strudel_wrapper():
    '''
    Wrapper for Strudel validation tool.
    '''
    def __init__(self,
                 job_location,
                 command,
                 map_path,
                 library_path,
                 pdb_path,
                 out_path,
                 name='Strudel'):
        assert map_path is not None
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.map_path = ccpem_utils.get_path_abs(map_path)
        self.pdb_path = ccpem_utils.get_path_abs(pdb_path)
        self.lib_path = library_path
        # Set args
        self.args = ['-m', self.map_path]
        self.args += ['-p', self.pdb_path]
        self.args += ['-l', self.lib_path]        
        if out_path is not None:
            self.args += ['-o', out_path]
        else:
            self.args += ['-o', os.path.join(job_location,'strudel_out')]
        
        #print self.args
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)
