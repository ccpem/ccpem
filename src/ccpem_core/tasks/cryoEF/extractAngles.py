#THis is a simple script to extract particles orientation angles from
#RELION starfiles for analysis with cryoEF.
#See Naydenova & Russo 2017 for more information.

Usage = '''python extractAngles.py inputstarfile.star
where the inputstarfile.star is the datafile (_data.star)
of any 3D reconstruction from RELION (3D Class or 3D autorefine)
Example: python extractAngles.py relion_run2_data.star > angles.dat
cryoEF -f angles.dat'''

import sys,os

def main():
    if len(sys.argv) == 1:
        print 'Relion angles star file not found'
        print Usage
        sys.exit()
    starfile = sys.argv[1]
    assert os.path.isfile(starfile)
    
    angles_file = '.'.join(os.path.basename(starfile).split('.')[:-1])+'_angles.dat'
    
    flag_readcolumns = 0
    count_angles = 0
    with open(starfile,'r') as f:
        for line in f:
            if '_rlnAngleTilt' in line:
                thetacol = line.split()[1][1:] #_rlnAngleTilt #26
                flag_readcolumns += 1
            elif '_rlnAngleRot' in line:
                phicol = line.split()[1][1:] #_rlnAngleRot #25
                flag_readcolumns += 1
            if flag_readcolumns == 2:
                linesplit = line.split()
                if len(linesplit) > 2:
                    thetacolvar = linesplit[int(thetacol)-1]
                    phicolvar = linesplit[int(phicol)-1]
                    if count_angles == 0:
                        of = open(angles_file,'w')
                    count_angles += 1
                    of.write("{} {}".format(phicolvar,thetacolvar))
                    of.write("\n")
    
    if flag_readcolumns < 2:
        sys.exit('Angles not found. Check the star file input.')
    if count_angles < 100: sys.exit('Check angles file')
    of.close()
            
        
if __name__ == '__main__':
    sys.exit(main())