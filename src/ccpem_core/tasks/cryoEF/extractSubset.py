#THis is a simple script to extract extract a small subset of
#particle orientation angles from a list.
#which allows quicker analysis of the orientation distribution.
#See Naydenova & Russo 2017 for more information.

Usage = '''python extractAngles.py number inputdatafile.dat
where number is the size of the subset
and inputdatafile.dat is the list of orientation angles
possibly created with the script extractAngles.py
Example: python extractAngles.py relion_run2_data.star > angles.dat
python extractSubset.py 1000 angles.dat > angles_subset.dat
cryoEF -f angles_subset.dat'''

import sys,os
import random
import linecache

def main():
    if len(sys.argv) < 3:
        print Usage
        sys.exit()
    try:
        subset_size = int(sys.argv[1])
    except TypeError:
        print Usage
        sys.exit()
    assert os.path.isfile(sys.argv[2])
    anglesfile = sys.argv[2]
    
    angles_subset_file = '.'.join(os.path.basename(
                                anglesfile).split('.')[:-1])+'_subset.dat'
    
    total_lines = 0
    with open(anglesfile,'r') as f:
        for line in f:
            total_lines += 1
    
    try:
        random_indices = random.sample(xrange(total_lines),subset_size)
    except:
        print 'Check angles text file'
        sys.exit()
    
    count_angles = 0
    for i in random_indices:
        if count_angles == 0:
            of = open(angles_subset_file,'w')
        of.write(linecache.getline(anglesfile,i))
        count_angles += 1
        
    if count_angles < 100: sys.exit('Check angles file')
    of.close()

if __name__ == '__main__':
    sys.exit(main())
