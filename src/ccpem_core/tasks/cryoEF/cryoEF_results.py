import os,sys
import shutil
import pyrvapi
from ccpem_core import ccpem_utils
import json
from ccpem_core import process_manager

class cryoEF_ResultsViewer():
    def __init__(self,
                 pipeline=None,
                 pipeline_path=None,
                 angles_file_path=None):
        if pipeline_path is not None:
            self.pipeline = process_manager.CCPEMPipeline(
                pipeline=None,
                import_json=pipeline_path)
        else:
            self.pipeline = pipeline
        args_path = self.pipeline.args_path
        with open(args_path,'r') as fp:
            job_args = json.load(fp)
        #print job_args
        if angles_file_path is None:
            angles_file_path = job_args['angles_file_path']
        #print angles_file_path
        log_file = os.path.splitext(angles_file_path)[0] + \
                        '.log'
        # setup doc
        ccp4 = os.environ['CCPEM']
        share_jsrview = os.path.join(ccp4, 'share', 'jsrview')
        self.directory = os.path.join(self.pipeline.location, 'rvapi_data')
        self.index = os.path.join(self.directory, 'index.html')
        
        if os.path.exists(self.directory):
            shutil.rmtree(self.directory)
        ccpem_utils.check_directory_and_make(self.directory)
        #setup pages
        pyrvapi.rvapi_init_document(self.pipeline.location, self.directory,
                                    self.pipeline.location, 1, 4, share_jsrview,
                                    None, None, None, None)
        pyrvapi.rvapi_flush()

        logparser = Log_parser(log_file)
        
        pyrvapi.rvapi_add_tab( 'cryoEF_results', "cryoEF:results", True)
        pyrvapi.rvapi_add_section("results_section","Angle distribution",'cryoEF_results',0,0,1,1,True )
        for result_line in logparser.output:
            print_line = result_line+'<br>'
            pyrvapi.rvapi_add_text(print_line, "results_section", 1, 0, 1, 1)
        for result_line in logparser.runtime:
            if 'alert' in result_line:
                print_line = "<h3 style='color:red'>{0}</h3>".format(result_line)
            else: print_line = result_line+'<br>'
            pyrvapi.rvapi_add_text(print_line, "results_section", 1, 0, 1, 1)
        pyrvapi.rvapi_flush()
        plot_angles = os.path.join(self.pipeline.location,'angles.png')
        if os.path.isfile(plot_angles):
            pyrvapi.rvapi_add_text("<img src='{}' alt='' style=width:450px;height:350px;>".format(plot_angles),
                                   "results_section", 1, 0, 1, 1)
        pyrvapi.rvapi_flush()
        #pyrvapi.rvapi_add_tab( 'cryoEF_recommendations', "cryoEF:recommendations", True)
        pyrvapi.rvapi_add_section("recommendation_section","Tilt recommendations",'cryoEF_results',0,0,1,1,True )
        for result_line in logparser.recommendations:
            pyrvapi.rvapi_add_text(result_line+'<br>', "recommendation_section", 1, 0, 1, 1)
        pyrvapi.rvapi_flush()


class Log_parser():
    def __init__(self,logfile):
        self.log = logfile
        self.runtime = []
        self.output = []
        self.recommendations = []
        self.parse_log()
        
    def parse_log(self):
        flagruntime = 0
        flagoutput = 0
        flagrecommendations = 0
        with open(self.log,'r') as f:
            for line in f:
                if flagruntime == 1:
                    if 'alert' in line:
                        self.runtime.append(line[:-1])
                    if 'Fraction' in line:
                        self.runtime.append(line[:-1])
                if flagoutput == 1:
                    if 'Efficiency' in line:
                        self.output.append(line[:-1])
                if flagrecommendations == 1:
                    self.recommendations.append(line[:-1])
                if ' Runtime Info ' in line:
                    flagruntime = 1
                if ' Output ' in line:
                    flagruntime = 0
                    flagoutput = 1
                if ' Tilt Recommendations ' in line:
                    flagrecommendations = 1
                    flagoutput = 0
                    
def main():
    cryoEF_results = cryoEF_ResultsViewer(pipeline_path=sys.argv[1],
                                          angles_file_path=sys.argv[2])
if __name__ == '__main__':
    main()