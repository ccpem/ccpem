import sys
import re
import os, glob
import time
import pyrvapi
import fileinput
import collections
import shutil, copy
import numpy as np
import pandas as pd
import math
from ccpem_core import ccpem_utils
from ccpem_core.tasks.atomic_model_validation.pyrvapi_utils import *


class SetFDRValidationResultsViewer():
    def __init__(self, directory, set_results=True):
        self.job_location = directory
        self.directory = os.path.join(directory,
                                      'rvapi_data')
        
        if os.path.exists(self.directory):
            shutil.rmtree(self.directory)
        if not os.path.isdir(self.directory): os.mkdir(self.directory)
        ccpem_utils.check_directory_and_make(self.directory)
        
        # Metadata contained in pandas dataframe
        self.fdr_dataframe = self.set_dataframe()
        if self.fdr_dataframe is None:
            sys.exit('Output CSV file missing')
        # Set results
        self.index = None
        if set_results:
            self.set_pyrvapi_page()
            self.set_results_summary()
            self.set_results_graphs()
    
    def set_dataframe(self):
        csv_output = glob.glob(os.path.join(self.job_location, '*_FDR_ranked_residues.csv'))[0]
        print 'Parsing: ', csv_output
        if os.path.isfile(csv_output):
            return pd.read_csv(csv_output)
        
    def set_pyrvapi_page(self):
        # Setup index.html
        # Create results if not already done
        rv_index = os.path.join(
            self.job_location,
            'rvapi_data/index.html')
        self.index = rv_index
        # Setup share jsrview
        ccp4 = os.environ['CCPEM']
        share_jsrview = os.path.join(ccp4, 'share', 'jsrview')
        # Setup pages
        pyrvapi.rvapi_init_document('TestRun', self.directory,
                                    'RVAPI Demo 1', 1, 4, share_jsrview,
                                    None, None, None, None)
        pyrvapi.rvapi_flush()
    
    def set_results_summary(self,section_name='sec1'):
        # Setup refine_results (summary, graphs and output files)
        pyrvapi.rvapi_add_header('FDR validation scores')
        pyrvapi.rvapi_add_tab('tab2', 'FDR Scores', True)
        pyrvapi.rvapi_add_section(section_name, 'Results', 'tab2', 0, 0, 1, 1,
                                  True)
        pyrvapi.rvapi_add_text(
            'FDR validation scores per residue (toggle chain selection below)',
            'sec1', 0, 0, 1, 1)
    
        # See flex-em results for example of setting a datatable here.
        pyrvapi.rvapi_flush()
    
    def set_results_graphs(self,section_name='sec1'):
        # Refinement statistics vs cycle
        graphwidget = 'graphWidget1'
        
        #pyrvapi.rvapi_append_loggraph1(section_name+'/graphWidget1')
        pyrvapi.rvapi_add_graph(graphwidget, section_name, 1, 0, 1, 1)
        
        # Add graph data
        x_label = 'Residue'
        # Set plot
        y_label = 'FDR score'
        
        list_chains = []
        ct_plot = 0
        dict_chain = {}
        list_chains = self.fdr_dataframe.Chain_name.unique()
        
        # Get data
        for chain in list_chains:
            ct_plot += 1
            data = 'data'+str(ct_plot)
            dataname = 'FDR score'
            plotname = 'chain_'+chain
            pyrvapi.rvapi_add_graph_data1(graphwidget+'/'+data,
                                                    dataname)
            plotlinename = 'chain_'+chain
            df_chain = self.fdr_dataframe.loc[self.fdr_dataframe['Chain_name'] == chain]#locate rows where Chain_name is chain
            list_resnum = []
            list_scores = []
            df_res = df_chain['residue_id'].tolist()
            df_score = df_chain['conf_score'].tolist()
            for l in xrange(len(df_chain['residue_id'])):
                res = df_res[l]
                sc = df_score[l]
                try: 
                    resnum = int(res)
                except ValueError: continue#skip insertion codes
                list_resnum.append(resnum)
                list_scores.append(float(sc))
            
            lnum=1
            plot_id='plot'+str(ct_plot)
            add_plot_to_graph(graphwidget,data,plotname,
                                          plotlinename,list_resnum,list_scores,
                                          x_label, y_label,
                                          plot_id=plot_id,
                                          xtype='int',ytype='float',lnum=lnum)
            list_sel_scores = []
            list_sel_resnum = []
            for s in xrange(len(list_scores)):
                if list_scores[s] < 0.95 and list_scores[s] >= 0.9:
                    list_sel_scores.append(list_scores[s])
                    list_sel_resnum.append(list_resnum[s])
            if len(list_sel_resnum) > 0:
                lnum+=1
                plotlinename = 'FDR<0.95'
                add_line_to_plot(graphwidget,data,plotlinename,list_sel_scores,
                                         list_x=list_sel_resnum,x_label=x_label,
                          ytype='float',
                          plot_id=plot_id,
                          lnum=lnum,
                          line_color=pyrvapi.RVAPI_COLOR_Gold,
                          line_show=pyrvapi.RVAPI_LINE_Off)
            list_sel_scores = []
            list_sel_resnum = []
            for s in xrange(len(list_scores)):
                if list_scores[s] < 0.9:
                    list_sel_scores.append(list_scores[s])
                    list_sel_resnum.append(list_resnum[s])
            if len(list_sel_resnum) > 0:
                lnum+=1
                plotlinename = 'FDR<0.9'
                add_line_to_plot(graphwidget,data,plotlinename,list_sel_scores,
                                         list_x=list_sel_resnum,x_label=x_label,
                          ytype='float',
                          plot_id=plot_id,
                          lnum=lnum,
                          line_color=pyrvapi.RVAPI_COLOR_Red,
                          line_show=pyrvapi.RVAPI_LINE_Off)
            pyrvapi.rvapi_flush()
                #print row['resnum'], row['smoc']
    #         pyrvapi.rvapi_set_plot_xmin('plot1', 'graphWidget1', 0.0)
    #         pyrvapi.rvapi_set_plot_ymin('plot1', 'graphWidget1', 0.0)
        #
        pyrvapi.rvapi_flush()

def main():
    setresults = SetFDRValidationResultsViewer(sys.argv[1])
    
if __name__ == '__main__':
    main()