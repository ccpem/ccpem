#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest

from ccpem_core.tasks.fdr_validation import fdr_validation_task

@unittest.skip
class Test(unittest.TestCase):
    '''
    Unit test for Confidence Maps task
    '''

    def test_arg_list_generation_with_no_args(self):
        '''
        Check that the command line arguments for the FDRcontrol script are
        generated properly.
        '''
        task = fdr_validation_task.FDRValidationTask(args_json_string="{}")
        args = task.prepare_cmdline_args_validation()
        assert '-input_map' in args and '-input_pdb' in args

    def test_arg_list_generation_with_all_args(self):
        '''
        Check that the command line arguments for the script are
        generated properly.
        '''
        task = fdr_validation_task.FDRValidationTask(args_json_string="""{
            "job_title": "Test FDR validation task",
            "job_location": "path/to/job",
            "fdr_map": "input fdr map",
            "input_model": "input atomic model",
            "prune": true
        }""")
        args = task.prepare_cmdline_args_validation()
        assert args == ['-input_map', 'input fdr map', '-input_pdb', 'input atomic model', '-prune']


if __name__ == '__main__':
    unittest.main()
