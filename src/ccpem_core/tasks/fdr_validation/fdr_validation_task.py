#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import FDRcontrol
import os
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core import ccpem_utils
from ccpem_core.tasks.fdr_validation import fdr_validation_results
from ccpem_progs.fdr_validation import val_pdb_python


# Conversions between GUI labels and Max's argument values
METHODS = {
    'FDR-BY': 'BY',
    'FDR-BH': 'BH',
    'FWER-Holm': 'Holm',
    'FWER-Hochberg': 'Hochberg'
}
TEST_PROCS = {
    'Left-sided': 'leftSided',
    'Right-sided': 'rightSided',
    'Two-sided': 'twoSided'
}


class FDRValidationTask(task_utils.CCPEMTask):
    '''
    CCPEM FDR validation Task.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='FDR validation',
        author='O. Mateusz',
        version='0',
        description=(
            'Atomic model validation based on Confidence maps using False Discovery Rate '
            'control.<br><br>'),
        short_description=(
            'Atomic model validation based on Confidence maps using False Discovery Rate '
            'control'),
        documentation_link='',
        references=None)
    commands = {
        'confidence_maps_python': ['ccpem-python', FDRcontrol.__file__],
        'fdr_validation_python' : ['ccpem-python', val_pdb_python.__file__],
        'fdr_validation_results' : ['ccpem-python', fdr_validation_results.__file__]
    }

    def __init__(self, **kwargs):
        super(FDRValidationTask, self).__init__(**kwargs)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)

        parser.add_argument(
            '-input_selection',
            '--input_selection',
            help='input unmasked map or a confidence map',
            metavar='Input file options',
            choices=['unmasked map','confidence map'],
            type=str,
            default='unmasked map')

        #
        parser.add_argument(
            '-em_map',
            '--em_map',
            help='Input map (mrc format)',
            metavar='Input map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-fdr_map',
            '--fdr_map',
            help='Input fdr map (mrc format)',
            metavar='Input fdr map',
            type=str,
            default=None)
                #
        parser.add_argument(
            '-input_model',
            '--input_model',
            help='Input model (pdb/cif format)',
            metavar='Input model',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-prune',
            '--prune',
            help="Prune low confidence areas of the model",
            metavar='Prune model?',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-valCA',
            '--valCA',
            help="Validate model based on the CA positions",
            metavar='Validate CA?',
            type=bool,
            default=False)

        #
        parser.add_argument(
            '-apix',
            '--apix',
            help='Override pixel size of input map.',
            metavar='Pixel size (angstroms)',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-locResMap',
            '--locResMap',
            help='Input local resolution map (optional).',
            metavar='Local resolution map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-method',
            '--method',
            help=("Method for multiple testing correction. FDR for False Discovery Rate or FWER for Family-Wise Error "
                  "Rate. 'BY' for Benjamini-Yekutieli, 'BH' for Benjamini-Hochberg."),
            metavar='Correction method',
            type=str,
            choices=['FDR-BY',
                     'FDR-BH',
                     'FWER-Holm',
                     'FWER-Hochberg'],
            default='FDR-BY')
        #
        parser.add_argument(
            '-window_size',
            '--window_size',
            help='Size of box for background noise estimation (in pixels).',
            metavar='Noise box size',
            type=int,
            default=None)
        #
        parser.add_argument(
            '-noise_box',
            '--noise_box',
            help='Coordinates of centre of box for noise estimation (in pixels, x y z). Leave blank for default.',
            metavar='Noise box coordinates',
            type=list,
            default=None)
        #
        parser.add_argument(
            '-meanMap',
            '--meanMap',
            help='3D map of noise means to be used for FDR control.',
            metavar='Mean map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-varianceMap',
            '--varianceMap',
            help='3D map of noise variances to be used for FDR control.',
            metavar='Variance map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-test_proc',
            '--test_proc',
            help="Choose between right, left and two-sided testing.",
            metavar='Test procedure',
            type=str,
            choices=['Left-sided',
                     'Right-sided',
                     'Two-sided'],
            default='Right-sided')
        #
        parser.add_argument(
            '-lowPassFilter',
            '--lowPassFilter',
            help="Low-pass filter the map at the given resoultion prior to FDR control.",
            metavar='Low-pass filter',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-ecdf',
            '--ecdf',
            help="Use empirical cumulative distribution function instead of the standard normal distribution.",
            metavar='Use ECDF',
            type=bool,
            default=None)
        #
        return parser

    def prepare_cmdline_args_validation(self):
                # Required args
        args = [
            '-input_map', str(self.args.fdr_map.value),
            '-input_pdb', str(self.args.input_model.value),
            #'-prune', str(self.args.prune.value),
            #'-valCA', str(self.args.valCA.value)
        ]
        #if self.args.fdr_map.value != None:
        #     args.append('-fdr_map')

        if self.args.prune.value:
            args.append('-prune')
        if self.args.valCA.value:
            args.append('-valCA')
        return args

    def prepare_cmdline_args_fdr(self):

        # Required args
        args = [
            '--em_map', str(self.args.em_map.value),
            '-method', METHODS[self.args.method.value],
            '--testProc', TEST_PROCS[self.args.test_proc.value]
        ]

        noise_box_coords = self.args.noise_box.value

        if noise_box_coords is not None and len(noise_box_coords) > 0:
            args.extend(['-noiseBox'] + noise_box_coords)

        def add_arg_if_not_none(name):
            arg = getattr(self.args, name.strip('-'))
            if arg is not None and str(arg.value) != 'None':
                args.extend([name, str(arg.value)])

        add_arg_if_not_none('--apix')
        add_arg_if_not_none('-locResMap')
        add_arg_if_not_none('--window_size')
        add_arg_if_not_none('--meanMap')
        add_arg_if_not_none('--varianceMap')
        add_arg_if_not_none('--lowPassFilter')

        if self.args.ecdf.value:
            args.append('-ecdf')

        return args

    def run_pipeline(self, job_id=None, db_inject=None):

        pl = []
        # Confidence Maps process
        if self.args.input_selection.value == 'unmasked map':
            args = self.prepare_cmdline_args_fdr()
            confidence_maps_process = process_manager.CCPEMProcess(
                name='Confidence maps',
                command=self.commands['confidence_maps_python'],
                args=args,
                location=self.job_location)
            pl.append([confidence_maps_process])
            map_basename = os.path.splitext(os.path.basename(self.args.em_map.value))[0]
            self.args.fdr_map.value = os.path.join(self.job_location,map_basename + '_confidenceMap.mrc')

        else : self.confidence_map = self.args.fdr_map.value#em_map.value

        # FDR Validation process
        args=self.prepare_cmdline_args_validation()
        fdr_validation_process = process_manager.CCPEMProcess(
            name='FDR validation',
            command=self.commands['fdr_validation_python'],
            args=args,
            location=self.job_location)
        pl.append([fdr_validation_process])
        # Results process
        fdrval_results_process = ResultsRun(
            command=self.commands['fdr_validation_results'][:],
            location=self.job_location,
            name='Results')
        pl.append([fdrval_results_process.process])
        
        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            db_inject=db_inject,
            database_path=self.database_path,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()


class ResultsRun(object):
    '''
    Wrapper for results generation.
    '''
    def __init__(self,
                 command,
                 location,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
         # Set process
        assert command is not None
        command += [location]
        
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            location=self.job_location,
            stdin=None)