#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import shutil
import time
import tempfile
import unittest

from ccpem_core import process_manager

from . import lafter_task

class Test(unittest.TestCase):
    '''
    Unit test for LAFTER task
    '''

    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_dir = tempfile.mkdtemp()
        os.chdir(self.test_dir)

    def tearDown(self):
        '''
        Remove temporary data.
        '''
        if os.path.exists(self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_lafter_binary_no_args(self):
        '''
        Check that:
        * the LAFTER binary can be found
        * it displays a usage message if run with no arguments
        '''
        # Set up and run task
        task = lafter_task.LafterTask()
        task.run_task(self.test_dir)

        count = 0
        while not os.path.exists(task.pipeline.json):
            time.sleep(0.01)
            count += 1
            assert count < 50, "No task json file found"

        # Wait for task to finish
        status = process_manager.get_process_status(task.pipeline.json)
        count = 0
        while status == 'running':
            time.sleep(0.1)
            status = process_manager.get_process_status(task.pipeline.json)
            count += 1
            assert count < 30, "Task running for too long"

        # Check status - expect this to fail because no args given
        assert status == 'failed'

        # Check output
        with open(task.get_process('LAFTER').stdout) as stdout:
            output = stdout.read()
        assert 'Usage:' in output
        assert 'lafter --v1 half_map1.mrc --v2 half_map2.mrc' in output
        assert 'Necessary maps not found or not specified' in output


if __name__ == '__main__':
    unittest.main()
