#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import mrcfile

from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import process_manager
from ccpem_core import settings
from ccpem_core.tasks import task_utils


class LafterTask(task_utils.CCPEMTask):
    '''
    CCPEM LAFTER Task.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='LAFTER',
        author='K. Ramlaul, C. M. Palmer, C. H. S. Aylett',
        version='1.1',
        description=(
            'Local de-noising of cryo-EM maps.<br>'
            'Please see the paper for details: '
            '<a href="https://doi.org/10.1016/j.jsb.2018.11.011">'
            'Ramlaul, Palmer & Aylett (2019)</a>'),
        short_description=(
            'Local de-noising of cryo-EM maps'),
        documentation_link='https://github.com/StructuralBiology-ICLMedicine/lafter',
        references=None)

    commands = {'lafter': settings.which('lafter')}

    def __init__(self, **kwargs):
        super(LafterTask, self).__init__(**kwargs)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-half_map_1',
            '--half_map_1',
            help='First half map for denoising (mrc format)',
            metavar='First half map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-half_map_2',
            '--half_map_2',
            help='Second half map for denoising (mrc format)',
            metavar='Second half map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-mask',
            '--mask',
            help='Mask to define particle area (mrc format)',
            metavar='Mask',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-particle_diameter',
            '--particle_diameter',
            help='Particle diameter in voxels (if no mask available)',
            metavar='Particle diameter',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-sharp',
            '--sharp',
            help='Additional sharpening (usually not necessary)',
            metavar='Additional sharpening',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-fsc',
            '--fsc',
            help='FSC cut-off threshold (default 0.143)',
            metavar='FSC cut-off threshold',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-downsample',
            '--downsample',
            help='Do not over-sample output maps',
            metavar='No over-sampling',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-overfitting',
            '--overfitting',
            help='Attempt to mitigate against the effects of overfitting from 3D refinement',
            metavar='Compensate for overfitting',
            type=bool,
            default=False)
        #
        return parser

    def run_pipeline(self, job_id=None, db_inject=None):

        # Set up arguments. This is very simplistic and does not attempt any
        # validation. If the args are incorrect, we just pass them through and
        # allow LAFTER to fail and complain.

        # Required args
        args = [
            '--v1', str(self.args.half_map_1.value),
            '--v2', str(self.args.half_map_2.value),
        ]

        # Optional float values
        mask = self.args.mask.value
        if mask is not None:
            args.extend(['--mask', str(mask)])
            # Check mask mode
            with mrcfile.open(mask, header_only=True) as mask_mrc:
                mode = mask_mrc.header.mode
                if mode != 2:
                    print("Warning! Mask file uses MRC mode {}, but LAFTER requires MRC"
                          " mode 2".format(mode))
        elif self.args.particle_diameter.value is not None:
            args.extend(['--particle_diameter', str(self.args.particle_diameter.value)])
        if self.args.fsc.value is not None:
            args.extend(['--fsc', str(self.args.fsc.value)])

        # Optional bool values
        if self.args.sharp.value:
            args.append('--sharp')
        if self.args.downsample.value:
            args.append('--downsample')
        if self.args.overfitting.value:
            args.append('--overfitting')

        # LAFTER process
        lafter_process = process_manager.CCPEMProcess(
            name='LAFTER',
            command=self.commands['lafter'],
            args=args,
            location=self.job_location)

        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=[[lafter_process]],
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            db_inject=db_inject,
            database_path=self.database_path,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

    def validate_args(self):
        # TODO: check for essential args
        # TODO: doesn't look like validate_args is actually called anywhere! need to fix, but could break things...
        pass
