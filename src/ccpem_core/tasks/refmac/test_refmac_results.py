#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import unittest, os, tempfile, shutil
from ccpem_core.test_data.tasks import refmac as refmac_test
import refmac_results

class RefmacResultsTest(unittest.TestCase):
    '''
    Unit test for Refmac results.
    '''
    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_data = os.path.dirname(refmac_test.__file__)
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        if os.path.exists(self.test_output):
            shutil.rmtree(self.test_output)

    def test_refmac_results(self):
        name = self.test_data+'/results/maptomtz_stdout.txt'
        print name
        assert os.path.exists(name)
        refmac_results.RefmacResultsViewer(
            job_location=self.test_output,
            mrc2mtz_stdout=self.test_data+'/results/maptomtz_stdout.txt',
            refine_stdout=self.test_data+'/results/refmacrefineglobal_stdout.txt',
            validate_hm1_stdout=self.test_data+'/results/refmacrefinehm1_stdout.txt',
            validate_hm2_stdout=self.test_data+'/results/refmacstatshm2_stdout.txt',
            fsc_stdout=self.test_data+'/results/refmacfschm1vshm2_stdout.txt')

    def test_refmac_fsc_table_reading(self):
        parser = refmac_results.RefmacFSCMapParser(stdout=self.test_data+'/results/mac_bug_refmacfschm1vshm2_stdout.txt')
        assert parser.fsc_table is not None

if __name__ == '__main__':
    unittest.main()
