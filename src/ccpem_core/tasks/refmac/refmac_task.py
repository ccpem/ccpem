#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

# ProSMART CCP4 implementation details
# http://www2.mrc-lmb.cam.ac.uk/groups/murshudov/content/prosmart/documentation.html#refmac
# http://www.ccp4.ac.uk/newsletters/newsletter48/articles/ProSmart/an_overview_of_prosmart.html

import os
import shutil
import copy
import json
import mrcfile

from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core.tasks.refmac import servalcat_results
from ccpem_core import ccpem_utils
from ccpem_core.settings import which
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core import settings

# From Garib:
# source em mb # generate electron form factors from X-ray. (Preferred)
# source em    # use electron from factors (Peng et al). In this case atomsf
#                should be changed to atomsf_electron.lib
refmac_source = 'source EM MB'

class Refmac(task_utils.CCPEMTask):
    '''
    CCPEM Refmac task.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='Refmac Servalcat',
        author=('Yamashita K, Palmer CM, Burnley T, Brown A, Long F, Nicholls RA, '
                'Long F, Toots J, Murshudov GN'),
        version='5.8.0103',
        description=(
            'Macromolecular refinement program. Servalcat is a wrapper for '
            'REFMAC5 that adds map sharpening, symmetry handling, difference '
            'maps and other functions. REFMAC5 is a program designed for '
            'REFinement of MACromolecular structures. It uses maximum '
            'likelihood and some elements of Bayesian statistics. '
            'N.B. requires CCP4.'),
        short_description=(
            'Atomic structure refinement.'),
        documentation_link=('https://www2.mrc-lmb.cam.ac.uk/groups/murshudov/content'
                            '/refmac/refmac_keywords.html'),
        references=None)

    commands = {
        'refmac': settings.which(program='refmac5'),
        'sfcheck': settings.which(program='sfcheck'),
        'pdbset':  settings.which(program='pdbset'),
        'libg': settings.which(program='libg'),
        'ccpem-python':  settings.which(program='ccpem-python')
    }


    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 verbose=False,
                 parent=None):
#        self.atomsf = os.path.join(
#            os.environ['CLIBD'],
#           'atomsf_electron.lib')
#        assert os.path.exists(self.atomsf)
        self.atomsf = None # Garib prefer to use 'source EM MB' rather than atomsf - so switching atomsf off

        super(Refmac, self).__init__(database_path=database_path,
                                     args=args,
                                     args_json=args_json,
                                     pipeline=pipeline,
                                     job_location=job_location,
                                     verbose=verbose,
                                     parent=parent)
        # XXX Debug
        # import servalcat
        # print 'Servalcat version:', servalcat.__version__
        # import gemmi
        # print 'Gemmi version:', gemmi.__version__

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-dc_mode',
            '--dc_mode',
            help='Refmac divide and conquer for multiple job submission (in testing)',
            type=bool,
            metavar='Multi PDBs/Maps',
            default=False)
        #
        parser.add_argument(
            '-dc_proto',
            '--dc_proto',
            help='Refmac divide and conquer for multiple job submission (in testing)',
            type=bool,
            metavar='Refmac DAC Proto',
            default=True)
        #
        parser.add_argument(
            '-libg',
            '--libg',
            help='Use LIBG to generate restraints for nucleic acids',
            metavar='Nucleic acids',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-libg_selection',
            '--libg_selection',
            help=('Specify nucleotide ranges for libg (please refer to libg'
                  ' documentation). If you ignore this field, all nucleic acids will be'
                  ' selected for restraint generation'),
            metavar='Nucleotide range (ignore to select all)',
            type=str,
            default='')
        #
        parser.add_argument(
            '-input_map',
            '--input_map',
            help=('Target input full map (mrc format). Use half maps instead if'
                  ' available.'),
            type=str,
            metavar='Input map',
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-half_map_refinement',
            '--half_map_refinement',
            help='Use half map refinement, if false use full map instead',
            type=bool,
            metavar='Half map refinement',
            default=True)
        #
        parser.add_argument(
            '-resolution',
            '--resolution',
            help='Resolution of input map (Angstrom)',
            metavar='Resolution',
            type=float,
            nargs='*',
            default=None)

        parser.add_argument(
            '-use_cluster',
            '--use_cluster',
            help='Submit jobs to cluster rather then run on local CPU',
            type=bool,
            metavar='Submit to cluster',
            default=False)

        parser.add_argument(
            '-numberCPUsOrNodes',
            '--numberCPUsOrNodes',
            help='Number of CPUs or cluster nodes for parallel execution',
            type=int,
            metavar='# of CPUs or nodes',
            default=1)

        parser.add_argument(
            '-timeQuant',
            '--timeQuant',
            help=('Waiting time before polling Refmac5 jobs - for quick unittesting'
                  ' only'),
            type=int,
            metavar='Time Quant',
            default=60)

        parser.add_argument(
            '-validate_on',
            '--validate_on',
            help=('Perform cross-validation with half maps to check for model'
                  ' overfitting'),
            metavar='Validate',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-half1',
            '--half_map_1',
            help='Half map 1 of input map (mrc format)',
            metavar='Half map 1',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-half2',
            '--half_map_2',
            help='Half map 2 of input map (mrc format)',
            type=str,
            metavar='Half map 2',
            default=None)
        #
        parser.add_argument(
            '-mask_for_fofc',
            '--mask_for_fofc',
            help='Mask for difference map calculation (mrc format)',
            type=str,
            metavar='Mask for Fo-Fc map',
            default=None)
        #
        parser.add_argument(
            '-start_pdb',
            '--start_pdb',
            help='Input coordinate file (pdb/cif format)',
            type=str,
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-lib_in',
            '--lib_in',
            help='Library dictionary file for ligands (lib/cif format)',
            metavar='Ligand dictionary',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-external_restraints_on',
            '--external_restraints_on',
            help='Use external restraints',
            metavar='Use restraints',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-restraints_files',
            '--restraints_files',
            help='External restraints files',
            type=str,
            metavar='Restraints',
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-external_weight_scales',
            '--external_weight_scales',
            help=('External restraints weight. Increasing this weight '
                  'increases the influence of external restraints during '
                  'refinement'),
            type=float,
            nargs='*',
            metavar='Weight',
            default=10.0)
        #
        parser.add_argument(
            '-external_weight_gmwts',
            '--external_weight_gmwts',
            help=('Geman-McClure parameter, which controls robustness to outliers.'
                  ' Increasing this value reduces the influence of outliers (i.e.'
                  ' restraints that are very different from the current interatomic'
                  ' distance)'),
            type=float,
            nargs='*',
            metavar='GMWT',
            default=0.02)
        #
        parser.add_argument(
            '-restraints_file',
            '--restraints_file',
            help='External restraints files',
            type=str,
            metavar='Restraints',
            default=None)
        #
        parser.add_argument(
            '-external_weight_scale',
            '--external_weight_scale',
            help=('External restraints weight. Increasing this weight '
                  'increases the influence of external restraints during '
                  'refinement'),
            type=float,
            metavar='Weight',
            default=10.0)
        #
        parser.add_argument(
            '-external_weight_gmwt',
            '--external_weight_gmwt',
            help=('Geman-McClure parameter, which controls robustness to outliers.'
                  ' Increasing this value reduces the influence of outliers (i.e.'
                  ' restraints that are very different from the current inter-atomic'
                  ' distance)'),
            type=float,
            metavar='GMWT',
            default=0.02)
#
        parser.add_argument(
            '-external_weight_dmx',
            '--external_weight_dmx',
            help='Maximum restraint inter-atomic distance (Angstroms)',
            type=float,
            metavar='Max restraint distance',
            default=4.2)
        #
        parser.add_argument(
            '-external_main_only',
            '--external_main_only',
            help=('Discards any side-chain restraints that may be present in the'
                  ' external restraints file'),
            type=bool,
            metavar='Main chain only',
            default=False)
        #
        parser.add_argument(
            '-masked_refinement_on',
            '--masked_refinement_on',
            help=('Trim map around supplied model file and perform refinement with this'
                  ' sub-volume.'),
            metavar='Masked refinement',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-mask_radius',
            '--mask_radius',
            help='Distance around molecule the map should be cut',
            type=float,
            default=3.0)
        #
        parser.add_argument(
            '-ncsc_relion_symbol',
            '--ncsc_relion_symbol',
            help='Point group symbol (using RELION conventions)',
            type=str,
            default="")
        #
        parser.add_argument(
            '-ncycle',
            '--ncycle',
            help='Number of refinement cycles',
            metavar='Refmac cycles',
            type=int,
            default=20)
        #
        parser.add_argument(
            '-weight',
            '--weight',
            help=('Relative weight of experimental restraints w.r.t. '
                  'geometric restraints.  Smaller values result in stricter '
                  'geometric restraints'),
            metavar='Auto weight scale',
            type=float,
            default=1.0)
        #
        parser.add_argument(
            '-weight_auto',
            '--weight_auto',
            help='Use auto-weighting by Servalcat (recommended)',
            metavar='Auto weight',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-symmetry_auto',
            '--symmetry_auto',
            help=('Automatically detect and apply symmetry (recommended). Use Global if'
                  ' the input map was reconstructed with strict symmetry, otherwise use'
                  ' Local if the model contains multiple copies of some part of the'
                  ' structure but the map was not symmetrised.'),
            metavar='Auto symmetry',
            choices=['None',
                     'Local',
                     'Global'
                     ],
            type=str,
            default='Local')
        #
        parser.add_argument(
            '-map_sharpen',
            '--map_sharpen',
            metavar='Sharpen / blur',
            help=('B-factor to apply to map. Negative B-factor to '
                  'sharpen, positive to blur, zero to leave map as '
                  'input'),
            type=float,
            default=None)
        #
        parser.add_argument(
            '-set_bfactor_on',
            '--set_bfactor_on',
            help='Reset all atomic B-factors to given value on input',
            metavar='Set B-factor',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-set_bfactor',
            '--set_bfactor',
            help='Set all input atomic B-factors to this value',
            metavar='B-factor',
            type=float,
            default=40.0)
        #
        parser.add_argument(
            '-jelly_body',
            '--jelly_body',
            help='Use jelly body restraints',
            type=bool,
            metavar='Jelly body',
            default=True)
        #
        parser.add_argument(
            '-hydrogens',
            '--hydrogens',
            help='Recreate hydrogens or use existing hydrogens or ignore hydrogens',
            metavar='Hydrogens',
            choices=[
                'Recreate',
                'Existing',
                'Ignore'],
            type=str,
            default='Recreate')
        #
        parser.add_argument(
            '-twist',
            '--twist',
            help='Helical refinement, angular rotation per subunit (deg)',
            metavar='Twist',
            type=float,
            default=0.0)
        #
        parser.add_argument(
            '-rise',
            '--rise',
            help='Helical refinement, axial rise per subunit (A)',
            metavar='rise',
            type=float,
            default=0.0)
        #
        parser.add_argument(
            '-keywords',
            '--keywords',
            help='REFMAC keywords for advanced options.',
            type=str,
            default='')
        #
        return parser

    def run_pipeline(self, job_id=None, db_inject=None, run=True):
        '''
        Generate job classes and process.  Run=false for reloading.
        '''
        pl = []

        if self.args.start_pdb.value.endswith("cif"):
            self.model_ext = ".mmcif"
        else:
            self.model_ext = ".pdb"

        self.libg_restraints = False
        self.libg_restraints_path = []
        if self.args.libg.value:
            if self.args.libg_selection.value:
                if len(self.args.libg_selection.value.strip()) > 0:
                    self.process_libg = LibgRestraints(
                        command=self.commands['libg'],
                        job_location=self.job_location,
                        name='DNA or RNA basepair restraints',
                        pdb_path=self.args.start_pdb.value,
                        u = self.args.libg_selection.value.strip())
                else:
                    self.process_libg = LibgRestraints(
                        command=self.commands['libg'],
                        job_location=self.job_location,
                        name='DNA or RNA basepair restraints',
                        pdb_path=self.args.start_pdb.value)
            else:
                self.process_libg = LibgRestraints(
                    command=self.commands['libg'],
                    job_location=self.job_location,
                    name='DNA or RNA basepair restraints',
                    pdb_path=self.args.start_pdb.value)
            self.libg_restraints = True
            self.libg_restraints_path = [self.process_libg.libg_restraints_path]
            pl.append([self.process_libg.process])

        if self.args.masked_refinement_on():
            # Refinement parameters: Masked
            mode = 'Masked'
            name='Refmac refine (masked) via Servalcat'
        else:
            # Refinement parameters: Global
            mode = 'Global'
            name = 'Refmac refine (global) via Servalcat'

        # Remove remove maps dependant on half_map_refinement
        if self.args.half_map_refinement():
            self.args.input_map.value = None
        else:
            self.args.half_map_1.value = None
            self.args.half_map_2.value = None
            self.args.validate_on.value = False

        # Invert sharpening value for refmac
        if self.args.map_sharpen() is None:
            sharp = 0
        else:
            sharp = -1.0*self.args.map_sharpen()

        # Refine process
        self.process_refine = RefmacRefineServal(
            command=self.commands['ccpem-python'],
            job_location=self.job_location,
            pdb_path=self.args.start_pdb(),
            resolution=self.args.resolution(),
            halfmap_paths=[self.args.half_map_1(), self.args.half_map_2()],
            mask_for_fofc=self.args.mask_for_fofc(),
            map_path=self.args.input_map(),
            mode=mode,
            name=name,
            mask_radius=self.args.mask_radius(),
            pg_symbol=self.args.ncsc_relion_symbol.value,
            weight=self.args.weight(),
            weight_auto=self.args.weight_auto(),
            symmetry_auto=self.args.symmetry_auto(),
            sharp=sharp,
            set_bfactor=self.args.set_bfactor_on(),
            atom_bfactor=self.args.set_bfactor(),
            ncycle=self.args.ncycle.value,
            external_restraints_on=self.args.external_restraints_on(),
            external_restraints_path=self.args.restraints_files(),
            external_weight_scale=self.args.external_weight_scales(),
            external_weight_gmwt=self.args.external_weight_gmwts(),
            external_weight_dmax=self.args.external_weight_dmx(),
            external_main_chain=self.args.external_main_only(),
            lib_path=self.args.lib_in(),
            jelly_body_on=self.args.jelly_body(),
            hydrogens=self.args.hydrogens(),
            keywords=self.args.keywords(),
            libg_restraints=self.libg_restraints,
            libg_restraints_path=self.libg_restraints_path,
            run_validation=self.args.validate_on(),
            twist=self.args.twist(),
            rise=self.args.rise())
            #
        pl.append([self.process_refine.process])

        # Generate results
        servalcat_results_path = os.path.realpath(servalcat_results.__file__)
        servalcat_results_process = process_manager.CCPEMProcess(
            name='Refmac results',
            command=self.commands['ccpem-python'],
            args=[servalcat_results_path, self.job_location, mode],
            location=self.job_location)
        pl.append([servalcat_results_process])

        if run:
            self.pipeline = process_manager.CCPEMPipeline(
                pipeline=pl,
                job_id=job_id,
                args_path=self.args.jsonfile,
                location=self.job_location,
                database_path=self.database_path,
                db_inject=db_inject,
                taskname=self.task_info.name,
                title=self.args.job_title.value,
                verbose=self.verbose)
            self.pipeline.start()


class SetModelCell(object):
    '''
    Set input PDB cell and scale cards to input map.
    '''
    def __init__(self,
                 job_location,
                 pdb_path,
                 map_path,
                 cella=None,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.pdb_path = ccpem_utils.get_path_abs(pdb_path)
        self.map_path = map_path
        if map_path is not None:
            self.map_path = ccpem_utils.get_path_abs(map_path)
        self.pdbout_path = os.path.join(self.job_location, 'starting_model.pdb')
        self.cifout_path = os.path.join(self.job_location, 'starting_model.cif')
        #
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        #
        command = which('ccpem-python')
        self.args = [
            '-m', 'ccpem_core.model_tools.command_line',
            '--model', self.pdb_path,
            '--output_pdb',
            '--output_cif',
            '--output_name', os.path.splitext(self.cifout_path)[0]
        ]
        if map_path:
            self.args += ['--set_cell_from_map', self.map_path]
        elif cella:
            self.args += ['--cella', cella]
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location)


class LibgRestraints(object):
    '''
    Generates DNA/RNA restraints using libg; later they will be used by Refmac.
    '''
    def __init__(self,
                 command,
                 job_location,
                 pdb_path,
                 name=None,
                 w='',
                 u=''):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.pdb_path = pdb_path
        self.w = w
        self.u = u

        self.libg_u_path =  os.path.join(self.job_location,
                                        'libg_u.txt')
        if len(self.w) > 0:
            self.libg_restraints_path =  os.path.join(self.job_location,
                                            'libg_restraints_%s.txt' % self.w)
        else:
            self.libg_restraints_path =  os.path.join(self.job_location,
                                            'libg_restraints.txt')
        #
        self.set_stdin()
        self.set_args()
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin)

    def set_args(self):
        if os.path.splitext(self.pdb_path)[-1].lower() in ['.cif','.mmcif']:
            self.args = ['-c', self.pdb_path]
        else:
            self.args = ['-p', self.pdb_path]
        self.args += ['-o', self.libg_restraints_path]
        if len('w') > 0:
            self.args += ['-w', self.w]
        if len('u') > 0:
            uFile = open(self.libg_u_path, 'w')
            uFile.write(self.u)
            uFile.close()
            self.args += ['-u', self.libg_u_path]

    def set_stdin(self):
        self.stdin = '\n'

class RefmacMapToMtz(object):
    '''
    Refmac process to convert map to mtz
    '''
    modes = ['Global',  'Masked']

    def __init__(self,
                 command,
                 job_location,
                 map_path,
                 resolution=None,
                 mode='Global',
                 name=None,
                 pdb_path=None,
                 pdbout_path='shifted_local.pdb',
                 lib_path=None,
                 hklout_path=None,
                 mrad=3.0,
                 atomsf_path=None,
                 blur_array=None,
                 sharp_array=None,
                 on_finish_custom=None,
                 stdin = None):
        self.command = command
        self.job_location = job_location
        self.map_path = map_path
        self.resolution = resolution
        self.mode = mode
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__ + self.mode
        assert self.mode in self.modes
        self.lib_path = lib_path
        self.pdb_path = pdb_path
        if self.mode == 'Masked':
            assert self.pdb_path is not None
        self.pdbout_path = os.path.join(
            self.job_location,
            pdbout_path)
        self.mrad = mrad
        self.atomsf_path = atomsf_path
        self.blur_array = blur_array
#         if self.blur_array == [0.0]:
#             self.blur_array = None
        self.sharp_array = sharp_array
#         if self.sharp_array == [0.0]:
#             self.sharp_array = None
        self.stdin = stdin
        self.hklout_path = hklout_path
        if self.hklout_path is None:
            self.hklout_path = os.path.join(self.job_location,
                                            'starting_map.mtz')
        #
        self.set_args()
        self.set_stdin()
        #
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=self.command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin,
            on_finish_custom=on_finish_custom)

    def set_args(self):
        self.args = ['mapin', self.map_path]
        self.args += ['hklout', self.hklout_path]
        if self.atomsf_path is not None:
            self.atomsf_path = ccpem_utils.get_path_abs(self.atomsf_path)
            self.args += ['atomsf', self.atomsf_path]
        if self.lib_path is not None:
            self.args += ['libin', self.lib_path]
        if self.pdb_path is not None:
            self.args += ['xyzin', self.pdb_path]
            self.args += ['xyzout', self.pdbout_path]

    def set_stdin(self):
        if self.stdin:
            self.stdin += '\nmode sfcalc'
        else: self.stdin = 'mode sfcalc'
        self.stdin += '\n' + refmac_source
        if self.resolution is not None:
            self.stdin += '\nreso {0}'.format(self.resolution)
        if self.mode == 'Masked':
            self.stdin += '\nsfcalc mrad {0}'.format(self.mrad)
            self.stdin += '\nsfcalc shift'
        if self.blur_array is not None and len(self.blur_array) > 0:
            blur_str = '\nsfcalc blur'
            for b in self.blur_array:
                if b is not None:
                    blur_str += ' {0}'.format(b)
            self.stdin += blur_str
        if self.sharp_array is not None and len(self.sharp_array) > 0:
            if self.sharp_array[0] is not None:
                sharp_str = '\nsfcalc sharp'
                for b in self.sharp_array:
                    if b is not None:
                        sharp_str += ' {0}'.format(b)
                self.stdin += sharp_str
        self.stdin += '\nend'


class RefmacRefineServal(object):
    '''
    Refmac refinement job using Servalcat.  Global or masked refinement.
    '''
    modes = ['Global',  'Masked']
    def __init__(self,
                 command,
                 job_location,
                 pdb_path,
                 resolution,
                 halfmap_paths,
                 map_path,
                 mask_for_fofc,
                 mode='Global',
                 name=None,
                 sharp=None,
                 mask_radius=None,
                 pg_symbol=None,
                 lib_path=None,
                 ncycle=20,
                 set_bfactor=False,
                 atom_bfactor=None,
                 jelly_body_on=True,
                 jelly_body_sigma=0.01,
                 jelly_body_dmax=4.2,
                 hydrogens=False, # not true/false!
                 external_restraints_on=False,
                 external_restraints_path=None,
                 external_weight_scale=None,
                 external_weight_dmax=4.2,
                 external_weight_gmwt=None,
                 external_main_chain=False,
                 weight=0.0001,
                 weight_auto=True,
                 symmetry_auto='Local',
                 keywords=None,
                 libg_restraints=False,
                 libg_restraints_path=None,
                 run_validation=False,
                 twist=0.0,
                 rise=0.0,
                 pdbout_path='refined.pdb'): # the pdbout_path for buccaneer
        self.command = command
        self.job_location = job_location
        self.pdb_path = pdb_path
        self.resolution = resolution
        self.mode = mode
        assert self.mode in self.modes
        self.halfmap_paths = halfmap_paths
        self.map_path = map_path
        self.output_prefix = os.path.splitext(pdbout_path)[0]
        self.pdbout_path = os.path.join(self.job_location, pdbout_path) # "refined.pdb")
        self.cifout_path = os.path.splitext(self.pdbout_path)[0] + '.mmcif'
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__ + self.mode
        self.halfmap_paths = halfmap_paths
        self.map_path = map_path
        self.mask_for_fofc = mask_for_fofc
        self.sharp = sharp
        self.mask_radius = mask_radius
        self.pg_symbol = pg_symbol
        self.lib_path = lib_path
        self.ncycle = ncycle
        self.set_bfactor = set_bfactor
        self.atom_bfactor = atom_bfactor
        self.weight = weight
        self.weight_auto = weight_auto
        self.symmetry_auto = symmetry_auto
        self.jelly_body_on = jelly_body_on
        self.jelly_body_sigma = jelly_body_sigma
        self.jelly_body_dmax = jelly_body_dmax
        self.hydrogens = hydrogens
        self.external_restraints_on = external_restraints_on
        self.external_restraints_path = external_restraints_path
        if self.external_restraints_path is not None:
            if type(self.external_restraints_path) is list:
                absPathList = []
                for path in self.external_restraints_path:
                    absPathList.append(ccpem_utils.get_path_abs(
                    path))
                self.external_restraints_path = absPathList
        self.external_weight_scale = external_weight_scale
        self.external_weight_dmax = external_weight_dmax
        self.external_weight_gmwt = external_weight_gmwt
        self.external_main_chain = external_main_chain
        self.keywords = keywords
        self.stdin = None
        self.libg_restraints = libg_restraints
        self.libg_restraints_path = libg_restraints_path
        self.twist = twist
        self.rise = rise
        self.run_validation = run_validation
        #
        self.set_process()

    def set_process(self):
        self.set_args()
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=self.command,
            args=self.args,
            location=self.job_location)

    def set_args(self):
        self.args = ['-m', 'servalcat.command_line', 'refine_spa']
        self.args += ['--show_refmac_log']
        self.args += ['--model', self.pdb_path]
        if all(self.halfmap_paths):
            self.args += ['--halfmaps', self.halfmap_paths[0], self.halfmap_paths[1]]
        if self.map_path is not None:
            self.args += ['--map', self.map_path]
        if self.mask_for_fofc is not None:
            self.args += ['--mask_for_fofc', self.mask_for_fofc]
        if self.lib_path is not None:
            self.args += ['--ligand', self.lib_path]
        if self.mode != 'Masked':
            self.args += ['--no_mask']
        elif self.mask_radius is not None:
            self.args += ['--mask_radius', '{0}'.format(self.mask_radius)]
        if self.pg_symbol is not None:
            if self.pg_symbol.strip():
                self.args += ['--pg', self.pg_symbol]
        # Set hydrogen option
        if self.hydrogens == 'Recreate':
            self.args += ['--hydrogen', 'all']
        elif self.hydrogens == 'Existing':
            self.args += ['--hydrogen', 'yes']
        elif self.hydrogens == 'Ignore':
            self.args += ['--hydrogen', 'no']
        if self.sharp is not None and self.sharp != 0:
            self.args  += ['--blur', '{0}'.format(-self.sharp)]
        self.args += ['--ncycle', '{0}'.format(self.ncycle)]
        if self.weight is not None and not self.weight_auto:
            self.args += ['--weight_auto_scale', '{0}'.format(self.weight)]
        if self.symmetry_auto in ('Local', 'Global'):
            self.args += ['--ncsr', self.symmetry_auto.lower()]
        if self.set_bfactor and self.atom_bfactor is not None:
            self.args += ['--bfactor', '{0}'.format(self.atom_bfactor)]
        self.args += ['--resolution', '{0}'.format(self.resolution)]
        if self.jelly_body_on:
            self.args += ['--jellybody', '--jellybody_params',
                          '{0}'.format(self.jelly_body_sigma),
                          '{0}'.format(self.jelly_body_dmax)]
        if self.libg_restraints:
            for fileName in self.libg_restraints_path:
                self.args += ['--keyword_file', fileName]
        if self.output_prefix is not None:
            self.args += ['--output_prefix', self.output_prefix]

# External restraints
        if self.external_restraints_on:

            if (len(self.external_weight_scale) == len(self.external_weight_gmwt)) and (len(self.external_weight_scale) == len(self.external_restraints_path)):
                exte = []
                for erp, ews, ewg in zip(self.external_restraints_path, self.external_weight_scale, self.external_weight_gmwt):
                    exte.append(dict(use="main" if self.external_main_chain else "all",
                                     dmax=self.external_weight_dmax,
                                     weight_scale=ews,
                                     weight_gmwt=ewg,
                                     file=erp))

                filename = os.path.join(self.job_location,'external_restraints.json')
                json.dump(exte, open(filename, "w"), indent=2)
                self.args += ['--external_restraints_json', filename]
            else:
                print 'Something wrong with external restraints, they will not be used'
                print self.external_restraints_path
                print self.external_weight_scale
                print self.external_weight_gmwt
# end of external restraints

            self.args += ['--keywords', 'MONI DIST 1000000']
        # Helical refinement
        if self.twist or self.rise > 0.0:
            self.args += ['--twist', self.twist]
            self.args += ['--rise', self.rise]

        if self.keywords:
            self.args += ['--keywords'] + self.keywords.splitlines()

        if self.run_validation:
            self.args += ['--cross_validation']

class RefmacRefine(object):
    '''
    Refmac refinement job.  Global or masked refinement.
    '''
    modes = ['Global',  'Masked']

    def __init__(self,
                 command,
                 job_location,
                 pdb_path,
                 resolution,
                 mtz_path,
                 mode='Global',
                 pdbout_path='refined.pdb',
                 name=None,
                 atomsf_path=None,
                 sharp=None,
                 lib_path=None,
                 ncycle=20,
                 set_bfactor=False,
                 atom_bfactor=None,
                 jelly_body_on=True,
                 jelly_body_sigma=0.01,
                 jelly_body_dmax=4.2,
                 hydrogens=False,
                 external_restraints_on=False,
                 external_restraints_path=None,
                 external_weight_scale=None,
                 external_weight_dmax=4.2,
                 external_weight_gmwt=None,
                 external_main_chain=False,
                 vector_diff_map=False,
                 weight=0.0001,
                 symmetry_auto='Local',
                 keywords=None,
                 libg_restraints=False,
                 libg_restraints_path=None,
                 output_hkl=False):
        self.command = command
        self.job_location = job_location
        self.pdb_path = pdb_path
        self.resolution = resolution
        self.mode = mode
        assert self.mode in self.modes
        self.mtz_path = mtz_path
        self.pdbout_path = os.path.join(self.job_location,
                                        pdbout_path)
        self.cifout_path = os.path.splitext(self.pdbout_path)[0] + '.mmcif'
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__ + self.mode
        self.mtz_path = os.path.join(self.job_location,
                                     self.mtz_path)
        self.atomsf_path = atomsf_path
        self.sharp = sharp
        self.lib_path = lib_path
        self.ncycle = ncycle
        self.set_bfactor = set_bfactor
        self.atom_bfactor = atom_bfactor
        self.weight = weight
        self.symmetry_auto = symmetry_auto
        self.jelly_body_on = jelly_body_on
        self.jelly_body_sigma = jelly_body_sigma
        self.jelly_body_dmax = jelly_body_dmax
        self.hydrogens = hydrogens
        self.external_restraints_on = external_restraints_on
        self.external_restraints_path = external_restraints_path
        if self.external_restraints_path is not None:
            if type(self.external_restraints_path) is list:
                absPathList = []
                for path in self.external_restraints_path:
                    absPathList.append(ccpem_utils.get_path_abs(
                    path))
                self.external_restraints_path = absPathList
        self.external_weight_scale = external_weight_scale
        self.external_weight_dmax = external_weight_dmax
        self.external_weight_gmwt = external_weight_gmwt
        self.external_main_chain = external_main_chain
        self.vector_diff_map = vector_diff_map
        self.keywords = keywords
        self.stdin = None
        self.libg_restraints=libg_restraints
        self.libg_restraints_path=libg_restraints_path
        self.output_hkl = output_hkl
        #
        self.set_process()

    def set_process(self):
        self.set_args()
        self.set_stdin()
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=self.command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin)

    def set_args(self):
        self.args = ['xyzin', self.pdb_path]
        self.args += ['hklin', self.mtz_path]
        if self.lib_path is not None:
            self.args += ['libin', self.lib_path]
        self.args += ['xyzout', self.pdbout_path]
        if self.atomsf_path is not None:
            self.atomsf_path = ccpem_utils.get_path_abs(self.atomsf_path)
            self.args += ['atomsf', self.atomsf_path]
        if self.output_hkl:
            self.hklout_path = os.path.join(self.job_location,'refine.mtz')
            self.args += ['hklout', self.hklout_path]

    def set_stdin(self):
        self.stdin = 'labin FP=Fout0 PHIB=Pout0'
        if self.mode == 'Masked':
            self.stdin += '\n@shifts.txt'
        if self.hydrogens:
            self.stdin += '\nmake hydr all'
            self.stdin += '\nmake hout no' # do NOT write hydrogens out
        else:
            self.stdin += '\nmake hydr no'
        self.stdin += '\nsolvent no'
        self.stdin += '\n' + refmac_source
        # Keyword to fix smooth gauss error
#         out += '\nscale lsscale function lsq'
        if self.vector_diff_map:
            self.stdin += '\nmapc vector'
        if self.sharp is not None:
            if self.sharp != 0:
                self.stdin += '\nrefine sharp {0}'.format(self.sharp)
        self.stdin += '\nncycle {0}'.format(self.ncycle)
        if self.weight is not None:
            self.stdin += '\nweight matrix {0}'.format(self.weight)
        else:
            self.stdin += '\nweight auto'
        if self.symmetry_auto == 'Local':
            self.stdin += '\nncsr local'
        elif self.symmetry_auto == 'Global':
            self.stdin += '\nncsr global'
        if self.set_bfactor and self.atom_bfactor is not None:
            self.stdin += '\nBFACtor SET {0}'.format(self.atom_bfactor)
        self.stdin += '\nreso {0}'.format(self.resolution)
        if self.jelly_body_on:
            if self.jelly_body_on:
                self.stdin += '\nridge dist sigma {0}'.format(
                    self.jelly_body_sigma)
                self.stdin += '\nridge dist dmax {0}'.format(
                    self.jelly_body_dmax)
        if self.libg_restraints:
            for fileName in self.libg_restraints_path:
                self.stdin += '\n@' + fileName

# External restraints
        if self.external_restraints_on:

            if (len(self.external_weight_scale) == len(self.external_weight_gmwt)) and (len(self.external_weight_scale) == len(self.external_restraints_path)):
                if self.external_main_chain:
                    self.stdin += '\nEXTERNAL USE MAIN'
                self.stdin += '\nEXTERNAL DMAX {0}'.format(
                    self.external_weight_dmax)

                for erp, ews, ewg in zip(self.external_restraints_path, self.external_weight_scale, self.external_weight_gmwt):
                    self.stdin += '\nEXTERNAL WEIGHT SCALE {0}'.format(ews)
                    self.stdin += '\nEXTERNAL WEIGHT GMWT {0}'.format(ewg)
                    self.stdin += '\n@' + erp

            else:
                print 'Something wrong with external restraints, they will not be used'
                print self.external_restraints_path
                print self.external_weight_scale
                print self.external_weight_gmwt
# end of external restraints

            self.stdin += '\nMONI DIST 1000000'
        if self.keywords is not None:
            self.stdin += '\n' + self.keywords
        self.stdin += '\nend'



class RefmacSfcalcCrd(object):
    '''
    Calculate sfcalc (mtz) from coordinate input.
    '''
    command = 'refmac5'
    def __init__(self,
                 job_location,
                 pdb_path,
                 lib_in=None,
                 resolution=None,
                 name=None,
                 command = None,
                 stdin = None):
        self.job_location = job_location
        self.pdb_path = pdb_path
        self.lib_in = lib_in
        self.resolution = resolution
        self.stdin = stdin
        if name is None:
            self.name = self.__class__.__name__
        else:
            self.name = name
        if command is not None:
            self.command = command
        self.set_args()
        self.set_stdin()

        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=self.command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin)

    def set_args(self):
        self.args = ['xyzin', self.pdb_path]
        if self.lib_in is not None:
            self.args += ['libin', self.lib_in]

    def set_stdin(self):
        if self.stdin:
            self.stdin += '\nmode sfcalc'
        else:
            self.stdin = 'mode sfcalc'
        self.stdin += '\nsfcalc cr2f'
        self.stdin += '\n' + refmac_source
        self.stdin += '\nresol {0}'.format(self.resolution)
        self.stdin += '\nend'


class PDBSetShake(object):
    '''
    PDBSet process to shake structure for cross validation.
    '''
    def __init__(self,
                 job_location,
                 pdb_path,
                 pdbout_path=None,
                 name=None,
                 shift=0.5):
        command = which('pdbset')
        assert command is not None
        self.job_location = job_location
        self.pdb_path = pdb_path
        self.pdbout_path = pdbout_path
        if self.pdbout_path is None:
            self.pdbout_path = os.path.join(self.job_location,
                                            'shaked.pdb')
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.stdin = None
        #
        self.set_args()
        self.set_stdin(shift)
        #
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin)

    def set_args(self):
        self.args = ['xyzin', self.pdb_path]
        self.args += ['xyzout', self.pdbout_path]
        
            

    def set_stdin(self, shift):
        self.stdin = ''
        if os.path.splitext(self.pdb_path)[-1].lower() in ['.mmcif','.cif']:
            self.stdin = '\nOUTPUT CIF\n'
        self.stdin += '\nnoise {}\nend'.format(shift)
        


class RefmacFSC(object):
    '''
    Refmac process to shift back coordinates after masked refinement
    '''
    def __init__(self,
                 command,
                 job_location,
                 map1_path,
                 map2_path,
                 resolution=None,
                 name=None,
                 atomsf_path=None):
        self.command = command
        self.job_location = job_location
        self.map1_path = map1_path
        self.map2_path = map2_path
        self.resolution = resolution
        self.atomsf_path = atomsf_path
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.stdin = None
        #
        self.set_args()
        self.set_stdin()
        #
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=self.command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin)

    def set_args(self):
        self.args = ['mapin1', self.map1_path]
        self.args += ['mapin2', self.map2_path]
        if self.atomsf_path is not None:
            self.atomsf_path = ccpem_utils.get_path_abs(self.atomsf_path)
            self.args += ['atomsf', self.atomsf_path]

    def set_stdin(self):
        self.stdin = '\nmode sfcalc'
        self.stdin += '\nsfcalc fsc'
        if self.resolution is not None:
            self.stdin += '\nRESO {0}'.format(self.resolution)
        self.stdin += '\nend'


class DivideAndConquer(object):
    '''
    Divide and Conquer script for refinement of large complexes (several PDBs and several maps)
    '''
    def __init__(self,
                 command,
                 job_location,
                 option_l = None,
                 option_cluster = False,
                 option_nCPUsOrNodes = 1,
                 option_ncyc = None,
                 option_timeQuant = None,
                 name = None,
                 lib_in = None,
                 sharp = None,
                 weight  = None,
                 weight_auto = True,
                 set_bfactor  = None,
                 atom_bfactor = None,
                 jelly_body_on = None,
                 jelly_body_sigma = 0.01,
                 jelly_body_dmax = 4.2,
                 hydrogens=False,
                 keywords  = None ):
        self.command = command
        self.job_location = job_location

        self.option_l = option_l
        self.option_nCPUsOrNodes = option_nCPUsOrNodes
        self.option_cluster = option_cluster
        self.option_ncyc = option_ncyc
        self.option_timeQuant = option_timeQuant

        self.lib_in = lib_in
        self.sharp = sharp
        self.weight = weight
        self.weight_auto = weight_auto
        self.set_bfactor  = set_bfactor
        self.atom_bfactor = atom_bfactor
        self.jelly_body_on = jelly_body_on
        self.jelly_body_sigma = jelly_body_sigma
        self.jelly_body_dmax = jelly_body_dmax
        self.hydrogens = hydrogens
        self.keywords  = keywords

        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.stdin = None
        #
        self.set_args()
        self.set_stdin()
        #

        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=self.command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin)

    def set_args(self):
        self.args = ['-l', self.option_l]
        self.args += ['-i']
        if self.option_cluster:
            self.args += ['-cluster', self.option_nCPUsOrNodes]
        else:
            self.args += ['-cpu', self.option_nCPUsOrNodes]
        if self.option_ncyc is not None:
            self.args += ['-ncyc', self.option_ncyc]
        if self.option_timeQuant is not None:
            self.args += ['-time', self.option_timeQuant]
        if self.lib_in is not None:
            self.args += ['-lib_in', self.lib_in]



    def set_stdin(self):
        self.stdin = 'labin FP=Fout0 PHIB=Pout0'
        if self.hydrogens:
            self.stdin += '\nmake hydr all'
            self.stdin += '\nmake hout no' # do NOT write hydrogens out
        else:
            self.stdin += '\nmake hydr no'
        self.stdin += '\nsolvent no'
        self.stdin += '\nmake segid yes'
        self.stdin += '\n@shifts.txt'
        if self.sharp is not None:
            if self.sharp != 0:
                self.stdin += '\nrefine sharp {0}'.format(self.sharp)
        if self.weight_auto:
            self.stdin += '\nweight auto'
        else:
            if self.weight is not None:
                self.stdin += '\nweight matrix {0}'.format(self.weight)
        if self.set_bfactor and self.atom_bfactor is not None:
            self.stdin += '\nBFACtor SET {0}'.format(self.atom_bfactor)
        if self.jelly_body_on:
            if self.jelly_body_on:
                self.stdin += '\nridge dist sigma {0}'.format(
                    self.jelly_body_sigma)
                self.stdin += '\nridge dist dmax {0}'.format(
                    self.jelly_body_dmax)
            self.stdin += '\nMONI DIST 1000000'
        if self.keywords is not None:
            self.stdin += '\n' + self.keywords
        self.stdin += '\nend\n'
