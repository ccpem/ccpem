#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

# N.B. to run in google chrome use following flag
# google-chrome index.html --allow-file-access-from-files


# Refmac results - to do
# 1) Add mean fsc:
#  -> Average Fourier shell correlation
#  -> Marked up as:
#    ->  $TEXT:Result: $$ Final results $$
# rvapi - to do
# 1) Set focus for single tab (i.e. to show log)
# 2) Intermittent display bug (e.g. chrome vs firefox)

import os
import sys
import collections
import math
import fileinput
import shutil
import re
import numpy as np
import json
import pandas as pd
import pyrvapi
from ccpem_core import ccpem_utils
from ccpem_core.data_model import metadata_utils
from ccpem_core.ccpem_utils.ccp4_log_parser import smartie
from PyQt4 import QtCore, QtGui, QtWebKit


# Ask Eugene...
# N.B. large log files take a long time to run.
#     e.g. typical refmac refine log is too big at ~400K

# Resolution label (equivalent to <4SSQ/LL>
ang_min_one = (ur'Resolution (\u00c5-\u00B9)').encode('utf-8')
angstrom_label = (ur'Resolution (\u00c5)').encode('utf-8')

# Ignore pandas unicode warnings
import warnings
warnings.filterwarnings('ignore')

class RefmacResultsViewer(object):
    '''
    Refmac results viewer for RVAPI
        refine_process = which program in concatenated log file corresponds
            to refmac refinement process
    '''
    def __init__(self,
                 job_location='',
                 mrc2mtz_stdout='',
                 refine_stdout='',
                 validate_hm1_stdout='',
                 validate_hm2_stdout='',
                 fsc_json=''):
        self.job_location = job_location
        self.mrc2mtz_stdout = mrc2mtz_stdout
        self.refine_stdout = refine_stdout
        self.validate_hm1_stdout = validate_hm1_stdout
        self.validate_hm2_stdout = validate_hm2_stdout
        # Json metadata output from Servalcat
        # Includes:
        #    Model v Halfmap 1 FSC "fsc_model_half1"
        #    Model v Halfmap 2 FSC "fsc_model_half2"
        #    Halfmap 1 v Halfmap 2 FSC "fsc_half"
        self.fsc_json = fsc_json
        #
        self.refine_results = None
        self.directory = os.path.join(self.job_location,
                                      'rvapi_data')
        if os.path.exists(self.directory):
            shutil.rmtree(self.directory)
        ccpem_utils.check_directory_and_make(self.directory)
        self.index = os.path.join(self.directory, 'index.html')
        # XXX debug
        ccp4 = os.environ['CCPEM']
        share_jsrview = os.path.join(ccp4, 'share', 'jsrview')
        # Setup pages
        pyrvapi.rvapi_init_document(
            self.job_location, self.directory,
            self.job_location, 1, 4, share_jsrview,
            None, None, None, None)
        pyrvapi.rvapi_flush()

        # Set results
        self.set_results()

    @staticmethod
    def fix_refmac_fsc_table(stdout):
        '''
        Fix missing column headings for fsc table
        '''
        error_line = ' 2sin(th)/l 2sin(th)/l NREF sigma  FSC FSCT PHdiff cos(PHdiff) sigmaSig ZZ TT co\n'
        fixed_line = ' 2sin(th)/l 2sin(th)/l NREF sigma  FSC FSCT PHdiff cos(PHdiff) sigmaSig ZZ TT cor(|F1|,|F2|) $$'
        ignore_line = ' r(|F1|,|F2|) $$\n'
        #
        for line in fileinput.input(stdout, inplace=True):
            if line == error_line:
                print fixed_line,
            elif line == ignore_line:
                pass
            else:
                print line,
        fileinput.close()

    def set_results(self):
        # Get mrc2mtz results (power spectrum)
        if os.path.exists(self.mrc2mtz_stdout):
            self.mrc2mtz_results = RefmacPowerSpectrumParser(
                stdout=self.mrc2mtz_stdout)
        else:
            self.mrc2mtz_results = None

        # Get refinement results
        if os.path.exists(self.refine_stdout):
            self.refine_results = RefmacRefineResultsParser(
                stdout=self.refine_stdout)
            if self.refine_results.refine_results is not None:
                self.set_refine_results_summary()

        validate_processes = [
            os.path.exists(self.validate_hm1_stdout),
            os.path.exists(self.fsc_json)]
        if all(validate_processes):
            self.refine_hm1_results = RefmacRefineResultsParser(
                stdout=self.validate_hm1_stdout)
            #
            with open(self.fsc_json) as f:
                data = json.load(f)
            self.validation_md = metadata_utils.MetaDataTable(data)
            self.set_validation_results()

    def set_log(self, path):
        '''
        Set log text file display.
        '''
        pyrvapi.rvapi_add_tab('log_tab', 'Log file', False)
        pyrvapi.rvapi_append_content(path, True, 'log_tab') # To do_pageTop replaces tab1
        pyrvapi.rvapi_flush()

    def set_refine_results_summary(self):
        '''
        Set summary of refinement results from metadata table.
        '''
#         validate_tab = 'tab3'
#         validate_sec = 'sec_hm1'
#         validate_table = 'table_hm1'
#         # Setup refine_results (summary, graphs and output files)
#         pyrvapi.rvapi_add_header('Refmac Results')
#         pyrvapi.rvapi_add_tab(validate_tab, 'Validation', True)
#         pyrvapi.rvapi_add_section(validate_sec,
#                                   'Refinement summary',
#                                   validate_tab,
#                                   0, 0, 1, 1, True)
#
        # Set table
        refine_tab = 'refine_tab'
        refine_sec = 'sec_refine'
        refine_summary_table = 'refine_table'
        pyrvapi.rvapi_add_header('Refmac Results')
        pyrvapi.rvapi_add_tab(refine_tab, 'Refinement', True)
        pyrvapi.rvapi_add_section(refine_sec,
                                  'Refinement summary',
                                  refine_tab,
                                  0, 0, 1, 1, True)
        # pyrvapi.rvapi_add_text('Refinement summary', refine_sec, 0, 0, 1, 1)
        pyrvapi.rvapi_add_table(
            refine_summary_table, 'Refinement statistics', refine_sec, 0, 0, 1, 1, False)
        self.refine_results.results_summary.set_pyrvapi_table(
             table_id=refine_summary_table)

        if len(self.refine_results.weightText) > 1:
            pyrvapi.rvapi_add_text('\nActual weight: ' + self.refine_results.weightText, refine_sec, 1, 0, 1, 1)

        # Set refinement graphs - per ncyc and final
        refinement_graphs_id = 'ncyc_graphs'

        # Set graphs - refinement statistics vs cycle

        # Remove unwanted cols and set index
        keep_col = ['Ncyc', 'FSCaverage', 'rmsBOND', 'rmsANGL', 'rmsCHIRAL']
        for col in self.refine_results.ncyc_table.columns:
            if col not in keep_col:
                self.refine_results.ncyc_table.drop(col, axis=1, inplace=True)
        self.refine_results.ncyc_table.set_index('Ncyc', inplace=True)

        # Set col
        ncyc_data_id = 'ncyc_data'
        pyrvapi.rvapi_append_loggraph(refinement_graphs_id, refine_tab)
        pyrvapi.rvapi_add_graph_data(ncyc_data_id,
                                     refinement_graphs_id,
                                     'Statistics per refinement cycle')
        self.refine_results.ncyc_table.set_pyrvapi_graph(
            graph_id=refinement_graphs_id,
            data_id=ncyc_data_id,
            originalXAxis=True,
            step=1)

        # Set graphs - final graphs

        # Remove unwanted cols and set index
        keep_col = [angstrom_label, 'FSCwork', 'SigmaA_Fc1', 'CorrFofcWork']
        for col in self.refine_results.fsc_fom_table.columns:
            if col not in keep_col:
                self.refine_results.fsc_fom_table.drop(
                    col,
                    axis=1,
                    inplace=True)
        self.refine_results.fsc_fom_table.set_index(angstrom_label,
                                                    inplace=True)
        final_graph_id = 'final_graphs'
        final_data_id = 'final_data'
        pyrvapi.rvapi_append_loggraph(final_graph_id, refine_tab)
        pyrvapi.rvapi_add_graph_data(final_data_id,
                                     refinement_graphs_id,
                                     'Final refinement statistics')
        self.refine_results.fsc_fom_table.set_pyrvapi_graph(
            graph_id=refinement_graphs_id,
            data_id=final_data_id,
            originalXAxis=True,
            step=max(int(len(self.refine_results.fsc_fom_table) / 7.0 ), 1),
            ymin=0.0)

        ### Power spectrum
        #    1) Mn(|F|)
        #    2) Mn(|F|^2)
        #    3) Var = sqrt(<Fmean**2> - <Fmean>2)
        #        -> math.sqrt ( Mn(|F|^2) - math.pow(Mn(|F|), 2) )
        if self.mrc2mtz_results is not None:
            if self.mrc2mtz_results.mean_amp_table is not None:
                keep_col = [angstrom_label, 'Mn(|F|)', 'Mn(|F|^2)']
                for col in self.mrc2mtz_results.mean_amp_table.columns:
                    if col not in keep_col:
                        self.mrc2mtz_results.mean_amp_table.drop(
                            col,
                            axis=1,
                            inplace=True)
                self.mrc2mtz_results.mean_amp_table.set_index(angstrom_label,
                                                              inplace=True)
                # Calculate variance
                self.mrc2mtz_results.mean_amp_table['Mn(|F|)^2'] = \
                    self.mrc2mtz_results.mean_amp_table['Mn(|F|)'].apply(
                        lambda x: str(math.pow(float(x), 2)))
                self.mrc2mtz_results.mean_amp_table['Variance'] = \
                    self.mrc2mtz_results.mean_amp_table['Mn(|F|^2)'].astype(float)
                self.mrc2mtz_results.mean_amp_table['Variance'] = \
                    self.mrc2mtz_results.mean_amp_table['Variance'].subtract(
                        self.mrc2mtz_results.mean_amp_table['Mn(|F|)^2'].astype(float))
                self.mrc2mtz_results.mean_amp_table['Variance'] = \
                    self.mrc2mtz_results.mean_amp_table['Variance'].apply(
                        np.sqrt).astype(str)
                self.mrc2mtz_results.mean_amp_table.drop(
                    'Mn(|F|)^2',
                    axis=1,
                    inplace=True)

                self.mrc2mtz_results.mean_amp_table['Log(Mn(|F|))'] = \
                    self.mrc2mtz_results.mean_amp_table['Mn(|F|)'].apply(
                        lambda x: str(math.log(float(x))))
                self.mrc2mtz_results.mean_amp_table['Log(Mn(|F|^2))'] = \
                    self.mrc2mtz_results.mean_amp_table['Mn(|F|^2)'].apply(
                        lambda x: str(math.log(float(x))))
                self.mrc2mtz_results.mean_amp_table['Log(Variance)'] = \
                    self.mrc2mtz_results.mean_amp_table['Variance'].apply(
                        lambda x: str(math.log(float(x))))

                self.mrc2mtz_results.mean_amp_table.drop(
                    'Mn(|F|)',
                    axis=1,
                    inplace=True)
                self.mrc2mtz_results.mean_amp_table.drop(
                    'Mn(|F|^2)',
                    axis=1,
                    inplace=True)
                self.mrc2mtz_results.mean_amp_table.drop(
                    'Variance',
                    axis=1,
                    inplace=True)
                #
                map_data_id = 'map_data'
                pyrvapi.rvapi_append_loggraph(refinement_graphs_id, refine_tab)
                pyrvapi.rvapi_add_graph_data(map_data_id,
                                             refinement_graphs_id,
                                             'Input SF statistics')
                self.mrc2mtz_results.mean_amp_table.set_pyrvapi_graph(
                    graph_id=refinement_graphs_id,
                    data_id=map_data_id,
                    originalXAxis=True,
                    step=max(int(len(self.mrc2mtz_results.mean_amp_table) / 7.0), 1))

    def set_validation_results(self):
        '''
        Refine against half map 1.
        '''
        validate_tab = 'tab3'
        validate_sec = 'sec_hm1'
        validate_table = 'table_hm1'
        # Setup refine_results (summary, graphs and output files)
        pyrvapi.rvapi_add_header('Refmac Results')
        pyrvapi.rvapi_add_tab(validate_tab, 'Validation', True)
        pyrvapi.rvapi_add_section(validate_sec,
                                  'Refinement summary',
                                  validate_tab,
                                  0, 0, 1, 1, True)
        pyrvapi.rvapi_add_text(
            'Refinement summary (shaken structure vs work half map)',
            validate_sec, 0, 0, 1, 1)

        ### Set validate table
        pyrvapi.rvapi_add_table(
            validate_table, 'Refinement statistics', validate_sec, 1, 0, 1,
            1, False)
        self.refine_hm1_results.results_summary.set_pyrvapi_table(
            table_id=validate_table)

#         ### Set model vs maps (work and free) plots
        map_v_model_data_id = 'map_v_model_data_id' #map_v_model_data'
        map_v_model_graphs_id = 'map_v_model_graphs_id'

#         # Remove unwanted cols and set index
        self.validation_md.rename(
            columns={
                'fsc_model_half1': 'FSCwork',
                'fsc_model_half2': 'FSCfree',
                'fsc_half': 'FSChalfmaps',
                'd_min': angstrom_label},
            inplace=True)
        #
        for col in self.validation_md.columns:
            if str(col) not in [
                    'FSCwork',
                    'FSCfree',
                    'FSChalfmaps',
                    angstrom_label]:
                self.validation_md.drop(
                    col,
                    axis=1,
                    inplace=True)
        self.validation_md.set_index(angstrom_label, inplace=True)

        pyrvapi.rvapi_append_loggraph(map_v_model_graphs_id, validate_tab)
        pyrvapi.rvapi_add_graph_data(map_v_model_data_id,
                                     map_v_model_graphs_id,
                                     'Model vs map (work and free)')

        print self.validation_md

        # Set plots from datatable
        # Map model FSCwork
        self.validation_md.set_pyrvapi_graph(
            graph_id=map_v_model_graphs_id,
            data_id=map_v_model_data_id,
            plot_id_list=pd.Index([u'FSCwork']),
            overlay_plot_id='FSC_work_free',
            originalXAxis=True,
            step=int(len(self.validation_md) / 7.0))
        # Map model FSCfree
        self.validation_md.set_pyrvapi_graph(
            graph_id=map_v_model_graphs_id,
            data_id=map_v_model_data_id,
            plot_id_list=pd.Index([u'FSCfree']),
            overlay_plot_id='FSC_work_free',
            originalXAxis=True,
            step=int(len(self.validation_md) / 7.0))

        # Half map 1 v 2 fsc
        map_v_map_data_id = 'halfmap_1_2_fsc'
        pyrvapi.rvapi_append_loggraph(map_v_model_graphs_id, validate_tab)
        pyrvapi.rvapi_add_graph_data(map_v_map_data_id,
                                     map_v_model_graphs_id,
                                     'Half map 1 vs 2')
#         # Set plots from datatable
        self.validation_md.set_pyrvapi_graph(
            graph_id=map_v_model_graphs_id,
            data_id=map_v_map_data_id,
            plot_id_list=pd.Index([u'FSChalfmaps']),
            originalXAxis=True,
            step=int(len(self.validation_md) / 7.0))

        pyrvapi.rvapi_flush()


def json_to_meta_data_table(json):
    md_table = metadata_utils.MetaDataTable()
    md_table.import_json(json=json)
    return md_table

def smartie_table_to_meta_data_table(table):
    '''
    Convenience utility to convert CCP4 log graph to numpy array.
    '''
    ncolumns = table.ncolumns()
    if ncolumns > 0:
        nrows = len(table.table_column(0))
    else:
        nrows = 0
    md_table = metadata_utils.MetaDataTable()
    for i in range(0, nrows):
        for j in range(0, ncolumns):
            col = table.table_column(j)
            md_table.add_column(label=col.title(),
                                ccpem_labels=None,
                                data=col.data())
    return md_table


class RefmacPowerSpectrumParser(object):
    '''
    RefmacMapToMtz program should have following table:
        Mean(|F|) and other statistics
    '''
    def __init__(self, stdout):
        self.log = smartie.parselog(stdout)
        self.mean_amp_table = None
        prog = self.log.program(0)
        for table in prog.tables():
            if table.title() == 'Mean(|F|) and other statistics':
                try:
                    self.mean_amp_table = \
                        smartie_table_to_meta_data_table(table=table)
                    self.mean_amp_table.convert_to_resolution_angstrom()
                    self.mean_amp_table.replace_infinity()
                except ValueError:
                    self.mean_amp_table = None
                    ccpem_utils.print_warning('Unable to parse Mean(|F|) table')

class RefmacFSCMapParser(object):
    '''
    FSC program should have following table:
        FSC and other statistics
    '''
    def __init__(self, stdout):
        self.log = smartie.parselog(stdout)
        # Find FSC program in stdout
        fsc_prog = self.log.program(0)
        self.fsc_table = None
        for table in fsc_prog.tables():
            if table.title() == 'FSC and other statistics':
                self.fsc_table = \
                    smartie_table_to_meta_data_table(table=table)
                self.fsc_table.convert_to_resolution_angstrom()
                self.fsc_table.replace_infinity()
        if self.fsc_table is None:
            ccpem_utils.print_warning('Unable to parse FSC table')

    def print_summary(self):
        if self.fsc_table is not None:
            print self.fsc_table.to_string()

class RefmacRefineResultsParser(object):
    '''
    Program number specifies which program in cases of concatenated stout
    files.
    '''
    def __init__(self, stdout):
        self.stdout = stdout
        self.log = smartie.parselog(self.stdout)
        #
        self.refine_results = None
        self.ncyc_table = None
        self.fsc_fom_table = None
        self.weightText = ''

        stdoutIN = open(self.stdout, 'r')

        for line in stdoutIN:
            match = re.search('Actual weight *([^ ]+) *is applied to the X-ray term', line)
            if match:
                self.weightText = match.group(1)

        # Get refinement prog
        refine_prog = self.log.program(0)

        # Get FSCaverage by cycle
        fscaverages = []
        for l in open(self.stdout):
            if "Average Fourier shell correlation    =" in l:
                fsc = float(l[l.index("=")+1:].strip())
                fscaverages.append(fsc)
        
        # Get results summary
        if refine_prog.keytext(-1).name() == 'Result':
            self.refine_results = refine_prog.keytext(-1)
        if self.refine_results is not None:

            # Parse table list in reverse order to find final FSC and ncyc table
            for cntr in reversed(xrange(len(refine_prog.tables()))):
                table = refine_prog.tables()[cntr]
                if table.title().find('FSC and  Fom') != -1:
                    if self.fsc_fom_table is None:
                        self.fsc_fom_table = smartie_table_to_meta_data_table(
                            table=table)
                        self.fsc_fom_table.convert_to_resolution_angstrom()

                if table.title().find('Rfactor analysis, stats vs cycle') != -1:
                    if self.ncyc_table is None:
                        self.ncyc_table = smartie_table_to_meta_data_table(
                            table=table)
                        if len(self.ncyc_table.index) == len(fscaverages):
                            self.ncyc_table.insert(0, "FSCaverage", fscaverages)
                # Stop if both table found
                if self.ncyc_table is not None \
                        and self.fsc_fom_table is not None:
                    break
                
            self.set_results_summary()

    def set_results_summary(self):
        data = collections.OrderedDict()
        data['Start'] = self.get_initial_full_refine_stats()
        data['Finish'] = self.get_final_refine_stats()
        # Need to reindex because from_dict only retains order of columns, not rows
        df = pd.DataFrame.from_dict(data, orient='index').reindex(['Start', 'Finish'])
        self.results_summary = metadata_utils.MetaDataTable(df)

    def get_initial_full_refine_stats(self):
        res = self.refine_results.message().split()
        i_res = collections.OrderedDict()
        if "FSCaverage" in self.ncyc_table: i_res['FSC average'] = self.ncyc_table.FSCaverage.iat[0]
        i_res['RMS bond'] = res[8]
        i_res['RMS angle'] = res[12]
        i_res['RMS chiral'] = res[16]
        return i_res

    def get_final_refine_stats(self):
        res = self.refine_results.message().split()
        f_res = collections.OrderedDict()
        if "FSCaverage" in self.ncyc_table: f_res['FSC average'] = self.ncyc_table.FSCaverage.iat[-1]
        f_res['RMS bond'] = res[9]
        f_res['RMS angle'] = res[13]
        f_res['RMS chiral'] = res[17]
        return f_res

    def find_fsc_average_at_rfactor(self,
                                    r_string,
                                    reverse=False):
        # Find starting fsc
        search_file = open(self.stdout, 'r').read()
        r_line = 'Overall R factor                     =     {0}'.format(
            r_string)
        if reverse:
            find = search_file.rfind(r_line)
        else:
            find = search_file.find(r_line)
        fsc_line = search_file[find+40:find+100]
        fsc_line = fsc_line.split()
        fsc_average = fsc_line[fsc_line.index('=')+1]
        return fsc_average

    def print_summary(self):
        print self.refine_results.message()
        print self.fsc_fom_table.to_string()
        print self.ncyc_table.to_string()

def show_results(rv_index):
    app = QtGui.QApplication([])
    rv_view = QtWebKit.QWebView()
    rv_view.load(QtCore.QUrl(rv_index))
    rv_view.show()
    app.exec_()

def main():
    job_location = sys.argv[1]
    mode = sys.argv[2]
#     
#     # Test/Debug
#     mode = "Masked"
#     job_location = "/Users/tom/ccpem_project_tst/Refmac5_277"
#     mode = "Global"
#     job_location = "/Users/tom/ccpem_project_tst/Refmac5_278"

    print mode, job_location
    if mode == 'Masked':
        refine_stdout = os.path.join(
            job_location,
            'shifted_refined.log')
        validate_hm1_stdout = os.path.join(
            job_location,
            'shifted_refined_shaken_refined.log')
        fsc_json = os.path.join(
            job_location,
            'refined_fsc.json')

    else:
        # XXX Check names for global ref
        refine_stdout = os.path.join(
            job_location,
            'refined.log')
        validate_hm1_stdout = os.path.join(
            job_location,
            'refined_shaken_refined.log')
        fsc_json = os.path.join(
            job_location,
            'refined_fsc.json')

    RefmacResultsViewer(
        job_location=job_location,
        mrc2mtz_stdout=os.path.join(
            job_location,
            'maptomtz_stdout.txt'),
        refine_stdout=refine_stdout,
        validate_hm1_stdout=validate_hm1_stdout,
        fsc_json=fsc_json)

#     # Show results - testing/debugging
#     rv_index = os.path.join(
#         job_location,
#         'rvapi_data/index.html')
#     assert os.path.exists(rv_index)
#     show_results(rv_index)

if __name__ == '__main__':
    main()
