#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core.settings import which
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core import settings

class EmdaMap2Mtz(task_utils.CCPEMTask):
    '''
    CCPEM EMDA Map2Mtz task.
    '''

    task_info = task_utils.CCPEMTaskInfo(
        name='EMDA',
        author='Rangana Warshamanage, Garib Murshudov',
        version='1.0',
        description=(
            '<p>EM map tools.'),
        short_description=(
            'EM map tools.'),
        documentation_link='https://emda.readthedocs.io/',
        references=None)

    commands = {'emda': which('emda')}

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        super(EmdaMap2Mtz, self).__init__(
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        target_map = parser.add_argument_group()
        target_map.add_argument(
            '-target_map',
            '--target_map',
            help='Target map to be used (mrc format)',
            metavar='Target map',
            type=str,
            default=None)
        #
        return parser

    def run_pipeline(self, job_id=None, db_inject=None):
        pl = []
        self.process = EMDAMaptoMTZWrapper(
            name='Map to mtz',
            job_location=self.job_location,
            target_map=self.args.target_map())
        pl.append([self.process.process])
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

class EMDAMaptoMTZWrapper(object):
    '''
    Wrapper for EMDAMaptoMTZWrapper process.
    '''
    def __init__(self,
                 job_location,
                 target_map,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__

        self.args = [
            'map2mtz',
            '--map', os.path.abspath(target_map)]

        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=EmdaMap2Mtz.commands['emda'],
            args=self.args,
            location=self.job_location,
            stdin=None)
