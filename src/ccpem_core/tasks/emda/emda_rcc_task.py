#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core.settings import which
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core import settings

class EmdaRcc(task_utils.CCPEMTask):
    '''
    CCPEM EMDA task.
    
    # Test version - map2mtz
    
    # To decide - one GUI w/ multiple functions or several seperate ones?
    
    # Add unit test args
    
    # Unit test - check expected outputs
    '''

    task_info = task_utils.CCPEMTaskInfo(
        name='EMDA',
        author='Rangana Warshamanage, Garib Murshudov',
        version='1.0',
        description=(
            '<p>EM map tools.'),
        short_description=(
            'EM map tools.'),
        documentation_link='https://emda.readthedocs.io/',
        references=None)

    commands = {'emda': which('emda')}

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        super(EmdaRcc, self).__init__(
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-model',
            '--model',
            help='Reference model (PDB format)',
            metavar='Reference model',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-half_map_1',
            '--half_map_1',
            help='Half map one (mrc format)',
            metavar='Half map 1',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-half_map_2',
            '--half_map_2',
            help='Half map two (mrc format)',
            metavar='Half map 2',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-resolution',
            '--resolution',
            help='''Resolution of input map (Angstrom)''',
            metavar='Resolution',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-mask',
            '--mask',
            help='mask (mrc format)',
            metavar='Mask',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-kernel_size',
            '--kernel_size',
            help=('Kernel size in voxels. Default 5.'),
            metavar='Kernel size',
            type=int,
            default=5)
        #
        parser.add_argument(
            '-normalised_maps',
            '--normalised_maps',
            help='Use normalised maps',
            metavar='Normalized maps',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-cif_lib',
            '--cif_lib',
            help='Ligand dictionary (cif format)',
            metavar='Ligand dictionary',
            type=str,
            default=None)
        #
        return parser

    def run_pipeline(self, job_id=None, db_inject=None):
        pl = []
        self.process = EmdaRccWrapper(
            name='EMDA_RCC',
            job_location=self.job_location,
            model=self.args.model(),
            half_map_1=self.args.half_map_1(),
            half_map_2=self.args.half_map_2(),
            resolution=self.args.resolution(),
            mask=self.args.mask(),
            kernel_size=self.args.kernel_size(),
            normalised_maps=self.args.normalised_maps(),
            cif_lib=self.args.cif_lib()
            )
        pl.append([self.process.process])
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

class EmdaRccWrapper(object):
    '''
    Wrapper for EMDA RCC process.
    '''
    
    def __init__(self,
                 job_location,
                 model,
                 half_map_1,
                 half_map_2,
                 resolution,
                 mask,
                 normalised_maps,
                 kernel_size,
                 cif_lib,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__

        self.args = [
            'rcc',
            '--mdl', os.path.abspath(model),
            '--h1', os.path.abspath(half_map_1),
            '--h2', os.path.abspath(half_map_2),
            '--res', resolution,
            '--knl', kernel_size
            ]
        if normalised_maps:
            self.args += ['--nrm']
        if mask is not None:
            self.args += ['--msk', os.path.abspath(mask)]
        if cif_lib is not None:
            self.args += ['--lgf', os.path.abspath(cif_lib)]

        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=EmdaRcc.commands['emda'],
            args=self.args,
            location=self.job_location,
            stdin=None)
