#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os

from ccpem_core import process_manager
from ccpem_core import settings
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core.settings import which
from ccpem_core.tasks import task_utils


# To do
# 1) Swap to using gemmi command line tool when available
class ModelTools(task_utils.CCPEMTask):
    '''
    CCPEM Model Tools.
    
    Collection of utilities for modification of atomic models
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='ModelTools',
        author='Burnley T',
        version='1.0',
        description=(
            '<p> Collection of utilities for modification of atomic '
            'models.</p>'
            '<p>Full documentation:</p>'
            '<p>http://www.ccpem.ac.uk</p>'),
        short_description=(
            'Model utilies'),
        documentation_link='http://www.ccpem.ac.uk',
        references=None)

    commands = {'ccpem-python': settings.which(program='ccpem-python')}

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        super(ModelTools, self).__init__(
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            parent=parent)
        self.output_mmcif_path = None
        self.output_pdb_path = None

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-input_model',
            '--input_model',
            help='''Input coordinate file''',
            metavar='Input model',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-output_mmcif',
            '--output_mmcif',
            help='''Select output in mmCIF/PDBx format''',
            metavar='Output mmCIF',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-output_pdb',
            '--output_pdb',
            help='''Select output in PDB format''',
            metavar='Output PDB',
            type=bool,
            default=False)
        #
        return parser

    def run_pipeline(self, job_id=None, db_inject=None):
        pl = []
        export_name = os.path.join(
            self.job_location,
            os.path.splitext(
                os.path.basename(self.args.input_model()))[0])

        # For GUI laucher
        self.output_mmcif_path = export_name + '.cif'
        self.output_pdb_path = export_name + '.pdb'

        self.process_model_tools = ModelToolsWrapper(
            job_location=self.job_location,
            input_model=self.args.input_model(),
            output_name=export_name,
            output_mmcif=self.args.output_mmcif(),
            output_pdb=self.args.output_pdb())

        pl.append([self.process_model_tools.process])
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()


class ModelToolsWrapper(object):
    '''
    Wrapper for model tools.
    '''
    def __init__(self,
                 job_location,
                 input_model,
                 output_name,
                 output_mmcif=False,
                 output_pdb=False):
        self.job_location = job_location
        command=which('ccpem-python')
        assert command is not None
        self.name = self.__class__.__name__
        #
        self.args = ['-m',
                     'ccpem_core.model_tools.command_line']
        self.args += ['--model', input_model]
        if output_mmcif:
            self.args += ['--output_cif']
        if output_pdb:
            self.args += ['--output_pdb']
        self.args += ['--output_name', output_name]
        #
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location)
