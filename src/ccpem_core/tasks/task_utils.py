#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from ccpem_core import ccpem_utils
from ccpem_core.ccpem_utils import ccpem_argparser


class CCPEMBibtexRef(object):
    '''
    Store bibtex formated reference.
    N.B. Citeulike.org is a good source of Bibtex formated refs.
    '''
    def __init__(self,
                 author,
                 journal,
                 year,
                 title,
                 volume,
                 number,
                 pages):
        self.author = ' '.join(author.split()).replace(' and', ',')
        self.journal = journal
        self.year = int(year)
        self.title = title
        self.volume = volume
        self.number = number
        self.pages = pages


class CCPEMTaskInfo(object):
    '''
    Store program information.
    '''
    def __init__(self,
                 name,
                 author,
                 version,
                 description,
                 short_description,
                 documentation_link=None,
                 references=None):
        self.name = name
        self.author = author
        self.version = version
        self.id = self.name + '_' + str(version)
        self.description = description
        self.documentation_link = documentation_link
        assert len(short_description) < 200
        self.short_description = short_description
        self.references = references

    def add_reference(self, bibtex_ref):
        '''
        Add bibtex reference to task
        '''
        self.references.append(bibtex_ref)

class CCPEMTask(object):
    '''
    Base class for CCPEM tasks
    '''
    # dictionary of command name and command

    commands = {}
    command = None

    task_info = CCPEMTaskInfo(
        name='example prog',
        author='Doe J',
        version='1.0',
        description=(
            'Complete description'),
        short_description=(
            'Short for tooltips (less 100 characters)'),
        documentation_link=None,
        references=None)

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 args_json_string=None,
                 pipeline=None,
                 job_location=None,
                 verbose=False,
                 parent=None):
        assert self.commands
        self.parent = parent
        self.database_path = database_path
        self.verbose = verbose
        # Set args
        if args_json is not None or args_json_string is not None or args is None:
            self.set_args(jsonfile=args_json,
                          json_string=args_json_string)
        else:
            self.args = args
        self.pipeline = pipeline
        if self.pipeline is not None:
            self.set_args(jsonfile=self.pipeline.args_path)
            self.job_location = self.pipeline.location
        else:
            self.job_location = job_location

    def set_args(self, jsonfile=None, json_string=None):
        if jsonfile is not None:
            if not os.path.exists(jsonfile):
                ccpem_utils.print_warning(
                    message='Json file not found : ' + jsonfile)
        self.args = self.parser().generate_arguments(
            jsonfile=jsonfile,
            json_string=json_string)

    def parser(self):
        '''
        Set args from CCPEM args parser.
        '''
        parser = ccpem_argparser.ccpemArgParser()
        return parser

    def run_task(self,
                 job_location,
                 run=True,
                 job_id=None,
                 db_inject=None):
        '''
        Validate args, set job location, save args and launch pipeline.
        '''
        # Set job_location
        self.job_location = job_location

        # Set job title if not done so
        if self.args.job_title() in [None, 'None']:
            self.set_job_title()

        # Save arguments
        filename = os.path.join(self.job_location, 'args.json')
        self.args.output_args_as_json(
            filename=filename,
            set_abs_path=True)
        # Run job
        if run:
            self.run_pipeline(job_id=job_id,
                              db_inject=db_inject)

    def set_job_title(self):
        title = 'Untitled'
        self.args.job_title.value = title

    def validate_args(self):
        '''
        Check arguments before running pipeline.  Return true if ok, return
        false and give warning if not.  Unique to each task.
        '''
        return True

    def run_pipeline(self, job_id=None):
        '''
        Setup and run pipeline.  Unique to each task.
        '''
        pass

    def set_arg_absolute_path(self, arg):
        '''
        Check file input is given and can be found.  Converts to abs path.
        '''
        warnings = ''
        if arg.value is None:
            warnings += '\n{0} not defined'.format(arg.metavar)
        else:
            arg.value = os.path.abspath(arg.value)
            if not os.path.exists(path=arg.value):
                warnings += '\n{0} not found : {1}'.format(
                    arg.metavar,
                    arg.value)
        return warnings

    def print_command_line_warnings(self, warnings):
        print '\n\nError: {0} failed'.format(self.task_info.name)
        print warnings

    def get_process(self, name):
        '''
        Get a named process from the pipeline.

        This method returns the first matching process it finds, so be careful
        if process names might not be unique!

        Raises ValueError if no process with the given name exists.
        '''
        for stage in self.pipeline.pipeline:
            for process in stage:
                if process.name == name:
                    return process
        raise ValueError("Could not find process named '{}' in task pipeline".format(name))