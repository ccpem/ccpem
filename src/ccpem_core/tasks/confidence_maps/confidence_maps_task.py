#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import FDRcontrol

from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils


# Conversions between GUI labels and Max's argument values
METHODS = {
    'FDR-BY': 'BY',
    'FDR-BH': 'BH',
    'FWER-Holm': 'Holm',
    'FWER-Hochberg': 'Hochberg'
}
TEST_PROCS = {
    'Left-sided': 'leftSided',
    'Right-sided': 'rightSided',
    'Two-sided': 'twoSided'
}


class ConfidenceMapsTask(task_utils.CCPEMTask):
    '''
    CCPEM ConfidenceMaps Task.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='Confidence Maps',
        author='M. Beckers, A. J. Jakobi, C. Sachse',
        version='0',
        description=(
            'Thresholding of cryo-EM density maps by False Discovery Rate '
            'control.<br><br>'
            'Please see the paper '
            '(<a href="https://doi.org/10.1107/S2052252518014434">Beckers, '
            'Jakobi & Sachse, 2019)</a> for details of the method, and the '
            'tutorial available with the '
            '<a href="https://git.embl.de/mbeckers/FDRthresholding">source '
            'code</a> for instructions and guidance.'),
        short_description=(
            'Thresholding of cryo-EM density maps by False Discovery Rate '
            'control'),
        documentation_link='https://git.embl.de/mbeckers/FDRthresholding',
        references=None)
    commands = {
        'confidence_maps_python': ['ccpem-python', FDRcontrol.__file__],
    }

    def __init__(self, **kwargs):
        super(ConfidenceMapsTask, self).__init__(**kwargs)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-em_map',
            '--em_map',
            help='Input map (mrc format)',
            metavar='Input map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-apix',
            '--apix',
            help='Override pixel size of input map.',
            metavar='Pixel size (angstroms)',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-locResMap',
            '--locResMap',
            help='Input local resolution map (optional).',
            metavar='Local resolution map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-method',
            '--method',
            help=("Method for multiple testing correction. FDR for False Discovery Rate or FWER for Family-Wise Error "
                  "Rate. 'BY' for Benjamini-Yekutieli, 'BH' for Benjamini-Hochberg."),
            metavar='Correction method',
            type=str,
            choices=['FDR-BY',
                     'FDR-BH',
                     'FWER-Holm',
                     'FWER-Hochberg'],
            default='FDR-BY')
        #
        parser.add_argument(
            '-window_size',
            '--window_size',
            help='Size of box for background noise estimation (in pixels).',
            metavar='Noise box size',
            type=int,
            default=None)
        #
        parser.add_argument(
            '-noise_box',
            '--noise_box',
            help='Coordinates of centre of box for noise estimation (in pixels, x y z). Leave blank for default.',
            metavar='Noise box coordinates',
            type=list,
            default=None)
        #
        parser.add_argument(
            '-meanMap',
            '--meanMap',
            help='3D map of noise means to be used for FDR control.',
            metavar='Mean map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-varianceMap',
            '--varianceMap',
            help='3D map of noise variances to be used for FDR control.',
            metavar='Variance map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-test_proc',
            '--test_proc',
            help="Choose between right, left and two-sided testing.",
            metavar='Test procedure',
            type=str,
            choices=['Left-sided',
                     'Right-sided',
                     'Two-sided'],
            default='Right-sided')
        #
        parser.add_argument(
            '-lowPassFilter',
            '--lowPassFilter',
            help="Low-pass filter the map at the given resoultion prior to FDR control.",
            metavar='Low-pass filter',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-ecdf',
            '--ecdf',
            help="Use empirical cumulative distribution function instead of the standard normal distribution.",
            metavar='Use ECDF',
            type=bool,
            default=None)
        #
        return parser

    def prepare_cmdline_args(self):

        # Required args
        args = [
            '--em_map', str(self.args.em_map.value),
            '-method', METHODS[self.args.method.value],
            '--testProc', TEST_PROCS[self.args.test_proc.value]
        ]

        noise_box_coords = self.args.noise_box.value

        if noise_box_coords is not None and len(noise_box_coords) > 0:
            args.extend(['-noiseBox'] + noise_box_coords)

        def add_arg_if_not_none(name):
            arg = getattr(self.args, name.strip('-'))
            if arg is not None and str(arg.value) != 'None':
                args.extend([name, str(arg.value)])

        add_arg_if_not_none('--apix')
        add_arg_if_not_none('-locResMap')
        add_arg_if_not_none('--window_size')
        add_arg_if_not_none('--meanMap')
        add_arg_if_not_none('--varianceMap')
        add_arg_if_not_none('--lowPassFilter')

        if self.args.ecdf.value:
            args.append('-ecdf')

        return args

    def run_pipeline(self, job_id=None, db_inject=None):

        args = self.prepare_cmdline_args()

        # Confidence Maps process
        confidence_maps_process = process_manager.CCPEMProcess(
            name='Confidence Maps',
            command=self.commands['confidence_maps_python'],
            args=args,
            location=self.job_location)

        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=[[confidence_maps_process]],
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            db_inject=db_inject,
            database_path=self.database_path,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()
