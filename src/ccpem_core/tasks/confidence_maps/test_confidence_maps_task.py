#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest

from . import confidence_maps_task

class Test(unittest.TestCase):
    '''
    Unit test for Confidence Maps task
    '''

    def test_arg_list_generation_with_no_args(self):
        '''
        Check that the command line arguments for the FDRcontrol script are
        generated properly.
        '''
        task = confidence_maps_task.ConfidenceMapsTask(args_json_string="{}")
        args = task.prepare_cmdline_args()
        assert args == ['--em_map', 'None', '-method', 'BY', '--testProc', 'rightSided']

    def test_arg_list_generation_with_all_args(self):
        '''
        Check that the command line arguments for the FDRcontrol script are
        generated properly.
        '''
        task = confidence_maps_task.ConfidenceMapsTask(args_json_string="""{
            "job_title": "Test Confidence Maps task",
            "job_location": "path/to/job",
            "em_map": "input map",
            "apix": 1.34,
            "locResMap": "loc res map",
            "method": "FDR-BY",
            "window_size": 25,
            "noise_box": [ 10, 20, 30 ],
            "meanMap": "mean map",
            "varianceMap": "variance map",
            "test_proc": "Two-sided",
            "lowPassFilter": 3.5,
            "ecdf": true
        }""")
        args = task.prepare_cmdline_args()
        assert args == ['--em_map', 'input map', '-method', 'BY', '--testProc', 'twoSided', '-noiseBox', 10, 20, 30, '--apix', '1.34', '-locResMap', 'loc res map', '--window_size', '25', '--meanMap', 'mean map', '--varianceMap', 'variance map', '--lowPassFilter', '3.5', '-ecdf']


if __name__ == '__main__':
    unittest.main()
