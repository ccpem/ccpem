import os,sys,re
import subprocess
from ccpem_progs import jpred
import glob
import time
import types
import urllib2
import tarfile
from ccpem_core.pdb_tools.biopy_tools import *
import json
import results_download
from collections import OrderedDict

commands = {
        'jpredapi' : ['perl',os.path.join(
                        os.path.dirname(jpred.__file__),'jpredapi')],
        'jpred_download': ['ccpem-python',
                        results_download.__file__]}

class JpredDownload:
    '''
    Class for Jpred job status check and results download 
    uses the jpredapi script
    '''
    def __init__(self,job_location=None,seqdir=None):
        cwd = os.getcwd()
        if job_location is None:
            self.job_location = os.getcwd()
        elif os.path.isdir(job_location):
            self.job_location = job_location
            os.chdir(job_location)
        
        self.check_submitted_jobs(job_location)
        
    def check_submitted_jobs(self,jobdir,delay=2):
        '''
        Check submitted jpred jobs, parse and download outputs
        '''
        #count jobs
        list_jobs = glob.glob(jobdir+'/*log_running_job_id.txt')
        nJobsRunning = len(list_jobs)
        #count seq
        list_seqfiles = glob.glob(jobdir+'/*.fasta') #pdb_ch.fasta
        nseq = len(list_seqfiles)
        #save chains
        for seqfile in list_seqfiles:
            chain = os.path.basename(seqfile).split('_')[-1]
            
        self.failed_jobs = []
        self.finished_jobs = []
        self.failed_downloads = []
        self.dict_jobids = {}
        count_delays = 0
        while nJobsRunning <= nseq:
            time.sleep(delay) #delay may not be required in the results parser
            count_new_jobs = 0
            for jobpath in list_jobs:
                logfile = os.path.basename(jobpath)
                seqname = logfile.split('_log_running_job_id')[0]
                if seqname in self.failed_jobs: continue
                if seqname in self.finished_jobs: continue
                count_new_jobs += 1
                #check job status and download
                self.check_job_status(jobpath)
                #print 'Failed: ', self.failed_jobs
                #print 'Finished: ', self.finished_jobs
            count_delays += 1
            if count_delays >= 100: break
            if count_new_jobs == 0: break
            #if nJobsRunning == self.nseq: break
            #update running jobs
            list_jobs = glob.glob(jobdir+'/*log_running_job_id.txt')
            nJobsRunning = len(list_jobs)
        

    def check_job_status(self,logfilepath,checkevery=2):
        '''
        Check Jpred job status and download
        '''
        jobid = ''
        logfile = os.path.basename(logfilepath)
        #seqname is prefix of log file
        seqname = logfile.split('_log_running_job_id')[0]
        jobid = self.get_job_id(logfile)
        #save jobid
        self.dict_jobids[seqname] = jobid
        #if no jobid in log file
        if len(jobid) == 0: 
            try: self.failed_jobs.append(seqname)
            except: 
                self.failed_jobs = [seqname]
            return
        args = ['status','jobid={}'.format(jobid), 'getResults=no','silent',
                'checkEvery={}'.format(checkevery)]
        #get job status in a status file
        status_file = os.path.splitext(logfilepath)[0]
        status_file = status_file.split('log_running_job_id')[0] +'log_status.txt'
        #if status file exists
        if os.path.isfile(status_file):
            with open(status_file,'r') as sf:
                for line in sf:
                    #if job is finished, download results
                    if seqname in self.finished_jobs:
                        if 'http://' in line:
                            linesplit = line.split('http')
                            href = 'http'+\
                                        linesplit[1][:-1]
                            print "Link to results for {} : {}".format(seqname,href)
                            jpred_url = 'http'+\
                                        '/'.join(linesplit[1].split('/')[:-1])+\
                                        '/'+jobid+'.tar.gz'
                            #add a delay to download finished jobs
                            time.sleep(2)
                            download_flag = self.download_job_output(jpred_url)
                            if not download_flag:
                                self.failed_jobs.append(seqname)
                                print 'Failed: ', self.failed_jobs
                            return
                    #check if finished
                    if "finished. Results available at the following" in line:
                        try:
                            if not seqname in self.finished_jobs: 
                                self.finished_jobs.append(seqname)
                        except: self.finished_jobs = [seqname]
                        
        #check status of the job
        else:
            #add delay to check status
            time.sleep(1)
            command_args = commands['jpredapi'][:]
            command_args += args
            #print command_args
            with open(status_file,'w') as sf:
                subprocess.call(command_args,stdout=sf)

    def get_job_id(self,logfile):
        #get job id from logfile
        with open(logfile,'r') as lf:
            for line in lf:
                if ':' in line:
                    jobid = line.split()[-1]
                    break
        return jobid

    def download_job_output(self,job_url):
        return_flag = True
        cwd = os.getcwd()
        jobid = job_url.split('/')[-1]
        jobid = jobid.split('.')[0]
        if not os.path.isdir(jobid):
            os.mkdir(jobid)
            os.chdir(jobid)
        
        jpred_tarfile = job_url.split('/')[-1]
        command_args = commands['jpred_download'][:]
        command_args.append(job_url)
        command_args.append(jpred_tarfile)
        if not os.path.isfile(jpred_tarfile):
            print 'Downloading Jpred results:'
            print job_url
            subprocess.call(command_args)
        
        try:
            assert os.path.isfile(jpred_tarfile)
            tar = tarfile.open(jpred_tarfile, "r:gz")
            tar.extractall()
            tar.close()
        except (AssertionError,TypeError) as exc:
            self.failed_downloads.append(jobid)
            return_flag = False
        os.chdir(cwd)
        return return_flag

def main():
    job_location = sys.argv[1]
    jp = JpredDownload(job_location=job_location)

if __name__ == '__main__':
    main()