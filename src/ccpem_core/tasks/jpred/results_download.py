import os,sys,re
import glob
import time
import types
import urllib2

def download_jpred_output(job_url,jpred_tarfile):
    return_flag = True
    #jpred_results_request = urllib2.Request(job_url)
    try:
        jpred_results_f = urllib2.urlopen(job_url.strip(), timeout=5)
        with open(jpred_tarfile,'w') as f:
            f.write(jpred_results_f.read())
    except urllib2.URLError:
        return_flag = False
    
    try:
        jpred_results_f.close()
    except: pass

def main():
    job_url = sys.argv[1]
    jpred_tarfile = sys.argv[2]
    download_jpred_output(job_url,jpred_tarfile)
    

if __name__ == '__main__':
    main()