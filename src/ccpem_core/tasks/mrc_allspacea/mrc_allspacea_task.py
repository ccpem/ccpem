#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
MRC Allspacea task.
'''

import os
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import settings
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils


class MrcAllspacea(task_utils.CCPEMTask):
    '''
    Inherits from CCPEMTaskWindow.
    '''

    task_info = task_utils.CCPEMTaskInfo(
        name='MRCallspacea',
        author='Crowther RA, Henderson R, Smith JM',
        version='0.1',
        description=(
            'Umbrella program to calculate internal phase residual in all '
            '17 space groups from data on a single film.  Can do the same '
            'thing within one film as ORIGTILT does between different '
            'files.'),
        short_description=(
            'Calculate internal phase residual in all space groups'),
        references=None)

    commands = {'mrc_allspacea': settings.which(program='mrc_allspacea')}

    def __init__(self,
                 task_info=task_info,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        super(MrcAllspacea, self).__init__(database_path=database_path,
                                           args=args,
                                           args_json=args_json,
                                           pipeline=pipeline,
                                           job_location=job_location,
                                           parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        datafile = parser.add_argument_group()
        datafile.add_argument(
            '-f',
            '--datafile',
            help=('Spot file input, any number of spots with any index '
                  '(preceded by title line) H, K, amplitude, phase'),
            metavar='Data file',
            type=str,
            default='None')
        symmetry = parser.add_argument_group()
        symmetry.add_argument(
            '-symmetry',
            '--symmetry',
            help='Symmetry required',
            choices=['ALL',
                     'HEXA',
                     'SQUA',
                     'RECT',
                     'OBLI',
                     'TWO',
                     'THRE',
                     'SIX',
                     'FOUR',
                     'SPEC'],
            metavar='Symmetry',
            type=str,
            default='ALL')
        symmetry.add_argument(
            '-ispg',
            '--ispg',
            help='List of space group numbers (optional)',
            required=False,
            type=str,
            default='None')
        #
        search_refine_tilt_ncyc = parser.add_argument_group()
        search_refine_tilt_ncyc.add_argument(
            '-s',
            '--search',
            help='Search',
            type=bool,
            default=True)
        search_refine_tilt_ncyc.add_argument(
            '-r',
            '--refine',
            help='Search',
            type=bool,
            default=True)
        search_refine_tilt_ncyc.add_argument(
            '-t',
            '--tilt',
            help='Tilt',
            type=bool,
            default=True)
        search_refine_tilt_ncyc.add_argument(
            '-c',
            '--ncycle',
            help='Number of cycles',
            type=int,
            default=4000)
        #
        orig = parser.add_argument_group()
        orig.add_argument(
            '-oh',
            '--origh',
            help='Starting origin phase shift H',
            type=float,
            default=0.)
        orig.add_argument(
            '-ok',
            '--origk',
            help='Starting origin phase shift K',
            type=float,
            default=0.)
        #
        tilt = parser.add_argument_group()
        tilt.add_argument(
            '-th',
            '--tilth',
            help='Starting beam tilt H',
            type=float,
            default=0.)
        tilt = parser.add_argument_group()
        tilt.add_argument(
            '-tk',
            '--tiltk',
            help='Starting beam tilt K',
            type=float,
            default=0.)
        #
        stepsize = parser.add_argument_group()
        stepsize.add_argument(
            '-st',
            '--stepsize',
            help='Phase-shift step size',
            type=float,
            default=1.0)
        phasesize = parser.add_argument_group()
        phasesize.add_argument(
            '-is',
            '--phasesize',
            help='Size of search area',
            type=int,
            default=61)
        #
        cell_dims = parser.add_argument_group()
        cell_dims.add_argument(
            '-a',
            '--alpha',
            help='Alpha cell dimension',
            type=float,
            default=1.0)
        cell_dims.add_argument(
            '-b',
            '--beta',
            help='Beta cell dimension',
            type=float,
            default=1.0)
        cell_dims.add_argument(
            '-g',
            '--gamma',
            help='Gamma cell dimension',
            type=float,
            default=1.0)
        #
        resolution_range = parser.add_argument_group()
        resolution_range.add_argument(
            '-ri',
            '--rin',
            help='Resolution in (A)',
            type=float,
            default=10.0)
        resolution_range.add_argument(
            '-ro',
            '--rout',
            help='Resolution out (A)',
            type=float,
            default=100.0)
        #
        cs = parser.add_argument_group()
        cs.add_argument(
            '-cs',
            '--cs',
            help='Spherical aberration (nm)',
            type=float,
            default=1.0)
        #
        kv = parser.add_argument_group()
        kv.add_argument(
            '-kv',
            '--kv',
            help='Microscope operating voltage (KV)',
            type=float,
            default=100.0)
        #
        ilist = parser.add_argument_group()
        ilist.add_argument(
            '-il',
            '--ilist',
            help='Ilist = True gives more detailed output',
            type=bool,
            default=True)
        #
        rot180 = parser.add_argument_group()
        rot180.add_argument(
            '-r180',
            '--rot180',
            help=('Rot180 = 1 enables same convention as other programs to be'
                  'used for P3 indexing convention'),
            type=int,
            default=0)
        #
        iqmax  = parser.add_argument_group()
        iqmax.add_argument(
            '-iqm',
            '--iqmax',
            help='IQmax is used to restrict the comparisons to sport with IQ '
                 '<= IQmax',
            type=int,
            default=10)
        return parser

    def run_pipeline(self, job_id=None, db_inject=None):
        '''
        Launch pipeline using new job manager
        '''
        # Run task process
        stdin = self.write_stdin()
        self.mrcallspacea_job = process_manager.CCPEMProcess(
            name='Allspacea',
            command=self.commands['mrc_allspacea'],
            location=self.job_location,
            env={'IN': self.args.datafile.value},
            stdin=stdin)
        self.pipeline = process_manager.CCPEMPipeline(
            job_id=job_id,
            pipeline=self.mrcallspacea_job,
            args_path=self.args.jsonfile,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value,
            location=self.job_location)
        self.pipeline.start()

    def validate_args(self):
        '''
        Check arguments before running job.
        '''
        args_correct = True
        warnings = 'Error:'
        if not os.path.exists(self.args.datafile.value):
            warnings += '\nDatafile not found'
            args_correct = False
        self.args.datafile.value = os.path.abspath(self.args.datafile.value)
        if not args_correct:
            if warnings is not '':
                print warnings
        return args_correct

    def write_stdin(self):
        '''
        Convert args to task arguments for stdin
        '''
        out = self.args.symmetry.value + '\n'
        out += (str(self.args.search.value) + ' '
                + str(self.args.refine.value) + ' '
                + str(self.args.tilt.value) + ' '
                + str(self.args.ncycle.value) + '\n')
        out += (str(self.args.origh.value) + ' '
                + str(self.args.origk.value) + ' '
                + str(self.args.tilth.value) + ' '
                + str(self.args.tiltk.value) + '\n')
        out += (str(self.args.stepsize.value) + ' '
                + str(self.args.phasesize.value) + '\n')
        out += (str(self.args.alpha.value) + ' '
                + str(self.args.beta.value) + ' '
                + str(self.args.gamma.value) + ' '
                + str(self.args.rin.value) + ' '
                + str(self.args.rout.value) + ' '
                + str(self.args.cs.value) + ' '
                + str(self.args.kv.value) + '\n')
        out += (str(self.args.ilist.value) + ' '
                + str(self.args.rot180.value) + ' '
                + str(self.args.iqmax.value) + '\n')
        out += 'eot'
        return out
