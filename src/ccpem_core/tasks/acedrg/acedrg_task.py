#
#     Copyright (C) 2020 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import process_manager
from ccpem_core import settings
from ccpem_core.tasks import task_utils


class AcedrgTask(task_utils.CCPEMTask):
    '''
    CCPEM AceDRG Task.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='AceDRG',
        author=('Fei Long, Robert A Nicholls, Paul Emsley, Saulius Grazulis, Andrius '
                'Merkys, Antanas Vaitkus and Garib N Murshudov'),
        version='217',
        description=(
            'AceDRG generates stereochemical descriptions of ligands'),
        short_description=(
            'A stereochemical description generator for ligands'),
        documentation_link='https://www2.mrc-lmb.cam.ac.uk/groups/murshudov/content/acedrg/acedrg.html',
        references=None)

    commands = {'acedrg': settings.which('acedrg')}

    def __init__(self, **kwargs):
        super(AcedrgTask, self).__init__(**kwargs)
        self.out_root = None

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-smiles_string',
            '--smiles_string',
            help='Input SMILES string',
            metavar='Input SMILES string',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-smiles_file',
            '--smiles_file',
            help='Input SMILES file',
            metavar='Input SMILES file',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-mol_file',
            '--mol_file',
            help='Input MOL file',
            metavar='Input MOL file',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-mmcif_file',
            '--mmcif_file',
            help='Input mmCIF file',
            metavar='Input mmCIF file',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-monomer_code',
            '--monomer_code',
            help='Three-letter code for output monomer (e.g. GLU)',
            metavar='Monomer code',
            type=str,
            default='DRG')
        #
        return parser

    def run_pipeline(self, job_id=None, db_inject=None):
        # Set up arguments
        args = []
        if self.args.smiles_string.value:
            args.append('--smi={}'.format(self.args.smiles_string.value))
        elif self.args.smiles_file.value:
            args.append('--smi={}'.format(self.args.smiles_file.value))
        elif self.args.mol_file.value:
            args.append('--mol={}'.format(self.args.mol_file.value))
        elif self.args.mmcif_file.value:
            args.append('--mmcif={}'.format(self.args.mmcif_file.value))
        else:
            raise ValueError("No input provided for AceDRG job")

        out_root = 'acedrg'
        if self.args.monomer_code.value:
            args.append('--res={}'.format(self.args.monomer_code.value))
            out_root = '{}_acedrg'.format(self.args.monomer_code.value)
        args.append('--out={}'.format(out_root))

        # AceDRG process
        acedrg_process = process_manager.CCPEMProcess(
            name='AceDRG',
            command=self.commands['acedrg'],
            args=args,
            location=self.job_location)

        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=[[acedrg_process]],
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            db_inject=db_inject,
            database_path=self.database_path,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()
