#
#     Copyright (C) 2018 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
import shutil
from loc_scale import np_locscale_fft
import mrcfile
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.tasks.refmac import refmac_task
from ccpem_core import settings
from ccpem_core.tasks.bin_wrappers import mapmask
from ccpem_core.model_tools.gemmi_utils import shift_coordinates, remove_atomic_charges
from ccpem_core.map_tools.TEMPy import map_preprocess
from ccpem_core.tasks.model2map.model2map_task import mapprocess_wrapper, \
                sf2mapWrapper


mrc_axis = {1: 'x',
            2: 'y',
            3: 'z'}

class LocScale(task_utils.CCPEMTask):
    '''
    CCPEM LocScale Task.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='LocScale',
        author='A. J. Jakobi, M. Wilmanns, C. Sachse',
        version='1.1',
        description=(
            'Local amplitude sharpening based target structure'),
        short_description=(
            'Local amplitude sharpening based target structure'
            'Coefficient'),
        documentation_link='https://git.embl.de/jakobi/LocScale/wikis/home',
        references=None)
    commands = {
        'loc_scale_python': ['ccpem-python',
                             os.path.realpath(np_locscale_fft.__file__)],
        'refmac': settings.which(program='refmac5'),
        'pdbset':  settings.which(program='pdbset'),
        'ccpem-gemmi':settings.which(program='gemmi'),
        'ccpem-mapprocess':
        ['ccpem-python', os.path.realpath(map_preprocess.__file__)]}

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        super(LocScale, self).__init__(database_path=database_path,
                                       args=args,
                                       args_json=args_json,
                                       pipeline=pipeline,
                                       job_location=job_location,
                                       parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-target_map',
            '--target_map',
            help='Target map to be auto sharpened (mrc format)',
            metavar='Target map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-reference_map',
            '--reference_map',
            help=(
                'Reference map (mrc format). If provided this will be '
                'used as reference otherwise model will be'),
            metavar='Reference map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-mask_map',
            '--mask_map',
            help='Mask map (mrc format)',
            metavar='Mask map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-resolution',
            '--resolution',
            help='Resolution of target map (Angstrom)',
            metavar='Resolution',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-pixel_size',
            '--pixel_size',
            help=('Pixel size in Angstrom, if not defined it will be '
                  'calculated from the header'),
            metavar='Pixel size',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-reference_model',
            '--reference_model',
            help='Reference model (PDB or mmCIF format)',
            metavar='Reference model',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-lib_in',
            '--lib_in',
            help='Ligand dictionary (cif format)',
            metavar='Ligand dictionary',
            type=str,
            default=None)
        # 
        parser.add_argument(
            '-refine_bfactors',
            '--refine_bfactors',
            help='Refine reference structure B-factors using Refmac',
            metavar='Refine B-factors',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-window_size',
            '--window_size',
            help=('Window size in pixel, if not given 7 * map resolution '
                  '/ pixel size used'),
            metavar='Window size',
            type=int,
            default=None)
        #
        parser.add_argument(
            '-use_mpi',
            '--use_mpi',
            help='Use mpi for parallel processing',
            metavar='Use MPI',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-n_mpi',
            '--n_mpi',
            help='Number of mpi nodes',
            metavar='MPI nodes',
            type=int,
            default=2)
        #
        return parser
    
    def fix_model_for_refmac(self,pdb_path,pdb_edit_path,trans_vector,
                             remove_charges=False):
        shift_coordinates(pdb_path,pdb_edit_path,trans_vector,remove_charges)
        
    def run_pipeline(self, job_id=None, db_inject=None):
        fix_map_model = True
        # Calculate pixel size
        with mrcfile.open(self.args.target_map(), 'r') as mrc:
            map_nx = mrc.header.nx
            map_ny = mrc.header.ny
            map_nz = mrc.header.nz
            map_mapc = mrc.header.mapc
            map_mapr = mrc.header.mapr
            map_maps = mrc.header.maps
            map_nxstart = mrc.header.nxstart
            map_nystart = mrc.header.nystart
            map_nzstart = mrc.header.nzstart
            ori_x = mrc.header.origin.x
            ori_y = mrc.header.origin.y
            ori_z = mrc.header.origin.z
            cella = (mrc.header.cella.x,mrc.header.cella.y,
                     mrc.header.cella.z)
            apix = mrc.voxel_size.item()

            if self.args.pixel_size() is None:
                self.args.pixel_size.value = (mrc.voxel_size.x + 
                                              mrc.voxel_size.y + 
                                              mrc.voxel_size.z) / 3.0
        self.map_edit_path = self.args.target_map.value
        
#         # Run Refmac to refine reference structure B-factors
#         if self.args.refine_bfactors():
#             self.refmac_process = refmac_task.RefmacRefine(
#                 command=self.command,
#                 job_location=self.job_location,
#                 pdb_path=self.args.reference_model(),
#                 mtz_path=self.process_free_r_flags.hklout,
#                 resolution=self.args.resolution.value,
#                 mode='Global',
#                 name='Refmac refine (global)',
#                 sharp=None,
#                 ncycle=20,
#                 output_hkl=True)
#             pl.append([self.refmac_process.process])
#             # XXX TODO - get refmac structure
#             loc_scale_structure = ''
#         else:
#             loc_scale_structure = self.args.reference_model

        
        pl = []
        if self.args.reference_map() is None:
            self.reference_map = os.path.join(
                self.job_location,
                'model_reference.mrc')
            #fix map and model when nstart is non-zero
            if ori_x == 0. and ori_y == 0. and ori_z == 0.:
                if map_nxstart != 0 or map_nystart != 0 or map_nzstart != 0:
                    map_edit = os.path.splitext(
                        os.path.basename(self.args.target_map.value))[0]+'_fix.mrc'
                    self.map_edit_path = os.path.join(
                                    self.job_location,map_edit)
                    # fix origin to agree with nstart
                    shutil.copyfile(self.args.target_map.value,self.map_edit_path)
                    assert os.path.isfile(self.map_edit_path)
                    with mrcfile.open(self.map_edit_path,'r+') as mrc:
                        mrc.header.origin.x = map_nxstart*apix[0]
                        mrc.header.origin.y = map_nystart*apix[1]
                        mrc.header.origin.z = map_nzstart*apix[2]
                    ori_x = map_nxstart*apix[0]
                    ori_y = map_nystart*apix[1]
                    ori_z = map_nzstart*apix[2]
                
            ##self.pdb_edit_path = self.args.pdb_path.value
            orig_name = os.path.basename(self.args.reference_model.value)
            orig_name_parts = os.path.splitext(orig_name)
            fix_name = orig_name_parts[0] + '_fix' + orig_name_parts[1]
            self.pdb_edit_path = os.path.join(self.job_location, fix_name)
            #translate model to origin 0 if nstart is 0
            if fix_map_model and not all(o == 0. for o in (ori_x,ori_y,ori_z)):
                trans_vector = [-ori_x,
                                -ori_y,
                                -ori_z]
                self.fix_model_for_refmac(self.args.reference_model.value,
                                          self.pdb_edit_path,trans_vector,
                                          remove_charges=True)
            else:
                #remove charges
                remove_atomic_charges(self.args.reference_model.value,
                                          self.pdb_edit_path)
                
            # Set PDB cryst from mtz
            self.process_set_unit_cell = refmac_task.SetModelCell(
                job_location=self.job_location,
                name='Set model unit cell',
                pdb_path=self.pdb_edit_path,
                map_path=self.map_edit_path)
            pl.append([self.process_set_unit_cell.process])

            if os.path.splitext(self.args.reference_model.value)[-1].lower() in ['.cif','.mmcif'] or \
                 os.stat(self.args.reference_model.value).st_size > 10000000:
                pdb_set_path = self.process_set_unit_cell.cifout_path
            else:
                pdb_set_path = self.process_set_unit_cell.pdbout_path

            # Calculate mtz from pdb 
            self.refmac_sfcalc_crd_process = refmac_task.RefmacSfcalcCrd(
                job_location=self.job_location,
                name='SF calc from coord',
                pdb_path=pdb_set_path,
                lib_in=self.args.lib_in(),
                resolution=self.args.resolution())
            self.refmac_sfcalc_mtz = os.path.join(
                self.job_location,
                'sfcalc_from_crd.mtz')
            pl.append([self.refmac_sfcalc_crd_process.process])

            #gemmi sf2map
            self.sf2map_process = sf2mapWrapper(
                job_location=self.job_location,
                name='SF to map',
                command=self.commands['ccpem-gemmi'],
                map_nx=map_nx,
                map_ny=map_ny,
                map_nz=map_nz,
                # For now only use x,y,z
    #                 fast=mrc_axis[int(map_mapc)],
    #                 medium=mrc_axis[int(map_mapr)],
    #                 slow=mrc_axis[int(map_maps)],
                fast='X',
                medium='Y',
                slow='Z',
                mtz_path=self.refmac_sfcalc_mtz,
                map_path=self.reference_map)
            pl.append([self.sf2map_process.process])
            #set origin and remove background
            self.reference_map_processed = os.path.join(
                self.job_location,
                'model_reference_syn.mrc')
            if not all(o == 0. for o in (ori_x,ori_y,ori_z)):
                self.mapprocess_wrapper = mapprocess_wrapper(
                                job_location=self.job_location,
                                command=self.commands['ccpem-mapprocess'],
                                map_path=self.reference_map,
                                list_process=['shift_origin','threshold'],
                                map_origin=(ori_x,ori_y,ori_z),
                                map_contour=0.02,
                                out_map=self.reference_map_processed
                                )
            else:
                self.mapprocess_wrapper = mapprocess_wrapper(
                                job_location=self.job_location,
                                command=self.commands['ccpem-mapprocess'],
                                map_path=self.reference_map,
                                list_process=['threshold'],
                                map_origin=(ori_x,ori_y,ori_z),
                                map_contour=0.02,
                                out_map=self.reference_map_processed
                                )
            pl.append([self.mapprocess_wrapper.process])
            
            self.reference_map = self.reference_map_processed
        
        else:
            self.reference_map = self.args.reference_map()
            with mrcfile.open(self.reference_map, 'r') as mrc:
                ref_mapc = mrc.header.mapc
                ref_mapr = mrc.header.mapr
                ref_maps = mrc.header.maps

            if not all([ref_mapc == 1,
                        ref_mapr == 2,
                        ref_maps == 3]):
                ref_xyz_path = 'xyz_' + os.path.basename(
                    self.reference_map)
                self.mapmask_process = mapmask.MapMask(
                    job_location=self.job_location,
                    fast='X',
                    medium='Y',
                    slow='Z',
                    mapin1=self.reference_map,
                    mapout=ref_xyz_path)
                pl.append([self.mapmask_process.process])
                self.reference_map = os.path.join(
                    self.job_location,
                    ref_xyz_path)

        # Temporary fix to set axis order to x,y,z
        # Target map
        self.target_map = self.args.target_map()
        if not all([map_mapc == 1,
                    map_mapr == 2,
                    map_maps == 3]):
            map_xyz_path = 'xyz_' + os.path.basename(self.target_map)
            self.mapmask_process = mapmask.MapMask(
                job_location=self.job_location,
                fast='X',
                medium='Y',
                slow='Z',
                mapin1=self.target_map,
                mapout=map_xyz_path)
            pl.append([self.mapmask_process.process])
            self.target_map = os.path.join(
                self.job_location,
                map_xyz_path)

        # Mask map
        self.mask_map = self.args.mask_map()
        if self.mask_map is not None:
            with mrcfile.open(self.mask_map, 'r') as mrc:
                mask_mapc = mrc.header.mapc
                mask_mapr = mrc.header.mapr
                mask_maps = mrc.header.maps

            if not all([mask_mapc == 1,
                        mask_mapr == 2,
                        mask_maps == 3]):
                mask_xyz_path = 'xyz_' + os.path.basename(self.mask_map)
                self.mapmask_process = mapmask.MapMask(
                    job_location=self.job_location,
                    fast='X',
                    medium='Y',
                    slow='Z',
                    mapin1=self.mask_map,
                    mapout=mask_xyz_path)
                pl.append([self.mapmask_process.process])
                self.mask_map = os.path.join(
                    self.job_location,
                    mask_xyz_path)

        # LocScale process
        self.loc_scale_process = LocScaleWrapper(
            job_location=self.job_location,
            target_map=self.target_map,
            resolution=self.args.resolution(),
            pixel_size=self.args.pixel_size(),
            reference_model_map=self.reference_map,
            use_mpi=self.args.use_mpi(),
            n_mpi=self.args.n_mpi(),
            mask_map = self.mask_map,
            output_map=None,
            window_size=self.args.window_size(),
            name='LocScale')
        pl.append([self.loc_scale_process.process])

        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            db_inject=db_inject,
            database_path=self.database_path,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

class LocScaleWrapper(object):
    '''
    Wrapper for LocScale process.
    '''
    commands = {
    'loc_scale_python': ['ccpem-python',
                         os.path.realpath(np_locscale_fft.__file__)]}

    def __init__(self,
                 job_location,
                 target_map,
                 resolution,
                 pixel_size,
                 reference_model_map,
                 use_mpi=False,
                 n_mpi=None,
                 output_map=None,
                 mask_map=None,
                 window_size=None,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        if window_size is None:
            window_size = int((7.0 * resolution) / pixel_size)
        if output_map is None:
            output_map = 'loc_scale.mrc'
        output_map = os.path.join(self.job_location,
                                  output_map)

        self.args = [
            '--em_map', os.path.abspath(target_map),
            '--model_map', os.path.abspath(reference_model_map),
            '--apix', pixel_size,
            '--window_size', window_size,
            '--outfile', output_map,
            '--verbose', 'True']

        if mask_map is not None:
            self.args += ['--mask', mask_map]

        # Set process, use mpi for multithreaded
        if use_mpi and n_mpi > 1:
            command = ['ccpem-mpirun',
                       '-np',
                       str(n_mpi)]
            self.args = self.commands['loc_scale_python'] + self.args
            self.args += ['--mpi']
            self.process = process_manager.CCPEMProcess(
                name=self.name,
                command=command,
                args=self.args,
                location=self.job_location,
                stdin=None)
        else:
            self.process = process_manager.CCPEMProcess(
                name=self.name,
                command=self.commands['loc_scale_python'],
                args=self.args,
                location=self.job_location,
                stdin=None)
