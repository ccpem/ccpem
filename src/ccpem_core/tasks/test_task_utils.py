#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import unittest
from ccpem_core.tasks import task_utils


class Test(unittest.TestCase):
    '''
    Test task utils
    '''
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_task_utils(self):
        '''
        Test CCPEMBibtexRef class
        '''
        ref = task_utils.CCPEMBibtexRef(
            author=('Adrian, M. and Dubochet, J. and Lepault, J. '
                    'and McDowall, A. W.'),
            journal='Nature',
            number='5954',
            pages='32--36',
            title='Cryo-electron microscopy of viruses.',
            volume='308',
            year='1984')
        assert ref.year == 1984
        assert ref.journal == 'Nature'


if __name__ == '__main__':
    unittest.main()
