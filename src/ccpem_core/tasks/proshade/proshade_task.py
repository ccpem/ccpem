#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from PyQt4 import QtGui, QtCore, QtWebKit
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import settings
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils


class ProShade(task_utils.CCPEMTask):
    '''
    ProShade task.
    '''
    task_info       = task_utils.CCPEMTaskInfo ( name               = 'ProShade',
                                                 author             = 'Tykac M, Murshudov G',
                                                 version            = 'ProSHADE 0.6.6 (DEC 2018)',
                                                 description        = (
                            'ProShade (Protein Shape Description and Symmetry Detection) tool provides several separate functionalities. These functionalities accept both, coordinate data as well as map data. '
                            'Symmetry detection allows finding the symmetry, the symmetry axes and symmetry group elements for any input file. Structure overlay functionality rotates and translates the moving '
                            'structure (map or PDB) to find the optimal overlay measured by the overall correlation with the static structure (also map or PDB); however, without optimisation (rough match only). '
                            'The map re-boxing functionality masks the input (map only) and attempts to find smaller box containing all the mask, thus decreasing the linear processing time for the map. This '
                            'functionality is also available for half maps. Finally distance computation functionality computes three shape similarity descriptors of increasing accuracy to allow enumerating '
                            'how similar two input structures (map or PDB) are in terms of their shape.' ),
                                                 documentation_link = 'http://www2.mrc-lmb.cam.ac.uk/groups/murshudov/content/proshade/proshade.html',
                                                 short_description  = ( 'ProSHADE tool supplies various functionalities for coordinate and map '
                                                                        'structures manipulation and processing.'  ),
                                                 references         = None)
        
    commands = {'proshade': settings.which(program='proshade')}

    def __init__ ( self,
                   database_path  = None,
                   args           = None,
                   args_json      = None,
                   pipeline       = None,
                   job_location   = None,
                   verbose        = False,
                   parent         = None):
        super ( ProShade, self).__init__ ( database_path    = database_path,
                                           args             = args,
                                           args_json        = args_json,
                                           pipeline         = pipeline,
                                           job_location     = job_location,
                                           verbose          = verbose,
                                           parent           = parent )

    def parser(self):
        # Option to add:
        #  ... describe detaiuls here

        parser = ccpem_argparser.ccpemArgParser()

        parser.add_argument ( '-job_title',
                              '--job_title',
                              help            = 'Short description of job',
                              metavar         = 'Job title',
                              type            = str,
                              default         = None )
        parser.add_argument ( '-mode',
                              '--mode',
                              help            = 'Functionality mode selection',
                              metavar         = 'Functionality mode',
                              choices         = [ 'Symmetry detection',
                                                  'Distance computation',
                                                  'Structure overlay',
                                                  'Map re-boxing' ],
                              type            = str,
                              default='Symmetry detection' )
        parser.add_argument ( '-input_str',
                              '--input_str',
                              help            = '',
                              type            = str,
                              metavar         = 'Map or PDB structure',
                              nargs           = '*',
                              default         = None )
        
        parser.add_argument ( '-sym_str',
                              '--sym_str',
                              help            = 'Structure whose symmetry is to be detected',
                              type            = str,
                              metavar         = 'Map or PDB structure',
                              default         = 'None' )
                              
        parser.add_argument ( '-static_str',
                              '--static_str',
                              help            = 'The static structure for structure overlay',
                              type            = str,
                              metavar         = 'Static map or PDB',
                              default         = None )
                              
        parser.add_argument ( '-moving_str',
                              '--moving_str',
                              help            = 'The moving structure for structure overlay',
                              type            = str,
                              metavar         = 'Moving map or PDB',
                              default         = None )
                              
        parser.add_argument ( '-rebox_str',
                               '--rebox_str',
                               help            = 'Input map for re-boxing',
                               type            = str,
                               metavar         = 'Map for re-boxing',
                               default         = None )
                               
        parser.add_argument ( '-out_file',
                              '--out_file',
                              help            = 'Output file name including path, if needed',
                              type            = str,
                              metavar         = 'Output file name',
                              default         = '' )
                              
        parser.add_argument ( '-req_sym',
                              '--req_sym',
                              help            = 'Requested symmetry to be preferentially found. Options are Cx (Cyclic), Dx (Dihedral), T (Tetrahedral), O (Octhedral) and I (Icosahedral), where x is the fold.',
                              type            = str,
                              metavar         = 'Requested Symmetry',
                              default         = '' )
                              
        parser.add_argument ( '-resolution',
                              '--resolution',
                              help            = 'Resolution (Angstrom)',
                              metavar         = 'Resolution (Angstrom)',
                              type            = float,
                              default         = None )
                              
        parser.add_argument ( '-bFactor',
                              '--bFactor',
                              help            = 'Change B-factors of PDB files',
                              metavar         = 'Change B-factors of PDB files',
                              type            = float,
                              default         = None )

        parser.add_argument ( '-sharpen',
                              '--sharpen',
                              help            = 'Sharpening/Blurring factor',
                              metavar         = 'Sharpening/Blurring factor',
                              type            = float,
                              default         = None )
                              
        parser.add_argument ( '-extraSpace',
                              '--extraSpace',
                              help            = 'Extra space around the structure',
                              metavar         = 'Extra space around the structure',
                              type            = float,
                              default         = None )
                              
        parser.add_argument ( '-bandwidth',
                              '--bandwidth',
                              help            = 'Maximum number of bands for SH',
                              metavar         = 'Maximum number of bands for SH',
                              type            = int,
                              default         = None )
                              
        parser.add_argument ( '-integOrder',
                              '--integOrder',
                              help            = 'Gauss-Legendre Integration Order',
                              metavar         = 'Gauss-Legendre Integration Order',
                              type            = int,
                              default         = None )
                              
        parser.add_argument ( '-sphDistance',
                              '--sphDistance',
                              help            = 'Distance between spheres (A)',
                              metavar         = 'Distance between spheres (A)',
                              type            = float,
                              default         = None )
                              
        parser.add_argument ( '-sphNumber',
                              '--sphNumber',
                              help            = 'Total number of spheres',
                              metavar         = 'Total number of spheres',
                              type            = int,
                              default         = None )
                              
        parser.add_argument ( '-mapMaskThres',
                              '--mapMaskThres',
                              help            = 'IQRs from median for map mask',
                              metavar         = 'IQRs from median for map mask',
                              type            = float,
                              default         = None )
                              
        parser.add_argument ( '-mapMaskBlur',
                              '--mapMaskBlur',
                              help            = 'Sharpening factor for map mask',
                              metavar         = 'Sharpening factor for map mask',
                              type            = float,
                              default         = None )
                              
        parser.add_argument ( '-symPeakSize',
                              '--symPeakSize',
                              help            = 'No map points forming sym. peak',
                              metavar         = 'No map points forming sym. peak',
                              type            = int,
                              default         = None )
                              
        parser.add_argument ( '-symPeakThres',
                              '--symPeakThres',
                              help            = 'IQRs from median for peaks',
                              metavar         = 'IQRs from median for peaks',
                              type            = float,
                              default         = None )
                              
        parser.add_argument ( '-axisTolerance',
                              '--axisTolerance',
                              help            = 'Symmetry axes tolerance',
                              metavar         = 'Symmetry axes tolerance',
                              type            = float,
                              default         = None )

        return parser

    def run_pipeline ( self, job_id = None, db_inject = None ):
        '''
        Generate job classes and process.  Run=false for reloading.
        '''
        # Get data from multiple inputs
        
        # Get processes
        ps = ProShadeCLI ( name               = self.task_info.name,
                           command            = self.commands['proshade'],
                           jsrview_path       = os.path.join(os.environ['CCPEM'], 'share'),
                           job_location       = self.job_location,
                           mode               = self.args.mode ( ),
                           input_str          = self.args.input_str ( ),
                           sym_str            = self.args.sym_str ( ),
                           static_str         = self.args.static_str ( ),
                           moving_str         = self.args.moving_str ( ),
                           rebox_str          = self.args.rebox_str ( ),
                           out_file           = self.args.out_file ( ),
                           req_sym            = self.args.req_sym ( ),
                           resolution         = self.args.resolution ( ),
                           bFactor            = self.args.bFactor ( ),
                           sharpen            = self.args.sharpen ( ),
                           extraSpace         = self.args.extraSpace ( ),
                           bandwidth          = self.args.bandwidth ( ),
                           integOrder         = self.args.integOrder ( ),
                           sphDistance        = self.args.sphDistance ( ),
                           sphNumber          = self.args.sphNumber ( ),
                           mapMaskThres       = self.args.mapMaskThres ( ),
                           mapMaskBlur        = self.args.mapMaskBlur ( ),
                           symPeakSize        = self.args.symPeakSize ( ),
                           symPeakThres       = self.args.symPeakThres ( ),
                           axisTolerance      = self.args.axisTolerance ( ) )
        pl = [[ps.process]]

        # Run pipeline
        self.pipeline = process_manager.CCPEMPipeline (
                           pipeline           = pl,
                           job_id             = job_id,
                           args_path          = self.args.jsonfile,
                           location           = self.job_location,
                           database_path      = self.database_path,
                           db_inject          = db_inject,
                           taskname           = self.task_info.name,
                           title              = self.args.job_title.value,
                           verbose            = self.verbose,
                           on_finish_custom   = None )
        self.pipeline.start()

class ProShadeCLI(object):
    '''
    Run ProShade via Commanc Line Interface
    '''
    def __init__ ( self,
                   name,
                   command,
                   jsrview_path,
                   job_location,
                   mode,
                   input_str,
                   sym_str,
                   static_str,
                   moving_str,
                   rebox_str,
                   out_file,
                   req_sym,
                   resolution,
                   bFactor,
                   sharpen,
                   extraSpace,
                   bandwidth,
                   integOrder,
                   sphDistance,
                   sphNumber,
                   mapMaskThres,
                   mapMaskBlur,
                   symPeakSize,
                   symPeakThres,
                   axisTolerance ):
        self.command                          = command
        self.jsrview_path                     = jsrview_path
        self.job_location                     = job_location
        self.mode                             = mode
        self.input_str                        = input_str
        self.sym_str                          = sym_str
        self.static_str                       = static_str
        self.moving_str                       = moving_str
        self.rebox_str                        = rebox_str
        self.out_file                         = out_file
        self.req_sym                          = req_sym
        self.resolution                       = resolution
        self.bFactor                          = bFactor
        self.sharpen                          = sharpen
        self.extraSpace                       = extraSpace
        self.bandwidth                        = bandwidth
        self.integOrder                       = integOrder
        self.sphDistance                      = sphDistance
        self.sphNumber                        = sphNumber
        self.mapMaskThres                     = mapMaskThres
        self.mapMaskBlur                      = mapMaskBlur
        self.symPeakSize                      = symPeakSize
        self.symPeakThres                     = symPeakThres
        self.axisTolerance                    = axisTolerance
       
        self.args                             = []
        self.set_args                         ()

        self.process = process_manager.CCPEMProcess (
                           name               = name,
                           command            = self.command,
                           args               = self.args,
                           location           = self.job_location,
                           stdin              = None )

    def representsInt ( self, str_in ):
        try:
            int ( str_in )
            return True
        except ValueError:
            return False

    def set_args ( self ):
        # Set path to jsrview
        self.args.append('--rvpath')
        self.args.append(self.jsrview_path)

        # Deal with functionality mode value
        if self.mode == 'Symmetry detection':
            self.args.append                  ( '-S' )
            if self.sym_str is not None:
                self.args.append              ( '-f' )
                self.args.append              ( self.sym_str )
            else:
                # Error message
                text = 'No input structures provided'
                QtGui.QMessageBox.critical        ( None, 'Error', text )
                return False
            if self.req_sym is not None:
                secVal                        = self.req_sym[1:]
                if self.req_sym == 'I':
                    self.args.append          ( '--sym' )
                    self.args.append          ( self.req_sym )
                elif self.req_sym == 'O':
                    self.args.append          ( '--sym' )
                    self.args.append          ( self.req_sym )
                elif self.req_sym == 'T':
                    self.args.append          ( '--sym' )
                    self.args.append          ( self.req_sym )
                elif self.representsInt ( secVal ) and self.req_sym[0] == 'D':
                    self.args.append          ( '--sym' )
                    self.args.append          ( self.req_sym )
                elif self.representsInt ( secVal ) and self.req_sym[0] == 'C':
                    self.args.append          ( '--sym' )
                    self.args.append          ( self.req_sym )
                else:
                    # Error message
                    text = 'Not allowed requested symmetry description. Please use one of the following values: I (for Icosahedral symmetry), O (for Octahedral symmetry), T (for tetrahedral symmetry), D followed by integer for dihedral symmetry or C followed by integer for cyclic symmetry.'
                    QtGui.QMessageBox.critical ( None, 'Error', text )
            if self.out_file is not None:
                self.args.append              ( '--clearMap' )
                self.args.append              ( self.out_file )
            if self.resolution is not None:
                self.args.append              ( '--resolution' )
                self.args.append              ( self.resolution )
            if self.bFactor is not None:
                self.args.append              ( '--bValues' )
                self.args.append              ( self.bFactor )
            if self.sharpen is not None:
                self.args.append              ( '--bChange' )
                self.args.append              ( self.sharpen )
            if self.bandwidth is not None:
                self.args.append              ( '--bandWidth' )
                self.args.append              ( self.bandwidth )
            if self.integOrder is not None:
                self.args.append              ( '--integration' )
                self.args.append              ( self.integOrder )
            if self.sphDistance is not None:
                self.args.append              ( '--sphDistance' )
                self.args.append              ( self.sphDistance )
            if self.sphNumber is not None:
                self.args.append              ( '--sphNumber' )
                self.args.append              ( self.sphNumber )
            if self.mapMaskThres is not None:
                self.args.append              ( '--mapMaskThres' )
                self.args.append              ( self.mapMaskThres )
            if self.mapMaskBlur is not None:
                self.args.append              ( '--mapMaskBlur' )
                self.args.append              ( self.mapMaskBlur )
            if self.symPeakSize is not None:
                self.args.append              ( '--peakSize' )
                self.args.append              ( self.symPeakSize )
            if self.symPeakThres is not None:
                self.args.append              ( '--peakThres' )
                self.args.append              ( self.symPeakThres )
            if self.axisTolerance is not None:
                self.args.append              ( '--axisTolerance' )
                self.args.append              ( self.axisTolerance )
        if self.mode == 'Distance computation':
            self.args.append                  ( '-D' )
            # Deal with no/single/multiple input maps
            if self.input_str is not None:
                if isinstance ( self.input_str, list ):
                    for str in self.input_str:
                        self.args.append      ( '-f' )
                        self.args.append      ( str )
                else:
                    text = 'A single structure was provided for distances between structures computation'
                    QtGui.QMessageBox.critical    ( None, 'Error', text )
                    return False
            else:
                # Error message
                text = 'No input structures provided'
                QtGui.QMessageBox.critical        ( None, 'Error', text )
                return False
            if self.resolution is not None:
                self.args.append              ( '--resolution' )
                self.args.append              ( self.resolution )
            if self.bFactor is not None:
                self.args.append              ( '--bValues' )
                self.args.append              ( self.bFactor )
            if self.sharpen is not None:
                self.args.append              ( '--bChange' )
                self.args.append              ( self.sharpen )
            if self.extraSpace is not None:
                self.args.append              ( '--cellBorderSpace' )
                self.args.append              ( self.extraSpace )
            if self.bandwidth is not None:
                self.args.append              ( '--bandWidth' )
                self.args.append              ( self.bandwidth )
            if self.integOrder is not None:
                self.args.append              ( '--integration' )
                self.args.append              ( self.integOrder )
            if self.sphDistance is not None:
                self.args.append              ( '--sphDistance' )
                self.args.append              ( self.sphDistance )
            if self.sphNumber is not None:
                self.args.append              ( '--sphNumber' )
                self.args.append              ( self.sphNumber )
            if self.mapMaskThres is not None:
                self.args.append              ( '--mapMaskThres' )
                self.args.append              ( self.mapMaskThres )
            if self.mapMaskBlur is not None:
                self.args.append              ( '--mapMaskBlur' )
                self.args.append              ( self.mapMaskBlur )
            if self.symPeakSize is not None:
                self.args.append              ( '--peakSize' )
                self.args.append              ( self.symPeakSize )
            if self.symPeakThres is not None:
                self.args.append              ( '--peakThres' )
                self.args.append              ( self.symPeakThres )
        if self.mode == 'Structure overlay':
            self.args.append                  ( '-O' )
            if self.static_str is not None:
                self.args.append              ( '-f' )
                self.args.append              ( self.static_str )
            else:
                # Error message
                text = 'Missing static structure in the structure overlay mode.'
                QtGui.QMessageBox.critical        ( None, 'Error', text )
                return False
            if self.moving_str is not None:
                self.args.append              ( '-f' )
                self.args.append              ( self.moving_str )
            else:
                # Error message
                text = 'Missing moving structure in the structure overlay mode.'
                QtGui.QMessageBox.critical        ( None, 'Error', text )
                return False
            if self.out_file is not None:
                self.args.append              ( '--clearMap' )
                self.args.append              ( self.out_file )
            if self.resolution is not None:
                self.args.append              ( '--resolution' )
                self.args.append              ( self.resolution )
            if self.bFactor is not None:
                self.args.append              ( '--bValues' )
                self.args.append              ( self.bFactor )
            if self.sharpen is not None:
                self.args.append              ( '--bChange' )
                self.args.append              ( self.sharpen )
            if self.bandwidth is not None:
                self.args.append              ( '--bandWidth' )
                self.args.append              ( self.bandwidth )
            if self.integOrder is not None:
                self.args.append              ( '--integration' )
                self.args.append              ( self.integOrder )
            if self.sphDistance is not None:
                self.args.append              ( '--sphDistance' )
                self.args.append              ( self.sphDistance )
            if self.sphNumber is not None:
                self.args.append              ( '--sphNumber' )
                self.args.append              ( self.sphNumber )
            if self.symPeakSize is not None:
                self.args.append              ( '--peakSize' )
                self.args.append              ( self.symPeakSize )
            if self.symPeakThres is not None:
                self.args.append              ( '--peakThres' )
                self.args.append              ( self.symPeakThres )
            if self.extraSpace is not None:
                self.args.append              ( '--cellBorderSpace' )
                self.args.append              ( self.extraSpace )
        if self.mode == 'Map re-boxing':
            self.args.append                  ( '-E' )
            if self.rebox_str is not None:
                self.args.append              ( '-f' )
                self.args.append              ( self.rebox_str )
            else:
                # Error message
                text = 'No input structures provided'
                QtGui.QMessageBox.critical        ( None, 'Error', text )
                return False
            if self.out_file is not None:
                self.args.append              ( '--clearMap' )
                self.args.append              ( self.out_file )
            if self.extraSpace is not None:
                self.args.append              ( '--cellBorderSpace' )
                self.args.append              ( self.extraSpace )
            if self.mapMaskThres is not None:
                self.args.append              ( '--mapMaskThres' )
                self.args.append              ( self.mapMaskThres )
            if self.mapMaskBlur is not None:
                self.args.append              ( '--mapMaskBlur' )
                self.args.append              ( self.mapMaskBlur )
