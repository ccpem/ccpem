#!/bin/sh
# Run Haruspex with hpx conda environment setup
# $1 == target map
# $2 == output directory

# Source hpx channel for conda
. activate hpx

# Print setup
echo "Running Haruspex..."
echo "Target map: $1"
echo "Output directory: $2"

# # Reset python path
# export PYTHONPATH=""
# export PATH=""
#
# __conda_setup="$(CONDA_REPORT_ERRORS=false '/opt/anaconda3/bin/conda' shell.bash hook 2> /dev/null)"
# if [ $? -eq 0 ]; then
#     \eval "$__conda_setup"
# else
#     if [ -f "/opt/anaconda3/etc/profile.d/conda.sh" ]; then
#         . "/opt/anaconda3/etc/profile.d/conda.sh"
#         CONDA_CHANGEPS1=false conda activate base
#     else
#         \export PATH="/opt/anaconda3/bin:$PATH"
#     fi
# fi
# unset __conda_setup
#
# echo $PYTHONPATH
# echo $PATH

# Launch Haruspex
OPT="-n /Users/tom/code/haruspex/network/hpx_190116 -d map-predict -m $1 -o $2"
# exec python3
exec python3 '/Users/tom/code/haruspex/source/hpx_unet_190116.py' $OPT
