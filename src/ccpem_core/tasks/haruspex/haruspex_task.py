#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core import settings
import hpx_unet_190116

class Haruspex(task_utils.CCPEMTask):
    '''
    CCPEM Haruspex task.
    N.B. requires:
    ccpem-python -m pip install tensorflow==1.8.0
    
    To do:
    (1) Finish job process / GUI
    
    (2) Write warning in ccpem tasks to check for presence of tensor flow
        --> Give pip instructions if missing
    
    (3) Write unit test - check voxel values also check python2.7 version is
    same as python3
    
    (4) Where to put trained model - just in CCP-EM for now?!
    
    (5) Deposit python2.7 version / merge w/ python3 version
    
    (6) Check license
    
    (7) Get test data from Andrea - i.e. know map w/ outputs
    
    (8) Add nucleic acid button
    '''

    task_info = task_utils.CCPEMTaskInfo(
        name='Haruspex',
        author='Philipp Mostosi, Hermann Schindelin, Philip Kollmannsberger, Andrea Thorn',
        version='1.0',
        description=(
            '<p> Identifies secondary structure regions in a EM map.'),
        short_description=(
            'Label secondary structure'),
        documentation_link='https://www.uni-wuerzburg.de/en/rvz/research/associated-research-groups/thorn-group/',
        references=None)

    commands = {'haruspex': ['ccpem-python',
                             os.path.realpath(hpx_unet_190116.__file__)]
                }

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        super(Haruspex, self).__init__(
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        target_map = parser.add_argument_group()
        target_map.add_argument(
            '-target_map',
            '--target_map',
            help='Target map to be used (mrc format)',
            metavar='Target map',
            type=str,
            default=None)
        #
        display_model = parser.add_argument_group()
        display_model.add_argument(
            '-display_model',
            '--display_model',
            help='Optional atomic model. Not used for calculation only for display. (pdb format)',
            metavar='Display model',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-n_cpu',
            '--n_cpu',
            help='Number of cpu to use',
            metavar='CPU cores',
            type=int,
            default=None)
        #
        parser.add_argument(
            '-p_mem',
            '--p_mem',
            help='Percentage of GPU memory to use',
            metavar='GPU memory %',
            type=float,
            default=50)

        return parser

    def run_pipeline(self, job_id=None, db_inject=None):
        try:
            import tensorflow as tf # @UnusedImport
        except ImportError:
            print('Please install tensor flow to use Haruspex')
            print('ccpem-python -m pip install tensorflow==1.8.0')

        pl = []
        self.process_haruspex = HaruspexWrapper(
            name='Label secondary structure',
            job_location=self.job_location,
            n_cpu=self.args.n_cpu(),
            p_mem=self.args.p_mem(),
            target_map=self.args.target_map())
        pl.append([self.process_haruspex.process])
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

class HaruspexWrapper(object):
    '''
    Wrapper for Haruspex process.
    '''
    def __init__(self,
                 job_location,
                 target_map,
                 n_cpu,
                 p_mem,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__

        # Find Haruspex
        settings_args = settings.get_ccpem_settings()
        network_path = None
        if settings_args.haruspex_path() is not None:
            path = os.path.join(
                settings_args.haruspex_path(),
                'network/hpx_190116')
            if os.path.exists(path):
                network_path = path

        if network_path is None:
            print('Haruspex path not found in CCP-EM settings')
            print('Please add path in CCP-EM settings json file e.g.:')
            print('"haruspex_path": "<path_to_haruspex>",')
            print('Settings path:')
            print settings_args.location()

        self.args = [
            '-n', network_path,
            '-d', 'map-predict', os.path.abspath(target_map),
            '-F',
            '-n_cpu', n_cpu,
            '-p_mem', p_mem,
            '-o', job_location]

        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=Haruspex.commands['haruspex'],
            args=self.args,
            location=self.job_location,
            stdin=None)
