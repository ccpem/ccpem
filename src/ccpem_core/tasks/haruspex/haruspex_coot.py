'''
author: @Luise Kandler, Thorn Lab, University of Hamburg
written: 22.06.21

Please put this script in your .coot-preferences folder.
'''

####################################################################################################################
####################################################################################################################

import gtk
import os
import webbrowser

####################################################################################################################
####################################################################################################################

plugin_menu = coot_menubar_menu("Haruspex")
add_simple_coot_menu_menuitem(plugin_menu, "Haruspex Settings", lambda func: haruspex_settings_window())
add_simple_coot_menu_menuitem(plugin_menu, "Load existing Haruspex Output", lambda func: load_haruspex_output_window())
add_simple_coot_menu_menuitem(plugin_menu, "What is Haruspex?", lambda func: what_is_haruspex_window())

####################################################################################################################
####################################################################################################################

# LOAD OUTPUT MAPS AND SET DEFAULT VALUES


# radius function: to set the map radius (in Angstrom)
set_map_radius(70)

# read, open, colour and set sigma level of helix map
imol_helix = 0  # loads helix map
set_map_colour(imol_helix, 0.8, 0.0, 0.0)  # colour red
set_contour_level_absolute(imol_helix, 0.07)  # default sigma level

# read, open, colour and set sigma level of sheet map
imol_sheet = 1  # loads sheet map
set_map_colour(imol_sheet, 0.3, 0.4, 1.0)  # colour blue
set_contour_level_absolute(imol_sheet, 0.07)  # default sigma level

# read, open, colour and set sigma level of npair map
imol_npair = 2  # loads npair map
set_map_colour(imol_npair, 1.0, 0.6, 0.0)  # colour orange
set_contour_level_absolute(imol_npair, 0.07)  # default sigma level

# read, open, colour and set sigma level of npair map
imol_input = 3  # loads npair map
set_map_colour(imol_input, 0.6, 0.6, 0.6)  # colour grey
set_contour_level_absolute(imol_input, 0.07)  # default sigma level
set_draw_map_standard_lines(int(imol_input), 0) # hides the input map by default, but can be displayed in Haruspex Settings

# save imol numbers
curr_working_dir = os.getcwd()
curr_imol_filename = curr_working_dir + "/imol_numbers.txt"
curr_imol_file = open(curr_imol_filename, "w")
curr_imol_file.write(str(imol_helix) + "\n" + str(imol_sheet) + "\n" + str(imol_npair) + "\n" + str(imol_input))
curr_imol_file.close()

####################################################################################################################
####################################################################################################################

# HARUSPEX SETTINGS WINDOW


def new_sigma_value_set(sigma_level_button):
    sigma_value = sigma_level_button.get_value()

    curr_working_dir = os.getcwd()
    curr_imol_filename = curr_working_dir + "/imol_numbers.txt"
    with open(curr_imol_filename) as file:
        for line in file:
            imol = line.strip()
            set_contour_level_absolute(int(imol), sigma_value)


def Helix_button_toggled(helix_on_off):
    curr_working_dir = os.getcwd()
    curr_imol_filename = curr_working_dir + "/imol_numbers.txt"
    imol_file = open(curr_imol_filename)
    imol_helix = imol_file.readline()
    imol_sheet = imol_file.readline()
    imol_npair = imol_file.readline()
    imol_input = imol_file.readline()
    print(imol_helix)
    print(imol_sheet)
    print(imol_npair)
    print(imol_input)

    helix_state = helix_on_off.get_active()
    if helix_state == True:
        print("Helix Map displayed")
        set_draw_map_standard_lines(int(imol_helix), 1)
    else:
        print("Helix Map hidden")
        set_draw_map_standard_lines(int(imol_helix), 0)


def Sheet_button_toggled(sheet_on_off):
    curr_working_dir = os.getcwd()
    curr_imol_filename = curr_working_dir + "/imol_numbers.txt"
    imol_file = open(curr_imol_filename)
    imol_helix = imol_file.readline()
    imol_sheet = imol_file.readline()
    imol_npair = imol_file.readline()
    imol_input = imol_file.readline()
    print(imol_helix)
    print(imol_sheet)
    print(imol_npair)
    print(imol_input)

    sheet_state = sheet_on_off.get_active()
    if sheet_state == True:
        print("Sheet Map displayed")
        set_draw_map_standard_lines(int(imol_sheet), 1)
    else:
        print("Sheet Map hidden")
        set_draw_map_standard_lines(int(imol_sheet), 0)


def DNA_button_toggled(npair_on_off):
    curr_working_dir = os.getcwd()
    curr_imol_filename = curr_working_dir + "/imol_numbers.txt"
    imol_file = open(curr_imol_filename)
    imol_helix = imol_file.readline()
    imol_sheet = imol_file.readline()
    imol_npair = imol_file.readline()
    imol_input = imol_file.readline()
    print(imol_helix)
    print(imol_sheet)
    print(imol_npair)
    print(imol_input)

    npair_state = npair_on_off.get_active()
    if npair_state == True:
        print("DNA/RNA Map displayed")
        set_draw_map_standard_lines(int(imol_npair), 1)
    else:
        print("DNA/RNA Map hidden")
        set_draw_map_standard_lines(int(imol_npair), 0)


def Input_button_toggled(input_on_off):
    curr_working_dir = os.getcwd()
    curr_imol_filename = curr_working_dir + "/imol_numbers.txt"
    imol_file = open(curr_imol_filename)
    imol_helix = imol_file.readline()
    imol_sheet = imol_file.readline()
    imol_npair = imol_file.readline()
    imol_input = imol_file.readline()
    print(imol_helix)
    print(imol_sheet)
    print(imol_npair)
    print(imol_input)

    input_state = input_on_off.get_active()
    if input_state == True:
        print("Input Map displayed")
        set_draw_map_standard_lines(int(imol_input), 1)
    else:
        print("Input Map hidden")
        set_draw_map_standard_lines(int(imol_input), 0)


def link_button_clicked(data, url, label):
    webbrowser.open(url)


def haruspex_settings_window():

   # defining the window
   window = gtk.Window(gtk.WINDOW_TOPLEVEL)
   window.set_title("Haruspex Settings")
   window.set_default_size(300, 100)

   # defining three frames
   sigma_frame = gtk.AspectFrame("Sigma Level", 0.5, 0.5, 1.0, True)
   npair_frame = gtk.AspectFrame("Display/Hide Maps", 0.5, 0.5, 1.0, True)
   legend_frame = gtk.AspectFrame("Legend", 0.5, 0.5, 1.0, True)

   # defining a big vertical box to pack all frames
   big_vbox = gtk.VBox(False, 0)
   big_vbox.pack_start(sigma_frame, False, False, 6)
   big_vbox.pack_start(npair_frame, False, False, 6)
   big_vbox.pack_start(legend_frame, False, False, 6)
   window.add(big_vbox)

   # defining three small vertical boxes that are packed into the frames
   sigma_vbox = gtk.VBox(False, 0)
   npair_vbox = gtk.VBox(False, 0)
   legend_vbox = gtk.VBox(False, 0)
   sigma_frame.add(sigma_vbox)
   npair_frame.add(npair_vbox)
   legend_frame.add(legend_vbox)



   ################################################################## SIGMA BOX
   # label
   label_text = " Set your own sigma level.                            "
   label = gtk.Label(label_text)

   # sigma_level_button
   adjustment = gtk.Adjustment(0.07, 0.00, 1.0, 0.01, 0.05, 0) # start value, minimum, maximum, step increment, page increment, page size
   sigma_level_button = gtk.SpinButton(adjustment, 0.1, 3) # adjustment values + climb rate, digits
   sigma_level_button.set_update_policy(gtk.UPDATE_ALWAYS)
   sigma_level_button.connect("value-changed", new_sigma_value_set)  # it emits a signal every time the value changes

   # pack label and button to sigma box
   sigma_vbox.pack_start(label, False, False, 6)
   sigma_vbox.pack_start(sigma_level_button, False, False, 6)

   # tooltip
   sigma_level_button_tooltip = gtk.Tooltips()
   sigma_level_button_tooltip_text = "Adjust the sigma level by clicking the up/down arrows or by scrolling. Or just type in your prefered value."
   sigma_level_button_tooltip.set_tip(sigma_level_button, sigma_level_button_tooltip_text, None)
   ################################################################## SIGMA BOX



   ################################################################## DNA/RNA BOX

   # Helix check button
   helix_on_off = gtk.CheckButton("display Helix Map                                ", True)
   helix_on_off.set_active(True)
   helix_on_off.connect("toggled", Helix_button_toggled)

   # DNA check button
   sheet_on_off = gtk.CheckButton("display Sheet Map                                ", True)
   sheet_on_off.set_active(True)
   sheet_on_off.connect("toggled", Sheet_button_toggled)

   # DNA check button
   npair_on_off = gtk.CheckButton("display DNA/RNA Map                              ", True)
   npair_on_off.set_active(True)
   npair_on_off.connect("toggled", DNA_button_toggled)

   # DNA check button
   input_on_off = gtk.CheckButton("display Input Map                                ", True)
   input_on_off.set_active(False)
   input_on_off.connect("toggled", Input_button_toggled)

   # pack button to npair box
   npair_vbox.pack_start(helix_on_off, False, False, 6)
   npair_vbox.pack_start(sheet_on_off, False, False, 6)
   npair_vbox.pack_start(npair_on_off, False, False, 6)
   npair_vbox.pack_start(input_on_off, False, False, 6)

   # tooltip
   helix_on_off_tooltip = gtk.Tooltips()
   helix_on_off_tooltip_text = "Here you can decide if you want to display or hide the Helix output map."
   helix_on_off_tooltip.set_tip(helix_on_off, helix_on_off_tooltip_text, None)

   sheet_on_off_tooltip = gtk.Tooltips()
   sheet_on_off_tooltip_text = "Here you can decide if you want to display or hide the Sheet output map."
   sheet_on_off_tooltip.set_tip(sheet_on_off, sheet_on_off_tooltip_text, None)

   npair_on_off_tooltip = gtk.Tooltips()
   npair_on_off_tooltip_text = "Here you can decide if you want to display or hide the DNA/RNA output map."
   npair_on_off_tooltip.set_tip(npair_on_off, npair_on_off_tooltip_text, None)

   input_on_off_tooltip = gtk.Tooltips()
   input_on_off_tooltip_text = "Here you can decide if you want to display or hide the input map."
   input_on_off_tooltip.set_tip(input_on_off, input_on_off_tooltip_text, None)
   ################################################################## DNA/RNA BOX



   ################################################################## LEGEND BOX
   # legend label
   legend_text = ''' red = Helix Map                                 
 blue = Sheet Map                                 
 orange = DNA/RNA Map
 grey = Input Map                                       '''
   legend = gtk.Label(legend_text)

   # link button
   gtk.link_button_set_uri_hook(link_button_clicked, None)  # sets a global link
   link_to_paper = gtk.LinkButton("https://onlinelibrary.wiley.com/doi/epdf/10.1002/anie.202000421", "Haruspex Reference")

   # pack label and link button to legend box
   legend_vbox.pack_start(legend, False, False, 6)
   legend_vbox.pack_start(link_to_paper, False, False, 6)

   # tool tips
   link_to_paper_tooltip = gtk.Tooltips()
   link_to_paper_tooltip_text = "This is a link to the Haruspex paper."
   link_to_paper_tooltip.set_tip(link_to_paper, link_to_paper_tooltip_text, None)
   ################################################################## LEGEND BOX

   window.show_all()

####################################################################################################################
####################################################################################################################

# LOAD EXISTING HARUSPEX OUTPUT


def error_popup_invalid_choice():
    error_popup = gtk.MessageDialog(None, 0, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, None)
    error_popup.set_title("Error: Invalid choice")
    error_popup.set_markup('<span foreground="blue" size="100">Blue text</span>Invalid choice. Please choose a file with the specified endings.')
    error_popup.run()
    error_popup.destroy()


def files_chosen(ok_button, helix_map, sheet_map, npair_map, window):
    # Warning popup
    warning_popup = gtk.MessageDialog(None, 0, gtk.MESSAGE_INFO, gtk.BUTTONS_OK_CANCEL, None)
    warning_popup.set_title("Warning: Process takes some time")
    warning_popup.set_markup('<span foreground="blue" size="100">Blue text</span>This process can take a few seconds..... Please click OK to continue and wait until this window disappears.')
    response = warning_popup.run()
    print(response)
    if response == -5:
        try:
            # deleting existing maps
            try:
                curr_working_dir = os.getcwd()
                curr_imol_filename = curr_working_dir + "/imol_numbers.txt"
                with open(curr_imol_filename) as file:
                    for line in file:
                        imol = line.strip()
                        close_molecule(int(imol))
            except:
                print("no files loaded previously.")

            haruspex_helix = helix_map.get_filename()
            haruspex_sheet = sheet_map.get_filename()
            haruspex_npair = npair_map.get_filename()

            # radius function: to set the map radius (in Angstrom)
            set_map_radius(70)

            # read, open, colour and set sigma level of helix map
            imol_helix = handle_read_ccp4_map(haruspex_helix, 0)  # loads helix map
            set_map_colour(imol_helix, 0.8, 0.0, 0.0)  # colour red
            set_contour_level_absolute(imol_helix, 0.07)  # default sigma level

            # read, open, colour and set sigma level of sheet map
            imol_sheet = handle_read_ccp4_map(haruspex_sheet, 0)  # loads sheet map
            set_map_colour(imol_sheet, 0.3, 0.4, 1.0)  # colour blue
            set_contour_level_absolute(imol_sheet, 0.07)  # default sigma level

            # read, open, colour and set sigma level of npair map (if existent)
            imol_npair = handle_read_ccp4_map(haruspex_npair, 0)  # loads npair map
            set_map_colour(imol_npair, 1.0, 0.6, 0.0)  # colour orange
            set_contour_level_absolute(imol_npair, 0.07)  # default sigma level

            # save imol numbers
            curr_working_dir = os.getcwd()
            curr_imol_filename = curr_working_dir + "/imol_numbers.txt"
            curr_imol_file = open(curr_imol_filename, "w")
            curr_imol_file.write(str(imol_helix) + "\n" + str(imol_sheet) + "\n" + str(imol_npair))
            curr_imol_file.close()

            warning_popup.destroy()
            window.destroy()
            haruspex_settings_window()

        except:
            error_popup_invalid_choice()
            warning_popup.destroy()

    else:
        warning_popup.destroy()


def load_haruspex_output_window():
    window = gtk.Window(gtk.WINDOW_TOPLEVEL)
    window.set_title("Load Haruspex Output")
    window.set_default_size(400, 150)

    frame = gtk.AspectFrame(None, 0.5, 0.5, 1.0, True)
    window.add(frame)

    vbox = gtk.VBox(False, 0)
    frame.add(vbox)

    ################################################################## VERTICAL BOX
    # Label
    label_text = ''' Select an existing Haruspex output series. '''
    label = gtk.Label(label_text)

    helix_label_text = " Helix Map: "
    helix_label = gtk.Label(helix_label_text)

    sheet_label_text = " Sheet Map: "
    sheet_label = gtk.Label(sheet_label_text)

    npair_label_text = " Npair Map: "
    npair_label = gtk.Label(npair_label_text)

    # Input map File Chooser
    helix_map = gtk.FileChooserButton("Helix map")
    sheet_map = gtk.FileChooserButton("Sheet map")
    npair_map = gtk.FileChooserButton("DNA/RNA map")

    # Ok Button
    ok_button = gtk.Button("OK")
    ok_button.connect("clicked", files_chosen, helix_map, sheet_map, npair_map, window)

    # Tooltips
    helix_map_tooltip = gtk.Tooltips()
    helix_map_tooltip_text = "Select the Helix map of your output series."
    helix_map_tooltip.set_tip(helix_map, helix_map_tooltip_text, None)

    sheet_map_tooltip = gtk.Tooltips()
    sheet_map_tooltip_text = "Select the Sheet map of your output series."
    sheet_map_tooltip.set_tip(sheet_map, sheet_map_tooltip_text, None)

    npair_map_tooltip = gtk.Tooltips()
    npair_map_tooltip_text = "Select the Npair map of your output series."
    npair_map_tooltip.set_tip(npair_map, npair_map_tooltip_text, None)

    ok_button_tooltip = gtk.Tooltips()
    ok_button_tooltip_text = "Click this button to load your selected output series."
    ok_button_tooltip.set_tip(ok_button, ok_button_tooltip_text, None)

    # packing everything into the vertical box
    vbox.pack_start(label, False, False, 6)
    vbox.pack_start(helix_label, False, False, 6)
    vbox.pack_start(helix_map, False, False, 6)
    vbox.pack_start(sheet_label, False, False, 6)
    vbox.pack_start(sheet_map, False, False, 6)
    vbox.pack_start(npair_label, False, False, 6)
    vbox.pack_start(npair_map, False, False, 6)
    vbox.pack_start(ok_button, False, False, 6)
    ################################################################## VERTICAL BOX

    window.show_all()


####################################################################################################################
####################################################################################################################

# WHAT IS HARUSPEX?


def what_is_haruspex_window():
    popup = gtk.MessageDialog(None, 0, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, None)
    popup.set_title("What is Haruspex?")
    popup.set_markup('<span foreground="blue" size="100">Blue text</span>Haruspex is a neural network for an automated annotation of protein secondary structure and oligonucleotides from Cryo-Electron Microscopy maps. It can facilitate inicial model building extremely by predicting the present secondary structure. The result of a Haruspex annotation are three maps, one for each annotated structural element. Red indicates alpha-helices, blue stands for beta-sheets and DNA/RNA is coloured in orange. The HARUSPEX SETTINGS window provides options to set the same sigma level for all three output maps simultaneously and an option to display or hide DNA/RNA.')
    popup.run()
    popup.destroy()
