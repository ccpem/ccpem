#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import os
import shutil
import tempfile
import unittest

import pandas as pd

import mrc_to_mtz_results
from ccpem_core import test_data


class MrcToMtzResultsTest(unittest.TestCase):
    '''
    Unit test for MRC to MTZ results.
    '''
    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_data_dir = os.path.dirname(test_data.__file__)
        self.test_mtz = os.path.join(self.test_data_dir, 'mtz', 'starting_map.mtz')
        self.test_output = tempfile.mkdtemp()
        os.chdir(self.test_output)

    def tearDown(self):
        if os.path.exists(self.test_output):
            shutil.rmtree(self.test_output)

    def test_arg_parsing(self):
        parsed = mrc_to_mtz_results.parse_args(['--mtz_file', 'Test.mtz', '1.0', '2.0', '-3.1'])
        assert parsed.mtz_file == 'Test.mtz'
        assert parsed.blur_factors == [1.0, 2.0, -3.1]

    def test_amps_calculation(self):
        assert os.path.isfile(self.test_mtz)
        mean_amps, mean_amps_log = mrc_to_mtz_results.\
            calculate_mean_amplitudes(self.test_mtz, [200, 150, 100, 50, -50, -100, -150, -200])
        assert len(mean_amps) == 20
        assert len(mean_amps_log) == 20
        # Check a few values to make sure results are vaguely sensible
        self.assertAlmostEqual(mean_amps.iloc[0, 0], 477.193, 3)
        self.assertAlmostEqual(mean_amps.iloc[17, 1], 241.685, 3)
        self.assertAlmostEqual(mean_amps.iloc[19, 8], 4.249, 3)
        self.assertAlmostEqual(mean_amps_log.iloc[1, 1], 5.240, 3)
        self.assertAlmostEqual(mean_amps_log.iloc[18, 7], 2.148, 3)

    def test_writing_amps_cache(self):
        shutil.copy(self.test_mtz, self.test_output)
        mrc_to_mtz_results.cache_mean_amplitudes(os.path.basename(self.test_mtz),
                                                 [200, 150, 100, 50, -50, -100, -150, -200])
        assert os.path.isfile(os.path.join(self.test_output, mrc_to_mtz_results.MEAN_AMPS_CSV))
        assert os.path.isfile(os.path.join(self.test_output, mrc_to_mtz_results.MEAN_AMPS_LOG_CSV))

    def test_writing_amps_cache_twice(self):
        """Make sure the cache files are overwritten, not appended to"""
        # Write cache files
        shutil.copy(self.test_mtz, self.test_output)
        mrc_to_mtz_results.cache_mean_amplitudes(os.path.basename(self.test_mtz),
                                                 [200, 150, 100, 50, -50, -100, -150, -200])
        # Save cache file sizes
        amps_size = os.stat(mrc_to_mtz_results.MEAN_AMPS_CSV).st_size
        amps_log_size = os.stat(mrc_to_mtz_results.MEAN_AMPS_LOG_CSV).st_size

        # Write cache files again
        mrc_to_mtz_results.cache_mean_amplitudes(os.path.basename(self.test_mtz),
                                                 [200, 150, 100, 50, -50, -100, -150, -200])
        # Check cache file sizes are the same as before
        assert amps_size == os.stat(mrc_to_mtz_results.MEAN_AMPS_CSV).st_size
        assert amps_log_size == os.stat(mrc_to_mtz_results.MEAN_AMPS_LOG_CSV).st_size

    def test_reading_amps_cache(self):
        mean_amps, mean_amps_log = mrc_to_mtz_results.get_amps_from_cache(self.test_mtz)
        assert mean_amps is not None
        assert mean_amps_log is not None

    def test_amps_calculation_and_cache_are_equal(self):
        mean_amps_calc, mean_amps_log_calc = mrc_to_mtz_results.\
            calculate_mean_amplitudes(self.test_mtz, [200, 150, 100, 50, -50, -100, -150, -200])
        mean_amps_cached, mean_amps_log_cached = mrc_to_mtz_results.get_amps_from_cache(self.test_mtz)
        pd.testing.assert_frame_equal(mean_amps_calc, mean_amps_cached)
        pd.testing.assert_frame_equal(mean_amps_log_calc, mean_amps_log_cached)


if __name__ == '__main__':
    unittest.main()
