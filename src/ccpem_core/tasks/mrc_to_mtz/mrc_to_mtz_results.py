#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
from __future__ import print_function

import argparse
import os.path
from collections import OrderedDict

import numpy
import pandas as pd
from scipy import stats

from clipper_tools.em import structure_factors

MEAN_AMPS_CSV = 'mean_amps.csv'
MEAN_AMPS_LOG_CSV = 'mean_amps_log.csv'

INDEX_NAME = r'Resolution ($\AA$)'


def calculate_mean_amplitudes(mtz_in_path, blur_array):
    '''
    Calculate mean amplitudes from mtz file.

    Returns two DataFrames for mean amplitudes and log(mean amplitudes)
    indexed by resolution.
    '''
    # Prepare column labels for different sharpening factors
    blur_array.append(0)
    column_labels = OrderedDict()
    for blur in sorted(blur_array):
        # For unprocessed input data
        if blur == 0:
            column_labels['[Fout0, Pout0]'] = (r'0 $\AA^2$ (input)')
        else:
            if blur > 0:
                label = '[FoutBlur_{0:.2f}, Pout0]'.format(blur)
                column_labels[label] = (r'{0:.0f} $\AA^2$'.format(blur))
            else:
                label = '[FoutSharp_{0:.2f}, Pout0]'.format(-blur)
                column_labels[label] = (r'{0:.0f} $\AA^2$'.format(blur))

    reso = None
    mean_amps = None
    mean_amps_log = None

    for col in column_labels.keys():
        mtz = structure_factors.ClipperMTZ(mtz_in_path)
        if reso is None:
            mtz.import_column_data(column_label=col,
                                   get_resolution=True)
            reso = mtz.column_data['resolution_1/Ang^2']
        else:
            mtz.import_column_data(column_label=col,
                                   get_resolution=False)
        data = mtz.column_data[col]['F']

        # Get binned stats (mean / shell)
        n_bins = len(reso) / 1000
        n_bins = max(20, n_bins)
        # Use numpy.nanmean to avoid NaN in the first bin from F000
        bin_means, bin_edges, bin_num = stats.binned_statistic(
            reso,
            data,
            numpy.nanmean,
            bins=n_bins)

        # Get bin centres
        bin_centres = []
        for n in xrange(len(bin_edges) - 1):
            c = 0.5 * (bin_edges[n] + bin_edges[n+1])
            bin_centres.append(c)

        if mean_amps is None:
            mean_amps = pd.DataFrame(data=bin_means,
                                     index=bin_centres,
                                     columns=[column_labels[col]])
            mean_amps.index.name = INDEX_NAME
            mean_amps_log = pd.DataFrame(data=numpy.log(bin_means),
                                         index=bin_centres,
                                         columns=[column_labels[col]])
            mean_amps_log.index.name = INDEX_NAME
        else:
            mean_amps[column_labels[col]] = bin_means
            mean_amps_log[column_labels[col]] = numpy.log(bin_means)

    return mean_amps, mean_amps_log


def get_amps_from_cache(mtz_path):
    out_dir = os.path.dirname(mtz_path)
    amps_file = os.path.join(out_dir, MEAN_AMPS_CSV)
    amps_log_file = os.path.join(out_dir, MEAN_AMPS_LOG_CSV)
    if os.path.isfile(amps_file):
        mean_amps = pd.read_csv(amps_file, index_col=INDEX_NAME)
        mean_amps_log = pd.read_csv(amps_log_file, index_col=INDEX_NAME)
        return mean_amps, mean_amps_log
    else:
        return None, None


def cache_mean_amplitudes(mtz_path, blur_array):
    mean_amps, mean_amps_log = calculate_mean_amplitudes(mtz_path, blur_array)
    out_dir = os.path.dirname(mtz_path)
    mean_amps.to_csv(os.path.join(out_dir, MEAN_AMPS_CSV))
    mean_amps_log.to_csv(os.path.join(out_dir, MEAN_AMPS_LOG_CSV))


def parse_args(args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('--mtz_file', default='starting_map.mtz')
    parser.add_argument('blur_factors', type=float, nargs='+')
    return parser.parse_args(args)


def main():
    args = parse_args()
    print("Calculating structure factor amplitude statistics...")
    cache_mean_amplitudes(args.mtz_file, args.blur_factors)
    print("Done")


if __name__ == '__main__':
    main()
