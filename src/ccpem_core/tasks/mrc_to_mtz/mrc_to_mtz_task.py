#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import os

from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core.settings import which
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.tasks.refmac import refmac_task
from ccpem_core.tasks.mrc_to_mtz import mrc_to_mtz_results


class MrcToMtz(task_utils.CCPEMTask):
    '''
    CCPEM Refmac map blur / sharpen utility.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='MRCtoMTZ',
        author='Brown A, Long F, Nicholls RA, Long F, Toots J, Murshudov G',
        version='5.8.0103',
        description=(
            'Convert MRC map to MTZ structure factors using Refmac.  '
            'Includes map blur / sharpen utility.  '
            'Macromolecular refinement program.  REFMAC is a program '
            'designed for REFinementof MACromolecular structures.  It uses '
            'maximum likelihood and some elements of Bayesian '
            'statistics.  N.B. requires CCP4.'),
        short_description=(
            'Convert MRC map to MTZ structure factors using Refmac.  '
            'Includes map blur / sharpen utility.'),
        references=None)

    commands = {'refmac': which(program='refmac5')}

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 verbose=False,
                 parent=None):
        super(MrcToMtz, self).__init__(
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            verbose=verbose,
            parent=parent)
        # Set empty mtz_path so we don't have to use hasattr elsewhere
        self.mtz_path = ''

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-mapin',
            '--input_map',
            help='''Target input map (MRC format)''',
            type=str,
            metavar='Input map',
            default=None)
        #
        parser.add_argument(
            '-resolution',
            '--resolution',
            help='''Resolution of input map (Angstrom)''',
            metavar='Resolution',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-mapref',
            '--reference_map',
            help='Reference map for sharpening (MRC format)',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-start_pdb',
            '--start_pdb',
            help=('Optional to aid visualisation only; does not affect '
                  'MTZ generation (PDB format)'),
            type=str,
            default=None)
        #
        parser.add_argument(
            '-blur_array',
            '--blur_array',
            help=(
                'List of B factors to sharpen or blur map. Negative '
                'values to sharpen, positive to blur'),
            metavar='Sharpen / blur',
            type=list,
            default=[-200,-100,-50,50,100,200])
        #
        return parser

    def run_pipeline(self, run=True, job_id=None, db_inject=None):
        '''
        Generate job classes and process.  Run=false for reloading.
        '''
        self.log_path = os.path.join(self.job_location,
                                     'stdout.txt')

        # Wrangle blur / sharpen arrays so only postive values in either
        # This is for cosmetic display in Coot etc. e.g.-100 blur is shown
        # with 100 sharp column label.
        blur_array = []
        sharp_array = []
        for n in self.args.blur_array():
            if n > 0:
                blur_array.append(n)
            else:
                sharp_array.append(-n)

        process_maptomtz = refmac_task.RefmacMapToMtz(
            command=self.commands['refmac'],
            name='MRC to MTZ',
            resolution=self.args.resolution.value,
            mode='Global',
            job_location=self.job_location,
            map_path=self.args.input_map.value,
            blur_array=blur_array,
            sharp_array=sharp_array)

        self.mtz_path = process_maptomtz.hklout_path

        # Generate results
        mrc_to_mtz_results_path = os.path.realpath(mrc_to_mtz_results.__file__)
        mrc_to_mtz_results_process = process_manager.CCPEMProcess(
            name='MRC to MTZ results',
            command='ccpem-python',
            args=[mrc_to_mtz_results_path, '--mtz_file', self.mtz_path] + self.args.blur_array.value,
            location=self.job_location)

        # Set pipeline
        pl = [[process_maptomtz.process],
              [mrc_to_mtz_results_process]]

        if run:
            self.pipeline = process_manager.CCPEMPipeline(
                pipeline=pl,
                job_id=job_id,
                args_path=self.args.jsonfile,
                location=self.job_location,
                database_path=self.database_path,
                db_inject=db_inject,
                taskname=self.task_info.name,
                title=self.args.job_title.value,
                verbose=self.verbose)
            self.pipeline.start()

    def validate_args(self):
        # Now do at gui level
        return True
