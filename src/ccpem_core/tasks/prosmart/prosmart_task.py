#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


# ProSMART CCP4 implementation details
# http://www2.mrc-lmb.cam.ac.uk/groups/murshudov/content/prosmart/documentation.html#refmac
# http://www.ccp4.ac.uk/newsletters/newsletter48/articles/ProSmart/an_overview_of_prosmart.html

from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import settings
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils


class ProSMART(task_utils.CCPEMTask):
    '''
    ProSMART task.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='ProSMART',
        author='Nicholls R, Murshudov G',
        version='0.850',
        description=(
            'ProSMART (Procrustes Structural Matching Alignment and Restraints '
            'Tool) has two main purposes: conformation-independent comparison '
            'of protein structures, and the generation of interatomic distance '
            'restraints for subsequent use in macromolecular crystallographic '
            'refinement by REFMAC5.'),
        documentation_link = 'http://www2.mrc-lmb.cam.ac.uk/groups/murshudov/content/prosmart/documentation.html',
#              '<p><a href="http://www2.mrc-lmb.cam.ac.uk/groups/murshudov/content/prosmart/documentation.html">Documentation</a>'),
        short_description=(
            'External restraint generation for atomic modelling. '
            'Requires CCP4'),
        references=None)

    commands = {'prosmart': settings.which(program='prosmart')}

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 verbose=False,
                 parent=None):
        super(ProSMART, self).__init__(
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            verbose=verbose,
            parent=parent)
        #

    def parser(self):
        # Option to add:
        #  Restraints mode
        #    1) Reference model
        #    2) H-bonding patterns
        #    3) Structural fragments (SSE conformations)
        #    4) Use target structure as reference

        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        parser.add_argument(
            '-mode',
            '--mode',
            help='Alignment mode',
            metavar='Aligment mode',
            choices=['Reference model',
                     'H-bonding patterns',
                     'Structural fragments',
                     'All-on-all',
                     'Self restrain'],
            type=str,
            default='Reference model'
            )
        parser.add_argument(
            '-target_pdbs',
            '--target_pdbs',
            help='Target pdb(s)',
            metavar='Target PDB',
            type=str,
            nargs='*',
            default=None)
        parser.add_argument(
            '-target_pdb_chains',
            '--target_pdb_chains',
            help='Target PDB chain(s)',
            metavar='Target PDB chain selections',
            type=str,
            nargs='*',
            default=None)
        parser.add_argument(
            '-ref_pdbs',
            '--ref_pdbs',
            help='Reference PDB(s)',
            metavar='Reference PDB',
            type=str,
            nargs='*',
            default=None)
        parser.add_argument(
            '-ref_pdb_chains',
            '--ref_pdb_chains',
            help='Reference PDB chain(s)',
            metavar='Reference PDB chain selections',
            type=str,
            nargs='*',
            default=None)
        parser.add_argument(
            '-threads',
            '--threads',
            help='Number of threads',
            metavar='Threads',
            type=int,
            default=4)
        #
        parser.add_argument(
            '-keywords',
            '--keywords',
            help='Additional ProSMART arguments for advanced options.',
            type=str,
            default='')
        #
        return parser

    def run_pipeline(self, job_id=None, db_inject=None):
        '''
        Generate job classes and process.  Run=false for reloading.
        '''
        # Get processes
        ps = ProSMARTCLI(
            name=self.task_info.name,
            command=self.commands['prosmart'],
            job_location=self.job_location,
            mode=self.args.mode(),
            target_pdbs=self.args.target_pdbs(),
            target_pdb_chains=self.args.target_pdb_chains(),
            reference_pdbs=self.args.ref_pdbs(),
            reference_pdb_chains=self.args.ref_pdb_chains(),
            keywords=self.args.keywords(),
            threads=self.args.threads())
        pl = [[ps.process]]

        # Run pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value,
            verbose=self.verbose,
            on_finish_custom=None)
        self.pipeline.start()

class ProSMARTCLI(object):
    '''
    Run ProSMART via CLI
    '''
    def __init__(self,
                 name,
                 command,
                 job_location,
                 mode,
                 target_pdbs,
                 target_pdb_chains,
                 reference_pdbs,
                 reference_pdb_chains,
                 keywords='',
                 threads=None):
        self.command = command
        self.job_location = job_location
        self.mode = mode
        self.target_pdbs = target_pdbs
        self.target_pdb_chains = target_pdb_chains
        self.reference_pdbs = reference_pdbs
        self.reference_pdb_chains = reference_pdb_chains
        if not isinstance(self.target_pdbs, list):
            self.target_pdbs = [self.target_pdbs]
        if not isinstance(self.reference_pdbs, list):
            self.reference_pdbs = [self.reference_pdbs]
        self.keywords = keywords
        self.threads = threads
        #
        self.args = []
        self.set_args()
        #
        self.process = process_manager.CCPEMProcess(
            name=name,
            command=self.command,
            args=self.args,
            location=self.job_location,
            stdin=None)

    def set_args(self):
        self.args.append('-p1')
        for pdb in self.target_pdbs:
            self.args.append(pdb)
        if self.target_pdb_chains is not None:
            self.args.append('-c1')
            for chain in self.target_pdb_chains:
                self.args.append(chain)
        if self.mode == 'Reference model':
            self.args.append('-p2')
            for pdb in self.reference_pdbs:
                self.args.append(pdb)
        if self.reference_pdb_chains is not None:
            self.args.append('-c2')
            for chain in self.reference_pdb_chains:
                self.args.append(chain)
        elif self.mode == 'Structural fragments':
            self.args.append('-lib')
        elif self.mode == 'H-bonding patterns':
            self.args.append('-h')
        elif self.mode == 'All-on-all':
            # No additional arguments should be specified
            pass
        elif self.mode == 'Self restrain':
            self.args.append('-self_restrain')
        if self.threads is not None:
            self.args.append('-threads')
            self.args.append(self.threads)
        if self.keywords is not None:
            if len(self.keywords) > 0:
                keywordsSplit = self.keywords.split()
                self.args += keywordsSplit
