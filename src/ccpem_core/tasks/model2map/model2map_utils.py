import os
from ccpem_core.model_tools.gemmi_utils import *
import numpy as np

def set_map_grid(modelfile,apix,edge=20.0):
    list_coords = get_coordinates(modelfile)
    coords_array = np.array(list_coords)
    del list_coords
    list_x,list_y,list_z = np.transpose(coords_array)
    min_x = min(list_x)-edge
    max_x = max(list_x)+edge
    min_y = min(list_y)-edge
    max_y = max(list_y)+edge
    min_z = min(list_z)-edge
    max_z = max(list_z)+edge
    dim_x,dim_y,dim_z = max_x-min_x,max_y-min_y,max_z-min_z
    #adjust dimensions to number of voxels
    rem_x,rem_y,rem_z = dim_x%apix,dim_y%apix,dim_z%apix
    dim_x,dim_y,dim_z = dim_x+apix-rem_x,dim_y+apix-rem_y,dim_z+apix-rem_z
    if int(dim_z/apix)%2 != 0 : dim_z = dim_z+apix #z not even?
    if int(dim_x/apix)%2 != 0 : dim_x = dim_x+apix #z not even?
    origin = min_x-apix+rem_x,min_y-apix+rem_y,min_z-apix+rem_z
    return origin, (int(dim_x/apix),int(dim_y/apix),int(dim_z/apix))

def set_cubic_map_grid(modelfile,apix,edge=20.0):
    list_coords = get_coordinates(modelfile)
    coords_array = np.array(list_coords)
    del list_coords
    list_x,list_y,list_z = np.transpose(coords_array)
    min_x = min(list_x)-edge
    max_x = max(list_x)+edge
    min_y = min(list_y)-edge
    max_y = max(list_y)+edge
    min_z = min(list_z)-edge
    max_z = max(list_z)+edge
    dim_x,dim_y,dim_z = max_x-min_x,max_y-min_y,max_z-min_z
    dim = [dim_x,dim_y,dim_z]
    max_dim = max(dim)
    #set cubic dimensions
    if max_dim-dim_x != 0.: 
        min_x = min_x-(max_dim-dim_x)/2.
        max_x = max_x+(max_dim-dim_x)/2.
    if max_dim-dim_y != 0.:
        min_y = min_y-(max_dim-dim_y)/2.
        max_y = max_y+(max_dim-dim_y)/2.
    if max_dim-dim_z != 0.:
        min_z = min_z-(max_dim-dim_z)/2.
        max_z = max_z+(max_dim-dim_z)/2.
    dim_x,dim_y,dim_z = max_x-min_x,max_y-min_y,max_z-min_z
    #adjust dimensions to number of voxels
    rem_x,rem_y,rem_z = dim_x%apix,dim_y%apix,dim_z%apix
    dim_x,dim_y,dim_z = dim_x+apix-rem_x,dim_y+apix-rem_y,dim_z+apix-rem_z
    origin = min_x-apix+rem_x,min_y-apix+rem_y,min_z-apix+rem_z
    return origin, (int(dim_x/apix),int(dim_y/apix),int(dim_z/apix))