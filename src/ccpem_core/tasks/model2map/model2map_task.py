#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.tasks.refmac import refmac_task
from ccpem_core.tasks.bin_wrappers import fft
from ccpem_core.map_tools.TEMPy import map_preprocess
from ccpem_core.model_tools.gemmi_utils import shift_coordinates, remove_atomic_charges
import mrcfile
from ccpem_core import settings
import shutil,json
from model2map_utils import set_cubic_map_grid, set_map_grid

class Model2Map(task_utils.CCPEMTask):
    '''
    CCPEM tool for atomic model to map conversion.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='model2map',
        author='G. Murshudov, R. Nicholas, T. Burnley, C. Palmer, A. Joseph',
        version='1.0',
        description=(
            '''
            Tool for atomic model to MRC map conversion using
            Refmac5 (CCP4), gemmi, mrcfile and TEMPy mapprocess. Refmac5 uses 
            electron scattering factors and considers the map resolution and 
            atomic B-factors to generate map.<br>
            References:<br>
            <a href="http://scripts.iucr.org/cgi-bin/paper?S1399004714021683"> 
            Brown et al, 2015</a><br>
            <a href="http://scripts.iucr.org/cgi-bin/paper?S2059798318007313">
            Nicholls et al. 2017</a>
            '''
                    ),
        short_description=(
            'Model to map coversion'),
        documentation_link='',
        references=('http://scripts.iucr.org/cgi-bin/paper?S1399004714021683',
                    'http://scripts.iucr.org/cgi-bin/paper?S2059798318007313'))

    commands = {
        'refmac': settings.which(program='refmac5'),
        'ccpem-gemmi':settings.which(program='gemmi'),
        'ccpem-mapprocess':
        ['ccpem-python', os.path.realpath(map_preprocess.__file__)]}
    
    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        #
        super(Model2Map, self).__init__(
             database_path=database_path,
             args=args,
             args_json=args_json,
             pipeline=pipeline,
             job_location=job_location,
             parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        pdb_path = parser.add_argument_group()
        pdb_path.add_argument(
            '-pdb_path',
            '--pdb_path',
            help=('Synthetic map will be created and difference maps '
                  'calculated using this'),
            metavar='Input atomic model',
            type=str,
            default=None)
        #
        map_resolution = parser.add_argument_group()
        map_resolution.add_argument(
            '-map_resolution',
            '--map_resolution',
            help='''Resolution of synthetic map (Angstrom)''',
            metavar='Resolution of map',
            type=float,
            default=None)
        #ligand dictionary for Refmac
        parser.add_argument(
            '-lib_in',
            '--lib_in',
            help='Ligand dictionary for Refmac (cif format)',
            metavar='Ligand dictionary',
            type=str,
            default=None)
        #
        map_apix = parser.add_argument_group()
        map_apix.add_argument(
            '-map_apix',
            '--map_apix',
            help='''Grid spacing for synthetic map (Angstrom)''',
            metavar='Grid spacing',
            type=float,
            default=None)
        #
        map_path = parser.add_argument_group()
        map_path.add_argument(
            '-map_path',
            '--map_path',
            help='Input reference map (mrc format)',
            metavar='Input reference map',
            type=str,
            default=None)
        
        map_path.add_argument(
            '-map_edit_path',
            '--map_edit_path',
            help='Edited map (mrc format)',
            metavar='Edited input map',
            type=str,
            default=None)
                
        return parser
    
    def edit_argsfile(self,args_file):
        with open(args_file,'r') as f:
            json_args = json.load(f)
        #set map and pdb paths if fixed for origin
        json_args['pdb_path'] = self.pdb_edit_path
                    
        with open(args_file,'w') as f:
            json.dump(json_args,f)
    
    def fix_model_for_refmac(self,pdb_path,pdb_edit_path,trans_vector,
                             remove_charges=False):
        shift_coordinates(pdb_path,pdb_edit_path,trans_vector,remove_charges)
    
    def run_pipeline(self, job_id=None, db_inject=None):
        pl = []
        fix_map_model = True
        self.map_edit_path = None
        self.args.map_edit_path.value = None
        #use Refmac for model map conversion
        #if a target map is given
        if self.args.map_path.value != None and \
                os.path.isfile(self.args.map_path.value):
            self.args.map_edit_path.value = self.args.map_path.value
            self.args.map_edit_path.value, fix_map_model, self.cella, \
            (ori_x_shift,ori_y_shift,ori_z_shift), (map_nx,map_ny,map_nz), (map_ori_x,map_ori_y,map_ori_z)= \
                fix_map_model_refmac(self.args.map_path.value,self.args.map_edit_path.value,
                                     self.job_location)
            #print ori_x_shift,ori_y_shift,ori_z_shift, map_ori_x,map_ori_y,map_ori_z
#                     #reset map path
#                     self.args.map_path.value = self.map_edit_path
        else:
            #find map grid origin and dimensions
            #voxel size = resolution/3
            if self.args.map_apix.value == None or self.args.map_apix.value == 0.0:
                apix = self.args.map_resolution.value/3.0
            else:
                apix = self.args.map_apix.value
            #add edge to the box bounding atom coordinates
            edge = max(30.0,3*self.args.map_resolution.value)
            ori,map_n = set_map_grid(self.args.pdb_path.value,apix,edge=edge)
            map_nx,map_ny,map_nz = map_n
            max_dim = max(map_n)
            ori_x_shift,ori_y_shift,ori_z_shift = ori
            map_ori_x,map_ori_y,map_ori_z = ori_x_shift,ori_y_shift,ori_z_shift
            self.cella = (map_nx*apix,map_ny*apix,map_nz*apix)
            #cella = (max_dim*apix,max_dim*apix,max_dim*apix)
            
        ##self.pdb_edit_path = self.args.pdb_path.value
        if os.path.splitext(self.args.pdb_path.value)[-1].lower() in ['.cif','.mmcif']:
            self.pdb_edit_path = os.path.join(self.job_location,
                                os.path.splitext(os.path.basename(
                                    self.args.pdb_path.value))[0]+'_fix.cif')
        else:
            self.pdb_edit_path = os.path.join(self.job_location,
                                os.path.splitext(os.path.basename(
                                    self.args.pdb_path.value))[0]+'_fix.pdb')

        #translate model to origin 0 if origin is non zero or nstart is zero
        if fix_map_model and not all(o == 0. for o in (ori_x_shift,ori_y_shift,ori_z_shift)):
            trans_vector = [-ori_x_shift,
                            -ori_y_shift,
                            -ori_z_shift]
            self.fix_model_for_refmac(self.args.pdb_path.value,
                                      self.pdb_edit_path,trans_vector,
                                      remove_charges=True)
        else:
            #remove charges
            remove_atomic_charges(self.args.pdb_path.value,
                                      self.pdb_edit_path)
#             #reset pdb path
#             self.args.pdb_path.value = self.pdb_edit_path
#             args_file = os.path.join(self.job_location,
#                          'args.json')
#             self.edit_argsfile(args_file)
            
        model_map_filename = os.path.splitext(
                    os.path.basename(self.args.pdb_path.value))[0]
        self.reference_map = os.path.join(
            self.job_location,
            model_map_filename+'_refmac.mrc')
        
        # Set cell parameters and scale of model
        self.process_set_unit_cell = refmac_task.SetModelCell(
            job_location=self.job_location,
            name='Set model unit cell ',
            pdb_path=self.pdb_edit_path,
            cella = self.cella,
            map_path=self.args.map_edit_path.value)
        pl = [[self.process_set_unit_cell.process]]

        if os.path.splitext(self.args.pdb_path.value)[-1].lower() in ['.cif','.mmcif'] or \
             os.stat(self.args.pdb_path.value).st_size > 10000000:
            self.pdb_set_path = self.process_set_unit_cell.cifout_path
            self.refmac_refined = self.process_set_unit_cell.cifout_path
        else:
            self.pdb_set_path = self.process_set_unit_cell.pdbout_path
            self.refmac_refined = self.process_set_unit_cell.pdbout_path

        # Calculate mtz from pdb 
        self.refmac_sfcalc_crd_process = refmac_task.RefmacSfcalcCrd(
            job_location=self.job_location,
            pdb_path=self.pdb_set_path,
            lib_in=self.args.lib_in(),
            resolution=self.args.map_resolution.value,
            command=self.commands['refmac'])
        self.refmac_sfcalc_mtz = os.path.join(
            self.job_location,
            'sfcalc_from_crd.mtz')
        pl.append([self.refmac_sfcalc_crd_process.process])
        #gemmi sf2map
        self.sf2map_process = sf2mapWrapper(
            job_location=self.job_location,
            command=self.commands['ccpem-gemmi'],
            map_nx=map_nx,
            map_ny=map_ny,
            map_nz=map_nz,
            # For now only use x,y,z
#                 fast=mrc_axis[int(map_mapc)],
#                 medium=mrc_axis[int(map_mapr)],
#                 slow=mrc_axis[int(map_maps)],
            fast='X',
            medium='Y',
            slow='Z',
            mtz_path=self.refmac_sfcalc_mtz,
            map_path=self.reference_map)
        pl.append([self.sf2map_process.process])
        
# #         # Convert calc mtz to map
#         self.fft_process = fft.FFT(
#             job_location=self.job_location,
#             map_nx=map_nx,
#             map_ny=map_ny,
#             map_nz=map_nz,
#             # For now only use x,y,z
# #                 fast=mrc_axis[int(map_mapc)],
# #                 medium=mrc_axis[int(map_mapr)],
# #                 slow=mrc_axis[int(map_maps)],
#             fast='X',
#             medium='Y',
#             slow='Z',
#             mtz_path=self.refmac_sfcalc_mtz,
#             map_path=self.reference_map)
#         pl.append([self.fft_process.process])
        
        #set origin and remove background
        self.reference_map_processed = os.path.join(
                self.job_location,
                model_map_filename+'_syn.mrc')
        if not all(o == 0. for o in (map_ori_x,map_ori_y,map_ori_z)):
            self.mapprocess_wrapper = mapprocess_wrapper(
                            job_location=self.job_location,
                            command=self.commands['ccpem-mapprocess'],
                            map_path=self.reference_map,
                            list_process=['shift_origin','threshold'],
                            map_origin=(map_ori_x,map_ori_y,map_ori_z),
                            map_contour=0.09,
                            out_map=self.reference_map_processed
                            )
        else:
            self.mapprocess_wrapper = mapprocess_wrapper(
                            job_location=self.job_location,
                            command=self.commands['ccpem-mapprocess'],
                            map_path=self.reference_map,
                            list_process=['threshold'],
                            map_origin=(map_ori_x,map_ori_y,map_ori_z),
                            map_contour=0.09,
                            out_map=self.reference_map_processed
                            )
        pl.append([self.mapprocess_wrapper.process])
        
        self.reference_map = self.reference_map_processed
        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

def fix_map_model_refmac(mapfile,mapeditfile,job_location):
    ori_x = ori_y = ori_z = 0.
    fix_map_model = True
    with mrcfile.open(mapfile, mode='r',permissive=True) as mrc:
        map_nx = mrc.header.nx
        map_ny = mrc.header.ny
        map_nz = mrc.header.nz
        map_nxstart = mrc.header.nxstart
        map_nystart = mrc.header.nystart
        map_nzstart = mrc.header.nzstart
        map_mapc = mrc.header.mapc
        map_mapr = mrc.header.mapr
        map_maps = mrc.header.maps
        ori_x = mrc.header.origin.x
        ori_y = mrc.header.origin.y
        ori_z = mrc.header.origin.z
        cella = (mrc.header.cella.x,mrc.header.cella.y,
                 mrc.header.cella.z)
        apix = mrc.voxel_size.item()
        map_dim = (map_nx,map_ny,map_nz)
    map_ori_x = ori_x
    map_ori_y = ori_y
    map_ori_z = ori_z
    
    #fix map origin when nstart is non-zero
    #if ori_x == 0. and ori_y == 0. and ori_z == 0.:
    if map_nxstart != 0 or map_nystart != 0 or map_nzstart != 0:
        #fix origin (for Refmac5)
        map_edit = os.path.splitext(
                    os.path.basename(mapfile))[0]+'_fix.mrc'
        mapeditfile = os.path.join(job_location,map_edit)
        shutil.copyfile(mapfile,mapeditfile)
        try: assert os.path.isfile(mapeditfile)
        except AssertionError:
            mapeditfile,fix_map_model, cella,(ori_x,ori_y,ori_z), (map_nx,map_ny,map_nz)
        #edit float origin
        with mrcfile.open(mapeditfile,'r+') as mrc:
            mrc.header.origin.x = map_nxstart*apix[0]
            mrc.header.origin.y = map_nystart*apix[1]
            mrc.header.origin.z = map_nzstart*apix[2]
            mrc.header.nxstart = 0
            mrc.header.nystart = 0
            mrc.header.nzstart = 0
            
        map_ori_x = map_nxstart*apix[0]
        map_ori_y = map_nystart*apix[1]
        map_ori_z = map_nzstart*apix[2]
        
        ori_x = map_ori_x
        ori_y = map_ori_y
        ori_z = map_ori_z
            
            
    return mapeditfile,fix_map_model, cella,[ori_x,ori_y,ori_z], \
        (map_nx,map_ny,map_nz), [map_ori_x,map_ori_y,map_ori_z]


class sf2mapWrapper():
    '''
    Wrapper for gemmi sf2map
    '''
    def __init__(self,
                 job_location,
                 command,
                 map_nx,
                 map_ny,
                 map_nz,
                 fast,
                 medium,
                 slow,
                 mtz_path,
                 map_path,
                 name=None):
        
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        # Set args
        self.args = ['sf2map','-f','Fout0','-p','Pout0']
        gridarg = '--grid='+','.join([str(map_nx),str(map_ny),str(map_nz)])
        self.args += [gridarg]
        self.args += ['--exact',mtz_path,map_path]
        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)

class mapprocess_wrapper():
    '''
    Wrapper for TEMPy Map Processing tool.
    '''
    def __init__(self,
                 job_location,
                 command,
                 map_path,
                 list_process,
                 map_resolution=None,
                 map_contour=None,
                 map_apix=None,
                 map_pad=None,
                 cropped_dim=None,
                 map_origin=None,
                 mask_path=None,
                 out_map=None,
                 name='MapProcess'):
        assert map_path is not None
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.map_path = ccpem_utils.get_path_abs(map_path)
        # Set args
        self.args = ['-m', self.map_path]
        if list_process is not None:
            list_process_args = ['-l']
            list_process_args.extend(list_process)
            self.args += list_process_args
        if map_resolution is not None:
            self.args += ['-r', map_resolution]
        #threshold
        if map_contour is not None:
            self.args += ['-t', map_contour]
        #new apix
        if map_apix is not None:
            self.args += ['-p', map_apix]
        #padding
        if map_pad is not None:
            pad_args = ['-pad']
            pad_args.extend(list(map_pad))
            if len(map_pad) > 0: self.args += pad_args#' '.join(
                                                    #[str(p) for p in map_pad])]
        #padding
        if cropped_dim is not None:
            
            crop_args = ['-cr']
            crop_args.extend(list(cropped_dim))
            if len(cropped_dim) > 0: self.args += crop_args
        #new origin
        if map_origin is not None:
            origin_args = ['-ori']
            origin_args.extend(map_origin)
            if len(map_origin) > 0: self.args += origin_args
        #mask file
        if mask_path is not None:
            self.args += ['-ma', mask_path]
        
        if out_map is not None:
            self.args += ['-out', out_map]
        
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)
