#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
model2map task.
'''

import os
import sys
from ccpem_core.ccpem_utils import ccpem_argparser
from textwrap import dedent
from ccpem_core.gui_ext import job_manager
from ccpem_core.gui_ext.job_manager import job_wrapper
from ccpem_core.tasks import task_utils
import refmac_window
from ccpem_core import ccpem_utils
from ccpem_core.ccpem_utils import ccpem_widgets

def refmac_parser():
    parser = ccpem_argparser.ccpemArgParser(
             fromfile_prefix_chars='@',
             usage=dedent('''
model2map.  Converts model to map using sfall.  N.B. requires CCP4.
             '''),
             description='model2map options',
             epilog=dedent('''
To import parameters from argument text file use e.g. :\
python class_selections.py @my_args.txt
             '''))
    #
    pdb_in = parser.add_argument_group()
    pdb_in.add_argument('-pdb_in',
        '--pdb_in',
        help='''
Input coordinate file (pdb format)
        ''',
        type=str,
        default=None)
    #
    map_out = parser.add_argument_group()
    refined_mtzout.add_argument('-map_out',
        '--map_out',
        help='''
Output map (mrc format)
        ''',
        type=str,
        default=None)
    return parser

class ProgramInfo(object):
    '''
    Store program information for database.
    '''
    def __init__(self, database_path=None):
        self.program_id = 'model2map_1'
        self.jobname = 'model2map'
        self.version = '1'
        self.description = '''
Converts model to map using sfall.  N.B. requires CCP4.
'''
        self.author = '???'
        self.module = os.path.realpath(__file__)
        self.database_path = database_path

def launch_gui(parent,
               database_path,
               args=None,
               job_manager=None):
    # Add program info to database
    if database_path is not None:
            program_info = ProgramInfo()
            task_utils.add_program_info_to_database(database_path,
                                                    program_info)
    window = refmac_window.Refmac5Window(
                       parent=parent,
                       args=args,
                       job_manager=job_manager)
    window.setGeometry(parent.geometry().x()+50,
                       parent.geometry().x()+50,
                       window.geometry().width(),
                       window.geometry().height())
    window.show()

class RunTask(object):
    '''
    Run task.
    '''
    def __init__(self,
                 job_id,
                 job_location,
                 database_path,
                 args=None,
                 parent=None):
        self.command = settings.which(program='refmac5')
        assert self.command is not None
        self.job_id = job_id
        self.job_location = job_location
        if self.job_location is not None:
            os.chdir(self.job_location)
        self.database_path = database_path
        self.args = args
        self.parent = parent
        if self.args is None:
            self.args = refmac_parser().generate_arguments()
        self.stdin_map2mtz = \
            os.path.join(self.job_location, 'stdin_map2mtz.txt')
        self.stdin_refine = \
            os.path.join(self.job_location, 'stdin_refine.txt')
        print self.stdin_refine
        # Save arguments
        self.args.output_args_as_json(os.path.join(self.job_location, 'args.json'))
        # Check arguments
        assert(self.validate_map2mtz_args())
        if self.args.mode.value in ['Refine', 'Refine and Validate']:
            assert(self.validate_refine_args())
        if self.args.mode.value in ['Validate', 'Refine and Validate']:
            assert(self.validate_shake_args())
            self.stdin_shake = \
                os.path.join(self.job_location, 'stdin_shake.txt')
            self.stdin_map2mtz_hm_1 = \
                os.path.join(self.job_location, 'stdin_map2mtz_hm_1.txt')
        # Setup inputs
        self.write_map2mtz_stdin()
        if self.args.mode.value in ['Refine', 'Refine and Validate']:
            self.write_refine_stdin()
        if self.args.mode.value in ['Validate', 'Refine and Validate']:
            self.write_shake_stdin()
            self.write_map2mtz_hm_1_stdin()
        # Run task process
        self.run_detached()

    def run_detached(self):
        '''
        Convert args to mrc stdin format, run as detached job.
        '''
        # Convert to map to mtz
        args_map2mtz = ['xyzin', self.args.input_pdb.value,
                        'mapin', self.args.input_map.value,
                        'xyzout', self.args.shifted_pdb.value]
        #
        self.stdout_map2mtz = \
            os.path.join(self.job_location, 'stdout.txt')
        self.stderr_map2mtz = \
            os.path.join(self.job_location, 'stderr_map2mtz.txt')
        self.json_file = \
            os.path.join(self.job_location, 'args_map2mtz.json')
        job_map2mtz = job_wrapper.SeriesJobInfo(command=self.command,
                                                qarg_list=args_map2mtz,
                                                set_environment=None,
                                                stdin_file=self.stdin_map2mtz,
                                                stdout_file=self.stdout_map2mtz,
                                                stderr_file=self.stderr_map2mtz,
                                                json_file=self.json_file)
        job_info_list = [job_map2mtz]
        #
        job_wrapper.launch_detached_process_series(
                job_info_list=job_info_list,
                job_id=self.job_id,
                job_location=self.job_location,
                database_path=self.database_path,
                relay_output=True,
                parent=None)

    def validate_map2mtz_args(self):
        args_correct = True
        warnings = ''
        if self.args.input_map.value == 'None':
            warnings += '\nInput map not defined'
            args_correct = False
        if not os.path.isfile(self.args.input_map.value):
            warnings += '\nInput map not found : {0}'.format(self.args.input_map.value)
            args_correct = False
        if self.args.input_pdb.value == 'None':
            warnings += '\nInput PDB not defined'
            args_correct = False
        if not os.path.isfile(self.args.input_pdb.value):
            warnings += '\nInput PDB not found : {0}'.format(self.args.input_pdb.value)
            args_correct = False
        if self.args.mask_radius.value < 0:
            warnings += '\nMask radius must be greater than 0.'
            args_correct = False
        if self.args.shifted_pdb.value == 'None':
            shifted = os.path.basename(self.args.input_pdb.value)
            if shifted[-4:] == '.pdb':
                shifted = shifted.replace('.pdb', '_shifted.pdb')
            else:
                shifted = self.args.input_pdb.value + '_shifted.pdb'
            self.args.shifted_pdb.value = os.path.join(self.job_location, shifted)
        if warnings is not '':
            if self.parent is not None:
                ccpem_widgets.CCPEMErrorDialog(parent = self.parent,
                                               text = warnings)
        return args_correct

    def validate_refine_args(self):
        args_correct = True
        warnings = ''
        if self.args.ncycles.value < 1:
            warnings += '\nNumber of cycles must be greater than 1'
            args_correct = False
        if self.args.refined_pdb.value == None:
            refined = os.path.basename(self.args.input_pdb.value)
            if refined[-4:] == '.pdb':
                refined = refined.replace('.pdb', '_refined.pdb')
            else:
                refined = self.args.input_pdb.value + '_refined.pdb'
            self.args.refined_pdb.value = os.path.join(self.job_location, refined)
        if self.args.refined_mtzout.value == None:
            refined_mtz = os.path.basename(self.args.input_pdb.value)
            if refined_mtz[-4:] == '.pdb':
                refined_mtz = refined_mtz.replace('.pdb', '_refined.mtz')
            else:
                refined_mtz = self.args.input_pdb.value + '_refined.mtz'
            self.args.refined_mtzout.value = os.path.join(self.job_location, refined_mtz)
        if self.args.map_resolution.value is None:
            warnings += '\nResolution not defined'
            args_correct = False
        if warnings is not '':
            print warnings
            if self.parent is not None:
                ccpem_widgets.CCPEMErrorDialog(parent = self.parent,
                                               text = warnings)
        return args_correct

    def validate_shake_args(self):
        args_correct = True
        warnings = ''
        if self.args.shake_pdb.value is None:
            if self.args.mode.value is 'Validate':
                self.args.shake_pdb.value = self.args.input_pdb.value
            if self.args.mode.value is 'Refine and Validate':
                self.args.shake_pdb.value = self.args.refined_pdb.value
        if self.args.shake_pdb.value is None:
            warnings += '\nInput PDB to shake not defined'
            args_correct = False
        if self.args.shaken_pdb.value is None:
            name = os.path.basename(self.args.input_pdb.value)
            if name[-4:] == '.pdb':
                name = name.replace('.pdb', '_shaken.pdb')
            else:
                name = self.args.input_pdb.value + '_shaken.pdb'
            self.args.shaken_pdb.value = os.path.join(self.job_location, name)
        if self.args.half_map_1.value is None:
            warnings += '\nHalf map 1 not defined'
            args_correct = False
        if self.args.half_map_2.value is None:
            warnings += '\nHalf map 2 not defined'
            args_correct = False
        if warnings is not '':
            print warnings
            if self.parent is not None:
                ccpem_widgets.CCPEMErrorDialog(parent=self.parent,
                                               text=warnings)
        return args_correct

    def write_map2mtz_stdin(self):
        '''
        Convert args to task arguments for map2mtz stdin .
        '''
        refmac_map2mtz_stdin = open(self.stdin_map2mtz, 'w')
        out = 'mode sfcalc'
        out += '\nsfcalc mrad {0}'.format(self.args.mask_radius.value)
        out += '\nsfcalc shift'
        
        out += '\n' + self.args.keywords.value
        out += '\nend'
        refmac_map2mtz_stdin.write(out)
        refmac_map2mtz_stdin.close()

    def write_refine_stdin(self):
        '''
        Convert args to task arguments for refine stdin.
        '''
        refmac_map2mtz_stdin = open(self.stdin_refine, 'w')
        out = 'labin FP=Fout0 PHIB=Pout0'
        out += '\nsolvent no'
        out += '\nREFI sharpen {0}'.format(self.args.map_sharpen.value)
        out += '\nBFACtor SET {0}'.format(self.args.set_bfactor.value)
        out += '\nncycles {0}'.format(self.args.ncycles.value)
        out += '\nweight auto {0}'.format(self.args.ncycles.value)
        out += '\nRESO {0}'.format(self.args.map_resolution.value)
        out += '\nridge dist sigma 0.02'
        out += '\n@shifts.txt'
        out += '\n' + self.args.keywords.value
        out += '\nend'
        refmac_map2mtz_stdin.write(out)
        refmac_map2mtz_stdin.close()

    def write_shake_stdin(self):
        '''
        Convert args to task arguments for shake stdin.
        '''
        refmac_shake_stdin = open(self.stdin_shake, 'w')
        out = '\nnoise {0}'.format(self.args.noise.value)
        out += '\nsolvent no'
        out += '\nend'
        refmac_shake_stdin.write(out)
        refmac_shake_stdin.close()

    def write_map2mtz_hm_1_stdin(self):
        '''
        Convert args to task arguments for shake stdin.
        '''
        refmac_stdin_map2mtz_hm_1 = open(self.stdin_map2mtz_hm_1, 'w')
        out = 'labin FP=Fout0 PHIB=Pout0'
        out += '\nweight auto 2'
        out += '\nRESO {0}'.format(self.args.map_resolution.value)
        out += '\nncycles 40'
        out += '\n@shifts.txt'
        out += '\nend'
        refmac_stdin_map2mtz_hm_1.write(out)
        refmac_stdin_map2mtz_hm_1.close()



def main(test=True):
    '''
    Run command line instance.  Does not create DB entry, output goes into
    subdirectory of cwd (subdirectory automatically assigned).
    '''
    ccpem_utils.print_header(message='Refmac5')
    
    if test:
        import shutil
        test_output = os.getcwd() + '/refmac5_job_1'
        if os.path.exists(test_output):
            shutil.rmtree(test_output)
        args = refmac_parser().generate_arguments()
        if len(sys.argv) == 1:
            assert 0
            args.output_args_as_json(filename='test_args.json')
        else:
            args.import_args_from_json(filename=sys.argv[1])
            print args.output_args_as_text()
            
        # Set output directory
        job_id = None
        job_location = ccpem_utils.check_directory_and_make(
                            os.getcwd() + '/refmac5_job_1',
                            verbose=False,
                            auto_suffix=True)
        database_path = None
        # Run job
        RunTask(job_id=job_id,
                job_location=job_location,
                database_path=database_path,
                args=args,
                parent=None)
        print '  Job location : ', job_location
        print '  Job will run in background.'
    
    else:
        if len(sys.argv) == 1:
            print '  Please supply arguments.'
        else:
            args = refmac_parser().generate_arguments()
            if sys.argv[1][-5:] == '.json':
                args_json = sys.argv[1]
                args.import_args_from_json(filename=args_json)
            print args.output_args_as_text()
            # Set output directory
            job_id = None
            job_location = ccpem_utils.check_directory_and_make(
                                os.getcwd() + '/refmac5_job_1',
                                verbose=False,
                                auto_suffix=True)
            database_path = None
            # Run job
            RunTask(job_id=job_id,
                    job_location=job_location,
                    database_path=database_path,
                    args=args,
                    parent=None)
            print '  Job location : ', job_location
            print '  Job will run in background.'
    ccpem_utils.print_footer()

if __name__ == '__main__':
    main()
