#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
MRC Mrc2tif task.
'''

import os
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import settings
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils


class Mrc2Tif(task_utils.CCPEMTask):
    '''
    Inherits from CCPEMTaskWindow.
    '''

    task_info = task_utils.CCPEMTaskInfo(
        name='MRC2tif',
        author='Crowther RA, Henderson R, Smith JM',
        version='0.1',
        description=('Convert images from mrc to tif format'),
        short_description=(
            'Convert images from mrc to tif format'),
        references=None)

    commands = {'mrc_mrc2tif': settings.which(program='mrc_mrc2tif')}

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        super(Mrc2Tif, self).__init__(database_path=database_path,
                                      args=args,
                                      args_json=args_json,
                                      pipeline=pipeline,
                                      job_location=job_location,
                                      parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        mrc_in = parser.add_argument_group()
        mrc_in.add_argument(
            '-mrc_in',
            '--mrc_in',
            help=('Input MRC file for image conversion'),
            metavar='MRC input',
            type=str,
            default=None)
        #
        return parser

    def run_pipeline(self, run=True, job_id=None, db_inject=None):
        '''
        Launch pipeline using new job manager
        '''
        # Run task process
        tif_out = os.path.basename(self.args.mrc_in.value)
        tif_out = tif_out.replace('.map', '.tif')
        tif_out = tif_out.replace('.mrc', '.tif')
        self.tif_out_path = os.path.join(self.job_location,
                                         tif_out)
        stdin = self.write_stdin()
        #
        self.mrcallspacea_job = process_manager.CCPEMProcess(
            name='MRC to TIF',
            command=self.commands['mrc_mrc2tif'],
            location=self.job_location,
            stdin=stdin)
        if run:
            self.pipeline = process_manager.CCPEMPipeline(
                job_id=job_id,
                pipeline=self.mrcallspacea_job,
                args_path=self.args.jsonfile,
                db_inject=db_inject,
                database_path=self.database_path,
                taskname=self.task_info.name,
                title=self.args.job_title.value,
                location=self.job_location)
            self.pipeline.start()

    def validate_args(self):
        '''
        Check arguments before running job.
        '''
        args_correct = True
        warnings = 'Error:'
        if not os.path.exists(self.args.mrc_in.value):
            warnings += '\nDatafile not found'
            args_correct = False
        self.args.mrc_in.value = os.path.abspath(self.args.mrc_in.value)
        if not args_correct:
            if warnings is not '':
                print warnings
        return args_correct

    def write_stdin(self):
        '''
        Convert args to task arguments for stdin
        '''
        out = self.args.mrc_in.value + '\n'
        out += self.tif_out_path + '\n'
        out += 'eot'
        return out

