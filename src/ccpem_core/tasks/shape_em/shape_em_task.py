#
#     Copyright (C) 2018 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import shutil
import json
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.ccpem_utils.spherical_angles_distribution import angle_distn
from ccpem_core import settings

class ShapeEM(task_utils.CCPEMTask):
    '''
    CCPEM ShapeEM task.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='Shape-EM',
        author='Roseman, AM',
        version='2.0',
        description=(
            'TBC'),
        short_description=(
            'Quantitatively dock subvolumes into maps'),
        references=None)

    commands = {
        'mapfmap': settings.which('MapfilterDKM2'),
        'makedensity': settings.which(program='MakedensityV2'),
        'mapfpdb': settings.which(program='MapfilterDKM2'),
        'maskc': settings.which(program='MaskConvoluteDKM2'),
        'dockemlsv': settings.which(program='DockEM2-LocalSearchV2'),
        'dockempeak': settings.which(program='DockEM2XpeaksV2'),
        'dockemsoln': settings.which(program='DockXsolnV2'),
        'dockemlsv_merge': settings.which(
            program='DockEM2-LocalSearchV2_pll_merge'),
        'dockemlsv_pll': settings.which(
            program='DockEM2-LocalSearchV2_pll'),
        'dockem_roi': settings.which(
            program='DockEM2-LocalSearchV2-ROIcrop')
        }

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        #
        super(ShapeEM, self).__init__(
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        run_search = parser.add_argument_group()
        run_search.add_argument(
            '-run_search',
            '--run_search',
            help='Run full analysis',
            metavar='Run search',
            type=bool,
            default=False)
        #
        target_map = parser.add_argument_group()
        target_map.add_argument(
            '-target_map',
            '--target_map',
            help=('Target map used for rigid body search'),
            metavar='Target map',
            type=str,
            default=None)
        #
        centre_model_in_map = parser.add_argument_group()
        centre_model_in_map.add_argument(
            '-centre_model',
            '--centre_model',
            help=('Centre model in map'),
            metavar='Centre model',
            type=bool,
            default=False)
        #
        search_object = parser.add_argument_group()
        search_object.add_argument(
            '-search_object',
            '--search_object',
            help=('Search object to be docked inside the target map'),
            metavar='Search PDB',
            type=str,
            default=None)
        #
        map_resolution = parser.add_argument_group()
        map_resolution.add_argument(
            '-map_resolution',
            '--map_resolution',
            help=('Resolution of the target map (A)'),
            metavar='Map resolution',
            type=float,
            default=None)
        #
        high_pass_filter = parser.add_argument_group()
        high_pass_filter.add_argument(
            '-high_pass_filter',
            '--high_pass_filter',
            help=('Lowest Resolution cut off (A). This is used for map '
                  'filtering.'),
            metavar='High pass filter',
            type=float,
            default=100)
        #
        target_map_filtered = parser.add_argument_group()
        target_map_filtered.add_argument(
            '-target_map_filtered',
            '--target_map_filtered',
            help=('Target map filtered with low pass filter'),
            type=str,
            metavar='Target map filtered',
            default=None)
        #
        search_object_map = parser.add_argument_group()
        search_object_map.add_argument(
            '-search_object_map',
            '--search_object_map',
            help='Search object generated from input PDB',
            metavar='Search map',
            type=str,
            default=None)
        #
        search_object_filtered = parser.add_argument_group()
        search_object_filtered.add_argument(
            '-search_object_filtered',
            '--search_object_filtered',
            help=('Search object map filtered to the resolution of the target '
                  'map'),
            type=str,
            metavar='Search object filtered',
            default=None)
        #
        search_object_mask = parser.add_argument_group()
        search_object_mask.add_argument(
            '-search_object_mask',
            '--search_object_mask',
            help=('Search object masked with map convolute'),
            metavar='Search object mask',
            type=str,
            default=None)
        #
        theta_step = parser.add_argument_group()
        theta_step.add_argument(
            '-s',
            '--theta_step',
            help=('The value of theta step is between 0-180 and is used to '
                  'generate angular distribution on sphere.  Angles files are '
                  'in Euler angles convention, in spider document format.'),
            metavar='Theta sampling',
            type=float,
            default=10.0)
        #
        angl_sampling = parser.add_argument_group()
        angl_sampling.add_argument(
            '-angl_sampling',
            '--angl_sampling',
            help=('Angular sampling increment for the omega search (deg).'),
            type=float,
            metavar='Angular sampling',
            default=10.0)
        #
        angl_filename = parser.add_argument_group()
        angl_filename.add_argument(
            '-angl_filename',
            '--angl_filename',
            help=('Angular sampling filename.'),
            type=str,
            default='angles_file.spi')
        #
        grid_sampling = parser.add_argument_group()
        grid_sampling.add_argument(
            '-grid_sampling',
            '--grid_sampling',
            help=('Grid sampling by default is set to the voxel size of '
                  'the map file.'),
            metavar='Grid sampling',
            type=float,
            default=None)
        #
        mask_smooth = parser.add_argument_group()
        mask_smooth.add_argument(
            '-mask_smooth',
            '--mask_smooth',
            help='Radius of sphere to convolute (A)',
            type=float,
            metavar='Mask smoothening',
            default=5.0)
        #
        mask_threshold = parser.add_argument_group()
        mask_threshold.add_argument(
            '-mask_threshold',
            '--mask_threshold',
            help=('Isosurface that includes approximately the expected volume '
                  'of the domain'),
            metavar='Mask threshold',
            type=float,
            default=10.0)
        #
        box_extent = parser.add_argument_group()
        box_extent.add_argument(
            '-box',
            '--box_extent',
            help=('Extent of the search, default is half the box size (pixels) '
                  'to next power of 2'),
            metavar='Search extent',
            type=int,
            default=None)
        #
        num_hits = parser.add_argument_group()
        num_hits.add_argument(
            '-hits',
            '--num_hits',
            help=('Number of solutions required (1-10000)'),
            type=int,
            default=100)
        #
        peak_radius = parser.add_argument_group()
        peak_radius.add_argument(
            '-peak_radius',
            '--peak_radius',
            help=('Excludes neighbouring solutions within radius cutoff'),
            type=float,
            default=20.0)
        #
        number_solutions = parser.add_argument_group()
        number_solutions.add_argument(
            '-number_solutions',
            '--number_solutions',
            help='Number of PDB solutions to output',
            metavar='Number solutions',
            type=int,
            default=10)
        #
        job_prefix = parser.add_argument_group()
        job_prefix.add_argument(
            '-job_prefix',
            '--job_prefix',
            help='Prefix string for output files',
            metavar='File prefix',
            type=str,
            default=None)
        #
        n_proc = parser.add_argument_group()
        n_proc.add_argument(
            '-n_proc',
            '--n_proc',
            help='Number of processors to use for peak search',
            metavar='No. processors',
            type=int,
            default=None)
        return parser

    def validate_args(self):
        '''
        Add any args to be validated here...
        '''
        args_correct = True
        warnings = ''
        warnings += self.set_arg_absolute_path(
            arg=self.args.target_map)
        self.args.angl_filename.value = os.path.join(
            self.job_location,
            'angles_file.spi')
        # Set target map filtered
        tmf_filename = os.path.basename(self.args.target_map.value)
        tmf_filename = tmf_filename.split('.')[0]
        res_str = '{0:.2f}'.format(
            self.args.map_resolution.value).replace('.', '-')
        tmf_filename += '_ff_{0}.mrc'.format(res_str)
        target_map_filtered_path = os.path.join(
            self.job_location,
            tmf_filename)
        self.args.target_map_filtered.value = target_map_filtered_path
        if self.args.search_object_map.value is None:
            self.args.search_object_map.value = 'search_object.mrc'
        if self.args.search_object_filtered.value is None:
            self.args.search_object_filtered.value \
                = 'search_object_filtered.mrc'
        if self.args.search_object_mask.value is None:
            self.args.search_object_mask.value \
                = 'search_object_mask.mrc'
        for arg in [self.args.search_object_map,
                    self.args.search_object_mask,
                    self.args.search_object_filtered,
                    ]:
            arg.value = os.path.join(
                self.job_location,
                arg.value)

        # Reset n_proc dependant on number of items in angular sweep
        if self.args.n_proc() != 0:
            angles = angle_distn(self.args.theta_step(), write_output=False)
            n_proc = min(len(angles), self.args.n_proc())
            self.args.n_proc.value = n_proc

        # Display warnings in parent GUI
        if warnings != '':
            args_correct = False
            print warnings
        return args_correct

    def job_mapfmap(self):
        stdin_mapfmap = self.write_stdin_mapfmap()
        stdout_mapfmap = \
            os.path.join(self.job_location, 'stdout_mapfmap.txt')
        stderr_mapfmap = \
            os.path.join(self.job_location, 'stderr_mapfmap.txt')
        self.mapfmap_process = process_manager.CCPEMProcess(
            name='Map filter (map)',
            command=self.commands['mapfmap'],
            args=None,
            location=self.job_location,
            stdin=stdin_mapfmap,
            stdout=stdout_mapfmap,
            stderr=stderr_mapfmap)
        return self.mapfmap_process

    def job_make_density(self):
        stdin_make_density = self.write_stdin_make_density()
        stdout_make_denisty = \
            os.path.join(self.job_location, 'stdout_make_denisty.txt')
        stderr_make_denisty = \
            os.path.join(self.job_location, 'stderr_make_denisty.txt')
        self.make_density_process = process_manager.CCPEMProcess(
            name='Make density',
            command=self.commands['makedensity'],
            args=None,
            location=self.job_location,
            stdin=stdin_make_density,
            stdout=stdout_make_denisty,
            stderr=stderr_make_denisty)
        return self.make_density_process

    def job_centre_model_in_map(self, output_path):
        stdout = os.path.join(self.job_location,
                              'stdout_centre_model.txt')
        stderr = os.path.join(self.job_location,
                              'stderr_centre_model.txt')
        args = ['-c']
        script = '''
from ccpem_core.ccpem_utils import centre_model_in_map
centre_model_in_map.AlignMapModel(
    model_path="{0}",
    map_path="{1}",
    output_model_path="{2}")
'''.format(self.args.search_object(),
           self.args.target_map(),
           output_path)
        args.append(script)
        self.centre_model_process = process_manager.CCPEMProcess(
            name='Centre model in map',
            command='ccpem-python',
            args=args,
            location=self.job_location,
            stdout=stdout,
            stderr=stderr)
        return self.centre_model_process

    def job_mapfpdb(self):
        stdin_mapfpdb = self.write_stdin_mapfpdb()
        stdout_mapfpdb = \
            os.path.join(self.job_location, 'stdout_mapfpdb.txt')
        stderr_mapfpdb = \
            os.path.join(self.job_location, 'stderr_mapfpdb.txt')
        self.mapfpdb_process = process_manager.CCPEMProcess(
            name='Map filter (pdb)',
            command=self.commands['mapfpdb'],
            args=None,
            location=self.job_location,
            stdin=stdin_mapfpdb,
            stdout=stdout_mapfpdb,
            stderr=stderr_mapfpdb)
        return self.mapfpdb_process

    def job_mask_convolute(self):
        stdin_mask_convolute = self.write_stdin_mask_convolute()
        stdout_mask_convolute = \
            os.path.join(self.job_location, 'stdout_mask_convolute.txt')
        stderr_mask_convolute = \
            os.path.join(self.job_location, 'stderr_mask_convolute.txt')
        self.mask_convolute_process = process_manager.CCPEMProcess(
            name='Mask convolute',
            command=self.commands['maskc'],
            args=None,
            location=self.job_location,
            stdin=stdin_mask_convolute,
            stdout=stdout_mask_convolute,
            stderr=stderr_mask_convolute)
        return self.mask_convolute_process

    def job_dockemlsv(self, n_proc=1, proc=0):
        '''
        Run in single or multiprocess mode
        N.B. proc = individual process number starting at 1
        '''
        # Must give proc number if running in multi-process mode
        if n_proc > 1:
            assert proc > 0
            suffix = '{0}_dockemlsv.txt'.format(proc)
            process_name = '{0}_DockEM LSV'.format(proc)
        else:
            suffix = 'dockemlsv.txt'
            process_name = 'DockEM LSV'
        stdin_dockemlsv = self.write_stdin_dockemlsv(proc=proc)
        stdout_dockemlsv = \
            os.path.join(self.job_location,
                         'stdout_' + suffix)
        stderr_dockemlsv = \
            os.path.join(self.job_location,
                         'stderr_' + suffix)
        if n_proc == 1:
            command = self.commands['dockemlsv']
        else:
            command = self.commands['dockemlsv_pll']
        self.dockemlsv_process = process_manager.CCPEMProcess(
            name=process_name,
            command=command,
            args=None,
            location=self.job_location,
            stdin=stdin_dockemlsv,
            stdout=stdout_dockemlsv,
            stderr=stderr_dockemlsv)
        return self.dockemlsv_process

    def job_dockem_lsv_merge(self):
        '''
        Merge parallel jobs together if n_proc > 1
        '''
        assert os.path.exists(self.commands['dockemlsv_merge'])
        stdin_lsv_merge = self.write_stdin_lsv_merge()
        stdout_lsv_merge = \
            os.path.join(self.job_location, 'stdout_lsv_merge.txt')
        stderr_lsv_merge = \
            os.path.join(self.job_location, 'stderr_lsv_merge.txt')
        self.dockem_lsv_merge_process = process_manager.CCPEMProcess(
            name='DockEM LSV merge',
            command=self.commands['dockemlsv_merge'],
            args=None,
            location=self.job_location,
            stdin=stdin_lsv_merge,
            stdout=stdout_lsv_merge,
            stderr=stderr_lsv_merge)
        return self.dockem_lsv_merge_process

    def job_dockempeak(self):
        stdin_dockempeak = self.write_stdin_dockempeak()
        stdout_dockempeak = \
            os.path.join(self.job_location, 'stdout_dockempeak.txt')
        stderr_dockempeak = \
            os.path.join(self.job_location, 'stderr_dockempeak.txt')
        self.dockempeak_process = process_manager.CCPEMProcess(
            name='DockEM peak',
            command=self.commands['dockempeak'],
            args=None,
            location=self.job_location,
            stdin=stdin_dockempeak,
            stdout=stdout_dockempeak,
            stderr=stderr_dockempeak)
        return self.dockempeak_process

    def job_dockemsoln(self):
        stdin_dockemsoln = self.write_stdin_dockemsoln()
        stdout_dockemsoln = \
            os.path.join(self.job_location, 'stdout_dockemsoln.txt')
        stderr_dockemsoln = \
            os.path.join(self.job_location, 'stderr_dockemsoln.txt')
        self.dockemsoln_process = process_manager.CCPEMProcess(
            name='DockEM soln',
            command=self.commands['dockemsoln'],
            args=None,
            location=self.job_location,
            stdin=stdin_dockemsoln,
            stdout=stdout_dockemsoln,
            stderr=stderr_dockemsoln)
        return self.dockemsoln_process

    def job_roi(self):
        '''
        Return region of interest process
        '''
        stdin = self.stdin_roi()
        suffix = 'dock_em_roi.txt'
        stdout = os.path.join(self.job_location,
                              'stdout_' + suffix)
        stderr =  os.path.join(self.job_location,
                               'stderr_' + suffix)
        self.dockemsoln_process = process_manager.CCPEMProcess(
            name='DockEM Region of interest',
            command=self.commands['dockem_roi'],
            args=None,
            location=self.job_location,
            stdin=stdin,
            stdout=stdout,
            stderr=stderr)
        # Output
        self.roi_crop_map = os.path.join(
            self.job_location,
            'local_cropmap{0}.mrc'.format(self.job_str))
        return self.dockemsoln_process

    def stdin_roi(self):
        '''
        Return stdin string for ROI process
        '''
        # map file name
        out = self.args.target_map.value + '\n'
        # grid sampling
        out += str(self.args.grid_sampling.value) + '\n'
        # search object map
        out += self.args.search_object_map.value + '\n'
        # search object mask
        out += str(self.args.search_object_mask.value) + '\n'
        # mask threshold
        out += str(self.args.mask_threshold.value) + '\n'
        # angles file (spider)
        out += self.args.angl_filename.value + '\n'
        # Omega angular increment
        out += str(self.args.angl_sampling.value) + '\n'
        # run code (i.e. job directory)
        out += self.job_str + '\n'
        # search object pdb
        out += self.args.search_object.value + '\n'
        # size of local area, border
        out += str(self.args.box_extent.value) + ',0' + '\n'
        return out

    def run_pipeline(self, job_id=None, run=True, db_inject=None):
        '''
        Run pipeline to prepare input files.
        '''
        # If re-running prep phase remove directory...
        remove_data = False
        try:
            if not self.args.run_search() and self.job_location is not None:
                path = os.path.join(self.job_location,
                                    'task.ccpem')
                if os.path.exists(path):
                    status = json.load(open(path))['status']
                    print 'Status : ', status
                    if status != 'ready':
                        remove_data = True
            if remove_data:
                shutil.rmtree(self.job_location)
        except OSError:
            ccpem_utils.print_warning(message='DockEM: error removing old data')

        # Set Job number based on job location
        path = self.job_location
        path = path.replace('_', ' ').split()
        try:
            job_num = int(path[-1])
        except ValueError:
            job_num = 1
        if job_num < 10:
            self.job_str = '00{0}'.format(job_num)
        elif job_num < 100:
            self.job_str = '0{0}'.format(job_num)
        else:
            self.job_str = '{0}'.format(job_num)
            if job_num > 999:
                self.job_str = self.job_str[-3:]
        self.args.job_prefix.value = self.job_str
        # Save args to include prefix
        self.args.output_args_as_json(os.path.join(self.job_location,
                                                   'args.json'))

        # Set pipeline
        pl = []
        # Generate processes
        # ## Part A - Generate inputs
        # 1) mapfmap
        pl.append([self.job_mapfmap()])
        # 2) Make density
        # Optional centre maps
        if self.args.centre_model():
            output_path = os.path.join(self.job_location,
                                       'centred.pdb')
            pl.append([self.job_centre_model_in_map(output_path=output_path)])
            self.args.search_object.value = output_path
        pl.append([self.job_make_density()])
        # 3) Map filer - mapfpdb
        pl.append([self.job_mapfpdb()])
        # 4) Mask convolute search object
        pl.append([self.job_mask_convolute()])
        # ## Part B - Run DockEM search
        # 5) Generate angular sampling file
        angle_distn(theta_step=self.args.theta_step.value,
                    output_path=self.job_location)
        # 6) DockEM Region of interest
        job_roi = self.job_roi()
        pl.append([job_roi])

        if self.args.run_search() :
            # Para. implementation
            # DockEMLSV only
            # Run in root job directory, n_proc value added to log file
            jobs_lsv = []
            # Run local search in single process mode
            if self.args.n_proc.value == 1:
                job_lsv = self.job_dockemlsv()
                jobs_lsv.append(job_lsv)
            # Run local search in parallel multi-process mode
            else:
                for n in xrange(self.args.n_proc.value):
                    job_lsv = self.job_dockemlsv(n_proc=self.args.n_proc.value,
                                                 proc=n+1)
                    jobs_lsv.append(job_lsv)
            # 7) DockEM LSV
            # Merge parallel jobs if required
            pl.append(jobs_lsv)
            if self.args.n_proc.value > 1:
                job_lsv_merge = self.job_dockem_lsv_merge()
                pl.append([job_lsv_merge])

            # Just run peak and soln if in analysis only mode
            job_peak = self.job_dockempeak()
            job_soln = self.job_dockemsoln()
            # 8) DockEM Peak
            pl.append([job_peak])
            # 9) DockEM Soln
            pl.append([job_soln])

        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)

        if run:
            self.pipeline.start()

    def write_stdin_mapfmap(self):
        '''
        Convert args to task arguments for stdin for mapfilter for map
        '''
        # High resolution is by default set to target map resolution
        # MapFilter stdin for target map
        out = self.args.target_map.value + '\n'
        out += str(self.args.grid_sampling.value) + '\n'
        out += str(self.args.map_resolution.value) + ',' + str(self.args.high_pass_filter.value) + '\n'
        out += self.args.target_map_filtered.value + '\n'
        return out

    def write_stdin_make_density(self):
        '''
        Make density stdin
        '''
        out = self.args.search_object.value + '\n'
        out += self.args.target_map.value + '\n'
        out += self.args.search_object_map.value + '\n'
        out += str(self.args.grid_sampling.value) + '\n'
        out += '0\n'
        return out

    def write_stdin_mapfpdb(self):
        '''
        MapFilter stdin for search object
        '''
        out = self.args.search_object_map.value + '\n'
        out += str(self.args.grid_sampling.value) + '\n'
        out += str(self.args.map_resolution.value) + ',' + str(self.args.high_pass_filter.value) + '\n'
        out += self.args.search_object_filtered.value + '\n'
        return out

    def write_stdin_mask_convolute(self):
        '''
        MaskConvolute stdin for search object
        '''
        out = self.args.search_object_map.value + '\n'
        out += str(self.args.grid_sampling.value) + '\n'
        out += str(self.args.mask_smooth.value) + '\n'
        out += self.args.search_object_mask.value + '\n'
        return out

    def write_stdin_dockemlsv(self, proc=0):
        '''
        DockEM SLV stdin
        '''
        out = self.args.target_map_filtered.value + '\n'
        out += str(self.args.grid_sampling.value) + '\n'
        out += self.args.search_object_filtered.value + '\n'
        out += self.args.search_object_mask.value + '\n'
        out += str(self.args.mask_threshold.value) + '\n'
        out += self.args.angl_filename.value + '\n'
        out += str(self.args.angl_sampling.value) + '\n'
        out += self.job_str + '\n'
        out += self.args.search_object.value + '\n'
        out += str(self.args.box_extent.value) + ',0\n'
        #
        if proc > 0:
            out += str(proc) + ',' + str(self.args.n_proc.value)
        return out

    def write_stdin_lsv_merge(self):
        '''
        LSV merge stdin for merging parallel local search
        '''
        # Search object
        out = self.args.search_object_map.value + '\n'
        # Angles file
        out += self.args.angl_filename.value + '\n'
        # XXX remove increment once re-compiled new bin
        #  Angle sampling increment
        out += str(self.args.angl_sampling.value) + '\n'
        # Run code
        out += self.job_str + '\n'
        # Box extent range
        out += str(self.args.box_extent.value) + ',0' + '\n'
        # Number of processes
        out +=  str(self.args.n_proc.value)
        return out

    def write_stdin_dockempeak(self):
        '''
        DockEM peak stdin
        '''
        out = str(self.args.grid_sampling.value) + '\n'
        out += str(self.args.num_hits.value) + '\n'
        out += self.args.job_prefix.value + '\n'
        out += '"' + self.args.search_object.value + '"' + '\n' #quotes needed
        out += str(self.args.peak_radius.value)
        return out

    def write_stdin_dockemsoln(self):
        '''
        DockEM soln stdin
        '''
        out = self.args.search_object.value + '\n'
        out += self.args.job_prefix.value + '\n'
        out += str(self.args.grid_sampling.value) + '\n'
        out += '1,' + str(self.args.number_solutions.value) + '\n'
        out += self.args.search_object.value + '\n'
        return out
