#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Opens map and solutions of DockEM (passed as a string) in Chimera
'''

from sys import argv
from chimera import runCommand as rc
import VolumeViewer as VV

# Sets the transparency and level for mask file
rc('open %s' % (argv[1]))
v = VV.active_volume()
stdev = v.data.full_matrix().std()
rc('volume #0 level %f transparency 0.5' % (stdev))

# Sets the transparency and level for filtered target map
rc('open %s' % (argv[2]))
v = VV.active_volume()
stdev = v.data.full_matrix().std()
rc('volume #1 level %f transparency 0.5' % (stdev))

# Sets the transparency and level for filtered search object map
rc('open %s' % (argv[3]))
v = VV.active_volume()
stdev = v.data.full_matrix().std()
rc('volume #2 level %f transparency 0.5' % (stdev))
