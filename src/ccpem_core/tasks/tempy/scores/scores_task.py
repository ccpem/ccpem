#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
import json
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.tasks.tempy.scores import scores_process
from ccpem_core.tasks.refmac import refmac_task
from ccpem_core.tasks.bin_wrappers import fft
from ccpem_core.process_manager import job_register
from ccpem_core.tasks.ribfind import ribfind_task
from ccpem_core.map_tools.TEMPy import map_preprocess
from ccpem_core.model_tools.gemmi_utils import shift_coordinates, remove_atomic_charges
from ccpem_core import settings
import mrcfile, shutil
from ccpem_core.tasks.model2map.model2map_task import sf2mapWrapper, fix_map_model_refmac

class GlobScore(task_utils.CCPEMTask):
    '''
    CCPEM / TEMPy global fit scores wrapper.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='TEMPy GlobScore',
        author='A. Joseph, M.Topf, M. Winn',
        version='1.1',
        description=(
            '''Global scoring using TEMPy library.<br>
            Input a map and one or more atomic models.<br>
            Multiple scores are calculated to evaluate atomic model
            fit. The 'local_*' scores are calculated on the voxels 
            in the region of overlap between contoured map and model.
            These scores are combined with the extent of overlap 
            between the map and model to estimate combined scores 
            (ccc_ov,mi_ov).<br>
            Please see the references for the details of the 
            scores:<br>
            <a href=https://www.ncbi.nlm.nih.gov/pubmed/28552721> 
            Joseph et al. 2017</a><br>
            <a href="https://www.ncbi.nlm.nih.gov/pubmed/26306092">
            Farabella et al. 2015</a>
            '''),
        short_description=(
            '[BETA TEST] Multiple global scores to evaluate atomic model'
            ' fit'),
        documentation_link='http://topf-group.ismb.lon.ac.uk/TEMPY.html',
        references=('https://www.ncbi.nlm.nih.gov/pubmed/28552721',
                    'https://www.ncbi.nlm.nih.gov/pubmed/26306092'))

    commands = {'ccpem-python':
        ['ccpem-python', os.path.realpath(scores_process.__file__)],
        'refmac': settings.which(program='refmac5'),
        'pdbset':  settings.which(program='pdbset'),
        'ccpem-gemmi':settings.which(program='gemmi'),
        'ccpem-mapprocess':
        ['ccpem-python', os.path.realpath(map_preprocess.__file__)]}


    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        #
        super(GlobScore, self).__init__(
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        map_path = parser.add_argument_group()
        map_path.add_argument(
            '-map_path',
            '--map_path',
            help='Input map (mrc format)',
            metavar='Input map',
            type=str,
            default=None)
        
        map_path.add_argument(
            '-map_path_edit',
            '--map_path_edit',
            help='Edited map (mrc format)',
            metavar='Edited input map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-input_pdbs',
            '--input_pdbs',
            help='One or more models (PDB/CIF) to score against map',
            metavar='Input atomic models',
            type=str,
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-input_pdb_chains',
            '--input_pdb_chains',
            help='Input atomic model chain(s)',
            metavar='Input atomic model chain selections',
            type=str,
            nargs='*',
            default=None)
        
        parser.add_argument(
            '-sim_maps',
            '--sim_maps',
            help='Synthetic maps from models, to score against map',
            metavar='Input synthetic maps',
            type=str,
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-use_refmac',
            '--use_refmac',
            help=('Use Refmac to make map from model. Ensure atomic B-factors are refined.'),
            metavar='Use Refmac to simulate map from model?',
            type=bool,
            default=False)
        #ligand dictionary for Refmac
        parser.add_argument(
            '-lib_in',
            '--lib_in',
            help='Ligand dictionary for Refmac (cif format)',
            metavar='Ligand dictionary',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-map_resolution',
            '--map_resolution',
            help=('Resolution of input map (Angstrom)'),
            metavar='Map resolution',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-auto_contour_level',
            '--auto_contour_level',
            help='''Automatically set map contour level. 
                    Unselect to set manually (recommended for masked/segmented maps).''',
            metavar='Auto set contour',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-map_contour_level',
            '--map_contour_level',
            help='''Set map contour level''',
            metavar='Contour level',
            type=float,
            default=None)

        #
        return parser

    def edit_argsfile(self,args_file):
        with open(args_file,'r') as f:
            json_args = json.load(f)
            #set mapprocess task output
            if self.mapprocess_task is not None:
                mapprocess_job_location = self.mapprocess_task.job_location
                #the processed map is written to the same dir as input map
                #the job location has other output files
                if mapprocess_job_location is not None:
                    processed_map_path = os.path.splitext(
                                        os.path.abspath(self.args.map_path.value))[0] \
                                        +'_processed.mrc'
                    #print processed_map_path
                    if os.path.isfile(processed_map_path):
                        json_args['map_path'] = processed_map_path
                        self.unprocessed_map = self.args.map_path.value
                        self.args.map_path.value = processed_map_path
            #add synthetic maps as an argument
            if self.args.use_refmac.value:
                json_args['map_path_edit'] = self.args.map_edit_path
                if len(self.args.sim_maps) == len(self.args.pdb_path_list):
                    json_args['sim_maps'] = self.args.sim_maps
                
        with open(args_file,'w') as f:
            json.dump(json_args,f)

    def check_processed_map(self):
        processed_map_path = os.path.splitext(
                            os.path.abspath(self.args.map_path.value))[0] \
                            +'_processed.mrc'
        #print processed_map_path
        if os.path.isfile(processed_map_path) and \
            self.mapprocess_task is not None:
            self.map_input.value_line.setText(processed_map_path)
            
    def fix_model_for_refmac(self,pdb_path,pdb_edit_path,trans_vector,
                             remove_charges=False):
        shift_coordinates(pdb_path,pdb_edit_path,trans_vector,remove_charges)

    def fix_map_model_refmac(self,mapfile):
        ori_x = ori_y = ori_z = 0.
        fix_map_model = True
        with mrcfile.open(mapfile, mode='r',permissive=True) as mrc:
            map_nx = mrc.header.nx
            map_ny = mrc.header.ny
            map_nz = mrc.header.nz
            map_nxstart = mrc.header.nxstart
            map_nystart = mrc.header.nystart
            map_nzstart = mrc.header.nzstart
            map_mapc = mrc.header.mapc
            map_mapr = mrc.header.mapr
            map_maps = mrc.header.maps
            ori_x = mrc.header.origin.x
            ori_y = mrc.header.origin.y
            ori_z = mrc.header.origin.z
            cella = (mrc.header.cella.x,mrc.header.cella.y,
                     mrc.header.cella.z)
            apix = mrc.voxel_size.item()
            self.map_dim = (map_nx,map_ny,map_nz)
        #fix map and model when nstart is non-zero
        if ori_x == 0. and ori_y == 0. and ori_z == 0.:
            if map_nxstart != 0 or map_nystart != 0 or map_nzstart != 0:
                fix_map_model = False
                #set nstart to 0 and fix origin (for Refmac5)
                map_edit = os.path.splitext(
                            os.path.basename(mapfile))[0]+'_fix.mrc'
                self.args.map_edit_path = os.path.join(
                                        self.job_location,map_edit)
                shutil.copyfile(mapfile,self.args.map_edit_path)
                try: assert os.path.isfile(self.args.map_edit_path)
                except AssertionError:
                    return fix_map_model, (ori_x,ori_y,ori_z), (map_nx,map_ny,map_nz)
                #edit float origin
                with mrcfile.open(self.args.map_edit_path,'r+') as mrc:
                    mrc.header.origin.x = map_nxstart*apix[0]
                    mrc.header.origin.y = map_nystart*apix[1]
                    mrc.header.origin.z = map_nzstart*apix[2]
                    
                ori_x = 0.0#map_nxstart*apix[0]
                ori_y = 0.0#map_nystart*apix[1]
                ori_z = 0.0#map_nzstart*apix[2]
                
        return fix_map_model, (ori_x,ori_y,ori_z), (map_nx,map_ny,map_nz)


    def run_pipeline(self, job_id=None, db_inject=None):
        
        self.mapprocess_task = None
        pl = []
        # Set args
        args_file = os.path.join(self.job_location,
                                 'args.json')
        self.args.sim_maps = []
        self.args.map_edit_path = self.args.map_path.value
        fix_map_model = True
        self.args.map_edit_path, fix_map_model, self.cella, self.origin_vector, self.map_dim, self.nstart_ori = fix_map_model_refmac(
                                    self.args.map_path.value,self.args.map_edit_path,
                                    self.job_location)
        #use Refmac for model map conversion
        if self.args.use_refmac.value:
            ori_x = self.origin_vector[0]
            ori_y = self.origin_vector[1]
            ori_z = self.origin_vector[2]
            map_nx, map_ny,map_nz = self.map_dim
                    
            self.args.pdb_path_list = self.args.input_pdbs.value
            if not isinstance(self.args.pdb_path_list, list):
                self.args.pdb_path_list = [self.args.pdb_path_list]
            for pdb_path in self.args.pdb_path_list:
                pdb_edit_path = pdb_path
                model_map_filename = os.path.splitext(
                        os.path.basename(pdb_path))[0]
                reference_map = os.path.join(
                    self.job_location,
                    model_map_filename+'_refmac.mrc')
                if len(model_map_filename) > 8:
                    modelid = model_map_filename[:4]+'_'+model_map_filename[-4:]
                else:
                    modelid = model_map_filename
                #translate model to nstart 0
                if fix_map_model and (self.origin_vector[0] != 0. or self.origin_vector[1] != 0. \
                        or self.origin_vector[2] != 0.):
                    pdb_edit_path = os.path.join(self.job_location,
                                        os.path.splitext(os.path.basename(
                                            pdb_path))[0]+'_fix.pdb')
                    trans_vector = (-self.origin_vector[0],
                                -self.origin_vector[1],
                                -self.origin_vector[2])
                    self.fix_model_for_refmac(pdb_path,pdb_edit_path,trans_vector,
                                              remove_charges=True)
                else:
                    #remove charges
                    remove_atomic_charges(pdb_path,
                                      pdb_edit_path)
                # Set PDB cryst from mtz
                self.process_set_unit_cell = refmac_task.SetModelCell(
                    job_location=self.job_location,
                    name='Set model unit cell '+modelid,
                    pdb_path=pdb_edit_path,
                    map_path=self.args.map_edit_path)
                pl.append([self.process_set_unit_cell.process])
    
                self.pdb_set_path = os.path.join(
                    self.job_location,
                    'pdbset.pdb')
                # Calculate mtz from pdb 
                self.refmac_sfcalc_crd_process = refmac_task.RefmacSfcalcCrd(
                    job_location=self.job_location,
                    pdb_path=self.pdb_set_path,
                    #name='RefmacSfcalcCrd '+modelid,
                    lib_in=self.args.lib_in(),
                    resolution=self.args.map_resolution.value)
                self.refmac_sfcalc_mtz = os.path.join(
                    self.job_location,
                    'sfcalc_from_crd.mtz')
                pl.append([self.refmac_sfcalc_crd_process.process])
    
#                 # Convert calc mtz to map
#                 self.fft_process = fft.FFT(
#                     job_location=self.job_location,
#                     map_nx=map_nx,
#                     map_ny=map_ny,
#                     map_nz=map_nz,
#                     # For now only use x,y,z
#     #                 fast=mrc_axis[int(map_mapc)],
#     #                 medium=mrc_axis[int(map_mapr)],
#     #                 slow=mrc_axis[int(map_maps)],
#                     fast='X',
#                     medium='Y',
#                     slow='Z',
#                     mtz_path=self.refmac_sfcalc_mtz,
#                     #name='FFT '+modelid,
#                     map_path=reference_map
#                     )
#                 pl.append([self.fft_process.process])
                
                #gemmi sf2map
                self.sf2map_process = sf2mapWrapper(
                    job_location=self.job_location,
                    command=self.commands['ccpem-gemmi'],
                    map_nx=map_nx,
                    map_ny=map_ny,
                    map_nz=map_nz,
                    # For now only use x,y,z
        #                 fast=mrc_axis[int(map_mapc)],
        #                 medium=mrc_axis[int(map_mapr)],
        #                 slow=mrc_axis[int(map_maps)],
                    fast='X',
                    medium='Y',
                    slow='Z',
                    mtz_path=self.refmac_sfcalc_mtz,
                    map_path=reference_map)
                pl.append([self.sf2map_process.process])
                
                #set origin and remove background
                reference_map_processed = os.path.join(
                        self.job_location,
                        model_map_filename+'_syn.mrc')
                if not all(o == 0. for o in (ori_x,ori_y,ori_z)):
                    self.mapprocess_wrapper = mapprocess_wrapper(
                                    job_location=self.job_location,
                                    command=self.commands['ccpem-mapprocess'],
                                    map_path=reference_map,
                                    list_process=['shift_origin','threshold'],
                                    map_origin=(ori_x,ori_y,ori_z),
                                    map_contour=0.09,
                                    #map_resolution=self.args.map_resolution.value,
                                    out_map=reference_map_processed,
                                    name='MapProcess '+modelid
                                    )
                else:
                    self.mapprocess_wrapper = mapprocess_wrapper(
                                    job_location=self.job_location,
                                    command=self.commands['ccpem-mapprocess'],
                                    map_path=reference_map,
                                    list_process=['threshold'],
                                    map_origin=(ori_x,ori_y,ori_z),
                                    map_contour=0.09,
                                    #map_resolution=self.args.map_resolution.value,
                                    out_map=reference_map_processed,
                                    name='MapProcess '+modelid
                                    )
                pl.append([self.mapprocess_wrapper.process])
                self.args.sim_maps.append(reference_map_processed)
        
        #check for mapprocess output
        self.check_processed_map()
        self.edit_argsfile(args_file)
        # Generate process
        self.score_wrapper = GlobScoreWrapper(
            command=self.commands['ccpem-python'],
            job_location=self.job_location,
            name='TEMPy scores')

        pl.append([self.score_wrapper.process])
        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

class GlobScoreWrapper(object):
    '''
    Wrapper for TEMPy GlobScore process.
    '''
    def __init__(self,
                 command,
                 job_location,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__

        # Set args
        self.args = os.path.join(self.job_location,
                                 'args.json')
        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)

class mapprocess_wrapper():
    '''
    Wrapper for TEMPy Map Processing tool.
    '''
    def __init__(self,
                 job_location,
                 command,
                 map_path,
                 list_process,
                 map_resolution=None,
                 map_contour=None,
                 map_apix=None,
                 map_pad=None,
                 map_origin=None,
                 mask_path=None,
                 out_map=None,
                 name='MapProcess'):
        assert map_path is not None
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.map_path = ccpem_utils.get_path_abs(map_path)
        # Set args
        self.args = ['-m', self.map_path]
        if list_process is not None:
            list_process_args = ['-l']
            list_process_args.extend(list_process)
            self.args += list_process_args
        if map_resolution is not None:
            self.args += ['-r', map_resolution]
        #threshold
        if map_contour is not None:
            self.args += ['-t', map_contour]
        #new apix
        if map_apix is not None:
            self.args += ['-p', map_apix]
        #padding
        if map_pad is not None:
            pad_args = ['-pad']
            pad_args.extend(list(map_pad))
            if len(map_pad) > 0: self.args += pad_args#' '.join(
                                                    #[str(p) for p in map_pad])]
        #new origin
        if map_origin is not None:
            origin_args = ['-ori']
            origin_args.extend(map_origin)
            if len(map_origin) > 0: self.args += origin_args
        #mask file
        if mask_path is not None:
            self.args += ['-ma', mask_path]
        
        if out_map is not None:
            self.args += ['-out', out_map]
        
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)
