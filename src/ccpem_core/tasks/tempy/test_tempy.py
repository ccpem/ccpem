
#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import unittest
from ccpem_core.TEMPy import MapParser
from ccpem_core import test_data

class Test(unittest.TestCase):
    '''
    Test TEMPy and give examples of TEMPy functionality
    '''
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_get_centre_of_mass(self):
        path = os.path.join(test_data.get_test_data_path(),
            'map/mrc/1ake_4-5A.mrc')
        assert os.path.exists(path)
        em_map = MapParser.MapParser.readMRC(path)
        contour = 1.0
        com_map = em_map._get_com_threshold(contour)
        self.assertAlmostEqual(com_map[0], 24.498, places=2)

if __name__ == '__main__':
    unittest.main()
