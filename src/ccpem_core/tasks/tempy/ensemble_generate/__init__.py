'''
see Generate_Ensemble_of_Fits.py

Make stand alone GUI to call this

-> Inputs
    -> PDB file
    -> Angular sweep or random
    -> translation
    -> rotation angle
    -> centre of axis
        -> Set default of COM
    -> Add atom selection
        -> run CG minimization for each new conformer?
'''