#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
import shutil
import pyrvapi
from ccpem_core import ccpem_utils


class SCCCResultsViewer(object):
    def __init__(self, sccc_dataframe, directory):
        self.directory = os.path.join(directory,
                                      'rvapi_data')
        if os.path.exists(self.directory):
            shutil.rmtree(self.directory)
        ccpem_utils.check_directory_and_make(self.directory)

        # Metadata contained in pandas dataframe
        self.dataframe = sccc_dataframe

        # Set results
        self.index = None
        self.set_pyrvapi_page()
        self.set_results()

    def set_results(self):
        self.set_results_summary()
        self.set_results_graphs()

    def set_pyrvapi_page(self):
        # Setup index.html
        self.index = os.path.join(self.directory, 'index.html')
        # Setup share jsrview
        ccp4 = os.environ['CCPEM']
        share_jsrview = os.path.join(ccp4, 'share', 'jsrview')
        # Setup pages
        pyrvapi.rvapi_init_document('TestRun', self.directory,
                                    'RVAPI Demo 1', 1, 4, share_jsrview,
                                    None, None, None, None)
        pyrvapi.rvapi_flush()

    def set_results_summary(self):
        # Setup refine_results (summary, graphs and output files)
        pyrvapi.rvapi_add_header('SCCC results')
        pyrvapi.rvapi_add_tab('tab2', 'SCCC Scores', True)
        pyrvapi.rvapi_add_section('sec1', 'Results', 'tab2', 0, 0, 1, 1,
                                  True)
        pyrvapi.rvapi_add_text(
            'Local fragment Score based on Manders\' Overlap Coefficient (SCCC)',
            'sec1', 0, 0, 1, 1)

        # See flex-em results for example of setting a datatable here.
        pyrvapi.rvapi_flush()

    def set_results_graphs(self):
        # Refinement statistics vs cycle
        pyrvapi.rvapi_append_loggraph1('sec1/graphWidget1')
        # Add graph data
        pyrvapi.rvapi_add_graph_data1('graphWidget1/data1',
                                      'Scores')

        # Set plot
        x_label = 'Residue'
        y_label = 'SCCC'
        pyrvapi.rvapi_add_graph_plot1(
            'graphWidget1/plot',
             y_label,
             x_label,
             y_label)

        # Get data
        for pdb in self.SCCC_.columns.levels[0]:
            pyrvapi.rvapi_add_graph_dataset1(
                'graphWidget1/data1/x'+pdb,
                'x',
                x_label)
            pyrvapi.rvapi_add_graph_dataset1(
                'graphWidget1/data1/y1'+pdb,
                pdb,
                pdb)

            pdb_df = self.dataframe[pdb].dropna(axis=0, how='any')

            for index, row in pdb_df.iterrows():
                pyrvapi.rvapi_add_graph_int1(
                    'graphWidget1/data1/x'+pdb,
                    int(row['residue']))
                pyrvapi.rvapi_add_graph_real1(
                    'graphWidget1/data1/y1'+pdb,
                    float(row['sccc']),
                    '%g')

            pyrvapi.rvapi_add_plot_line1('graphWidget1/data1/plot',
                                         'x'+pdb,
                                         'y1'+pdb)
#         pyrvapi.rvapi_set_plot_xmin('plot1', 'graphWidget1', 0.0)
#         pyrvapi.rvapi_set_plot_ymin('plot1', 'graphWidget1', 0.0)
        #
        pyrvapi.rvapi_flush()
