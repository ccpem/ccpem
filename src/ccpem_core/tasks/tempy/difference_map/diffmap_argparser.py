import argparse

class DiffMapParser(object):

    
    def __init__(self,
                 args=None):
        self.args = args

        
    def generate_args(self):
        parser = argparse.ArgumentParser()
        #map input
        inp_map = parser.add_argument_group()
        inp_map.add_argument(
            '-m',
            '--map',
            help=('Input map'),
            metavar='Input map',
            type=str,
            dest='inp_map',
            default=None,
            required=False)
        inp_map.add_argument(
            '-m1',
            '--map1',
            help=('Input map1'),
            metavar='Input map1',
            type=str,
            dest='inp_map1',
            default=None,
            required=False)
        inp_map.add_argument(
            '-m2',
            '--map2',
            help=('Input map2'),
            metavar='Input map2',
            type=str,
            dest='inp_map2',
            default=None,
            required=False)
        inp_map.add_argument(
            '-sm',
            '--sim_map',
            help=('Input simulated map'),
            metavar='Input simulated map',
            type=str,
            dest='sim_map',
            default=None,
            required=False)
        
        #input model
        search_model = parser.add_argument_group()
        search_model.add_argument(
        '-p',
            '--pdb',
            help=('Input model'),
            type=str,
            default=None,
        dest='pdb',
        required=False)
        search_model.add_argument(
        '-p1',
            '--pdb1',
            help=('Input model1'),
            type=str,
            default=None,
        dest='pdb1',
        required=False)
        search_model.add_argument(
        '-p2',
            '--pdb2',
            help=('Input model2'),
            type=str,
            default=None,
        dest='pdb2',
        required=False)
        
        #map resolution
        map_res = parser.add_argument_group()
        map_res.add_argument(
        '-r',
            '-res',
            '--map_res',
            help=('Resolution of the map'),
            type=float,
            dest='res',
            default=None)
        map_res.add_argument(
            '-r1',
            '-res1',
            '--map_res1',
            help=('Resolution of the map1'),
            type=float,
            dest='res1',
            default=None)
        map_res.add_argument(
            '-r2',
            '-res2',
            '--map_res2',
            help=('Resolution of the map2'),
            type=float,
            dest='res2',
            default=None)
        
        #contour threshold
        map_threshold = parser.add_argument_group()
        map_threshold.add_argument(
            '-t',
            '--thr',
            help='Threshold value to mask the output',
            type=float,
            dest='thr',
            default=None)
        map_threshold.add_argument(
            '-t1',
            '--thr1',
            help='Map1 threshold value to mask difference',
            type=float,
            dest='thr1',
            default=None)
        map_threshold.add_argument(
            '-t2',
            '--thr2',
            help='Map2 threshold value to mask difference',
            type=float,
            dest='thr2',
            default=None)
        map_threshold.add_argument(
            '-tf',
            '--thr_frac',
            help='Threshold of fractional difference [0-1.] to mask difference map',
            type=float,
            dest='thr_frac',
            default=None)
        
        window = parser.add_argument_group()
        window.add_argument(
            '-w',
            '--window',
            help=('Window size for scaling'),
            type=int,
            dest='window',
            default=None)

        mode = parser.add_argument_group()
        mode.add_argument(
            '-ref',
            '--refscale',
            help=('Use a reference for amplitude scaling'),
            dest='refscale',
            action='store_true',
            default=False)
        
        apix = parser.add_argument_group()
        apix.add_argument(
            '-s',
            '--apix',
            help=('Grid spacing for the maps'),
            type=float,
            dest='apix',
            default=None)
        
        mask_args = parser.add_argument_group()
        mask_args.add_argument(
            '-mask',
            '--mask',
            help=('Apply a mask on the differences (mask map file)'),
            #type=bool,
            dest='mask',
            type=str,
            default=None)
        
        parser.add_argument(
            '-sf',
            '--sigfac',
            help=('Input Sigma factor for blurring'),
            type=float,
            dest='sigfac',
            default=None)
        
        filt_args = parser.add_argument_group()
        filt_args.add_argument(
            '-nofilt',
            '--nofilt',
            help=('Do not lowpass filter maps'),
            #type=bool,
            dest='nofilt',
            action='store_true',
            default=False)
        filt_args.add_argument(
            '-dust',
            '--dust',
            help=('Apply dust filter'),
            #type=bool,
            dest='dust',
            action='store_true',
            default=bool(False))
        filt_args.add_argument(
            '-dp',
            '--dustprob',
            help=('Probability of finding dust among all density particles (greater than this value)'),
            type=float,
            dest='dustprob',
            default=0.1)
        
        flag_shell = parser.add_argument_group()
        flag_shell.add_argument(
            '-noscale',
            '--noscale',
            help=('Disable amplitude scaling'),
            #type=bool,
            dest='noscale',
            action='store_true',
            default=False)
        flag_shell.add_argument(
            '-sw',
            '--shellwidth',
            help=('Width of frequency (1/resolution) shell'),
            type=float,
            dest='shellwidth',
            default=None)

        mpi_args = parser.add_argument_group()
        mpi_args.add_argument(
            '-mpi',
            '--mpi',
            help=('Use mpi for parallel runs of local window scaling'),
            #type=bool,
            dest='mpi',
            action='store_true',
            default=False)
        mpi_args.add_argument(
            '-n_mpi',
            '--n_mpi',
            help=('Number of processors for mpi run'),
            type=int,
            dest='n_mpi',
            default=1)
        
        
        plot_args = parser.add_argument_group()
        plot_args.add_argument(
            '-plot',
            '--plot',
            help=('Plot power spectra (only for global mode)'),
            #type=bool,
            dest='plot_spectra',
            action='store_true',
            default=False)
        
        parser.add_argument(
            '-mode',
            '--mode',
            help=('local or global scaling'),
            #type=bool,
            dest='mode',
            type=str,
            default='global')
        
        parser.add_argument(
            '-nofrac',
            '--nofracmap',
            help=('local or global scaling'),
            #type=bool,
            dest='nofracmap',
            action='store_true',
            default=False)
        
        self.parser = parser
        
    def parse_args(self):
        self.args = self.parser.parse_args()
    
    def check_arguments(self):
        if not self.args.inp_map1 is None and self.args.inp_map2 is None:
            if self.args.inp_map is None:
                self.args.inp_map = self.args.inp_map1
        elif self.args.inp_map1 is None and not self.args.inp_map2 is None:
            if self.args.inp_map is None:
                self.args.inp_map = self.args.inp_map2
        elif self.args.inp_map1 is None and not self.args.inp_map is None:
            self.args.inp_map1 = self.args.inp_map
        
        if not self.args.pdb1 is None and self.args.pdb2 is None:
            self.args.pdb = self.args.pdb1
        elif self.args.pdb1 is None and not self.args.pdb2 is None:
            self.args.pdb = self.args.pdb2
        elif self.args.pdb1 is None and not self.args.pdb is None:
            self.args.pdb1 = self.args.pdb
        
        if not self.args.thr1 is None and self.args.thr2 is None:
            self.args.thr = self.args.thr1
        elif self.args.thr1 is None and not self.args.thr is None:
            self.args.thr1 = self.args.thr
#         elif self.args.thr1 is None and not self.args.thr2 is None:
#             self.args.thr = self.args.thr2
            
        if not self.args.res1 is None and self.args.res2 is None:
            self.args.res = self.args.res1
        elif self.args.res1 is None and not self.args.res is None:
            self.args.res1 = self.args.res
#         elif self.args.res1 is None and not self.args.res2 is None:
#             self.args.res = self.args.res2
        