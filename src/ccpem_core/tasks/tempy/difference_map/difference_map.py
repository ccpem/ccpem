#Added by AGNEL PRAVEEN JOSEPH
#Generate difference maps with amplitude scaling/matching, remove dusts 

import sys
from TEMPy.StructureParser import PDBParser, mmCIFParser
from TEMPy.StructureBlurrer import StructureBlurrer
import os, subprocess
from ccpem_core.tasks.tempy.difference_map.diffmap_argparser import DiffMapParser
from ccpem_core.map_tools.loc_scale.set_locscale_diffmap import \
        launch_amplitude_scaling
from TEMPy.ShowPlot import Plot
from TEMPy.mapprocess import mapcompare
from TEMPy.mapprocess import Filter
from TEMPy.mapprocess import array_utils
from copy import deepcopy
import gc
import numpy as np
import argparse
mrcfile_import=True
try:
    import mrcfile
except ImportError:
    mrcfile_import = False
#from memory_profiler import profile
import datetime
import warnings
from ccpem_core.map_tools.loc_scale import np_locscale_avg
loc_scale_python= ['ccpem-python',os.path.realpath(np_locscale_avg.__file__)]


#calculate model contour
def model_contour(p,res=4.0,emmap=False,t=-1.):
    modelmap,modelinstance = blur_model(p,res,emmap)
    contour = None
    if t != -1.0:
        contour = t*modelmap.std()#0.0
    return modelmap,contour,modelinstance
def blur_model(p,res=4.0,emmap=None):
    print 'Reading the model'
    if os.splitext(p)[-1].lower() in ['.cif','.mmcif']:
        structure_instance=mmCIFParser.read_PDB_file('pdbfile',p)
    else:
        structure_instance=PDBParser.read_PDB_file('pdbfile',p,hetatm=True,water=False)
    print 'Generating map from the model'
    blurrer = StructureBlurrer()
    if res is None: sys.exit('Map resolution required..')
    modelmap = blurrer.gaussian_blur_real_space(structure_instance, 
                                              res,sigma_coeff=0.225,
                                              densMap=emmap,normalise=True) 
    return modelmap, structure_instance    

def read_mapfile(map_path):
    if mrcfile_import:
        mrcobj=mrcfile.open(map_path,mode='r')
        if np.any(np.isnan(mrcobj.data)):
            sys.exit('Map has NaN values: {}'.format(map_path))
        emmap = Filter(mrcobj)
        emmap.fix_origin()
    else:
        mrcobj=MapParser.readMRC(map_path)
        emmap = Filter(mrcobj)
        emmap.set_apix_as_tuple()
        emmap.fix_origin()
    return emmap

def write_mapfile(mapobj,map_path):
    #TEMPy map
    if not mrcfile_import and mapobj.__class__.__name__ == 'Map':
        mapobj.write_to_MRC_file(map_path)
    #mrcfile map
    elif mrcfile_import:
        newmrcobj = mrcfile.new(map_path,overwrite=True)
        mapobj.set_newmap_data_header(newmrcobj)
        newmrcobj.close()
    #tempy mapprocess map
    else:
        newmrcobj = Map(np.zeros(mapobj.fullMap.shape), 
                        list(mapobj.origin), 
                        mapobj.apix, 'mapname')
        mapobj.set_newmap_data_header(newmrcobj)
        newmrcobj.update_header()
        newmrcobj.write_to_MRC_file(map_path)

class AttrDict(dict):
    def __init__(self, *args, **kwargs):
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self

def set_args_local_scale(args,outfile,mpi=False,
                         verbose=True, map1=None, map2=None,
                         apix=None,mask=None):
    '''
    Add arguments for loc scale (as attributes)
    '''
    ##dict_args = AttrDict()
    if map1 is None: args.em_map = args.inp_map1
    else: args.em_map = map1
    if map2 is None: args.model_map = args.inp_map2
    else: args.model_map = map2
    if args.refscale:
        args.avg = False
    else: args.avg = True
    args.outfile = outfile
    args.window_size = args.window
    #args.mpi = mpi
    args.verbose = verbose
    if args.apix is None:
        args.apix = apix
    if args.mask is None:
        args.mask = mask

def lowpass_filter(mrcmap,resolution,inplace=False):
    lowpass_freq = mrcmap.apix[0]/resolution
    ftfilter = array_utils.tanh_lowpass(mrcmap.fullMap.shape,
                                        cutoff=lowpass_freq, 
                                        fall=0.2)
    filtmap = mrcmap.fourier_filter(ftfilter=ftfilter,inplace=inplace)
    if inplace: return mrcmap
    return filtmap


def main():
    #print help
    Usage = \
    '''
    -m/-m1 [map] for input map; -m1,-m2 for two input maps
    -p/-p1 [pdb] for input pdb; -p1, -p2 for two input pdbs
    -r for resolution; -r1,r2 for the two map resolutions
    -t for mask difference by input map density threshold; -t1,t2 for the two map thresholds
    -tf [difference fraction threshold] threshold output difference 
    based on ratio within initial density, range [0-1.0]
    --dust to enable dusting of difference maps, with tf=0.35 (by default)
    --refscale to consider second map (or model) amplitudes as reference for scaling the first map
    --nofilt to disable lowpass filter before amplitude scaling (not recommended)
    --noscale to disable amplitude scaling (not recommended)
    --nofracmap to disable generating fractional difference maps
    -w for size of window for local scaling (optional)
    -mpi to use mpi processing for local scaling (optional)
    -mask to provide mask file for local scaling (optional). 
        Mask based on density threshold used by default (1.5 sigma or value of -t if provided)
    '''
    
    tp = DiffMapParser()
    tp.generate_args()
    tp.parse_args()
    tp.check_arguments()
    #COMMAND LINE OPTIONS
    #map input
    m1 = tp.args.inp_map1
    m2 = tp.args.inp_map2
    m = tp.args.inp_map
    
    #map resolutions
    r1 = tp.args.res1
    r2 = tp.args.res2
    r = tp.args.res
    #map contours
    c1 = tp.args.thr1
    c2 = tp.args.thr2
    c = tp.args.thr
    #TODO: ccpem GUI doesnt reset to None at the moment
    if tp.args.thr_frac == 0.0: tp.args.thr_frac = None
    cf = tp.args.thr_frac
    #atomic model input
    p = tp.args.pdb
    p1 = tp.args.pdb1
    p2 = tp.args.pdb2
    #voxel size
    apix = tp.args.apix
    #apply contour mask to calculated difference map?
    maskfile = tp.args.mask
    input_mask = False
    if maskfile is not None:
        input_mask = True
        
    #whether to scale amplitudes
    flag_scale = True
    if tp.args.noscale: 
        flag_scale = False
        print 'Warning: scaling disabled!'
    #scaling mode
    scaling_mode = tp.args.mode
    #whether to lowpass filter before scaling
    flag_filt = True
    if tp.args.nofilt:
        flag_filt = False
    #whether to use the second map (model map) as reference
    refsc=tp.args.refscale
    # width of resolution shell
    sw = tp.args.shellwidth
    #whether to apply dust filter after difference
    flag_dust = False
    if tp.args.dust: flag_dust = True
    randsize = 0.1
    if flag_dust:
        randsize = tp.args.dustprob
    #plot spectra after scaling
    plot_spectra = tp.args.plot_spectra
    if tp.args.window == 0: tp.args.window = None
    
    Name1 = Name2 = ''
    #GET INPUT DATA
    output_synthetic_map = False
    #no map input
    if all(x is None for x in [m,m1,m2]):
        # for 2 models
        if None in [p1,p2]:
            sys.exit('Input two maps/map-and-model/two models')
        Name1 = os.path.basename(p1).split('.')[0]
        emmap1,c1,p1inst = model_contour(p1,res=4.0,emmap=False,t=0.1)
        r1 = r2 = r = 4.0
        Name2 = os.path.basename(p2).split('.')[0]
        emmap2,c2,p2inst = model_contour(p2,res=r,emmap=False,t=0.1)
        flag_filt = False
        flag_scale = False
        output_synthetic_map = True
    #map and model
    elif None in [m1,m2]:
        # for one map and model, m = map, c1 = map contour, c2 = model contour
        #read map
        Name1 = os.path.basename(m).split('.')[0]
        print 'Reading map'
        emmap1 = read_mapfile(m)
        #set contour
        if c1 is None and c is None: 
            c1 = emmap1.calculate_map_contour(sigma_factor=2.0)
            c = c1
            
        if r1 is None and r is None: 
            sys.exit('Input two maps/map-and-model/two models, \
            map resolution(s) (required)')
        
        if all(x is None for x in [p,p1,p2]): 
            sys.exit('Input two maps/map-and-model/two models')
        
        r2 = r1
        #TODO : fix a model contour
        if r1 > 20.0: mt = 2.0
        elif r1 > 10.0: mt = 1.0
        elif r1 > 6.0: mt = 0.5
        else: mt = 0.1
        if c2 is None:
            Name2 = os.path.basename(p).split('.')[0]+'_syn'
            emmap2,c2,p2inst = model_contour(p,res=r1,emmap=emmap1,t=mt)
        #scale based on the model amplitudes
        if refsc is None:
            refsc = tp.args.refscale = True
            #flag_filt = False
        output_synthetic_map = True
    else: 
        # For 2 input maps
        if None in [r1,r2]: 
            sys.exit('Input two maps/map-and-model/two models, \
            their resolutions(required)')
        print 'Reading map1'
        Name1 = os.path.basename(m1).split('.')[0]
        emmap1 = read_mapfile(m1)
        if c1 is None:
            c1 = emmap1.calculate_map_contour(sigma_factor=2.0)
        print 'Reading map2' 
        Name2 = os.path.basename(m2).split('.')[0]
        emmap2 = read_mapfile(m2)
        if c2 is None:
            c2 = emmap2.calculate_map_contour(sigma_factor=2.0)
            
    gc.collect()
    #MAIN CALCULATION
    #check if map objects are from mrcfile
    if emmap1.__class__.__name__ == 'Map':
        emmap1 = Filter(emmap1)
    if emmap2.__class__.__name__ == 'Map':
        emmap2 = Filter(emmap2)
    #whether to shift density to positive values
    '''
    c1 = (c1 - emmap1.min())
    c2 = (c2-emmap2.min())
    emmap1.fullMap = (emmap1.fullMap - emmap1.min())
    emmap2.fullMap = (emmap2.fullMap - emmap2.min())
    '''
    #check if grid dimensions are same
    samegrid = False
    try:
        mapcompare.compare_grid(emmap1,emmap2)
        print 'Map dimensions are same'
        samegrid = True
    except AssertionError: samegrid = False
    #check if spacing along all axes are same
    #this is a temporary fix until variable spacing is tested
    if emmap1.apix[0] != emmap1.apix[1] or emmap1.apix[1] != emmap1.apix[2]:
        samegrid = False
    if emmap2.apix[0] != emmap2.apix[1] or emmap2.apix[1] != emmap2.apix[2]:
        samegrid = False
    
    #SCALING, scaled maps (diff1,diff2)
    if flag_scale:
            
        if refsc: print 'Using second model/map amplitudes as reference'
        #local scaling
        if scaling_mode != 'global':
            #make a shallow copy
            emmap_1 = emmap1.copy(deep=False)
            emmap_2 = emmap2.copy(deep=False)
            #low pass filter (new maps generated)
            if flag_filt:
                print 'Low-pass filtering'
                try:
                    emmap_1 = lowpass_filter(emmap1,r1,inplace=False)
                    emmap_2 = lowpass_filter(emmap2,r2,inplace=False)
                    gc.collect()
                except:
                    flag_filt = False # to check failure
                    print 'Low-pass filtering failed'
            else:
                print 'Low-pass filter disabled'
                    
            print 'Using local scaling mode'
            outfile = Name1+'_'+Name2+'.mrc'
            #set loc scale arguments
            if tp.args.mpi:
                list_args = ['ccpem-mpirun',
                       '-np',
                       str(tp.args.n_mpi)]+loc_scale_python+['-mpi']
            else:
                list_args = loc_scale_python
            #interpolate to common grid
            if not samegrid:
                spacing = max(max(emmap_1.apix),max(emmap_2.apix))
                grid_shape, new_ori = mapcompare.alignment_box(emmap_1,emmap_2,spacing)
                #     if not apix is None: spacing = apix
                #interpolate to common grid (new maps)
                diff1 = emmap_1.interpolate_to_grid(grid_shape,spacing,new_ori,inplace=False)
                diff2 = emmap_2.interpolate_to_grid(grid_shape,spacing,new_ori,inplace=False)
                #save maps
                write_mapfile(diff1, 'map1.mrc')
                write_mapfile(diff2, 'map2.mrc')
                #set mask for local scaling
                if tp.args.mask is None:
                    if output_synthetic_map:#model input
                        maskmap = diff2.copy(deep=True)
                        maskmap.fullMap[:] = diff2.fullMap > 0.1
                    else:
                        maskmap = diff1.copy(deep=True)
                        #union of both map contours
                        maskmap.fullMap[:] = (diff1.fullMap > c1) | (diff2.fullMap > c2)
                        #maskmap.fullMap[:] = maskmap.fullMap > 0
                    write_mapfile(maskmap, 'mask.mrc')
                    del maskmap
                #delete interpolated maps
                if refsc: del diff1
                else: del diff1, diff2
                #delete lowpass filtered maps
                if flag_filt:
                    del emmap_2
                #set local scaling arguments
                list_args += ['-em','map1.mrc','-mm','map2.mrc',
                                 '-p',str(round(spacing,3)),
                                 '-o',outfile]
                set_args_local_scale(tp.args, outfile, map1='map1.mrc', map2='map2.mrc',
                                     apix=round(spacing,3),mask='mask.mrc',mpi=True)
                gc.collect()
            else:
                if tp.args.mask is None:
                    #create mask
                    if output_synthetic_map:#input model
                            maskmap = emmap2.copy(deep=True)
                            #TODO: check mask with both model and map contours
                            maskmap.fullMap[:] = emmap2.fullMap > 0.1
                    else:
                        try:
                            mapcompare.compare_grid(emmap1,emmap_1)#if lowpass filtered map has same grid
                            maskmap = emmap1.copy(deep=True)
                            #union of two masks
                            maskmap.fullMap[:] = (emmap1.fullMap > c1) | (emmap2.fullMap > c2)
                        except AssertionError:
                            maskmap = emmap_1.copy(deep=True)
                            maskmap.fullMap[:] = (emmap_1.fullMap > c1) | (emmap_2.fullMap > c2)
                        #maskmap.fullMap[:] = maskmap.fullMap > 0
                    write_mapfile(maskmap, 'mask.mrc')
                    del maskmap
                #if lowpass filtered
                if flag_filt:
                    write_mapfile(emmap_1, 'map1.mrc')
                    write_mapfile(emmap_2, 'map2.mrc')
                    list_args += ['-em','map1.mrc','-mm','map2.mrc',
                                 '-p',str(round(max(emmap_1.apix),3)),
                                 '-o',outfile]
                    set_args_local_scale(tp.args, outfile,map1='map1.mrc',map2='map2.mrc',
                                         apix=max(emmap_1.apix),mask='mask.mrc')
                #for model input
                elif tp.args.inp_map2 is None: 
                    write_mapfile(emmap_2, 'map2.mrc')
                    list_args += ['-em',tp.args.inp_map1,'-mm','map2.mrc',
                                 '-p',str(round(max(emmap_1.apix),3)),
                                 '-o',outfile]
                    set_args_local_scale(tp.args, outfile,map2='map2.mrc',
                                         apix=max(emmap_1.apix),mask='mask.mrc')
                else:
                    list_args += ['-em',tp.args.inp_map1,'-mm',tp.args.inp_map2,
                                 '-p',str(round(max(emmap_1.apix),3)),
                                 '-o',outfile]
                    set_args_local_scale(tp.args, outfile,apix=max(emmap_1.apix),
                                           mask='mask.mrc')
                #store diff2, delete lowpass filtered map
                if refsc: 
                    if not flag_filt: 
                        diff2 = emmap_2.copy(deep=True)
                        del emmap_2
                    else: diff2 = emmap_2.copy(deep=False)
                gc.collect()
            #delete low-pass filtered map
            if flag_filt:
                del emmap_1
            if tp.args.mask is None:
                list_args += ['-ma','mask.mrc']
            else:
                list_args += ['-ma',tp.args.mask]
            if tp.args.window != None:
                list_args += ['-w',str(tp.args.window)]
            list_args += ['-v','True']
            print ' '.join(list_args)
            if refsc:
                emmap1_locscaled = outfile
                subprocess.call(list_args)
                assert os.path.isfile(emmap1_locscaled)
                
#                 emmap1_locscaled = launch_amplitude_scaling(tp.args)
                diff1 = read_mapfile(emmap1_locscaled)
                
            else:
                list_args += ['-avg']
                emmap1_locscaled = '.'.join(outfile.split('.')[:-1])+'_map1.mrc'
                emmap2_locscaled = '.'.join(outfile.split('.')[:-1])+'_map2.mrc'
                subprocess.call(list_args)
                assert os.path.isfile(emmap1_locscaled)
                assert os.path.isfile(emmap2_locscaled)
                
#                 emmap1_locscaled,emmap2_locscaled = launch_amplitude_scaling(tp.args)
                diff1 = read_mapfile(emmap1_locscaled)
                diff2 = read_mapfile(emmap2_locscaled)
            
            gc.collect()
        #global scaling
        else:
            if not flag_filt:
                print 'Low-pass filter disabled'
                
            print 'Using global scaling mode'
            if not samegrid:
                spacing = max(max(emmap1.apix),max(emmap2.apix))
                grid_shape, new_ori = mapcompare.alignment_box(emmap1,emmap2,spacing)
                #resample scaled maps to common grid
                #     if not apix is None: spacing = apix
                #interpolate to common grids
                emmap_1 = emmap1.interpolate_to_grid(grid_shape,spacing,new_ori,inplace=False)
                emmap_2 = emmap2.interpolate_to_grid(grid_shape,spacing,new_ori,inplace=False)
                # amplitude scaling 
                diff1,diff2, dict_plot = mapcompare.amplitude_match(
                                    emmap_1,emmap_2,reso=max(r1,r2),lpfiltb=flag_filt,
                                    ref=refsc)
                #delete interpolated maps
                del emmap_1, emmap_2
                gc.collect()
            else:
                # amplitude scaling 
                diff1,diff2, dict_plot = mapcompare.amplitude_match(
                                    emmap1,emmap2,reso=max(r1,r2),lpfiltb=flag_filt,
                                    ref=refsc)

            if plot_spectra:
                pl = Plot()
                pl.lineplot(dict_plot,'spectra.png')
    else:
        print 'Scaling disabled'
        #make copies of input maps
        diff1 = emmap1.copy(deep=True)
        diff2 = emmap2.copy(deep=True)

    ##min of minimums in the two scaled maps
    min1 = diff1.min()
    min2 = diff2.min()
    min_scaled_maps = min(min1,min2)
    #shift to positive values
    if (min_scaled_maps < 0.):
        #make values non-zero
        min_scaled_maps = min_scaled_maps + 0.05*min_scaled_maps
        diff1.shift_density(-min_scaled_maps,inplace=True)
        diff2.shift_density(-min_scaled_maps,inplace=True)
    print 'Calculating difference'
    
    # store scaled map 1
    scaledmap1 = diff1.copy(deep=True)
    #calculate difference map 1
    diff1.fullMap = (diff1.fullMap - diff2.fullMap)
    #store scaled map 2
    if not tp.args.nofracmap or flag_dust or not tp.args.thr_frac is None:
        scaledmap2 = diff2.copy(deep=True)
    #calculate difference map 2
    diff2.fullMap = (diff2.fullMap - scaledmap1.fullMap)
    
#     if contour_mask:
#         diff1.apply_mask(mask1.fullMap,inplace=True)
#         diff2.apply_mask(mask2.fullMap,inplace=True)
    #difference fraction
    if not tp.args.nofracmap or flag_dust:
        mask1 = scaledmap1.copy(deep=True)
        mask2 = scaledmap2.copy(deep=True)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            mask1.fullMap[:] = diff1.fullMap/scaledmap1.fullMap
        mask1.fullMap[np.isnan(mask1.fullMap)]  = 1.0
        mask1.fullMap[np.isinf(mask1.fullMap)]  = 1.0
        #mask1.fullMap[:] = mask1.fullMap*(mask1.fullMap > 0.0)
        if not tp.args.nofracmap: write_mapfile(mask1, 'diff_frac1.mrc')
        del scaledmap1
        
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            mask2.fullMap[:] = diff2.fullMap/scaledmap2.fullMap
        mask2.fullMap[np.isnan(mask2.fullMap)] = 1.0
        mask2.fullMap[np.isinf(mask2.fullMap)] = 1.0
        #mask2.fullMap[:] = mask2.fullMap*(mask2.fullMap > 0.0)
        if not tp.args.nofracmap: write_mapfile(mask2, 'diff_frac2.mrc')
        del scaledmap2
        gc.collect()
    #dust filter or threshold based on fraction difference
    if flag_dust or not tp.args.thr_frac is None:
        
        if tp.args.thr_frac is None: 
            if tp.args.mode == 'global': 
                print 'Using threshold {} for fractional difference'.format(0.3)
                mask1.fullMap[:] = mask1.fullMap > 0.3
            else: 
                print 'Using threshold {} for fractional difference'.format(0.3)
                mask1.fullMap[:] = mask1.fullMap > 0.3
        else: 
            print 'Using threshold {} for fractional difference'.format(tp.args.thr_frac)
            mask1.fullMap[:] = mask1.fullMap > tp.args.thr_frac
        diff1.apply_mask(mask1.fullMap,inplace=True)
        if flag_dust:
            print 'Dusting the differences'
            diff1.remove_dust_by_size(0.0,
                            prob=randsize,inplace=True)
        
        if tp.args.thr_frac is None: 
            if tp.args.mode == 'global': mask2.fullMap[:] = mask2.fullMap > 0.3
            else: mask2.fullMap[:] = mask2.fullMap > 0.3
        else: mask2.fullMap[:] = mask2.fullMap > tp.args.thr_frac
        diff2.apply_mask(mask2.fullMap,inplace=True)
        
        if flag_dust: 
            diff2.remove_dust_by_size(0.0,
                            prob=randsize,inplace=True)
        
        diff1.apply_mask(diff1.fullMap>0.0,inplace=True)
        diff2.apply_mask(diff2.fullMap>0.0,inplace=True)
        
        del mask1, mask2
    elif tp.args.nofracmap: 
        del scaledmap1
    
    gc.collect()
    if not samegrid:
        #interpolate back to original grids
        diff1_inigrid = diff1.interpolate_to_grid(emmap1.fullMap.shape,emmap1.apix,
                                  emmap1.origin,inplace=False)
        diff2_inigrid = diff2.interpolate_to_grid(emmap2.fullMap.shape,emmap2.apix,
                                  emmap2.origin,inplace=False)
        del diff1, diff2
    else:
        diff1_inigrid = diff1.copy(deep=False)
        diff2_inigrid = diff2.copy(deep=False)
    #write the difference maps
    print 'Writing differences'
    mapfile_diff1 = Name1+'-'+Name2+'_diff.mrc'
    mapfile_diff2 = Name2+'-'+Name1+'_diff.mrc'
    write_mapfile(diff1_inigrid,mapfile_diff1)
    write_mapfile(diff2_inigrid,mapfile_diff2)

    # If PDB given write out synthetic map
    if output_synthetic_map:
        print 'Output synthetic map from : ', Name2
        synmap_file = Name2+'.mrc'
        write_mapfile(emmap2,synmap_file)

if __name__ == '__main__':
    main()
