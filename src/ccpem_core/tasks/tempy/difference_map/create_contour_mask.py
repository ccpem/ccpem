from TEMPy.MapProcess import MapEdit
import mrcfile,argparse
import numpy as np 
import os

parser = argparse.ArgumentParser()
parser.add_argument(
    '-m',
    '--map',
    help=('Input map'),
    metavar='Input map',
    type=str,
    dest='inp_map',
    default=None,
    required=False)
parser.add_argument(
    '-t',
    '--thr',
    help='Map threshold value',
    type=float,
    dest='thr',
    default=None)
args = parser.parse_args()
if args.inp_map is None: 
    sys.exit('No map input')
else:
    mapfile = args.inp_map
    mrcobj = mrcfile.open(mapfile,mode='r')
    emmap = MapEdit(mrcobj)
contour = args.thr
if contour is None:
    contour = emmap.calculate_map_contour(sigma_factor=1.5)

contoured_map = emmap.copy(deep=False)
contoured_map.fullMap = np.array((emmap.fullMap > contour)*1.0,dtype='float32')
maskfile = os.path.abspath(os.path.splitext(mapfile)[0]+'_contourmask.mrc')
maskmap = mrcfile.new(maskfile,overwrite=True)
contoured_map.set_newmap_data_header(maskmap)
maskmap.close()


 

