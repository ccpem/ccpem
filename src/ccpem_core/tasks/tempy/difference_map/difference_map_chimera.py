'''
Chimera script called by TEMPy.difference map GUI.
Sets map and diffmap contour levels to 1.5 and 2.5 sigma.
'''

import sys
import os
#from nis import maps
try:
    from chimera import runCommand as rc
    run_chimera_script = True
except ImportError:
    run_chimera_script = False
from collections import OrderedDict
import numpy as np


if run_chimera_script:
    # Expect following sys arg inputs:
    #    1) this script
    #    2+3) input maps
    #    4+5) difference maps
    if len(sys.argv) != 6:
        pass
    else:
        # Sigma values for map and difference map
        sigma_map = 2.0
        sigma_diffmap = 1.0

        # Dictionary:
        #    map_name : (path, sigma level)
        maps = OrderedDict([
            ('map1', (sys.argv[1], sigma_map)),
            ('map2', (sys.argv[2], sigma_map)),
            ('diffmap1', (sys.argv[3], sigma_diffmap)),
            ('diffmap2', (sys.argv[4], sigma_diffmap))])
        # Set sigma values and display items
        model_index = 0
        for key, value in maps.iteritems():
            if value[0] == 'None':
                continue
            # Load map
            rc("open %s"%(value[0]))
            # Get model
            model_list = chimera.openModels.list()
            model = model_list[model_index]
            model_index += 1
            # Calc
            matrix = model.data.full_matrix()
            mean = np.mean(matrix)
            std = np.std(matrix)
            print rc("measure mapStats #0")
            # Set sigma values, display params, hide dust
            if key == 'map1':
                rc("volume #0 level %f hide"%(value[1]*std))
            elif key == 'map2':
                rc("volume #1 level %f"%(value[1]*std))
            elif key == 'diffmap1':
                rc("volume #2 level %f transparency 0.5 color green"%(
                    value[1]*std))
                # Hide dust
#                 rc("sop hideDust #2 size 15")
            elif key == 'diffmap2':
                rc("volume #3 level %f color grey"%(
                    value[1]*std))

        # Show pdb
        if sys.argv[5] != 'no_pdb':
            if os.path.exists(sys.argv[5]):
                rc("open %s"%(sys.argv[5]))