#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.tasks.tempy.difference_map import difference_map
from ccpem_core.tasks.refmac import refmac_task
from ccpem_core.tasks.model2map.model2map_task import sf2mapWrapper
from ccpem_core.tasks.bin_wrappers import fft
from ccpem_core.map_tools.TEMPy import map_preprocess, get_map_density, \
                        attribute_map_values
from ccpem_core.model_tools.gemmi_utils import shift_coordinates, remove_atomic_charges
import mrcfile
from ccpem_core import settings
import shutil,json

class DifferenceMap(task_utils.CCPEMTask):
    '''
    CCPEM TEMPy difference map wrapper.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='TEMPy Difference Map',
        author='A. Joseph, M. Topf, M. Winn',
        version='1.1',
        description=(
            '''Difference map calculation using TEMPy and LocScale libraries.The maps
            are matched by amplitude scaling in resolution shells before 
            calculating difference.<br><br>
            Input two maps or a map and an atomic model.<br>
            - The maps have to be aligned before using as input 
            for this task. If an atomic model is provided, 
            it has to be fitted in the experimental map.<br><br>
            - Local or Global mode can be used for scaling. For local mode,
            scaling in done in local sliding windows and hence local resolution
             variations are considered for scaling. Local mode has MPI option 
             for faster run.
            - A fractional difference map is calculated by default (difference/initial).
            This can be disabled. A difference fraction threshold can be used to 
            mask the difference map as well.
            - Optionally, a dust filter can be applied on the difference maps.
            This is done to remove small isolated densities that usually 
            correspond to noise in the differences. A fractional difference threshold 
            is used to mask first before dusting. The threshold can be set.<br>
            References:<br>
            <a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7254831/"> 
            Joseph et al, 2019</a><br>
            <br>
            '''),
        short_description=(
            'Difference map calculation'),
        documentation_link='',
        references='https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7254831/')

    commands = {'ccpem-diffmap':
        ['ccpem-python', os.path.realpath(difference_map.__file__)],
        'refmac': settings.which(program='refmac5'),
        'pdbset':  settings.which(program='pdbset'),
        'ccpem-gemmi':settings.which(program='gemmi'),
        'ccpem-mapval':['ccpem-python', 
                        os.path.realpath(attribute_map_values.__file__)],
        'ccpem-mapprocess':
        ['ccpem-python', os.path.realpath(map_preprocess.__file__)]}

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        #
        super(DifferenceMap, self).__init__(
             database_path=database_path,
             args=args,
             args_json=args_json,
             pipeline=pipeline,
             job_location=job_location,
             parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        map_path_1 = parser.add_argument_group()
        map_path_1.add_argument(
            '-map_path_1',
            '--map_path_1',
            help='Input map 1 (mrc format)',
            metavar='Input map 1',
            type=str,
            default=None)
        #
        map_resolution_1 = parser.add_argument_group()
        map_resolution_1.add_argument(
            '-map_resolution_1',
            '--map_resolution_1',
            help='''Resolution of map 1 (Angstrom)''',
            metavar='Resolution map 1',
            type=float,
            default=None)
                #
        map_path_2 = parser.add_argument_group()
        map_path_2.add_argument(
            '-map_path_2',
            '--map_path_2',
            help='Input map 2 (mrc format)',
            metavar='Input map 2',
            type=str,
            default=None)
        #
        map_resolution_2 = parser.add_argument_group()
        map_resolution_2.add_argument(
            '-map_resolution_2',
            '--map_resolution_2',
            help='''Resolution of map 2 (Angstrom)''',
            metavar='Resolution map 2',
            type=float,
            default=None)

        self.add_diffmap_specific_arguments(parser)
        return parser
        
    def add_diffmap_specific_arguments(self,parser):
        #
        pdb_path = parser.add_argument_group()
        pdb_path.add_argument(
            '-pdb_path',
            '--pdb_path',
            help=('Synthetic map will be created and difference maps '
                  'calculated using this'),
            metavar='Input atomic model',
            type=str,
            default=None)
        #
        map_or_pdb_selection = parser.add_argument_group()
        map_or_pdb_selection.add_argument(
            '-map_or_pdb_selection',
            '--map_or_pdb_selection',
            help=('Select map or PDB'),
            metavar='Input selection',
            choices=['Map', 'Model'],
            type=str,
            default='Map')
        #local or global
        mode_selection = parser.add_argument_group()
        mode_selection.add_argument(
            '-mode_selection',
            '--mode_selection',
            help=('Select local or global scaling mode'),
            metavar='Mode selection',
            choices=['global', 'local','None'],
            type=str,
            default='global')
        parser.add_argument(
            '-noscale',
            '--noscale',
            help=('Do not scale for difference'),
            metavar='Scale the maps?',
            type=bool,
            default=False)
        parser.add_argument(
            '-nofilt',
            '--nofilt',
            help=('Do not lowpass filter input maps'),
            metavar='Disable lowpass filter?',
            type=bool,
            default=False)
        parser.add_argument(
            '-use_refmac',
            '--use_refmac',
            help=('Use Refmac to make map from model. \
                    Ensure atomic B-factors are refined.'),
            metavar='Use Refmac to simulate map from model?',
            type=bool,
            default=False)
        #ligand dictionary for Refmac
        parser.add_argument(
            '-lib_in',
            '--lib_in',
            help='Ligand dictionary for Refmac (cif format)',
            metavar='Ligand dictionary',
            type=str,
            default=None)
                
        parser.add_argument(
            '-use_mpi',
            '--use_mpi',
            help=('Use MPI for local scaling'),
            metavar='Use MPI?',
            type=bool,
            default=False)
        parser.add_argument(
            '-n_mpi',
            '--n_mpi',
            help=('Use n proc for mpi'),
            metavar='Use n proc',
            type=int,
            default=1)
        parser.add_argument(
            '-maskfile',
            '--maskfile',
            help=('Input mask map. Local scaling is calculated within this mask'),
            metavar='Input mask map',
            type=str,
            default=None)
        parser.add_argument(
            '-w',
            '--window_size',
            help=('Window size for local scaling (default 7*resolution)'),
            metavar='Window size',
            type=int,
            default=None)
        #
        parser.add_argument(
            '-refscale',
            '--refscale',
            help='''Use second map as reference for scaling''',
            metavar='Use map2 as reference?',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-threshold_fraction',
            '--threshold_fraction',
            help='''Threshold difference map by fractional difference (difference/initial).'''
                    ''' Set this for dusting (0.3 by default for dusting)''',
            metavar='Cutoff: fractional difference',
            type=float,
            default=None)
        #
        dust_filter = parser.add_argument_group()
        dust_filter.add_argument(
            '-dust_filter',
            '--dust_filter',
            help='''Remove dust beyond a threshold of fractional difference (default: 0.3)''',
            metavar='Dust filter',
            type=bool,
            default=False)
        #
        dust_filter.add_argument(
            '-dustprob',
            '--dustprob',
            help='''Probability of dust size (sizes divided in 20 bins) greather than?'''
                ''' Dusts (small sized densities) are usually more probable than useful differences''',
            metavar='Dust size probability',
            type=float,
            default=0.1
            )
        #
        frac_map = parser.add_argument_group()
        frac_map.add_argument(
            '-save_fracmap',
            '--save_fracmap',
            help='''Calculate fractional difference maps''',
            metavar='Calc fractional difference',
            type=bool,
            default=True)
        #
        map_alignment = parser.add_argument_group()
        map_alignment.add_argument(
            '-map_alignment',
            '--map_alignment',
            help='''Map alignment (~15mins) (optional)''',
            metavar='Align maps',
            type=bool,
            default=False)
        #softmask currently not enabled
        #
#         soft_mask = parser.add_argument_group()
#         soft_mask.add_argument(
#             '-soft_mask',
#             '--soft_mask',
#             help='''Apply soft mask to input maps (optional)''',
#             metavar='Apply soft mask',
#             type=bool,
#             default=False)
        #

    def check_processed_maps(self):
        if self.args.map_path_1.value is not None:
            processed_map1_path = os.path.splitext(
                                os.path.basename(self.args.map_path_1.value))[0] \
                                +'_processed.mrc'
            if os.path.isfile(processed_map1_path) and \
                self.mapprocess_task1 is not None:
                self.unprocessed_map1 = self.args.map_path_1.value
                self.args.map_path_1.value = processed_map1_path
            
        if self.args.map_path_2.value is not None:
            processed_map2_path = os.path.splitext(
                        os.path.basename(self.args.map_path_2.value))[0] \
                        +'_processed.mrc'
            if os.path.isfile(processed_map2_path) and \
                self.mapprocess_task2 is not None:
                self.unprocessed_map2 = self.args.map_path_2.value
                self.args.map_path_2.value = processed_map2_path

    def edit_argsfile(self,args_file):
        with open(args_file,'r') as f:
            json_args = json.load(f)
        #set map and pdb paths if fixed for origin
        if self.args.use_refmac.value:
            json_args['map_path_1'] = self.map_edit_path
            json_args['pdb_path'] = self.pdb_edit_path
                    
        with open(args_file,'w') as f:
            json.dump(json_args,f)


    def fix_model_for_refmac(self,pdb_path,pdb_edit_path,trans_vector,
                             remove_charges=False):
        shift_coordinates(pdb_path,pdb_edit_path,trans_vector,remove_charges)

    def run_pipeline(self, job_id=None, db_inject=None):
        pl = []
        fix_map_model = True
        self.map_edit_path = self.args.map_path_1.value
        self.pdb_edit_path = self.args.pdb_path.value
        self.map_or_pdb_selection_edit = self.args.map_or_pdb_selection.value
        #if model is given as input
        if self.args.map_or_pdb_selection.value == "Model":
            if self.args.pdb_path.value != None:
                self.args.map_path_2.value = None
            #use Refmac for model map conversion
            if self.args.use_refmac.value and \
                os.path.isfile(self.args.map_path_1.value):
                with mrcfile.open(self.args.map_path_1.value, mode='r',permissive=True) as mrc:
                    map_nx = mrc.header.nx
                    map_ny = mrc.header.ny
                    map_nz = mrc.header.nz
                    map_nxstart = mrc.header.nxstart
                    map_nystart = mrc.header.nystart
                    map_nzstart = mrc.header.nzstart
                    map_mapc = mrc.header.mapc
                    map_mapr = mrc.header.mapr
                    map_maps = mrc.header.maps
                    ori_x = mrc.header.origin.x
                    ori_y = mrc.header.origin.y
                    ori_z = mrc.header.origin.z
                    cella = (mrc.header.cella.x,mrc.header.cella.y,
                             mrc.header.cella.z)
                    apix = mrc.voxel_size.item()
                
                #fix map and model when nstart is non-zero
                if ori_x == 0. and ori_y == 0. and ori_z == 0.:
                    if map_nxstart != 0 or map_nystart != 0 or map_nzstart != 0:
                        map_edit = os.path.splitext(
                            os.path.basename(self.args.map_path_1.value))[0]+'_fix.mrc'
                        self.map_edit_path = os.path.join(
                                        self.job_location,map_edit)
                        # fix origin to agree with nstart
                        shutil.copyfile(self.args.map_path_1.value,self.map_edit_path)
                        assert os.path.isfile(self.map_edit_path)
                        with mrcfile.open(self.map_edit_path,'r+') as mrc:
                            mrc.header.origin.x = map_nxstart*apix[0]
                            mrc.header.origin.y = map_nystart*apix[1]
                            mrc.header.origin.z = map_nzstart*apix[2]
                        ori_x = map_nxstart*apix[0]
                        ori_y = map_nystart*apix[1]
                        ori_z = map_nzstart*apix[2]
                        #fix_map_model = False
#                         #reset map path
#                         self.args.map_path_1.value = self.map_edit_path

                model_map_filename = os.path.splitext(
                            os.path.basename(self.args.pdb_path.value))[0]
                self.reference_map = os.path.join(
                    self.job_location,
                    model_map_filename+'_refmac.mrc')
                
                if os.path.splitext(self.args.pdb_path.value)[-1].lower() in ['.cif','.mmcif']:
                    self.pdb_edit_path = os.path.join(self.job_location,
                                        os.path.splitext(os.path.basename(
                                            self.args.pdb_path.value))[0]+'_fix.cif')
                else:
                    self.pdb_edit_path = os.path.join(self.job_location,
                                        os.path.splitext(os.path.basename(
                                            self.args.pdb_path.value))[0]+'_fix.pdb')

                #translate model to origin 0
                if fix_map_model and not all(o == 0. for o in (ori_x,ori_y,ori_z)):
                    trans_vector = [-ori_x,
                                    -ori_y,
                                    -ori_z]
                    self.fix_model_for_refmac(self.args.pdb_path.value,
                                              self.pdb_edit_path,trans_vector,
                                              remove_charges=True)
                else:
                    remove_atomic_charges(self.args.pdb_path.value,
                                      self.pdb_edit_path)
#                     #reset pdb path
#                     self.args.pdb_path.value = self.pdb_edit_path
#                     args_file = os.path.join(self.job_location,
#                                  'args.json')
#                     self.edit_argsfile(args_file)
                        # Set cell parameters and scale of model
                self.process_set_unit_cell = refmac_task.SetModelCell(
                    job_location=self.job_location,
                    name='Set model unit cell ',
                    pdb_path=self.pdb_edit_path,
                    map_path=self.map_edit_path)
                pl = [[self.process_set_unit_cell.process]]

                if os.path.splitext(self.args.pdb_path.value)[-1].lower() in ['.cif','.mmcif'] or \
                     os.stat(self.args.pdb_path.value).st_size > 10000000:
                    self.pdb_set_path = self.process_set_unit_cell.cifout_path
                else:
                    self.pdb_set_path = self.process_set_unit_cell.pdbout_path
            

                # Calculate mtz from pdb 
                self.refmac_sfcalc_crd_process = refmac_task.RefmacSfcalcCrd(
                    job_location=self.job_location,
                    pdb_path=self.pdb_set_path,
                    lib_in=self.args.lib_in(),
                    resolution=self.args.map_resolution_1.value)
                self.refmac_sfcalc_mtz = os.path.join(
                    self.job_location,
                    'sfcalc_from_crd.mtz')
                pl.append([self.refmac_sfcalc_crd_process.process])

#                 # Convert calc mtz to map
#                 self.fft_process = fft.FFT(
#                     job_location=self.job_location,
#                     map_nx=map_nx,
#                     map_ny=map_ny,
#                     map_nz=map_nz,
#                     # For now only use x,y,z
#     #                 fast=mrc_axis[int(map_mapc)],
#     #                 medium=mrc_axis[int(map_mapr)],
#     #                 slow=mrc_axis[int(map_maps)],
#                     fast='X',
#                     medium='Y',
#                     slow='Z',
#                     mtz_path=self.refmac_sfcalc_mtz,
#                     map_path=self.reference_map)
#                 pl.append([self.fft_process.process])
                
                #gemmi sf2map
                self.sf2map_process = sf2mapWrapper(
                    job_location=self.job_location,
                    command=self.commands['ccpem-gemmi'],
                    map_nx=map_nx,
                    map_ny=map_ny,
                    map_nz=map_nz,
                    # For now only use x,y,z
        #                 fast=mrc_axis[int(map_mapc)],
        #                 medium=mrc_axis[int(map_mapr)],
        #                 slow=mrc_axis[int(map_maps)],
                    fast='X',
                    medium='Y',
                    slow='Z',
                    mtz_path=self.refmac_sfcalc_mtz,
                    map_path=self.reference_map)
                pl.append([self.sf2map_process.process])
                
                #set origin and remove background
                self.reference_map_processed = os.path.join(
                        self.job_location,
                        model_map_filename+'_syn.mrc')
                if not all(o == 0. for o in (ori_x,ori_y,ori_z)):
                    self.mapprocess_wrapper = mapprocess_wrapper(
                                    job_location=self.job_location,
                                    command=self.commands['ccpem-mapprocess'],
                                    map_path=self.reference_map,
                                    list_process=['shift_origin','threshold'],
                                    map_origin=(ori_x,ori_y,ori_z),
                                    map_contour=0.02,
                                    #map_resolution=self.args.map_resolution_1.value,
                                    out_map=self.reference_map_processed
                                    )
                else:
                    self.mapprocess_wrapper = mapprocess_wrapper(
                                    job_location=self.job_location,
                                    command=self.commands['ccpem-mapprocess'],
                                    map_path=self.reference_map,
                                    list_process=['threshold'],
                                    map_origin=(ori_x,ori_y,ori_z),
                                    map_contour=0.02,
                                    #map_resolution=self.args.map_resolution_1.value,
                                    out_map=self.reference_map_processed
                                    )
                pl.append([self.mapprocess_wrapper.process])
                
                self.reference_map = self.reference_map_processed
                self.args.map_path_2.value = self.reference_map
                self.args.map_resolution_2.value = self.args.map_resolution_1.value
                self.map_or_pdb_selection_edit = 'Map'
        #check processed maps
        self.check_processed_maps()
        # Generate diffmap process
        self.tempy_difference_map = TEMPyDifferenceMap(
            job_location=self.job_location,
            command=self.commands['ccpem-diffmap'],
            map_path_1=self.map_edit_path,
            map_or_pdb=self.map_or_pdb_selection_edit,
            map_path_2=self.args.map_path_2.value,
            map_resolution_1=self.args.map_resolution_1.value,
            map_resolution_2=self.args.map_resolution_2.value,
            pdb_path = self.args.pdb_path.value,
            mode=self.args.mode_selection.value,
            noscale=self.args.noscale.value,
            nofilt=self.args.nofilt.value,
            maskfile=self.args.maskfile.value,
            mpi=self.args.use_mpi.value,
            n_mpi = self.args.n_mpi.value,
            window_size=self.args.window_size.value,
            threshold_fraction=self.args.threshold_fraction.value,
            dust_filter=self.args.dust_filter.value,
            fracmap=self.args.save_fracmap.value,
            dust_prob = self.args.dustprob.value,
            ref_scale = self.args.refscale.value)
            #map_alignment=self.args.map_alignment.value)
            #soft_mask=self.args.soft_mask.value)

        pl.append([self.tempy_difference_map.process])
#         #if model is given as input
#         if self.args.map_or_pdb_selection.value == "Model" and \
#             self.args.pdb_path.value != None:
#             # Generate getmapval process
#             #model-map
#             diff_frac2_map = os.path.join(
#                     self.job_location,'diff_frac2.mrc')
#             self.get_map_val = GetMapVal(
#                 job_location=self.job_location,
#                 command=self.commands['ccpem-mapval'],
#                 map_path_1=diff_frac2_map,
#                 map_resolution_1=self.args.map_resolution_1.value,
#                 pdb_path = self.args.pdb_path.value,
#                 name = 'Get model difference')
#             pl.append([self.get_map_val.process])
                
        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

class GetMapVal(object):
    '''
    Wrapper for TEMPy get_map_density and attribute as model b-factors.
    '''
    def __init__(self,
                 job_location,
                 command,
                 map_path_1,
                 map_resolution_1,
                 pdb_path=None,
                 name=None):
        assert pdb_path is not None
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.map_path_1 = ccpem_utils.get_path_abs(map_path_1)
        # Set args
        self.args = ['-m', self.map_path_1,
                     '-r', map_resolution_1]
        # PDB file
        self.pdb_path = ccpem_utils.get_path_abs(pdb_path)
        self.args += ['-p', self.pdb_path]
        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)


class TEMPyDifferenceMap(object):
    '''
    Wrapper for TEMPy difference map process.
    '''
    def __init__(self,
                 job_location,
                 command,
                 map_path_1,
                 map_resolution_1,
                 map_path_2=None,
                 map_resolution_2=None,
                 pdb_path=None,
                 map_or_pdb='Map',
                 mode='global',
                 nofilt=False,
                 noscale=False,
                 maskfile=None,
                 mpi=False,
                 n_mpi=1,
                 window_size=None,
                 threshold_fraction=None,
                 dust_filter=False,
                 fracmap=True,
                 contour_mask=False,
                 map_alignment=False,
                 map_contour_1=None,
                 map_contour_2=None,
                 dust_prob=None,
                 ref_scale = False,
                 name=None):
        
        assert [pdb_path, map_path_2].count(None) != 2
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.map_path_1 = ccpem_utils.get_path_abs(map_path_1)
        # Set args
        self.args = ['-m1', self.map_path_1,
                     '-r1', map_resolution_1]
        # Difference map 1 vs map 2 or PDB
        if map_or_pdb == 'Model':
            self.pdb_path = ccpem_utils.get_path_abs(pdb_path)
            self.args += ['-p', self.pdb_path]
        else:
            self.map_path_2 = ccpem_utils.get_path_abs(map_path_2)
            self.args += ['-m2', self.map_path_2]
            self.args += ['-r2', map_resolution_2]
        #scaling mode
        if mode in ['global','local']:
            self.args += ['-mode',mode]
        if noscale:
            self.args += ['--noscale']
        if maskfile is not None:
            self.args += ['--mask', maskfile]
        if nofilt:
            self.args += ['--nofilt']
        if ref_scale:
            self.args += ['--refscale']
        #
        if dust_filter:
            self.args += ['-dust']
        if not fracmap:
            self.args += ['--nofracmap']
        if threshold_fraction is not None:
            self.args += ['-tf',threshold_fraction]
        if window_size is not None:
            self.args += ['-w',str(window_size)]
        if dust_prob is not None:
            self.args += ['-dp',dust_prob]
        if mpi:
            self.args += ['-mpi']
            self.args += ['-n_mpi',n_mpi]
#         XXX TODO: Agnel to write code for alignment, may take some time!
#         if map_alignment:
#             self.args += ['-XXX', '???']

        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)

class mapprocess_wrapper():
    '''
    Wrapper for TEMPy Map Processing tool.
    '''
    def __init__(self,
                 job_location,
                 command,
                 map_path,
                 list_process,
                 map_resolution=None,
                 map_contour=None,
                 map_apix=None,
                 map_pad=None,
                 map_origin=None,
                 mask_path=None,
                 out_map=None,
                 name='MapProcess'):
        assert map_path is not None
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.map_path = ccpem_utils.get_path_abs(map_path)
        # Set args
        self.args = ['-m', self.map_path]
        if list_process is not None:
            list_process_args = ['-l']
            list_process_args.extend(list_process)
            self.args += list_process_args
        if map_resolution is not None:
            self.args += ['-r', map_resolution]
        #threshold
        if map_contour is not None:
            self.args += ['-t', map_contour]
        #new apix
        if map_apix is not None:
            self.args += ['-p', map_apix]
        #padding
        if map_pad is not None:
            pad_args = ['-pad']
            pad_args.extend(list(map_pad))
            if len(map_pad) > 0: self.args += pad_args#' '.join(
                                                    #[str(p) for p in map_pad])]
        #new origin
        if map_origin is not None:
            origin_args = ['-ori']
            origin_args.extend(map_origin)
            if len(map_origin) > 0: self.args += origin_args
        #mask file
        if mask_path is not None:
            self.args += ['-ma', mask_path]
        
        if out_map is not None:
            self.args += ['-out', out_map]
        
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)
