#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Run TEMPy:SMOC scoring process.
    Take input list of structures and score against single map.
    Output csv table of results
'''

import sys
import os, re
import json, glob
import pandas as pd
from ccpem_core.TEMPy.MapParser import MapParser
from ccpem_core.TEMPy.StructureParser import PDBParser, mmCIFParser
from TEMPy.RigidBodyParser import RBParser
from ccpem_core.TEMPy.ScoringFunctions import ScoringFunctions
from TEMPy.ShowPlot import Plot
from ccpem_core import ccpem_utils
from ccpem_core.tasks.tempy.smoc import smoc_results
from collections import OrderedDict
from TEMPy.mapprocess import Filter
import numpy as np
mrcfile_import=True
try:
    import mrcfile
except ImportError:
    mrcfile_import = False
    
def main(json_path, verbose=True, set_results=True):
    # Process arguments from json parameter file
    args = json.load(open(json_path, 'r'))
    map_path = args['map_path']
    map_resolution =  args['map_resolution']
    pdb_path_list = args['input_pdbs']
    rigid_body_path = args['rigid_body_path']
    rigid_body_dir = args['rigid_body_dir']
    job_location = args['job_location']
    syn_map_list = []
#     if args['use_refmac']:
#         if 'sim_maps' in args:
#             simmap = True
#             for smap in args['sim_maps']:
#                 if not os.path.exists(smap):
#                     simmap = False
#                     break
#             if simmap: syn_map_list = args['sim_maps']
#         if 'map_path_edit' in args:
#             try:
#                 assert os.path.isfile(args['map_path_edit'])
#                 map_path = args['map_path_edit']
#             except:
#                 pass

    distance_or_fragment_selection = args['dist_or_fragment_selection']
#     if args['use_smoc'] and args['auto_local_distance']:
#         local_distance = map_resolution
#     else: local_distance = args['local_distance']
    if args['use_smoc'] and args['auto_fragment_length']:
        if map_resolution < 2.5:
            fragment_length = 3
        elif map_resolution < 3.5:
            fragment_length = 5
        elif map_resolution < 5.0:
            fragment_length = 7
        else: fragment_length = 9
    else:
        fragment_length = args['fragment_length']
        
    if not distance_or_fragment_selection == 'Distance':
#         print 'Local distance for SMOC scoring: {}'.format(local_distance)
#     else:
        print '\nFragment length : {0}'.format(fragment_length)

    if job_location is None:
        job_location = os.getcwd() 

    if not isinstance(pdb_path_list, list):
        pdb_path_list = [pdb_path_list]

    #get rigid body file
    if not rigid_body_dir is None:
        if not os.path.isdir(rigid_body_dir): rigid_body_dir = None
        if map_resolution < 10.0:
            cutoff = 100
        elif map_resolution < 15.0:
            cutoff = 60
        elif map_resolution < 100.0:
            cutoff = 30
        list_cutoffs = []
        rigid_body_files = []
        for file in os.listdir(rigid_body_dir):
            if re.search("_denclust_[0-9]+\.txt",file) != None:
                rigid_body_files.append(file)

        for rf in rigid_body_files:
            basename = '.'.join(os.path.basename(rf).split('.')[:-1])
            basename = basename.split('_')[-1]
            try:
                list_cutoffs.append(int(basename))
            except TypeError: pass
        list_cutoffs.sort()
        cutoff_array = np.array(list_cutoffs)
        sel_indices = np.searchsorted(cutoff_array,cutoff) 
        if isinstance(sel_indices,int):
            sel_cutoff = ''
            if sel_indices >= len(cutoff_array):
                sel_indices = -1
            sel_cutoff = cutoff_array[sel_indices]
        else: 
            try: sel_cutoff = cutoff_array[sel_indices[0]]
            except IndexError: sel_cutoff = cutoff_array[-1]
        pdb_outdir = os.path.splitext(os.path.basename(pdb_path_list[0]))[0]
        rigid_body_path = \
            os.path.join(rigid_body_dir,
                         pdb_outdir+'_denclust_{}.txt'.format(sel_cutoff))
        if os.path.isfile(rigid_body_path):
            edit_argsfile(json_path,rigid_body_path)
        
    if args['use_smoc']:
        if verbose:
            ccpem_utils.print_sub_header('Process SMOC scores')
        # Run smoc score
        smoc_df = process_smoc_scores(
            map_path=map_path,
            map_resolution=map_resolution,
            pdb_path_list=pdb_path_list,
            rigid_body_path=rigid_body_path,
            fragment_length=fragment_length,
            directory=job_location,
            syn_map_list=syn_map_list,
            distance_or_fragment_selection=distance_or_fragment_selection)
            #local_distance=local_distance)
    
        # Save smoc scores as csv
        csv_path = os.path.join(job_location,
                                'smoc_score.csv')
        if verbose:
            ccpem_utils.print_sub_header('Save raw scores')
            ccpem_utils.print_sub_sub_header(csv_path)
        with open(csv_path, 'w') as path:
            smoc_df.to_csv(path)

        if set_results and not set_results == 'False':
            # Set jsrview
            smoc_results.SMOCResultsViewer(
                smoc_dataframe=smoc_df,
                directory=job_location)
    else:
        if verbose:
            ccpem_utils.print_sub_header('Process SCCC scores')
        # Run sccc score
        sccc_df = process_sccc_scores(
            map_path=map_path,
            map_resolution=map_resolution,
            pdb_path_list=pdb_path_list,
            rigid_body_path=rigid_body_path,
            fragment_length=fragment_length,
            directory=job_location)
        
        # Save smoc scores as csv
        csv_path = os.path.join(job_location,
                                'sccc_score.csv')
        if verbose:
            ccpem_utils.print_sub_header('Save raw scores')
            ccpem_utils.print_sub_sub_header(csv_path)
        with open(csv_path, 'w') as path:
            sccc_df.to_csv(path,sep=';')
        
        if set_results and not set_results == 'False':
            # Set jsrview
            smoc_results.SCCCResultsViewer(
                sccc_df,
                directory=job_location)

def edit_argsfile(args_file,rigid_body_file):
    with open(args_file,'r') as f:
        json_args = json.load(f)
        #set ribfind task output
        json_args['rigid_body_path'] = rigid_body_file
    with open(args_file,'w') as f:
        json.dump(json_args,f)


def process_smoc_scores(map_path,
                        map_resolution,
                        pdb_path_list,
                        rigid_body_path=None,
                        fragment_length=9,
                        directory=os.getcwd(),
                        syn_map_list=[],
                        distance_or_fragment_selection='Distance'):
                        #local_distance=5.0):
    mrcobj=mrcfile.open(map_path,mode='r',permissive=True)
    em_map = Filter(mrcobj)
    em_map.fix_origin()
    em_map.set_apix_tempy()
    #em_map = MapParser.readMRC(map_path)
    #list of sim_map objects
    syn_mapobj_list = []
    for synmapfile in syn_map_list:
        try:
            modelmap = read_mapfile(synmapfile)
            syn_mapobj_list.append(modelmap)
        except:
            syn_mapobj_list = []
            break
    # Get score for each structure in list
    smoc_frames = []
    residue_label = 'resnum'
    resname_label = 'resname'
    CAx_label = 'CAx'
    CAy_label = 'CAy'
    CAz_label = 'CAz'
    score_label = 'smoc'
    l = 1
    for pdb_path in pdb_path_list:
        pdb_id = os.path.splitext(os.path.basename(pdb_path))[0]+'_'+str(l)
        # Read structure
        if os.path.splitext(pdb_path)[-1].lower() in ['.cif','.mmcif']:
            structure = mmCIFParser.read_mmCIF_file(
                structure_id=pdb_id,
                filename=pdb_path,
                hetatm=False,
                water=False)
        else:
            try:
                structure = PDBParser.read_PDB_file(
                structure_id=pdb_id,
                filename=pdb_path,
                hetatm=True,
                water=False)
            except:
                structure = PDBParser.read_PDB_file(
                structure_id=pdb_id,
                filename=pdb_path,
                hetatm=False,
                water=False)
        # Get scores
        if len(syn_mapobj_list) > 0:
            modelmap = syn_mapobj_list[l-1]
            smoc_chain_scores, smoc_chain_CA = get_smoc_score(
                em_map=em_map,
                map_resolution=map_resolution,
                structure=structure,
                rigid_body_path=rigid_body_path,
                fragment_length=fragment_length,
                sim_map = modelmap,
                #local_distance=local_distance,
                distance_or_fragment_selection=distance_or_fragment_selection)
            
        else:
            smoc_chain_scores, smoc_chain_CA = get_smoc_score(
                em_map=em_map,
                map_resolution=map_resolution,
                structure=structure,
                rigid_body_path=rigid_body_path,
                fragment_length=fragment_length,
                #local_distance=local_distance,
                distance_or_fragment_selection=distance_or_fragment_selection)
        
         #set scores as b-factors for coloring
        scored_pdb_file = os.path.join(
            directory,pdb_id+'_smoc.pdb')
        try:
            ScoringFunctions().set_score_as_bfactor(
                structure_instance=structure,
                dict_scores=smoc_chain_scores,
                outfile=scored_pdb_file)
        except: 
            pass
        #save scores
        smoc_csv_file = pdb_id+'_smoc.csv'
        smocf = open(smoc_csv_file,'w')
        smocf.write(','.join(['#chain','residue','score','z_local','z_global'])+'\n')
        
        # Put residue scores into dataframe
        for chain, scores in smoc_chain_scores.iteritems():
            if chain.isspace():
                chainfix = '_'
            else:
                chainfix = '_' + chain
            list_resnum = scores.keys()
            list_resscore = scores.values()
            list_resname = []
            list_CAx = []
            list_CAy = []
            list_CAz = []
            for rnum in list_resnum:
                try: list_resname.append(smoc_chain_CA[chain][rnum][0])
                except (KeyError,TypeError) as e: 
                    list_resname.append(' ')
                try: list_CAx.append(smoc_chain_CA[chain][rnum][1])
                except (KeyError,TypeError) as e: 
                    list_CAx.append(0.0)
                try: list_CAy.append(smoc_chain_CA[chain][rnum][2])
                except (KeyError,TypeError) as e: 
                    list_CAy.append(0.0)
                try: list_CAz.append(smoc_chain_CA[chain][rnum][3])
                except (KeyError,TypeError) as e: 
                    list_CAz.append(0.0)
            dict_results = {}
            dict_results['residue'] = list_resnum
            dict_results['smoc'] = list_resscore
            dict_results['CAx'] = list_CAx
            dict_results['CAy'] = list_CAy
            dict_results['CAz'] = list_CAz
            outlier_bool, list_z_local, list_z_global = select_outliers_by_z(dict_results,'smoc',z_cutoff=-1.5)
            #save csv files
            save_smoc_csv_plot(smocf,pdb_id,chain,list_resnum,list_resscore,list_z_local,list_z_global)
            
            dict_smocdata = OrderedDict()
            dict_smocdata = {
                    (pdb_id+chainfix, residue_label):list_resnum, 
#                     (pdb_id+chainfix, resname_label): list_resname,
                    (pdb_id+chainfix, score_label):list_resscore ,
                    (pdb_id+chainfix, 'z_local'): list_z_local,
                    (pdb_id+chainfix, 'z_global'): list_z_global,
#                     (pdb_id+chainfix, CAz_label): list_CAz,
                            }
            df = pd.DataFrame(dict_smocdata)
            smoc_frames.append(df)
        l += 1
        smocf.close()
    return pd.concat(smoc_frames, axis=1)

def save_smoc_csv_plot(smocf,pdb_id,chain,list_resnum,list_score,list_z_local,list_z_global):
    plt = Plot()
    if len(list_resnum) < 5: return #skip ambiguities
    dict_points = {}
    for i in range(len(list_resnum)):
        smoc_res = [chain,str(list_resnum[i]),str(round(float(
                                            list_score[i]),3)),
                                            str(list_z_local[i]),
                                            str(list_z_global[i])]
        smocf.write(','.join(smoc_res)+'\n')
    dict_points[chain] = [list_resnum,list_score]
    chain_id_edit = chain
    if chain.islower():
        chain_id_edit = chain+'_l'
    smoc_plot = pdb_id+'_'+chain_id_edit+'_smocscore.png'
    plt.lineplot(dict_points,smoc_plot,xlabel='Residue_num',ylabel='SMOC',marker=False,lstyle=False,leg_pos=1.8)#,ylim=[-0.1,1.0])
            
def process_sccc_scores(map_path,
                        map_resolution,
                        pdb_path_list,
                        rigid_body_path,
                        fragment_length,
                        directory):
    em_map = MapParser.readMRC(map_path)
#     mrcobj=mrcfile.open(map_path,mode='r')
#     em_map = Filter(mrcobj)
#     em_map.fix_origin()
#     em_map.set_apix_tempy()
    # Get score for each structure in list
    smoc_frames = []

    dict_rbs = RBParser.read_rigid_body_file_as_dict(rigid_body_path)
    list_rbs = dict_rbs.keys()
    list_rigid_segs = []
    for rb in list_rbs:
        for ch in dict_rbs[rb]:
            list_segs = dict_rbs[rb][ch]
            list_seg_str = []
            ct_seg = 0
            for seg in list_segs:
                try:
                    seg_ch = [seg[0]+str(ch),seg[1]+str(ch)]
                except: continue
                seg_str = ' '.join(seg_ch)
                if ct_seg == 3:
                    seg_str += '\n'
                    ct_seg = 0
                ct_seg += 1
                list_seg_str.append(str(seg_str))
            list_rigid_segs.append(','.join(list_seg_str))
    dict_table = {}
    l = 1
    for pdb_path in pdb_path_list:
        pdb_id = os.path.splitext(os.path.basename(pdb_path))[0]+'_'+str(l)
        print 'Scoring', pdb_id
        # Read structure
        structure = PDBParser.read_PDB_file(
            structure_id=pdb_id,
            filename=pdb_path,
            hetatm=False,
            water=False)

        # Get scores
        sccc_chain_scores, rigid_body_scores = get_sccc_score(
            em_map=em_map,
            map_resolution=map_resolution,
            structure=structure,
            rigid_body_path=rigid_body_path)
        
         #set scores as b-factors for coloring
        scored_pdb_file = os.path.join(
            directory,pdb_id+'_sc.pdb')
        try:
            ScoringFunctions().set_score_as_bfactor(
                structure_instance=structure,
                dict_scores=sccc_chain_scores,
                outfile=scored_pdb_file)
        except: 
            pass
        
        l += 1
        # make a dict of scores
        list_scores = []
        for rb in list_rbs:
            try: 
                list_scores.append(rigid_body_scores[rb])
            except KeyError: 
                list_scores.append('-')
        dict_table[pdb_id] = list_scores
    df = pd.DataFrame(dict_table,index = list_rigid_segs)
    return df


def get_smoc_score(em_map,
                   map_resolution,
                   structure,
                   rigid_body_path=None,
                   fragment_length=9,
                   sigma_coeff=0.225,
                   sim_map=None,
                   distance_or_fragment_selection='Distance'):
                   #local_distance=5.0):
    '''
    Score window (number residues)
    Sigma coeff
    Returns chain scores dictionary
    '''
    if distance_or_fragment_selection == 'Distance':
        fragment_score=False
    else: fragment_score=True
    # Calculate SMOC scores
    score_class = ScoringFunctions()
    chain_scores, chain_residues, chain_ca = score_class.SMOC(
        map_target=em_map,
        resolution_densMap=map_resolution,
        structure_instance=structure,
        win=fragment_length,
        rigid_body_file=rigid_body_path,
        sigma_map=sigma_coeff,
        fragment_score=fragment_score,
        sim_map=sim_map,
        #dist=local_distance,
        sigma_thr=2.0,
        calc_metric='smoc',
        get_coord=True)
    return chain_scores, chain_ca

def get_sccc_score(em_map,
                   map_resolution,
                   structure,
                   rigid_body_path=None,
                   sigma_coeff=0.225):
    
    # Calculate SMOC scores
    score_class = ScoringFunctions()
    chain_scores, rigid_body_scores = score_class.get_sccc(
        map_target=em_map,
        resolution=map_resolution,
        structure_instance=structure,
        rigid_body_file=rigid_body_path,
        sigma_map=sigma_coeff)
    return chain_scores, rigid_body_scores

def read_mapfile(map_path):
    if mrcfile_import:
        mrcobj=mrcfile.open(map_path,mode='r')
        emmap = Filter(mrcobj)
        emmap.fix_origin()
        emmap.set_apix_tempy()
    else:
        mrcobj=MapParser.readMRC(map_path)
        emmap = Filter(mrcobj)
        emmap.fix_origin()
    return emmap


def import_smoc_scores_from_csv(path):
    return pd.read_csv(path, index_col=0)

def select_outliers_by_z(dict_results, process_name, z_cutoff = -2.5):
    list_smoc = dict_results[process_name]
    array_smoc = np.array(list_smoc)
    #use z = 0 for a very narrow distribution
    if np.median(list_smoc) > 0.6 and np.std(list_smoc) < 0.05:
        list_z_global = [0.0]*len(list_smoc)
        #print list_z_global
    else:
        list_z_global = calc_z_median(list_smoc)
    #print len(list_smoc),len(list_z_global)
    #if coords are available, set z score locally
    if 'CAx' in dict_results and 'CAy' in dict_results and \
        'CAz' in dict_results:
        indi = zip(dict_results['CAx'], dict_results['CAy'], dict_results['CAz'])
        try: 
            from scipy.spatial import cKDTree
            gridtree = cKDTree(indi)
        except ImportError:
            try:
                from scipy.spatial import KDTree
                gridtree = KDTree(indi)
            except ImportError: 
                gridtree = None
        list_z_local = []
        if gridtree is not None:
            list_z = []
            for l in xrange(len(list_smoc)):
                ca_coord = indi[l]
                val = list_smoc[l]
                list_neigh = get_indices_sphere(gridtree,ca_coord,dist=12.0)
                list_smoc_local = array_smoc[list_neigh]
                if np.median(list_smoc_local) > 0.6 and np.std(list_smoc_local) < 0.05:
                    z_local = 0.0
                else:
                    z_local = calc_z_median(list_smoc_local,val)
                #res_index = list_neigh.index(l)
                list_z_local.append(z_local)
                list_z.append(z_local < z_cutoff)
            
    else:
        list_z_local = [0.]*len(list_z_global) #when no local z is calculated
        z = calc_z_median(array_smoc)
        list_z = z < z_cutoff
    #print len(list_z),len(list_z_local),len(list_z_global)
    #print list_z, list_z_local, list_z_global
    return list_z, list_z_local, list_z_global

def calc_z(list_vals,val=None):
    if val is not None:
        try:
            list_vals.remove(val) #remove val from list
        except: pass
    sum_mapval = np.sum(list_vals)
    mean_mapval = sum_mapval/len(list_vals)
    #median_mapval = np.median(list_vals)
    std_mapval = np.std(list_vals)
    if val is None:
        if std_mapval == 0.:
            z = [0.0]*len(list_vals)
        else:
            z = np.round((np.array(list_vals) - mean_mapval)/std_mapval,2)
    else:
        if std_mapval == 0.:
            z = 0.0
        else:
            z = (val - mean_mapval)/std_mapval
    return z
 
def calc_z_median(list_vals,val=None):
    if val is not None:
        try:
            list_vals.remove(val)
        except: pass
    sum_smoc = np.sum(list_vals)
    median_smoc = np.median(list_vals)
    mad_smoc = np.median(np.absolute(np.array(list_vals)-median_smoc))
    if val is None:
        if mad_smoc == 0.:
            z = [0.0]*len(list_vals)
        else:
            z = np.around((np.array(list_vals) - median_smoc)/mad_smoc,2)
    else:
        if mad_smoc == 0.:
            z = 0.0
        else:
            z = round(((val - median_smoc)/mad_smoc),2)
    return z    

def get_indices_sphere(gridtree,coord,dist=5.0):
    list_points = gridtree.query_ball_point(\
                [coord[0],coord[1],coord[2]], 
                dist)
    return list_points


if __name__ == '__main__':
    if len(sys.argv) <= 1:
        main(json_path='./test_data/unittest_args.json')
#         print 'Please supply json parameter file'
    elif len(sys.argv) > 2:
        main(json_path=sys.argv[1], set_results=sys.argv[2])
    else:
        main(json_path=sys.argv[1])
