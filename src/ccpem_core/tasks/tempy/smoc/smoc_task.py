#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
import json
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.tasks.tempy.smoc import smoc_process
from ccpem_core.process_manager import job_register
from ccpem_core.tasks.ribfind import ribfind_task
from ccpem_core import settings
from ccpem_core.tasks.refmac import refmac_task
from ccpem_core.tasks.model2map.model2map_task import sf2mapWrapper, fix_map_model_refmac
from ccpem_core.tasks.bin_wrappers import fft
import mrcfile
from ccpem_core.model_tools.gemmi_utils import shift_coordinates, remove_atomic_charges
from ccpem_core.map_tools.TEMPy import map_preprocess

class SMOC(task_utils.CCPEMTask):
    '''
    CCPEM / TEMPy difference map wrapper.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='TEMPy LocScore',
        author='A. Joseph, M. Topf',
        version='1.1',
        description=(
            '''Local scoring using TEMPy library.<br><br>
            Input a map and one or more atomic models. The atomic models
            are expected to be fitted in the map.<br><br>
            Two methods are provided to evaluate atomic model fit.<br>
            SMOC: Local fragment Score based on Manders Overlap 
            Coefficient. Scores are calculated either on <br>
            - a local region within a distance from each amino acid or<br>
            - on overlapping fragments of amino acids along each chain.<br> 
            Distance or Fragment length can be adjusted (default setting 
            is based on map resolution).<br>
            SCCC: Cross correlation coefficient for each rigid body 
            segment. Each rigid body segment defined in the rigid body 
            file is scored. If a rigid body file is not provided, it is 
            calculated using RIBFIND. NOTE that the current implementation
            of RIBFIND doesnt support multiple chains, so a rigid body 
            file has to be uploaded for the calculation to work.<br>
            Rigid body file format (two lines):<br>
            10:A 20:A<br>
            30:B 40:B 60:B 70:B<br>
            indicates two rigid bodies, one formed of residues 10 
            to 20 from chain A, and another formed of residues 30 to 40 
            and 60 to 70 of chain B.<br><br>
            The choice of the scoring method is based on map resolution 
            by default. If resolution is better than 7.5A, SMOC score 
            is calculated.
            '''),
        short_description=(
            'SMOC: Local fragment Score based on Manders\' Overlap '
            'Coefficient\n'
            'SCCC: Cross correlation coefficient for each rigid body segment'),
        documentation_link='http://topf-group.ismb.lon.ac.uk/TEMPY.html',
        references=None)

    commands = {'ccpem-python':
        ['ccpem-python', os.path.realpath(smoc_process.__file__)],
        'ccpem-ribfind': settings.which(
                        'ccpem-ribfind'),
        'refmac': settings.which(program='refmac5'),
        'pdbset':  settings.which(program='pdbset'),
        'ccpem-gemmi':settings.which(program='gemmi'),
        'ccpem-mapprocess':
        ['ccpem-python', os.path.realpath(map_preprocess.__file__)]}


    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        #
        super(SMOC, self).__init__(
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        map_path = parser.add_argument_group()
        map_path.add_argument(
            '-map_path',
            '--map_path',
            help='Input map (mrc format)',
            metavar='Input map',
            type=str,
            default=None)
        map_path.add_argument(
            '-map_path_edit',
            '--map_path_edit',
            help='Edited map (mrc format)',
            metavar='Edited input map',
            type=str,
            default=None)

        #
        parser.add_argument(
            '-input_pdbs',
            '--input_pdbs',
            help=('Input pdb/cif(s)'
                  'ensure same residue numbering in all'),
            metavar='Input atomic models',
            type=str,
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-input_pdb_chains',
            '--input_pdb_chains',
            help='Input PDB chain(s)',
            metavar='Input PDB chain selections',
            type=str,
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-sim_maps',
            '--sim_maps',
            help='Synthetic maps from models, to score against map',
            metavar='Input synthetic maps',
            type=str,
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-use_refmac',
            '--use_refmac',
            help=('Use Refmac to make map from model. Ensure atomic B-factors are refined.'),
            metavar='Use Refmac to simulate map from model?',
            type=bool,
            default=False)
        #ligand dictionary for Refmac
        parser.add_argument(
            '-lib_in',
            '--lib_in',
            help='Ligand dictionary for Refmac (cif format)',
            metavar='Ligand dictionary',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-map_resolution',
            '--map_resolution',
            help=('Resolution of map (Angstrom). Maximum resolution recommended '
                  'for use of SMOC is 7.5 Angstrom'),
            metavar='Map resolution',
            type=float,
            default=None)

        self.add_basic_arguments(parser)
        self.add_advanced_arguments(parser)
        return parser
    
    def add_basic_arguments(self,parser):
        #
        parser.add_argument(
            '-use_smoc',
            '--use_smoc',
            help= ('''SMOC: calculate Segment Manders\' Overlap Coefficient  \n''' \
                   ''' Scores overlapping fragments and outputs per residue plots'''),
            metavar='SMOC score',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-use_sccc',
            '--use_sccc',
            help= ('''SCCC: calculate Segment Cross Correlation Coefficient \n''' \
                   ''' Scores rigid body fits and outputs score per rigid body'''),
            metavar='SCCC score',
            type=bool,
            default=False)
        
        parser.add_argument(
            '-rigid_body_path',
            '--rigid_body_path',
            help=('Input rigid body file. '
                  'Can be used with SMOC as well'),
            metavar='Input rigid body',
            type=str,
            nargs='*',
            default=None)
        
        parser.add_argument(
            '-rigid_body_dir',
            '--rigid_body_dir',
            help=('Dir with RIBFIND output files'),
            metavar='Ribfind output dir',
            type=str,
            default=None)
        #
        ribfind_cutoff = parser.add_argument_group()
        ribfind_cutoff.add_argument(
            '-ribfind_cutoff',
            '--ribfind_cutoff',
            help=('Set Ribfind cutoff for rigid body elements.  Default '
                  'of 100 corresponds to one rigid body per secondary '
                  'structure element'),
            metavar='Ribfind cutoff',
            type=int,
            default=100)
        #
        create_rigid_body = parser.add_argument_group()
        create_rigid_body.add_argument(
            '-create_rigid_body',
            '--create_rigid_body',
            help=('Generate rigid bodies based on '
                  'secondary structure elements'),
            metavar='Auto rigid body',
            type=bool,
            default=False)
        
    def add_advanced_arguments(self,parser):
        
        dist_or_fragment_selection = parser.add_argument_group()
        dist_or_fragment_selection.add_argument(
            '-dist_or_fragment_selection',
            '--dist_or_fragment_selection',
            help=('''Calculate SMOC score on neighboring voxels 
                    selected by: distance or sequence fragment length'''),
            metavar='''Residue neighborhood''',
            choices=['Distance', 'Fragment'],
            type=str,
            default='Distance')

        parser.add_argument(
            '-local_distance',
            '--local_distance',
            help='''Distance over which SMOC score is calculated 
                    for each residue (Angstroms)''',
            metavar='Local distance for scoring',
            type=float,
            default=5.0)

        parser.add_argument(
            '-auto_local_distance',
            '--auto_local_distance',
            help='''Automatically set distance for SMOC score calculation
                    for each residue''',
            metavar='Auto distance (SMOC)',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-auto_fragment_length',
            '--auto_fragment_length',
            help='''Automatically set number of residues in averaging window''',
            metavar='Auto window (SMOC)',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-fragment_length',
            '--fragment_length',
            help='''Number of resiudes in averaging window''',
            metavar='Fragment length',
            type=int,
            default=9)
        
    
    def ribfind_job(self, db_inject=None, job_title=None):
        # Set 
        if self.database_path is None:
            path = os.path.dirname(self.job_location)
        else:
            path = os.path.dirname(self.database_path)

        job_id, job_location = job_register.job_register(
            db_inject=db_inject,
            path=path,
            task_name=ribfind_task.Ribfind.task_info.name)

        # Set task args
        args = ribfind_task.Ribfind().args
        args.job_title.value = job_title
        pdbs = self.args.input_pdbs()
        if not isinstance(pdbs, list):
            pdbs = [pdbs]
        args.input_pdb.value = pdbs[0]
        args_json_string = args.output_args_as_json(
            return_string=True)

        # Set process args
        process_args = ['--no-gui']
        process_args += ["--args_string='{0}'".format(
            args_json_string)]
        process_args += ['--job_location={0}'.format(
            job_location)]
        if self.database_path is not None:
            process_args += ['--project_location={0}'.format(
                os.path.dirname(self.database_path))]
        if job_id is not None:
            process_args += ['--job_id={0}'.format(
                job_id)]

        # Create process
        self.ribfind_process = process_manager.CCPEMProcess(
            name='Ribfind auto run task',
            command=self.commands['ccpem-ribfind'],
            args=process_args,
            location=job_location,
            stdin=None)

    def edit_argsfile(self,args_file):
        with open(args_file,'r') as f:
            json_args = json.load(f)
            #set ribfind task output
            json_args['rigid_body_path'] = self.args.rigid_body_path.value
            json_args['rigid_body_dir'] = self.args.rigid_body_dir.value
#             #set mapprocess task output
#             if self.mapprocess_task is not None:
#                 mapprocess_job_location = self.mapprocess_task.job_location
#                 #the processed map is written to the same dir as input map
#                 #the job location has other output files
#                 if mapprocess_job_location is not None:
#                     processed_map_path = os.path.splitext(
#                                         os.path.abspath(self.args.map_path.value))[0] \
#                                         +'_processed.mrc'
#                     #print processed_map_path
#                     if os.path.isfile(processed_map_path):
#                         json_args['map_path'] = processed_map_path
#                         self.unprocessed_map = self.args.map_path.value
#                         self.args.map_path.value = processed_map_path
        #add synthetic maps as an argument
        if self.args.use_refmac.value:
            json_args['map_path_edit'] = self.args.map_edit_path
            if len(self.args.sim_maps) == len(self.args.pdb_path_list):
                json_args['sim_maps'] = self.args.sim_maps
                    
        with open(args_file,'w') as f:
            json.dump(json_args,f)
            
    def check_processed_map(self):
        processed_map_path = os.path.splitext(
                            os.path.basename(self.args.map_path.value))[0] \
                            +'_processed.mrc'
        #print processed_map_path
        if os.path.isfile(processed_map_path) and \
            self.mapprocess_task is not None:
            self.map_input.value_line.setText(processed_map_path)
        

    def fix_model_for_refmac(self,pdb_path,pdb_edit_path,trans_vector,
                             remove_charges=False):
        shift_coordinates(pdb_path,pdb_edit_path,trans_vector,remove_charges)

    def run_pipeline(self, job_id=None, db_inject=None):
        
        pl = []
        
        if self.args.use_sccc.value and self.args.rigid_body_path.value is None : ##self.args.map_resolution.value > 7.5
            ribfind_job_title = 'Auto run'
            if job_id is not None:
                ribfind_job_title += ' {0}'.format(
                    job_id)
            self.ribfind_job(db_inject=db_inject,
                             job_title=ribfind_job_title)
            pl.append([self.ribfind_process])
            
            # Set rigid body path
            # TODO: set Cutoffs based on resolution
            if self.args.map_resolution.value < 10.0:
                cutoff = 100
            elif self.args.map_resolution.value < 15.0:
                cutoff = 60
            elif self.args.map_resolution.value < 100.0:
                cutoff = 30
            self.args.ribfind_cutoff.value = cutoff
#             self.args.rigid_body_path.value = os.path.join(
#                 self.ribfind_process.location,
#                 'rigid_body_{0:0>3}.txt'.format(
#                     self.args.ribfind_cutoff.value))
            pdbs = self.args.input_pdbs()
            if not isinstance(pdbs, list):
                pdbs = [pdbs]
            pdb = pdbs[0]
            pdb_outdir = os.path.splitext(os.path.basename(pdb))[0]
            self.args.rigid_body_dir.value = \
                os.path.join(self.ribfind_process.location,
                             pdb_outdir,'protein')
        # Set args
        args_file = os.path.join(self.job_location,
                                 'args.json')
        fix_map_model = True
        self.args.map_edit_path = self.args.map_path.value
        self.args.map_edit_path, fix_map_model, cella, self.origin_vector, self.map_dim, self.nstart_ori = fix_map_model_refmac(
                                        self.args.map_path.value,self.args.map_edit_path,
                                        self.job_location)
        #use Refmac for model map conversion
        if self.args.use_refmac.value:
            self.args.sim_maps = []
            self.args.map_edit_path = self.args.map_path.value
            fix_map_model = True
            with mrcfile.open(self.args.map_path.value, mode='r',permissive=True) as mrc:
                map_nx = mrc.header.nx
                map_ny = mrc.header.ny
                map_nz = mrc.header.nz
                map_nxstart = mrc.header.nxstart
                map_nystart = mrc.header.nystart
                map_nzstart = mrc.header.nzstart
                map_mapc = mrc.header.mapc
                map_mapr = mrc.header.mapr
                map_maps = mrc.header.maps
                ori_x = mrc.header.origin.x
                ori_y = mrc.header.origin.y
                ori_z = mrc.header.origin.z
                cella = (mrc.header.cella.x,mrc.header.cella.y,
                         mrc.header.cella.z)
                apix = mrc.voxel_size.item()
                
            #fix map and model when nstart is non-zero
            if ori_x == 0. and ori_y == 0. and ori_z == 0.:
                if map_nxstart != 0 or map_nystart != 0 or map_nzstart != 0:
                    map_edit = os.path.splitext(
                        os.path.basename(self.args.map_path.value))[0]+'_fix.mrc'
                    self.args.map_edit_path = os.path.join(
                                    self.job_location,map_edit)
                    # fix origin to agree with nstart
                    shutil.copyfile(self.args.map_path.value,self.map_edit_path)
                    assert os.path.isfile(self.map_edit_path)
                    with mrcfile.open(self.map_edit_path,'r+') as mrc:
                        mrc.header.origin.x = map_nxstart*apix[0]
                        mrc.header.origin.y = map_nystart*apix[1]
                        mrc.header.origin.z = map_nzstart*apix[2]
                        
                    ori_x = map_nxstart*apix[0]
                    ori_y = map_nystart*apix[1]
                    ori_z = map_nzstart*apix[2]
                    ##fix_map_model = False
                    
            self.args.pdb_path_list = self.args.input_pdbs.value
            if not isinstance(self.args.pdb_path_list, list):
                self.args.pdb_path_list = [self.args.pdb_path_list]
            ct_pdb = 1
            for pdb_path in self.args.pdb_path_list:
                pdb_edit_path = pdb_path
                model_map_filename = os.path.splitext(
                        os.path.basename(pdb_path))[0]+'_'+str(ct_pdb)
                reference_map = os.path.join(
                    self.job_location,
                    model_map_filename+'_refmac.mrc')
                if len(model_map_filename) > 10:
                    modelid = model_map_filename[:5]+'_'+model_map_filename[-5:]
                else:
                    modelid = model_map_filename
                    
                pdb_edit_path = os.path.join(self.job_location,
                                        model_map_filename+'_fix.pdb')
                ct_pdb += 1
                #translate model to origin 0
                if fix_map_model and not all(o == 0. for o in (ori_x,ori_y,ori_z)):
                    
                    trans_vector = [-ori_x,
                                    -ori_y,
                                    -ori_z]
                    self.fix_model_for_refmac(pdb_path,pdb_edit_path,trans_vector,
                                              remove_charges=True)
                else:
                    #remove charges
                    remove_atomic_charges(pdb_path,
                                      pdb_edit_path)
                # Set PDB cryst from mtz
                self.process_set_unit_cell = refmac_task.SetModelCell(
                    job_location=self.job_location,
                    name='Set model unit cell '+modelid,
                    pdb_path=pdb_edit_path,
                    map_path=self.args.map_edit_path)
                pl.append([self.process_set_unit_cell.process])
    
                self.pdb_set_path = os.path.join(
                    self.job_location,
                    'pdbset.pdb')
                # Calculate mtz from pdb 
                self.refmac_sfcalc_crd_process = refmac_task.RefmacSfcalcCrd(
                    job_location=self.job_location,
                    pdb_path=self.pdb_set_path,
                    name='RefmacSfcalcCrd '+modelid,
                    lib_in=self.args.lib_in(),
                    resolution=self.args.map_resolution.value)
                self.refmac_sfcalc_mtz = os.path.join(
                    self.job_location,
                    'sfcalc_from_crd.mtz')
                pl.append([self.refmac_sfcalc_crd_process.process])

                #gemmi sf2map
                self.sf2map_process = sf2mapWrapper(
                    job_location=self.job_location,
                    command=self.commands['ccpem-gemmi'],
                    map_nx=map_nx,
                    map_ny=map_ny,
                    map_nz=map_nz,
                    # For now only use x,y,z
        #                 fast=mrc_axis[int(map_mapc)],
        #                 medium=mrc_axis[int(map_mapr)],
        #                 slow=mrc_axis[int(map_maps)],
                    fast='X',
                    medium='Y',
                    slow='Z',
                    mtz_path=self.refmac_sfcalc_mtz,
                    map_path=reference_map)
                pl.append([self.sf2map_process.process])
                
#                 # Convert calc mtz to map
#                 self.fft_process = fft.FFT(
#                     job_location=self.job_location,
#                     map_nx=map_nx,
#                     map_ny=map_ny,
#                     map_nz=map_nz,
#                     # For now only use x,y,z
#     #                 fast=mrc_axis[int(map_mapc)],
#     #                 medium=mrc_axis[int(map_mapr)],
#     #                 slow=mrc_axis[int(map_maps)],
#                     fast='X',
#                     medium='Y',
#                     slow='Z',
#                     mtz_path=self.refmac_sfcalc_mtz,
#                     name='FFT '+modelid,
#                     map_path=reference_map
#                     )
#                 pl.append([self.fft_process.process])
                 
                #set origin and remove background
                reference_map_processed = os.path.join(
                        self.job_location,
                        model_map_filename+'_syn.mrc')
                if not all(o == 0. for o in (ori_x,ori_y,ori_z)):
                    self.mapprocess_wrapper = mapprocess_wrapper(
                                    job_location=self.job_location,
                                    command=self.commands['ccpem-mapprocess'],
                                    map_path=reference_map,
                                    list_process=['shift_origin','threshold'],
                                    map_origin=(ori_x,ori_y,ori_z),
                                    map_contour=0.02,
                                    #map_resolution=self.args.map_resolution.value,
                                    out_map=reference_map_processed,
                                    name='MapProcess '+modelid
                                    )
                else:
                    self.mapprocess_wrapper = mapprocess_wrapper(
                                    job_location=self.job_location,
                                    command=self.commands['ccpem-mapprocess'],
                                    map_path=reference_map,
                                    list_process=['shiftpeak_to_zero','threshold'],
                                    map_origin=(ori_x,ori_y,ori_z),
                                    map_contour=0.02,
                                    #map_resolution=self.args.map_resolution.value,
                                    out_map=reference_map_processed,
                                    name='MapProcess '+modelid
                                    )
                pl.append([self.mapprocess_wrapper.process])
                self.args.sim_maps.append(reference_map_processed)

#         self.check_processed_map()
#         if self.args.rigid_body_path.value is not None:
        self.edit_argsfile(args_file)

        # Generate process
        if self.args.use_smoc.value:
            self.smoc_wrapper = SMOCWrapper(
                command=self.commands['ccpem-python'],
                job_location=self.job_location,
                name='SMOC score')
        else:
            self.smoc_wrapper = SMOCWrapper(
                command=self.commands['ccpem-python'],
                job_location=self.job_location,
                name='SCCC score')

        if len(pl) == 0:
            pl = [[self.smoc_wrapper.process]]
        else:
            pl.append([self.smoc_wrapper.process])
        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

class SMOCWrapper(object):
    '''
    Wrapper for TEMPy SMOC process.
    '''
    def __init__(self,
                 command,
                 job_location,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__

        # Set args
        self.args = os.path.join(self.job_location,
                                 'args.json')
        
        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)

class mapprocess_wrapper():
    '''
    Wrapper for TEMPy Map Processing tool.
    '''
    def __init__(self,
                 job_location,
                 command,
                 map_path,
                 list_process,
                 map_resolution=None,
                 map_contour=None,
                 map_apix=None,
                 map_pad=None,
                 map_origin=None,
                 mask_path=None,
                 out_map=None,
                 name='MapProcess'):
        assert map_path is not None
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.map_path = ccpem_utils.get_path_abs(map_path)
        # Set args
        self.args = ['-m', self.map_path]
        if list_process is not None:
            list_process_args = ['-l']
            list_process_args.extend(list_process)
            self.args += list_process_args
        if map_resolution is not None:
            self.args += ['-r', map_resolution]
        #threshold
        if map_contour is not None:
            self.args += ['-t', map_contour]
        #new apix
        if map_apix is not None:
            self.args += ['-p', map_apix]
        #padding
        if map_pad is not None:
            pad_args = ['-pad']
            pad_args.extend(list(map_pad))
            if len(map_pad) > 0: self.args += pad_args#' '.join(
                                                    #[str(p) for p in map_pad])]
        #new origin
        if map_origin is not None:
            origin_args = ['-ori']
            origin_args.extend(map_origin)
            if len(map_origin) > 0: self.args += origin_args
        #mask file
        if mask_path is not None:
            self.args += ['-ma', mask_path]
        
        if out_map is not None:
            self.args += ['-out', out_map]
        
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)
