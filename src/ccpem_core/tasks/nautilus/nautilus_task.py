#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import os
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core.settings import which
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.tasks.refmac import refmac_task
from ccpem_core.tasks.buccaneer import buccaneer_task
from ccpem_core.tasks.nautilus import nautilus_results
from ccpem_core.tasks.buccaneer.buccaneer_utils import shiftback_models

class Nautilus(task_utils.CCPEMTask):
    '''
    CCPEM Nautilus task.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='Nautilus',
        author='Hoh SW, Cowtan K',
        version='0.5.4',
        description=(
            'Nautilus performs automated building of RNA/DNA from electron '
            'density.\n'
            'Nautilus does not currently perform refinement - you will need '
            'to refine and recycle for further model building yourself. '
            'N.B. requires CCP4.'),
        short_description=(
            'Automated model building.  Requires CCP4'),
        documentation_link='http://www.ccp4.ac.uk/html/cnautilus.html',
        references=None)

    commands = {'refmac': which(program='refmac5'),
                'cnautilus': which(program='cnautilus'),
                'sftools': which(program='sftools'),
                'freerflag': which(program='freerflag'),
                'libg': which(program='libg'),
                'ccpem-python': which(program='ccpem-python'),
                'emda':which(program='emda')
                }

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 verbose=False,
                 parent=None):
        # No longer need to set atomsf since refmac job defaults to use
        # "source EM MB" instead.
        #        self.atomsf = os.path.join(
        #            os.environ['CLIBD'],
        #            'atomsf_electron.lib')

        super(Nautilus, self).__init__(
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            verbose=verbose,
            parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-mapin',
            '--input_map',
            help='''Target input map (mrc format)''',
            type=str,
            metavar='Input map',
            default=None)
        #
        parser.add_argument(
            '-resolution',
            '--resolution',
            help='''Resolution of input map (Angstrom)''',
            metavar='Resolution',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-input_seq',
            '--input_seq',
            help='Input sequence file in any common format (e.g. pir, fasta)',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-extend_pdb',
            '--extend_pdb',
            help='Initial PDB model to extend (pdb format)',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-ncycle',
            '--ncycle',
            help='Number of Nautilus pipeline cycles',
            metavar='Build cycles',
            type=int,
            default=5)
        #
        parser.add_argument(
            '-ncycle_nau1st',
            '--ncycle_nau1st',
            help='Number of Nautilus cycles in 1st pipeline cycle',
            metavar='1st Nautilus cycles',
            type=int,
            default=5)
        #
        parser.add_argument(
            '-ncycle_naunth',
            '--ncycle_naunth',
            help='Number of Nautilus cycles in subsequent pipeline cycle',
            metavar='N-th Nautilus cycles',
            type=int,
            default=5)
        #
        parser.add_argument(
            '-ncycle_refmac',
            '--ncycle_refmac',
            help='Number of refmac cycles',
            metavar='Refine cycles',
            type=int,
            default=20)
        #
        parser.add_argument(
            '-map_sharpen',
            '--map_sharpen',
            metavar='Sharpen / blur',
            help=('B-factor to apply to map. Negative B-factor to '
                  'sharpen, positive to blur, zero to leave map as input'),
            type=float,
            default=0)
        #
        parser.add_argument(
            '-lib_in',
            '--lib_in',
            help='Library dictionary file for ligands (lib/cif format)',
            metavar='Ligand dictionary',
            type=str,
            default=None)
        # added
        parser.add_argument(
            '-masked_refinement_on',
            '--masked_refinement_on',
            help=('Trim map around supplied model file '
                  'and perform refinement with this sub-volume.'),
            metavar='Masked refinement',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-mask_radius',
            '--mask_radius',
            help='Distance around molecule the map should be cut.',
            type=float,
            default=3.0)
        #
        parser.add_argument(
            '-half_map_refinement',
            '--half_map_refinement',
            help='Use half map refinement, if false use full map instead',
            type=bool,
            metavar='Half map refinement',
            default=False)
        #
        parser.add_argument(
            '-half1',
            '--half_map_1',
            help='Half map 1 of input map (mrc format)',
            metavar='Half map 1',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-half2',
            '--half_map_2',
            help='Half map 2 of input map (mrc format)',
            type=str,
            metavar='Half map 2',
            default=None)
        #
        parser.add_argument(
            '-build_from_nemap',
            '--build_from_nemap',
            help=('Build model using normalized expected map calculated '
                  'from two halfmaps using Servalcat.'),
            type=bool,
            metavar='Build from nemap',
            default=False)
        #
        parser.add_argument(
            '-input_mask',
            '--input_mask',
            help="Input mask for sfcalc.",
            type=str,
            metavar='Input mask',
            default=None)
        #
        parser.add_argument(
            '-mask_for_fofc',
            '--mask_for_fofc',
            help=('If True, input mask will be mask for difference '
                  'map calculation (mrc format) '
                  'No mask will be used when set to False'),
            type=bool,
            metavar='Mask for Fo-Fc map',
            default=False)
        #
        parser.add_argument(
            '-no_trim',
            '--no_trim',
            help=('If True, will not trim input maps before '
                  'proceeding down the pipeline.'),
            type=bool,
            metavar='No trim',
            default=False
        )
        #
        parser.add_argument(
            '-freer_on',
            '--freer_on',
            help=('Turn on Free R flags to use in Nautilus.'),
            type=bool,
            metavar='FreerR on',
            default=True
        )
        #
        parser.add_argument(
            '-keywords',
            '--keywords',
            help='Nautilus keywords for advanced options.',
            type=str,
            metavar='Keywords',
            default='')
        #
        parser.add_argument(
            '-refmac_keywords',
            '--refmac_keywords',
            help='REFMAC keywords for advanced options.',
            type=str,
            metavar='Refmac Keywords',
            default='')
        return parser

    def run_pipeline(self, job_id=None, run=True, db_inject=None):
        '''
        Generate job classes and process.  Run=false for reloading.
        '''
        # Convert map to mtz (refmac)
        # set F columns label suffix
        pl = []
        
        bfactor = self.args.map_sharpen()
        if bfactor >= 0:
            sharp_array = None
            blur_array = [bfactor]
            if bfactor == 0:
                label_out_suffix = '0'
            else:
                label_out_suffix = 'Blur_{0:.2f}'.format(
                    self.args.map_sharpen.value)
        else:
            sharp_array = [-1 * bfactor]
            blur_array = None
            label_out_suffix = 'Sharp_{0:.2f}'.format(
                -1 * self.args.map_sharpen.value)

        # self.process_maptomtz = refmac_task.RefmacMapToMtz(
        #     command=self.commands['refmac'],
        #     resolution=self.args.resolution.value,
        #     mode='Global',
        #     name='Map to MTZ',
        #     job_location=self.job_location,
        #     map_path=self.args.input_map.value,
        #     blur_array=blur_array,
        #     sharp_array=sharp_array)
        ##      atomsf_path=self.atomsf)
        # pl = [[self.process_maptomtz.process]]

        # Create a mask
        # Trim maps, need a mask,
        # if no mask provided, use EMDA to calculate map2mask
        if not self.args.no_trim():
            maskin = self.args.input_mask()
            if self.args.input_mask.value is None:
                if self.args.input_map():
                    map_for_mapmask = self.args.input_map()
                else:
                    map_for_mapmask = self.args.half_map_1()
                self.process_mapmask = buccaneer_task.EMDAMapMask(
                    command=self.commands['emda'],
                    job_location=self.job_location,
                    map_path=map_for_mapmask,
                    knl_rad=10,
                    resolution=10.0,
                    name='EMDA Map to Mask'
                )
                pl.append([self.process_mapmask.process])
                maskin = self.process_mapmask.maskout_path
            # trim outputs:
            # self.process_trim_maps.map_trim_path
            # self.process_trim_maps.hm1_trim_path
            # self.process_trim_maps.hm2_trim_path
            # self.process_trim_maps.mask_trim_path
            
            self.process_trim_maps = buccaneer_task.ServalcatMapTrim(
                command=self.commands['ccpem-python'],
                job_location=self.job_location,
                map_path=self.args.input_map(),
                halfmap_paths=[self.args.half_map_1(), self.args.half_map_2()],
                mask_path=maskin,
                input_model_path=self.args.extend_pdb.value,
                name='Trim Maps via Servalcat'
            )
            pl.append([self.process_trim_maps.process])
            map_for_naut = self.process_trim_maps.map_trim_path
            hm1_for_naut = self.process_trim_maps.hm1_trim_path
            hm2_for_naut = self.process_trim_maps.hm2_trim_path
            msk_for_naut = self.process_trim_maps.mask_trim_path
            extend_pdb_naut = self.process_trim_maps.input_model_trim_path
        else:
            map_for_naut = self.args.input_map()
            hm1_for_naut = self.args.half_map_1()
            hm2_for_naut = self.args.half_map_2()
            msk_for_naut = self.args.input_mask()
            extend_pdb_naut =self.args.extend_pdb.value
        
        if self.args.build_from_nemap():
            self.process_maptomtz = buccaneer_task.ServalcatNemap(
                command=self.commands['ccpem-python'],
                job_location=self.job_location,
                halfmap_paths=[hm1_for_naut, hm2_for_naut],
                mask_path=msk_for_naut,
                resolution=self.args.resolution.value,
                name='Halfmaps to Normalized Expected Map'
            )
            pl.append([self.process_maptomtz.process])
            colin_fo = 'FWT'
        else:
            self.process_maptomtz = refmac_task.RefmacMapToMtz(
                command=self.commands['refmac'],
                resolution=self.args.resolution.value,
                mode='Global',
                name='Map to MTZ',
                job_location=self.job_location,
                map_path=map_for_naut,
                blur_array=blur_array,
                sharp_array=sharp_array)
            pl.append([self.process_maptomtz.process])
            colin_fo = 'Fout{0}'.format(label_out_suffix)
            
        # Set MTZ SigF and FOM using GEMMI module
        self.process_sigf_nautilus = buccaneer_task.SetColumnsBuccaneer(
            command=self.commands['ccpem-python'],
            job_location=self.job_location,
            mtzin=self.process_maptomtz.hklout_path,
            name='Set MTZ SigF and FOM',
            mtzout='starting_map1.mtz',
            colin_fo=colin_fo,
            FOM=1.0,
            SIGF_scale=0.01)  # set SIGF as 1% of F column
        pl.append([self.process_sigf_nautilus.process])
        nautmtz = self.process_sigf_nautilus.mtzout_path
        if self.args.freer_on():
            # Create R free flags
            hklout = os.path.join(self.job_location,
                                  'nautilus.mtz')
            self.process_free_r_flags = buccaneer_task.FreeRFlags(
                command=self.commands['freerflag'],
                job_location=self.job_location,
                hklin=self.process_sigf_nautilus.mtzout_path,
                hklout=hklout,
                name='Set Rfree')
            pl.append([self.process_free_r_flags.process])
            nautmtz = self.process_free_r_flags.hklout

        # Save seq if sequence string is provided rather than file path
        if (not os.path.exists(self.args.input_seq.value) and
                isinstance(self.args.input_seq.value, str)):
            path = os.path.join(self.job_location,
                                'input.seq')
            f = open(path, 'w')
            f.write(self.args.input_seq.value)
            f.close()
            self.args.input_seq.value = f.name

        # Add optional refmac keywords
        refine_keywords = ''
        # make newligand noexit to prevent refmac from exiting if no ligand dictionary found,
        # very rare unless is a new ligand
        if self.args.lib_in.value:
            refine_keywords += 'MAKE NEWLigand Noexit\n'
        refine_keywords += add_refmac_keywords(self.args.refmac_keywords.value)

        # Run Nautilus pipeline
        # Nautilus->LIBG->{1}->RefmacServalcat->repeat...{1}maptomtz
        # RefmacServalcat refinement modes: Global, Masked
        for i in range(1, (self.args.ncycle.value + 1)):
            # 1st pipeline cycle Nautilus/Refmac
            if i == 1:
                naut_int_cycle = self.args.ncycle_nau1st.value
                pdbin_path = extend_pdb_naut
            else:
                naut_int_cycle = self.args.ncycle_naunth.value
                pdbin_path = self.process_refine.pdbout_path

            self.process_nautilus_pipeline = NautilusPipeline(
                command=self.commands['cnautilus'],
                job_location=self.job_location,
                hklin=nautmtz,
                seqin=self.args.input_seq.value,
                label_out_suffix=label_out_suffix,
                build_from_nemap=self.args.build_from_nemap(),
                ncycle=i,
                internal_cycle=naut_int_cycle,
                resolution=self.args.resolution.value,
                pdbin=pdbin_path,
                pdbout=None,
                name='Nautilus build {0}'.format(str(i)),
                job_title=self.args.job_title.value,
                freer_on=self.args.freer_on(),
                keywords=self.args.keywords.value)
            pl.append([self.process_nautilus_pipeline.process])

            # Run libg for DNA/RNA restraints
            self.process_libg = refmac_task.LibgRestraints(
                command=self.commands['libg'],
                job_location=self.job_location,
                name='DNA or RNA basepair restraints {0}'.format(str(i)),
                pdb_path=self.process_nautilus_pipeline.pdbout)
            pl.append([self.process_libg.process])

            # Run RefmacRefine (mode=Global||Masked)
            # # Invert sharpening value for refmac
            if self.args.map_sharpen() is None:
                sharp = 0
            else:
                sharp = -1.0 * self.args.map_sharpen()
            
            if self.args.masked_refinement_on():
                # Refinement parameters: Masked
                mode = 'Masked'
                name='Refmac refine (masked) via Servalcat {0}'.format(str(i))
            else:
                # Refinement parameters: Global
                mode = 'Global'
                name = 'Refmac refine (global) via Servalcat {0}'.format(str(i))

            if self.args.mask_for_fofc():
                if not self.args.no_trim():
                    refmac_fofc_mask = msk_for_naut
                elif self.args.input_mask():
                    refmac_fofc_mask = self.args.input_mask()
                else:
                    refmac_fofc_mask = None
            else:
                refmac_fofc_mask = None
            # Refine process
            # pass on only halfmaps when half_map_refinement is on
            ###
            if self.args.half_map_refinement():
                self.process_refine = refmac_task.RefmacRefineServal(
                    command=self.commands['ccpem-python'],
                    job_location=self.job_location,
                    pdb_path=self.process_nautilus_pipeline.pdbout,
                    map_path=None,
                    halfmap_paths=[hm1_for_naut, hm2_for_naut],
                    mask_for_fofc=refmac_fofc_mask,
                    resolution=self.args.resolution(),
                    weight_auto=True,
                    mask_radius=self.args.mask_radius(),
                    mode=mode,
                    name=name,
                    sharp=sharp,
                    set_bfactor=True,
                    atom_bfactor=40.0,
                    ncycle=self.args.ncycle_refmac.value,
                    symmetry_auto='None',
                    keywords=refine_keywords,
                    libg_restraints=True,
                    libg_restraints_path=[self.process_libg.libg_restraints_path],
                    pdbout_path='refined{0}.pdb'.format(str(i)))
                pl.append([self.process_refine.process])
            else:
                self.process_refine = refmac_task.RefmacRefineServal(
                    command=self.commands['ccpem-python'],
                    job_location=self.job_location,
                    pdb_path=self.process_nautilus_pipeline.pdbout,
                    map_path=map_for_naut,
                    halfmap_paths=[None, None],
                    mask_for_fofc=refmac_fofc_mask,
                    resolution=self.args.resolution(),
                    weight_auto=True,
                    mask_radius=self.args.mask_radius(),
                    mode=mode,
                    name=name,
                    sharp=sharp,
                    set_bfactor=True,
                    atom_bfactor=40.0,
                    ncycle=self.args.ncycle_refmac.value,
                    symmetry_auto='None',
                    keywords=refine_keywords,
                    libg_restraints=True,
                    libg_restraints_path=[self.process_libg.libg_restraints_path],
                    pdbout_path='refined{0}.pdb'.format(str(i)))
                pl.append([self.process_refine.process])

        # shiftback build and refined models
        # save in folder /shiftback_models
        # results
        custom_finish = NautilusResultsOnFinish(
            pipeline_path=self.job_location + '/task.ccpem',
            refine_process=self.process_refine,
            shifts_json=self.job_location + '/trim_shifts.json',
            pipeline_ncycles=self.args.ncycle(),
            job_location=self.job_location,
            no_trim_maps=self.args.no_trim())

        if run:
            os.chdir(self.job_location)
            self.pipeline = process_manager.CCPEMPipeline(
                pipeline=pl,
                job_id=job_id,
                args_path=self.args.jsonfile,
                location=self.job_location,
                db_inject=db_inject,
                database_path=self.database_path,
                taskname=self.task_info.name,
                title=self.args.job_title.value,
                verbose=self.verbose,
                on_finish_custom=custom_finish)
            self.pipeline.start()

    def validate_args(self):
        # Now do at gui level
        return True


class NautilusPipeline(object):
    '''
    Real time nucleic acid chain tracing (N.B. runs cnautilus)
    '''

    def __init__(self,
                 command,
                 job_location,
                 hklin,
                 seqin,
                 label_out_suffix,
                 build_from_nemap=False,
                 ncycle=1,
                 internal_cycle=5,
                 resolution=2.0,
                 pdbin=None,
                 pdbout=None,
                 name=None,
                 job_title=None,
                 freer_on=True,
                 keywords=None):
        # counter for overall NautilusPipeline cycles, start with 1
        assert command is not None
        self.job_location = job_location
        self.hklin = hklin
        self.seqin = seqin
        assert os.path.exists(path=self.seqin)
        self.label_out_suffix = label_out_suffix
        self.pdbout = pdbout
        self.ncycle = ncycle
        self.internal_cycle = internal_cycle
        self.resolution = resolution
        self.pdbin = pdbin
        self.build_from_nemap = build_from_nemap
        self.freer_on = freer_on
        if self.pdbout is None:
            self.pdbout = os.path.join(
                job_location, 'build' + str(ncycle) + '.pdb')
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.job_title = job_title
        self.stdin = None
        self.stdin_extra = None
        self.keywords = keywords
        #
        self.set_args()
        self.set_stdin()
        #
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin)

    def set_args(self):
        self.args = ['-stdin']

    def set_stdin(self):
        pdbin_ref = os.path.join(os.environ['CLIBD'],
                                 'nautilus_lib.pdb')
        # build using Servalcat calculated normalized expected map
        if self.build_from_nemap:
            colin_fo = 'FWT'
            colin_phi = 'PHWT'
        else:
            colin_fo = 'Fout{0}'.format(self.label_out_suffix)
            colin_phi = 'Pout0'

        self.stdin = '''
title {0}
pdbin-ref {1}
seqin {2}
mtzin {3}
colin-fo {4},SIGF
colin-phifom {5},FOM
pdbout {6}
cycles {7}
resolution {8}
anisotropy-correction
xmlout program{9}.xml
'''.format(self.job_title,
           pdbin_ref,
           self.seqin,
           self.hklin,
           colin_fo,
           colin_phi,
           self.pdbout,
           self.internal_cycle,
           self.resolution,
           self.ncycle)

        #self.stdin = self.stdin + self.stdin_extra
        # Add optional nautilus keywords
        if isinstance(self.keywords, str):
            if self.keywords != '':
                # Remove trailing white space and new lines
                keywords = self.keywords.strip()
                for line in keywords.split('\n'):
                    self.stdin += line + '\n'
        if self.pdbin is not None:
            self.stdin += 'pdbin {0}\n'.format(self.pdbin)
        if self.freer_on:
            self.stdin += 'colin-free FreeR_flag\n'


class NautilusResultsOnFinish(process_manager.CCPEMPipelineCustomFinish):
    '''
    Generate RVAPI results on finish.
    '''

    def __init__(self,
                 pipeline_path,
                 refine_process,
                 shifts_json,
                 pipeline_ncycles,
                 job_location,
                 no_trim_maps):
        super(NautilusResultsOnFinish, self).__init__()
        self.pipeline_path = pipeline_path
        self.refine_process = refine_process
        self.shifts_json = shifts_json
        self.pipeline_ncycles = pipeline_ncycles
        self.job_location = job_location
        self.no_trim_maps = no_trim_maps

    def on_finish(self, parent_pipeline=None):
        # shiftback build and refined output models
        # save into /shiftback_models folder
        shiftback_folder = os.path.join(self.job_location, 'shiftback_models')
        if not self.no_trim_maps:
            if not os.path.exists(shiftback_folder):
                check_outmmcif = os.path.join(shiftback_folder,
                                              'refined{0}_shiftback.mmcif'.format(self.pipeline_ncycles))
                if not os.path.exists(check_outmmcif):
                    shiftback_models(shifts_json=self.shifts_json,
                                     pipeline_ncycles=self.pipeline_ncycles,
                                     job_location=self.job_location)
        
        # generate RVAPI report
        nautilus_results.PipelineResultsViewer(
            pipeline_path=self.pipeline_path)


def add_refmac_keywords(refkeywords):
    '''
    Add default and additional refmac keywords
    '''
    refine_keywords = ''
# PHOUT
# PNAME nautilus
# DNAME nautilus
#'''.format(labelout_suffix)

    if isinstance(refkeywords, str):
        if refkeywords != '':
            # Remove trailing white space and new lines
            keywords = refkeywords.strip()
            for line in keywords.split('\n'):
                refine_keywords += line + '\n'

    return refine_keywords
