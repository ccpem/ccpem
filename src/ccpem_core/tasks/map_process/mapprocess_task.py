#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
import json
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.map_tools.TEMPy import map_preprocess
from ccpem_core.process_manager import job_register
from ccpem_core import settings
import parser


class MapProcess(task_utils.CCPEMTask):
    '''
    CCPEM / TEMPy Map Process.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='MapProcess',
        author='A. Joseph, M. Topf, M. Winn',
        version='1.1',
        description=(
            '''
            A set of basic tools to process maps.<br>
            The order of the map processing functions can 
            re-arranged by mouse drag. The re-arranged order 
            will be used to process the map.
            'shiftpeak_to_zero': Move background peak to zero.<br>
            'downsample': Downsample voxel size.<br>
            'threshold': 'Threshold map at this contour level'.<br>
            'lowpass_filter': 'Lowpass filter at given resolution'.<br>
            'dust_filter': Remove small isolated densities at a contour.<br>
            'softmask': Apply softmask at the edges.<br>
            'crop': crop map grid at a certain density threshold.<br>
            'Pad map given (X Y Z or X,Y,Z) padding'.<br>
            'mask':'Apply a mask(mrc file)'.<br>
            'shift_origin': 'Shift map origin to (X Y Z or X,Y,Z)'.<br>
            '''),
        short_description=(
            'Tools to mask/filter maps'),
        documentation_link='http://topf-group.ismb.lon.ac.uk/TEMPY.html',
        references=None)

    commands = {'ccpem-python':
                ['ccpem-python', os.path.realpath(map_preprocess.__file__)]}

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        #
        super(MapProcess, self).__init__(
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        map_path = parser.add_argument_group()
        map_path.add_argument(
            '-map_path',
            '--map_path',
            help='Input map (mrc format)',
            metavar='Input map',
            type=str,
            default=None)

        list_process = parser.add_argument(
            '-list_process',
            '--list_process',
            nargs='+',
            help='List map process operations',
            metavar='List map process operations',
            default=None)

        parser.add_argument(
            '-map_resolution',
            '--map_resolution',
            help=('Resolution of map (Angstrom)'),
            metavar='Map resolution',
            type=float,
            default=None)

        parser.add_argument(
            '-map_apix',
            '--map_apix',
            help=('New voxel size (Angstrom)'),
            metavar='New voxel size',
            type=float,
            default=None)

        parser.add_argument(
            '-map_contour',
            '--map_contour',
            help=('Map density threshold'),
            metavar='Map density threshold',
            type=float,
            default=None)

        mask_path = parser.add_argument_group()
        mask_path.add_argument(
            '-mask_path',
            '--mask_path',
            help='Input mask file (mrc format)',
            metavar='Input mask file',
            type=str,
            default=None)

        map_pad = parser.add_argument(
            '-map_pad',
            '--map_pad',
            nargs='+',
            help='X Y Z padding',
            metavar='X Y Z padding',
            default=None,
            type=list)

        map_origin = parser.add_argument(
            '-map_origin',
            '--map_origin',
            nargs='+',
            help='X Y Z origin',
            metavar='X Y Z origin',
            default=None,
            type=list)

        parser.add_argument(
            '-crop_cubic_map',
            '--crop_cubic_map',
            type=bool,
            help='Output a cubic map after crop',
            metavar='Output a cubic map after crop',
            default=False
        )
        parser.add_argument(
            '-fixed_dimension',
            '--fixed_dimension',
            nargs='+',
            help=('X Y Z fixed grid size of output map.\n'
                  'Will not be used if threshold is set'),
            metavar='X Y Z grid size',
            default=None,
            type=list
        )

        return parser

    def run_pipeline(self, job_id=None, db_inject=None):
        self.mapprocess_wrapper = mapprocess_wrapper(
            job_location=self.job_location,
            command=self.commands['ccpem-python'],
            map_path=self.args.map_path.value,
            list_process=self.args.list_process.value,
            map_resolution=self.args.map_resolution.value,
            map_apix=self.args.map_apix.value,
            map_contour=self.args.map_contour.value,
            map_pad=self.args.map_pad.value,
            map_origin=self.args.map_origin.value,
            mask_path=self.args.mask_path.value,
            cubic_map=self.args.crop_cubic_map.value,
            fixed_dimension=self.args.fixed_dimension.value
        )
        pl = [[self.mapprocess_wrapper.process]]
        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()


class mapprocess_wrapper():
    '''
    Wrapper for TEMPy Map Processing tool.
    '''

    def __init__(self,
                 job_location,
                 command,
                 map_path,
                 list_process,
                 map_resolution=None,
                 map_contour=None,
                 map_apix=None,
                 map_pad=None,
                 map_origin=None,
                 mask_path=None,
                 cubic_map=None,
                 fixed_dimension=None,
                 name='MapProcess'):
        assert map_path is not None
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.map_path = ccpem_utils.get_path_abs(map_path)
        # Set args
        self.args = ['-m', self.map_path]
        if list_process is not None:
            list_process_args = ['-l']
            list_process_args.extend(list_process)
            self.args += list_process_args
        if map_resolution is not None:
            self.args += ['-r', map_resolution]
        # threshold
        if map_contour is not None:
            self.args += ['-t', map_contour]
        # new apix
        if map_apix is not None:
            self.args += ['-p', map_apix]
        # padding
        if map_pad is not None:
            pad_args = ['-pad']
            pad_args.extend(list(map_pad))
            if len(map_pad) > 0:
                self.args += pad_args  # ' '.join(
                # [str(p) for p in map_pad])]
        # new origin
        if map_origin is not None:
            origin_args = ['-ori']
            origin_args.extend(map_origin)
            if len(map_origin) > 0:
                self.args += origin_args
        # mask file
        if mask_path is not None:
            self.args += ['-ma', mask_path]
        # crop option, cubic map
        if cubic_map:
            self.args += ['-cc']
        # crop option, fixed dimension
        if fixed_dimension is not None:
            # check dimensions (grid int) given not zero
            if all(x != 0 for x in fixed_dimension):
                dimension_args = ['-cr']
                dimension_args.extend(fixed_dimension)
                if len(fixed_dimension) > 0:
                    self.args += dimension_args
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)
