#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
try:
    import modeller
    modeller_available = True
    from ccpem_progs.choyce import model_build
except ImportError:
    modeller_available = False
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils

class Choyce(task_utils.CCPEMTask):
    '''
    CCPEM Choyce task.
    '''

    task_info = task_utils.CCPEMTaskInfo(
        name='Choyce',
        author='R. Rawi, L. Whitmore, M. Topf',
        version='1.0',
        description=(
            '''
            Constrained homology modelling with cryoEM maps.
            Builds a homology model based on template model restraints.
            <br><br>
            Takes a template atomic model and a target sequence as 
            input.
            '''),
        short_description=(
            'Constrained homology modelling with cryoEM maps'),
        documentation_link='http://choyce.ismb.lon.ac.uk/',
        references=None)

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        self.commands = {
            'model_build': ['ccpem-python', os.path.realpath(model_build.__file__)]
            }
        super(Choyce, self).__init__(database_path=database_path,
                                     args=args,
                                     args_json=args_json,
                                     pipeline=pipeline,
                                     job_location=job_location,
                                     parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        reference_model_path = parser.add_argument_group()
        reference_model_path.add_argument(
            '-reference_model_path',
            '--reference_model_path',
            help='Reference coordinate file (pdb format)',
            metavar='Reference PDB',
            type=str,
            default=None)
        #
        sequence_path = parser.add_argument_group()
        sequence_path.add_argument(
            '-sequence_path',
            '--sequence_path',
            help='Target sequence (fasta format)',
            metavar='Target Sequence',
            type=str,
            default=None)
        #        #
        output_model_path = parser.add_argument_group()
        output_model_path.add_argument(
            '-output_model_path',
            '--output_model_path',
            help='Output coordinate file (pdb format)',
            metavar='Output PDB',
            type=str,
            default=None)
        return parser

    def job_choyce_homology(self):
        '''
        Call module choyce homology to generate model using reference structure
        and target sequence.
        '''
        #
        json_path = os.path.join(self.job_location,
                                 'args.json')
        self.choyce_homology_process = process_manager.CCPEMProcess(
            name='Choyce',
            command=self.commands['model_build'],
            args=[json_path],
            location=self.job_location,
            stdin=None)

    def run_pipeline(self, job_id=None, db_inject=None):
        # Set args job location
        self.args.job_location.value = self.job_location
        filename = os.path.join(self.job_location, 'args.json')
        self.args.output_args_as_json(
            filename=filename,
            set_abs_path=True)

        # Directory to store intermediate data
        self.job_data = os.path.join(self.job_location, 'job_data')
        ccpem_utils.check_directory_and_make(self.job_data)
        # Generate processes
        # FlexEM
        self.job_choyce_homology()
        pl = [[self.choyce_homology_process]]

        # Pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

