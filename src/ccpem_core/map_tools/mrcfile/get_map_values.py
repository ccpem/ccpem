import re,sys
import numpy as np
from collections import OrderedDict

def get_map_values(structure_instance,emmap,res_map,
                   sim_sigma_coeff=0.225,win=1,sigma_thr=2.0):
    """
    Returns avg map density from voxels occupied by each residue in a structure.
    
    Arguments:
    
       *structure_instance*
           Gemmi structure instance of the model.
       *emmap*
           Map instance the model is to be placed on.
       *res_map*
           Resolution of the map
       *sim_sigma_coeff*
           Sigma factor used for blurring
       *win*
           Fragment length (odd values)
        *sigma_thr*
            Threshold of sigma to determine gaussian width
    """
    dict_chain_indices, dict_chain_res,dict_chain_CA, gridtree = \
        get_indices(structure_instance,emmap,res_map,
                         sim_sigma_coeff=sim_sigma_coeff,
                         sigma_thr=sigma_thr)
    origin = emmap.origin
    apix = emmap.apix
    nz,ny,nx = emmap.fullMap.shape
    zg,yg,xg = np.mgrid[0:nz,0:ny,0:nx]
    indi = zip(xg.ravel(), yg.ravel(), zg.ravel())
           #for residues not in rigid bodies: consider pentapeptides
    dict_chain_scores = {}
    for ch in dict_chain_indices:
        dict_res_scores = {}
        dict_res_indices = dict_chain_indices[ch]    
        prev_mapval = 0.0
        for res in dict_res_indices:
            try:
                resname = dict_chain_CA[ch][res][0]
                #ca_coord = dict_chain_CA[ch][res][1:]
            except KeyError: 
                resname = ' '
                #continue #skip residues with no ca-coordinates?
            if not dict_res_scores.has_key(res):
                indices = dict_res_indices[res][:]
                #consider residues on both sides. NOTE: wont work for insertion codes!
                #need to rewite res numbers to avoid insertion codes
                for ii in range(1,int(round((win+1)/2))):
                    try:
                        #get prev residue indices
                        indices.extend(
                            dict_res_indices[dict_chain_res[ch][dict_chain_res[ch].index(res)-ii]])
                    except: pass
                for ii in range(1,int(round((win+1)/2))):
                    try:
                        indices.extend(
                            dict_res_indices[dict_chain_res[ch][dict_chain_res[ch].index(res)+ii]])
                    except: pass

                tmplist = indices[:]
                setlist = set(tmplist)
                indices = list(setlist)
                sc_indices = []
                for ii in indices: sc_indices.append(indi[ii])
                '''
                if len(indices) < 10:
                    try: 
                        dict_res_scores[res] = dict_res_scores[dict_chain_res[ch][dict_chain_res[ch].index(res)-1]]
                        try: dict_res_scores[res] = (dict_res_scores[res]+dict_res_scores[dict_chain_res[ch][dict_chain_res[ch].index(res)+1]])/2.0
                        except (IndexError,KeyError): pass
                    except (IndexError,KeyError): 
                        try: dict_res_scores[res] = dict_res_scores[dict_chain_res[ch][dict_chain_res[ch].index(res)+1]]
                        except (IndexError,KeyError): dict_res_scores[res] = 0.0
                    continue
                '''
                res_id = str(res) + '_' + resname
                array_indices = np.array(sc_indices)
                ind_arrxyz = np.transpose(array_indices)
                try:
                    ind_arrzyx = (ind_arrxyz[2],ind_arrxyz[1],ind_arrxyz[0])
                except IndexError:
                    dict_res_scores[res_id] = prev_mapval
                    continue
                mapval = np.mean(emmap.fullMap[ind_arrzyx])
                dict_res_scores[res_id] = mapval
                prev_mapval = mapval
        dict_chain_scores[ch] = dict_res_scores.copy()
    return dict_chain_scores, dict_chain_CA

# get nearest grid indices for a residue atom
def get_indices(structure_instance,emmap,map_resolution,
                sim_sigma_coeff=0.225,sigma_thr=2.0,
                atom_sel=None, atom_centre='CA'):
    """
    
    Returns flat indices of the pixels occupied by each residue in a chain.
    
    Arguments:
    
       *structure_instance*
           Gemmi structure instance of the model.
       *emmap*
           Mapprocess/MrcFile map instance (where the model is to be placed on).
       *map_resolution*
           Resolution of the map
       *sim_sigma_coeff*
           Sigma factor used for blurring
    """

    dict_res_indices = OrderedDict()
    dict_res_CA = OrderedDict()
    dict_chain_res = OrderedDict() #residue numbers in each chain
    dict_chain_CA = OrderedDict()
    dict_chain_indices = OrderedDict()
    gridtree = maptree(emmap)[0] #get coordinates of the map
    points = []
    #get chain details
    ca_coord = ['','','']
    for model in structure_instance:
        for chain in model:
            currentChain = chain.name
            polymer = chain.get_polymer()
#             #skip non polymers
#             if not polymer: continue
            dict_res_CA = {} #CA coords of residues in the chain
            dict_res_indices = {} #map indices covered by the residues
            for residue in chain:
                residue_id = str(residue.seqid.num)+'_'+residue.name
                cur_res = residue.seqid.num
                #save residue numbers in order
                try: 
                  if not cur_res in dict_chain_res[currentChain]: 
                      dict_chain_res[currentChain].append(cur_res)
                except KeyError: dict_chain_res[currentChain] = [cur_res]
                list_coord = []
                ca_coord = []
                for atom in residue:
                    #use only selected atom/res type
                    if atom_sel == 'CA' and not atom.name == 'CA': continue
                    elif atom_sel == 'AR' and residue.name not in ['TYR','PHE','TRP'] \
                                            : continue
                    coord = atom.pos
                    #get indices covered by gaussian blur
                    if not map_resolution is None: 
                        points = mapGridPositions(emmap,atom,gridtree,map_resolution,
                                                    sim_sigma_coeff,sigma_thr)
                    #get indices covered by vdW radii
                    else: points, gridpos = mapGridPositions_vdw(emmap,atom,gridtree)
                    
                    if len(points) == 0:
                        print "Warning, atom out of map box", \
                                    currentChain, cur_res, atom.name
                    #set atom_centre for nucleic acids and get coordinates of atom centre
                    if atom.name == atom_centre:
                        ca_coord = [atom.pos.x, atom.pos.y, atom.pos.z]
                    elif residue.name in ['A','T','C','G','U']:#nuc acids
                        if atom_centre in ["P","C3'","C1'"]: #if already defined
                            pass
                        else:
                            atom_centre = "C1'"
                        if atom.name in ["P","C3'","C1'"]:
                            ca_coord = [atom.pos.x, atom.pos.y, atom.pos.z] #if C1' not found
                    elif atom.name == 'CA': #if atom_centre not defined
                        ca_coord = [atom.pos.x, atom.pos.y, atom.pos.z]
                    else:# non prot and nuc_acid
                        list_coord.append(atom.pos)
                    #continue if no indices are returned for the atom
                    if len(points) == 0:
                        dict_res_indices[cur_res] = []
                        continue
                    # get points occupied by the residue
                    if dict_res_indices.has_key(cur_res):
                        dict_res_indices[cur_res].extend(points)
                    else: dict_res_indices[cur_res] = points
                if len(ca_coord) == 0:#if atom centre is not found
                    if len(list_coord) > 0:
                        residue_centre = list_coord[int(len(list_coord)/2)]
                    else: residue_centre = atom.pos
                    ca_coord = [residue_centre.x,residue_centre.y,residue_centre.z]
                try: dict_res_CA[cur_res] = [residue.name]+ca_coord
                except AttributeError: 
                    dict_res_CA[cur_res] = [' ']+ca_coord
            #uniquify indices of residues in the chain
            for e in dict_res_indices:
                tmplist = dict_res_indices[e][:]
                setlist = set(tmplist)
                dict_res_indices[e] = list(setlist)
            if len(dict_res_indices) > 0:
                dict_chain_indices[currentChain] = dict_res_indices.copy()
            if len(dict_res_CA) > 0:
                dict_chain_CA[currentChain] = dict_res_CA.copy()
    return dict_chain_indices, dict_chain_res,dict_chain_CA, gridtree

def maptree(densMap,strmap=None):
    """
    
    Returns the KDTree of coordinates from a map grid.
    
    Arguments:
    
       *densMap*
           Map instance the atom is to be placed on.
           
    """
    origin = densMap.origin
    apix = densMap.apix
    nz,ny,nx = densMap.fullMap.shape

    #convert to real coordinates 
    zg,yg,xg = np.mgrid[0:nz,0:ny,0:nx]
    #to get indices in real coordinates
    zg = zg*apix + origin[2] + apix/2.0
    yg = yg*apix + origin[1] + apix/2.0
    xg = xg*apix + origin[0] + apix/2.0
    indi = zip(xg.ravel(), yg.ravel(), zg.ravel())
    try: 
        from scipy.spatial import cKDTree
        gridtree = cKDTree(indi)
    except ImportError:
        try:
            from scipy.spatial import KDTree
            gridtree = KDTree(indi)
        except ImportError: return
    return gridtree, indi

def mapGridPositions_vdw(densMap, atom, gridtree):
    
    """
    
    Returns the index of the nearest pixel to an atom, and atom mass (4 values in list form).
    
    Arguments:
    
       *densMap*
           Map instance the atom is to be placed on.
       *atom*
           Atom instance.
           
       """
    origin = densMap.origin
    apix = densMap.apix
    x_pos = int(round((atom.x-origin[0])/apix,0))
    y_pos = int(round((atom.y-origin[1])/apix,0))
    z_pos = int(round((atom.z-origin[2])/apix,0))
    if((densMap.x_size() > x_pos >= 0) and (densMap.y_size() > y_pos >= 0) and (densMap.z_size() > z_pos >= 0)):
        #search all points withing 1.5sigma
        list_points = gridtree.query_ball_point([atom.x,atom.y,atom.z], atom.vdw)
        return list_points, (x_pos, y_pos, z_pos)
    else:
        print "Warning, atom out of map box"
        return [], ()


def mapGridPositions(densMap, atom, 
                     gridtree,map_resolution,sim_sigma_coeff=0.187,
                     sigma_thr=2.0):
    """
    
    Returns the indices of the nearest pixels to an atom as a list.
    
    Arguments:
    
       *densMap*
           Map instance the atom is to be placed on.
       *atom*
           Atom instance.
       *gridtree*
           KDTree of the map coordinates (absolute cartesian)
       *map_resolution*
           Map resolution               
    """

    origin = densMap.origin
    apix = densMap.apix
    x_pos = int(round((atom.pos.x-origin[0])/apix,0))
    y_pos = int(round((atom.pos.y-origin[1])/apix,0))
    z_pos = int(round((atom.pos.z-origin[2])/apix,0))
    #
    if((densMap.x_size() >= x_pos >= 0) and 
       (densMap.y_size() >= y_pos >= 0) and 
       (densMap.z_size() >= z_pos >= 0)):
        #search all points withing 1.5sigma
        list_points = gridtree.query_ball_point(\
                        [atom.pos.x,atom.pos.y,atom.pos.z], 
                        max(apix,sigma_thr*max(sim_sigma_coeff*map_resolution,1.0)))
        return list_points
    else:
        return []

