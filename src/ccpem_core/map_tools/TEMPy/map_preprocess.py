#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
"""
Script to perform a series of map processing steps 

Classes:
    :class:`MapPreprocess`: Object to setup map attributes and 
                            run selected processing steps

Methods:
    shiftpeak_to_zero: Move background peak to zero
    lowpass_filter: Lowpass filter at given resolution
    threshold: Threshold map at this contour level
    mask:Apply a mask(mrc file)
    crop: Crop map at a given contour
    pad: Pad map given (X Y Z or X,Y,Z) padding
    downsample: Downsample map to a larger voxel size
    dust_filter: Remove small isolated densities at given contour
    softmask: Softmask the map edges
    shift_origin: Shift map origin to (X Y Z or X,Y,Z)

Run as:
    ccpem-python map_preprocess.py -m mapfile 
    -l shiftpeak_to_zero pad lowpass_filter downsample dust 
    -r resolution -p voxel_size -t contour_level -pad 10 10 10

Required arguments:
    map input [-m]
    list of operations [-l]
    resolution [-r]: used for lowpass
    voxel size [-p]: used for downsample
    mask file [-ma]: used for mask
    x y z [-pad]: used for pad
    contour [-thr] or x y z [-cr]: used for crop

Dependencies for system python:
    mrcfile
    TEMPy
"""
import os
import sys
import mrcfile
from TEMPy.mapprocess import Filter
from ccpem_core.map_tools import array_utils
import argparse


def set_argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--map', help='Map file',
                        default=None)
    parser.add_argument('-l', '--list', nargs='+', required='True', default=None,
                        help="list of map processing steps")
    parser.add_argument('-p', '--apix', type=float, help='pixel size in Angstrom',
                        default=None)
    parser.add_argument('-r', '--res', type=float, help='Resolution in Angstrom',
                        default=None)
    parser.add_argument('-t', '--thr', type=float, help='Contour threshold',
                        default=None)
    parser.add_argument('-ma', '--mask', help='Mask map',
                        default=None)
    parser.add_argument('-pad', '--pad', nargs='+', help='X Y Z padding',
                        default=None)
    parser.add_argument('-cr', '--cr', nargs='+', help='X Y Z cropped dimensions',
                        default=None)
    parser.add_argument('-ori', '--ori', nargs='+', help='X Y Z float origin',
                        default=None)
    parser.add_argument('-out', '--out', help='Output map file',
                        default=None)
    parser.add_argument('-cc', '--ccubic',
                        action='store_true', help='Crop cubic map')
    return parser


class MapPreprocess():
    def __init__(self,
                 args):
        mapfile = args.map
        assert os.path.isfile(mapfile)
        mrcobj = mrcfile.open(mapfile, mode='r')
        self.mrcmap = Filter(mrcobj)
        # map process
        self.list_methods = args.list
        try:
            assert self.list_methods is not None
        except AssertionError:
            sys.exit('Input atleast one map process operation')

        self.setup_attributes(args)
        array_utils.plot_density_histogram(self.mrcmap.fullMap,
                                           plotfile="initial_histogram.png")
        if args.res is not None:
            array_utils.get_raps(self.mrcmap.fullMap,
                                 map_apix=self.apix[0],
                                 plotfile="initial_powspectra.png")
#         list_methods = ['shiftpeak','pad','lowpass','downsample','crop',
#                         'threshold','mask']
        inplace = False
        self.processed_map = self.mrcmap
        for i in range(len(self.list_methods)):
            p = self.list_methods[i]
            print p
            try:
                method = getattr(self, p)
            except AttributeError:
                print 'Method not implemented: ', p
            self.processed_map = method(self.processed_map, inplace=inplace)
            inplace = True

        array_utils.plot_density_histogram(self.processed_map.fullMap,
                                           plotfile="processed_histogram.png")
        if args.res is not None:
            array_utils.get_raps(self.processed_map.fullMap,
                                 map_apix=self.newapix,
                                 plotfile="processed_powspectra.png")
        self.mrcmap.close()
        # self.processed_map.close()

    def setup_attributes(self, args):
        self.apix = self.mrcmap.apix
        if not args.apix is None:
            self.newapix = args.apix
        elif 'downsample' in self.list_methods and \
                args.res is not None:
            self.newapix = args.res/3.
        else:
            self.newapix = self.apix[0]
        # resolution
        self.lowpass_freq = None
        self.resolution = args.res
        if args.res is not None:
            self.lowpass_freq = self.apix[0]/self.resolution
        self.maskfile = args.mask
        if args.mask is not None:
            try:
                assert os.path.isfile(self.maskfile)
            except AssertionError:
                sys.exit('Mask file not found')
        self.contour = args.thr
        if self.contour is None:
            self.contour = self.mrcmap.calculate_map_contour()
        self.pad_xyz = (0, 0, 0)
        if not args.pad is None:
            self.pad_xyz = (int(args.pad[0]), int(
                args.pad[1]), int(args.pad[2]))
        self.cropped_xyz = args.cr
        if not (args.cr is None):
            self.cropped_xyz = (int(args.cr[0]), int(
                args.cr[1]), int(args.cr[2]))
            print self.cropped_xyz
        # origin
        self.ori = self.mrcmap.origin
        if not args.ori is None:
            self.newori = (float(args.ori[0]), float(args.ori[1]),
                           float(args.ori[2]))
        else:
            self.newori = self.ori
        # set crop cubic map
        if args.ccubic:
            self.cubic = True
        else:
            self.cubic = False

    def shiftpeak_to_zero(self, mrcmap, inplace=False):
        peak, ave, sig = mrcmap.peak_density()
        shiftedmap = mrcmap.shift_density(-peak, inplace=inplace)
        print "Map density shifted by", -peak
        self.contour = self.contour - peak
        if inplace:
            return mrcmap
        return shiftedmap

    def lowpass_filter(self, mrcmap, inplace=False):
        try:
            assert self.lowpass_freq is not None
        except AssertionError:
            sys.exit('Input map resolution')
        ftfilter = array_utils.tanh_lowpass(mrcmap.fullMap.shape,
                                            cutoff=self.lowpass_freq,
                                            fall=0.25)
        filtmap = mrcmap.fourier_filter(ftfilter=ftfilter, inplace=inplace)
        if inplace:
            return mrcmap
        return filtmap

    def bandpass_filter(self, mrcmap, inplace=False):
        try:
            assert self.lowpass_freq is not None
        except AssertionError:
            sys.exit('Input map resolution')
        ftfilter = array_utils.tanh_bandpass(mrcmap.fullMap.shape,
                                             high_cutoff=100.0,
                                             low_cutoff=self.lowpass_freq,
                                             high_fall=0.25,
                                             low_fall=0.25)
        filtmap = mrcmap.fourier_filter(ftfilter=ftfilter, inplace=inplace)
        if inplace:
            return mrcmap
        return filtmap

    def threshold(self, mrcmap, inplace=False):
        contouredmap = mrcmap.threshold_map(
            contour=self.contour, inplace=inplace)
        if inplace:
            return mrcmap
        return contouredmap

    def mask(self, mrcmap, inplace=False):
        mrcobj = mrcfile.open(self.maskfile, mode='r+')
        maskedmap = mrcmap.apply_mask(mask_array=mrcobj.data,
                                      inplace=inplace)
        if inplace:
            return mrcmap
        return maskedmap

    def crop(self, mrcmap, inplace=False):

        if self.cropped_xyz is not None:
            self.contour = None
        croppedmap = mrcmap.crop_map(contour=self.contour, factor_sigma=0.1,
                                     inplace=inplace, new_dim=self.cropped_xyz,
                                     cubic=self.cubic)
        if inplace:
            return mrcmap
        return croppedmap

    def pad(self, mrcmap, inplace=False):
        paddedmap = mrcmap.pad_map(self.pad_xyz[0], self.pad_xyz[1],
                                   self.pad_xyz[2],
                                   inplace=inplace)
        if inplace:
            return mrcmap
        return paddedmap

    def downsample(self, mrcmap, inplace=False):
        downsampledmap = mrcmap.downsample_apix(self.newapix,
                                                inplace=inplace)
        if inplace:
            return mrcmap
        return downsampledmap

    def dust_filter(self, mrcmap, inplace=False):
        dustedmap = mrcmap.remove_dust_by_size(self.contour,
                                               prob=0.1,
                                               inplace=inplace)
        if inplace:
            return mrcmap
        return dustedmap[0]  # isolated density labels are also returned

    def softmask(self, mrcmap, inplace=False):
        softmaskedmap = mrcmap.softmask_edges(window=3, inplace=inplace)
        if inplace:
            return mrcmap
        return softmaskedmap

    def shift_origin(self, mrcmap, inplace=False):
        moved_map = mrcmap.move_map(self.newori, inplace=inplace)
        if inplace:
            return mrcmap
        return moved_map


def main():
    parser = set_argparser()
    arguments = parser.parse_args()
    if arguments.out is None:
        newmapfile = os.path.splitext(os.path.basename(arguments.map))[
            0]+'_processed.mrc'
    else:
        newmapfile = arguments.out
    newmap = mrcfile.new(newmapfile, overwrite=True)
    mp = MapPreprocess(arguments)
    # update data and header
    mp.processed_map.set_newmap_data_header(newmap)
    newmap.close()


if __name__ == '__main__':
    sys.exit(main())
