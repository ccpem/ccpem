#THIS SCRIPT IS TEMPORARY AND NOT FUNCTIONAL

import sys
import re
import os
from TEMPy.MapParser import MapParser
from TEMPy.StructureBlurrer import StructureBlurrer
from TEMPy.StructureParser import PDBParser
import TEMPy.Vector  as Vector
from TEMPy.ScoringFunctions import ScoringFunctions
import numpy as np
from TEMPy.class_arg import TempyParser
from TEMPy.mapprocess import Filter
from TEMPy.ShowPlot import Plot
from collections import OrderedDict
import glob
import pandas as pd
import gemmi
from ccpem_core.map_tools.mrcfile.get_map_values import *

mrcfile_import=True
try:
    import mrcfile
except ImportError:
    mrcfile_import = False

def read_mapfile(map_path):
    if mrcfile_import:
        mrcobj=mrcfile.open(map_path,mode='r')
        emmap = Filter(mrcobj)
        emmap.fix_origin()
        emmap.set_apix_tempy()
    else:
        mrcobj=MapParser.readMRC(map_path)
        emmap = Filter(mrcobj)
        emmap.fix_origin()
    return emmap

def main():
    tp = TempyParser()
    tp.generate_args()
    
    m = tp.args.inp_map
    if m is None: m = tp.args.inp_map1
    elif tp.args.res is None: sys.exit('Input a map, is resolution(required) and contour(optional)')
    else: r = tp.args.res
    plist = tp.args.plist
    mlist = tp.args.mlist
    rlist = tp.args.rlist
    pdir = tp.args.pdir
    
    # the sigma factor determines the width of the Gaussian distribution used to describe each atom
    sim_sigma_coeff = 0.187
    if not tp.args.sigfac is None:
        sim_sigma_coeff = tp.args.sigfac
        
    flag_model = 0
    p = tp.args.pdb
    for p in [tp.args.pdb,tp.args.pdb2,tp.args.pdb1]:
        if not p is None: 
            flag_model = 1
            break
    
    if not mlist is None:
        list_maps = mlist
        if rlist is None: sys.exit('Input resolutions for multiple maps with rlist')
        list_res = rlist
    elif not m is None:
        list_maps = [m]
        rlist = [r]
    if not pdir is None:
        if not os.path.isdir(pdir): sys.exit('Directory with multiple models not found!')
        list_models = glob.glob1(pdir,'*.pdb')
        list_models = [os.path.join(os.path.abspath(pdir),pdbfile) for pdbfile in list_models]
    elif not plist is None:
        list_models = [os.path.abspath(pd) for pd in plist]
    elif not p is None:
        list_models = [p]

    residue_label = 'resnum'
    resname_label = 'resname'
    score_label = 'mapval'
    mapval_frames = []
    m_count = 0
    dict_chains_mapval = {}
    maxlim = 0.0
    for m in list_maps:
        #get details of map
        Name1 = os.path.basename(m).split('.')[0]
        #read map file
        map_target = read_mapfile(m)
        
        resolution_densMap = rlist[m_count]
        m_count += 1
        
        for p in list_models:
            #read the model  
#             structure_instance=PDBParser.read_PDB_file('pdb1',p,
#                                                        hetatm=True,water=False)
            structure_instance = gemmi.read_structure(p)
            
            Name2 = os.path.basename(p).split('.')[0]
            print Name1, Name2
            #get coordinates of chains
            #dict_chain_indices, dict_chain_CA = blurrer.get_coordinates(structure_instance)
            #get map indices corresponding to residues
#             blurrer = StructureBlurrer()
#             dict_chain_scores1 = blurrer.get_map_values(
#                                 structure_instance,map_target,
#                                 resolution_densMap,win=1,sigma_thr=0.5)
            dict_chain_scores1 = get_map_values(structure_instance,map_target,
                                resolution_densMap,win=1,sigma_thr=0.5)
            list_val = []
            maxval = 100.5
            #include scores as b-factor records
            i = 0
            list_res = []
            list_mapval = []
            list_resname = []
            for x in structure_instance.atomList:
                cur_chain = x.chain
                cur_res = x.get_res_no()
                cur_resname = x.res
                cur_id = str(x.get_res_no())+cur_chain
                labelname = Name1+"_"+Name2
                if dict_chain_scores1.has_key(cur_chain):
                    #TODO: diffmap score to be used? model diff or both
                    res_id = str(cur_res) + '_' + x.res
                    try: 
                        val = dict_chain_scores1[cur_chain][res_id]
                        if dict_chain_scores1[cur_chain][res_id] > maxval:
                            val = maxval
                        x.temp_fac = val
                         
                    except KeyError, IndexError: 
                        print 'Residue missing: ',cur_res, cur_chain
                        x.temp_fac = 0.0
                        val = 0.0
                else:
                    x.temp_fac = 0.0
                    val = 0.0
                #first atom
                if i == 0: 
                    prev_id = cur_id
                    prev_chain = cur_chain
                    prev_res = cur_res
                    prev_resname = cur_resname
                    avg_val = val
                #each residue
                if cur_id != prev_id:
                    list_res.append(prev_res)
                    list_resname.append(prev_resname)
                    list_mapval.append(avg_val)
                    list_val = []
                    if cur_chain != prev_chain:
                        dict_model = {}
                        if maxlim < max(list_mapval):
                            maxlim = max(list_mapval)
                        dict_model[labelname] = [list_res,list_mapval] 
                        if not dict_chains_mapval.has_key(prev_chain): 
                            dict_chains_mapval[prev_chain] = OrderedDict()
                        dict_chains_mapval[prev_chain][labelname] = [list_res,list_resname,list_mapval]
                        # Put residue scores into dataframe
                        dict_mapvaldata = OrderedDict()
                        dict_mapvaldata = {
                                (labelname+'_'+prev_chain, residue_label):list_res, 
                                (labelname+'_'+prev_chain, resname_label): list_resname,
                                (labelname+'_'+prev_chain, score_label):list_mapval
                                        }
                        df = pd.DataFrame(dict_mapvaldata)
                        mapval_frames.append(df)
                        
                        list_res = []
                        list_resname = []
                        list_mapval = []
                        
                    print '{} {}'.format(prev_id,avg_val)
                #if not dict_chain_scores.has_key(cur_chain): continue
                prev_id = cur_id
                prev_res = cur_res
                prev_resname = cur_resname
                prev_chain = cur_chain
                avg_val = val
                i += 1
            list_res.append(prev_res)
            list_resname.append(prev_resname)
            list_mapval.append(avg_val)
            if maxlim < max(list_mapval):
                maxlim = max(list_mapval)
            dict_model = {}
            dict_model[labelname] = [list_res,list_mapval] 
            if not dict_chains_mapval.has_key(prev_chain): 
                dict_chains_mapval[prev_chain] = OrderedDict()
            dict_chains_mapval[prev_chain][labelname] = [list_res,list_resname,list_mapval]
                        
            print '{} {}'.format(prev_id,avg_val)
            structure_instance.write_to_PDB(labelname+"_mapval.pdb",hetatom=False)
            
            #save as a dataframe
            # Put residue scores into dataframe
            dict_mapvaldata = OrderedDict()
            dict_mapvaldata = {
                    (labelname+'_'+prev_chain, residue_label):list_res, 
                    (labelname+'_'+prev_chain, resname_label): list_resname,
                    (labelname+'_'+prev_chain, score_label):list_mapval
                            }
            df = pd.DataFrame(dict_mapvaldata)
            mapval_frames.append(df)
    
    mapval_df = pd.concat(mapval_frames, axis=1)
    # Save smoc scores as csv
    csv_path = os.path.join('mapval.csv')
    with open(csv_path, 'w') as path:
        mapval_df.to_csv(path)
    #normalise values
    # for c in dict_chains_mapval:
    #     for k in dict_chains_mapval[c]:
    #         dict_chains_mapval[c][k][1] = [val/maxlim for val in dict_chains_mapval[c][k][1]]
    plt = Plot()
    for c in dict_chains_mapval:
        dict_chains_copy = {}
        scoreout = os.path.join("mapval_"+c+".txt")
        sco = open(scoreout,'w')
        for labelname in dict_chains_mapval[c]:
            list_res,list_resname,list_mapval = dict_chains_mapval[c][labelname]
            dict_chains_copy[labelname] = [list_res,list_mapval]
            for l in xrange(len(list_res)):
                sco.write(str(list_res[l])+"\t"+str("{0:.2f}".format(list_mapval[l]))+"\n")
        sco.close()
        outfile = c+"_mapvalplot.png"
        plt.lineplot(dict_chains_copy,outfile,xlabel='Residue_num',ylabel='MapVal',marker=False,lstyle=False,leg_pos=1.8),#legend_loc='upper right')


if __name__ == '__main__':
    main()