'''
USE THIS SCRIPT TO GENERATE LOCAL MANDERS' OVERLAP COEFFICIENT SCORES
E.g. : Initial and output models from FLEX_EM refinement can be scored against the target map.
OUTPUTS local scores and PDB with B-factor records replaced with SMOC scores (CAN BE COLORED IN CHIMERA)
NOTE: that for models with both protein and nucleic acids, the range of local cross correlation values differs for protein and nucleic acid contents and their scores need to be analysed separately.
*NOTE: Residues whose smoc score calculation fails gets a score of 0.0
NOTE: For multiple model inputs, make sure that they have same chain IDs for comparison
Agnel Praveen Joseph, Maya Topf
'''

import sys
import os
from TEMPy.MapParser import MapParser
from TEMPy.StructureBlurrer import StructureBlurrer
from TEMPy.StructureParser import PDBParser
from TEMPy.ScoringFunctions import ScoringFunctions
import numpy as np
import shutil,glob
from TEMPy.class_arg import TempyParser
from collections import OrderedDict
from TEMPy.ShowPlot import Plot
import json
mrcfile_import=True
try:
    import mrcfile
except ImportError:
    mrcfile_import = False

#from datetime import datetime
EXAMPLEDIR = 'Test_Files'
print 'use --help for help'
print '-m [map] for input map'
print '-p [pdb] for input pdb'
print '-r [resolution]'
#print '-w [residue window length], length of overlapping residue window used for scoring'
#print '--sf [sigma factor], determines the width of the Gaussian used to describe each atom (default 0.187; other values 0.225,0.356,0.425)'
print '\n\n###################################'
#labels for the models [if there are multiple models], fill this if you need customized labels
list_labels = []

tp = TempyParser()
tp.generate_args()
#COMMAND LINE OPTIONS
m1 = tp.args.inp_map1
m2 = tp.args.inp_map2
m = tp.args.inp_map
r1 = tp.args.res1
r2 = tp.args.res2
r = tp.args.res
c1 = tp.args.thr1
c2 = tp.args.thr2
c = tp.args.thr
p = tp.args.pdb
p1 = tp.args.pdb1
p2 = tp.args.pdb2
apix = tp.args.apix
pdir = tp.args.pdir
mdir = tp.args.mdir
plist = tp.args.plist
mlist = tp.args.mlist
smlist = tp.args.smlist
rlist = tp.args.rlist
rigid_out = tp.args.rigidout
rigid_out_prefix = 'rigid'
mode = tp.args.mode
sm = tp.args.sim_map
# the sigma factor determines the width of the Gaussian distribution used to describe each atom
sim_sigma_coeff = 0.225
if not tp.args.sigfac is None:
    sim_sigma_coeff = tp.args.sigfac
#score window
win=9
if mode == 'CA' or mode == 'AR': 
    win = 3
    if c1 is None and c is None: sys.exit('Enter map contour level for this mode') 
    elif c is None: c = c1
if not tp.args.window is None:
    win = tp.args.window

#EXAMPLE RUN
flag_example = False
if len(sys.argv) == 1:
    path_example=os.path.join(os.getcwd(),EXAMPLEDIR)
    if os.path.exists(path_example)==True:
        print "%s exists" %path_example
    flag_example = True

z_score = False

#GET INPUT DATA
if flag_example:
    map_file = os.path.join(path_example,'1akeA_10A.mrc')
    res_map = 10.0
    DATADIR = path_example
    list_to_check = ['1ake_mdl1.pdb','mdl_ss2.pdb','mdl_ss2_all1.pdb']
else:
    
    #multiple maps in a dir
    if not mdir is None:
        if not os.path.isdir(mdir): sys.exit('Directory with maps not found!')
        list_maps = glob.glob1(mdir,'*.mrc')   
        list_maps.extend(glob.glob1(mdir,'*.map'))
        list_maps.extend(glob.glob1(mdir,'*.ccp4'))
    #list of multiple maps
    elif not mlist is None:
        list_maps = mlist
        if rlist is None: sys.exit('Input resolutions for multiple maps with rlist')
        list_res = rlist
    elif m2 is None:
        # for one map and model, m = map, c1 = map contour, c2 = model contour
        if m is None and m1 is not None: 
            m = m1
            list_maps = [m]
            if not r1 is None:
                list_res = [r1]
                r = r1
            else: sys.exit('Enter map resolution')
        elif not m is None: 
            m1 = m
            list_maps = [m]
            if not r is None: list_res = [r]
            else: sys.exit('Enter map resolution')
    elif not m1 is None and not m2 is None:
        #for 2 maps
        list_maps = [m1,m2]
        list_models = []
        if not r1 is None and not r2 is None: list_res = [r1,r2]
        else: sys.exit('Enter map resolutions')
    
    if not pdir is None:
        if not os.path.isdir(pdir): sys.exit('Directory with multiple models not found!')
        list_models = glob.glob1(pdir,'*.pdb') #TODO: file extension
    elif not plist is None:
        list_models = [os.path.abspath(pd) for pd in plist]
    elif p2 is None:
        # 1 model
        if p is None and p1 is not None: 
            p = p1
            list_models = [p]
        elif p is not None:
            list_models = [p]
    elif not p2 is None:
        #2 models
        if p1 is not None: list_models = [p1,p2]
        else:
            # s1 model
            p = p2
            list_models = [p2]
        
        
if len(list_maps) != len(list_res): 
    sys.exit("Number of input map(s) and resolution(s) dont match")  
if len(list_maps) == 0 or len(list_models) == 0: sys.exit('Input atleast one map and a model')      
#change these in case of multiple fitted models with single chain
#labels for the models
if len(list_labels) == 0: list_labels = ['.'.join(os.path.basename(x).split('.')[:-1]) for x in list_models]#['initial','final']
list_styles = [':','-.','--','-','-',':','-.','--','-','-',':','-.','--','-','-',':','-.','--','-','-',':','-.','--','-', \
               '-','--','-','-','--','-','-','--','-','-','--','-','-']#'--'
#index of model to check z-scores
z_score_check = 2
#----------------------------
def model_tree(list_coord1,distpot=3.5,list_coord2=None):
    try: 
        from scipy.spatial import cKDTree
        coordtree = cKDTree(list_coord2)
    except ImportError:
        from scipy.spatial import KDTree
        coordtree = KDTree(list_coord2)
        #if list_coord2 != None: coordtree1 = KDTree(list_coord2)
    if list_coord2 != None: 
        neigh_points = coordtree.query_ball_point(list_coord1,distpot)
            # use count_neighbors if the corresponding indices are not required
        #else: 
        #    neigh_points = coordtree.query_ball_point(coordtree,distpot)
    return neigh_points

def write_rbfile_from_score(scorelist,dict_chain_CA,ch,rigidf,slow=0.25):
    list_sccc = scorelist[:]
    list_sccc.sort()
    ch_coord = ch
    #TODO: sort model number issue
    seqres = dict_chain_CA[0][ch_coord].keys()[:]
    seqres.sort()
    list_ca_coord = [dict_chain_CA[0][ch_coord][x] for x in seqres]
    #lower 25%
    low_list = list_sccc[:int(len(list_sccc)*slow)]
    list_ca_coord_low = [] 
    list_res_low = []     
    for scr in low_list:
        indi = scorelist.index(scr)
        try: resi = int(reslist[indi])
        except TypeError: continue
        #print scr, indi, reslist[indi]
        if dict_chain_CA[0][ch_coord].has_key(resi):
            try: ca_coord = dict_chain_CA[0][ch_coord][resi]
            except KeyError: print 'Cannot write rigid file for :', lab, ch_coord
            list_ca_coord_low.append(ca_coord)
            list_res_low.append(resi)
        else: print resi, dict_chain_CA[0][ch_coord].keys()
        
    list_flex = get_neigh_residues(list_ca_coord_low,list_ca_coord,seqres)
    for l in range(len(list_flex)):
        try:
            if seqres.index(list_flex[l+1]) - seqres.index(list_flex[l]) != 1:
                rigidf.write(
                    `seqres[seqres.index(list_flex[l])+1]`+':'+ch_coord + ' ' + 
                    `seqres[seqres.index(list_flex[l+1])-1]`+':'+ch_coord+"\n")
        except IndexError: pass
    if seqres.index(seqres[-1]) - seqres.index(list_flex[-1]) > 1:
        rigidf.write(
            `seqres[seqres.index(list_flex[-1])+1]`+':'+ch_coord + ' ' + 
            `seqres[-1]`+':'+ch_coord+"\n")
    
def get_neigh_residues(list_ca_coord_low,list_ca_coord,seqres,dist=5.0):
    #find residues within 5A from low scoring and add them to rbs
    neigh_points = model_tree(list_ca_coord_low,dist,list_ca_coord)
    list_neigh = []
    for indi in neigh_points:
        for ii in indi:
            list_neigh.append(seqres[ii])
    #get unique and sort
    setlist = set(list_neigh)
    list_flex = list(setlist)
    list_flex.sort()
    return list_flex

def compare_tuple(tuple1,tuple2):
    for val1, val2 in zip(tuple1, tuple2):
        if type(val2) is float:
            if round(val1,2) != round(val2,2):
                return False
        else:
            if val1 != val2:
                return False
    return True

def save_smoc_csv_plot(pdb_id,smoc_chain_scores):
    plt = Plot()
    #pdb_id = os.path.splitext(pdb_id)[0]
    smoc_csv_file = pdb_id+'_smoc.csv'
    with open(smoc_csv_file,'w') as smocf:
        for chain in smoc_chain_scores:
            if len(smoc_chain_scores[chain]) < 5: continue #skip ambiguities
            list_resnum = []
            list_score = []
            dict_points = {}
            chain_id_edit = chain
            if chain.islower():
                chain_id_edit = chain+'_l'
            chain_score_txt = pdb_id+'_'+chain_id_edit+'.txt'
            cs = open(chain_score_txt,'w')
            for resnum in smoc_chain_scores[chain]:
                smoc_res = [chain]+[str(resnum)] + [str(round(float(
                                            smoc_chain_scores[chain][resnum]),3))]
                smocf.write(','.join(smoc_res)+'\n')
                cs.write(str(resnum)+'\t'+str(round(float(
                                            smoc_chain_scores[chain][resnum]),3))+'\n')
                try:
                    list_resnum.append(int(resnum))
                    list_score.append(round(float(
                                            smoc_chain_scores[chain][resnum]),3))
                except TypeError:
                    continue
            dict_points[chain] = [list_resnum,list_score]
            smoc_plot = pdb_id+'_'+chain_id_edit+'_smocscore.png'
            plt.lineplot(dict_points,smoc_plot,xlabel='Residue_num',ylabel='SMOC',marker=False,lstyle=False,leg_pos=1.8)#,ylim=[-0.1,1.0])
            cs.close()
            
intermed_file = ""
slow = 0.50
shigh = 0.25 # fraction of structure fitted reasonably well initially
#rigid body file
rigidbody_file = None#
#get scores for start pdb
rfilepath = rigidbody_file
if rigidbody_file is not None: rfilepath = os.path.abspath(rigidbody_file)
blurrer = StructureBlurrer()
sc = ScoringFunctions()
fragment_window = True
if tp.args.dist:
    fragment_window = False
    score_by_dist = tp.args.dist
else:
    score_by_dist = False
    
dict_map_scores = OrderedDict()
m_count = 0
dict_str_scores = OrderedDict()
if mrcfile_import:
    from TEMPy.mapprocess import Filter

def read_mapfile(map_path):
    if mrcfile_import:
        mrcobj=mrcfile.open(map_path,mode='r')
        emmap = Filter(mrcobj)
        emmap.fix_origin()
        emmap.set_apix_tempy()
    else:
        mrcobj=MapParser.readMRC(map_path)
        emmap = Filter(mrcobj)
        emmap.fix_origin()
    return emmap

#if model map is provided
sim_map = None
if sm is not None:
    map_file = sm
    smlist = [sm]
    #sim_map = read_mapfile(sm)

for map_file in list_maps:
    #read map file
    emmap = read_mapfile(map_file)
        
    map_name = os.path.basename(map_file).split('.')[0]
    res_map = list_res[m_count]

    if not score_by_dist and tp.args.window is None:
        if res_map < 2.5:
            win = 3
        elif res_map < 3.5:
            win = 5
        elif res_map < 5.0:
            win = 7
        else: win = 9
        
    if m_count == 0 and score_by_dist:
        print 'Considering voxels by distance'
    else:
        print 'Considering voxels by sequence window, size ', win

    list_zscores = []
    curdir = os.getcwd()
    rerun_ct=0
    flag_rerun = 0
    it = 0
    dict_reslist = {}
    dict_res_common = {}
    iter_num = len(list_models)
    #iterate through atomic models
    while iter_num > 0:
        dict_chains_scores = OrderedDict()
        out_iter_pdb = list_models[it]
        if smlist is not None:
            if len(list_models) == len(smlist):
                sim_mapfile = smlist[it]
                if os.path.isfile(sim_mapfile):
                    sim_map = read_mapfile(sim_mapfile)
                    print 'Using simulated map: ',sim_mapfile
        model_name = '.'.join(os.path.basename(out_iter_pdb).split('.')[:-1])
        lab  = '_vs_'.join([map_name,model_name])
        if os.path.isfile(out_iter_pdb):
            #read pdb
            structure_instance=PDBParser.read_PDB_file('pdbfile',
                                                       out_iter_pdb,
                                                       hetatm=True,
                                                       water=False)
        else: 
            print 'PDB file not found:', out_iter_pdb
            iter_num = iter_num - 1
            continue
            
        #get scores
        if mode == 'CA' or mode == 'AR': dict_ch_scores,dict_chain_res = \
            sc.S_OVR(emmap,res_map,structure_instance,c,win=win,
                    rigid_body_file=rfilepath,sim_map=sim_map,
                    sigma_map=sim_sigma_coeff,
                    atom_sel=mode,fragment_score=fragment_window,
                    dist=tp.args.dist)
        else: 
            if fragment_window:
                dict_ch_scores,dict_chain_res, dict_chain_CA = \
                sc.SMOC(emmap,res_map,structure_instance=structure_instance,win=win,
                        rigid_body_file=rfilepath,sim_map=sim_map,
                        sigma_map=sim_sigma_coeff,
                        sigma_thr=2.5,fragment_score=fragment_window,
                        dist=tp.args.dist)
            else:
                dict_ch_scores,dict_chain_res, dict_chain_CA = \
                sc.SMOC(emmap,res_map,structure_instance=structure_instance,win=win,
                        rigid_body_file=rfilepath,sim_map=sim_map,
                        sigma_map=sim_sigma_coeff,
                        sigma_thr=2.5,fragment_score=fragment_window,
                        dist=tp.args.dist, atom_centre='CB')
        
        if rigid_out:
                dict_chain_indices, dict_chain_CA = blurrer.get_coordinates(
                                                    structure_instance)
                rigidf = open(rigid_out_prefix+'_'+lab,'w')

        sum_avg_smoc = 0.
        ct_chain = 0  
        for ch in dict_ch_scores:
            flagch = 1
            dict_res_scores = dict_ch_scores[ch]
            #get res number list (for ref)
            if it == 0:
                dict_reslist[ch] = dict_chain_res[ch][:]
            try: 
                if len(dict_reslist[ch]) == 0: 
                    print 'Chain missing:', ch, out_iter_pdb
                    flagch = 0
                    continue
            except KeyError: 
                print 'Chain not common:', ch, out_iter_pdb
                flagch = 0
                continue
            reslist = dict_reslist[ch]
            if not ch in dict_chains_scores: dict_chains_scores[ch] = {}
            scorelist = []
            for res in reslist:
                #save scores
                try: scorelist.append(dict_res_scores[res])
                except KeyError: 
                    if reslist.index(res) <= 0: 
                        #assign neighbour score
                        try: scorelist.append(dict_res_scores[reslist[reslist.index(res)+1]])
                        except KeyError, IndexError: scorelist.append(0.0)
                    else: 
                        try:  scorelist.append(dict_res_scores[reslist[reslist.index(res)-1]])
                        except KeyError, IndexError: scorelist.append(0.0)
                #save scores for each chain
                curscore = "{0:.2f}".format(round(scorelist[-1],2))
                dict_chains_scores[ch][res] = str(curscore)
            #calculate z-scores
            npscorelist = np.array(scorelist)
            avg_smoc = np.mean(npscorelist)
            try: list_zscores.append((npscorelist-avg_smoc)/np.std(npscorelist))
            except: list_zscores.append(npscorelist-avg_smoc)
            
            sum_avg_smoc += avg_smoc
            ct_chain += 1

            if rigid_out:
                #output rigid body file based on current scores
                if len(reslist) != len(scorelist):
                    print 'Cannot write rigid file for :', lab, ch
                else:
                    write_rbfile_from_score(scorelist,dict_chain_CA,ch,rigidf)

            #print statistics per chain
            print list_models[it],ch, 'avg and mean deviation:', \
                    np.sum(scorelist)/len(scorelist), np.std(scorelist)
            mad = np.median(np.absolute(np.array(scorelist)-np.median(scorelist)))
            print list_models[it],ch, 'median and median deviation:', \
                    np.median(scorelist), mad
    
        #store scores for each structure
        dict_str_scores[lab] = dict_chains_scores
        #include smoc scores as b-factor records
        pdbid = lab
        sc.set_score_as_bfactor(structure_instance,dict_chains_scores,
                                chid=ch,
                                outfile=pdbid+"_smoc.pdb")
        #save and plot smoc scores
        save_smoc_csv_plot(pdbid,dict_chains_scores)
        
        it = it+1
        iter_num = iter_num-1
        #legendlist.append('iter_'+`it`)
        if not ct_chain is 0: print '** Mean SMOC for {} is {}'.format(pdbid,sum_avg_smoc/ct_chain)
        else: print '** Mean SMOC for {} is 0.0'.format(pdbid)
    
    if rigid_out: rigidf.close()
    m_count += 1

# list_chains = dict_chains_scores.keys()
# #plot score per chain
# plt = Plot()
# for ch in dict_chains_scores:
#     it = 0
#     dict_points = OrderedDict()
#     flag_chain = 1
#     #check if chain is there in all models
#     for model in dict_str_scores:
#         if not dict_str_scores[model].has_key(ch):
#             flag_chain = 0
#             break 
#     if flag_chain == 0:
#         print 'Chain absent in one of the models: {}'.format(ch) 
#         continue
#     list_models = dict_str_scores.keys()
#     scoreout = "smoc_"+ch+".txt"
#     if len(list_models) == 1:
#         scoreout = list_models[0]+"smoc_"+ch+".txt"
#     
#     sco = open(scoreout,'w')
#     sco.write("Res_num\t"+'\t'.join(list_models)+"\n")
#     
#     mdl_ini = 0  
#     for model in list_models:
#         ch1 = ch
#         if ch == ' ': ch1 = ''
#         labelname = model+'_'+ch1
#         reslist = []
#         scorelist = []
#         for res in dict_reslist[ch]:
#             flag_res = 1
#             list_ressc = []
#             for model1 in list_models:
#                 try: 
#                     res_sc = dict_str_scores[model1][ch][res]
#                 except KeyError: 
#                     flag_res = 0
#                     break
#             list_ressc.append(res_sc)
#             if flag_res == 0: continue 
#             reslist.append(int(res))
#             scorelist.append(float(dict_str_scores[model][ch][res]))
#             if mdl_ini == 0: sco.write(str(res)+"\t"+'\t'.join(list_ressc)+"\n")
#         if z_score: 
#             npscorelist = np.array(scorelist,dtype='float')
#             zscorelist = (npscorelist-np.mean(npscorelist))/np.std(npscorelist)
#             dict_points[labelname] = [reslist,zscorelist]
#         else: dict_points[labelname] = [reslist,scorelist]
#         it += 1
#         mdl_ini += 1
#     sco.close()
#     outfile = ch+"_smocscoreplot.png"
#     if len(list_models) == 1:
#         outfile = list_models[0]+ch+"_smocscoreplot.png"
#     
#     plt.lineplot(dict_points,outfile,xlabel='Residue_num',ylabel='SMOC',marker=False,lstyle=False,leg_pos=1.8)#,ylim=[-0.1,1.0]),#legend_loc='upper right')  