import sys
import re
import os
from TEMPy.MapParser import MapParser
from TEMPy.StructureBlurrer import StructureBlurrer
from TEMPy.StructureParser import PDBParser
import TEMPy.Vector  as Vector
from TEMPy.ScoringFunctions import ScoringFunctions
import numpy as np
from TEMPy.class_arg import TempyParser
from TEMPy.mapprocess import Filter
from TEMPy.ShowPlot import Plot
from collections import OrderedDict
import glob
import pandas as pd
import gemmi
from ccpem_core.map_tools.mrcfile.get_map_values import *
import numpy as np
import warnings

mrcfile_import=True
try:
    import mrcfile
except ImportError:
    mrcfile_import = False

def read_mapfile(map_path):
    if mrcfile_import:
        mrcobj=mrcfile.open(map_path,mode='r',permissive=True)
        emmap = Filter(mrcobj)
        emmap.fix_origin()
        emmap.set_apix_tempy()
    else:
        mrcobj=MapParser.readMRC(map_path)
        emmap = Filter(mrcobj)
        emmap.fix_origin()
    return emmap

def get_gridtree(dict_chain_CA,dict_chain_scores1):
    #generate a kdtree for residue coordinates
    list_model_coords = []
    list_model_residues = []
    list_model_scores = []
    for ch in dict_chain_CA:
        for res in dict_chain_CA[ch]:
            c = dict_chain_CA[ch][res]
            try: list_model_scores.append(dict_chain_scores1[ch][str(res)+'_'+c[0]])
            except: continue
            list_model_coords.append(c[1:])
            list_model_residues.append(ch+'_'+str(res))
    
    try: 
        from scipy.spatial import cKDTree
        gridtree = cKDTree(list_model_coords)
    except ImportError:
        try:
            from scipy.spatial import KDTree
            gridtree = KDTree(list_model_coords)
        except ImportError: 
            gridtree = None
    return gridtree, list_model_residues, np.array(list_model_scores)

def get_indices_sphere(gridtree,coord,dist=5.0):
    list_points = gridtree.query_ball_point(\
                [coord[0],coord[1],coord[2]], 
                dist)
    return list_points

def calc_z(list_vals,val=None):
    if val is not None:
        try:
            list_vals.remove(val)
        except: pass
    sum_mapval = np.sum(list_vals)
    mean_mapval = sum_mapval/len(list_vals)
    #median_mapval = np.median(list_vals)
    std_mapval = np.std(list_vals)
    if val is None:
        if std_mapval == 0.:
            z = [0.0]*len(list_vals)
        else:
            z = np.round((np.array(list_vals) - mean_mapval)/std_mapval,2)
    else:
        if std_mapval == 0.:
            z = 0.0
        else:
            z = (val - mean_mapval)/std_mapval
    return z

def calc_z_median(list_vals,val=None):
    if val is not None:
        try:
            list_vals.remove(val)
        except: pass
    sum_smoc = np.sum(list_vals)
    median_smoc = np.median(list_vals)
    mad_smoc = np.median(np.absolute(np.array(list_vals)-median_smoc))
    if val is None:
        if mad_smoc == 0.:
            z = [0.0]*len(list_vals)
        else:
            z = np.around((np.array(list_vals) - median_smoc)/mad_smoc,2)
    else:
        if mad_smoc == 0.:
            z = 0.0
        else:
            z = round(((val - median_smoc)/mad_smoc),2)
    return z

def main():
    tp = TempyParser()
    tp.generate_args()
    
    m = tp.args.inp_map
    if m is None: m = tp.args.inp_map1
    elif tp.args.res is None: sys.exit('Input a map, is resolution(required) and contour(optional)')
    else: r = tp.args.res
    plist = tp.args.plist
    mlist = tp.args.mlist
    rlist = tp.args.rlist
    pdir = tp.args.pdir
    
    # the sigma factor determines the width of the Gaussian distribution used to describe each atom
    sim_sigma_coeff = 0.187
    if not tp.args.sigfac is None:
        sim_sigma_coeff = tp.args.sigfac
        
        
    flag_model = 0
    p = tp.args.pdb
    for p in [tp.args.pdb,tp.args.pdb2,tp.args.pdb1]:
        if not p is None: 
            flag_model = 1
            break
    
    if not mlist is None:
        list_maps = mlist
        if rlist is None: sys.exit('Input resolutions for multiple maps with rlist')
        list_res = rlist
    elif not m is None:
        list_maps = [m]
        rlist = [r]
    if not pdir is None:
        if not os.path.isdir(pdir): sys.exit('Directory with multiple models not found!')
        list_models = glob.glob1(pdir,'*.pdb')
        list_models = [os.path.join(os.path.abspath(pdir),pdbfile) for pdbfile in list_models]
    elif not plist is None:
        list_models = [os.path.abspath(s) for s in plist]
    elif not p is None:
        list_models = [p]

    residue_label = 'resnum'
    resname_label = 'resname'
    score_label = 'mapval'
    mapval_frames = []
    m_count = 0
    dict_chains_mapval = {}
    maxlim = 0.0
    for m in list_maps:
        #get details of map
        Name1 = os.path.basename(m).split('.')[0]
        #read map file
        map_target = read_mapfile(m)
        
        resolution_densMap = rlist[m_count]
        m_count += 1
        
        for p in list_models:
            #read the model  
#             structure_instance=PDBParser.read_PDB_file('pdb1',p,
#                                                        hetatm=True,water=False)
            structure_instance = gemmi.read_structure(p)
            
            Name2 = os.path.basename(p).split('.')[0]
            print Name1, Name2
            #get coordinates of chains
            #dict_chain_indices, dict_chain_CA = blurrer.get_coordinates(structure_instance)
            #get map indices corresponding to residues
#             blurrer = StructureBlurrer()
#             dict_chain_scores1 = blurrer.get_map_values(
#                                 structure_instance,map_target,
#                                 resolution_densMap,win=1,sigma_thr=0.5)
            print 'Calculate scores'
            dict_chain_scores1, dict_chain_CA = get_map_values(
                                structure_instance,map_target,
                                resolution_densMap,win=1,sigma_thr=0.5)
            print 'Calculate z scores'
            #get kdtree of model coords
            gridtree, list_model_residues, array_model_mapval = get_gridtree(dict_chain_CA,
                                                         dict_chain_scores1)
            
            list_val = []
            maxval = 100.5
            #include scores as b-factor records
            i = 0
            labelname = Name1+"_"+Name2
            scoreout = labelname+"_mapval.csv"
            sco = open(scoreout,'w')
            sco.write('#chain,residue,score,z_local,z_global\n')
            for model in structure_instance:
                for chain in model:
                    cur_chain = chain.name
                    list_z_local = []
                    list_z_global = calc_z(array_model_mapval)
                    assert len(list_z_global) == len(array_model_mapval)
                    list_res = []
                    list_mapval = []
                    list_resname = []

                    #polymer = chain.get_polymer()
                    for residue in chain:
                        res_id = str(residue.seqid.num)+'_'+residue.name
                        cur_id = res_id
                        cur_resname = residue.name
                        cur_res = residue.seqid.num
                        for atom in residue:
                            #TODO: diffmap score to be used? model diff or both
                            try: 
                                val = dict_chain_scores1[cur_chain][res_id]
                                if dict_chain_scores1[cur_chain][res_id] > maxval:
                                    val = maxval
                                atom.b_iso = val
                                 
                            except KeyError, IndexError: 
                                print 'Residue missing: ',cur_res, cur_chain
                                atom.b_iso = 0.0
                                val = 0.0
                            if not dict_chain_scores1.has_key(cur_chain):
                                atom.b_iso = 0.0
                                val = 0.0
                        avg_val = val
                        list_res.append(cur_res)
                        list_resname.append(cur_resname)
                        list_mapval.append(avg_val)
                        try:
                            ca_coord = dict_chain_CA[cur_chain][cur_res][1:]
                            list_neigh = get_indices_sphere(gridtree,ca_coord,dist=10.0)
                            list_mapval_local = array_model_mapval[list_neigh]
                            z_local = calc_z(list_mapval_local,avg_val)
                            list_z_local.append(z_local)
                        except KeyError:
                            list_z_local.append(0.0) #failed cases
                        
                    dict_model = {}
                    if maxlim < max(list_mapval):
                        maxlim = max(list_mapval)
                    dict_model[labelname] = [list_res,list_mapval] 
                    if not dict_chains_mapval.has_key(cur_chain): 
                        dict_chains_mapval[cur_chain] = OrderedDict()
                    dict_chains_mapval[cur_chain][labelname] = \
                                            [list_res,list_resname,list_mapval]
                    for l in xrange(len(list_res)):
                        residue_scores = ','.join([cur_chain,str(list_resname[l])+
                                                   '_'+str(list_res[l]),
                                                   "{0:.2f}".format(list_mapval[l]),
                                                   "{0:.2f}".format(list_z_local[l]),
                                                   "{0:.2f}".format(list_z_global[l])
                                                   ])
                        sco.write(residue_scores+"\n")
                    # Put residue scores into dataframe
                    dict_mapvaldata = OrderedDict()
                    dict_mapvaldata = {
                            (labelname+'_'+cur_chain, residue_label):list_res, 
                            (labelname+'_'+cur_chain, resname_label): list_resname,
                            (labelname+'_'+cur_chain, score_label):list_mapval
                                    }
                    df = pd.DataFrame(dict_mapvaldata)
                    mapval_frames.append(df)
                    
                    i += 1
            structure_instance.write_pdb(labelname+"_mapval.pdb")
            sco.close()
    del list_model_residues, array_model_mapval
    del dict_chain_CA, dict_chain_scores1
    mapval_df = pd.concat(mapval_frames, axis=1)
    # Save smoc scores as csv
    csv_path = os.path.join('mapval.csv')
    with open(csv_path, 'w') as path:
        mapval_df.to_csv(path)
    #normalise values
    # for c in dict_chains_mapval:
    #     for k in dict_chains_mapval[c]:
    #         dict_chains_mapval[c][k][1] = [val/maxlim for val in dict_chains_mapval[c][k][1]]
    
    plt = Plot()
    for c in dict_chains_mapval:
        dict_chains_copy = {}
        for labelname in dict_chains_mapval[c]:
            list_res,list_resname,list_mapval = dict_chains_mapval[c][labelname]
            dict_chains_copy[labelname] = [list_res,list_mapval]
        outfile = c+"_mapvalplot.png"
        with warnings.catch_warnings():
            warnings.filterwarnings('ignore',category=UserWarning)
            plt.lineplot(dict_chains_copy,outfile,xlabel='Residue_num',
                         ylabel='MapVal',marker=False,lstyle=False,leg_pos=1.8)#legend_loc='upper right')

if __name__ == '__main__':
    main()