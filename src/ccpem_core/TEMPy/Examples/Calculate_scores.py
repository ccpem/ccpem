from TEMPy.MapParser import MapParser
from TEMPy.ScoringFunctions import ScoringFunctions
from TEMPy.StructureParser import PDBParser
from TEMPy.StructureBlurrer import StructureBlurrer
from TEMPy.EMMap import Map
import os,sys
from TEMPy.class_arg import TempyParser
from traceback import print_exc
from collections import OrderedDict
import glob
from TEMPy.mapprocess import Filter
import numpy as np

mrcfile_import=True
try:
    import mrcfile
except ImportError:
    mrcfile_import = False

tp = TempyParser()
tp.generate_args()
pdir = tp.args.pdir
mdir = tp.args.mdir
plist = tp.args.plist
mlist = tp.args.mlist
mdir = tp.args.mdir
smlist = tp.args.smlist
rlist = tp.args.rlist
p = tp.args.pdb
p1 = tp.args.pdb1
p2 = tp.args.pdb2
sm = tp.args.sim_map
m2 = tp.args.inp_map2
#if model maps are provided
sim_map = None
if sm is not None:
    map_file = sm
    smlist = [sm]
#map input
m = tp.args.inp_map
r = tp.args.res
if m is None: 
    m = tp.args.inp_map1
    r = tp.args.res1
    if m is None: 
        sys.exit('Input a map, its resolution(required) and contour(optional)')
    elif tp.args.res1 is None: 
        sys.exit('Input a map, is resolution(required) and contour(optional)')
    else: r = tp.args.res1
elif r is None:
    r = tp.args.res1

if (plist is not None or mlist is not None) and rlist is None:
    sys.exit('Input map resolution(required) and contour(optional)')

if m is not None and r is None:
    sys.exit('Input map resolution(required) and contour(optional)')

list_models=[]
#model input
if not pdir is None:
    if not os.path.isdir(pdir): sys.exit('Directory with multiple models not found!')
    list_models = glob.glob1(pdir,'*.pdb') #TODO: file extension
    list_models = [os.path.join(os.path.abspath(pdir),pdbfile) for pdbfile in list_models]
elif not plist is None:
    list_models = [os.path.abspath(pd) for pd in plist]
elif p2 is None:
    # 1 model
    if p is None and p1 is not None: 
        p = p1
        list_models = [p]
    elif p is not None:
        list_models = [p]
elif not p2 is None:
    #2 models
    if p1 is not None: list_models = [p1,p2]
    else:
        # s1 model
        p = p2
        list_models = [p2]

#optional args
c1 = tp.args.thr
if c1 is None: c1 = tp.args.thr1
c2 = tp.args.thr2
if len(list_models) == 0 and m2 is None:
    #multiple map input
    if not mlist is None:
        mlist = [os.path.abspath(mp) for mp in mlist]
    elif not mdir is None:
        if not os.path.isdir(mdir): sys.exit('Directory with multiple maps not found!')
        mlist = glob.glob1(mdir,'*.mrc') #TODO: file extension
        mlist = [os.path.join(os.path.abspath(mdir),mapfile) for mapfile in mlist]

#calculate model contour
def model_contour(modelmap,t=-1.):
    c2 = 0.0
    if t != -1.0:
        c2 = t*modelmap.std()#0.0
    return c2

def blur_model(p,res=4.0,emmap=False):
    pName = os.path.basename(p).split('.')[0]
    structure_instance=PDBParser.read_PDB_file(pName,p,hetatm=False,water=False)
    blurrer = StructureBlurrer()
    if res is None: sys.exit('Map resolution required..')
    #emmap = blurrer.gaussian_blur(structure_instance, res,densMap=emmap_1,normalise=True)
    modelmap = blurrer.gaussian_blur_real_space(structure_instance, res,sigma_coeff=0.225,densMap=emmap,normalise=True) 
    return pName,modelmap

def read_mapfile(map_path):
    if mrcfile_import:
        mrcobj=mrcfile.open(map_path,mode='r')
        if np.any(np.isnan(mrcobj.data)):
            sys.exit('Map has NaN values: {}'.format(map_path))
        emmap = Filter(mrcobj)
        emmap.set_apix_tempy()
        emmap.fix_origin()
    else:
        mrcobj=MapParser.readMRC(map_path)
        emmap = Filter(mrcobj)
        emmap.fix_origin()
    return emmap

def write_mapfile(mapobj,map_path):
    #TEMPy map
    if not mrcfile_import and mapobj.__class__.__name__ == 'Map':
        mapobj.write_to_MRC_file(map_path)
    #mrcfile map
    elif mrcfile_import:
        newmrcobj = mrcfile.new(map_path,overwrite=True)
        mapobj.set_newmap_data_header(newmrcobj)
        newmrcobj.close()
    #tempy mapprocess map
    else:
        newmrcobj = Map(np.zeros(mapobj.fullMap.shape), 
                        list(mapobj.origin), 
                        mapobj.apix, 'mapname')
        mapobj.set_newmap_data_header(newmrcobj)
        newmrcobj.update_header()
        newmrcobj.write_to_MRC_file(map_path)

def process_global_scores(map_path,
                        map_resolution,
                        map_level,
                        pdb_path_list,
                        map2_path=None,
                        c2=None,
                        directory=None
                        ):
    
    em_map = read_mapfile(map_path)
    if map_level is None:
        map_level = em_map.calculate_map_contour(sigma_factor=1.5)
        print 'Calculated map contour level is', map_level
    # Get score for each structure in list
    dict_table = {}
    dict_scores = {}
    if len(pdb_path_list) > 0:
        list_pdbs = []
        l = 0
        for pdb_path in pdb_path_list:
            pdb_id = os.path.basename(pdb_path)
            pName = os.path.splitext(pdb_id)[0]
            list_pdbs.append(pdb_id)
            if smlist is not None:
                if len(list_models) == len(smlist):
                    sim_mapfile = smlist[l]
                    if os.path.isfile(sim_mapfile):
                        modelmap = read_mapfile(sim_mapfile)
                        print 'Using second map: ',sim_mapfile
                    else: continue
            else:
                pName,modelmap = blur_model(pdb_path, map_resolution, em_map)
            model_map = modelmap
            if modelmap.__class__.__name__ == 'Map':
                 model_map = Filter(modelmap)
            synmap_file = pName+'_syn.mrc'
            write_mapfile(model_map,synmap_file)
            if map_resolution > 20.0: t = 2.0
            elif map_resolution > 10.0: t = 1.0
            elif map_resolution > 6.0: t = 0.5
            else: t = 0.1
            model_level = model_contour(modelmap, t)
            #calculate scores
            get_global_scores(em_map,modelmap,map_level,model_level,dict_scores,pName)
            l += 1
    elif map2_path is not None:
        m2 = map2_path
        list_pdbs = [m2]
        pName = os.path.splitext(os.path.basename(m2))[0]
        em_map2 = read_mapfile(m2)
        if c2 is None:
            c2 = em_map2.calculate_map_contour(sigma_factor=1.5)
            print 'Calculated map2 contour level is', map2_level
        
        get_global_scores(em_map,em_map2,map_level,c2,dict_scores,pName)
    elif mlist is not None:
        list_pdbs = []
        for m2 in mlist:
            list_pdbs.append(m2)
            pName = os.path.splitext(os.path.basename(m2))[0]
            em_map2 = read_mapfile(m2)
            if c2 is None:
                c2 = em_map2.calculate_map_contour(sigma_factor=1.5)
                print 'Calculated map2 contour level for {} is {}'.format(pName,map2_level)
            
            get_global_scores(em_map,em_map2,map_level,c2,dict_scores,pName)
        
    sc = ScoringFunctions()
    
    dict_scores['mi_ov'] = sc.scale_median(
                            dict_scores['overlap'],
                            dict_scores['local_mi'])
    dict_scores['ccc_ov'] = sc.scale_median(
                            dict_scores['overlap'],
                            dict_scores['local_correlation'])

    order = ['overlap','correlation','mi','nmi','local_correlation',
             'local_mi','ccc_ov','mi_ov']
    dict_scores_ordered = OrderedDict()

    for k in order:
        dict_scores_ordered[k] = dict_scores[k]
#     df = pd.DataFrame(dict_scores_ordered,index = list_pdbs)
#     return df
    
    return dict_scores_ordered, list_pdbs

def get_global_scores(emmap1,emmap2,c1,c2,dict_scores_hits,Name2):
    sc = ScoringFunctions()
    print Name2
    #OVR
    try:
        ccc_mask,ovr = sc.CCC_map(emmap1,emmap2,c1,c2,3,meanDist=True)
        print 'Percent overlap:', ovr
        if ovr < 0.0: ovr = 0.0
    except IOError:
        print 'ccc and overlap score calculation failed for', Name2
        ovr = 0.0
    if ovr < 0.02:
        print "Maps do not overlap: ", Name2
        ccc_mask = ccc = -1.0
        mi_mask = mi = nmi = 0.0
    else:
        #SCCC
        print 'Local correlation score: ', ccc_mask
        if ccc_mask < -1.0 or ccc_mask > 1.0:
            ccc_mask = -1.0
        #LMI
        try:
            mi_mask = sc.MI(emmap1,emmap2,c1,c2,3)
            print 'Local Mutual information score: ', mi_mask
            if mi_mask < 0.0: mi_mask = 0.0
        except:
            print 'MI score calculation failed for', Name2
            mi_mask = 0.0
        
        #CCC
        try:
            ccc,overlap = sc.CCC_map(emmap1,emmap2,c1,c2,1,meanDist=True)
        except:
            print 'ccc score calculation failed for', Name2
        #SCCC
        print 'Correlation score: ', ccc
        if ccc < -1.0 or ccc > 1.0:
            ccc = -1.0
            
          #NMI
        try:
            nmi = sc.MI(emmap1,emmap2,c1,c2,1,None,None,True)
            print 'Normalized Mutual information score:', nmi
            if nmi < 0.0: nmi = 0.0
        except:
            print 'NMI score calculation failed for', Name2
            nmi = 0.0
          #MI
        try:
            mi = sc.MI(emmap1,emmap2,c1,c2,mode=1,weight=False,layers1=20,layers2=20)
            print 'Mutual information score:', mi
            if mi < 0.0: mi = 0.0
        except:
            print 'MI score calculation failed for', Name2
            mi = 0.0
        
    try: dict_scores_hits['local_correlation'].append(ccc_mask)
    except KeyError: dict_scores_hits['local_correlation'] = [ccc_mask]
    try: dict_scores_hits['local_mi'].append(mi_mask)
    except KeyError: dict_scores_hits['local_mi'] = [mi_mask] 
    try: dict_scores_hits['overlap'].append(ovr)
    except KeyError: dict_scores_hits['overlap'] = [ovr]
    try: dict_scores_hits['correlation'].append(ccc)
    except KeyError: dict_scores_hits['correlation'] = [ccc]
    try: dict_scores_hits['nmi'].append(nmi)
    except KeyError: dict_scores_hits['nmi'] = [nmi]
    try: dict_scores_hits['mi'].append(mi)
    except KeyError: dict_scores_hits['mi'] = [mi]

def write_to_file(headers, dict_scores, scorefile='tempy_scores.txt'):
    with open(scorefile,'w') as f:
        f.write('{}\t{}\n'.format('Scores','\t'.join(headers)))
        for k in dict_scores:
            assert len(dict_scores[k]) == len(headers)
            str_scores = [str(round(sc,2)) for sc in dict_scores[k]]
            f.write("{}\t{}\n".format(k,'\t'.join(str_scores)))

'''
    #CD
    try:
        chm = sc._surface_distance_score(emmap_1,emmap_2,c1,c2,'Minimum')
        if chm == 0.0 or chm is None:
            chm = sc._surface_distance_score(emmap_1,emmap_2,c1,c2,'Mean')
        print 'Surface distance score: ', chm
        if chm < 0.0: chm = 0.0
    except:
        print 'Exception for surface distance score'
        print_exc()
        chm = 0.0
    try:
        #nv = sc.normal_vector_score(emmap_1,emmap_2,float(c1)-(emmap_1.std()*0.05),float(c1)+(emmap_1.std()*0.05),None)
        nv = sc.normal_vector_score(emmap_1,emmap_2,float(c1),float(c1)+(emmap_1.std()*0.05),'Minimum')
        if nv == 0.0 or nv is None: nv = sc.normal_vector_score(emmap_1,emmap_2,float(c1),float(c1)+(emmap_1.std()*0.05))
        print 'Normal vector score: ', nv
        if nv < 0.0: 
        nv = 0.0
    except:
        print 'Exception for NV score'
        print_exc()
        nv = 0.0
    
    try: dict_scores_hits['chamfer_distance'].append(chm)
    except KeyError: dict_scores_hits['chamfer_distance'] = [chm]
    try: dict_scores_hits['normal_vector'].append(nv)
    except KeyError: dict_scores_hits['normal_vector'] = [nv]

'''
def main():
    dict_scores, list_pdbs = process_global_scores(m,r,c1,list_models,m2,c2)
    write_to_file(list_pdbs,dict_scores)
            
if __name__ == '__main__':
    sys.exit(main())
            
