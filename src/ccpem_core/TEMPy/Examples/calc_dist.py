import sys
import re
import os
from TEMPy.MapParser import MapParser
from TEMPy.StructureBlurrer import StructureBlurrer
from TEMPy.StructureParser import PDBParser
import TEMPy.Vector  as Vector
from TEMPy.ScoringFunctions import ScoringFunctions
import numpy as np
from TEMPy.class_arg import TempyParser
import json
from collections import OrderedDict
from TEMPy.ShowPlot import Plot

tp = TempyParser()
tp.generate_args()
p1 = tp.args.pdb1
p2 = tp.args.pdb2

num_window = 5
if not tp.args.window is None:
    num_window = tp.args.window
assert os.path.isfile(p1)
assert os.path.isfile(p2)
Name1 = os.path.basename(p1).split('.')[0]
Name2 = os.path.basename(p2).split('.')[0]

#NW alignment 
def nw_2seq(seq1,seq2):
    try:
        #NW from biopython
        import Bio.pairwise2
        aln1 = Bio.pairwise2.align.globalxx(''.join(seq1),''.join(seq2))[0]
        align_seq1,align_seq2 = list(aln1[0]),list(aln1[1])
    except ImportError:
        return seq1,seq2
    return align_seq1,align_seq2

def dist_coord(coord1,coord2):
    try: dist = np.sqrt(np.sum(np.square(np.array(coord1)-np.array(coord2))))
    except: return 10.0
    return dist
    
plt = Plot()
blurrer = StructureBlurrer()

structure_instance1=PDBParser.read_PDB_file('pdb1',p1,hetatm=False,water=False)
#get coordinates of chains
dict_chain_indices1, dict_chain_CA1 = blurrer.get_coordinates(structure_instance1)

structure_instance2=PDBParser.read_PDB_file('pdb2',p2,hetatm=False,water=False)
#get coordinates of chains
dict_chain_indices2, dict_chain_CA2 = blurrer.get_coordinates(structure_instance2)

for c in dict_chain_CA1[0]:
    dict_points = OrderedDict()
    labelname = '__'.join([Name1,Name2,c])
    #print dict_chain_CA1[c].keys()
    seq_res1 = dict_chain_CA1[0][c].keys()[:]
    seq_res2 = dict_chain_CA2[0][c].keys()[:]
    seq_res1.sort()
    print seq_res1
    list_dist = []
    list_res = []
    for n in range(len(seq_res1)):
        rs = seq_res1[n]
        #ca coord of res from prot1
        ca_coord1 = dict_chain_CA1[0][c][rs]
        if not rs in dict_chain_CA2[0][c]: continue
        #ca coord of res from prot1
        ca_coord2 = dict_chain_CA2[0][c][rs]
        #dist btw coords
        dist = dist_coord(ca_coord1,ca_coord2)
        sum_dist = dist
        ct_dist = 1
        list_coord1_all = []
        list_coord1_all.extend(dict_chain_indices1[0][c][rs])
        list_coord1 = [ca_coord1]
        for i in range((num_window-1)/2):
            try: 
                list_coord1.append(dict_chain_CA1[0][c][seq_res1[n-i-1]])
                list_coord1_all.extend(dict_chain_indices1[0][c][seq_res1[n-i-1]])
            except IndexError: pass
            try: 
                list_coord1.append(dict_chain_CA1[0][c][seq_res1[n+i+1]])
                list_coord1_all.extend(dict_chain_indices1[0][c][seq_res1[n+i+1]])
            except IndexError: pass
            try:
                if seq_res1[n-i-1] in dict_chain_CA2[0][c]:
                    sum_dist += dist_coord(dict_chain_CA1[0][c][seq_res1[n-i-1]],dict_chain_CA2[0][c][seq_res1[n-i-1]])
                    ct_dist += 1
            except IndexError: pass
            try:
                if seq_res1[n+i+1] in dict_chain_CA2[0][c]:
                    sum_dist += dist_coord(dict_chain_CA1[0][c][seq_res1[n+i+1]],dict_chain_CA2[0][c][seq_res1[n+i+1]])
                    ct_dist += 1
            except IndexError: pass
        avg_dist = float(sum_dist)/ct_dist
        list_dist.append(avg_dist)
        list_res.append(rs)
    print list_dist
    print `rs`+c, avg_dist
    for x in structure_instance1.atomList:
        cur_chain = x.chain
        if cur_chain == c:
            cur_res = x.get_res_no()
            try:
                ind = list_res.index(cur_res)
            except ValueError:
                x.temp_fac = 25.0 
                continue
            x.temp_fac = list_dist[ind]
            
        
    dict_points[labelname] = [list_res,list_dist]
    outfile = labelname+"_CAdistplot"
    plt.lineplot(dict_points,outfile,xlabel='Residue_num',ylabel='CA dist')
    with open(labelname+"_CAdistdata.json", 'w') as fp:
        json.dump(dict_points, fp)
    structure_instance1.write_to_PDB(Name1+"_cadist.pdb")