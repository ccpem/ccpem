#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import json
import os
import datetime
import sys
import copy
from ccpem_core import ccpem_utils
from . import process_utils
from ccpem_core import settings


class CCPEMProcessCustomFinish(object):
    '''
    Class for custom process finish.
    '''
    def __init__(self):
        pass

    def on_finish(self, parent_process=None):
        raise NotImplementedError('Subclasses should implement this')

class CCPEMProcess(object):
    '''
    Class to store meta data for ccpem process.
    N.B. processes run parallel must be given different names
    
    All args are optional, but at least one of ``command`` and ``import_json``
    must be given. 
    
    Args:
        name: Name for the process, as a string.
        command: Name or full path of a command to run, as a string.
        args: Arguments for the command, as a list of strings.
        env: Environment definitions for the command, as a dictionary, which
            will be used to update the current environment before the command
            is run.
        location: Working directory location for the command, as a string. If
            ``None``, the current directory will be used.
        status: Initial status for the job, as a string. Default is 'ready'.
        stdin: Input which will be passed to the command's standard input after
            the process has been started, as a string.
        stdout: Full path name to use for the command's standard output, as a
            string. If ``None``, a file will be created in the job's working
            directory with a name based on the process name.
        stderr: Full path name to use for the command's standard error, as a
            string. If ``None``, a file will be created in the job's working
            directory with a name based on the process name.
        metadata: A dictionary of metadata to attach to the job.
        import_json: Name or full path of a file containing job data in JSON
            format. If this argument is given, the job will be loaded from the
            JSON file and no other arguments will have any effect.
        on_finish_custom: An instance of CCPEMProcessCustomFinish, which will
            be called after (and only if) the command has finished
            successfully.
        log_parser: A log parser. When the process runs, the standard output
            will be passed to this parser's ``parse_stream()`` method.
            NOTE: the log parser feature is still experimental, do not use this
            for real jobs!
    '''
    def __init__(self,
                 name=None,
                 command=None,
                 args=None,
                 env=None,
                 location=None,
                 status='ready',
                 stdin=None,
                 stdout=None,
                 stderr=None,
                 metadata=None,
                 import_json=None,
                 on_finish_custom=None,
                 log_parser=None,
                 link_prev_process=None):
        if import_json is not None:
            # Set default values for attributes that are not serialized
            self.on_finish_custom = None
            self.log_parser = None
            self.import_json(jsonfile=import_json)
        else:
            self.name = name
            # Must deep copy as commands dictionary is a class variable and can get corrupted by
            # multiple calls e.g. running same program > 1 times from main GUI
            command = copy.deepcopy(command)
            if not isinstance(command, list):
                self.command = [command]
            else:
                self.command = command
            if args is not None:
                if not isinstance(args, list):
                    args = [args]
                self.command += args

            # Check command can be found
            assert settings.which(program=self.command[0]) is not None,\
                   "Cannot find command {}".format(self.command[0])

            # Sanity check: command args cannot be None
            for n, arg in enumerate(self.command):
                if arg is None:
                    raise ValueError('Command args must not be None')
                # All args must be string type
                if not isinstance(arg, str):
                    self.command[n] = str(arg)
            self.env = env
            self.status = status
            self.location = location
            if self.location is None:
                self.location = os.getcwd()
            assert os.path.exists(self.location)
            self.stdin = stdin
            self.stdout = stdout
            if self.name is not None:
                name_prefix = self.name.lower() + '_'
                for char in [' ', '(', ')']:
                    name_prefix = name_prefix.replace(char, '')
            else:
                name_prefix = ''
            if self.stdout is None:
                self.stdout = os.path.join(self.location,
                                           name_prefix + 'stdout.txt')
            self.stderr = stderr
            if self.stderr is None:
                self.stderr = os.path.join(self.location,
                                           name_prefix + 'stderr.txt')
            if metadata is not None:
                assert isinstance(metadata, dict)
            self.metadata = metadata
            self.pid = None
            if name_prefix == '':
                json_name = '.proc_' + '.json'
            else:
                json_name = '.proc_' + name_prefix[0:-1] + '.json'
            self.json = os.path.join(self.location, json_name)
            self.json_pipeline = None
            self.on_finish_custom = on_finish_custom
            # Only for testing - do not set a log parser for real jobs!
            self.log_parser = log_parser
        self.link_prev_process = link_prev_process
        # Check path lengths are less than 255 to prevent problems on all pre
        # Windows 10
        if sys.platform == 'win32':
            for path in [self.json,
                         self.location,
                         self.stdout,
                         self.stderr]:
                if path is not None:
                    if len(path) > 255:
                        print 'Warning: path exceeds 255 characters'
                        print 'PATH NAME : ', path

    def set_metadata(self,
                     key,
                     value):
        if self.metadata is None:
            self.metadata = {}
        self.metadata[key] = value
        self.export_json()

    def get_metadata(self, key):
        with open(self.json, 'r') as json_file:
            metadata = json.load(fp=json_file)['metadata']
            self.metadata = metadata
            if self.metadata is not None:
                if key in self.metadata.keys():
                    return self.metadata[key]
            else:
                return None

    def set_status_ready(self):
        self.status = 'ready'
        self.export_json()

    def set_status_finished(self):
        self.status = 'finished'
        self.export_json()

    def set_status_running(self):
        self.status = 'running'
        self.export_json()

    def set_status_failed(self):
        self.status = 'failed'
        self.export_json()

    def pre_start(self):
        self.write_header_stdout()

    def check_link_to_previous_jobs(self):
        failed = False
        if self.link_prev_process is not None:
            if isinstance(self.link_prev_process,list):
                for job in self.link_prev_process:
                    try:
                        if job.status != 'finished':
                            failed = True
                            self.on_failed()
                    except AttributeError:
                        pass
        return failed
    
    def on_start(self, pid, json_pipeline=None):
        self.pid = pid
        self.json_pipeline = json_pipeline
        failed = self.check_link_to_previous_jobs()
        if not failed: 
            self.set_status_running()

    def on_finish(self):
        self.write_footer_stdout()
        if self.on_finish_custom is not None:
            self.on_finish_custom.on_finish(parent_process=self)
        self.set_status_finished()

    def get_status(self):
        '''
        Updates from json
        '''
        if os.path.exists(self.json):
            with open(self.json, 'r') as json_file:
                return json.load(fp=json_file)['status']
        else:
            return 'ready'

    def on_failed(self):
        self.write_footer_stdout(failed=True)
        self.set_status_failed()
        print 'CCP-EM process "{}" failed in job directory "{}"'.format(self.name, self.location)

    def kill(self):
        print 'kill job from job pid : ', self.pid
        process_utils.kill_process(pid=self.pid)

    def export_json(self):
        # Export
        json_dict = {'name': self.name,
                     'command': self.command,
                     'env': self.env,
                     'location': self.location,
                     'status': self.status,
                     'stdin': self.stdin,
                     'stdout': self.stdout,
                     'stderr': self.stderr,
                     'metadata': self.metadata,
                     'json': self.json,
                     'json_pipeline': self.json_pipeline,
                     'pid': self.pid}
        ccpem_utils.atomic_write_json(json_dict,
                                      self.json,
                                      sort_keys=True,
                                      separators=(',', ': '),
                                      indent=4)

    def import_json(self, jsonfile):
        if os.path.exists(jsonfile):
            with open(jsonfile, 'r') as json_file:
                self.__dict__.update(json.load(fp=json_file))
                self.export_json()

    def write_header_stdout(self):
        '''
        Write command header to stdout.
        '''
        # Header
        h_str = 'CCP-EM Process | '
        h_str += str(self.name)
        header = ccpem_utils.print_header(message=h_str, return_str=True)
        # Command
        c_str = ccpem_utils.print_sub_sub_header(message='Command',
                                                 return_str=True)
        # Use pipes.quote if available to quote arguments correctly
        # (for example if they contain spaces or special characters)
        try:
            from pipes import quote
        except ImportError:
            # If pipes is not available, just use str() instead
            quote = str
        c_str += ' '.join(quote(arg) for arg in self.command)
        if self.stdin is not None:
            c_str += ' << eof\n' + self.stdin + '\neof'
        c_str += '\n'
        c_str += ccpem_utils.return_footer()
        
        with open(self.stdout, 'a') as stdout:
            stdout.write(header)
            stdout.write(c_str)

    def write_footer_stdout(self, failed=False):
        # Get version tag
        version = ccpem_utils.CCPEMVersion()
        v_str = 'CCP-EM version {0} {1}'.format(
            str(version.version),
            str(version.git_revision))
        footer = ccpem_utils.print_sub_header(
            message=v_str,
            return_str=True)

        # Get process status and date tag
        if failed:
            f_str = 'CCP-EM process failed '
        else:
            f_str = 'CCP-EM process finished '
        f_str += datetime.datetime.now().strftime("%A, %d. %B %Y %I:%M%p")
        footer += ccpem_utils.print_sub_header(
            message=f_str,
            return_str=True)
        footer += ccpem_utils.return_footer()

        # Append stdout with CCP-EM process information in footer
        with open(self.stdout, 'a') as stdout:
            stdout.write(footer)

