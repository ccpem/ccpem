#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

from __future__ import print_function

import os
import shutil
import time
import tempfile
import unittest

from ccpem_core.process_manager.ccpem_pipeline import CCPEMPipeline
from ccpem_core.process_manager.ccpem_process import CCPEMProcess
from ccpem_core.settings import which


class CCPEMPipelineTest(unittest.TestCase):
    '''
    Tests for CCPEMPipeline
    '''
    def setUp(self):
        '''
        Setup test output directory.
        '''
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        if os.path.exists(self.test_output):
            shutil.rmtree(self.test_output)

    def test_pipeline(self):
        '''
        Test pipeline
        '''
       
        # Change working directory
        os.chdir(self.test_output)
        
        # Create stage one dummy processes, creates file after delay
        stage1_processes = []
        output_files = []
        for variant in ['test_one', 'test_two', 'test_three']:
            output_files.append(variant + '_test.log')
            command = which('ccpem-python')
            args = ['-m', 'ccpem_core.process_manager.tests.dummy_task', variant]
            name = 'Dummy task ' + variant
            process = CCPEMProcess(
                name=name,
                command=command,
                args=args,
                location=self.test_output,
                stdin=None)
            stage1_processes.append(process)
        
        # Create stage two process, check all out files from stage one
        command = which('ccpem-python')
        args = ['-m', 'ccpem_core.process_manager.tests.dummy_task2'] + output_files
        name = 'Dummy task2'
        stage2_process = CCPEMProcess(
            name=name,
            command=command,
            args=args,
            location=self.test_output,
            stdin=None)

        # Ensure stage 2 runs after all stage 1 jobs complete
        pl = [stage1_processes, [stage2_process]]

        # Create pipeline
        pipeline = CCPEMPipeline(pipeline=pl)

        # Run and check not thread locked
        start_time = time.time()
        pipeline.start()
        elapsed_time = time.time() - start_time
        assert(elapsed_time < 0.1)

        # Monitor jobs
        delay = 0.25
        time_out = 15.0
        time_run = 0
        completed_ok = False

        # Wait for finsih
#         print pipeline.get_status()
        while pipeline.get_status() != 'finished':
            print ('Pipeline test running for {0} secs (timeout = {1})'.format(
                time_run,
                time_out))
            time.sleep(delay)
            time_run += delay
            assert time_run < time_out

        # Check expected output exists
        path = os.path.join(self.test_output, 'check.log')
        if os.path.exists(path):
            completed_ok = True
        assert completed_ok

if __name__ == '__main__':
    unittest.main()
