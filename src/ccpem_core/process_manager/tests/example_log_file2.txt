
________________________________________________________________________________

____________________ CCP-EM Process | Refmac refine (global) ___________________

__ Command _____________________________________________________________________
/home/colin/software/ccpem/ccpem_centos6_nightly_170901/bin/refmac5 xyzin /home/colin/tmp/ccpem/Refmac5_1/in_map.pdb hklin /home/colin/tmp/ccpem/Refmac5_1/starting_map.mtz xyzout /home/colin/tmp/ccpem/Refmac5_1/refined.pdb atomsf /home/colin/software/ccpem/ccpem_centos6_nightly_170901/lib/data/atomsf_electron.lib  << eof
labin FP=Fout0 PHIB=Pout0
make hydr no
solvent no
ncycle 3
weight auto
BFACtor SET 40.0
reso 4.5
ridge dist sigma 0.01
ridge dist dmax 4.2

end
eof
________________________________________________________________________________
<B><FONT COLOR="#FF0000"><!--SUMMARY_BEGIN-->
<html> <!-- CCP4 HTML LOGFILE -->
<hr>
<!--SUMMARY_END--></FONT></B>
<B><FONT COLOR="#FF0000"><!--SUMMARY_BEGIN-->
<pre>
 
 ###############################################################
 ###############################################################
 ###############################################################
 ### CCP4 7.0: Refmac              version 5.8.0158 : 03/10/16##
 ###############################################################
 User: unknown  Run date: 10/10/2017 Run time: 18:09:35 


 Please reference: Collaborative Computational Project, Number 4. 2011.
 "Overview of the CCP4 suite and current developments". Acta Cryst. D67, 235-242.
 as well as any specific reference in the program write-up.

<!--SUMMARY_END--></FONT></B>
 $TEXT:Reference1: $$ Main reference $$ 
   "REFMAC5 for the refinement of macromolecular crystal structures:"
   G.N.Murshudov, P.Skubak, A.A.Lebedev, N.S.Pannu, R.A.Steiner, R.A.Nicholls, M.D.Winn, F.Long and A.A.Vagin,(2011) 
   Acta Crystallogr. D67, 355-367

 $$
 $SUMMARY :Reference1:  $$ Refmac: $$
 :TEXT:Reference1: $$

 $TEXT:Reference2: $$ Secondary reference $$ 
   "Refinement of Macromolecular Structures by the  Maximum-Likelihood Method:"
   G.N. Murshudov, A.A.Vagin and E.J.Dodson,(1997)
   Acta Crystallogr. D53, 240-255
   EU  Validation contract: BIO2CT-92-0524

 $$
 $SUMMARY :Reference2:  $$ Refmac: $$
 :TEXT:Reference2: $$

  Data line--- labin FP=Fout0 PHIB=Pout0
  Data line--- make hydr no
  Data line--- solvent no
  Data line--- ncycle 3
  Data line--- weight auto
  Data line--- BFACtor SET 40.0
  Data line--- reso 4.5
  Data line--- ridge dist sigma 0.01
  Data line--- ridge dist dmax 4.2
  Data line--- END

 OPENED INPUT MTZ FILE 
 Logical Name: HKLIN   Filename: /home/colin/tmp/ccpem/Refmac5_1/starting_map.mtz 

===> Warning: Figure of merit of phases has not been assigned
===> Warning: They will be assumed to be equal to 1.0

    ****                     Input and Default parameters#                      ****


Input coordinate file.  Logical name - XYZIN actual file name  - /home/colin/tmp/ccpem/Refmac5_1/in_map.pdb
Output coordinate file. Logical name - XYZOUT actual file name - /home/colin/tmp/ccpem/Refmac5_1/refined.pdb
Input reflection file.  Logical name - HKLIN actual file name  - /home/colin/tmp/ccpem/Refmac5_1/starting_map.mtz
Output reflection file. Logical name - HKLOUT actual file name - HKLOUT

Cell from mtz :    75.000    75.000    75.000    90.000    90.000    90.000
Space group from mtz: number -    1; name - P 1

  Refinement type                        : Restrained

Initial B values of all atoms will be set to     40.000


    ****                           Makecif parameters                           ****

Dictionary files for restraints : /home/colin/software/ccpem/ccpem_centos6_nightly_170901/lib/data/monomers/mon*cif
Parameters for new entry and VDW: /home/colin/software/ccpem/ccpem_centos6_nightly_170901/lib/data/monomers/ener_lib.cif
    Apart from amino acids and DNA/RNA all monomers will be checked to see if atom names and connectivity is correct
    Hydrogens will not be used
    Links between monomers will be checked. Only those links present in the coordinate file will be used
    Standard sugar links will be analysed and used
    For new ligands "ideal restraint" values will be taken from the energetic libary ener_lib.cif
    Symmetry related links will be analysed and used
    Cis peptides will be found and used automatically


  Residual                               : Phase modified Maximum Likelihood for Fs
 Phase Blurred by  Scale*Exp(-B s**2)    :    1.00    0.00

    ****                    Least-square scaling parameters                     ****

Overall scale
Overall B value
Overall anisotropic B with tr(B) = 0.0


    ****                 Map sharpening parameters and methods                  ****

No map sharpening will be performed

  Method of minimisation                 : Sparse Matrix
  Experimental sigmas used for weighting
  Number of Bins and width:    20       0.0025
  Refinement of individual isotropic Bfactors
  Refinement resln        :    75.0000  4.5063
  Estimated number of reflections :      13756
  Auto weighting. An attempt
  Refinement cycles       :     3
  Scaling type                           :
          Bulk solvent using using Babinet principle

  Estimation of D/Sigma in resolution bins
  using  working set of reflns with experimental sigmas
  Input phases not used in SigmaA estimation

  Scaling and SigmaA resln:    75.0000  4.5063

  Damping factors:     1.0000  1.0000


    ****                    Geometry restraints and weights                     ****


                                              Sigma:
 Bonding distances
          Weight =  1.00

 Bond angles
          Weight =  1.00

 Planar groups (all and main chain)
          WEIGHT= 1.00 1.00

 Chiral centers
          Weight= 1.00

 NON-BONDED CONTACTS
          Overall weight                          =  1.00
          Sigma for simple VDW                    =  0.20
          Sigma for VDW trhough torsion angle     =  0.20
          Sigma for HBOND                         =  0.20
          Sigma for metal-ion                     =  0.20
          Sigma for DUMMY and other atom          =  0.30
          Distance for donor-accepetor = vdw1+vdw2+(-0.30)
          Distance for acceptor - H    = vdw1+     ( 0.10)
          VDW distance through torsion = vdw1+vdw2+(-0.30)
          Distance for DUMMY-others    = vdw1+vdw2+(-0.70)

 TORSION ANGLES
          Weight= 1.00

 THERMAL FACTORS
          Weight= 1.00
     Main chain bond (1-2 neighbour)          1.5A**2
     Main chain angle (1-3 neighbour)         2.0A**2
     Side chain bond                          3.0A**2
     Side chain angle                         4.5A**2

 RESTRAINTS AGAINST EXCESSIVE SHIFTS
     Positional parameters                   0.00A
     Thermal parameters                      0.00A
     Occupancy parameters                    0.00

 RADIUS OF CONFIDENCE
     Positional parameters 0.00A
     Thermal parameters    0.00A**2
     Occupancy parameters  0.00

Restraints on changes of interatomic distances is appplied

    ****                    Distance shift penlty parameters                    ****

     Sigma:           0.0100
     Maximum distance 4.2000

Monitoring style is "MEDIUM". Complete information will be printed out in the
first and last cycle. In all other cycles minimum information will be printed out
Sigma cutoffs for printing out outliers
If deviation of restraint parameter > alpha*sigma then information will be printed out
Distance outliers      10.000
Angle outliers         10.000
Torsion outliers       10.000
Chiral volume outliers 10.000
Plane outliers         10.000
Non-bonding outliers   10.000
B value  outliers      10.000
---------------------------------------------------------------

 Input file :/home/colin/tmp/ccpem/Refmac5_1/in_map.pdb
  ------------------------------
  ---  LIBRARY OF MONOMERS   ---
 _lib_name         mon_lib
 _lib_version      5.44
 _lib_update       30/05/14
  ------------------------------
  NUMBER OF MONOMERS IN THE LIBRARY          : 13516
                with complete description    : 13516
  NUMBER OF MODIFICATIONS                    :    63
  NUMBER OF LINKS                            :    73
  I am reading libraries. Please wait.
      - energy parameters
      - monomer"s description (links & mod )

FORMATTED      OLD     file opened on unit  45
<B><FONT COLOR="#FF0000"><!--SUMMARY_BEGIN-->
Logical name: ATOMSF, Filename: /home/colin/software/ccpem/ccpem_centos6_nightly_170901/lib/data/atomsf_electron.lib
<!--SUMMARY_END--></FONT></B>

  Number of atoms    :    1656
  Number of residues :     214
  Number of chains   :       1
  I am reading library. Please wait.
                mon_lib.cif
  WARNING : CIS peptide bond is found, angle =      1.90
            ch:AA   res:  86  PHE              -->  87  PRO
  --------------------------------
  --- title of input coord file ---

  PDB_code:XXXX
  PDB_name:----
  PDB_date:XX-XXX-XX
  --------------------------------
  Number of chains                  :       1
  Total number of monomers          :     214
  Number of atoms                   :    1656
  Number of missing atoms           :       0
  Number of rebuilt atoms           :       0
  Number of unknown atoms           :       0
  Number of deleted atoms           :       0

  Number of bonds restraints    :    1680
  Number of angles restraints   :    2264
  Number of torsions restraints :    1143
  Number of chiralities         :     254
  Number of planar groups       :     299


 loop_
     _atom_type_symbol
     _atom_type_scat_Cromer_Mann_a1
     _atom_type_scat_Cromer_Mann_b1
     _atom_type_scat_Cromer_Mann_a2
     _atom_type_scat_Cromer_Mann_b2
     _atom_type_scat_Cromer_Mann_a3
     _atom_type_scat_Cromer_Mann_b3
     _atom_type_scat_Cromer_Mann_a4
     _atom_type_scat_Cromer_Mann_b4
     _atom_type_scat_Cromer_Mann_c


  N      0.1022   0.2451   0.3219   1.7481   0.7982   6.1925   0.8197  17.3894   0.0000
  C      0.0893   0.2465   0.2563   1.7100   0.7570   6.4094   1.0487  18.6113   0.0000
  S      0.2497   0.2681   0.5628   1.6711   1.3899   7.0267   2.1865  19.5377   0.0000
  O      0.0974   0.2067   0.2921   1.3815   0.6910   4.6943   0.6990  12.7105   0.0000


 Number of distances         :           0
 Number of angles            :           0
 Number of torsions          :           0
 Number of planes            :           0
 Number of chirals           :           0
 Number of intervals         :           0
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
                        Standard  External       All
                Bonds:      1680         0      1680
               Angles:      2264         0      2264
              Chirals:       254         0       254
               Planes:       299         0       299
             Torsions:      1143         0      1143
            Intervals:         0         0         0
--------------------------------------------------------------------------------


  Number of harmonic restraints       =            0
  Number of atoms in special position =            0
 -----------------------------------------------------


    ****                           Info from mtz file                           ****

FreeR flag                         -999
Number of "free" reflections          0
Number of   all  reflections       9690
 Warning ==>The number of free reflections with flag         -999  is zero
 Warning ==> Switching off use of free R
--------------------------------------------------------------------------------
 Number of reflections in file       9690
 Number of reflections read           9690


######  TLS Group Definitions ######



     CGMAT cycle number =      1

 Limits of asymmetric unit      : 1.00 1.00 1.00
 Grid spacing to be used        :    88   88   88
 Maximuum H,K,L                 :    17   17   17
 Minimum acceptable grid spacing:    58   58   58
 Weight matrix    1.00159063E-03
 Actual weight    10.0000000      is applied to the X-ray term
Norm of X_ray positional gradient                64.2
Norm of Geom. positional gradient                27.5
Norm of X_ray B-factor gradient                  52.5
Norm of Geom. B-factor gradient                  0.00
Product of X_ray and Geom posit. gradients     -0.154E+06
 Cosine of angle between them                      -0.018
Product of X_ray and Geom B-fact gradients       0.00
 Cosine of angle between them                       0.000


Residuals: XRAY=    -0.3355E+07 GEOM=      4544.     TOTAL=    -0.3351E+07
 function value   -3350629.25    

-------------------------------------------------------------------------------
             Restraint type              N restraints   Rms Delta   Av(Sigma)
Bond distances: refined atoms                  1680     0.009     0.019
Bond angles  : refined atoms                   2264     1.256     1.990
Torsion angles, period  1. refined              213     3.889     5.000
Torsion angles, period  2. refined               75    32.231    24.533
Torsion angles, period  3. refined              313    13.235    15.000
Torsion angles, period  4. refined               13    10.385    15.000
Chiral centres: refined atoms                   254     0.043     0.200
Planar groups: refined atoms                   1258     0.006     0.021
VDW repulsions: refined_atoms                  1026     0.196     0.200
VDW; torsion: refined_atoms                    2202     0.339     0.200
HBOND: refined_atoms                             48     0.213     0.200
M. chain bond B values: refined atoms           855     0.000     4.000
M. chain angle B values: refined atoms         1067     0.000     6.000
S. chain bond B values: refined atoms           825     0.000     4.000
S. chain angle B values: refined atoms         1197     0.000     6.000
Long range B values: refined atoms             5761     0.000    74.767
-------------------------------------------------------------------------------

-----------------------------------------------------------------------------
Overall               : scale =    1.005, B  =  52.443
Overall anisotropic scale factors
   B11 = -0.92 B22 = -1.37 B33 =  2.28 B12 =  1.56 B13 = -1.39 B23 = -0.18
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------

    ****                Things for loggraph, R factor and others                ****


$TABLE: Cycle    1. Rfactor analysis, F distribution v resln  :
$GRAPHS:Cycle    1. M(Rfactor) v. resln :N:1,6,7:
:Cycle    1. M(Fobs) and M(Fc) v. resln :N:1,4,5:
:Cycle    1. % observed v. resln :N:1,3:
$$
M(4SSQ/LL) NR_used %_obs M(Fo_used) M(Fc_used) Rf_used WR_used $$
$$
 0.001     125 100.00   407.5   405.7  0.07  0.07
 0.004     184 100.00   155.8   146.7  0.19  0.19
 0.006     285 100.00   124.4   118.0  0.19  0.19
 0.009     301 100.00   105.5    99.1  0.25  0.25
 0.011     346 100.00    83.9    78.4  0.28  0.28
 0.014     361 100.00    73.4    69.4  0.30  0.30
 0.016     384 100.00    64.3    59.7  0.32  0.32
 0.019     493 100.00    57.8    53.6  0.36  0.36
 0.021     471 100.00    54.8    51.2  0.36  0.36
 0.023     486 100.00    49.0    46.1  0.40  0.40
 0.026     511 100.00    47.0    47.0  0.42  0.42
 0.028     582 100.00    44.8    44.6  0.41  0.41
 0.031     543 100.00    45.3    46.3  0.40  0.40
 0.033     616 100.00    46.3    46.5  0.41  0.41
 0.036     589 100.00    46.9    45.4  0.40  0.40
 0.038     636 100.00    47.8    45.7  0.35  0.35
 0.041     699 100.00    46.7    42.2  0.36  0.36
 0.043     658 100.00    47.6    40.0  0.35  0.35
 0.046     663 100.00    47.0    38.9  0.36  0.36
 0.048     748 100.00    46.6    35.9  0.38  0.38
$$

    ****                      Fom and SigmaA vs resolution                      ****

 $TABLE: Cycle    1. FSC and  Fom(<cos(DelPhi)>-acentric, centric, overall v resln:
 $GRAPHS:Cycle    1. M(Fom) v. resln :N:1,3,5,7,8,9,10:
 $$
 <4SSQ/LL> NREFa  FOMa  NREFc FOMc NREFall FOMall  SigmaA_Fc1 FSCwork CorrFofcWork$$
 $$
  0.0014    125   0.995      0   0.000    125   0.995  1.011  0.9977  0.9976
  0.0039    184   0.992      0   0.000    184   0.992  1.029  0.9664  0.9078
  0.0063    285   0.991      0   0.000    285   0.991  1.008  0.9560  0.9100
  0.0088    301   0.990      0   0.000    301   0.990  0.988  0.9246  0.8288
  0.0112    339   0.989      0   0.000    339   0.989  0.951  0.8965  0.7896
  0.0137    361   0.989      0   0.000    361   0.989  0.898  0.8716  0.7376
  0.0161    384   0.989      0   0.000    384   0.989  0.888  0.8367  0.6999
  0.0186    493   0.989      0   0.000    493   0.989  0.882  0.8193  0.6176
  0.0210    471   0.989      0   0.000    471   0.989  0.868  0.8129  0.5786
  0.0235    486   0.988      0   0.000    486   0.988  0.805  0.7715  0.5591
  0.0259    511   0.988      0   0.000    511   0.988  0.749  0.7609  0.4928
  0.0284    582   0.988      0   0.000    582   0.988  0.736  0.7347  0.4972
  0.0308    543   0.988      0   0.000    543   0.988  0.761  0.7771  0.5390
  0.0333    616   0.988      0   0.000    616   0.988  0.770  0.7700  0.5533
  0.0358    585   0.989      0   0.000    585   0.989  0.807  0.7815  0.5540
  0.0382    636   0.989      0   0.000    636   0.989  0.842  0.8064  0.5990
  0.0407    699   0.989      0   0.000    699   0.989  0.904  0.8036  0.5911
  0.0431    658   0.989      0   0.000    658   0.989  0.973  0.8269  0.6459
  0.0456    663   0.989      0   0.000    663   0.989  0.988  0.8122  0.6267
  0.0480    768   0.989      0   0.000    768   0.989  1.043  0.8065  0.5958
 $$
Resolution limits                    =     75.000     4.506
Number of used reflections           =       9670
Percentage observed                  =   100.0000
Percentage of free reflections       =     0.0000
Overall R factor                     =     0.3149
Average Fourier shell correlation    =     0.8154
Overall weighted R factor            =     0.3149
Overall weighted R2 factor           =     0.2525
Average correlation coefficient      =     0.6238
Overall correlation coefficient      =     0.9487
Cruickshanks DPI for coordinate error=     1.0463
Overall figure of merit              =     0.9889
ML based su of positional parameters =     0.7132
ML based su of thermal parameters    =    58.0184
-----------------------------------------------------------------------------
 Trying gamma equal    0.00000000    
 Trying gamma equal    5.00000007E-02
 Gamma decreased to    3.99999991E-02


 fvalues   -330602.500       4544.18213      -3355237.50      -3301480.75    


     CGMAT cycle number =      2

 Weight matrix    1.39139337E-03
 Actual weight    10.0000000      is applied to the X-ray term


 function value   -3359471.50    

-------------------------------------------------------------------------------
             Restraint type              N restraints   Rms Delta   Av(Sigma)
Bond distances: refined atoms                  1680     0.015     0.019
Bond angles  : refined atoms                   2264     1.544     1.990
-------------------------------------------------------------------------------

-----------------------------------------------------------------------------
Overall               : scale =    1.001, B  =  18.148
Overall anisotropic scale factors
   B11 = -0.53 B22 = -1.50 B33 =  2.03 B12 =  1.50 B13 = -1.28 B23 = -0.17
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
Overall R factor                     =     0.3007
Average Fourier shell correlation    =     0.8360
Average correlation coefficient      =     0.6594
Overall figure of merit              =     0.9890
-----------------------------------------------------------------------------
 Trying gamma equal    3.99999991E-02
 Gamma decreased to    3.09090894E-02


 fvalues   -336013.781       5180.43652      -3362856.00      -3354957.25    


     CGMAT cycle number =      3

    ****                         Bond distance outliers                         ****

Bond distance deviations from the ideal >10.000Sigma will be monitored

A    184 LYS CE  . - A    184 LYS NZ  . mod.= 1.802 id.= 1.489 dev= -0.313 sig.= 0.020

 Limits of asymmetric unit      : 1.00 1.00 1.00
 Grid spacing to be used        :    88   88   88
 Maximuum H,K,L                 :    17   17   17
 Minimum acceptable grid spacing:    58   58   58
 Weight matrix    1.29885529E-03
 Actual weight    9.09090900      is applied to the X-ray term
Norm of X_ray positional gradient                68.2
Norm of Geom. positional gradient                59.1
Norm of X_ray B-factor gradient                  49.6
Norm of Geom. B-factor gradient                  36.2
Product of X_ray and Geom posit. gradients     -0.239E+07
 Cosine of angle between them                      -0.119
Product of X_ray and Geom B-fact gradients     -0.169E+07
 Cosine of angle between them                      -0.570


Residuals: XRAY=    -0.3078E+07 GEOM=      8450.     TOTAL=    -0.3070E+07
 function value   -3069962.25    

-------------------------------------------------------------------------------
             Restraint type              N restraints   Rms Delta   Av(Sigma)
Bond distances: refined atoms                  1680     0.022     0.019
Bond angles  : refined atoms                   2264     1.916     1.990
Torsion angles, period  1. refined              213     4.248     5.000
Torsion angles, period  2. refined               75    33.437    24.533
Torsion angles, period  3. refined              313    14.121    15.000
Torsion angles, period  4. refined               13    11.070    15.000
Chiral centres: refined atoms                   254     0.107     0.200
Planar groups: refined atoms                   1258     0.013     0.021
VDW repulsions: refined_atoms                  1022     0.196     0.200
VDW; torsion: refined_atoms                    2218     0.337     0.200
HBOND: refined_atoms                             46     0.217     0.200
M. chain bond B values: refined atoms           855     5.490     6.874
M. chain angle B values: refined atoms         1067     8.722    10.299
S. chain bond B values: refined atoms           825     6.395     6.737
S. chain angle B values: refined atoms         1197     9.777    10.148
Long range B values: refined atoms             5904    14.876   127.411
-------------------------------------------------------------------------------

-----------------------------------------------------------------------------
Overall               : scale =    1.005, B  =  28.248
Overall anisotropic scale factors
   B11 = -0.65 B22 = -1.00 B33 =  1.65 B12 =  1.22 B13 = -1.34 B23 = -0.52
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------

    ****                Things for loggraph, R factor and others                ****


$TABLE: Cycle    3. Rfactor analysis, F distribution v resln  :
$GRAPHS:Cycle    3. M(Rfactor) v. resln :N:1,6,7:
:Cycle    3. M(Fobs) and M(Fc) v. resln :N:1,4,5:
:Cycle    3. % observed v. resln :N:1,3:
$$
M(4SSQ/LL) NR_used %_obs M(Fo_used) M(Fc_used) Rf_used WR_used $$
$$
 0.001     125 100.00   407.7   408.2  0.06  0.06
 0.004     184 100.00   155.8   147.8  0.17  0.17
 0.006     285 100.00   124.4   118.4  0.17  0.17
 0.009     301 100.00   105.5    99.4  0.22  0.22
 0.011     346 100.00    84.0    78.3  0.26  0.26
 0.014     361 100.00    73.5    69.4  0.27  0.27
 0.016     384 100.00    64.4    60.0  0.28  0.28
 0.019     493 100.00    57.9    54.2  0.32  0.32
 0.021     471 100.00    54.8    52.0  0.32  0.32
 0.023     486 100.00    49.1    47.4  0.34  0.34
 0.026     511 100.00    47.0    48.3  0.36  0.36
 0.028     582 100.00    44.8    45.6  0.36  0.36
 0.031     543 100.00    45.3    48.0  0.35  0.35
 0.033     616 100.00    46.3    47.7  0.36  0.36
 0.036     589 100.00    46.9    46.9  0.33  0.33
 0.038     636 100.00    47.9    47.5  0.30  0.30
 0.041     699 100.00    46.7    44.3  0.30  0.30
 0.043     658 100.00    47.6    42.5  0.28  0.28
 0.046     663 100.00    47.0    41.3  0.29  0.29
 0.048     748 100.00    46.7    38.4  0.32  0.32
$$

    ****                      Fom and SigmaA vs resolution                      ****

 $TABLE: Cycle    3. FSC and  Fom(<cos(DelPhi)>-acentric, centric, overall v resln:
 $GRAPHS:Cycle    3. M(Fom) v. resln :N:1,3,5,7,8,9,10:
 $$
 <4SSQ/LL> NREFa  FOMa  NREFc FOMc NREFall FOMall  SigmaA_Fc1 FSCwork CorrFofcWork$$
 $$
  0.0014    125   0.995      0   0.000    125   0.995  1.009  0.9981  0.9981
  0.0039    184   0.992      0   0.000    184   0.992  1.030  0.9715  0.9226
  0.0063    285   0.991      0   0.000    285   0.991  1.013  0.9643  0.9274
  0.0088    301   0.990      0   0.000    301   0.990  1.003  0.9398  0.8630
  0.0112    339   0.990      0   0.000    339   0.990  0.979  0.9214  0.8293
  0.0137    361   0.989      0   0.000    361   0.989  0.934  0.8996  0.7844
  0.0161    384   0.989      0   0.000    384   0.989  0.927  0.8746  0.7591
  0.0186    493   0.989      0   0.000    493   0.989  0.924  0.8675  0.7054
  0.0210    471   0.989      0   0.000    471   0.989  0.907  0.8647  0.6806
  0.0235    486   0.989      0   0.000    486   0.989  0.856  0.8415  0.6809
  0.0259    511   0.989      0   0.000    511   0.989  0.801  0.8304  0.6264
  0.0284    582   0.989      0   0.000    582   0.989  0.790  0.8135  0.6327
  0.0308    543   0.989      0   0.000    543   0.989  0.803  0.8443  0.6694
  0.0333    616   0.989      0   0.000    616   0.989  0.817  0.8415  0.6780
  0.0358    585   0.989      0   0.000    585   0.989  0.854  0.8585  0.6942
  0.0382    636   0.989      0   0.000    636   0.989  0.884  0.8766  0.7275
  0.0407    699   0.989      0   0.000    699   0.989  0.933  0.8766  0.7263
  0.0431    658   0.989      0   0.000    658   0.989  0.989  0.8931  0.7689
  0.0456    663   0.989      0   0.000    663   0.989  1.013  0.8845  0.7623
  0.0480    768   0.989      0   0.000    768   0.989  1.054  0.8730  0.7193
 $$
Resolution limits                    =     75.000     4.506
Number of used reflections           =       9670
Percentage observed                  =   100.0000
Percentage of free reflections       =     0.0000
Overall R factor                     =     0.2720
Average Fourier shell correlation    =     0.8741
Overall weighted R factor            =     0.2720
Overall weighted R2 factor           =     0.2192
Average correlation coefficient      =     0.7311
Overall correlation coefficient      =     0.9611
Cruickshanks DPI for coordinate error=     0.9037
Overall figure of merit              =     0.9893
ML based su of positional parameters =     0.5881
ML based su of thermal parameters    =    47.3732
-----------------------------------------------------------------------------
 Trying gamma equal    3.09090894E-02
 Gamma decreased to    2.26446278E-02


 fvalues   -336771.094       8450.46875      -3078248.25      -3053104.75    

    ****                         Bond distance outliers                         ****

Bond distance deviations from the ideal >10.000Sigma will be monitored

A    184 LYS CE  . - A    184 LYS NZ  . mod.= 1.847 id.= 1.489 dev= -0.358 sig.= 0.020

 LABOUT FP=FP SIGFP=SIGFP FC=FC PHIC=PHIC FC_ALL=FC_ALL PHIC_ALL=PHIC_ALL FWT=FWT PHWT=PHWT DELFWT=DELFWT PHDELWT=PHDELWT FOM=FOM PHCOMB=PHCOMB FC_ALL_LS=FC_ALL_LS PHIC_ALL_LS=PHIC_ALL_LS

 WRITTEN OUTPUT MTZ FILE 
 Logical Name: HKLOUT   Filename: HKLOUT 




-------------------------------------------------------------------------------
             Restraint type              N restraints   Rms Delta   Av(Sigma)
Bond distances: refined atoms                  1680     0.023     0.019
Bond angles  : refined atoms                   2264     2.094     1.990
Torsion angles, period  1. refined              213     4.440     5.000
Torsion angles, period  2. refined               75    33.582    24.533
Torsion angles, period  3. refined              313    14.666    15.000
Torsion angles, period  4. refined               13    10.588    15.000
Chiral centres: refined atoms                   254     0.116     0.200
Planar groups: refined atoms                   1258     0.013     0.021
VDW repulsions: refined_atoms                  1018     0.196     0.200
VDW; torsion: refined_atoms                    2222     0.336     0.200
HBOND: refined_atoms                             46     0.215     0.200
M. chain bond B values: refined atoms           855     5.593     8.766
M. chain angle B values: refined atoms         1067     8.846    13.147
S. chain bond B values: refined atoms           825     7.036     8.604
S. chain angle B values: refined atoms         1197    10.539    12.966
Long range B values: refined atoms             5901    14.991   163.029
-------------------------------------------------------------------------------

-----------------------------------------------------------------------------
Overall               : scale =    1.005, B  =  -4.289
Overall anisotropic scale factors
   B11 = -0.77 B22 = -0.79 B33 =  1.57 B12 =  1.14 B13 = -1.34 B23 = -0.56
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------

    ****                Things for loggraph, R factor and others                ****


$TABLE: Cycle    4. Rfactor analysis, F distribution v resln  :
$GRAPHS:Cycle    4. M(Rfactor) v. resln :N:1,6,7:
:Cycle    4. M(Fobs) and M(Fc) v. resln :N:1,4,5:
:Cycle    4. % observed v. resln :N:1,3:
$$
M(4SSQ/LL) NR_used %_obs M(Fo_used) M(Fc_used) Rf_used WR_used $$
$$
 0.001     125 100.00   407.5   409.4  0.06  0.06
 0.004     184 100.00   155.8   148.4  0.16  0.16
 0.006     285 100.00   124.4   118.5  0.17  0.17
 0.009     301 100.00   105.5    99.3  0.22  0.22
 0.011     346 100.00    83.9    78.3  0.25  0.25
 0.014     361 100.00    73.4    69.4  0.25  0.25
 0.016     384 100.00    64.3    60.1  0.27  0.27
 0.019     493 100.00    57.8    54.2  0.30  0.30
 0.021     471 100.00    54.8    51.9  0.31  0.31
 0.023     486 100.00    49.0    47.4  0.32  0.32
 0.026     511 100.00    47.0    48.3  0.34  0.34
 0.028     582 100.00    44.8    45.8  0.34  0.34
 0.031     543 100.00    45.3    48.2  0.33  0.33
 0.033     616 100.00    46.3    47.9  0.34  0.34
 0.036     589 100.00    46.9    47.2  0.31  0.31
 0.038     636 100.00    47.8    47.8  0.28  0.28
 0.041     699 100.00    46.7    44.6  0.29  0.29
 0.043     658 100.00    47.6    42.9  0.27  0.27
 0.046     663 100.00    47.0    41.5  0.27  0.27
 0.048     748 100.00    46.6    38.9  0.30  0.30
$$

    ****                      Fom and SigmaA vs resolution                      ****

 $TABLE: Cycle    4. FSC and  Fom(<cos(DelPhi)>-acentric, centric, overall v resln:
 $GRAPHS:Cycle    4. M(Fom) v. resln :N:1,3,5,7,8,9,10:
 $$
 <4SSQ/LL> NREFa  FOMa  NREFc FOMc NREFall FOMall  SigmaA_Fc1 FSCwork CorrFofcWork$$
 $$
  0.0014    125   0.995      0   0.000    125   0.995  1.006  0.9983  0.9983
  0.0039    184   0.992      0   0.000    184   0.992  1.028  0.9741  0.9297
  0.0063    285   0.991      0   0.000    285   0.991  1.015  0.9671  0.9331
  0.0088    301   0.991      0   0.000    301   0.991  1.008  0.9444  0.8734
  0.0112    339   0.990      0   0.000    339   0.990  0.987  0.9288  0.8412
  0.0137    361   0.990      0   0.000    361   0.990  0.947  0.9094  0.8066
  0.0161    384   0.989      0   0.000    384   0.989  0.939  0.8863  0.7794
  0.0186    493   0.989      0   0.000    493   0.989  0.938  0.8822  0.7347
  0.0210    471   0.989      0   0.000    471   0.989  0.923  0.8794  0.7087
  0.0235    486   0.989      0   0.000    486   0.989  0.875  0.8602  0.7157
  0.0259    511   0.989      0   0.000    511   0.989  0.819  0.8489  0.6660
  0.0284    582   0.989      0   0.000    582   0.989  0.807  0.8342  0.6707
  0.0308    543   0.989      0   0.000    543   0.989  0.814  0.8619  0.7045
  0.0333    616   0.989      0   0.000    616   0.989  0.830  0.8605  0.7111
  0.0358    585   0.989      0   0.000    585   0.989  0.866  0.8763  0.7252
  0.0382    636   0.989      0   0.000    636   0.989  0.897  0.8928  0.7599
  0.0407    699   0.989      0   0.000    699   0.989  0.943  0.8927  0.7557
  0.0431    658   0.990      0   0.000    658   0.990  0.996  0.9071  0.7928
  0.0456    663   0.989      0   0.000    663   0.989  1.023  0.8984  0.7878
  0.0480    768   0.989      0   0.000    768   0.989  1.059  0.8882  0.7488
 $$
Resolution limits                    =     75.000     4.506
Number of used reflections           =       9670
Percentage observed                  =   100.0000
Percentage of free reflections       =     0.0000
Overall R factor                     =     0.2589
Average Fourier shell correlation    =     0.8887
Overall weighted R factor            =     0.2589
Overall weighted R2 factor           =     0.2090
Average correlation coefficient      =     0.7586
Overall correlation coefficient      =     0.9647
Cruickshanks DPI for coordinate error=     0.8603
Overall figure of merit              =     0.9895
ML based su of positional parameters =     0.5881
ML based su of thermal parameters    =    47.3732
-----------------------------------------------------------------------------
  Time in seconds: CPU =         7.75
             Elapsed =           0.00

    ****           Things for loggraph, R factor and others vs cycle            ****


$TABLE: Rfactor analysis, stats vs cycle  :
$GRAPHS:<Rfactor> vs cycle :N:1,2,3:
:FOM vs cycle :N:1,4:
:-LL vs cycle :N:1,5:
:-LLfree vs cycle :N:1,6:
:Geometry vs cycle:N:1,7,8,9,10,11:
$$
    Ncyc    Rfact    Rfree     FOM      -LL     -LLfree  rmsBOND  zBOND rmsANGL  zANGL rmsCHIRAL $$
$$
       0   0.3149   0.0      0.989     -335517.       0.0   0.0087  0.441   1.256  0.599   0.043
       1   0.3007   0.0      0.989     -336465.       0.0   0.0155  0.787   1.544  0.722   0.080
       2   0.2720   0.0      0.989     -338625.       0.0   0.0224  1.139   1.916  0.895   0.107
       3   0.2589   0.0      0.990     -339663.       0.0   0.0234  1.193   2.094  0.988   0.116
 $$
 $TEXT:Result: $$ Final results $$
                      Initial    Final
           R factor    0.3149   0.2589
     Rms BondLength    0.0087   0.0234
      Rms BondAngle    1.2556   2.0941
     Rms ChirVolume    0.0425   0.1164
 $$
 Harvest: NO PNAME_KEYWRD given - no deposit file created
<B><FONT COLOR="#FF0000"><!--SUMMARY_BEGIN-->
 Refmac:  End of Refmac_5.8.0158  
Times: User:      10.9s System:    0.3s Elapsed:     0:12  
</pre>
</html>
<!--SUMMARY_END--></FONT></B>

CCP-EM process finished Tuesday, 10. October 2017 06:09pm ______________________
________________________________________________________________________________
