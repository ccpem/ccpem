#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import multiprocessing
import json
import os
import time
from ccpem_core import ccpem_utils
from ccpem_core import settings
from ccpem_core import test_data
from . import process_utils
from .ccpem_process import CCPEMProcess


class CCPEMPipelineCustomFinish(object):
    '''
    Class for custom pipeline finish.
    '''
    def __init__(self):
        pass

    def on_finish(self, parent_pipeline=None):
        raise NotImplementedError('Subclasses should implement this')
    
    def on_part_fail(self,parent_pipeline=None):
        pass

class CCPEMPipelineCustomRunning(object):
    '''
    Class for custom pipeline running.
    '''
    def __init__(self):
        pass

    def on_running(self, parent_pipeline=None):
        raise NotImplementedError('Subclasses should implement this')

class CCPEMPipeline(object):
    '''
    Launch pipeline in separate thread.  Pipeline made of nest list e.g.:
        [[job1, job2], [job]]
    First level allows jobs to be grouped in stages, second level for jobs to
    be launched in parallel.  I.e. successive stage waits for all jobs in the
    previous stage to finish before execution.
    '''
    def __init__(self,
                 pipeline,
                 import_json=None,
                 args_path=None,
                 location=None,
                 database_path=None,
                 db_inject=None,
                 taskname=None,
                 title=None,
                 metadata=None,
                 on_running_custom = None,
                 on_finish_custom=None,
                 wait_json=None,
                 job_id=None,
                 verbose=False):
        if pipeline is None:
            self.pipeline = [[]]
        elif not isinstance(pipeline, list):
            self.pipeline = [[pipeline]]
        elif not isinstance(pipeline[0], list):
            self.pipeline = [pipeline]
        else:
            self.pipeline = pipeline
        self.args_path = args_path
        # Set job location and change to it
        self.location = location
        if self.location is None:
            self.location = os.getcwd()
        assert os.path.exists(self.location)
        self.taskname = taskname
        if self.taskname is None:
            self.taskname = ''
        self.title = title
        if self.title is None:
            self.title = ''
        self.database_path = database_path
        if db_inject is not None:
            self.db = db_inject.connect_db()
        else:
            self.db = None
        self.db_id = None
        self.job_id = job_id
        self.wait_json = wait_json
        if metadata is not None:
            assert isinstance(metadata, dict)
        self.metadata = metadata
        self.on_running_custom = on_running_custom
        self.on_finish_custom = on_finish_custom
        self.json = os.path.join(self.location,
                                 process_utils.task_filename)
        self.json_pl = None
        self.set_jpipeline()
        self.status = 'ready'
        if import_json is not None:
            self.import_json(jsonfile=import_json)
        else:
            # Create json files for each process (to allow reloading of
            # pipeline before individual jobs have started)
            for stage in self.pipeline:
                for job in stage:
                    job.export_json()

    def start(self):
        '''
        Run pipeline
        '''
        if self.wait_json is not None:
            on_wait = self.on_wait
        else:
            on_wait = None
        m_process = CCPEMMultiProcess(pipeline=self.pipeline,
                                      json_path=self.json,
                                      on_start=self.on_start,
                                      on_wait=on_wait,
                                      on_finish=self.on_finish)
        m_process.start()


    def kill_jobs_and_terminate(self):
        for jobs in self.json_pl:
            for job in jobs:
                # TODO: cleaner to call job.kill() and make sure that's robust
                pid = process_utils.get_process_pid(json_filepath=job)
                if pid is not None:
                    process_utils.kill_process(pid=pid)

    def set_metadata(self,
                     key,
                     value):
        if self.metadata is None:
            self.metadata = {}
        self.metadata[key] = value
        self.export_json()

    def get_metadata(self, key):
        with open(self.json, 'r') as json_file:
            metadata = json.load(fp=json_file)['metadata']
            self.metadata = metadata
            if self.metadata is not None:
                if key in self.metadata.keys():
                    return self.metadata[key]
            else:
                return None

    def on_start(self):
        if self.db is not None:
            self.db_id = self.db.insert_new_job(
                program=self.taskname,
                job_id=self.job_id,
                job_location=self.location,
                title=self.title)
        self.set_status_running()
        self.on_running()

    def on_wait(self):
        '''
        Utility to check previous job status.  Used when pipeline is to
        wait for this job to finish before starting.
        '''
        if self.wait_json is not None:
            if os.path.exists(self.wait_json):
                return process_utils.get_process_status(
                    json_filepath=self.wait_json)
        return None
    
    def on_running(self):
        # 
        if self.on_running_custom is not None:
            self.on_running_custom.on_running(parent_pipeline=self)
            
    def on_finish(self):
        # Check all jobs finished
        failed = False
        count_failed = 0
        count_jobs = 0
        for stage in self.json_pl:
            for job in stage:
                status = process_utils.get_process_status(json_filepath=job)
                if status != 'finished':
                    failed = True
                    count_failed += 1
                count_jobs += 1
        #if multiple parallel jobs are run and
        #custom on_part_fail is provided
        if count_failed > 0 and float(count_failed)/count_jobs < 0.25:
            if self.on_finish_custom is not None:
                self.on_finish_custom.on_part_fail(parent_pipeline=self)
        if failed:
            self.set_status_failed()
        else:
            if self.on_finish_custom is not None:
                self.on_finish_custom.on_finish(parent_pipeline=self)
            self.set_status_finished()

    def set_jpipeline(self):
        '''
        Convert pipeline of CCPEMProcesses to pipeline of json args paths.
        '''
        self.json_pl = []
        for stage in self.pipeline:
            j = []
            for job in stage:
                # Sanity check
                assert isinstance(job,
                                  CCPEMProcess)
                if hasattr(job, 'json'):
                    j.append(job.json)
            self.json_pl.append(j)

    def set_status_ready(self):
        self.status = 'ready'
        self.export_json()

    def set_status_finished(self):
        self.status = 'finished'
        self.export_json()
        if self.db_id is not None:
            self.db.update_job_end(job_id=self.db_id)

    def set_status_running(self):
        self.status = 'running'
        self.export_json()
        if self.db_id is not None:
            self.db.update_job_start(job_id=self.db_id)

    def set_status_failed(self):
        self.status = 'failed'
        self.export_json()
        if self.db_id is not None:
            self.db.update_job_end(job_id=self.db_id, failed=True)

    def get_status(self):
        if os.path.exists(self.json):
            return process_utils.get_process_status(json_filepath=self.json)
        else:
            return None

    def export_json(self):
        json_dict = {'status': self.status,
                     'location': self.location,
                     'taskname': self.taskname,
                     'args_path': self.args_path,
                     'database_path': self.database_path,
                     'db_id': self.db_id,
                     'metadata': self.metadata,
                     'pipeline': self.json_pl}
        ccpem_utils.atomic_write_json(json_dict,
                                      self.json,
                                      sort_keys=True,
                                      separators=(',', ': '),
                                      indent=4)

    def import_json(self, jsonfile):
        self.json = jsonfile
        with open(self.json, 'r') as json_file:
            self.__dict__.update(json.load(fp=json_file))
            self.restore_processes()

    def restore_processes(self):
        pipeline = []
        for stage in self.pipeline:
            j = []
            for job in stage:
                process = CCPEMProcess(import_json=job)
                j.append(process)
            pipeline.append(j)
        self.pipeline = pipeline
        self.set_jpipeline()

    def get_process(self, taskname):
        '''
        Convenience function to return task with give task name.  Returns last
        instance found in pipeline.
        '''
        process = None
        for jobs in self.pipeline:
            for job in jobs:
                if job.name == taskname:
                    process = job
        return process


class CCPEMMultiProcess(multiprocessing.Process):
    '''
    Launch pipeline in separate thread.  Pipeline made of nest list e.g.:
        [[job1, job2], [job]]
    First level allows jobs to be grouped in stages, second level for jobs to
    be launched in parallel.  I.e. successive stage waits for all jobs in the
    previous stage to finish before execution.
    '''
    def __init__(self,
                 pipeline,
                 json_path,
                 on_start,
                 on_wait,
                 on_finish):
        super(CCPEMMultiProcess, self).__init__()
        self.pipeline = pipeline
        self.json_path = json_path
        self.on_start = on_start
        self.on_wait = on_wait
        self.on_finish = on_finish
        self.tp = None

    def run(self):
        self.on_start()
        if self.on_wait is not None:
            previous_status = None
            while previous_status != 'finished':
                previous_status = self.on_wait()
                time.sleep(2)
        for n in xrange(len(self.pipeline)):
            self.run_jobs(jobs=self.pipeline[n])
        self.on_finish()

    def run_jobs(self, jobs):
        num_workers = len(jobs)
        self.tp = multiprocessing.Pool(num_workers)
        try:
            for n in xrange(num_workers):
                if jobs[n].status != 'finished':
                    self.tp.apply_async(process_utils.run_ccpem_process,
                                        (jobs[n], self.json_path))
            self.tp.close()
            self.tp.join()
        except KeyboardInterrupt:
            self.tp.terminate()


def main():
    # Job pipeline thread
    import tempfile
    # Move to temp directory
    test_output = tempfile.mkdtemp()
    os.chdir(test_output)
    coot_bin_pdb = [settings.which(program='coot')]
    data_path = test_data.get_test_data_path()
    coot_bin_pdb.append(os.path.join(data_path,
                                     'pdb/1AKE_cha_molrep.pdb'))
    coot_bin = settings.which(program='coot')
    job1 = CCPEMProcess(name='coot1',
                        command=coot_bin,
                        stdout='1_stdout.txt')
    job2 = CCPEMProcess(name='coot2',
                        command=coot_bin,
                        stdout='2_stdout.txt')
    job3 = CCPEMProcess(name='coot3',
                        command=coot_bin_pdb,
                        stdout='3_stdout.txt')
    pl = CCPEMPipeline(pipeline=[[job1, job3], [job1]])
    pl.start()
    pl2 = CCPEMPipeline(pipeline=[job2])
    pl2.start()


if __name__ == '__main__':
    main()
