#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
"""
Standalone process-related functions.
"""

import subprocess
import json
import os
import signal
import traceback

from ccpem_core import ccpem_utils


statuses = ['ready', 'running', 'finished', 'failed']

task_filename = 'task.ccpem'


def run_ccpem_process(job, json_pipeline):
    # Run functions pre job start
    job.pre_start()
    
    # Set env
    env = os.environ.copy()

    # For python jobs removed buffered stdout for rapid GUI updates
    for command in job.command:
        if 'python' in command:
            env.setdefault('PYTHONUNBUFFERED', '1')
    if job.env is not None:
        assert isinstance(job.env, dict)
        env.update(job.env)

    # Remove DYLD_LIBRARY_PATH from environment if present.
    #     This is removed to stop conflicts with CCP4 programs
    dyld_lib_env = 'DYLD_LIBRARY_PATH'
    if dyld_lib_env in env.keys():
        env.pop(dyld_lib_env)

    # Check if stdin is set
    stdin = None
    if job.stdin is not None:
        stdin = subprocess.PIPE
    
    with open(job.stdout, 'a') as stdout, open(job.stderr, 'w') as stderr:
        
        # Choose stdout for the process
        proc_stdout = stdout
        if job.log_parser is not None:
            proc_stdout = subprocess.PIPE
        
        # Run process (use Popen to pipe stdin)
        # Note: os.setsid does not exist on Windows, and the preexec_fn
        # argument has no effect - when we come to make this work on Windows
        # we'll need to find an alternative. (Look into process groups, daemon
        # processes etc.) Note also that close_fds must be set to False on
        # Windows to allow redirection of stdin, stdout and stderr.
        try:
            process = subprocess.Popen(args=job.command,
                                       env=env,
                                       cwd=job.location,
                                       stdin=stdin,
                                       stdout=proc_stdout,
                                       stderr=stderr,
                                       preexec_fn=os.setsid)
        
        # Show error and traceback
        except Exception, e:
            print ('\n\nWarning! Error starting CCP-EM process:\n    {0}\n'
                   .format(job.command))
            traceback.print_exc()  # this prints to sys.stderr
            if hasattr(e, 'child_traceback'):
                print 'Child traceback:'
                print e.child_traceback
            job.on_failed()
            return
        
        # Run on start functions
        job.on_start(pid=process.pid,
                     json_pipeline=json_pipeline)
        
        # Write stdin from string if required
        if job.stdin is not None:
            process.stdin.write(job.stdin)
            process.stdin.close()
        
        # Pass stdout through the log parser if there is one
        # TODO: make sure stdout still gets written to file even if log parsing fails
        if job.log_parser is not None:
            job.log_parser.parse_stream(process.stdout, stdout)
        
        # Wait for process to finish and check completion
        rc = process.wait()
        if rc != 0:
            job.on_failed()
        else:
            job.on_finish()


def kill_process(pid):
    '''
    Kills process and children
    '''
    try:
        os.killpg(pid, signal.SIGTERM)
    except OSError:
        pass


def set_status(json_filepath, status):
    '''
    Set pipeline / process status
    '''
    assert os.path.exists(path=json_filepath)
    with open(json_filepath) as data_file:
        if status in statuses:
            md = json.load(data_file)
            md['status'] = status
    ccpem_utils.atomic_write_json(md, json_filepath)


def get_process_status(json_filepath):
    '''
    Get job status from job manager json file.
    '''
    assert os.path.exists(path=json_filepath)
    with open(json_filepath) as data_file:
        status = json.load(data_file)['status']
    if status in statuses:
        return status
    else:
        return None

def get_db_id(json_filepath):
    '''
    Get job status from job manager json file.
    '''
    assert os.path.exists(path=json_filepath)
    with open(json_filepath) as data_file:
        db_id = json.load(data_file)['db_id']
    return db_id

def get_process_pid(json_filepath):
    '''
    Get job status from job manager json file.
    '''
    if os.path.exists(path=json_filepath):
        with open(json_filepath) as data_file:
            proc_json = json.load(data_file)
            pid = proc_json['pid']
        return pid
    else:
        return None

