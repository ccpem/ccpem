#
#     Copyright (C) 2018 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import os
import re

def job_register(task_name, db_inject=None, path=None):
    '''
    Register job id and get directory

    Program = task.task_info.name
    '''
    # Avoid spaces in job directory names
    dir_name = task_name.replace(' ', '_')

    if path is None:
        path = os.getcwd()

    if db_inject is not None:
        db = db_inject.connect_db()
        job_id = db.insert_new_job(
                program=task_name,
                job_location='None')
        job_location = os.path.join(
            path,
            dir_name + '_' + str(job_id))
    else:
        job_id = None
        inc = get_path_increment(path=path)
        job_location = os.path.join(
            path,
            dir_name + '_{0}'.format(inc))

    # Register job directory
    if not os.path.exists(job_location):
        os.makedirs(job_location)

    return job_id, job_location


def get_path_increment(path):
    '''
    All CCP-EM job directories end '_n' where n is an integer.
    Search all jobs in directory and generate n+1 increment.
    '''
    inc = 1
    for name in os.listdir(path):
        groups = re.compile(r'(_\d+)$').search(name)
        if groups is not None:
            suffix = int(groups.group(1).replace('_',''))
            if suffix >= inc:
                inc = suffix + 1
    return inc
