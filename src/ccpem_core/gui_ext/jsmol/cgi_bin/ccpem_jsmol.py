#!/usr/bin/env python

'''
Server side script for jsmol; rewrite of jsmol.php to allow us to use it in a
python server instance. Additionally, added function to generate a sensible
cut-off value for isosurfaces that are generated from mrc file, and ability to
downsample a map file
'''

################################################################################
#                                                                              #
# Server-side code for jsmol; rewrite of jsmol.php to allow us to use it in a  #
# python server instance. Additionally, added function to generate a sensible  #
# cut-off value for isosurfaces that are generated from mrc file               #
# (C) STFC                                                                     #
# Author: Chris Wood <chris.wood@stfc.ac.uk>                                   #
# Last modified: 20/01/2015 by Chris Wood                                      #
#                                                                              #
################################################################################

import cgi, os, json, types
from urlparse import urlparse
from email import utils
from base64 import b64encode
from mrc_map_io_clipper import map_tools, read_mrc_header
import cgitb; cgitb.enable(format="text",
                           context=0,
                           logdir=os.getcwd(),
                           display=1)

class ServerActions(object):

    '''
    Functions to be done on the server
    '''

    def __init__(self):
        self.args = cgi.FieldStorage()
        
        ##
        # Very frustrating - I'm not sure what I changed but at some point I was
        # bitten by a known CGIHTTPServer bug:
        # 
        #   "CGIHTTPServer assumes that it is being provided with a path to a
        #    CGI script so it collapses the path without regard to any query
        #    string that might be present. Collapsing the path removes duplicate 
        #    forward slashes as well as relative path elements such as .. "
        #
        # This means that having a full url in our query doesn't work (but I
        # know it used to!); url parse therefore doesn't recognise that
        # localhost should be in netloc, so we need to strip it out ourselves
        #  - this works whether or not the URL is encoded (but the URL should be
        #    encoded for best practice)
        ##

        path = urlparse(self.args.getvalue("query")).path
        self._file = os.path.normpath(
                        os.path.join(os.getcwd(),
                            path.split("/",2)[2]
                        )
                     )

    @staticmethod
    def print_file_header(content_length=None):
        '''
        Print the HTTP header
        :params content_length: Length of content in body of file in bytes (as
         per rfc2616)
        '''
        print "Access-Control-Allow-Origin: *"
        print "Content-Type: text/plain; charset=x-user-defined"
        print "Last-Modified: " + utils.formatdate()
        print "Accept-Ranges: bytes"
        print "Content-Length: " + str(content_length)
        print # http headers *must* be followed by a blank line

    def binary_conversion(self):
        '''
        Convert binary density file to base64 so can be read by JSmol.
        '''
        with open(self._file, "rb") as _file:
            output = b64encode(_file.read())
        output=";base64," + output
        self.print_file_header(len(output))
        print output

    def get_cut_off(self):
        '''
        Determine the cutoff that should be used for the default isosurface for
        the given mrc file; returns a json file that can be read by javascript
        containing an array with contents:
            - rms
            - min value
            - max value
            - mean value
            - standard deviation
        '''
        nx_mrc_map = map_tools.ClipperNXMap(filename=str(self._file))
        mrc_map = read_mrc_header.MRCMapHeaderData(str(self._file))

        output = json.dumps({
                    "rms": mrc_map.header_rms[0],
                    "min": nx_mrc_map.clipper_nxmap.GetMin(),
                    "max": nx_mrc_map.clipper_nxmap.GetMax(),
                    "mean": nx_mrc_map.clipper_nxmap.GetMean(),
                    "std_dev": nx_mrc_map.clipper_nxmap.GetStdDev(),
                    "grid_size": nx_mrc_map.clipper_nxmap.GetGrid()[0]
                  })

        self.print_file_header(len(output))

        assert type(mrc_map.header_rms) is (types.TupleType or types.FloatType)
        assert type(nx_mrc_map.clipper_nxmap.GetGrid()) is \
                            (types.TupleType or types.FloatType)

        print output

    def downsample(self):

        self.print_file_header(0)

        nx_mrc_map = map_tools.ClipperNXMap(filename=str(self._file))
        nx_mrc_map.clipper_nxmap.ResampleGrid(
            (nx_mrc_map.clipper_nxmap.GetGrid()[0])/2)
        nx_mrc_map.clipper_nxmap.WriteMap(
                    str(self._file).replace(".mrc","_downsampled.mrc"))

if __name__ == "__main__":

    actions = ServerActions()
    if actions.args.getvalue("method") == "getcutoff":
        actions.get_cut_off()
    elif actions.args.getvalue("method") == "downsample":
        actions.downsample()
    else:
        actions.binary_conversion()
