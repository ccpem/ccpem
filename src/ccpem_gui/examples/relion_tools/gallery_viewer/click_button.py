#
#     Copyright (C) 2014 Tom Burnley
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
# Click button class for interactive clickable gallery viewer
#
# Tom Burnley 04/2014

import sys, os
from PyQt4.QtCore import Qt, QSize, QString, SIGNAL, QEvent
from PyQt4.QtGui import QApplication, QDialog, QGridLayout, QLabel, QPixmap, QImage, QFont, QWidget
import click_gallery_viewer
from ccpem_core import ccpem_utils

class ClickButton(QWidget):

  def __init__(self, 
      image_name = None,
      pil_image = None,
      image_tag = None,
      button_pixmap = None,
      parent = None, 
      *args):
    self.image_tag = image_tag
    self.image_name = image_name
    self.pil_image = pil_image
    self.button_pixmap = button_pixmap
    self.parent = parent

    apply(QWidget.__init__,(self, ) + args)
    QWidget.__init__(self)
    self.resize(140, 140)
    self.initButton()
    self.connect(self.ImageButton, SIGNAL('clicked()'), self.buttonClicked)
    self.connect(self.ImageButton, SIGNAL('doubleClicked()'), self.printTest)
    self.connect(self.ImageButton, SIGNAL('scroll(int)'), self.wheelScrolled)

  def printTest(self):
    print '\n\nDbl click detected: Place holder for optional function'

  def initButton(self):
    self.ImageButton = ExtendedQLabel(self)
    self.ImageButton.move(0,0)
    #
    assert [self.image_name, self.pil_image].count(None) == 1
    if self.button_pixmap is not None:
      pixmap = self.button_pixmap
    
    elif self.image_name is not None:
      pixmap = QPixmap(self.image_name)
      pixmap = pixmap.scaled(QSize(130, 130), Qt.KeepAspectRatioByExpanding)

    else:
      image = QImage(self.pil_image.tostring(),
                     self.pil_image.size[0],
                     self.pil_image.size[1],
                     QImage.Format_Indexed8)
      pixmap = QPixmap.fromImage(image)
      pixmap = pixmap.scaled(QSize(130, 130), Qt.KeepAspectRatioByExpanding)
    #
    self.ImageButton.setPixmap(pixmap)
    self.ImageButton.setScaledContents(True)

  def buttonClicked(self):
    print '\n  Class clicked : ', self.image_name
    if self.parent is not None:
      if self.image_tag not in self.parent.selected_images:
        self.parent.selected_images.append(self.image_tag)
        self.parent.selected_images = sorted(self.parent.selected_images)
        self.ImageButton.resize(140, 140)
      else:
        self.parent.selected_images.remove(self.image_tag)
        self.ImageButton.resize(130, 130)
      
      if len(self.parent.selected_images) > 0:
        ccpem_utils.print_sub_sub_header(message = 'Selected classes')
        print '    n    Class'
        for n, selected_image in enumerate(self.parent.selected_images):
          print '   {0:2d} {1:8d} '.format(n+1, selected_image)
      else:
        print "\n  No images currently selected"

      print "\n  Click on image to select, reclick to deselect or close window to finish"

  def wheelScrolled(self, scrollAmount):
    scrollAmount /= 10
    self.ImageButton.resize(self.ImageButton.width() + scrollAmount, self.ImageButton.height() + scrollAmount)


class ExtendedQLabel(QLabel):

  def __init(self, parent):
    QLabel.__init__(self, parent)

  def mousePressEvent(self, event):
    if event.type() == QEvent.MouseButtonPress:
      self.emit(SIGNAL('clicked()'))
    elif event.type() == QEvent.MouseButtonDblClick:
      self.emit(SIGNAL('doubleClicked()'))

  def wheelEvent(self, ev):
    self.emit(SIGNAL('scroll(int)'), ev.delta())

def main_example():
  app = QApplication(sys.argv)
  image_name = './tst_data/50_micrographs/00_micrograph_ac.png'
  win = ClickButton(image_name = image_name)
  win.show()
  app.exec_()
  sys.exit()

if __name__ == '__main__':
  main_example()
