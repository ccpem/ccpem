import os
from PyQt4 import QtGui, QtCore, QtWebKit
from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_gui.utils import command_line_launch
from ccpem_core import chimera_scripts
from ccpem_core.settings import which
from ccpem_core.process_manager import job_register
from ccpem_gui.project_database import sqlite_project_database

class PBBDatasets(object):
    def __init__(self,
                 pdb_path=None):
        self.pdb_path = None
        self.set_pdb_path(pdb_path=pdb_path)

    def set_pdb_path(self, pdb_path):
        if pdb_path is not None:
            self.pdb_path = os.path.abspath(pdb_path)

    def validate(self):
        if [self.pdb_path].count(None) == 0:
            return True
        else:
            return False


class PDBInput(QtGui.QWidget):
    def __init__(self,
                 label='PDB',
                 pdb_path=None,
                 chains=None,
                 parent=None):
        super(PDBInput, self).__init__()
        self.parent = parent
        self.dataset = PBBDatasets()
        self.pdb_stats = None
        button_width = 100
        label_width = 150
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)
        self.setToolTip('')

        # PDB
        pdb_box = QtGui.QHBoxLayout()
        self.layout.addLayout(pdb_box)

        self.pdb_label = QtGui.QLabel(label)
        self.pdb_label.setFixedWidth(label_width)
        pdb_box.addWidget(self.pdb_label)
        self.pdb_select = QtGui.QPushButton('Select')
        self.pdb_select.setFixedWidth(button_width)
        self.pdb_select.clicked.connect(self.get_pdb_file)
        pdb_box.addWidget(self.pdb_select)
        self.pdb_value = QtGui.QLineEdit()
        self.pdb_value.editingFinished.connect(self.edit_finished_pdb)
        pdb_box.addWidget(self.pdb_value)

        width_ar = 30
        # Add
        add_button = QtGui.QPushButton('+')
        add_button.setFixedWidth(width_ar)
        add_button.setToolTip('Add PDB')
        add_button.clicked.connect(self.add)
        pdb_box.addWidget(add_button)

        # Remove
        remove_button = QtGui.QPushButton('-')
        remove_button.setFixedWidth(width_ar)
        remove_button.setToolTip('Remove PDB')
        remove_button.clicked.connect(self.remove)
        pdb_box.addWidget(remove_button)

        # Set initial values
        # PDB
        self.set_pdb_file(path=pdb_path)

    def get_pdb_file(self):
        path = self.get_file_from_brower(file_types=ccpem_file_types.pdb_ext)
        if path is not None:
            self.set_pdb_file(path=path)

    def edit_finished_pdb(self):
        path = str(self.pdb_value.text())
        self.set_pdb_file(path=path)

    def set_pdb_file(self, path):
        self.set_shading(self.pdb_label, shade=True)
        self.set_shading(self.pdb_value, shade=True)
        self.dataset.pdb_path = None
        if path is not None:
            if os.path.exists(path):
                self.pdb_value.setText(path)
                self.set_shading(self.pdb_label, shade=False)
                self.set_shading(self.pdb_value, shade=False)
                self.dataset.set_pdb_path(path)
            else:
                path = None

    def get_file_from_brower(self, file_types):
        dialog_path = window_utils.get_last_directory_browsed()
        path = QtGui.QFileDialog.getOpenFileName(
            self,
            'Open a File',
            QtCore.QDir.path(QtCore.QDir(dialog_path)),
            file_types)
        path = str(path)
        if path == '':
            path = None
        if path is not None:
            dialog_path = os.path.dirname(path)
            window_utils.set_last_directory_browsed(path=dialog_path)
        return path

    def set_shading(self, widget, shade=True):
        '''
        Shade text QLineEdit red to display warning.
        '''
        if shade:
            widget.setStyleSheet('color: red')
        else:
            widget.setStyleSheet('color: None')

    def remove(self):
        '''
        Only remove if more than 2 are present
        '''
        if hasattr(self.parent, 'number_pdb_inputs'):
            if self.parent.number_pdb_inputs() >= 2:
                self.deleteLater()

    def add(self):
        if hasattr(self.parent, 'add_dataset'):
            self.parent.add_dataset()

    def disable(self):
        self.pdb_value.setReadOnly(True)
        self.pdb_select.setEnabled(False)

class MultiPDBInput(QtGui.QWidget):
    def __init__(self,
                 group_label=None,
                 pdbs_arg=None,
                 chains_arg=None,
                 required=True,
                 parent=None):
        super(MultiPDBInput, self).__init__()
        self.parent = parent
        self.pdbs_arg = pdbs_arg
        # Layout
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)
        
        # Set tooltip
        tooltip_text = self.pdbs_arg.help
        if required:
            tooltip_text += ' | Required'
        self.setToolTip(tooltip_text)
        
        # Label
        if group_label is not None:
            ds_label = QtGui.QLabel(group_label)
            self.layout.addWidget(ds_label)

        # Set args
        pdbs = self.pdbs_arg()

        # Add datasets
        if isinstance(pdbs, list):
            for n in xrange(len(pdbs)):
                self.add_dataset(
                    pdb_path=pdbs[n])
        else:
            self.add_dataset(
                pdb_path=pdbs)

    def handle_add_button(self):
        '''
        Handle dataset button
        '''
        self.add_dataset()

    def add_dataset(self,
                    pdb_path=None,
                    chains=None):
        dataset = PDBInput(pdb_path=pdb_path,
                           parent=self)
        self.layout.addWidget(dataset)

    def number_pdb_inputs(self):
        return len(self.findChildren(PDBInput))

    def validate_datasets(self):
        children = self.findChildren(PDBInput)
        # Check at least one dataset
        if len(children) == 0:
            # Warn no datasets provided
            text = 'No input datasets provided'
            QtGui.QMessageBox.warning(self,
                'Error',
                text)
            return False
        warning = False
        # Check datasets are complete (must have pdb, map, resolution)
        for child in children:
            if not child.dataset.validate():
                warning = True
        if warning:
            # Warn datasets incomplete information
            text = 'Dataset(s) incomplete (requires PDB)'
            QtGui.QMessageBox.warning(self,
                'Error',
                text)
            return False
        else:
            return True

    def set_args(self):
        children = self.findChildren(PDBInput)
        pdbs = []
        if len(children) > 1:
            for child in children:
                pdbs.append(child.dataset.pdb_path)
            self.pdbs_arg.value = pdbs
        else:
            child = children[0]
            self.pdbs_arg.value = child.dataset.pdb_path

    def disable(self):
        index = self.layout.count()
        for l in xrange(index):
            PDBWidget = self.layout.itemAt(l).widget()
            try: 
                PDBWidget.disable()
            except AttributeError:
                pass
