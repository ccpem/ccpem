import sys
from PyQt4 import QtGui
from ccpem_gui.utils import window_utils

class ListWidget(QtGui.QWidget):
    def __init__(self, list_items=None,parent=None):
        super(ListWidget,self).__init__(parent)
        
        self.widget_layout = QtGui.QVBoxLayout()
        # Create ListWidget
        self.list_widget = QtGui.QListWidget()
        for i in range(len(list_items)):
            self.list_widget.addItem('{}'.format(list_items[i]))

        # Enable drag & drop ordering of items.
        self.list_widget.setDragDropMode(QtGui.QAbstractItemView.InternalMove)

        self.widget_layout.addWidget(self.list_widget)
        self.setLayout(self.widget_layout)


if __name__ == '__main__':
  app = QtGui.QApplication(sys.argv)
  list_items = ['shiftpeakzero','lowpass','threshold','mask','crop',
                'pad','downsample','dust','softmask']
  widget = ListWidget(list_items)
  widget.show()

  app.exec_()
  itemsTextList =  [str(widget.list_widget.item(i).text()) for i in 
                    range(widget.list_widget.count())] 
  print itemsTextList
  sys.exit()