import sys
import os
from PyQt4 import QtGui
from PyQt4.QtCore import QT_VERSION_STR
from PyQt4.Qt import PYQT_VERSION_STR


class FileDialogDemo(QtGui.QWidget):
    def __init__(self, parent=None):
        super(FileDialogDemo, self).__init__(parent)
        layout = QtGui.QVBoxLayout()
        self.btn = QtGui.QPushButton('Click to select file')
        self.btn.clicked.connect(self.get_file)
        layout.addWidget(self.btn)
        self.setLayout(layout)
        self.setWindowTitle('File Dialog demo')

    def get_file(self):
        dialog_path = os.getcwd()
        self.filename = QtGui.QFileDialog.getOpenFileName(
            self,
            'Open a file',
            dialog_path,
            'All files (*.*)')
        self.filename = str(self.filename)
        self.file_info()

    def file_info(self):
        print '-File information-'
        print 'Qt version      : ', QT_VERSION_STR
        print 'PyQt version    : ', PYQT_VERSION_STR
        print '\nFile selection info:'
        file_test = {'File select': self.filename,
                     'Path': os.path.abspath(self.filename),
                     'Directory': os.path.dirname(self.filename),
                     'CWD': os.getcwd()}
        for name, val in file_test.iteritems():
            r_ok = os.access(val, os.R_OK)
            w_ok = os.access(val, os.W_OK)
            print '\n{0:>15} : {1}'.format(name, val)
            print '{0:>15} : {1}'.format('Read OK', r_ok)
            print '{0:>15} : {1}'.format('Write OK', w_ok)


def main():
    app = QtGui.QApplication(sys.argv)
    fd = FileDialogDemo()
    fd.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
