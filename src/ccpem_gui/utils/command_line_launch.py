#
#     Copyright (C) 2018 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import sys
import os
import argparse
from PyQt4 import QtCore, QtGui
from argparse import RawTextHelpFormatter
from ccpem_core import ccpem_utils
from ccpem_gui.project_database import sqlite_project_database
from ccpem_core.process_manager import process_utils, CCPEMPipeline
from ccpem_gui.utils import window_utils
from ccpem_core.process_manager import job_register
from ccpem_gui import icons

def command_line_parser():
    simple_command = 'ccpem-<task> -args args.json'
    parser = argparse.ArgumentParser(
        description=
            '*** CCP-EM command line task runner ***\n' + \
            ('\nExample usage: \"{0}\".'.format(simple_command)),
        formatter_class=RawTextHelpFormatter)
    parser.add_argument(
        '--args',
        type=str,
        default=None,
        required=False,
        metavar='Arguments',
        help='Path to JSON file containing task arguments')
    parser.add_argument(
        '--args_string',
        type=str,
        default=None,
        required=False,
        metavar='Arguments',
        help='String containg JSON format task arguments')
    parser.add_argument(
        '--job_location',
        type=str,
        default=None,
        required=False,
        help=('Path to directory to run job, if not supplied will be '
              'created in current directory'))
    parser.add_argument(
        '--job_id',
        type=int,
        default=None,
        required=False,
        help=('Job id'))
    parser.add_argument(
        '--project_location',
        type=str,
        default=None,
        required=False,
        help=('Path to project directory, if supplied job will be added'
              'to the project'))
    parser.add_argument(
        '--gui',
        dest='gui',
        action='store_true',
        help='Run with GUI window interface')
    parser.add_argument(
        '--no-gui',
        dest='gui',
        action='store_false',
        help='Run without GUI window interface')
    parser.set_defaults(gui=True)
    return parser

def ccpem_task_launch(task_class,
                      window_class):
    '''
    Command line launch for either CCP-EM tasks in either gui or non-gui
    mode
    '''

    # Print header
    message = 'CCP-EM | ' + task_class.task_info.name
    ccpem_utils.print_header(message=message)
    message = 'CCP-EM command line task'
    ccpem_utils.print_sub_header(message=message)

    # Get command line args
    args = command_line_parser().parse_args()
    # Get json path for task args
    args_json = None
    if args.args is not None:
        if os.path.exists(args.args):
            args_json=args.args
        
    # Get json string
    args_string = None
    if args.args_string is not None:
        # Remove trailing ' if present (added if launched via subprocess)
        args_string = args.args_string
        if args_string[0] == "'":
            args_string = args_string[1:]
        if args_string[-1] == "'":
            args_string = args_string[:-1]

    # Get task json if in job location path
    task_json = None
    pipeline = None
    if args.job_location is not None:
        task_json = os.path.join(
            args.job_location,
            process_utils.task_filename)
        if os.path.exists(task_json):
            pipeline = CCPEMPipeline(
                pipeline=None,
                import_json=task_json)
        else:
            task_json = None

    # If project location is set job location to be set automatically
    database_path = None
    if args.project_location is not None:
        database_path = os.path.join(
            os.path.abspath(args.project_location),
            sqlite_project_database.CCPEMDatabase.ccpem_db_filename)
        if not os.path.exists(database_path):
            message = 'Project path not found: {0}'.format(
                args.project_location)
            ccpem_utils.print_warning(message=message)
            database_path = None

    # Create task
    try:
        task = task_class(
            args_json=args_json,
            args_json_string=args_string,
            database_path=database_path,
            pipeline=pipeline)
    except TypeError:
        task = task_class(
            args_json=args_json,
            database_path=database_path,
            pipeline=pipeline)

    # Launch standalone window GUI
    if args.gui:
        standalone_window_launch(
            task=task,
            window_class=window_class)
    
    # Run task without GUI
    if args_json is not None or args.args_string is not None:
        command_line_task_runner(
            task=task,
            job_id=args.job_id,
            job_location=args.job_location,
            database_path=database_path)
    # If no arguments provided generate example args json file and return
    else:
        output_examaple_json(task=task)

def output_examaple_json(task):
    '''
    Produce example json file.
    '''
    ccpem_utils.print_sub_header(
        message='Warning: no JSON args supplied')
    ccpem_utils.print_sub_header(
        message='Generate example argument JSON input')
    job_args = task.parser().generate_arguments()
    task_name = task.task_info.name
    filename = task_name.lower() + '_eg.json'
    job_args.output_args_as_json(filename=filename)
    print '  Example argument file : ', filename
    ccpem_utils.print_footer()


def command_line_task_runner(task,
                             job_id=None,
                             job_location=None,
                             database_path=None):
    '''
    Run ccpem task on command line without GUI
    '''
    # Print args
    ccpem_utils.print_sub_header(message='Arguments')

    # Register job
    path = None
    db_inject = None
    if database_path is not None:
        path = os.path.dirname(database_path)
        db_inject = sqlite_project_database.CCPEMInjector(
            database_path=database_path)
    if job_location is None:
        job_id, job_location = job_register.job_register(
            path=path,
            db_inject=db_inject,
            task_name=task.task_info.name)

    # Run task
    task.run_task(job_id=job_id,
                  job_location=job_location,
                  db_inject=db_inject)
    print '  \nTask running in background, use ctrl-z to regain terminal'
    ccpem_utils.print_footer()
    sys.exit()


def standalone_window_launch(task,
                             window_class):
    '''
    Launch task standalone task GUI.
    '''
    # Launch GUI
    app = QtGui.QApplication(sys.argv)
    if sys.platform == 'linux' or sys.platform == 'linux2':
        style = 'plastique'
        app.setStyle(style)
        app.setStyleSheet('''QToolTip {background-color: black;
                                       color: white;
                                       border: black solid 1px
                                       }''')
    app.setWindowIcon(QtGui.QIcon(icons.icon_utils.get_ccpem_icon()))

    # Set local
    QtCore.QLocale.setDefault(QtCore.QLocale('en_UK'))

    window_utils.CCPEMStandAloneWindow(
        task=task,
        window=window_class)
    sys.exit(app.exec_())
