#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
from ccpem_gui.icons import icon_utils


class Test(unittest.TestCase):
    '''
    Test all icons are present and correct
    '''

    def test_icons(self):
        '''
        Test icons can be found
        '''
        # Check icons paths are present.
        for path in [icon_utils.get_ccpem_icon(),
                     icon_utils.get_icons_path(),
                     icon_utils.get_image_icons_path()]:
            assert os.path.exists(path)


if __name__ == '__main__':
    unittest.main()
