#     2018 CCP-EM



import unittest
import os
import sys
import shutil
import time
import tempfile
from PyQt4 import QtGui, QtCore
from PyQt4.QtTest import QTest
from ccpem_core import settings


app = QtGui.QApplication(sys.argv)

class Test(unittest.TestCase):
    '''
    Unit test for CCP-EM Main Window (invokes GUI).
    '''
    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        '''
        Remove temporary data.
        '''
        if os.path.exists(path=self.test_output):
            shutil.rmtree(self.test_output)

    def run_main_window(self):
        print ('Launch main window')
        time_start = time.time()
        # In line import to test GUI setup time
        from  ccpem_gui import ccpem_mw
        args = settings.get_ccpem_settings()
        args.alpha.value = True
        self.window = ccpem_mw.CCPEMMainWindow(splash=None,
                                               show_warning=False,
                                               args=args)
        self.window.set_tasks_projects()
        self.window.echo_main_window()
        time_gui_start = time.time() - time_start

        # Test start-up time
        print 'GUI start-up time :', time_gui_start
        self.assertLess(time_gui_start,
            7.5,
            'GUI start-up time too slow')

        # Test mouse click on test button
        QTest.mouseClick(
            self.window.test_button,
            QtCore.Qt.LeftButton)

    def test_ccpem_main_window_integration_all(self):
        '''
        Test main window with no dependancies removed
        '''
        self.run_main_window()

    def test_ccpem_main_window_integration_no_modeller(self):
        '''
        Test main window, removing modeller if present on python path
        '''
        # XXX To do
        # Remove modeller from python path... this doesn't work reliably...
        # Any modules imported previously will still have modeller available
        # on path 
        for p in sys.path:
            if p.find('modeller') > 0:
                print '\nFound:', p
                sys.path.remove(p)
        self.run_main_window()


if __name__ == '__main__':
    unittest.main()