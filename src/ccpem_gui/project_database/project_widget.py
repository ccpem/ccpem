#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import sys
import os
from PyQt4 import QtGui
from PyQt4 import QtCore
from ccpem_gui.project_database import project_manager
from ccpem_core import ccpem_utils


class AddProjectDialog(QtGui.QDialog):
    def __init__(self, project, projects, parent=None):
        super(AddProjectDialog, self).__init__(parent)
        self.project = project
        self.projects = projects
        self.setGeometry(self.geometry().x(),
                         self.geometry().y(),
                         600,
                         100)
        info_label = QtGui.QLabel('''\
CCP-EM project name must be unique with only one project per directory\n''')
        name_label = QtGui.QLabel('Name')
        name_label.setToolTip('Project name')
        self.name_edit = QtGui.QLineEdit(self.project.name)
        path_widget = QtGui.QWidget()
        path_layout = QtGui.QGridLayout()
        path_label = QtGui.QLabel('Path')
        path_label.setToolTip('Project location - project files will be placed in this directory')
        path_layout.addWidget(path_label, 0, 0)
        select_input_path = QtGui.QPushButton('Select')
        path_layout.addWidget(select_input_path, 0, 1)
        self.path_edit = QtGui.QLineEdit()
        if self.project.path is not None:
            self.path_edit.setText(self.project.path)
        path_layout.addWidget(self.path_edit, 0, 2)
        path_widget.setLayout(path_layout)
        user_label = QtGui.QLabel('User')
        user_label.setToolTip('User name - internal to CCPEM')
        self.user_edit = QtGui.QLineEdit(self.project.user)
        button_box = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok |
                                            QtGui.QDialogButtonBox.Cancel)
        grid = QtGui.QGridLayout()
        grid.addWidget(info_label, 0, 0, 1, 2)
        grid.addWidget(name_label, 1, 0)
        grid.addWidget(self.name_edit, 1, 1)
        grid.addWidget(path_widget, 2, 0, 1, 2)
        grid.addWidget(user_label, 3, 0)
        grid.addWidget(self.user_edit, 3, 1)
        grid.addWidget(button_box, 4, 0, 1, 2)
        self.setLayout(grid)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)
        select_input_path.clicked.connect(self.select_file)
        self.setWindowTitle('Add project')

    def select_file(self):
        directory = QtGui.QFileDialog.getExistingDirectory(
            self,
            'Select directory',
            self.project.path,
            QtGui.QFileDialog.ShowDirsOnly)
        if os.access(directory, os.W_OK):
            self.path_edit.setText(directory)
        else:
            QtGui.QMessageBox.warning(
                self,
               'CCP-EM error',
               'Selected directory does not have write permission')

    def accept(self):
        errors = ''
        directory = str(self.path_edit.text())
        name = str(self.name_edit.text())
        # Check name
        if name in self.projects.project_names:
            errors += '\nProject name already exists'
        # Check directory
        if directory in self.projects.project_paths:
            errors += '\nDirectory already has project'
        elif not os.path.exists(directory):
            directory = ccpem_utils.check_directory_and_make(
                directory,
                auto_suffix=False,
                verbose=False)
            if directory is None:
                errors += '\nDirectory cannot be made please try again'
        if errors != '':
            QtGui.QMessageBox.critical(self,
                                      'Error',
                                      errors)
            return
        if ' ' in directory:
            cont = QtGui.QMessageBox.warning(
                self,
                'Warning',
                'Warning! Your selected project path contains spaces.\n\nThis will'
                ' cause some CCP-EM features to fail, though most of the main tasks'
                ' should still work correctly.\n\nYou can continue anyway if you'
                ' choose, but we suggest you cancel and select a path with no spaces to'
                ' ensure CCP-EM works properly. Continue anyway?',
                buttons=(QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel),
                defaultButton=QtGui.QMessageBox.Cancel
            )
            if cont == QtGui.QMessageBox.Cancel:
                return
        self.project.name = name
        self.project.path = directory
        self.project.user = str(self.user_edit.text())
        QtGui.QDialog.accept(self)


class CCPEMProjectsWidget(QtGui.QDialog):

    def __init__(self, ccpem_projects, parent=None):
        super(CCPEMProjectsWidget, self).__init__(parent)
        self.projects = ccpem_projects
        self.active_project_button = QtGui.QPushButton()
        self.tree_widget = QtGui.QTreeWidget()
        self.tree_widget.setToolTip('Double click path to explore')
        add_project_button = QtGui.QPushButton('Add project')
        add_project_button.setToolTip('Add new project to CCP-EM')
        open_project_button = QtGui.QPushButton('Open project')
        open_project_button.setToolTip('Open selected project')
        remove_project_button = QtGui.QPushButton('Remove project')
        remove_project_button.setToolTip('Remove project from CCP-EM')
        remove_user_button = QtGui.QPushButton('Remove user')
        remove_user_button.setToolTip('Remove user from CCP-EM')

        # Set layout
        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(self.active_project_button)
        vbox.addWidget(self.tree_widget)
        button_layout = QtGui.QHBoxLayout()
        button_layout.addWidget(open_project_button)
        button_layout.addWidget(add_project_button)
        button_layout.addWidget(remove_project_button)
        button_layout.addWidget(remove_user_button)
        vbox.addLayout(button_layout)
        self.setLayout(vbox)

        open_project_button.clicked.connect(self.select_active_project)
        add_project_button.clicked.connect(self.add_project)
        remove_project_button.clicked.connect(self.remove_project)
        remove_user_button.clicked.connect(self.remove_user)
        self.tree_widget.doubleClicked.connect(self.view_project)
        self.active_project_button.clicked.connect(self.view_active_project)
        self.setWindowTitle('CCPEM Projects')
        self.update_tree()

    def set_active_project_display(self):
        active_project = self.projects.get_active_project()
        if active_project is None:
            self.active_project_text = 'No active project: please add a project'
        else:
            self.active_project_text = '''Active project:
    User : {0}
    Name : {1}
    Path : {2}'''.format(active_project.user,
                         active_project.name,
                         active_project.path)
            self.active_project_button.setToolTip('Click to explore')
        self.active_project_button.setText(self.active_project_text)
        self.active_project_button.setStyleSheet("Text-align:left")

    def update_tree(self):
        self.projects.save()
        self.set_active_project_display()
        selected = None
        self.tree_widget.clear()
        self.tree_widget.setColumnCount(2)
        self.tree_widget.setHeaderLabels(['User | Project', 'Path'])
        self.tree_widget.setItemsExpandable(True)
        parent_from_user = {}
        for project in self.projects:
            parent = parent_from_user.get(project.user)
            if parent is None:
                parent = QtGui.QTreeWidgetItem(self.tree_widget,
                                               [project.user])
                parent.setData(0,
                               QtCore.Qt.UserRole,
                               QtCore.QVariant(project.user))
                parent_from_user[project.user] = parent
            item = QtGui.QTreeWidgetItem(
                parent,
                [project.name, QtCore.QString('%L1').arg(project.path)])
            item.setData(0,
                         QtCore.Qt.UserRole,
                         QtCore.QVariant(project.identity()))
            item.setTextAlignment(
                1,
                QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
            self.tree_widget.expandItem(parent)
        self.tree_widget.resizeColumnToContents(0)
        self.tree_widget.resizeColumnToContents(1)
        if selected is not None:
            selected.setSelected(True)
            self.tree_widget.setCurrentItem(selected)

    def reject(self):
        self.accept()

    def accept(self):
        QtGui.QDialog.accept(self)

    def no_selected_project_warning(self):
        message = 'No project selected'
        QtGui.QMessageBox.warning(
            self,
            'CCP-EM warning',
            message)

    def no_selected_user_warning(self):
        message = 'No user selected'
        QtGui.QMessageBox.warning(
            self,
            'CCP-EM warning',
            message)


    def current_tree_project(self):
        item = self.tree_widget.currentItem()
        if item is None:
            return None
        identity = str(item.data(0, QtCore.Qt.UserRole).toString())
        if identity == 0:
            return None
        return self.projects.project(identity)

    def current_tree_user(self):
        item = self.tree_widget.currentItem()
        if item is None:
            return None
        identity = str(item.data(0, QtCore.Qt.UserRole).toString())
        return identity

    def add_project(self):
        path = ccpem_utils.get_home_directory()
        path = os.path.join(
            path,
            'ccpem_project')
        project = project_manager.CCPEMProject(user='user',
                                               name='ccpem_project',
                                               path=path,
                                               warning=False)
        dialog = AddProjectDialog(project=project,
                                  projects=self.projects,
                                  parent=self)
        ret = dialog.exec_()
        if ret > 0:
            project = dialog.project
            self.projects.add_project(project)
        self.update_tree()

    def select_active_project(self):
        project = self.current_tree_project()
        if project is None:
            self.no_selected_project_warning()
            return
        self.projects.set_active_project_id(project.identity())
        self.update_tree()

    def view_active_project(self):
        active_project = self.projects.get_active_project()
        if active_project is None:
            self.no_selected_project_warning()
            return
        path = 'file:///' + active_project.path
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(path))

    def view_project(self):
        item = self.tree_widget.currentItem()
        if item is None:
            self.no_selected_project_warning()
            return None
        identity = str(item.data(0, QtCore.Qt.UserRole).toString())
        if identity is None:
            return None
        project = self.projects.project(identity)
        if project is None:
            return None
        QtGui.QDesktopServices.openUrl(QtCore.QUrl('file:///' + project.path))

    def remove_project(self):
        project = self.current_tree_project()
        if project is None:
            self.no_selected_project_warning()
            return
        if QtGui.QMessageBox.question(
               self,
               'Remove project',
               QtCore.QString('Remove project %1 (data to be removed seperately)?').arg(project.name),
               QtGui.QMessageBox.Yes | QtGui.QMessageBox.No) == QtGui.QMessageBox.No:
            return
        self.projects.remove_project(project)
        # Launch native file viewer to prompt user to manually remove project
        path = 'file:///' + project.path
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(path))
        self.update_tree()

    def remove_user(self):
        user = self.current_tree_user()
        if user is None:
            self.no_selected_user_warning()
            return
        if QtGui.QMessageBox.question(
                self,
                'Remove user',
                QtCore.QString('Remove %1 (data to be removed seperately)?').arg(user),
                QtGui.QMessageBox.Yes | QtGui.QMessageBox.No) == QtGui.QMessageBox.No:
            return
        self.projects.remove_user(user)
        self.update_tree()


def main():
    app = QtGui.QApplication(sys.argv)
    ccpem_projects = project_manager.CCPEMProjectContainer(
        filename='projects.json')
    project_widget = CCPEMProjectsWidget(ccpem_projects=ccpem_projects)
    project_widget.show()
    app.exec_()

if __name__ == '__main__':
    main()
