#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.

import os
import shutil
from PyQt4 import QtSql
from PyQt4 import QtCore
from PyQt4 import QtGui
from ccpem_core import ccpem_utils
from ccpem_gui import project_database


class CCPEMDatabase(object):
    '''
    CCPEMDatabase object.  Sqlite database, at present stores:
        job history
        programs
    '''
    # Set filename for project database
    ccpem_db_filename = '.ccpemDB.sqlite'

    def __init__(self, database_path, max_connections=100):
        super(CCPEMDatabase, self).__init__()
        # Self complied PyQt requires plugins directory to be set
        if not QtSql.QSqlDatabase.isDriverAvailable('QSQLITE'):
            find = QtSql.__file__.find('/lib/')
            plugin_dir = os.path.join(QtSql.__file__[0:find], 'lib/qt4/plugins')
            assert os.path.exists(plugin_dir)
            QtGui.QApplication.addLibraryPath(plugin_dir)
        # Assert QtSql driver is available
        assert QtSql.QSqlDatabase.isDriverAvailable('QSQLITE')
        self.database_path = database_path
        # Process & thread safe connection.  New connection is made is each
        # time a new database object is called.  Connection name set
        # automatically based on active connections.
        connection_prefix = 'CCPEM_'
        connection_names = map(str, QtSql.QSqlDatabase.connectionNames())
        for n in xrange(max_connections-1):
            connection_name = connection_prefix + str(n)
            if n > max_connections-2:
                print "\n\nError: max number of database connections made"
                assert 0
            elif connection_name in connection_names:
                continue
            else:
                break
        self.database = QtSql.QSqlDatabase.addDatabase('QSQLITE',
                                                       connection_name)
        self.connect()
        self.create_db()

    def connect(self):
        if not os.path.exists(os.path.dirname(self.database_path)):
            os.makedirs(os.path.dirname(self.database_path))
        self.database.setDatabaseName(self.database_path)
        self.database.open()
        q = QtSql.QSqlQuery(self.database)
        assert q.exec_('PRAGMA foreign_keys=ON;')
        assert self.database.isOpen()

    def close_connection(self):
        assert self.database.open()
        assert self.database.transaction()
        assert self.database.commit()
        connection = self.database.connectionName()
        self.database.close()
        assert not self.database.isOpen()
        # Must delete self.database prior to removeDatabase for garbage collection
        del self.database
        QtSql.QSqlDatabase.removeDatabase(connection)

    def run_query(self, string):
        assert self.database.isOpen()
        q = QtSql.QSqlQuery(self.database)
        assert q.exec_(string)
#        # XXX Debug
#        if 1:
#            print q.lastQuery()
#            print 'Err      : ', q.lastError().text()
#            print 'Err text : ', q.lastError().databaseText()
        q.finish()

    def create_db(self):
        self.connect()
        if not self.database.tables(QtSql.QSql.Tables).contains('programs'):
            self.create_program_table()
        if not self.database.tables(QtSql.QSql.Tables).contains('jobs'):
            self.create_job_table()

    def create_job_table(self):
        assert self.database.tables(QtSql.QSql.Tables).contains('programs')
        # Individual job table
        sqlstring = '''
                CREATE TABLE jobs (
                    id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL,
                    program VARCHAR(255) references programs(id)
                                  on delete restrict
                                  deferrable initially deferred,
                    mode VARCHAR(255),
                    dirpath VARCHAR(255),
                    user VARCHAR(255),
                    title VARCHAR(255),
                    status VARCHAR(255) NOT NULL,
                    readytime DATATIME NOT NULL,
                    starttime DATATIME,
                    endtime DATATIME
                );
              '''
        self.run_query(sqlstring)

    def insert_new_job(self,
                       program,
                       job_location,
                       mode=None,
                       user=None,
                       job_id=None,
                       title=''):
        '''
        Insert a new job
        '''
        assert self.database.tables(QtSql.QSql.Tables).contains('jobs')
        assert self.database.tables(QtSql.QSql.Tables).contains('programs')
        # Finds lastest version of program if program_id not defined or not found
        self.insert_new_program(
            program_id=program,
            name='',
            module='',
            version='',
            description='',
            author='')
        if job_id is None:
            prepare_str = '''
                INSERT INTO jobs (program, mode, user, title, status, readytime)
                VALUES (:program, :mode, :user, :title, :status, :readytime)
                          '''
        else:
            prepare_str = '''
                REPLACE INTO jobs (id, program, mode, user, title, status, readytime, dirpath)
                VALUES (:id, :program, :mode, :user, :title, :status, :readytime, :dirpath)
                           '''
        q = QtSql.QSqlQuery(self.database)
        q.prepare(prepare_str)
        q.bindValue(':program', QtCore.QVariant(QtCore.QString(program)))
        if mode is None:
            mode = 'None'
        q.bindValue(':mode', QtCore.QVariant(QtCore.QString(mode)))
        if user is None:
            user = 'None'
        q.bindValue(':user', QtCore.QVariant(QtCore.QString(user)))
        q.bindValue(':title', QtCore.QVariant(QtCore.QString(title)))
        q.bindValue(':status', QtCore.QVariant(QtCore.QString('ready')))
        q.bindValue(':readytime',
                    QtCore.QVariant(QtCore.QDateTime.currentDateTime()))
        if job_id is not None:
#             q.bindValue(':location', QtCore.QVariant(QtCore.QString(job_location)))
            q.bindValue(':id', QtCore.QVariant(job_id))
            q.bindValue(':dirpath', QtCore.QVariant(QtCore.QString(job_location)))

        assert q.exec_()
        if job_id is None:
            job_id = q.lastInsertId().toInt()[0]
            prepare_str = '''UPDATE jobs SET dirpath = :dirpath WHERE id = :id'''
            q.prepare(prepare_str)
            q.bindValue(':location', QtCore.QVariant(QtCore.QString(job_location)))
            q.bindValue(':id', QtCore.QVariant(job_id))
            assert q.exec_()
        ccpem_utils.check_directory_and_make(directory=job_location)
        q.finish()
        return job_id

    def delete_job(self, job_id, remove_data=True):
        job_info = self.get_job_information(job_id=job_id)
        if remove_data:
            if job_info['status'] in ['finished', 'failed']:
                job_directory = str(job_info['dirpath'])
                if os.path.exists(job_directory):
                    if os.path.isdir(job_directory):
                        try:
                            shutil.rmtree(job_directory)
                        except:
                            pass
        assert self.database.tables(QtSql.QSql.Tables).contains('jobs')
        q = QtSql.QSqlQuery(self.database)
        prepare_str = '''DELETE FROM jobs WHERE ID = :id'''
        q.prepare(prepare_str)
        q.bindValue(':id', QtCore.QVariant(job_id))
        assert q.exec_()
        q.finish()

    def show_all_jobs(self):
        assert self.database.tables(QtSql.QSql.Tables).contains('jobs')
        # Speed up querry
        q = QtSql.QSqlQuery(self.database)
        q.setForwardOnly(True)
        q_str = '''SELECT id, program, dirpath, user, title, status, readytime, starttime, endtime
                FROM jobs ORDER BY id
                '''
        q.exec_(q_str)
        # Show results
        ID, PROGRAM, DIRPATH, USER, TITLE, STATUS, READYTIME, STARTTIME, ENDTIME = range(9)
        while q.next():
            print '\njob id       :', q.value(ID).toInt()[0]
            print 'program      :', q.value(PROGRAM).toString()
            print 'dirpath      :', q.value(DIRPATH).toString()
            print 'user         :', q.value(USER).toString()
            print 'title        :', q.value(TITLE).toString()
            print 'status       :', q.value(STATUS).toString()
            print 'readytime    :', q.value(READYTIME).toString()
            print 'starttime    :', q.value(STARTTIME).toString()
            print 'endtime      :', q.value(ENDTIME).toString()
        q.finish()

    def get_status_of_jobs(self, key_by=''):
        assert self.database.tables(QtSql.QSql.Tables).contains('jobs')
        job_history = {}
        # Speed up querry
        q = QtSql.QSqlQuery(self.database)
        q.setForwardOnly(True)
        q_str = '''SELECT id, status, dirpath FROM jobs ORDER BY id'''
        q.exec_(q_str)
        # Show results
        ID, STATUS, DIRPATH = range(3)
        while q.next():
            if key_by == 'dirpath':
                job_history[str(q.value(DIRPATH).toString())]=str(q.value(STATUS).toString())
            else:
                job_history[q.value(ID).toInt()[0]]=str(q.value(STATUS).toString())
        q.finish()
        return job_history

    def get_job_information(self, job_id):
        assert self.database.tables(QtSql.QSql.Tables).contains('jobs')
        # search for job info
        q = QtSql.QSqlQuery(self.database)
        q.setForwardOnly(True)
        prep_str = '''
        SELECT id, program, dirpath, user, title, status, readytime, starttime, endtime
        FROM jobs
        WHERE id = :id
        '''
        q.prepare(prep_str)
        q.bindValue(':id', QtCore.QVariant(job_id))
        assert q.exec_()
        # get results
        ID, PROGRAM, DIRPATH, USER, TITLE, STATUS, READYTIME, STARTTIME, ENDTIME = range(9)
        q.first()
        job_info = {}
        if not q.isValid():
            print 'Error: job ID {0} not found'.format(job_id)
            return
        job_info['job_id'] = q.value(ID).toInt()[0]
        job_info['program'] = q.value(PROGRAM).toString()
        job_info['dirpath'] = q.value(DIRPATH).toString()
        job_info['user'] = q.value(USER).toString()
        job_info['title'] = q.value(TITLE).toString()
        job_info['status'] = q.value(STATUS).toString()
        job_info['readytime'] = q.value(READYTIME).toString()
        job_info['starttime'] = q.value(STARTTIME).toString()
        job_info['endtime'] = q.value(ENDTIME).toString()
        q.finish()
        return job_info

    def update_job_start(self, job_id):
        self.connect()
        assert self.database.tables(QtSql.QSql.Tables).contains('jobs')
        q = QtSql.QSqlQuery(self.database)
        prepare_str = '''
        UPDATE jobs SET status = 'started', starttime = :starttime
        WHERE id = :id
        '''
        q.prepare(prepare_str)
        q.bindValue(':id', QtCore.QVariant(job_id))
        q.bindValue(':starttime', QtCore.QVariant(QtCore.QDateTime.currentDateTime()))
        assert q.exec_()
        q.finish()

    def update_job_end(self, job_id, failed=False):
        self.connect()
        assert self.database.tables(QtSql.QSql.Tables).contains('jobs')
        q = QtSql.QSqlQuery(self.database)
        if failed:
            prepare_str = '''
            UPDATE jobs SET status = 'failed', endtime = :endtime WHERE id = :id
            '''
        else:
            prepare_str = '''
            UPDATE jobs SET status = 'finished', endtime = :endtime WHERE id = :id
            '''
        q.prepare(prepare_str)
        q.bindValue(':id',
                    QtCore.QVariant(job_id))
        q.bindValue(':endtime',
                    QtCore.QVariant(QtCore.QDateTime.currentDateTime()))
        assert q.exec_()
        q.finish()

    def create_program_table(self):
        self.connect()
        # Program table
        sqlstring = '''
                CREATE TABLE programs
                (
                    id varchar(255) PRIMARY KEY UNIQUE NOT NULL,
                    name varchar(255) NOT NULL,
                    version varchar(255) NOT NULL,
                    module varchar(255) NOT NULL,
                    description text NOT NULL,
                    author varchar(255) NOT NULL
                );
                '''
        self.run_query(sqlstring)
        self.add_default_programs()

    def insert_new_program(self,
                           program_id,
                           name,
                           version,
                           module,
                           description='',
                           author=''):
        '''
        Insert a new program.  Note that program id is unique and is concatation
        of name and version string: name_version (e.g. myProg_0.1)
        '''
        self.connect()
        # Add programs
        assert self.database.tables(QtSql.QSql.Tables).contains('programs')
        # N.B. if unique id key already present entry is ignored
        prepare_str = '''
        INSERT OR IGNORE INTO programs (id, name, version, module, description, author)
        VALUES (:id, :name, :version, :module, :description, :author)
        '''
        q = QtSql.QSqlQuery(self.database)
        q.prepare(prepare_str)
        q.bindValue(':id', QtCore.QVariant(QtCore.QString(program_id)))
        q.bindValue(':name', QtCore.QVariant(QtCore.QString(name)))
        q.bindValue(':version', QtCore.QVariant(QtCore.QString(version)))
        q.bindValue(':module', QtCore.QVariant(QtCore.QString(module)))
        q.bindValue(':description', QtCore.QVariant(QtCore.QString(description)))
        q.bindValue(':author', QtCore.QVariant(QtCore.QString(author)))
        assert q.exec_()
        q.finish()

    def add_default_programs(self):
        assert self.database.tables(QtSql.QSql.Tables).contains('programs')
        self.insert_new_program(program_id='tst_prog-0.1',
                                name='tst_prog',
                                version='0.1',
                                module='tst',
                                description='a test entry',
                                author='tom')

    def get_latest_program_id(self, program_name, return_version=False):
        '''
        Return full program id for latest version of given program
        '''
        assert self.database.tables(QtSql.QSql.Tables).contains('programs')
        q = QtSql.QSqlQuery(self.database)
        q.setForwardOnly(True)
        prep_str = '''
        SELECT id, version FROM programs
        WHERE name = :name ORDER BY version DESC
        '''
        q.prepare(prep_str)
        q.bindValue(':name', QtCore.QVariant(program_name))
        assert q.exec_()
        q.first()
        ID, VERSION = range(2)
        program_id = q.value(ID).toString()
        version = q.value(VERSION).toString()
        q.finish()
        if return_version:
            return version
        else:
            return program_id

    def get_program_info(self, program_id):
        assert self.database.tables(QtSql.QSql.Tables).contains('programs')
        # search for job info
        q = QtSql.QSqlQuery(self.database)
        q.setForwardOnly(True)
        prep_str = '''
        SELECT id, name, version, module, description, author
        FROM programs
        WHERE id = :program_id
        '''
        q.prepare(prep_str)
        q.bindValue(':program_id', QtCore.QVariant(program_id))
        assert q.exec_()
        # get results
        PROGRAM_ID, NAME, VERSION, MODULE, DESCRIPTION, AUTHOR = range(6)
        q.first()
        prog_info = {}
        if not q.isValid():
            print 'Error: program ID {0} not found'.format(program_id)
            return
        prog_info['program_id'] = q.value(PROGRAM_ID).toInt()[0]
        prog_info['name'] = q.value(NAME).toString()
        prog_info['version'] = q.value(VERSION).toString()
        prog_info['module'] = q.value(MODULE).toString()
        prog_info['description'] = q.value(DESCRIPTION).toString()
        prog_info['author'] = q.value(AUTHOR).toString()
        q.finish()
        return prog_info

def add_program_info_to_database(database_path,
                                 program_info):
    database = project_database.sqlite_project_database.CCPEMDatabase(
        database_path=database_path)
    database.insert_new_program(program_id=program_info.program_id,
                                name=program_info.jobname,
                                module=program_info.module,
                                version=program_info.version,
                                description=program_info.description,
                                author=program_info.author)
    database.close_connection()


class CCPEMInjector(object):
    '''
    CCPEMDatabase injector
    '''
    def __init__(self, database_path):
        self.database_path = database_path
        self.db = None

    def connect_db(self):
        db = CCPEMDatabase(
            database_path=self.database_path)
        return db
