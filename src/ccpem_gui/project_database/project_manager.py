#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import json
import os
from ccpem_core import ccpem_utils
from ccpem_core import settings
from PyQt4 import QtGui

class CCPEMProject(object):
    def __init__(self, user, name, path, warning=True):
        self.user = user
        self.name = name
        self.path = path
        if self.path is not None:
            if warning:
                if not os.path.exists(self.path):
                    self.gui_warning()

    def identity(self):
        return 'user_' + self.user+'_name_' + self.name + '_path_' + self.path

    def gui_warning(self):
        message =  'Warning project directory not found:\n {0}'.format(
            self.path)
        QtGui.QMessageBox.warning(
            None,
           'CCP-EM error',
           message)


class CCPEMProjectContainer(object):
    def __init__(self, filename):
        self.filename = filename
        self.projects = {}
        self.active_project_id = None
        self.project_paths = set()
        self.project_names = set()
        if os.path.exists(self.filename):
            self.load()
        else:
            print 'No project'

    def __len__(self):
        return len(self.projects)

    def __iter__(self):
        for project in self.projects.values():
            yield project

    def set_active_project_id(self, identity):
        self.active_project_id = identity

    def get_active_project(self):
        if self.active_project_id is None:
            return None
        return self.projects.get(self.active_project_id)

    def get_active_project_path(self):
        if self.active_project_id is None:
            return None
        if not os.path.exists(self.projects.get(self.active_project_id).path):
            return None
        else:
            return self.projects.get(self.active_project_id).path

    def project(self, identity):
        return self.projects.get(identity)

    def inOrder(self):
        return sorted(self.projects.values())

    def add_project(self, project):
        # Name and path must be unique
        if project.name in self.project_names:
            return
        self.project_names.update([project.name])
        if project.path in self.project_paths:
            return
        self.project_paths.update([project.path])
        self.projects[project.identity()] = project
        self.set_active_project_id(project.identity())

    def remove_project(self, project):
        if project.identity() == self.active_project_id:
            'Reset identity'
            self.active_project_id = None
        del self.projects[project.identity()]
        del project

    def remove_user(self, user):
        for project in self.projects.values():
            if project.user == user:
                self.remove_project(project)

    def save(self):
        project_manager = {}
        projects = []
        for project in self.projects.values():
            projects.append({'user': project.user,
                             'name': project.name,
                             'path': project.path})
        project_manager['projects'] = projects
        project_manager['active_project_id'] = self.active_project_id
        if self.filename != '':
            ccpem_utils.atomic_write_json(project_manager,
                                          self.filename,
                                          sort_keys=True,
                                          separators=(',', ': '),
                                          indent=4)

    def load(self):
        project_manager = json.load(fp=open(self.filename, 'r'))
        for project in project_manager['projects']:
            p = CCPEMProject(user=str(project['user']),
                             name=str(project['name']),
                             path=str(project['path']))
            self.add_project(p)
        identity = project_manager.get('active_project_id')
        self.set_active_project_id(identity)


def get_db_path_from_project_name(project_name):
    # Get ccpem projects
    project_json = settings.get_ccpem_settings().projects.value
    if project_json is not None:
        ccpem_projects = CCPEMProjectContainer(
            filename=project_json)
        # Find project
        for project in ccpem_projects:
            if project.name == project_name:
                # Find db path and check exists
                db_path = os.path.join(project.path, '.ccpemDB.sqlite')
                if os.path.exists(db_path):
                    return db_path
    return None

def get_project_from_db_path(path):
    # Get ccpem projects
    project_json = settings.get_ccpem_settings().projects.value
    if project_json is not None:
        ccpem_projects = CCPEMProjectContainer(
            filename=project_json)
        # Find name
        for project in ccpem_projects:
            if project.path == os.path.dirname(os.path.abspath(path)):
                return project
    return None

def main():
    db_path = get_db_path_from_project_name(project_name='My_project')
    print db_path

if __name__ == '__main__':
    main()
