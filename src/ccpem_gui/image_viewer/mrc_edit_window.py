#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import sys
import collections
import numpy as np

import mrcfile
from PyQt4 import QtGui, QtCore

import ccpem_core.gui_ext as icons
from ccpem_gui.image_viewer import image_utils


class CCPEMMrcEditWindow(QtGui.QMainWindow):
    '''
    CCPEM MRC edit window.  Utility for editing mrc headers.
    '''
    def __init__(self,
                 filename=None,
                 parent=None):
        super(CCPEMMrcEditWindow, self).__init__(parent)
        self.setWindowTitle('CCPEM | MRC Edit')
        header_editor = CCPEMMrcHeaderEditWidget(filename=filename,
                                                 parent=self)
        scroll = QtGui.QScrollArea()
        scroll.setWidget(header_editor)
        scroll.setWidgetResizable(True)
        self.setCentralWidget(scroll)

class CCPEMMrcHeaderEditWidget(QtGui.QGroupBox):
    '''
    CCPEM MRC header viewer widget.  Displays header contents.

    Used to allow editing functionality but that part is commented out pending
    a re-write. (The old numpy_mrc_io code used for this is incompatible with
    newer versions of numpy. We should re-write using mrcfile instead.)
    '''
    info = ('MRC image header viewing utility. '
            # '\nN.B. only header contents changed, for repairing file only.  '
            'For more information on MRC format please see ccpem.ac.uk')
    display_items = [
        collections.OrderedDict([('filename', 'Filename')]),
        collections.OrderedDict([('nx', 'Nx'), ('ny', 'Ny'), ('nz', 'Nz')]),
        collections.OrderedDict([('mode', 'Mode')]),
        collections.OrderedDict([('nxstart', 'Nxstart'), ('nystart', 'Nystart'), ('nzstart', 'Nzstart')]),
        collections.OrderedDict([('mx', 'Mx'), ('my', 'My'), ('mz', 'Mz')]),
        collections.OrderedDict([('cella.x', 'Xlen'), ('cella.y', 'Ylen'), ('cella.z', 'Zlen')]),
        collections.OrderedDict([('cellb.alpha', 'Alpha'), ('cellb.beta', 'Beta'), ('cellb.gamma', 'Gamma')]),
        collections.OrderedDict([('mapc', 'Mapc'), ('mapr', 'Mapr'), ('maps', 'Maps')]),
        collections.OrderedDict([('dmin', 'Dmin'), ('dmax', 'Dmax'), ('dmean', 'Dmean')]),
        collections.OrderedDict([('ispg', 'Ispg')]),
        collections.OrderedDict([('nsymbt', 'Nsymbt')]),
        collections.OrderedDict([('extra1', 'Extra data')]),
        collections.OrderedDict([('exttyp', 'Exttyp')]),
        collections.OrderedDict([('nversion', 'Nversion')]),
        collections.OrderedDict([('extra2', 'Extra data 2')]),
        collections.OrderedDict([('origin.x', 'OriginX'), ('origin.y', 'OriginY'), ('origin.z', 'OriginZ')]),
        collections.OrderedDict([('map', 'MAP'), ('machst', 'Machst')]),
        collections.OrderedDict([('rms', 'Rms'), ('nlabl', 'Nlabl')]),
        collections.OrderedDict([('label 1', 'Label 1')]),
        collections.OrderedDict([('label 2', 'Label 2')]),
        collections.OrderedDict([('label 3', 'Label 3')]),
        collections.OrderedDict([('label 4', 'Label 4')]),
        collections.OrderedDict([('label 5', 'Label 5')]),
        collections.OrderedDict([('label 6', 'Label 6')]),
        collections.OrderedDict([('label 7', 'Label 7')]),
        collections.OrderedDict([('label 8', 'Label 8')]),
        collections.OrderedDict([('label 9', 'Label 9')]),
        collections.OrderedDict([('label 10', 'Label 10')])
        ]

    mrc_ext = ''
    for ext in image_utils.mrc_ext:
        mrc_ext += ('*'+ext+' ')
    allowed_ext = 'MRC ({0});;All files (*.*)'.format(mrc_ext)

    def __init__(self,
                 filename=None,
                 parent=None):
        super(CCPEMMrcHeaderEditWidget, self).__init__(parent)
        self.filename = filename
        self.filename_out = None
        self.header = None
        self.data = None
        self.setTitle('MRC header info')
        self.vbox_layout = QtGui.QVBoxLayout()
        self.setLayout(self.vbox_layout)
        # Add utility information
        self.info_label = QtGui.QLabel()
        self.info_label.setWordWrap(True)
        self.info_label.setFrameStyle(QtGui.QFrame.StyledPanel |
                                      QtGui.QFrame.Sunken)
        self.info_label.setSizePolicy(QtGui.QSizePolicy.Minimum,
                                      QtGui.QSizePolicy.Maximum)
        self.info_label.setText(self.info)
        self.vbox_layout.addWidget(self.info_label)
        # Button box
        self.button_box = QtGui.QDialogButtonBox(
            QtGui.QDialogButtonBox.Open)
            # QtGui.QDialogButtonBox.Save)
        self.button_box.button(QtGui.QDialogButtonBox.Open).clicked.connect(
            self.open_file)
        # self.button_box.button(QtGui.QDialogButtonBox.Save).clicked.connect(
        #     self.save_file)
        # self.button_box.button(QtGui.QDialogButtonBox.Save).setEnabled(False)
        self.vbox_layout.addWidget(self.button_box)
        # Edit button
        # self.edit_button = QtGui.QCheckBox('Edit header')
        # self.edit_button.setCheckable(True)
        # self.edit_button.setChecked(False)
        # self.edit_button.setToolTip(
        #     'Allow header to be manually edited / saved.')
        # self.edit_button.pressed.connect(self.set_editable)
        #
        # self.vbox_layout.addWidget(self.edit_button)
        # Setup header display
        self.image_layout = QtGui.QGridLayout()
        self.vbox_layout.addLayout(self.image_layout)
        self.set_image_form()
        # Get header
        self.display_header()

    # def set_editable(self):
    #     if self.edit_button.isChecked():
    #         read_only = True
    #     else:
    #         read_only = False
    #     line_edits = self.findChildren(QtGui.QLineEdit)
    #     for line_edit in line_edits:
    #         line_edit.setReadOnly(read_only)
    #     self.button_box.button(QtGui.QDialogButtonBox.Save).setEnabled(True)

    def get_header_data(self):
        if self.filename is not None:
            self.mrc_file = mrcfile.open(self.filename, permissive=True)
        else:
            self.mrc_file = None

    def set_image_form(self):
        max_row_size = max(len(row) for row in self.display_items)
        max_row_size = (max_row_size*2)-1
        for row, items in enumerate(self.display_items):
            for col, item in enumerate(items):
                label = QtGui.QLabel(items[item], self)
                edit = QtGui.QLineEdit(self)
                edit.setReadOnly(True)
                edit.setObjectName(item)
                col_adj = col * 2
                self.image_layout.addWidget(label, row, col_adj)
                if len(items) == 1:
                    self.image_layout.addWidget(edit, row, col_adj+1, 1,
                                                max_row_size)
                else:
                    self.image_layout.addWidget(edit, row, col_adj+1, 1, 1)

    def display_header(self):
        self.get_header_data()
        if self.mrc_file is None:
            for row in self.display_items:
                for item in row:
                    line_edit = self.findChild(QtGui.QLineEdit, item)
                    line_edit.clear()
        else:
            for row in self.display_items:
                for item in row:
                    line_edit = self.findChild(QtGui.QLineEdit, item)
                    if line_edit is not None:
                        if '.' in item:
                            parts = item.split('.', 2)
                            val = str(self.mrc_file.header[parts[0]][parts[1]])
                        elif item in self.mrc_file.header.dtype.names:
                            val = str(self.mrc_file.header[item])
                        # Filename not stored in MRC header
                        elif item == 'filename':
                            val = self.filename
                        elif item.startswith('label'):
                            n = int(item.split()[1])
                            val = str(self.mrc_file.header['label'][n-1])
                        else:
                            val = ''
                        line_edit.setText(val)

    def open_file(self):
        filename = QtGui.QFileDialog.getOpenFileName(
            self,
            'Open MRC file',
            QtCore.QDir.currentPath(),
            self.allowed_ext)
        if filename != '':
            self.filename = str(filename)
            self.display_header()

    # def save_file(self):
    #     filename = QtGui.QFileDialog.getSaveFileName(
    #         self,
    #         'Save MRC file',
    #         QtCore.QDir.currentPath(),
    #         self.allowed_ext)
    #     self.filename_out = str(filename)
    #     if self.filename_out == '':
    #         self.filename_out = None
    #     else:
    #         self.write_file()

    # def write_file(self):
    #     header_dtype = mrc_io.create_header_dtype()
    #     new_header = np.ndarray(shape=(), dtype=header_dtype)
    #     new_header.setflags(write=True)
    #     for item in self.header.dtype.names:
    #         if item == 'labels':
    #             labels = []
    #             for n in xrange(1, 11):
    #                 item_n = 'label ' + str(n)
    #                 line_edit = self.findChild(QtGui.QLineEdit, item_n)
    #                 val = str(line_edit.text())
    #                 labels.append(val[0:80])
    #             new_header['labels'] = labels
    #             continue
    #         #
    #         line_edit = self.findChild(QtGui.QLineEdit, item)
    #         val = str(line_edit.text())
    #         if item == 'triangles' or item == 'stamp':
    #             f_list = []
    #             val = val.replace(']', '').replace('[', '').split()
    #             for n in val:
    #                 try:
    #                     f = float(n)
    #                     f_list.append(f)
    #                 except ValueError:
    #                     pass
    #             val = f_list
    #             if item == 'triangles':
    #                 assert len(val) == 6
    #             if item == 'stamp':
    #                 assert len(val) == 4
    #         new_header[item] = val
    #     if self.filename_out is None:
    #         self.filename_out = self.filename + '_ed.mrc'
    #     mrc_io.write_raw_mrc(filename=self.filename_out,
    #                          header=new_header,
    #                          data=self.data)


def main():
    '''
    For testing.
    '''
    app = QtGui.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon(icons.icon_utils.get_ccpem_icon()))
    mw = CCPEMMrcEditWindow(
        filename=None)
    mw.show()
    app.exec_()
    sys.exit()

if __name__ == '__main__':
    main()
