#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Task window for LAFTER
'''
import os

from ccpem_gui.utils import command_line_launch
from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_core.tasks.lafter import lafter_task
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import lafter as test_data


class LafterWindow(window_utils.CCPEMTaskWindow):
    '''
    LAFTER window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Input half map 1
        half_map_1_input = window_utils.FileArgInput(
            parent=self,
            arg_name='half_map_1',
            args=self.args,
            file_types=ccpem_file_types.mrc_ext,
            required=True)
        self.args_widget.args_layout.addWidget(half_map_1_input)

        # Input half map 2
        half_map_2_input = window_utils.FileArgInput(
            parent=self,
            arg_name='half_map_2',
            args=self.args,
            file_types=ccpem_file_types.mrc_ext,
            required=True)
        self.args_widget.args_layout.addWidget(half_map_2_input)

        # Input mask
        mask_input = window_utils.FileArgInput(
            parent=self,
            arg_name='mask',
            args=self.args,
            file_types=ccpem_file_types.mrc_ext,
            required=False)
        self.args_widget.args_layout.addWidget(mask_input)

        # Extended options
        extended_options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Extended options',
            button_tooltip='Show extended options')
        self.args_widget.args_layout.addLayout(extended_options_frame)

        # Particle diameter
        diameter_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='particle_diameter',
            args=self.args)
        extended_options_frame.add_extension_widget(diameter_input)

        # Additional sharpening
        self.sharp_input = window_utils.CheckArgInput(
            parent=self,
            arg_name='sharp',
            args=self.args)
        extended_options_frame.add_extension_widget(self.sharp_input)

        # FSC cut-off
        fsc_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='fsc',
            args=self.args)
        extended_options_frame.add_extension_widget(fsc_input)

        # Avoid up-sampling
        self.downsample_input = window_utils.CheckArgInput(
            parent=self,
            arg_name='downsample',
            args=self.args)
        extended_options_frame.add_extension_widget(self.downsample_input)

        # Compensate overfitting
        self.overfitting_input = window_utils.CheckArgInput(
            parent=self,
            arg_name='overfitting',
            args=self.args)
        extended_options_frame.add_extension_widget(self.overfitting_input)

        # Set inputs and defaults for launcher
        self.launcher.set_mg_default('Chimera')
        self.launcher.add_file(
            arg_name='half_map_1',
            file_type='map',
            description=self.args.half_map_1.help,
            selected=True)
        self.launcher.add_file(
            arg_name='half_map_2',
            file_type='map',
            description=self.args.half_map_2.help,
            selected=False)
        self.launcher.add_file(
            arg_name='mask',
            file_type='map',
            description=self.args.mask.help,
            selected=False)

    def set_on_job_finish_custom(self):

        noise_suppressed_map = os.path.join(self.pipeline.location, 'noise_suppressed.mrc')
        if os.path.isfile(noise_suppressed_map):
            self.launcher.add_file(
                arg_name=None,
                path=noise_suppressed_map,
                file_type='map',
                description='Intermediate noise-suppressed MRC file',
                selected=False)

        denoised_map = os.path.join(self.pipeline.location, 'LAFTER_filtered.mrc')
        if os.path.isfile(denoised_map):
            self.launcher.add_file(
                arg_name=None,
                path=denoised_map,
                file_type='map',
                description='Final denoised MRC file',
                selected=True)

        self.launcher.set_tree_view()
        self.launcher_dock.raise_()


def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=lafter_task.LafterTask,
        window_class=LafterWindow)


if __name__ == '__main__':
    main()
