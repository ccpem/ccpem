#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Task window for LocScale.
'''
import os

from PyQt4 import QtCore, QtGui

from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_core.tasks.loc_scale_interface import loc_scale_task
from ccpem_gui.utils import command_line_launch
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import loc_scale as test_data


class LocScaleWindow(window_utils.CCPEMTaskWindow):
    '''
    LocScale window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(LocScaleWindow, self).__init__(task=task,
                                              parent=parent)

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Input target map
        target_map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='target_map',
            args=self.args,
            file_types=ccpem_file_types.mrc_ext,
            required=True)
        self.args_widget.args_layout.addWidget(target_map_input)

        # Reference model
        reference_model_input = window_utils.FileArgInput(
            parent=self,
            arg_name='reference_model',
            args=self.args,
            file_types=ccpem_file_types.pdb_ext,
            required=True)
        self.args_widget.args_layout.addWidget(reference_model_input)

        # Ligand library input for Refmac5
        lib_input = window_utils.FileArgInput(
            parent=self,
            arg_name='lib_in',
            args=self.args,
            file_types=ccpem_file_types.lib_ext,
            required=False)
        self.args_widget.args_layout.addWidget(lib_input)

        # Input mask map
        mask_map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='mask_map',
            args=self.args,
            file_types=ccpem_file_types.mrc_ext,
            required=False)
        self.args_widget.args_layout.addWidget(mask_map_input)

        # Resolution
        resolution_input = window_utils.NumberArgInput(
            parent=self,
            decimals=1,
            step=0.1,
            arg_name='resolution',
            args=self.args,
            required=True)
        self.args_widget.args_layout.addWidget(resolution_input)

        # Use MPI input

        # Disable if mulitple cores not available
        if QtCore.QThread.idealThreadCount() == 1:
            self.args.use_mpi.value = False

        use_mpi_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='use_mpi',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(use_mpi_input)
        use_mpi_input.value_line.currentIndexChanged.connect(
            self.set_n_mpi_visible)

        # Container widget for controls for number of MPI processes
        # This needs to be a widget (and not just a layout) so it can be 
        # shown and hidden easily
        self.mpi_widget = QtGui.QWidget()
        mpi_layout = QtGui.QHBoxLayout()
        self.mpi_widget.setLayout(mpi_layout)
        self.args_widget.args_layout.addWidget(self.mpi_widget)

        self.n_mpi_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='n_mpi',
            required=False,
            args=self.args)
        mpi_layout.addWidget(self.n_mpi_input)
        self.set_n_mpi_visible()

        set_mpi_all_cpus_button = QtGui.QPushButton('Use all CPUs')
        set_mpi_all_cpus_button.clicked.connect(self.set_n_mpi_all_cores)
        set_mpi_all_cpus_button.setToolTip("Set the number of MPI nodes to use all of the CPU cores on this computer")
        mpi_layout.addWidget(set_mpi_all_cpus_button)

        set_mpi_half_cpus_button = QtGui.QPushButton('Use half CPUs')
        set_mpi_half_cpus_button.clicked.connect(self.set_n_mpi_half_cores)
        set_mpi_half_cpus_button.setToolTip("Set the number of MPI nodes to use half of the CPU cores on this computer")
        mpi_layout.addWidget(set_mpi_half_cpus_button)

        # Add horizontal stretch so buttons don't expand sideways
        mpi_layout.addStretch()

        # Extended options
        extended_options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Extended options',
            button_tooltip='Show extended options')
        self.args_widget.args_layout.addLayout(extended_options_frame)

        # Reference model
        reference_map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='reference_map',
            args=self.args,
            file_types=ccpem_file_types.mrc_ext,
            required=False)
        extended_options_frame.add_extension_widget(reference_map_input)

        # Pixel size
        pixel_input = window_utils.NumberArgInput(
            parent=self,
            decimals=3,
            step=0.1,
            arg_name='pixel_size',
            args=self.args,
            required=False)
        extended_options_frame.add_extension_widget(pixel_input)

        # Set window size
        scale_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='window_size',
            required=False,
            args=self.args)
        extended_options_frame.add_extension_widget(scale_input)

        # Set inputs for launcher
        self.launcher.add_file(
            arg_name='target_map',
            file_type='map',
            description=self.args.target_map.help,
            selected=True)
        self.launcher.add_file(
            arg_name='reference_model',
            file_type='pdb',
            description=self.args.reference_model.help,
            selected=True)

    def set_n_mpi_all_cores(self):
        self.n_mpi_input.set_arg_value(QtCore.QThread.idealThreadCount())

    def set_n_mpi_half_cores(self):
        self.n_mpi_input.set_arg_value(
            QtCore.QThread.idealThreadCount() / 2)

    def set_n_mpi_visible(self):
        if self.args.use_mpi():
            self.mpi_widget.show()
        else:
            self.mpi_widget.hide()

    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.  Show LocScale map
        '''
        # Set launcher files
        output_map_path = os.path.join(self.task.job_location,
                                       'loc_scale.mrc')
        self.launcher.add_file(
            arg_name=None,
            path=output_map_path,
            file_type='map',
            description='LocScale scaled map',
            selected=True)
        self.launcher.add_file(
            arg_name='reference_map',
            file_type='map',
            description=self.args.reference_map.help,
            selected=True)
        self.launcher.set_tree_view()
        self.launcher_dock.raise_()

def main():
    '''
    Launch standalone task runner.
    '''
#     from ccpem_core.test_data.tasks import loc_scale as loc_scale_td
#     args_json = os.path.join(os.path.dirname(loc_scale_td.__file__),
#                              'unittest_args.json')
    command_line_launch.ccpem_task_launch(
        task_class=loc_scale_task.LocScale,
        window_class=LocScaleWindow)

if __name__ == '__main__':
    main()
