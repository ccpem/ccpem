#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Test ProShade task and GUI
'''

import unittest
import os
import sys
import shutil
import time
import tempfile
from PyQt4 import QtGui, QtCore
from PyQt4.QtTest import QTest
from ccpem_core.tasks.proshade import proshade_task
from ccpem_gui.tasks.proshade import proshade_window
from ccpem_core.test_data.tasks import proshade as proshade_test
from ccpem_core import ccpem_utils
from ccpem_core import process_manager

app                                           = QtGui.QApplication ( sys.argv )


def run_task_and_get_tail(run_task, tail_maxlen=100):
    # Run w/ gui
    window = proshade_window.ProShadeWindow(task=run_task)

    # Mouse click run
    QTest.mouseClick(window.tool_bar.widgetForAction(window.tb_run_button),
                     QtCore.Qt.LeftButton)

    # Wait for run to complete
    job_completed = False
    timeout = 0
    tail = ''

    # Global refine stdout (i.e. last job in pipeline)
    stdout = run_task.pipeline.pipeline[-1][-1].stdout
    delay = 1.0
    while not job_completed and timeout <= 10:
        time.sleep(delay)
        timeout += delay
        status = process_manager.get_process_status(run_task.pipeline.json)
        assert status != 'failed'
        if status == 'finished':
            if os.path.isfile(stdout):
                tail = ccpem_utils.tail(stdout, maxlen=tail_maxlen)
                if tail.find('CCP-EM process finished') != -1:
                    job_completed = True

    # Check timeout
    assert timeout <= 10

    # Check job completed
    assert job_completed

    return tail


class ProShadeTest ( unittest.TestCase ):
    '''
    Unit tests for ProShade (invokes GUI).
    '''
    
    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_data                        = os.path.dirname ( proshade_test.__file__ )
        self.test_output                      = tempfile.mkdtemp ( )
        os.chdir ( os.path.dirname ( os.path.realpath ( __file__ ) ) )

    def tearDown(self):
        if os.path.exists ( self.test_output ):
            shutil.rmtree ( self.test_output )

    def test_proshade_symmetry_default ( self ):
        '''
        Test ProShade default symmetry detection via GUI.
        '''
        ccpem_utils.print_header              ( message = 'Unit test - ProShade Symmetry Default' )
        
        # Unit test args contain relative paths, must change to this directory
        args_path = os.path.join ( self.test_data, 'unittest_args_Sym1.json' )
        
        run_task = proshade_task.ProShade (
            job_location                      = self.test_output,
            args_json                         = args_path )

        tail = run_task_and_get_tail(run_task)
        
        # Check if we can find the correct D7-2 and assert it.
        testPassed                            = False
        if tail.find ( 'Detected Dihedral symmetry' ) != -1:
            if tail.find ( 'C      7    -0.02   +0.02   +1.00    2pi / 7    +0.534' ) != -1:
                testPassed                    = True
        assert testPassed == True
        
    def test_proshade_symmetry_parameters ( self ):
        '''
        Test ProShade parameters symmetry detection via GUI.
        '''
        ccpem_utils.print_header              ( message = 'Unit test - ProShade Symmetry Parameters' )
        
        # Unit test args contain relative paths, must change to this directory
        args_path = os.path.join ( self.test_data, 'unittest_args_Sym2.json' )
        
        run_task = proshade_task.ProShade (
            job_location                      = self.test_output,
            args_json                         = args_path )

        tail = run_task_and_get_tail(run_task)
        
        # Check if we can find the correct D7-2 and assert it.
        testPassed                            = False
        if tail.find ( 'Detected Cyclic symmetry' ) != -1:
            if tail.find ( 'C      2    +0.00   +1.00   +0.05    2pi / 2    +0.800' ) != -1:
                testPassed                    = True
        assert testPassed == True

    def test_proshade_rebox_default ( self ):
        '''
        Test ProShade default re-boxing detection via GUI.
        '''
        ccpem_utils.print_header              ( message = 'Unit test - ProShade Re-boxing Default' )
        
        # Unit test args contain relative paths, must change to this directory
        args_path = os.path.join ( self.test_data, 'unittest_args_ReB1.json' )
        
        run_task = proshade_task.ProShade (
            job_location                      = self.test_output,
            args_json                         = args_path )

        # Run w/ gui
        window                                = proshade_window.ProShadeWindow ( task = run_task )

        tail = run_task_and_get_tail(run_task, tail_maxlen=10)
        
        # Check if we can find the correct D7-2 and assert it.
        testPassed                            = False
        if os.path.exists ( self.test_output + "/ProShade_1/out.map" ):
            testPassed                        = True
        assert testPassed == True
        
    def test_proshade_rebox_parameters ( self ):
        '''
        Test ProShade parameters re-boxing detection via GUI.
        '''
        ccpem_utils.print_header              ( message = 'Unit test - ProShade Re-boxing Parameters' )
        
        # Unit test args contain relative paths, must change to this directory
        args_path = os.path.join ( self.test_data, 'unittest_args_ReB2.json' )
        
        run_task = proshade_task.ProShade (
            job_location                      = self.test_output,
            args_json                         = args_path )

        tail = run_task_and_get_tail(run_task, tail_maxlen=10)
        
        # Check if we can find the correct D7-2 and assert it.
        testPassed                            = False
        if os.path.exists ( self.test_output + "/ProShade_1/out2.map" ):
            testPassed                        = True
        assert testPassed == True


if __name__ == '__main__':
    unittest.main()
