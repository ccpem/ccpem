#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Task window for ProShade.
'''
import os
import sys
import traceback

from PyQt4 import QtGui, QtCore, QtWebKit

from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_core.tasks.proshade import proshade_task
from ccpem_gui.utils import gui_process
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import proshade as test_data


class ProShadeTextInput ( window_utils.CCPEMArgBaseWidget ):
    def __init__ ( self,
                   parent,
                   arg_name,
                   args,
                   label                      = None,
                   label_width                = 150,
                   tooltip_text               = None,
                   required                   = False ):
        super ( ProShadeTextInput, self ).__init__ (
            parent                            = parent,
            arg_name                          = arg_name,
            args                              = args,
            label                             = label,
            label_width                       = label_width,
            tooltip_text                      = tooltip_text,
            required                          = required )
        
        # Keep reference to task window
        self.task_window                      = self.parent ( )
        
        # Set value display
        self.value_line                       = QtGui.QLineEdit()
        self.value_line.textChanged.connect   ( self.textchanged )
        self.value_line.editingFinished.connect ( self.textchanged )
        self.grid_layout.addWidget            ( self.value_line, 0, 1 )
        
        # Set value line
        self.value_line.editingFinished.connect ( self.on_edit_finished )
        if hasattr ( self.task_window, 'handle_title_set' ):
            self.value_line.editingFinished.connect ( self.task_window.handle_title_set )
        self.set_arg_value ( value = self.action.value )
    
    def textchanged ( self ):
        self.set_arg_value ( value = self.value_line.text ( ) )
    
    def disable ( self ):
        self.value_line.setEnabled            ( False )


class ProShadeStructure ( object ):
    def __init__ ( self, str_path = None ):
        self.str_path                         = None
        self.set_str_path                     ( str_path )
    
    def set_str_path ( self, str_path ):
        if str_path is not None:
            self.str_path                     = str ( str_path )

    def validate(self):
        if self.str_path.count(None) == 0:
            return True
        else:
            return False

class ProShadeStructureInput ( QtGui.QWidget ):
    def __init__(self,
                 str_path = None ):
        super ( ProShadeStructureInput, self ).__init__ ( )
        self.strData                          = ProShadeStructure()
        
        # Initialisation
        button_width                          = 100
        label_width                           = 150
        self.layout                           = QtGui.QVBoxLayout()
        self.setLayout                        ( self.layout )
        str_box                               = QtGui.QHBoxLayout()
        
        # Label
        self.str_label                        = QtGui.QLabel('Input structure')
        self.str_label.setFixedWidth          ( label_width )
        str_box.addWidget                     ( self.str_label )
        
        # Select button
        str_select                            = QtGui.QPushButton ( 'Select' )
        str_select.setFixedWidth              ( button_width )
        str_select.clicked.connect            ( self.get_str_file )
        str_box.addWidget                     ( str_select )
        
        # Remove button
        remove_button                         = QtGui.QPushButton ( 'Remove' )
        remove_button.setFixedWidth           ( button_width )
        remove_button.setToolTip              ( 'Remove structure input' )
        remove_button.clicked.connect         ( self.remove )
        str_box.addWidget                     ( remove_button )
        str_box.setAlignment                  ( str_box, QtCore.Qt.AlignRight)
        self.layout.addLayout                 ( str_box )
        
        # Path viewer
        self.str_value                        = QtGui.QLineEdit ( )
        self.str_value.editingFinished.connect (self.edit_finished_str)
        self.str_value.setSizePolicy          ( QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding )
        str_box.addWidget                     ( self.str_value, stretch = True )
        str_box.addStretch                    ( 0 )
        
        # Set initial value
        self.set_str_file                     ( path = str_path )
    
    def get_str_file ( self ):
        path = self.get_file_from_brower      ( )
        self.set_str_file                     ( path = path )
    
    def edit_finished_str (self):
        path                                  = self.str_value.text ( )
        self.set_str_file                     ( path = path )
        self.set_shading                      ( self.str_label, shade = False )
        self.set_shading                      ( self.str_value, shade = False )
    
    def set_str_file ( self, path ):
        self.set_shading ( self.str_label, shade = True )
        self.set_shading ( self.str_value, shade = True )
        self.strData.str_path                 = None
        if path is not None:
            if os.path.exists(path):
                self.str_value.setText        ( path )
                self.set_shading ( self.str_label, shade = False )
                self.set_shading ( self.str_value, shade = False )
                self.strData.set_str_path     ( path )

    def get_file_from_brower ( self ):
        dialog_path                           = window_utils.get_last_directory_browsed ( )
        path = QtGui.QFileDialog.getOpenFileName ( self,
                                                   'Open a File',
                                                   QtCore.QDir.path ( QtCore.QDir ( dialog_path ) ),
                                                   ccpem_file_types.all_ext )
        path                                  = str ( path )
        if path == '':
            path                              = None
        if path is not None:
            dialog_path                       = os.path.dirname ( path )
            window_utils.set_last_directory_browsed ( path = dialog_path )
        return path

    def set_shading ( self, widget, shade = True ):
        '''
        Shade text QLineEdit red to display warning.
        '''
        if shade:
            widget.setStyleSheet ( 'color: red' )
        else:
            widget.setStyleSheet ( 'color: None' )

    def remove ( self ):
        self.deleteLater ( )

class ProShadeStructuresInput ( QtGui.QWidget ):
    def __init__ ( self, strs_arg = None, parent = None ):
        super ( ProShadeStructuresInput, self ).__init__ ( )
        
        # Initalisation
        self.parent                           = parent
        width                                 = 100
        
        # Layout
        self.layout                           = QtGui.QVBoxLayout ( )
        self.setLayout                        ( self.layout )
        
        # Label
        ds_label                              = QtGui.QLabel ( 'Input Structures (maps and PDBs accepted)' )
        self.layout.addWidget                 ( ds_label )
        
        # Add dataset
        add_button                            = QtGui.QPushButton ( 'Add' )
        add_button.setFixedWidth              ( width )
        add_button.setToolTip                 ( 'Add another structure' )
        add_button.clicked.connect            ( self.handle_add_button )
        self.layout.addWidget                 ( add_button )
        
        # Set args
        strs                                  = strs_arg ( )
        
        # Add back dataset
        if isinstance ( strs, list ):
            for n in xrange ( len ( strs ) ):
                self.add_structure ( str_path = strs[n] )
        else:
            self.add_structure ( str_path = strs )

    def handle_add_button ( self ):
        '''
        Handle dataset button; stop bool signal transfer
        '''
        self.add_structure ( )

    def add_structure ( self, str_path = None ):
        strData = ProShadeStructureInput      ( str_path = str_path )
        self.layout.addWidget                 ( strData, stretch = True )

    def validate_structures ( self ):
        children = self.findChildren ( ProShadeStructureInput )
        # Check at least one dataset
        if len(children) == 0:
            # Warn no datasets provided
            text = 'No input structures provided'
            QtGui.QMessageBox.warning ( self, 'Error', text )
            return False
        warning = False
        # Check datasets are complete (must have pdb, map, resolution)
        for child in children:
            if not child.strData.validate ( ):
                warning = True
        if warning:
            # Warn datasets incomplete information
            text = 'Structure(s) incomplete (added a structure, but not supplied the file)'
            QtGui.QMessageBox.warning ( self, 'Error', text )
            return False
        else:
            return True

    def set_args ( self, strs_arg ):
        children = self.findChildren ( ProShadeStructureInput )
        strs = []
        if len(children) > 1:
            for child in children:
                strs.append                   ( child.strData.str_path )
            strs_arg.value                    = strs
        else:
            child                             = children[0]
            strs_arg.value                    = child.strData.str_path

class ProShadeWindow ( window_utils.CCPEMTaskWindow ):
    '''
    ProShade window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args_Sym1.json')

    def __init__(self,
                 task,
                 parent=None):
        super ( ProShadeWindow, self ).__init__ ( task   = task,
                                                  parent = parent )
                                                  
        # Initialisation of the interface
        self.structures_input_symm.show       ( )
        self.requested_symmetry.show          ( )
        self.structures_input_dists.hide      ( )
        self.structures_input_overlayS.hide   ( )
        self.structures_input_overlayM.hide   ( )
        self.structures_input_reBox.hide      ( )
        self.resolution_input.setDisabled ( False )
        self.bFactorChange_input.setDisabled ( False )
        self.sharpenFactor_input.setDisabled ( False )
        self.extraSpace_input.hide            ( )
        self.symm_path                        = "";
        

    def set_args(self):
        '''
        Set input arguments
        '''
        
        # Job title
        self.title_input = window_utils.TitleArgInput (
            parent                            = self,
            arg_name                          = 'job_title',
            args                              = self.args )
        self.args_widget.args_layout.addWidget ( self.title_input )
        self.title_input.value_line.editingFinished.connect( self.handle_title_set )

        # Functionality mode
        self.functionality_mode = window_utils.ChoiceArgInput (
            parent                            = self,
            arg_name                          = 'mode',
            second_width                      = None,
            args                              = self.args )
        self.args_widget.args_layout.addWidget ( self.functionality_mode )
        self.functionality_mode.value_line.currentIndexChanged.connect ( self.set_mode )

        self.structures_input_symm = window_utils.FileArgInput (
            parent                            = self,
            arg_name                          = 'sym_str',
            required                          = True,
            file_types                        = ccpem_file_types.all_ext,
            args                              = self.args )
        self.args_widget.args_layout.addWidget ( self.structures_input_symm )
        
        # Requested symmetry to detect
        self.requested_symmetry = ProShadeTextInput  (
            parent                            = self,
            arg_name                          = 'req_sym',
            args                              = self.args )
        self.args_widget.args_layout.addWidget ( self.requested_symmetry )

        # Input structures for overlay
        self.structures_input_overlayS = window_utils.FileArgInput (
            parent                            = self,
            arg_name                          = 'static_str',
            required                          = True,
            file_types                        = ccpem_file_types.all_ext,
            args                              = self.args )
        self.args_widget.args_layout.addWidget ( self.structures_input_overlayS )

        self.structures_input_overlayM = window_utils.FileArgInput (
            parent                            = self,
            arg_name                          = 'moving_str',
            required                          = True,
            file_types                        = ccpem_file_types.all_ext,
            args                              = self.args )
        self.args_widget.args_layout.addWidget ( self.structures_input_overlayM )

        # Input structures for re-boxing
        self.structures_input_reBox = window_utils.FileArgInput (
            parent                            = self,
            arg_name                          = 'rebox_str',
            required                          = True,
            file_types                        = ccpem_file_types.mrc_ext,
            args                              = self.args )
        self.args_widget.args_layout.addWidget ( self.structures_input_reBox )

        # Input structures for distances
        self.structures_input_dists = ProShadeStructuresInput (
            strs_arg                          = self.args.out_file,
            parent                            = self )
        self.args_widget.args_layout.insertWidget ( 3, self.structures_input_dists )
    
        # Output file frame
        self.output_files_frame = window_utils.CCPEMExtensionFrame (
            button_name                       = 'Output files',
            button_tooltip                    = 'Specify output file names for the selected functionality' )
        self.args_widget.args_layout.addLayout ( self.output_files_frame )

        # -> Output file entry
        self.output_name = ProShadeTextInput  (
            parent                            = self,
            arg_name                          = 'out_file',
            args                              = self.args,
            label_width                       = 200 )
        self.output_files_frame.add_extension_widget ( self.output_name )
    
        # Settings frame
        self.settings_frame = window_utils.CCPEMExtensionFrame (
            button_name                       = 'Settings',
            button_tooltip                    = 'Override default settings values' )
        self.args_widget.args_layout.addLayout ( self.settings_frame )
    
        # -> Resolution input
        self.resolution_input = window_utils.NumberArgInput (
            parent                            = self,
            arg_name                          = 'resolution',
            args                              = self.args,
            required                          = False,
            decimals                          = 2,
            step                              = 0.1,
            minimum                           = 0,
            maximum                           = 999,
            label_width                       = 200 )
        self.settings_frame.add_extension_widget ( self.resolution_input )
    
        # -> B-factor change input
        self.bFactorChange_input = window_utils.NumberArgInput (
            parent                            = self,
            arg_name                          = 'bFactor',
            args                              = self.args,
            required                          = False,
            decimals                          = 1,
            step                              = 1.0,
            minimum                           = -999,
            maximum                           = 999,
            label_width                       = 200 )
        self.settings_frame.add_extension_widget ( self.bFactorChange_input )
    
        # -> Sharpening factor input
        self.sharpenFactor_input = window_utils.NumberArgInput (
            parent                            = self,
            arg_name                          = 'sharpen',
            args                              = self.args,
            required                          = False,
            decimals                          = 1,
            step                              = 1.0,
            minimum                           = -999,
            maximum                           = 999,
            label_width                       = 200 )
        self.settings_frame.add_extension_widget ( self.sharpenFactor_input )
        
        # -> Extra space input
        self.extraSpace_input = window_utils.NumberArgInput (
            parent                            = self,
            arg_name                          = 'extraSpace',
            args                              = self.args,
            required                          = False,
            decimals                          = 1,
            step                              = 1.0,
            minimum                           = 0,
            maximum                           = 999,
            label_width                       = 200 )
        self.settings_frame.add_extension_widget ( self.extraSpace_input )
    
        # Advanced options frame
        self.advanced_options_frame = window_utils.CCPEMExtensionFrame (
            button_name                       = 'Advanced Options',
            button_tooltip                    = 'Allows specification of advanced parameters' )
        self.args_widget.args_layout.addLayout ( self.advanced_options_frame )
        
        # -> Bandwidt limit input
        self.bandwidthLimit_input = window_utils.NumberArgInput (
            parent                            = self,
            arg_name                          = 'bandwidth',
            args                              = self.args,
            required                          = False,
            step                              = 1,
            minimum                           = 0,
            maximum                           = 9999,
            label_width                       = max(300,len(self.task.args.bandwidth.metavar)*7 ))
        self.advanced_options_frame.add_extension_widget ( self.bandwidthLimit_input )
    
        # -> Integration order input
        self.integOrder_input = window_utils.NumberArgInput (
            parent                            = self,
            arg_name                          = 'integOrder',
            args                              = self.args,
            required                          = False,
            step                              = 1,
            minimum                           = 0,
            maximum                           = 9999,
            label_width                       = max(300,len(self.task.args.integOrder.metavar)*7 ))
        self.advanced_options_frame.add_extension_widget ( self.integOrder_input )
        
        # -> Distances between spheres input
        self.sphDistance_input = window_utils.NumberArgInput (
            parent                            = self,
            arg_name                          = 'sphDistance',
            args                              = self.args,
            required                          = False,
            decimals                          = 1,
            step                              = 0.5,
            minimum                           = 0,
            maximum                           = 999,
            label_width                       = max(300,len(self.task.args.sphDistance.metavar)*7 ))
        self.advanced_options_frame.add_extension_widget ( self.sphDistance_input )
        
        # -> Number of speres input
        self.sphNumber_input = window_utils.NumberArgInput (
            parent                            = self,
            arg_name                          = 'sphNumber',
            args                              = self.args,
            required                          = False,
            step                              = 1,
            minimum                           = 0,
            maximum                           = 9999,
            label_width                       = max(300,len(self.task.args.sphNumber.metavar)*7 ))
        self.advanced_options_frame.add_extension_widget ( self.sphNumber_input )
        
        # -> Number of interquartile ranges from median for map masking threshohld
        self.mapMaskThres_input = window_utils.NumberArgInput (
            parent                            = self,
            arg_name                          = 'mapMaskThres',
            args                              = self.args,
            required                          = False,
            decimals                          = 2,
            step                              = 0.25,
            minimum                           = 0,
            maximum                           = 999,
            label_width                       = max(300,len(self.task.args.mapMaskThres.metavar)*7 ))
        self.advanced_options_frame.add_extension_widget ( self.mapMaskThres_input )

        # -> Blurring factor for map masking
        self.mapMaskSharpen_input = window_utils.NumberArgInput (
            parent                            = self,
            arg_name                          = 'mapMaskBlur',
            args                              = self.args,
            required                          = False,
            decimals                          = 2,
            step                              = 1.0,
            minimum                           = 0,
            maximum                           = 999,
            label_width                       = max(300,len(self.task.args.mapMaskBlur.metavar)*7 ))
        self.advanced_options_frame.add_extension_widget ( self.mapMaskSharpen_input )

        # -> Number of points forming each single peak
        self.pointsPerPeak_input = window_utils.NumberArgInput (
            parent                            = self,
            arg_name                          = 'symPeakSize',
            args                              = self.args,
            required                          = False,
            step                              = 1,
            minimum                           = 0,
            maximum                           = 20,
            label_width                       = max(300,len(self.task.args.symPeakSize.metavar)*7 ))
        self.advanced_options_frame.add_extension_widget ( self.pointsPerPeak_input )

        # -> Number of interquartile ranges from median to decide peak is real
        self.iqrsPerPeak_input = window_utils.NumberArgInput (
            parent                            = self,
            arg_name                          = 'symPeakThres',
            args                              = self.args,
            required                          = False,
            decimals                          = 2,
            step                              = 0.25,
            minimum                           = 0,
            maximum                           = 999,
            label_width                       = max(300,len(self.task.args.symPeakThres.metavar)*7 ))
        self.advanced_options_frame.add_extension_widget ( self.iqrsPerPeak_input )

        # -> Tolerance value on matching rotation axes of different peaks
        self.peakAxisTolerance_input = window_utils.NumberArgInput (
            parent                            = self,
            arg_name                          = 'axisTolerance',
            args                              = self.args,
            required                          = False,
            decimals                          = 3,
            step                              = 0.025,
            minimum                           = 0.0,
            maximum                           = 1.0,
            label_width                       = max(300,len(self.task.args.symPeakThres.metavar)*7 ) )
        self.advanced_options_frame.add_extension_widget ( self.peakAxisTolerance_input )

    def set_mode ( self ):
        if self.args.mode() == 'Symmetry detection':
            self.structures_input_dists.hide  ( )
            self.structures_input_symm.show   ( )
            self.requested_symmetry.show      ( )
            self.structures_input_overlayS.hide ( )
            self.structures_input_overlayM.hide ( )
            self.structures_input_reBox.hide  ( )
            self.output_files_frame.button.setDisabled ( False )
            self.output_name.show             ( )
            self.resolution_input.show        ( )
            self.bFactorChange_input.show     ( )
            self.sharpenFactor_input.show     ( )
            self.extraSpace_input.hide        ( )
            self.bandwidthLimit_input.show    ( )
            self.integOrder_input.show        ( )
            self.sphDistance_input.show       ( )
            self.sphNumber_input.show         ( )
            self.mapMaskThres_input.show      ( )
            self.mapMaskSharpen_input.show    ( )
            self.pointsPerPeak_input.show     ( )
            self.iqrsPerPeak_input.show       ( )
            self.peakAxisTolerance_input.show ( )
        elif self.args.mode() == 'Structure overlay':
            self.structures_input_dists.hide  ( )
            self.structures_input_symm.hide   ( )
            self.requested_symmetry.hide      ( )
            self.structures_input_overlayS.show ( )
            self.structures_input_overlayM.show ( )
            self.structures_input_reBox.hide  ( )
            self.output_files_frame.button.setDisabled ( False )
            self.output_name.show             ( )
            self.resolution_input.show        ( )
            self.bFactorChange_input.show     ( )
            self.sharpenFactor_input.show     ( )
            self.extraSpace_input.show        ( )
            self.bandwidthLimit_input.show    ( )
            self.integOrder_input.show        ( )
            self.sphDistance_input.show       ( )
            self.sphNumber_input.show         ( )
            self.mapMaskThres_input.hide      ( )
            self.mapMaskSharpen_input.hide    ( )
            self.pointsPerPeak_input.show     ( )
            self.iqrsPerPeak_input.show       ( )
            self.peakAxisTolerance_input.hide ( )
        elif self.args.mode() == 'Map re-boxing':
            self.structures_input_dists.hide  ( )
            self.structures_input_symm.hide   ( )
            self.requested_symmetry.hide      ( )
            self.structures_input_overlayS.hide ( )
            self.structures_input_overlayM.hide ( )
            self.structures_input_reBox.show  ( )
            self.output_files_frame.button.setDisabled ( False )
            self.output_name.show             ( )
            self.resolution_input.hide        ( )
            self.bFactorChange_input.hide     ( )
            self.sharpenFactor_input.hide     ( )
            self.extraSpace_input.show        ( )
            self.bandwidthLimit_input.hide    ( )
            self.integOrder_input.hide        ( )
            self.sphDistance_input.hide       ( )
            self.sphNumber_input.hide         ( )
            self.mapMaskThres_input.show      ( )
            self.mapMaskSharpen_input.show    ( )
            self.pointsPerPeak_input.hide     ( )
            self.iqrsPerPeak_input.hide       ( )
            self.peakAxisTolerance_input.hide ( )
        elif self.args.mode() == 'Distance computation':
            self.structures_input_dists.show  ( )
            self.structures_input_symm.hide   ( )
            self.requested_symmetry.hide      ( )
            self.structures_input_overlayS.hide( )
            self.structures_input_overlayM.hide( )
            self.structures_input_reBox.hide  ( )
            self.output_files_frame.button.setDisabled ( True )
            self.output_name.hide             ( )
            self.resolution_input.show        ( )
            self.bFactorChange_input.show     ( )
            self.sharpenFactor_input.show     ( )
            self.extraSpace_input.show        ( )
            self.bandwidthLimit_input.show    ( )
            self.integOrder_input.show        ( )
            self.sphDistance_input.show       ( )
            self.sphNumber_input.show         ( )
            self.mapMaskThres_input.show      ( )
            self.mapMaskSharpen_input.show    ( )
            self.pointsPerPeak_input.show     ( )
            self.iqrsPerPeak_input.show       ( )
            self.peakAxisTolerance_input.hide ( )

    def validate_input(self):
        '''
        Override from baseclass to handle non-automatically updated values
        '''
        # Assign value for the non-automatically updated values
        self.structures_input_dists.set_args ( strs_arg = self.args.input_str )
        # TODO: should call super().validate_input() here to check other inputs
        #  but currently this causes some running modes to fail to launch even
        #  when the visible inputs are all correct
        return True

    def run_coot_custom ( self ):
        # Initialise
        args                                  = list ( )
        path                                  = ''
        
        # Proceed based on functionality type
        if self.args.mode() == 'Distance computation':
            print ( "!!! Warning !!! ProShade does not provide coot visualisation for the distances functionality." )
        
        if self.args.mode() == 'Symmetry detection':
            # Initialise
            path                              = os.path.join ( self.task.job_location, 'ps_coot.py' )
            f                                 = open ( path, 'w' )
            
            # Read the output into coot
            command                           = 'imol = read_pdb ( "{0}" )\n'.format( self.task.args.sym_str ( ) )
            f.write                           ( command )
            command                           = 'if is_valid_model_molecule ( imol ):\n'
            f.write                           ( command )
            command                           = '    pass\n'
            f.write                           ( command )
            command                           = 'else:\n'
            f.write                           ( command )
            command                           = '    imol = handle_read_ccp4_map ( "{0}", False )\n'.format( self.task.args.sym_str ( ) )
            f.write                           ( command )

            # Draw and done
            command                           = 'graphics_draw()\n'
            f.write                           ( command )
            f.close                           ( )
        if self.args.mode() == 'Map re-boxing':
            # Initialise
            path                              = os.path.join ( self.task.job_location, 'ps_coot.py' )
            f                                 = open ( path, 'w' )
            
            # Read the output into coot
            command                           = 'map_mol = handle_read_ccp4_map ( "{0}", False )\n'.format( self.task.args.rebox_str ( ) )
            f.write                           ( command )
            
            if self.task.args.out_file ( ) is not None:
                command                       = 'map_mol2 = handle_read_ccp4_map ( "{0}", False )\n'.format( os.path.join ( self.task.job_location, self.task.args.out_file ( ) ) )
                f.write                       ( command )
            else:
                command                       = 'map_mol2 = handle_read_ccp4_map ( "{0}", False )\n'.format( os.path.join ( self.task.job_location, 'proshade_reboxed.map' ) )
                f.write                       ( command )

            # Draw and done
            command                           = 'graphics_draw()\n'
            f.write                           ( command )
            f.close                           ( )
        if self.args.mode() == 'Structure overlay':
            # Initialise
            path                              = os.path.join ( self.task.job_location, 'ps_coot.py' )
            f                                 = open ( path, 'w' )

            # Read the output into coot
            command                           = 'imol = read_pdb ( "{0}" )\n'.format( self.task.args.static_str ( ) )
            f.write                           ( command )
            command                           = 'if is_valid_model_molecule ( imol ):\n'
            f.write                           ( command )
            command                           = '    pass\n'
            f.write                           ( command )
            command                           = 'else:\n'
            f.write                           ( command )
            command                           = '    imol = handle_read_ccp4_map ( "{0}", False )\n'.format( self.task.args.static_str ( ) )
            f.write                           ( command )

            command                           = 'imol = read_pdb ( "{0}" )\n'.format( self.task.args.moving_str ( ) )
            f.write                           ( command )
            command                           = 'if is_valid_model_molecule ( imol ):\n'
            f.write                           ( command )
            command                           = '    pass\n'
            f.write                           ( command )
            command                           = 'else:\n'
            f.write                           ( command )
            command                           = '    imol = handle_read_ccp4_map ( "{0}", False )\n'.format( self.task.args.moving_str ( ) )
            f.write                           ( command )

            if self.task.args.out_file ( ) is not None:
                command                       = 'imol = read_pdb ( "{0}" )\n'.format( os.path.join ( self.task.job_location, self.task.args.out_file ( ) ) )
                f.write                       ( command )
                command                       = 'if is_valid_model_molecule ( imol ):\n'
                f.write                       ( command )
                command                       = '    pass\n'
                f.write                       ( command )
                command                       = 'else:\n'
                f.write                       ( command )
                command                       = '    imol = handle_read_ccp4_map ( "{0}", False )\n'.format( os.path.join ( self.task.job_location, self.task.args.out_file ( ) ) )
                f.write                       ( command )
            else:
                command                       = 'map_mol3 = handle_read_ccp4_map ( "{0}", False )\n'.format( os.path.join ( self.task.job_location, 'rotStr.map' ) )
                f.write                       ( command )


            command                           = 'graphics_draw()\n'
            f.write                           ( command )
            f.close                           ( )

        args.append                           ( '-c' )
        args.append                           ( path )
        gui_process.run_coot                  ( args = args )

    def set_on_job_running_custom ( self ):
        if hasattr ( self, 'tb_chimera_button' ):
            self.tb_chimera_button.setEnabled ( False )
        if hasattr ( self, 'tb_coot_button' ):
            self.tb_coot_button.setEnabled    ( False )
        self.show_rv_ui                        ()

    def set_on_job_finish_custom(self):
        if hasattr(self, 'tb_chimera_button'):
            self.tb_chimera_button.setEnabled ( True )
        if hasattr ( self, 'tb_coot_button' ):
            self.tb_coot_button.setEnabled    ( True )
        self.show_rv_ui                        ()

    def run_chimera_custom ( self ):
        # Initialise
        args                                  = list ( )
    
        # Proceed based on functionality type
        if self.args.mode() == 'Distance computation':
            print ( "!!! Warning !!! ProShade does not provide Chimera visualisation for the distances functionality." )
        
        if self.args.mode() == 'Symmetry detection':
            args.append                       ( self.task.args.sym_str ( ) )
            if self.task.args.out_file ( ) is not None:
                args.append                   ( os.path.join ( self.task.job_location, self.task.args.out_file ( ) ) )

        if self.args.mode() == 'Map re-boxing':
            args.append                       ( self.task.args.rebox_str ( ) )
            if self.task.args.out_file ( ) is not None:
                args.append                   ( os.path.join ( self.task.job_location, self.task.args.out_file ( ) ) )
            else:
                args.append                   ( os.path.join ( self.task.job_location, 'proshade_reboxed.map' ) )

        if self.args.mode() == 'Structure overlay':
            args.append                       ( self.task.args.static_str ( ) )
            args.append                       ( self.task.args.moving_str ( ) )
            if self.task.args.out_file ( ) is not None:
                args.append                   ( os.path.join ( self.task.job_location, self.task.args.out_file ( ) ) )
            else:
                args.append                   ( os.path.join ( self.task.job_location, 'rotStr.map' ) )

        # Run
        gui_process.run_chimera ( args = args )

    def show_rv_ui(self):
        '''
        Show the RV API results viewer (and create it first if necessary)

        If the report does not exist and the job is running, this method schedules itself to run again until the report
        exists.
        '''
        if not hasattr ( self, 'results_dock' ):
            rv_index                              = os.path.join ( self.task.job_location, 'proshade_report/index.html' )
            if os.path.exists ( rv_index ):
                # Report exists. Create and raise the results view.
                self.rv_view                      = QtWebKit.QWebView ( )
                assert os.path.exists             ( rv_index )
                self.results_dock                 = QtGui.QDockWidget ( 'Results',
                                                                        self,
                                                                        QtCore.Qt.Widget )
                self.results_dock.setToolTip          ( 'ProSHADE Results overview' )
                self.tabifyDockWidget                 ( self.setup_dock, self.results_dock )
                self.rv_view.load                     ( QtCore.QUrl ( rv_index ) )
                self.results_dock.setWidget           ( self.rv_view )
                self.results_dock.show                ( )
                self.results_dock.raise_              ( )
            elif self.status == 'running':
                # Job is running but report does not exist yet. Schedule a repeat call of this method in 0.2 secs.
                QtCore.QTimer.singleShot(200, self.show_rv_ui)
        else:
            # Results view exists. (This method is probably being called again after job has finished).
            # Raise the results view again.
            self.results_dock.raise_                  ( )

def main():
    '''
    Launch standalone task runner.
    '''
    window_utils.standalone_window_launch ( task   = proshade_task.ProShade,
                                            window = ProShadeWindow )

if __name__ == '__main__':
    main()
