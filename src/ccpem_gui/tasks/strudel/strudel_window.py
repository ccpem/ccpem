#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
'''
Task window for Strudel Score.
'''
import os

from PyQt4 import QtGui, QtCore, QtWebKit

from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_core.tasks.strudel import strudel_task
from ccpem_gui.utils import command_line_launch
from ccpem_core import chimera_scripts
from ccpem_core.settings import which
from ccpem_core.process_manager import job_register
from ccpem_gui.project_database import sqlite_project_database
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import strudel as test_data

class StrudelWindow(window_utils.CCPEMTaskWindow):
    '''
    Strudel window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(StrudelWindow, self).__init__(task=task,
                                             parent=parent)

    def set_args(self):
        '''
        Set input arguments
        '''

        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Input map
        self.map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='map_path',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_input)
        
        
        # Input map or PDB
        self.library_selection = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='library_selection',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.library_selection)
    
        # Input pdb
        self.pdb_input = window_utils.FileArgInput(
            parent=self,
            arg_name='pdb_path',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.pdb_input)
        
        # N cpu
        self.ncpu = window_utils.NumberArgInput(
            parent=self,
            arg_name='ncpu',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.ncpu)
        
    
    def handle_chimera_view(self):
        output_dir = os.path.join(self.task.job_location,'strudel_out')
        button = self.sender()
        chimerax_bin = which('ChimeraX')
        chimerax_command = 'strudel open {}' .format(
            str(output_dir))
        
        arg_list_chimera = ['--cmd', '%s' % (chimerax_command)]
        process = QtCore.QProcess()
        #print arg_list_chimera
        if chimerax_bin is None:
            print 'ChimeraX executable not found (add path to the executable to system PATH)'
        else: 
            process.startDetached(chimerax_bin, arg_list_chimera)
            
    def set_chimera_button(self):
        #view in chimera
        chimera_button_widget = QtGui.QWidget()
        chimera_button_layout = QtGui.QHBoxLayout()
        chimera_button_widget.setLayout(chimera_button_layout)
        chimera_view_button = QtGui.QPushButton('View results in ChimeraX')
        if which('ChimeraX') is not None:
            chimera_view_button.clicked.connect(self.handle_chimera_view)
        else: chimera_view_button.setEnabled(False)
        chimera_button_layout.addWidget(chimera_view_button)
        return chimera_button_widget

    def set_rv_ui(self):
        '''
        RVAPI results viewer.
        '''
        # Create results if not already done
        rv_index = os.path.join(
            self.task.job_location,
            'rvapi_data/index.html')
        
        self.rv_view = QtWebKit.QWebView()
        rvapi_layout = QtGui.QHBoxLayout()
        if os.path.exists(rv_index):
            self.rv_view.load(QtCore.QUrl(rv_index))
        results_dock_layout = QtGui.QVBoxLayout()
        results_dock_widget = QtGui.QWidget()
        results_dock_widget.setLayout(results_dock_layout)
        self.results_dock = QtGui.QDockWidget('Results',
                                               self,
                                               QtCore.Qt.Widget)
        
        self.results_dock.setToolTip('Results overview')
        self.results_dock.setWidget(results_dock_widget)#self.rv_view)
        self.tabifyDockWidget(self.setup_dock, self.results_dock)
        chimera_view_button = self.set_chimera_button()
        #self.rv_view.setMinimumHeight(700)
        self.rv_view.setLayout(rvapi_layout)
        results_dock_layout.addWidget(chimera_view_button)
        results_dock_layout.addWidget(self.rv_view)
        results_dock_layout.addStretch(1)
        self.results_dock.raise_()

#     def validate_input(self):
#         '''
#         Override from baseclass to allow validation of multiple datasets
#         '''
#         ready = super(SMOCMapWindow, self).validate_input()
#         if ready:
#             # Validate inputs for multiple datasets
#             if not self.input_pdbs.validate_datasets():
#                 ready = False
#             else:
#                 self.input_pdbs.set_args()
#         return ready
    
    def set_on_job_running_custom(self):
        self.pipeline_view.job_widget.error_display.hide()
    def set_on_job_finish_custom(self):
        # Add pdbs to launcher
        pdb = self.task.args.pdb_path.value
        self.launcher.add_file(
                path=pdb,
                file_type='pdb',
                description='Input pdb',
                selected=True)
        # Add map to launcher
        self.launcher.add_file(
            arg_name='map_path',
            file_type='map',
            description=self.args.map_path.help,
            selected=True)
        # Add raw data
        self.rawdata_path = os.path.join(self.task.job_location,'strudel_out',
                                         'vs_motifs_'+self.args.library_selection.value,
                                         'scores_top.csv')
        #print self.rawdata_path
        
        if os.path.exists(self.rawdata_path):
            self.launcher.add_file(
                description='Raw scores (CSV text)',
                path=self.rawdata_path)
        
        self.pipeline_view.job_widget.error_display.hide()
        self.launcher.set_tree_view()
        self.launcher_dock.raise_()
        self.launcher_dock.show()
        self.set_rv_ui()

def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=strudel_task.Strudel,
        window_class=StrudelWindow)

if __name__ == '__main__':
    main()
