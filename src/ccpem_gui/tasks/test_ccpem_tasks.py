#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import time

from ccpem_gui.tasks import ccpem_tasks
from ccpem_core.tasks.molrep import molrep_task
from ccpem_core.tasks.atomic_model_validation import validate_task


class Test(unittest.TestCase):
    '''
    Unit test for ccpem tasks.
    '''
    
    def test_time_to_load_all_tasks(self):
        # Get task and assert returns correct class
        time_s = time.time()
        tasks = ccpem_tasks.CCPEMTasks(alpha=True)

        # Test task initialisation
        time_t = time.time() - time_s
        self.assertLess(time_t,
            5.0,
            'CCP-EM tasks initialisation too slow, check imports')

        # Check an example task to make sure they were loaded properly
        task, window = tasks.get_task_and_window_classes('Molrep')
        assert task == molrep_task.MolRep

    def test_task_errors(self):
        # Check only expected errors in task loading
        tasks = ccpem_tasks.CCPEMTasks(alpha=True)

        # Check for known errors if modeller is missing
        try:
            import modeller
        except ImportError:
            choyce_err = tasks.errors.pop('Choyce')
            assert "modeller" in str(choyce_err)
            flexem_err = tasks.errors.pop('Flex-EM')
            assert "modeller" in str(flexem_err)

        # Check for known errors if tensorflow is missing
        try:
            import tensorflow
        except ImportError:
            haruspex_err = tasks.errors.pop('Haruspex')
            assert "tensorflow" in str(haruspex_err)

        # Any remaining errors indicate an unexpected problem
        if tasks.errors:
            raise AssertionError(tasks.errors)

    def test_loading_validation_task_with_exact_name(self):
        tasks = ccpem_tasks.CCPEMTasks()
        task, window = tasks.get_task_and_window_classes('Model validation')
        assert task == validate_task.ValidateTask

    def test_loading_validation_task_with_fuzzy_name(self):
        tasks = ccpem_tasks.CCPEMTasks()
        task, window = tasks.get_task_and_window_classes('Model_validation', fuzzy=True)
        assert task == validate_task.ValidateTask


if __name__ == '__main__':
    unittest.main()
