#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Test buccaneer task
'''

import unittest
import os
import sys
import shutil
import time
import tempfile
from PyQt4 import QtGui, QtCore
from PyQt4.QtTest import QTest
from ccpem_core.tasks.buccaneer import buccaneer_task
from ccpem_gui.tasks.buccaneer import buccaneer_window
from ccpem_core.test_data.tasks import buccaneer as buccaneer_test
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from lxml import etree, html

app = QtGui.QApplication(sys.argv)


class BuccaneerTest(unittest.TestCase):
    '''
    Unit test for Buccaneer (invokes GUI).
    '''

    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_data = os.path.dirname(buccaneer_test.__file__)
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        if os.path.exists(self.test_output):
            shutil.rmtree(self.test_output)

    def test_buccaneer_window_integration(self):
        '''
        Test Buccaneer auto building pipeline via GUI.
        '''
        ccpem_utils.print_header(message='Unit test - Buccaneer')
        # Unit test args contain relative paths, must change to this directory
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        args_path = os.path.join(self.test_data, 'unittest_args.json')
        run_task = buccaneer_task.Buccaneer(
            job_location=self.test_output,
            args_json=args_path)
        print(run_task.args.output_args_as_text())
        # Run w/out gui
#         run_task.run_task()
        # Run w/ gui
        window = buccaneer_window.BuccaneerWindow(task=run_task)
        # Mouse click run
        QTest.mouseClick(
            window.tool_bar.widgetForAction(window.tb_run_button),
            QtCore.Qt.LeftButton)
        # Wait for run to complete
        job_completed = False
        xmlout_found = False
        # Global refine stdout (i.e. last job in pipeline)
        stdout_ref = run_task.pipeline.pipeline[-1][-1].stdout
        stdout_buc = run_task.pipeline.pipeline[-2][-1].stdout
        # Needed this to add the appropriate number to stdout filename
        stdout_suffix = run_task.args.ncycle.value
        assert os.path.basename(
            stdout_ref) == 'refmacrefineglobalviaservalcat' + str(stdout_suffix) + '_stdout.txt'
        assert os.path.basename(
            stdout_buc) == 'buccaneerbuild' + str(stdout_suffix) + '_stdout.txt'

        # Wait for job to finish
        run_time = 0
        timeout = 300
        delay = 5
        while not job_completed and run_time < timeout:
            print('Buccaneer running for {0} secs (timeout = {1})'.format(
                run_time,
                timeout))
            time.sleep(delay)
            run_time += delay
            status =\
                process_manager.get_process_status(run_task.pipeline.json)
            # Check job is finished
            if status == 'finished':
                if os.path.isfile(stdout_buc) and os.path.isfile(stdout_ref):
                    tail = ccpem_utils.tail(stdout_ref, maxlen=10)
                    if tail.find('CCP-EM process finished') != -1:
                        # Check Buccaneer output exists
                        buccaneer_pdb = \
                            run_task.process_buccaneer_pipeline.pdbout
                        if os.path.exists(buccaneer_pdb):
                            job_completed = True
                        # Get result summary
                        result_print = getResultLines(stdout_buc)
                        for line in result_print:
                            print(line)
                        xmlfilename = 'program' + str(stdout_suffix) + '.xml'
                        if os.path.isfile(xmlfilename):
                            xmlout_found = True
                            print('Found {0}'.format(xmlfilename))
                            getXML(xmlfilename)

        # Check timeout
        assert run_time < timeout
        # Check job completed
        assert job_completed
        # Check program.xml produced
        assert xmlout_found, 'xml output not found!'

    def test_buccaneer_windows_integration_maskedref(self):
        '''
        Test Buccaneer auto building pipeline with REFMAC's masked refinement via GUI.
        '''
        ccpem_utils.print_header(
            message='Unit test - Buccaneer (masked refinement on)')
        # Unit test args contain relative paths, must change to this directory
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        args_path = os.path.join(self.test_data, 'unittest_args_masked.json')
        run_task = buccaneer_task.Buccaneer(
            job_location=self.test_output,
            args_json=args_path)
        print(run_task.args.output_args_as_text())
        # Run w/out gui
#         run_task.run_task()
        # Run w/ gui
        window = buccaneer_window.BuccaneerWindow(task=run_task)
        # Mouse click run
        QTest.mouseClick(
            window.tool_bar.widgetForAction(window.tb_run_button),
            QtCore.Qt.LeftButton)
        # Wait for run to complete
        job_completed = False
        xmlout_found = False
        stdout_suffix = run_task.args.ncycle.value
        # Global & local refine stdout (i.e. last job in pipeline)
        if run_task.args.masked_refinement_on:
            stdout_ref = run_task.pipeline.pipeline[-1][-1].stdout
            #stdout_mtmtz = run_task.pipeline.pipeline[-2][-1].stdout
            stdout_buc = run_task.pipeline.pipeline[-2][-1].stdout
            assert os.path.basename(
                stdout_ref) == 'refmacrefinemaskedviaservalcat' + str(stdout_suffix) + '_stdout.txt'
            #assert os.path.basename(stdout_mtmtz) == 'maptomtzmasked' + str(stdout_suffix) + '_stdout.txt'
            assert os.path.basename(
                stdout_buc) == 'buccaneerbuild' + str(stdout_suffix) + '_stdout.txt'
        else:
            stdout_ref = run_task.pipeline.pipeline[-1][-1].stdout
            stdout_buc = run_task.pipeline.pipeline[-2][-1].stdout
            stdin_buc = run_task.pipeline.pipeline[-2][-1].stdin
            assert os.path.basename(
                stdout_ref) == 'refmacrefineglobalviaservalcat' + str(stdout_suffix) + '_stdout.txt'
            assert os.path.basename(
                stdout_buc) == 'buccaneerbuild' + str(stdout_suffix) + '_stdout.txt'

        # Wait for job to finish
        run_time = 0
        timeout = 300
        delay = 5
        while not job_completed and run_time < timeout:
            print('Buccaneer running for {0} secs (timeout = {1})'.format(
                run_time,
                timeout))
            time.sleep(delay)
            run_time += delay
            status =\
                process_manager.get_process_status(run_task.pipeline.json)
            # Check job is finished
            if status == 'finished':
                if os.path.isfile(stdout_buc) and os.path.isfile(stdout_ref):
                    tail = ccpem_utils.tail(stdout_ref, maxlen=10)
                    if tail.find('CCP-EM process finished') != -1:
                        # Check Buccaneer output exists
                        buccaneer_pdb = \
                            run_task.process_buccaneer_pipeline.pdbout
                        refined_pdb = \
                            run_task.process_refine.pdbout_path
                        if os.path.exists(buccaneer_pdb) and \
                           os.path.exists(refined_pdb):
                            job_completed = True
                        # Get result summary
                        result_print = getResultLines(stdout_buc)
                        for line in result_print:
                            print(line)
                        xmlfilename = 'program' + str(stdout_suffix) + '.xml'
                        if os.path.isfile(xmlfilename):
                            xmlout_found = True
                            print('Found {0}'.format(xmlfilename))
                            getXML(xmlfilename)

        # Check timeout
        assert run_time < timeout
        # Check job completed
        assert job_completed
        # Check program.xml produced
        assert xmlout_found, 'xml output not found!'
        # Check if output refined{lastcyc}.pdb is produced
        assert os.path.exists('refined{0}.pdb'.format(stdout_suffix))
        # Check if output build{lastcyc}.pdb is produced
        assert os.path.exists('build{0}.pdb'.format(stdout_suffix))
        # Read RVAPI table to check end result
        filename = os.path.join(
            self.test_output,
            'Buccaneer_1/report/refine_summary_table.0_0.1_1.table')
        with open(filename, 'r') as f:
            page = f.read()
        tree = html.fromstring(page)
        tr_elements = tree.xpath('//tr')
        assert tr_elements[0][1].text_content() == 'FSC average'
        assert tr_elements[1][2].text_content() == 'Finish'
        fsc_finish = float(tr_elements[2][2].text_content())
        print('Final FSC average: {0}'.format(fsc_finish))


'''
Get result summary from buccaneer stdout
'''


def getResultLines(filename):
    tail_str = []
    found_result = False
    found_summary = False
    found_bucstart = False
    count = 0
    with open(filename, 'r') as openfile:
        for line in openfile:
            if ' cbuccaneer' in line:
                found_bucstart = True
                tail_str = []
            if found_bucstart:
                if '--SUMMARY_END--' in line:
                    found_summary = True
                if found_summary:
                    if 'TEXT:Result:' in line:
                        found_result = True
            if found_result and count < 10:
                temp_line = line.strip('\n')
                tail_str.append(temp_line)
                count = count + 1
            if 'cbuccaneer: Normal termination' in line:
                found_summary = False
                found_result = False
                found_bucstart = False
                count = 0
    return tail_str


'''
Get result summary from buccaneer program.xml
'''


def getXML(filename):
    '''
    Get the data from XML output
    '''
    tree = etree.parse(filename)
    for child in tree.findall('Final'):
        CompResBuilt = child.find('CompletenessByResiduesBuilt')
        CompChainBuilt = child.find('CompletenessByChainsBuilt')
        ChainsBuilt = child.find('ChainsBuilt')
        FragsBuilt = child.find('FragmentsBuilt')
        ResUniq = child.find('ResiduesUnique')
        ResBuilt = child.find('ResiduesBuilt')
        ResSeq = child.find('ResiduesSequenced')
        ResLongFrag = child.find('ResiduesLongestFragment')

    print("")
    print("{0} : {1:.1f}%" .format(
        CompResBuilt.tag, float(CompResBuilt.text) * 100))
    print("{0} : {1:.1f}%" .format(
        CompChainBuilt.tag, float(CompChainBuilt.text) * 100))
    print("{0} : {1}" .format(ChainsBuilt.tag, ChainsBuilt.text))
    print("{0} : {1}" .format(FragsBuilt.tag, FragsBuilt.text))
    print("{0} : {1}" .format(ResUniq.tag, ResUniq.text))
    print("{0} : {1}" .format(ResBuilt.tag, ResBuilt.text))
    print("{0} : {1}" .format(ResSeq.tag, ResSeq.text))
    print("{0} : {1}" .format(ResLongFrag.tag, ResLongFrag.text))
    print("")


if __name__ == '__main__':
    unittest.main()
