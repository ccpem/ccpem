#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Test refmac sharpen/blur utility
'''

import unittest
import os
import sys
import shutil
import time
import tempfile
from PyQt4 import QtGui, QtCore
from PyQt4.QtTest import QTest
from ccpem_core.tasks.mrc_to_mtz import mrc_to_mtz_task
from ccpem_gui.tasks.mrc_to_mtz import mrc_to_mtz_window
from ccpem_gui.tasks.mrc_to_mtz import mrc_to_mtz_plot
from ccpem_core.test_data.tasks import mrc_to_mtz as mrc_to_mtz_test_data
from ccpem_core import ccpem_utils
from ccpem_core import process_manager


app = QtGui.QApplication(sys.argv)


class MrcToMtzTest(unittest.TestCase):
    '''
    Unit test for MrcToMtz (sharpen/blur utility) (invokes GUI).
    '''
    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_data = os.path.dirname(mrc_to_mtz_test_data.__file__)
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        if os.path.exists(self.test_output):
            shutil.rmtree(self.test_output)

    def test_mrc_to_mtz_window_integration(self):
        '''
        Test refmac sharpen/blur pipeline via GUI.
        '''
        ccpem_utils.print_header(message='Unit test - MRC to MTZ')
        # Unit test args contain relative paths, must change to this directory
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        args_path = os.path.join(self.test_data, 'unittest_args.json')
        run_task = mrc_to_mtz_task.MrcToMtz(
            job_location=self.test_output,
            args_json=args_path)
        # Run w/out gui
#         run_task.run_task()
        # Run w/ gui
        self.window = mrc_to_mtz_window.MrcToMtzWindow(task=run_task)
        # Mouse click run
        QTest.mouseClick(
            self.window.tool_bar.widgetForAction(self.window.tb_run_button),
            QtCore.Qt.LeftButton)
        # Wait for run to complete
        self.job_completed = False
        timeout = 0
        stdout = run_task.pipeline.pipeline[0][0].stdout
        while not self.job_completed and timeout < 500:
            print 'Refmac running for {0} secs (timeout = 500)'.format(timeout)
            time.sleep(5.0)
            timeout += 5
            status =\
                process_manager.get_process_status(run_task.pipeline.json)
            if status == 'finished':
                if os.path.isfile(stdout):
                    tail = ccpem_utils.tail(stdout, maxlen=10)
                    if tail.find('CCP-EM process finished') != -1:
                        self.job_completed = True
        # Check timeout
        assert timeout < 500
        # Check job completed
        assert self.job_completed
        # Check mtz processing
        mtz_out_path = run_task.mtz_path
        plot = mrc_to_mtz_plot.AmplitudePlotWidget(
            mtz_in_path=mtz_out_path,
            blur_array=[-100])
        label = r'-100 $\AA^2$'
        self.assertAlmostEqual(plot.data[label].mean(),
                               135.75,
                               2)


if __name__ == '__main__':
    unittest.main()
