#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Task window for Refmac sharpen / blur utility
'''
import os
import warnings

from PyQt4 import QtGui

from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_core.tasks.mrc_to_mtz import mrc_to_mtz_task
from ccpem_gui.tasks.mrc_to_mtz import mrc_to_mtz_plot
from ccpem_gui.utils import command_line_launch
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import mrc_to_mtz as test_data


class MrcToMtzWindow(window_utils.CCPEMTaskWindow):
    '''
    MRC to MTZ window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(MrcToMtzWindow, self).__init__(
            task=task,
            parent=parent)
        self.plot_widget = None

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Input map
        map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_map',
            args=self.args,
            label='Input map',
            file_types=ccpem_file_types.mrc_ext,
            required=True)
        self.args_widget.args_layout.addWidget(map_input)

        # Input pdb
        pdb_input = window_utils.FileArgInput(
            parent=self,
            arg_name='start_pdb',
            args=self.args,
            label='Input PDB',
            file_types=ccpem_file_types.pdb_ext,
            required=False)
        self.args_widget.args_layout.addWidget(pdb_input)

        # Resolution
        resolution_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='resolution',
            args=self.args,
            required=True)
        self.args_widget.args_layout.addWidget(resolution_input)

        # Blur array
        blur_array_input = window_utils.ListArgInput(
            parent=self,
            arg_name='blur_array',
            args=self.args,
            required=False,
            element_type=float)
        self.args_widget.args_layout.addWidget(blur_array_input)

        # Set inputs for launcher
        self.launcher.add_file(
            arg_name='input_map',
            file_type='map',
            description=self.args.input_map.help,
            selected=True)
        self.launcher.add_file(
            arg_name='start_pdb',
            file_type='pdb',
            description=self.args.start_pdb.help,
            selected=True)

    def set_plot_widget(self):
        '''
        Matplotlib results dock
        '''
        if os.path.exists(self.task.mtz_path):
            # Suppress following warning:
            # UserWarning: Unable to find pixel distance along axis for
            # interval padding of ticks; assuming no interval padding needed.
            warnings.simplefilter('ignore', UserWarning)
            # Set plot widget - std plot
            self.plot_widget = mrc_to_mtz_plot.AmplitudePlotWidget(
                parent=self,
                mtz_in_path=self.task.mtz_path,
                blur_array=self.args.blur_array.value,
                pdb_in_path=self.args.start_pdb.value)
            # Set dock and layout
            self.plot_dock = QtGui.QDockWidget(
                'Plot',
                self)
            self.plot_dock.setWidget(self.plot_widget)
            self.tabifyDockWidget(self.pipeline_dock, self.plot_dock)

            # Set plot widget - log plot
            self.log_plot_widget = \
                    mrc_to_mtz_plot.LogAmplitudePlotWidget(
                parent=self,
                mtz_in_path=self.task.mtz_path,
                blur_array=self.args.blur_array.value,
                pdb_in_path=self.args.start_pdb.value)
            # Set dock and layout
            self.log_plot_dock = QtGui.QDockWidget(
                'Plot ln',
                self)
            self.log_plot_dock.setWidget(self.log_plot_widget)
            self.tabifyDockWidget(self.pipeline_dock, self.log_plot_dock)
            # Show standard plot by default
            self.plot_dock.show()
            self.plot_dock.raise_()
            # Reassign coot button to launch coot with maps
            self.run_coot_custom = self.plot_widget.launch_coot

    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.  For now show starting, refined
        pdb and experimental map.
        '''
        # Set launcher files
        # Processed mtz (w/ sharp / blurred arrays)
        if os.path.exists(self.task.mtz_path):
            self.launcher.add_file(
                arg_name=None,
                path=self.task.mtz_path,
                file_type='mtz',
                description='Structure factors from input map',
                selected=True)
        self.launcher.set_tree_view()
        self.set_plot_widget()


def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=mrc_to_mtz_task.MrcToMtz,
        window_class=MrcToMtzWindow)


if __name__ == '__main__':
    main()
