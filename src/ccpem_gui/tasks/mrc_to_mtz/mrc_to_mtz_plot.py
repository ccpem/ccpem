#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os

from PyQt4 import QtCore, QtGui
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt4agg import (
    FigureCanvasQTAgg as FigureCanvas)
from matplotlib.backends.backend_qt4agg import (
    NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure

from ccpem_gui.utils import gui_process
from ccpem_core.tasks.mrc_to_mtz import mrc_to_mtz_results


# To do -> extend to use non-refmac column labels

class AmplitudePlotWidget(QtGui.QWidget):
    plt.style.use('ggplot')
    # Set legend font size; standard too large
    params = {'legend.fontsize': 10}
    plt.rcParams.update(params)

    y_label = '<|F|>'

    def __init__(self,
                 mtz_in_path,
                 blur_array,
                 pdb_in_path=None,
                 parent=None):
        super(AmplitudePlotWidget, self).__init__(parent)
        self.pdb_in_path = pdb_in_path
        self.mtz_in_path = mtz_in_path
        self.blur_array = blur_array
        self.blur_array.sort(reverse=True)
        self.data = None

        # Set layout
        self.layout = QtGui.QVBoxLayout()
        self.layout.setAlignment(QtCore.Qt.AlignHCenter)
        self.setLayout(self.layout)

        self.try_load_from_cache_and_plot()

    def clear_layout(self):
        while self.layout.count():
            child = self.layout.takeAt(0)
            if child.widget() is not None:
                child.widget().deleteLater()

    def try_load_from_cache_and_plot(self):
        self.data = self.load_data_from_cache()
        if self.data is not None:
            self.clear_layout()
            self.draw_plot(self.data)
        else:
            self.clear_layout()
            self.layout.addStretch()
            self.layout.addWidget(QtGui.QLabel("Could not load data for plots. Click the button to recalculate. "
                                               "If the map is large, this could take some time."))
            button = QtGui.QPushButton("Recalculate plots", self)
            button.clicked.connect(self.recalculate_plot_data)
            self.layout.addWidget(button)
            self.layout.addStretch()

    def load_data_from_cache(self):
        # in separate function to allow overriding
        mean_amps, mean_amps_log = mrc_to_mtz_results.get_amps_from_cache(self.mtz_in_path)
        return mean_amps

    def recalculate_plot_data(self):
        mrc_to_mtz_results.cache_mean_amplitudes(self.mtz_in_path, self.blur_array)
        self.try_load_from_cache_and_plot()

    def draw_plot(self, data):
        self.fig = Figure(dpi=100)
        self.canvas = FigureCanvas(self.fig)
        self.canvas.setParent(self)
        self.axes = self.fig.add_subplot(111)
        # Set options for plot
        data.plot(ax=self.axes)
        y_max = 2.0 * data[r'0 $\AA^2$ (input)'].max()

        # Convert X axis labels from 1/A^2 to A
        def update_xaxis_ticklabels(axes):
            # Ignore warnings from zero and negative values
            with np.errstate(divide='ignore', invalid='ignore'):
                ticks = axes.get_xticks()
                axes.set_xticklabels(np.power(ticks, -0.5).round(2))

        # Set the initial X axis labels
        update_xaxis_ticklabels(self.axes)

        # Make sure the X axis labels are updated whenever the plot changes
        self.axes.callbacks.connect('xlim_changed', update_xaxis_ticklabels)

        self.axes.set_xlabel(mrc_to_mtz_results.INDEX_NAME)
        self.axes.set_ylabel(self.y_label)
        self.axes.set_ylim(0, y_max)
        self.axes.legend(loc='center left',
                         bbox_to_anchor=(1, 0.5))
        # See border so legend is visible
        self.fig.subplots_adjust(right=0.75)
        self.canvas.updateGeometry()
        self.layout.addWidget(self.canvas)
        # Create the navigation toolbar, tied to the canvas
        self.mpl_toolbar = NavigationToolbar(self.canvas, self)
        self.layout.addWidget(self.mpl_toolbar)

    def launch_coot(self):
        '''
        Launch coot with blurred / sharpened maps.
        '''
        # Convert blur / sharp arrays to coot column label names
        column_labels = []
        if self.blur_array is not None:
            for b in self.blur_array:
                if b > 0:
                    column_labels.append('FoutBlur_{0:.2f}'.format(b))
                elif b < 0:
                    column_labels.append('FoutSharp_{0:.2f}'.format(-b))
        column_labels.append('Fout0')
        make_and_draw_map = ''
        # Make python script string to pass to coot
        for label in column_labels:
            make_and_draw_map += \
                'make_and_draw_map("{0}", "{1}", "Pout0", "", 0, 0);'.format(
                    os.path.abspath(self.mtz_in_path),
                    label)
        # Get args
        args = []
        if self.pdb_in_path is not None:
            args += ['--pdb', os.path.abspath(self.pdb_in_path)]
        args += ['--no-state-script',
                 '--python', '-c',
                 make_and_draw_map]
        gui_process.run_coot(args=args)


class LogAmplitudePlotWidget(AmplitudePlotWidget):
    y_label = 'ln <|F|>'

    def load_data_from_cache(self):
        # Override to return log data
        mean_amps, mean_amps_log = mrc_to_mtz_results.get_amps_from_cache(self.mtz_in_path)
        return mean_amps_log