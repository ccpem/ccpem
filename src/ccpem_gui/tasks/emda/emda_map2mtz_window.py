#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from PyQt4 import QtCore, QtGui
from ccpem_gui.utils import window_utils
from ccpem_core.tasks.emda import emda_map2mtz_task
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_gui.utils import command_line_launch
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import emda as test_data

class EmdaMap2MtzWindow(window_utils.CCPEMTaskWindow):
    '''
    EMDA window.
    '''
    gui_test_args = get_test_data_path(test_data,
                                       'unittest_map2mtz_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(EmdaMap2MtzWindow, self).__init__(task=task,
                                           parent=parent)

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)

        # Input map
        input_target_map = window_utils.FileArgInput(
            parent=self,
            arg_name='target_map',
            required=True,
            file_types=ccpem_file_types.mrc_ext,
            args=self.args)
        self.args_widget.args_layout.addWidget(input_target_map)

        # Set inputs for launcher
        self.launcher.add_file(
            arg_name='target_map',
            file_type='map',
            description=self.args.target_map.help,
            selected=True)

    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.  Show output pdb.
        '''
        # Set launcher
        # Add expected output map names based on input target
        mtz_path = os.path.join(
            self.task.job_location,
            'map2mtz.mtz'
            )

        self.launcher.add_file(
            arg_name=None,
            path=mtz_path,
            file_type='mtz',
            description='MTZ file',
            selected=True)
        self.launcher.set_tree_view()

def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=emda_map2mtz_task.EMDA,
        window_class=EmdaMap2MtzWindow)

if __name__ == '__main__':
    main()

