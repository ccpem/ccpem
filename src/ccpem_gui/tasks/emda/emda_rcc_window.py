#
#     Copyright (C) 2021 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from PyQt4 import QtCore, QtGui
from ccpem_gui.utils import window_utils
from ccpem_core.tasks.emda import emda_rcc_task
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_gui.utils import command_line_launch
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import emda as test_data

class EmdaRccWindow(window_utils.CCPEMTaskWindow):
    '''
    EMDA RCC window.
    '''
    gui_test_args = get_test_data_path(test_data,
                                       'unittest_rcc_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(EmdaRccWindow, self).__init__(task=task,
                                            parent=parent)

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)

        # Model
        reference_model_input = window_utils.FileArgInput(
            parent=self,
            arg_name='model',
            args=self.args,
            file_types=ccpem_file_types.pdb_ext,
            required=True)
        self.args_widget.args_layout.addWidget(reference_model_input)

        # Input half map 1
        half_map_1_input = window_utils.FileArgInput(
            parent=self,
            arg_name='half_map_1',
            args=self.args,
            file_types=ccpem_file_types.mrc_ext,
            required=True)
        self.args_widget.args_layout.addWidget(half_map_1_input)

        # Input half map 2
        half_map_2_input = window_utils.FileArgInput(
            parent=self,
            arg_name='half_map_2',
            args=self.args,
            file_types=ccpem_file_types.mrc_ext,
            required=True)
        self.args_widget.args_layout.addWidget(half_map_2_input)

        # Resolution
        resolution_input = window_utils.NumberArgInput(
            parent=self,
            decimals=1,
            step=0.1,
            arg_name='resolution',
            args=self.args,
            required=True)
        self.args_widget.args_layout.addWidget(resolution_input)

        # Input mask
        mask_input = window_utils.FileArgInput(
            parent=self,
            arg_name='mask',
            args=self.args,
            file_types=ccpem_file_types.mrc_ext,
            required=False)
        self.args_widget.args_layout.addWidget(mask_input)

        # Ligand library input for Refmac5
        lib_input = window_utils.FileArgInput(
            parent=self,
            arg_name='cif_lib',
            args=self.args,
            file_types=ccpem_file_types.lib_ext,
            required=False)
        self.args_widget.args_layout.addWidget(lib_input)

        # Pixel size
        kernel_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='kernel_size',
            args=self.args,
            required=False)
        self.args_widget.args_layout.addWidget(kernel_input)

        # Normalised maps
        normalised_maps_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='normalised_maps',
            args=self.args)
        self.args_widget.args_layout.addWidget(normalised_maps_input)

        # Set inputs for launcher
        self.launcher.add_file(
            arg_name='half_map_1',
            file_type='map',
            description=self.args.half_map_1.help,
            selected=True)
        self.launcher.add_file(
            arg_name='half_map_2',
            file_type='map',
            description=self.args.half_map_2.help,
            selected=True)
        self.launcher.add_file(
            arg_name='mask',
            file_type='map',
            description=self.args.mask.help,
            selected=True)

    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.  Show output pdb.
        '''
        # Add expected output map names based on input target
        rcc_fullmap = 'rcc_fullmap_smax{0}.mrc'.format(
            self.task.args.kernel_size())
        print rcc_fullmap
        rcc_fullmap = os.path.join(
            self.task.job_location,
            rcc_fullmap)
        self.launcher.add_file(
            arg_name=None,
            path=rcc_fullmap,
            file_type='map',
            description='RCC full map',
            selected=True)
        #
        rcc_mapmodel = 'rcc_mapmodel_smax{0}.mrc'.format(
            self.task.args.kernel_size())
        print rcc_mapmodel
        rcc_mapmodel = os.path.join(
            self.task.job_location,
            rcc_mapmodel)
        self.launcher.add_file(
            arg_name=None,
            path=rcc_mapmodel,
            file_type='map',
            description='RCC map model',
            selected=True)
        #
        rcc_truemapmodel = 'rcc_truemapmodel_smax{0}.mrc'.format(
            self.task.args.kernel_size())
        print rcc_truemapmodel
        rcc_truemapmodel = os.path.join(
            self.task.job_location,
            rcc_truemapmodel)
        self.launcher.add_file(
            arg_name=None,
            path=rcc_truemapmodel,
            file_type='map',
            description='RCC true map model',
            selected=True)

        self.launcher.set_tree_view()

def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=emda_rcc_task.EmdaRcc,
        window_class=EmdaRccWindow)

if __name__ == '__main__':
    main()

