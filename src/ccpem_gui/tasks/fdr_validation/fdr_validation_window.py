#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Task window for Confidence Maps
'''
import glob
import os

from ccpem_core import chimera_scripts
from ccpem_core.settings import which

from confidenceMapUtil import mapUtil
import mrcfile
from PyQt4 import QtGui, QtCore, QtWebKit

from ccpem_gui.utils import command_line_launch
from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_core.tasks.fdr_validation import fdr_validation_task
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import fdr_validation as test_data
import pandas as pd
from ccpem_core import ccpem_utils
import shutil

class FDRValidationWindow(window_utils.CCPEMTaskWindow):
    '''
    Confidence Maps window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # input options
        self.map_or_fdr = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='input_selection',
            required=False,
            second_width=150,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_or_fdr)
        self.map_or_fdr.value_line.currentIndexChanged.connect(
            self.set_input_options)

        # Input map
        self.map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='em_map',
            args=self.args,
            file_types=ccpem_file_types.mrc_ext,
            required=True)
        self.args_widget.args_layout.addWidget(self.map_input)

        # Input map
        self.fdr_input = window_utils.FileArgInput(
            parent=self,
            arg_name='fdr_map',
            args=self.args,
            file_types=ccpem_file_types.mrc_ext,
            required=False)
        self.args_widget.args_layout.addWidget(self.fdr_input)

        # Input map
        self.model_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_model',
            args=self.args,
            file_types=ccpem_file_types.pdb_ext,
            required=True)
        self.args_widget.args_layout.addWidget(self.model_input)

        # Noise box controls in their own layout
        noisebox_layout = QtGui.QHBoxLayout()
        self.args_widget.args_layout.addLayout(noisebox_layout)

        # Noise window size
        self.window_size_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='window_size',
            args=self.args,
            required=False)
        noisebox_layout.addWidget(self.window_size_input)

        # Noise window coordinates
        self.noise_box_input = window_utils.ListArgInput(
            parent=self,
            arg_name='noise_box',
            args=self.args,
            required=False,
            element_type=int)
        noisebox_layout.addWidget(self.noise_box_input)
        self.noise_box_input.value_line.setPlaceholderText('Default')

        self.check_noise_box_button = QtGui.QPushButton('Check noise box')
        self.check_noise_box_button.clicked.connect(self.check_noise_box)
        self.check_noise_box_button.setToolTip("Show the noise box size and location.\n\nYou should check that the box does"
                                          " not overlap the molecule or any masked-out areas; it\nshould contain only "
                                          "background noise. If the box size is not set, a default value will be used.")
        noisebox_layout.addWidget(self.check_noise_box_button)

        # Prune model based on FDR values?
        self.prune_model = window_utils.CheckArgInput(
            parent=self,
            arg_name='prune',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.prune_model)

        # Validate model based on CA positions?
        self.val_ca = window_utils.CheckArgInput(
            parent=self,
            arg_name='valCA',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.val_ca)


        # Extended options
        self.extended_options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Extended (confidence maps) options',
            button_tooltip='Show extended options for confidence maps')
        self.args_widget.args_layout.addLayout(self.extended_options_frame)
        self.extended_options_frame.frame.hide()
        # Map pixel size
        apix_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='apix',
            args=self.args,
            decimals=3,
            step=0.01)
        self.extended_options_frame.add_extension_widget(apix_input)

        # Local resolution map
        locresmap_input = window_utils.FileArgInput(
            parent=self,
            arg_name='locResMap',
            args=self.args,
            file_types=ccpem_file_types.mrc_ext)
        self.extended_options_frame.add_extension_widget(locresmap_input)

        # Correction method
        method_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='method',
            args=self.args,
            second_width=None)  # None for auto-width for the drop-down box
        self.extended_options_frame.add_extension_widget(method_input)

        # Mean map
        meanmap_input = window_utils.FileArgInput(
            parent=self,
            arg_name='meanMap',
            args=self.args,
            file_types=ccpem_file_types.mrc_ext)
        self.extended_options_frame.add_extension_widget(meanmap_input)

        # Variance map
        variancemap_input = window_utils.FileArgInput(
            parent=self,
            arg_name='varianceMap',
            args=self.args,
            file_types=ccpem_file_types.mrc_ext)
        self.extended_options_frame.add_extension_widget(variancemap_input)

        # Test procedure
        testproc_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='test_proc',
            args=self.args)
        self.extended_options_frame.add_extension_widget(testproc_input)

        # Low pass filter
        lowpass_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='lowPassFilter',
            args=self.args)
        self.extended_options_frame.add_extension_widget(lowpass_input)

        # Empirical CDF?
        ecdf_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='ecdf',
            args=self.args)
        self.extended_options_frame.add_extension_widget(ecdf_input)

        # Set inputs and defaults for launcher
        self.launcher.set_mg_default('Chimera')
        self.launcher.add_file(
            arg_name='em_map',
            file_type='map',
            description=self.args.em_map.help,
            selected=True)

        self.extended_options_frame.frame.hide()
        self.set_input_options()

    def set_on_job_finish_custom(self):

        diag_image = os.path.join(self.pipeline.location, 'diag_image.pdf')
        #print ('aaaaaaaaaaaa')
        #/dls/ebic/data/staff-scratch/Mat/ccpem-task/Confidence Maps_34

        #print(self.pipeline.location)
        if os.path.isfile(diag_image):
            self.launcher.add_file(
                arg_name=None,
                path=diag_image,
                file_type='standard',
                description='Noise box diagnostic image',
                selected=False)

        for output_map in glob.glob(os.path.join(self.pipeline.location, '*_confidenceMap.mrc')):
            self.launcher.add_file(
                arg_name=None,
                path=output_map,
                file_type='map',
                description="Confidence map",
                selected=True)

        #for structure_inn in glob.glob(os.path.join(self.pipeline.location, '*_BACKBONE_average.pdb')):
        #    self.launcher.add_file(
        #        arg_name=None,
        #        path=structure_inn,
        #        file_type='pdb',
        #        description="validated model",
        #        selected=True)
        for output_model in glob.glob(os.path.join(self.pipeline.location, '*_FDR__attribute.txt')):

#        output_model = os.path.join(self.pipeline.location, '*_BACKBONE_average.pdb')
            self.launcher.add_file(
                path=output_model,
                file_type='standard',
                description='Chimera attribute file',
                selected=False)
            
#         for output_model in glob.glob(os.path.join(self.pipeline.location, '*_BACKBONE_average.pdb')):
# 
# #        output_model = os.path.join(self.pipeline.location, '*_BACKBONE_average.pdb')
#             self.launcher.add_file(
#                 path=output_model,
#                 file_type='pdb',
#                 description='Model',
#                 selected=True)
# 
#         for output_modelCA in glob.glob(os.path.join(self.pipeline.location, '*_CA_validated.pdb')):
# 
# #        output_model = os.path.join(self.pipeline.location, '*_BACKBONE_average.pdb')
#             self.launcher.add_file(
#                 path=output_modelCA,
#                 file_type='pdb',
#                 description='Model CA',
#                 selected=True)

        for output_pruned in glob.glob(os.path.join(self.pipeline.location, '*_PRUNED.pdb')):

#        output_model = os.path.join(self.pipeline.location, '*_BACKBONE_average.pdb')
            self.launcher.add_file(
                path=output_pruned,
                file_type='pdb',
                description='Model pruned',
                selected=True)

        for list_pruned in glob.glob(os.path.join(self.pipeline.location, '*_FDR_pruned_removed_residues.txt')):

#        output_model = os.path.join(self.pipeline.location, '*_BACKBONE_average.pdb')
            self.launcher.add_file(
                path=list_pruned,
                file_type='standard',
                description='List of removed residues',
                selected=False)

        for ranked_residues in glob.glob(os.path.join(self.pipeline.location, '*_FDR_ranked_residues.csv')):

#        output_model = os.path.join(self.pipeline.location, '*_BACKBONE_average.pdb')
            self.launcher.add_file(
                path=ranked_residues,
                file_type='standard',
                description='Residues with FDR scores',
                selected=False)


        self.launcher.set_tree_view()
        self.launcher_dock.raise_()
        self.launcher_dock.show()
        self.set_rv_ui()


    def set_input_options(self):
        if self.args.input_selection.value == 'unmasked map':
            self.map_input.show()
            self.map_input.set_required(True)
            self.fdr_input.hide()
            self.fdr_input.set_required(False)
            self.extended_options_frame.button.show()
            self.window_size_input.show()
            self.noise_box_input.show()
            self.noise_box_input.set_required(False)
            self.check_noise_box_button.show()
        else:
            self.map_input.hide()
            self.map_input.set_required(False)
            self.fdr_input.show()
            self.fdr_input.set_required(False)
            self.extended_options_frame.hide()
            self.window_size_input.hide()
            self.window_size_input.set_required(False)
            self.noise_box_input.set_required(False)
            self.noise_box_input.hide()
            self.check_noise_box_button.hide()

    def check_noise_box(self):
        # Get map file name from GUI
        self.map_input.on_edit_finished()
        map_file = self.map_input.action.value

        if map_file is None or not os.path.isfile(map_file):
            QtGui.QMessageBox.warning(self, 'Error', "Map file '{}' not found".format(map_file))
            return

        # Get window size from GUI
        self.window_size_input.on_edit_finished()
        window_size = self.window_size_input.action.value

        # Get box coordinates from GUI
        self.noise_box_input.on_edit_finished()
        box_coords = self.noise_box_input.action.value
        assert len(box_coords) == 0 or len(box_coords) == 3
        if len(box_coords) == 0:
            box_coords = 0

        # Generate and show the diagnostic plot
        with mrcfile.mmap(map_file) as map:

            # This duplicates the calculation done in confidenceMapMain.calculateConfidenceMap()
            # (It would be better to rearrange Max's code slightly so we don't have to just copy the calculation.)
            map_size = map.data.shape
            if window_size is None or window_size == 0:
                window_size = max(int(0.05 * map_size[0]), 10)
                self.window_size_input.set_arg_value(window_size)

            if window_size < 20:
                result = QtGui.QMessageBox.warning(self, 'Warning',
                                                   "Noise box size ({}) is quite small. Please think about potential "
                                                   "inaccuracies of your noise estimates!".format(window_size),
                                                   buttons=QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
                if result == QtGui.QMessageBox.Cancel:
                    return

            import matplotlib.pyplot as plt
            plt.rcdefaults()
            plt.ion()
            mapUtil.makeDiagnosticPlot(map.data, window_size, False, box_coords)
            plt.show()

    def handle_chimera_view(self):
        button = self.sender()
        chim_script = os.path.join(
                os.path.dirname(os.path.realpath(chimera_scripts.__file__)),
                'color_by_existing_attribute.py')

        model_input_basename = os.path.splitext(os.path.basename(self.args.input_model.value))[0]

        list_pdbs = []

        list_pdbs.append(os.path.join(self.pipeline.location, model_input_basename+'_BACKBONE_average.pdb'))
        pruned_path = os.path.join(self.pipeline.location, model_input_basename+'_PRUNED.pdb')
        CA_validated_path = os.path.join(self.pipeline.location, model_input_basename+'_CA_validated.pdb')

        if  os.path.isfile(pruned_path):
            list_pdbs.append(pruned_path)

        if  os.path.isfile(CA_validated_path):
            list_pdbs.append(CA_validated_path)

        #list_pdbs.append(self.args.input_model.value)
        #if self.args.fdr_map.value is not None:
        #    list_pdbs.append(self.args.fdr_map.value)
        if self.args.em_map.value is not None:
            list_pdbs.append(self.args.em_map.value)
        elif self.args.fdr_map.value is not None:
            list_pdbs.append(self.args.fdr_map.value)

        #print(chim_script)
        #print(list_pdbs)
        #print(self.args.fdr_map.value)
        pdb_str = ' '.join(list_pdbs)
        #print(pdb_str)

        chimera_command = '{} {} bfactor' .format(
            str(chim_script),
            str(pdb_str))

        arg_list_chimera = ['--script', '%s' % (chimera_command)]
        #print(arg_list_chimera)
        process = QtCore.QProcess()
        chimera_bin = which('chimera')
        #print(chimera_bin)
        if chimera_bin is None:
            print 'Chimera executable not found (add executable to system PATH)'
        else:
            process.startDetached(chimera_bin, arg_list_chimera)

    def set_chimera_button(self):
        #view in chimera
        chimera_button_widget = QtGui.QWidget()
        chimera_button_layout = QtGui.QHBoxLayout()
        #chimera_button_layout.setContentsMargins(0, 0, 0, 0)
        #chimera_button_layout.setSpacing(0)
        chimera_button_widget.setLayout(chimera_button_layout)
        chimera_view_button = QtGui.QPushButton('View in Chimera')
        if which('chimera') is not None:
            chimera_view_button.clicked.connect(self.handle_chimera_view)
        else: chimera_view_button.setEnabled(False)
        chimera_button_layout.addWidget(chimera_view_button)
        return chimera_button_widget


    def set_rv_ui(self):
        '''
        RVAPI results viewer.
        '''
        # Create results if not already done
        #out_pdb = os.path.join(self.task.job_location, '*_BACKBONE_average.pdb')

        #os.path.join(
        #    self.task.job_location,
        #    '*.pdb')
        
        self.directory = os.path.join(self.task.job_location,
                                      'rvapi_data')
        # Create results if not already done
        rv_index = os.path.join(
            self.task.job_location,
            'rvapi_data/index.html')
        
        if os.path.exists(rv_index):
            rvapi_layout = QtGui.QHBoxLayout()
            #rvapi_layout.setSpacing(0)
            #rvapi_layout.setContentsMargins(0, 0, 0, 10)
            self.rv_view = QtWebKit.QWebView()
            self.rv_view.load(QtCore.QUrl(rv_index))
            results_dock_layout = QtGui.QVBoxLayout()
            results_dock_widget = QtGui.QWidget()
            results_dock_widget.setLayout(results_dock_layout)
            self.results_dock = QtGui.QDockWidget('Results',
                                                       self,
                                                       QtCore.Qt.Widget)
    
            #self.results_dock.setToolTip('Results overview')
            self.results_dock.setWidget(results_dock_widget)#self.rv_view)
            self.tabifyDockWidget(self.setup_dock, self.results_dock)
            chimera_view_button = self.set_chimera_button()
            results_dock_layout.addWidget(chimera_view_button)
            self.rv_view.setMinimumHeight(700)
            #self.rv_view.setLayout(rvapi_layout)
            results_dock_layout.addWidget(self.rv_view)
            results_dock_layout.addStretch(1)
            self.results_dock.raise_()

def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=fdr_validation_task.FDRValidationTask,
        window_class=FDRValidationWindow)


if __name__ == '__main__':
    main()
