#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Task window for ProSMART.
'''
import os

from PyQt4 import QtGui, QtCore, QtWebKit

from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_core.ccpem_utils import ccpem_coord_tools
from ccpem_core.tasks.prosmart import prosmart_task
from ccpem_gui.utils import gui_process
from ccpem_gui.utils import command_line_launch
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import prosmart as test_data


# XXX Coot - how to autoload restraints
# XXX Howto link with refmac

class PDBStats(QtCore.QThread):
    '''
    Thread to read PDB file.  Large PDBs may take time therefore use 
    seperate thread.
    '''
    def __init__(self,
                 path):
        super(PDBStats, self).__init__()
        self.path = path
        self.chains = None

    def run(self):
        self.get_chain_ids()

    def get_chain_ids(self):
        if self.path is not None:
            if os.path.exists(self.path):
                pdb = ccpem_coord_tools.CCPEMPDB(path=self.path)
                self.chains = pdb.get_chain_ids()


class PBBDatasets(object):
    def __init__(self,
                 pdb_path=None,
                 chains=None):
        self.pdb_path = None
        self.set_pdb_path(pdb_path=pdb_path)
        self.set_chains(chains=chains)

    def set_pdb_path(self, pdb_path):
        if pdb_path is not None:
            self.pdb_path = str(pdb_path)

    def validate(self):
        if [self.pdb_path].count(None) == 0:
            return True
        else:
            return False

    def set_chains(self, chains):
        self.chains=chains

class PDBInput(QtGui.QWidget):
    def __init__(self,
                 label='PBD',
                 pdb_path=None,
                 chains=None,
                 parent=None):
        super(PDBInput, self).__init__()
        self.parent = parent
        self.dataset = PBBDatasets()
        self.pdb_stats = None
        button_width = 100
        label_width = 150
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)
        self.setToolTip('Add PDB')
        pdb_chain_box = QtGui.QVBoxLayout()
        self.layout.addLayout(pdb_chain_box)

        # PDB
        pdb_box = QtGui.QHBoxLayout()
        pdb_chain_box.addLayout(pdb_box)
        self.pdb_label = QtGui.QLabel(label)
        self.pdb_label.setFixedWidth(label_width)
        pdb_box.addWidget(self.pdb_label)
        pdb_select = QtGui.QPushButton('Select')
        pdb_select.setFixedWidth(button_width)
        pdb_select.clicked.connect(self.get_pdb_file)
        pdb_box.addWidget(pdb_select)
        self.pdb_value = QtGui.QLineEdit()
        self.pdb_value.editingFinished.connect(self.edit_finished_pdb)
        pdb_box.addWidget(self.pdb_value)

        width_ar = 30
        # Add
        add_button = QtGui.QPushButton('+')
        add_button.setFixedWidth(width_ar)
        add_button.setToolTip('Add dataset')
        add_button.clicked.connect(self.add)
        pdb_box.addWidget(add_button)

        # Remove
        remove_button = QtGui.QPushButton('-')
        remove_button.setFixedWidth(width_ar)
        remove_button.setToolTip('Remove dataset')
        remove_button.clicked.connect(self.remove)
        pdb_box.addWidget(remove_button)

        # Chains
        chain_box = QtGui.QHBoxLayout()
        pdb_chain_box.addLayout(chain_box)
        self.chain_label = QtGui.QLabel('Chains')

        self.chain_label.setFixedWidth(label_width)
        chain_box.addWidget(self.chain_label)
        self.chain_list = QtGui.QListWidget(self)
        self.chain_list.setSelectionMode(
            QtGui.QAbstractItemView.MultiSelection)
        tooltip = 'Select chains, multiple selections made by mouse drag'
        self.chain_label.setToolTip(tooltip)
        self.chain_list.setToolTip(tooltip)
        self.chain_list.setFixedWidth(button_width)
        self.chain_list.itemSelectionChanged.connect(self.set_chains)
        chain_box.addWidget(self.chain_list)
        chain_box.setAlignment(self.chain_list, QtCore.Qt.AlignLeft)

        # Set initial values
        # PDB
        self.set_pdb_file(path=pdb_path)
        self.set_chains_list(chains=chains)

    def get_pdb_file(self):
        path = self.get_file_from_brower(file_types=ccpem_file_types.pdb_ext)
        if path is not None:
            self.set_pdb_file(path=path)

    def edit_finished_pdb(self):
        path = str(self.pdb_value.text())
        self.set_pdb_file(path=path)

    def set_pdb_file(self, path):
        self.set_shading(self.pdb_label, shade=True)
        self.set_shading(self.pdb_value, shade=True)
        self.dataset.pdb_path = None
        if path is not None:
            if os.path.exists(path):
                self.pdb_value.setText(path)
                self.set_shading(self.pdb_label, shade=False)
                self.set_shading(self.pdb_value, shade=False)
                self.dataset.set_pdb_path(path)
            else:
                path = None
        self.chain_list.clear()
        self.get_chain_list(path=path)

    def populate_chain_list(self):
        reset = True
        if self.pdb_stats.chains is not None:
            for chain in self.pdb_stats.chains:
                item = QtGui.QListWidgetItem(str(chain))
                self.chain_list.addItem(item)
                item.setSelected(True)
                reset = False

        # Reset chains if path does not exist
        if reset:
            self.chain_list.clear()

    def get_chain_list(self, path=None):
        # Launch in seperate thread as large files may take time to read
        self.pdb_stats = PDBStats(path=path)
        self.pdb_stats.start()
        self.pdb_stats.finished.connect(self.populate_chain_list)        

    def set_chains_list(self, chains=None):
        if isinstance(chains, str):
            chains = [chains]
        for i in xrange(self.chain_list.count()):
            item = self.chain_list.item(i)
            if chains is None:
                item.setSelected(True)
            else:
                if str(item.text()) in chains:
                    item.setSelected(True)
                else:
                    item.setSelected(False)

    def set_chains(self):
        chains = []
        for i in xrange(self.chain_list.count()):
            item = self.chain_list.item(i)
            if item.isSelected():
                chains.append(str(item.text()))
        self.dataset.set_chains(chains=chains)

    def get_file_from_brower(self, file_types):
        dialog_path = window_utils.get_last_directory_browsed()
        path = QtGui.QFileDialog.getOpenFileName(
            self,
            'Open a File',
            QtCore.QDir.path(QtCore.QDir(dialog_path)),
            file_types)
        path = str(path)
        if path == '':
            path = None
        if path is not None:
            dialog_path = os.path.dirname(path)
            window_utils.set_last_directory_browsed(path=dialog_path)
        return path

    def set_shading(self, widget, shade=True):
        '''
        Shade text QLineEdit red to display warning.
        '''
        if shade:
            widget.setStyleSheet('color: red')
        else:
            widget.setStyleSheet('color: None')

    def remove(self):
        '''
        Only remove if more than 2 are present
        '''
        if hasattr(self.parent, 'number_pdb_inputs'):
            if self.parent.number_pdb_inputs() >= 2:
                self.deleteLater()

    def add(self):
        if hasattr(self.parent, 'add_dataset'):
            self.parent.add_dataset()

class MultiPDBInput(QtGui.QWidget):
    def __init__(self,
                 group_label=None,
                 pdbs_arg=None,
                 chains_arg=None,
                 required=True,
                 parent=None):
        super(MultiPDBInput, self).__init__()
        self.parent = parent
        self.pdbs_arg = pdbs_arg
        self.chains_arg = chains_arg
        self.active = True

        # Layout
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)
        
        # Set tooltip
        tooltip_text = self.pdbs_arg.help
        if required:
            tooltip_text += ' | Required'
        self.setToolTip(tooltip_text)
        
        # Label
        if group_label is not None:
            ds_label = QtGui.QLabel(group_label)
            self.layout.addWidget(ds_label)

        # Set args
        pdbs = self.pdbs_arg()
        chains = self.chains_arg()

        # Add datasets
        if isinstance(pdbs, list):
            for n in xrange(len(pdbs)):
                self.add_dataset(
                    pdb_path=pdbs[n],
                    chains=chains[n])
        else:
            self.add_dataset(
                pdb_path=pdbs,
                chains=chains)

    def handle_add_button(self):
        '''
        Handle dataset button
        '''
        self.add_dataset()

    def add_dataset(self,
                    pdb_path=None,
                    chains=None):
        dataset = PDBInput(pdb_path=pdb_path,
                           chains=chains,
                           parent=self)
        self.layout.addWidget(dataset)

    def number_pdb_inputs(self):
        return len(self.findChildren(PDBInput))

    def set_active(self):
        '''
        Show self and switch on validation
        '''
        self.show()
        self.active = True

    def set_inactive(self):
        '''
        Hide self and switch off validation
        '''
        self.hide()
        self.active = False

    def validate_datasets(self):
        if self.active:
            children = self.findChildren(PDBInput)
            # Check at least one dataset
            if len(children) == 0:
                # Warn no datasets provided
                text = 'No input datasets provided'
                QtGui.QMessageBox.warning(self,
                    'Error',
                    text)
                return False
            warning = False
            # Check datasets are complete (must have pdb, map, resolution)
            for child in children:
                if not child.dataset.validate():
                    warning = True
            if warning:
                # Warn datasets incomplete information
                text = 'Dataset(s) incomplete (requires PDB)'
                QtGui.QMessageBox.warning(self,
                    'Error',
                    text)
                return False
            else:
                return True
        else:
            return True

    def set_args(self):
        children = self.findChildren(PDBInput)
        pdbs = []
        chains = []
        if len(children) > 1:
            for child in children:
                pdbs.append(child.dataset.pdb_path)
                chains.append(child.dataset.chains)
            self.pdbs_arg.value = pdbs
            self.chains_arg.value = chains
        else:
            child = children[0]
            self.pdbs_arg.value = child.dataset.pdb_path
            self.chains_arg.value = child.dataset.chains

class ProSMARTWindow(window_utils.CCPEMTaskWindow):
    '''
    ProSMART window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(ProSMARTWindow, self).__init__(task=task,
                                             parent=parent)

    def set_args(self):
        '''
        Set input arguments
        '''
        
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Alignment mode
        self.alignement_mode = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='mode',
            second_width=None,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.alignement_mode)
        self.alignement_mode.value_line.currentIndexChanged.connect(
            self.set_mode)

        # Target PDBs
        self.target_pdbs = MultiPDBInput(
            group_label='Target PDB(s)',
            pdbs_arg=self.args.target_pdbs,
            chains_arg=self.args.target_pdb_chains,
            parent=self)
        self.args_widget.args_layout.addWidget(self.target_pdbs)

        # Reference PDBs
        self.ref_pdbs = MultiPDBInput(
            group_label='Reference PDB(s)',
            pdbs_arg=self.args.ref_pdbs,
            chains_arg=self.args.ref_pdb_chains,
            parent=self)
        self.args_widget.args_layout.addWidget(self.ref_pdbs)

        # Extended options
        extended_options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Extended options',
            button_tooltip='Extended options for ProSMART')
        self.args_widget.args_layout.addLayout(extended_options_frame)

        # -> Ncycle
        threads_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='threads',
            args=self.args)
        extended_options_frame.add_extension_widget(threads_input)

        # Keyword frame
        keyword_frame = window_utils.CCPEMExtensionFrame(
            button_name='Additional arguments',
            button_tooltip='Optional user-supplied arguments')
        self.args_widget.args_layout.addLayout(keyword_frame)

        # -> Keyword entry
        self.keyword_entry_new = window_utils.KeywordArgInput(
            parent=self,
            arg_name='keywords',
            args=self.args,
            label='Additional arguments')
        keyword_frame.add_extension_widget(self.keyword_entry_new)

    def set_mode(self):
        if self.args.mode() == 'Reference model':
            self.ref_pdbs.set_active()
            
        else:
            self.ref_pdbs.set_inactive()

    def set_rv_ui(self):
        '''
        RVAPI results viewer.
        '''
        # Create results if not already done
        rv_index = os.path.join(
            self.task.job_location,
            'rvapi_data/index.html')
        if os.path.exists(rv_index):
            self.rv_view = QtWebKit.QWebView()
            assert os.path.exists(rv_index)
            self.rv_view.load(QtCore.QUrl(rv_index))
            self.results_dock = QtGui.QDockWidget('Results',
                                                   self,
                                                   QtCore.Qt.Widget)
            self.results_dock.setToolTip('Results overview')
            self.results_dock.setWidget(self.rv_view)
            self.tabifyDockWidget(self.setup_dock, self.results_dock)
            self.results_dock.show()
            self.results_dock.raise_()

    def validate_input(self):
        '''
        Override from baseclass to allow validation of multiple datasets
        '''
        # Remove reference pdbs if not used
        if self.args.mode() != 'Reference model':
            self.args.ref_pdbs.value = None
        ready = super(ProSMARTWindow, self).validate_input()
        if ready:
            # Validate inputs for multiple datasets
            if not self.target_pdbs.validate_datasets():
                ready = False
            else:
                self.target_pdbs.set_args()
            if self.ref_pdbs is not None:
                if not self.ref_pdbs.validate_datasets():
                    ready = False
                else:
                    self.ref_pdbs.set_args()
        return ready

    def set_on_job_running_custom(self):
        if hasattr(self, 'tb_chimera_button'):
            self.tb_chimera_button.setEnabled(False)

    def set_on_job_finish_custom(self):
        if hasattr(self, 'tb_chimera_button'):
            # Set chimera script
            self.chimera_script = os.path.join(
                self.task.job_location,
                'ProSMART_Output/Output_Files/Loader_Scripts/Chimera')
            script = os.listdir(self.chimera_script)
            if len(script) > 0:
                self.chimera_script = os.path.join(
                    self.chimera_script,
                    os.listdir(self.chimera_script)[0])
                if os.path.exists(path=self.chimera_script):
                    self.tb_chimera_button.setEnabled(True)

        # Set results html
        self.set_results_html()

        # Show target pdbs
        pdbs = self.task.args.target_pdbs()
        if not isinstance(pdbs, list):
            pdbs = [pdbs]
        for pdb in pdbs:
            self.launcher.add_file(
                arg_name=self.task.args.target_pdbs,
                path=pdb,
                file_type='pdb',
                description='Target pdb',
                selected=True)
        # Show restraints
        res_path = self.find_restrints_file_output()
        if res_path is not None:
            self.launcher.add_file(
                arg_name=None,
                path=res_path,
                file_type='standard',
                description='Restraints file',
                selected=True)
        self.launcher.set_tree_view()
        self.launcher_dock.raise_()
        self.launcher_dock.show()

    def run_chimera_custom(self):
        args = [self.chimera_script]
        gui_process.run_chimera(args=args)

    def find_restrints_file_output(self):
        ps_path = os.path.join(self.task.job_location,
                               'ProSMART_Output')
        sig = '# ProSMART Restraints File'
        for f in os.listdir(ps_path):
            if f.endswith('.txt'):
                path = os.path.join(
                    ps_path,
                    f)
                with open(path, 'r') as res:
                    first_line = res.readline()
                    if sig in first_line:
                        return path
        return None

    def run_coot_custom(self):
        path = self.find_restrints_file_output()
        coot_script = None
        if path is not None:
            coot_script = self.make_coot_script(path)
        args = self.task.args.target_pdbs()
        if args is not isinstance(args, list):
            args = [args]
        if coot_script is not None:
            args += ['-c',
                     coot_script]
        gui_process.run_coot(args=args)

    def make_coot_script(self, prosmart_file):
        path = os.path.join(self.task.job_location,
                            'ps_coot.py')
        f = open(path, 'w')
        command = 'add_refmac_extra_restraints(0, "{0}")'.format(
            prosmart_file)
        f.write(command)
        f.close()
        return path

    def set_results_html(self):
        '''
        ProSMART results viewer.
        '''
        # Create results if not already done
        path = os.path.join(
            self.task.job_location,
            'ProSMART_Output/ProSMART_Results.html')
        if os.path.exists(path):
            self.rv_view = QtWebKit.QWebView()
            self.rv_view.load(QtCore.QUrl(path))
            self.results_dock = QtGui.QDockWidget('Results',
                                                   self,
                                                   QtCore.Qt.Widget)
            self.results_dock.setToolTip('Results overview')
            self.results_dock.setWidget(self.rv_view)
            self.tabifyDockWidget(self.setup_dock, self.results_dock)
            self.results_dock.raise_()


def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=prosmart_task.ProSMART,
        window_class=ProSMARTWindow)

if __name__ == '__main__':
    main()
