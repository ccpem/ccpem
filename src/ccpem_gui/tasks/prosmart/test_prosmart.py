#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Test ProSMART task and GUI
'''

import unittest
import os
import sys
import shutil
import time
import tempfile
from PyQt4 import QtGui, QtCore
from PyQt4.QtTest import QTest
from ccpem_core.tasks.prosmart import prosmart_task
from ccpem_gui.tasks.prosmart import prosmart_window
from ccpem_core.test_data.tasks import prosmart as prosmart_test
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from numpy.testing.utils import assert_approx_equal

app = QtGui.QApplication(sys.argv)

class ProSMARTTest(unittest.TestCase):
    '''
    Unit test for ProSMART (invokes GUI).
    '''
    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_data = os.path.dirname(prosmart_test.__file__)
        self.test_output = tempfile.mkdtemp()
        print self.test_output

    def tearDown(self):
        if os.path.exists(self.test_output):
            shutil.rmtree(self.test_output)

    def test_prosmart_window_integration(self):
        '''
        Test prosmart restraint generation pipeline via GUI.
        '''
        ccpem_utils.print_header(message='Unit test - ProSMART')
        # Unit test args contain relative paths, must change to this directory
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        args_path = os.path.join(self.test_data, 'unittest_args.json')
        run_task = prosmart_task.ProSMART(
            job_location=self.test_output,
            args_json=args_path)
        print run_task.args.output_args_as_text()
        # Run w/ gui
        window = prosmart_window.ProSMARTWindow(task=run_task)
        # Mouse click run
        QTest.mouseClick(
            window.tool_bar.widgetForAction(window.tb_run_button),
            QtCore.Qt.LeftButton)
        # Wait for run to complete
        job_completed = False
        timeout = 0
        # Global refine stdout (i.e. last job in pipeline)
        stdout = run_task.pipeline.pipeline[-1][-1].stdout
        delay = 5.0
        while not job_completed and timeout < 500:
            print 'ProSMART running for {0} secs (timeout = 500)'.format(timeout)
            time.sleep(delay)
            timeout += delay
            status =\
                process_manager.get_process_status(run_task.pipeline.json)
            if status == 'finished':
                if os.path.isfile(stdout):
                    tail = ccpem_utils.tail(stdout, maxlen=10)
                    if tail.find('CCP-EM process finished') != -1:
                        job_completed = True
        # Check timeout
        assert timeout < 500
        # Check job completed
        assert job_completed
        # Check html output file created
        html = os.path.join(window.task.job_location,
                            'ProSMART_Output/ProSMART_Results.html')
        assert os.path.exists(path=html)
        # Check restraints file gives expected results. Check final line for 
        # expected values
        res = os.path.join(window.task.job_location,
                           'ProSMART_Output/1ryx_trg.txt')
        res_tail = ccpem_utils.tail(filename=res, maxlen=1)
        res_tail = res_tail.split()
        assert_approx_equal(res_tail[-1], 0.1, 2)
        assert_approx_equal(res_tail[-3], 3.63209, 4)
        assert res_tail[-5] == 'CB'


if __name__ == '__main__':
    unittest.main()
