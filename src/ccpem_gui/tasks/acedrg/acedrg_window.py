#
#     Copyright (C) 2020 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
'''
Task window for AceDRG
'''
import os

from PyQt4 import QtGui

from ccpem_gui.utils import command_line_launch
from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_core.tasks.acedrg import acedrg_task
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import acedrg as test_data


class AcedrgWindow(window_utils.CCPEMTaskWindow):
    '''
    AceDRG window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args_smiles_string.json')

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        input_group = QtGui.QGroupBox(parent=self,
                                      title='Inputs - use one')
        self.args_widget.args_layout.addWidget(input_group)
        input_group_layout = QtGui.QVBoxLayout()

        # Input SMILES string
        smiles_string_input = window_utils.StrArgInput(
            parent=self,
            label='SMILES string',
            arg_name='smiles_string',
            args=self.args)
        input_group_layout.addWidget(smiles_string_input)

        # Input SMILES file
        smiles_file_input = window_utils.FileArgInput(
            parent=self,
            label='SMILES file',
            arg_name='smiles_file',
            args=self.args,
            file_types=ccpem_file_types.all_ext)
        input_group_layout.addWidget(smiles_file_input)

        # Input MOL file
        mol_file_input = window_utils.FileArgInput(
            parent=self,
            label='MOL file',
            arg_name='mol_file',
            args=self.args,
            file_types=ccpem_file_types.all_ext)
        input_group_layout.addWidget(mol_file_input)

        # Input mmCIF file
        mmcif_file_input = window_utils.FileArgInput(
            parent=self,
            label='mmCIF file',
            arg_name='mmcif_file',
            args=self.args,
            file_types=ccpem_file_types.all_ext)
        input_group_layout.addWidget(mmcif_file_input)

        input_group.setLayout(input_group_layout)

        # Input monomer code
        monomer_code_input = window_utils.StrArgInput(
            parent=self,
            arg_name='monomer_code',
            args=self.args)
        self.args_widget.args_layout.addWidget(monomer_code_input)

        # Set inputs and defaults for launcher
        self.launcher.set_mg_default('Coot')
        self.launcher.add_file(
            arg_name='smiles_file',
            description=self.args.smiles_file.help,
            selected=False)
        self.launcher.add_file(
            arg_name='mol_file',
            description=self.args.mol_file.help,
            selected=False)
        self.launcher.add_file(
            arg_name='mmcif_file',
            description=self.args.mmcif_file.help,
            selected=False)

    def validate_input(self):
        ready = super(AcedrgWindow, self).validate_input()
        num_inputs = 0
        first_input = ''
        # Step through inputs in reverse order so first_input is assigned to the last
        # one seen
        for acedrg_input in (self.args.mmcif_file.value,
                             self.args.mol_file.value,
                             self.args.smiles_file.value,
                             self.args.smiles_string.value):
            if acedrg_input:
                num_inputs += 1
                first_input = acedrg_input
        if num_inputs == 0:
            ready = False
            QtGui.QMessageBox.warning(self, 'Error',
                                      'Error - you must give some kind of input')
        elif num_inputs > 1:
            message = ('You have given more than one type of input. '
                       'Only the first one ({}) will be used.').format(first_input)
            result = QtGui.QMessageBox.warning(self, 'Warning', message,
                                               buttons=QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
            ready = (result == QtGui.QMessageBox.Ok)
        return ready

    def set_on_job_finish_custom(self):
        found_output = False
        for out_file in os.listdir(self.task.job_location):
            # This is a bit fragile, finding the output files depends on the exact
            # output name chosen in AcedrgTask.run_pipeline()
            if out_file.endswith('acedrg.pdb'):
                found_output = True
                self.launcher.add_file(
                    path=os.path.join(self.task.job_location, out_file),
                    file_type='pdb',
                    description='Output PDB file',
                    selected=True)
            elif out_file.endswith('acedrg.cif'):
                found_output = True
                self.launcher.add_file(
                    path=os.path.join(self.task.job_location, out_file),
                    file_type='standard',
                    description='Output CIF dictionary file',
                    selected=False)
        if found_output:
            self.launcher.set_tree_view()
            self.launcher_dock.raise_()


def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=acedrg_task.AcedrgTask,
        window_class=AcedrgWindow)


if __name__ == '__main__':
    main()
