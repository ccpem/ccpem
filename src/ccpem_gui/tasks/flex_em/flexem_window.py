#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import os, re

from PyQt4 import QtGui, QtCore, QtWebKit

from ccpem_core.tasks.flex_em import flexem_task
from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import flex_em as test_data
try:
    import modeller # @UnusedImport
    modeller_available = True
except ImportError:
    modeller_available = False
from ccpem_gui.utils import command_line_launch


class FlexEMWindow(window_utils.CCPEMTaskWindow):
    '''
    FlexEM window.
    '''
    gui_test_args = get_test_data_path(test_data, 'demo_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(FlexEMWindow, self).__init__(task=task,
                                           parent=parent)
        self.output_pdb = None

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)

        # Input map
        self.map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_map',
            file_types=ccpem_file_types.mrc_ext,
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_input)

        # Input pdb
        pdb_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_pdb',
            file_types=ccpem_file_types.pdb_ext,
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(pdb_input)

        # Generate rigid body files
        self.create_rigid_body_input = window_utils.CheckArgInput(
            parent=self,
            arg_name='create_rigid_body',
            args=self.args)
        self.args_widget.args_layout.addWidget(
            self.create_rigid_body_input)
        self.create_rigid_body_input.value_line.stateChanged.connect(
            self.set_create_rigid_body)

        # Ribfind cutoff
        self.ribfind_cutoff_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='ribfind_cutoff',
            step=10,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.ribfind_cutoff_input)
        
        # Input rigid body file
        self.rigid_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_rigid',
            required=True,
            file_types=ccpem_file_types.text_ext,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.rigid_input)
        self.set_create_rigid_body()

        # Resolution restraints
        self.resolution_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='map_resolution',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.resolution_input)

        # Iterations
        self.iterations_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='iterations',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.iterations_input)

        # Extended options
        self.options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Extended options',
            button_tooltip='Extended Flex-EM options')
        self.args_widget.args_layout.addLayout(self.options_frame)

#         # -> Phi-Psi restraints
#         phi_psi_input = window_utils.ChoiceArgInput(
#             parent=self,
#             arg_name='phi_psi_restraints',
#             args=self.args)
#         self.options_frame.add_extension_widget(phi_psi_input)

#         # Mode
#         self.mode_input = window_utils.ChoiceArgInput(
#             parent=self,
#             arg_name='mode',
#             args=self.args)
#         self.options_frame.add_extension_widget(self.mode_input)
 
        # Cutoff 
        self.cutoff_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='max_atom_disp',
            required=False,
            args=self.args)
        self.options_frame.add_extension_widget(self.cutoff_input)
        # density weight 
        self.density_weight = window_utils.NumberArgInput(
            parent=self,
            arg_name='density_weight',
            required=False,
            args=self.args)
        self.options_frame.add_extension_widget(self.density_weight)
        #template restraint options
        self.set_template_restraints()
        
        # Keyword frame
        keyword_frame = window_utils.CCPEMExtensionFrame(
            button_name='Keywords',
            button_tooltip='Optional user-supplied keywords (see info)')
        self.args_widget.args_layout.addLayout(keyword_frame)
 
        # -> Keyword entry
        self.keyword_entry_new = window_utils.KeywordArgInput(
            parent=self,
            arg_name='keywords',
            args=self.args,
            label='Keywords')
        keyword_frame.add_extension_widget(self.keyword_entry_new)
        
        # Alpha test... refine B's; calc FSC; HM validation
        if self.args.alpha_mode():
            refine_b_input = window_utils.ChoiceArgInput(
                parent=self,
                arg_name='refine_b',
                args=self.args)
            self.options_frame.add_extension_widget(refine_b_input)
            #
            fsc_calc_input = window_utils.ChoiceArgInput(
                parent=self,
                arg_name='fsc_calc',
                args=self.args)
            self.options_frame.add_extension_widget(fsc_calc_input)
            #
            self.half_map_1_input = window_utils.FileArgInput(
                parent=self,
                arg_name='half_map_1',
                args=self.args,
                required=True)

            self.half_map_2_input = window_utils.FileArgInput(
                parent=self,
                arg_name='half_map_2',
                args=self.args,
                required=True)

            # -> Validation on
            self.validation_button = window_utils.CheckArgInput(
                parent=self,
                arg_name='validate',
                dependants=[self.half_map_1_input, self.half_map_2_input],
                args=self.args)
            self.options_frame.add_extension_widget(self.validation_button)

            # -> Half map 1
            self.options_frame.add_extension_widget(self.half_map_1_input)

            # -> Half map 2
            self.options_frame.add_extension_widget(self.half_map_2_input)

        # Add files to launcher
        self.launcher.set_mg_default('Chimera')
        self.launcher.add_file(
            arg_name='input_rigid',
            file_type='standard',
            description='Rigid body cluster',
            selected=True)
        self.launcher.add_file(
            arg_name='input_pdb',
            file_type='pdb',
            description=self.args.input_pdb.help,
            selected=True)
        self.launcher.add_file(
            arg_name='input_map',
            file_type='map',
            description=self.args.input_map.help,
            selected=True)

    def set_template_restraints(self):
        template_restraints_frame = window_utils.CCPEMExtensionFrame(
            button_name='template restraints',
            button_tooltip=('restrain based on template\n'))
        
        self.options_frame.add_extension_frame(template_restraints_frame)
        # restraint choices
        self.template_phipsi = window_utils.CheckArgInput(
            parent=self,
            arg_name='phi_psi_restraints',
#             tooltip_text=(''),
            args=self.args)
        template_restraints_frame.add_extension_widget(self.template_phipsi)
#         self.template_distance = window_utils.CheckArgInput(
#             parent=self,
#             arg_name='distance_restraints',
# #             tooltip_text=(''),
#             args=self.args)
#         template_restraints_frame.add_extension_widget(self.template_distance)
#         self.max_template_distance = window_utils.NumberArgInput(
#             parent=self,
#             arg_name='max_distance',
#             args=self.args)
#         self.options_frame.add_extension_widget(self.max_template_distance)
#         self.max_template_distance.hide()
#         self.template_distance.value_line.stateChanged.connect(
#                                             self.set_template_distance)
    def set_template_distance(self):
        if self.template_distance.value_line.isChecked():
            self.max_template_distance.show()
        else:
            self.max_template_distance.hide()
    
    def set_create_rigid_body(self):
        if self.args.create_rigid_body():
            self.rigid_input.hide()
            self.rigid_input.set_required(required=False)
            self.ribfind_cutoff_input.show()
        else:
            self.rigid_input.show()
            self.rigid_input.set_required(required=True)
            self.ribfind_cutoff_input.hide()

    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.  For now show starting, refined
        pdb and experimental map.
        '''
        md_output_dir = os.path.join(self.task.job_location,'1_MD')
        md_output_pdb = os.path.join(self.task.job_location,
                                     '1_MD/final1_mdcg.pdb')
        cg_output_pdb = os.path.join(self.task.job_location,
                                     '1_CG/final1_cg.pdb')
        if os.path.exists(md_output_pdb):
            self.output_pdb = md_output_pdb
        elif os.path.exists(cg_output_pdb):
            self.output_pdb = cg_output_pdb
        
        if self.output_pdb is not None:
            self.launcher.add_file(
                arg_name=None,
                file_type='pdb',
                path=self.output_pdb,
                description='Output PDB',
                display_from='finished',
                selected=True)
        
        for file in os.listdir(md_output_dir):
            if re.search("md[0-9]_[0-9]+.pdb",file) is not None:
                filepath = os.path.join(md_output_dir,file)
                self.launcher.add_file(
                    arg_name=None,
                    file_type='pdb',
                    path=filepath,
                    description='Output from each iteration',
                    display_from='finished',
                    selected=False)
        self.set_rv_ui()

    def set_rv_ui(self):
        '''
        RVAPI results viewer.
        '''
        rv_index = os.path.join(
            self.task.job_location,
            'rvapi_data/index.html')
        if os.path.exists(rv_index):
            self.web_view = QtWebKit.QWebView()
            self.web_view.load(QtCore.QUrl(rv_index))
            self.launcher_dock = QtGui.QDockWidget('Results',
                                                   self,
                                                   QtCore.Qt.Widget)
            self.launcher_dock.setWidget(self.web_view)
            self.tabifyDockWidget(self.pipeline_dock, self.launcher_dock)

def main():
    '''
    Launch stand alone window.
    '''
    if modeller_available:
        '''
        Launch standalone task runner.
        '''
        command_line_launch.ccpem_task_launch(
            task_class=flexem_task.FlexEM,
            window_class=FlexEMWindow)
    else:
        print ('Modeller required and not available'
               '\nFor installation details please see: '
               '\n    https://salilab.org/modeller/')

if __name__ == '__main__':
    main()
