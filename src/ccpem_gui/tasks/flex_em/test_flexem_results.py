import sys
from PyQt4 import QtGui, QtCore, QtWebKit
from ccpem_core.tasks.flex_em import flexem_results

class WebWindow(QtWebKit.QWebView):
    def __init__(self, parent=None):
        super(WebWindow, self).__init__(parent)


def main(verbose=False):
    # Setup viewer
    app = QtGui.QApplication(sys.argv)
    web_window = WebWindow()
    web_window.show()
    # Get example results
    pipeline_path = '/home/tom/test_project/Flex-EM_1/task.ccpem'
    metadata_file = '/home/tom/test_project/Flex-EM_1/1_MD/metadata.json'
    rv = flexem_results.FlexEMResultsViewer(
        pipeline_path=pipeline_path,
        metadata_file=metadata_file)
    if verbose:
        if rv.refine_results is not None:
            print rv.refine_results.fsc_fom_table.data_frame.to_string()
            rv.fsc_hm1hm2.print_summary()
    # Load index.html
    web_window.load(QtCore.QUrl(rv.index))
    app.exec_()

if __name__ == '__main__':
    main()
