#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
from src.ccpem_progs.ribfind import run_ribfind

'''
TemPy task
'''

import sys
import os
import collections
import operator
from ccpem_core.ccpem_utils import ccpem_argparser
from PyQt4 import QtCore
from textwrap import dedent
from ccpem_core.gui_ext import job_manager
from ccpem_core.gui_ext.job_manager import job_wrapper
from ccpem_core.tasks import task_utils
from ccpem_core import ccpem_utils
from ccpem_core.ccpem_utils import ccpem_widgets
import tempy_window 
import ccpem_core.gui_ext as window_utils
from ccpem_progs.tempy import MapParser
from ccpem_progs.tempy import StructureParser
from ccpem_progs.tempy.RigidBodyParser import RBParser
from ccpem_progs.tempy.ScoringFunctions import ScoringFunctions
from ccpem_progs.tempy.StructureBlurrer import StructureBlurrer
from ccpem_progs.dssp import dssp
from ccpem_progs.tempy.ShowPlot import Plot
from ccpem_progs.tempy import EnsembleGeneration

def tempy_parser():
    parser = ccpem_argparser.ccpemArgParser(
             fromfile_prefix_chars='@',
             usage=dedent('''
TEMPy is an object-oriented Python library designed to help the user in the manipulation 
and analysis of macromolecular assemblies, especially in the context of 3D electron microscopy 
density maps. It is designed with a set of functionalities that assess the goodness-of-fit 
between a given atomic model and a density map or between two maps using a variety of 
different scoring functions. It can also generate various ensembles of alternative fits, 

             '''),
             description='TEMPy options',
             epilog=dedent('''
To import parameters from argument text file use e.g. :\
python class_selections.py @my_args.txt
             '''))
    map1 = parser.add_argument_group()
    map1.add_argument('-map1',
        '--map1',
        help='''
One of the density map for assessing the goodness-of-fit
        ''',
        metavar='Input Map 1',
        type=str,
        default='MRC')
    #
    map2 = parser.add_argument_group()
    map2.add_argument('-map2',
        '--map2',
        help='''
Other density map for assessing the goodness-of-fit
        ''',
        metavar='Input Map 2',
        type=str,
        default='MRC')
    #
    search_object = parser.add_argument_group()
    search_object.add_argument('-pdb',
        '--atomic_model',
        help='atomic model',
        metavar='Atomic Model',
        type=str,
        default='PDB')
    #
    map1_contour = parser.add_argument_group()
    map1_contour.add_argument('-contour1',
        '--map1_contour',
        help='Threshold level of the first input map (default is 1sigma)',
        metavar='First map Contour',
        type=float,
        default=None)
    #
    map1_resolution = parser.add_argument_group()
    map1_resolution.add_argument('-res1',
        '--map1_resolution',
        help='Resolution of the first input map (Optional)',
        metavar='First map Resolution',
        type=float,
        default=None)
    #
    map2_contour = parser.add_argument_group()
    map2_contour.add_argument('-contour2',
        '--map2_contour',
        help='Threshold level of the second input map (default is 1sigma)',
        metavar='Second map Contour',
        type=float,
        default=None)
    #
    map2_resolution = parser.add_argument_group()
    map2_resolution.add_argument('-res2',
        '--map2_resolution',
        help='Resolution of the second input map (Optional)',
        metavar='Second map Resolution',
        type=float,
        default=None)
    #
    rigid_body_input = parser.add_argument_group()
    rigid_body_input.add_argument('-rbfile',
        '--rigid_body_file',
        help='''
        Rigid body file(Optional). If not provided its generated using RIBFIND
        (Pandurangan, A.P., Topf, M., 2012).
        ''',
        metavar='Rigid body file',
        type=float,
        default=None)
    #
    ensemble_num_struct = parser.add_argument_group()
    ensemble_num_struct.add_argument('-numr',
        '--no_of_structs_random',
        help='''
        Number of structures to output(int)
        ''',
        metavar='Structures to generate',
        type=int,
        default=10)
    #
    ensemble_max_trans = parser.add_argument_group()
    ensemble_max_trans.add_argument('-trans',
        '--max_trans',
        help='''
        Maximum translation permitted (in Angstroms)
        ''',
        metavar='Maximum translation',
        type=int,
        default=5)
    #
    ensemble_max_rot = parser.add_argument_group()
    ensemble_max_rot.add_argument('-rot',
        '--max_rot',
        help='Maximum rotation permitted (in degrees)',
        metavar='Maximum rotation',
        type=int,
        default=90)
    #
    angular_num_struct = parser.add_argument_group()
    angular_num_struct.add_argument('-numa',
        '--no_of_structs_angular',
        help='''
        Number of structures to output(int)
        ''',
        metavar='Structures to generate',
        type=int,
        default=10)
    #
    angular_trans_vec1 = parser.add_argument_group()
    angular_trans_vec1.add_argument('-vec1',
        '--trans_vec1',
        help='''
        Vector for translation
        ''',
        metavar='Translation Vector (X)',
        type=float,
        default=None)
    #
    angular_trans_vec2 = parser.add_argument_group()
    angular_trans_vec2.add_argument('-vec2',
        '--trans_vec2',
        help='''
        Vector for translation
        ''',
        metavar='Translation Vector (Y)',
        type=float,
        default=None)
    #
    angular_trans_vec3 = parser.add_argument_group()
    angular_trans_vec3.add_argument('-vec3',
        '--trans_vec3',
        help='''
        Vector for translation
        ''',
        metavar='Translation Vector (Z)',
        type=float,
        default=None)
    #
    angular_trans_axis1 = parser.add_argument_group()
    angular_trans_axis1.add_argument('-axis1',
        '--trans_axis1',
        help='''
        Axis for translation
        ''',
        metavar='Translation Axis (X)',
        type=float,
        default=None)
    #
    angular_trans_axis2 = parser.add_argument_group()
    angular_trans_axis2.add_argument('-axis2',
        '--trans_axis2',
        help='''
        Axis for translation
        ''',
        metavar='Translation Axis (Y)',
        type=float,
        default=None)
    #
    angular_trans_axis3 = parser.add_argument_group()
    angular_trans_axis3.add_argument('-axis3',
        '--trans_axis3',
        help='''
        Axis for translation
        ''',
        metavar='Translation Axis (Z)',
        type=float,
        default=None)
    #
    angular_rot_angle = parser.add_argument_group()
    angular_rot_angle.add_argument('-rota',
        '--rot_ang',
        help='Rotation angle for local rotation (degrees)',
        metavar='Rotation Angle',
        type=int,
        default=100)
    
    return parser

class ProgramInfo(object):
    '''
    Store program information for database.
    '''
    def __init__(self, database_path=None):
        self.program_id = 'TEMPy' 
        self.jobname = 'TEMPy'
        self.version = ''
        self.description = '''
Welcome to TEMPy!
TEMpy is an object-oriented Python library designed to help the user in the 
manipulation and analysis of macromolecular assemblies, especially in the 
context of 3D electron microscopy density maps. It is designed with a set of 
functionalities that assess the goodness-of-fit between a given atomic model 
and a density map or between two maps using a variety of different scoring functions. 
It can also generate various ensembles of alternative fits.
'''
        self.author = 'Maya Topf, Daven Vasishtan, Arun Prasad Pandurangan, \
                    Irene Farabella, Agnel-Praveen Joseph, Harpal Sahota'
        self.module = os.path.realpath(__file__)
        self.database_path = database_path

def launch_gui(parent,
               database_path,
               args=None,
               job_manager=None):
    # Add program info to database
    if database_path is not None:
            program_info = ProgramInfo()
            task_utils.add_program_info_to_database(database_path,
                                                    program_info)
    window = tempy_window.TemPyMainWindow(
                       parent=parent,
                       args=args,
                       job_manager=job_manager)
    window.setGeometry(parent.geometry().x()+50,
                       parent.geometry().x()+50,
                       window.geometry().width(),
                       window.geometry().height())
    window.show()

class RunTask(object):
    '''
    Run task.
    '''
    def __init__(self,
                 job_id,
                 job_location,
                 database_path,
                 flag=None,
                 args=None,
                 parent=None,
                 ccc=False,
                 mi=False,
                 sccc=False,
                 env=False,
                 nv=False,
                 score=False,
                 ensemble=False,
                 random=False,
                 angular=False):
        
        self.flag = flag
        self.ccc = ccc
        self.mi = mi
        self.sccc = sccc
        self.env = env
        self.nv = nv
        self.score = score
        self.ensemble = ensemble
        self.random = random
        self.angular = angular
        self.job_id = job_id
        self.job_location = job_location
        self.database_path = database_path
        self.args = args
        self.parent = parent
    
        if self.args is None:
            self.args = tempy_parser().generate_arguments()
        
        self.map1_in = self.args.map1.value
        self.map2_in = self.args.map2.value
        self.pdb_in = self.args.atomic_model.value
        
        # Check Input files exist

        if self.score:
            assert os.path.exists(self.map1_in)
            if self.flag == 'mrc':
                assert os.path.exists(self.map2_in)
            
            elif self.flag == 'pdb':
                assert os.path.exists(self.pdb_in)
            else:
                print 'Check input files'
        
        
        if self.ensemble:
            assert os.path.exists(self.pdb_in)
        
        # Save arguments

        self.args.output_args_as_json(self.job_location + '/args.json')
        
        #Run task process
        self.run_detached()
 

    def run_detached(self):
        '''
        Convert args to map/structure instances
        '''
        if self.score:
            self.logfile_scores = open('tempy_scores.log', 'w')
            self.emmap1 = MapParser.MapParser.readMRC(self.args.map1.value)
        
        if self.flag == 'mrc':
            self.emmap2 = MapParser.MapParser.readMRC(self.args.map2.value)
            self.check_grid(self.emmap1,self.emmap2)
            
        if self.flag == 'pdb':
            self.pdb =  StructureParser.PDBParser.read_PDB_file(
                                                    'pdb',
                                                    self.args.atomic_model.value,
                                                    hetatm=False,
                                                    water=False)
            if self.score:
                self.blur_pdb()
    
            if self.ensemble:
                self.generate_ensemble(self.pdb)
        
                   
    
    def blur_pdb(self):
        '''
        If pdb file uploaded simulates a map at taret map resolution
        '''
        print 'Bluring the PDB to map1 resolution'
        self.sim_sigma_coeff = 0.187 #default value in tempy taken
        blurrer = StructureBlurrer()
        assert self.args.map1_resolution.value is not None
        self.pdb_name = self.args.atomic_model.value.split('/')[-1].split('.')[0]
        self.pdb2mrc =  (self.job_location 
                            + '/' 
                            + self.pdb_name 
                            + '_simulated_' 
                            + str(self.args.map1_resolution.value) 
                            + '.mrc')
        self.sim_map = blurrer.gaussian_blur(self.pdb,
                                        self.args.map1_resolution.value,
                                        densMap=self.emmap1,
                                        sigma_coeff=self.sim_sigma_coeff)
        self.sim_map.write_to_MRC_file(self.pdb2mrc)
        print 'Simulated map is: ',self.pdb2mrc
        #Checks the grid
        self.check_grid(self.emmap1,self.sim_map)
        
        if self.score and self.ensemble==False:
            self.run_map_scores(self.map_1, self.map_2)
    
    def check_grid(self,
                   map1=None,
                   map2=None):
        '''
        Checks for the alignment box and interpolates to a new grid
        '''
        print 'Checking grid now'
        #Determine a common alignment box
        spacing = map2.apix
        if map2.apix < map1.apix: spacing = map1.apix
        grid_shape, new_ori = map1._alignment_box(map2,spacing)
        
        #Interpolate to new grid
        self.map_1 = map1._interpolate_to_grid(grid_shape,spacing,new_ori)
        self.map_2 = map2._interpolate_to_grid(grid_shape,spacing,new_ori)
        print 'Grid  check end'
    
    def run_map_scores(self,
                       map1=None,
                       map2=None):
        if self.score:
            print 'You selected to calculate scores:'
            sc = ScoringFunctions()
            if self.ccc:
                print 'Calculating CCC score now'
                ccc_score = sc.CCC(map1,map2)
                self.logfile_scores.write('CCC score is :' 
                                          + str(ccc_score)
                                          + '\n')
                print 'Finished calculating CCC score'
            if self.mi:
                print 'Calculating Mutual information score now'
                mi_score = sc.MI(map1,map2)
                self.logfile_scores.write('Mutual information score is :' 
                                          + str(mi_score)
                                          + '\n')
                print 'Finished calculating Mutual information score'
            if self.env:
                print 'Calculating Envelope score now'
                env_score = sc.envelope_score_map(map1,map2)
                self.logfile_scores.write('Envelope score is :'
                                          + str(env_score)
                                          + '\n')
                print 'Finished calculating Envelope score score'
            if self.nv: 
                print 'Calculating Normal Vector score now'
                points = round((self.map_1.map_size())*0.01)
                first_bound = self.args.map1_contour.value
                second_bound = self.map_1.get_second_boundary(first_bound, 
                                                              points, 
                                                              first_bound, 
                                                              self.map_1.max(),
                                                              err_percent=1)
                nv_score = sc.normal_vector_score(map_target=map1,
                                                  map_probe=map2, 
                                                  primary_boundary=first_bound, 
                                                  secondary_boundary=second_bound)
                self.logfile_scores.write('Normal vector score is :'
                                          + str(nv_score)
                                          + '\n')
                print 'Finished calculating Normal Vector score'
                 
            if self.sccc:
                print 'Calculating SCCC score now'
                if self.args.rigid_body_file.value == None:
                    self.run_dssp()
                else:
                    self.calculate_sccc(self.args.rigid_body_file.value,self.pdb)
             
    def run_dssp(self):
        '''
        Runs DSSP for given pdbfile, which is used as an input for ribfind
        '''
        print 'No rigid body file given as input. Now running DSSP...'
        self.dssp_filename =  (self.job_location 
                                + '/' 
                                + self.pdb_name 
                                + '.dssp')
        rd = dssp.RunDSSP(pdb_filename=self.args.atomic_model.value, 
                                  dssp_filename=self.dssp_filename)
        rd.run_dssp()
        self.run_ribfind()
    
    def run_ribfind(self):
        '''
        Runs ribfind on a given pdbfile and then selects the clusterfile with 
        max weighted number of clusters
        '''            
        print 'Now running ribfind'
        params = {'pdb_filename' : self.args.atomic_model.value,
                  'dssp_filename' : self.dssp_filename,
                  'contact_distance' : 6.5,
                  'output_path' : self.job_location}
        rb = run_ribfind.main(params=params)
        clustlogfile = self.job_location + '/' + self.pdb_name + '.clustlog'
        if os.path.exists(clustlogfile):
            dict_clusters = collections.OrderedDict()
            with open(clustlogfile) as clust:
                for line in clust:
                    if line[0].isdigit():
                        (cluster_cutoff, weighted_clusters) = (line.split()[0], 
                                                               line.split()[4])
                        dict_clusters[int(cluster_cutoff.split('.')[0])] = weighted_clusters
            #Get the cluster cut off for maximum weighted no of cluster
            max_weighted_cluster_cutoff = max(dict_clusters.iteritems(), 
                                              key=operator.itemgetter(1))[0]
            self.rbfile = (self.job_location 
                      + '/' + self.pdb_name 
                      + '.denclust_' 
                      +  str(max_weighted_cluster_cutoff))
            self.args.rigid_body_file.value = self.rbfile
        if  self.args.rigid_body_file.value != None:
            self.calculate_sccc(self.rbfile, self.pdb)
        
    def calculate_sccc(self,rbfile,structure_instance):
        '''
        Calculates SCCC  for each rigid body file
        '''
        if self.sccc:
            SCCC_list_structure_instance = []
            listRB = RBParser.read_FlexEM_RIBFIND_files(rbfile,structure_instance)
            sc = ScoringFunctions()
            self.logfile_scores.write('SCCC Score for individual rigid bodies' 
                                      + '\n')
            self.logfile_scores.write('Rigid body file used is: ' 
                                      + rbfile 
                                      + '\n')
            self.logfile_scores.write('PDB file used is: ' 
                                      + self.args.atomic_model.value
                                      + '\n')
            rb_number = 1
            for RB in listRB:
                score_SCCC=sc.SCCC(self.emmap1,
                                   self.args.map1_resolution.value,
                                   self.sim_sigma_coeff,
                                   structure_instance,
                                   RB)
                SCCC_list_structure_instance.append(score_SCCC)
                self.logfile_scores.write('SCCC Score for Rigid Body:' 
                                          + str(rb_number) 
                                          + ' is ' 
                                          +str(score_SCCC))
                rb_number += 1
        print 'Finished calculating Normal Vector score'
        #listRB = RBParser.RBfileToRBlist(rbfile)
        #Plot.PrintOutChimeraAttributeFileSCCC_Score('3MFP',SCCC_list_structure_instance,listRB)
    
    def generate_ensemble(self,pdbinstance):
        '''
        Can either generate a random or angular sweep ensemble and if map1 is 
        also uploaded it gives CCC score
        '''
        if self.ensemble:
            print 'You selected to make an ensemble of structures'
            if self.random:
                print 'Generating a Random ensemble'
                ec = EnsembleGeneration.EnsembleGeneration()
                self.ensemble_list = ec.randomise_structs(
                        structure_instance=pdbinstance,
                        no_of_structs=self.args.no_of_structs_random.value,
                        max_trans=self.args.max_trans.value,
                        max_rot=self.args.max_rot.value,
                        write=True)
                
                if os.path.exists(self.map1_in):
                    self.score_ensemble(pdbinstance=pdbinstance,
                                        ensemble_list=self.ensemble_list,
                                        output_file='ensemble_random_scores.log')
                else:
                    print 'No CCC scores calculated for ensemble as no map input'    
                print 'Finished generating random ensemble'
            
            if self.angular:
                print 'Generating angular sweep ensemble'  
                translation_vector = [self.args.trans_vec1.value,
                                      self.args.trans_vec2.value,
                                      self.args.trans_vec3.value
                                      ]
                axis = [self.args.trans_axis1.value,
                        self.args.trans_axis2.value,
                        self.args.trans_axis3.value
                        ]
                ec = EnsembleGeneration.EnsembleGeneration()
                ang_ensemble =  ec.anglar_sweep(
                        structure_instance=pdbinstance,
                        axis=axis,
                        translation_vector=translation_vector,
                        no_of_structs=self.args.no_of_structs_angular.value,
                        loc_rotation_angle=self.args.rot_ang.value,
                        write=True)
                if os.path.exists(self.map1_in):
                    self.score_ensemble(pdbinstance=pdbinstance,
                                        ensemble_list=ang_ensemble,
                                        output_file='ensemble_angular_scores.log')
                else:
                    print 'No CCC scores calculated for ensemble as no map input'   
                print 'Finished generating angular sweep ensemble'
    
    def score_ensemble(self,pdbinstance,ensemble_list,output_file):
        print 'Scoring the ensemble'
        self.ensemble_scores = open(output_file, 'w')
        sc = ScoringFunctions()
        blurrer = StructureBlurrer()
        self.emmap1 = MapParser.MapParser.readMRC(self.args.map1.value)
        pdb_map = blurrer.gaussian_blur(pdbinstance, 
                                        self.args.map1_resolution.value,
                                        densMap=self.emmap1,
                                        sigma_coeff=0.187)
        self.check_grid(self.emmap1, pdb_map)
        self.ensemble_scores.write('*CCC scores for the ensemble generated*')
        self.ensemble_scores.write(str('CCC score PDB uploaded') 
                                   + ' : '
                                   + str(round(sc.CCC(self.map_1,self.map_2),3))
                                   + '\n')
        for fit in ensemble_list:
            fit_name = fit[0]
            print 'Scoring the structure ', fit_name
            fit_structure_instance = fit[1]
            map_probe = blurrer.gaussian_blur(fit_structure_instance, 
                                            self.args.map1_resolution.value,
                                            densMap=self.emmap1,
                                            sigma_coeff=0.187)
            self.check_grid(self.emmap1, map_probe)
            self.ensemble_scores.write(str(fit_name)
                                       + ' : '
                                       + str(round(sc.CCC(self.map_1,self.map_2,3)))
                                       + '\n')
                    
        print 'Finished scoring the ensemble'
        
        