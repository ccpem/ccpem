#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


'''
Task window for TemPy program.
'''

import sys
import os
import time
import shutil
from os.path import exists
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import SIGNAL, SLOT
import ccpem_core.gui_ext as window_utils
from ccpem_core.mrc_map_io_clipper import read_mrc_header
from ccpem_core.ccpem_utils import ccpem_widgets
import ccpem_core.gui_ext as text_browser
from ccpem_core import ccpem_utils
import ccpem_core.gui_ext as icons
from ccpem_core.gui_ext.job_manager import job_wrapper
from ccpem_utils.spherical_angles_distribution import angle_distn
from ccpem_utils import which
from Bio.PDB import PDBParser
from Bio.PDB import Selection
import tempy

class TemPyMainWindow(window_utils.CCPEMTaskWindow):
    '''
    Inherits from CCPEMTaskWindow.
    '''
    def __init__(self, parent=None,
                       database_path=None,
                       args=None,
                       job_manager=False):
        super(TemPyMainWindow, self).__init__(parent)
        self.parent = parent
        self.database_path = database_path
        self.args = args
        self.job_manager = job_manager
        self.set_program_info(prog_info=tempy.ProgramInfo())
        self.set_args()
        avail_geo = QtGui.QDesktopWidget().availableGeometry(self).size()
        self.resize(QtCore.QSize(avail_geo.width() * 0.35,
                                 avail_geo.height() * 0.7)) 
        
    def set_setup_ui(self):
        '''
        Setup UI for input arguments.
        '''
        self.setup_ui = QtGui.QWidget()
        self.setup_ui_vbox_layout = QtGui.QVBoxLayout()
        # Add program information
        self.prog_label = QtGui.QLabel()
        self.prog_label.setWordWrap(True)
        self.prog_label.setFrameStyle(QtGui.QFrame.StyledPanel | 
                                      QtGui.QFrame.Sunken)
        self.setup_ui_vbox_layout.addWidget(self.prog_label)

        # Add args dialog
        self.args_widget = window_utils.CCPEMArgsDialog()
        self.setup_ui_vbox_layout.addWidget(self.args_widget)
        self.setup_ui.setLayout(self.setup_ui_vbox_layout)
        
        return self.setup_ui    
        
    def set_args(self):
        '''
        Set input arguments: stage1
        '''
        from ccpem_core.tasks.tempy.tempy import tempy_parser
        if self.args is None:
            self.args = tempy_parser().generate_arguments()
        
        # Input target mrc
        self.map1_input = window_utils.FileArgInput(
            parent=self,
            arg_name='map1',
            args=self.args,
            tooltip_text= 'Input first map',
            path_remember=True,
            file_type='MRC(*.mrc *.map *.ccp4)')
        self.args_widget.args_layout.addWidget(self.map1_input)
        filename=self.args.map1.value  
        self.map1_input.input_file_edit.textChanged.connect(self.set_contour_map1)
        self.map1_input.input_file_edit.textChanged.connect(
            lambda: self.map_header.header_values(
                        str(self.map1_input.input_file_edit.text())))
        
        #Input contour levels and resolution for the input maps
        hbox_map1 = QtGui.QHBoxLayout()
        self.map1_contour = window_utils.SimpleArgInput(
            parent=self,
            arg_name='map1_contour',
            args=self.args,
            width=200)
        hbox_map1.addWidget(self.map1_contour)
        self.map1_resolution = window_utils.SimpleArgInput(
            parent=self,
            arg_name='map1_resolution',
            args=self.args,
            width=200)  
        hbox_map1.addWidget(self.map1_resolution)
        self.args_widget.args_layout.addLayout(hbox_map1)
        #Input Map header details
        self.header_button=QtGui.QCheckBox("Show Map 1 Header Information")
        self.header_button.setCheckable(True)
        self.header_button.setFixedWidth(300)  
        self.args_widget.args_layout.addWidget(self.header_button) 
        self.map_header=window_utils.HeaderInfo(filename)
        self.HeaderFrame=self.map_header.HeaderFrame
        self.line=self.map_header.line
        self.args_widget.args_layout.addWidget(self.HeaderFrame)
        self.args_widget.args_layout.addWidget(self.line)
        self.args_widget.args_layout.addStretch()
        self.header_button.toggled.connect(self.HeaderFrame.setVisible) 
        
        self.selection_text = QtGui.QLabel('Do you have a map or atomic model?')
        self.args_widget.args_layout.addWidget(self.selection_text)
        self.mrc_bt = QtGui.QRadioButton('Density map')
        self.pdb_bt = QtGui.QRadioButton('Atomic model')
        self.mrc_bt.setAutoExclusive(True)
        self.pdb_bt.setAutoExclusive(True)
        self.mrc_bt.setChecked(True)
        self.args_widget.args_layout.addWidget(self.mrc_bt)
        self.args_widget.args_layout.addWidget(self.pdb_bt)
        self.mrc_bt.toggled.connect(self.mrc_clicked)
        self.pdb_bt.toggled.connect(self.pdb_clicked)

        # Input search density and its header
        self.map2_input = window_utils.FileArgInput(
            parent=self,
            arg_name='map2',
            args=self.args,
            tooltip_text= 'Input second map',
            path_remember=True,
            file_type='MRC(*.mrc *.map *.ccp4)')
        self.args_widget.args_layout.addWidget(self.map2_input)
        sfilename = self.args.map2.value 
        self.map2_input.input_file_edit.textChanged.connect(
            lambda: self.smap_header.header_values(
                        str(self.map2_input.input_file_edit.text())))
        self.map2_input.input_file_edit.textChanged.connect(self.set_contour_map2)
        #Input contour levels and resolution for the input maps
        hbox_map2 = QtGui.QHBoxLayout()
        self.map2_contour = window_utils.SimpleArgInput(
            parent=self,
            arg_name='map2_contour',
            args=self.args,
            width=200)
        hbox_map2.addWidget(self.map2_contour)
        self.map2_resolution = window_utils.SimpleArgInput(
            parent=self,
            arg_name='map2_resolution',
            args=self.args,
            width=200)  
        hbox_map2.addWidget(self.map2_resolution)
        self.args_widget.args_layout.addLayout(hbox_map2)
        
        #Search Map header details
        self.sheader_button=QtGui.QCheckBox("Show Map 2 Header Information")
        self.sheader_button.setCheckable(True)
        self.sheader_button.setFixedWidth(300)  
        self.args_widget.args_layout.addWidget(self.sheader_button) 
        self.smap_header=window_utils.HeaderInfo(sfilename)
        self.sHeaderFrame=self.smap_header.HeaderFrame
        self.sLine=self.smap_header.line
        self.args_widget.args_layout.addWidget(self.sHeaderFrame)
        self.args_widget.args_layout.addWidget(self.sLine)
        self.args_widget.args_layout.addStretch()
        self.sheader_button.toggled.connect(self.sHeaderFrame.setVisible) 
        
        #By default its selected as map vs map scoring
        self.flag = 'mrc'
        
        # Input search object and pdb header
        self.atomic_model_input = window_utils.FileArgInput(
            parent=self,
            arg_name='atomic_model',
            args=self.args,
            tooltip_text= 'Atomic model used for scoring',
            path_remember=True,
            file_type='PDB(*.pdb)')
        self.args_widget.args_layout.addWidget(self.atomic_model_input)
        self.atomic_model_input.hide()
        #sets up names for filtered map, search object and mask filenames
        self.pdbheader_button=QtGui.QCheckBox("Show atomic model Header Information")
        self.pdbheader_button.setCheckable(True)
        self.pdbheader_button.setFixedWidth(300)  
        self.args_widget.args_layout.addWidget(self.pdbheader_button) 
        self.pdbheader_button.hide()
        self.pdbheader_gridlayout = QtGui.QGridLayout()
        self.pdbheaderFrame = QtGui.QFrame()
        self.pdbheaderFrame.setFrameStyle(QtGui.QFrame.Plain |QtGui.QFrame.Box)
        pdbheader_labels = ['PDB Resolution', 'Number of Atoms', 'Number of Residues']
        ncol = 0
        for label in pdbheader_labels:
            self.pdbheader_gridlayout.addWidget(QtGui.QLabel(label), 0, ncol)
            ncol +=2
        self.pdbheaderFrame.setLayout(self.pdbheader_gridlayout)
        self.args_widget.args_layout.addWidget(self.pdbheaderFrame)
        self.pdbheaderFrame.setVisible(False)
        self.pdbheader_button.toggled.connect(self.pdb_header)
        self.pdbheader_button.toggled.connect(self.pdbheaderFrame.setVisible) 
        
        #Rigid body file upload
        self.rigid_body_file = window_utils.FileArgInput(
            parent=self,
            arg_name='rigid_body_file',
            args=self.args,
            tooltip_text= '''
        Rigid body file(Optional). If not provided its generated using RIBFIND
        (Pandurangan, A.P., Topf, M., 2012).
                          ''',
            path_remember=True,
            file_type='')
        self.args_widget.args_layout.addWidget(self.rigid_body_file)
        self.rigid_body_file.hide()
        
        #Button to view input files with Chimera
        hbox_chimera = QtGui.QHBoxLayout()
        hbox_chimera.addStretch(1)
        self.check_button = window_utils.ButtonWithLabel(
                    parent=self,
                    label='Check input files in Chimera',
                    width=200,
                    tooltip_text='Visualise files in Chimera')
        hbox_chimera.addWidget(self.check_button)
        
        #hbox to keep the view button on the right
        self.args_widget.args_layout.addLayout(hbox_chimera)
        self.check_button.SimpleButton.clicked.connect(self.show_chimera)
        
        #Scores to select, add score labels and their text
        select_label = 'Select the score(s) you wish to calculate:'
        self.score_bt = QtGui.QCheckBox(select_label)
        self.args_widget.args_layout.addWidget(self.score_bt)
        self.score_gridlayout = QtGui.QGridLayout()
        self.scoreFrame = QtGui.QFrame()
        self.scoreFrame.setFrameStyle(QtGui.QFrame.Plain | QtGui.QFrame.Box)
        self.scoreFrame.hide()
        self.score_bt.toggled.connect(self.scoreFrame.setVisible)
        
        #Add CCC button
        self.ccc_bt = QtGui.QCheckBox('Cross- correlation (CCC)')
        self.ccc_tooltip = '''
        Calculates cross-correlation between two Map instances. Score ranges 
        from -1 to 1. Higher the score better it is.
        '''
        self.ccc_bt.setToolTip(self.ccc_tooltip)
        self.score_gridlayout.addWidget(self.ccc_bt)
        self.ccc_bt.setChecked(True)
        
        #Add MI Button
        self.mi_bt = QtGui.QCheckBox('Mutual information (MI)')
        self.mi_tooltip = '''
        Calculates the mutual information score between two Map instances. Score 
        ranges from 0 to positive value. Higher the score better it is.
        '''
        self.mi_bt.setToolTip(self.mi_tooltip)
        self.score_gridlayout.addWidget(self.mi_bt)
        self.mi_bt.setChecked(True)
        
        #Add Sccc Button
        self.sccc_bt = QtGui.QCheckBox('Segment based cross-correlation (SCCC)')
        self.sccc_tooltip = '''
        Calculates cross-correlation between two Map instances. Score ranges 
        from -1 to 1. Higher the score better it is.
        '''
        self.sccc_bt.setToolTip(self.sccc_tooltip)
        self.score_gridlayout.addWidget(self.sccc_bt)
        #sccc set as disabled, if pdb uploaded it will be enabled
        self.sccc_bt.setDisabled(True)
        
        #Add ENV Button
        self.env_bt = QtGui.QCheckBox('Envelope score')
        self.env_tooltip = '''
        Calculate the envelope score between a target Map and Structure Instance
        '''
        self.env_bt.setToolTip(self.env_tooltip)
        self.score_gridlayout.addWidget(self.env_bt)
        self.env_bt.setChecked(True)
        
        #Add NV Button
        self.nv_bt = QtGui.QCheckBox('Normal Vector score')
        self.nv_tooltip = '''
        Calculate the Normal Vector Score between two Map instances. The score 
        ranges from 0 to pi (0 is the best). Using too small (0.187*resolution)
        or large (0.5*resolution) sigma values to produce a probe map might 
        disrupt the accuracy.
        '''
        self.nv_bt.setToolTip(self.nv_tooltip)
        self.score_gridlayout.addWidget(self.nv_bt)
        self.nv_bt.setChecked(True)
        
        # Add scores to layout
        self.scoreFrame.setLayout(self.score_gridlayout)
        self.args_widget.args_layout.addWidget(self.scoreFrame)
        
        #Add ensemble
        ensemble_label = 'Select the type of ensemble you wish to generate:'
        self.ensemble_bt = QtGui.QCheckBox(ensemble_label)
        self.ensemble_tooltip = ''''
        It is advisable to chose the number of structures for the ensemble 
        accordingly with the angular increment step (rotation angle/number 
        of structures) and/or the translational increment step (translation 
        vector/number of structures) to have a more homogeneous ensemble.
         '''
        self.ensemble_bt.setToolTip(self.ensemble_tooltip)
        self.args_widget.args_layout.addWidget(self.ensemble_bt)
        self.ensemble_bt.setDisabled(True)
        self.ensemble_gridlayout = QtGui.QGridLayout()
        self.ensembleFrame = QtGui.QFrame()
        self.ensembleFrame.setFrameStyle(QtGui.QFrame.Plain | QtGui.QFrame.Box)
        self.ensembleFrame.hide()
        self.ensemble_bt.toggled.connect(self.ensembleFrame.setVisible)
        
        #Add Random ensemble button
        self.random_bt = QtGui.QCheckBox('Random Ensemble')
        self.random_tooltip = '''
        Generates an ensemble of structures rotated within given angles and 
        translated within given distances
        '''
        self.random_bt.setToolTip(self.random_tooltip)
        self.ensemble_gridlayout.addWidget(self.random_bt)
        
        self.structures_random = window_utils.SimpleArgInput(
                                            parent=self,
                                            arg_name='no_of_structs_random',
                                            args=self.args,
                                            width=200)
        self.ensemble_gridlayout.addWidget(self.structures_random,1,0)
        self.structures_random.hide()
        
        self.translation = window_utils.SimpleArgInput(
                                            parent=self,
                                            arg_name='max_trans',
                                            args=self.args,
                                            width=200)
        self.ensemble_gridlayout.addWidget(self.translation,1,1)
        self.translation.hide()
        
        self.rotation_random = window_utils.SimpleArgInput(
                                            parent=self,
                                            arg_name='max_rot',
                                            args=self.args,
                                            width=200)
        self.ensemble_gridlayout.addWidget(self.rotation_random,2,0)
        self.rotation_random.hide()
        
        self.random_bt.toggled.connect(self.structures_random.setVisible)
        self.random_bt.toggled.connect(self.translation.setVisible)
        self.random_bt.toggled.connect(self.rotation_random.setVisible)
        
        
        #Add Angular sweep ensemble Button
        self.angular_bt = QtGui.QCheckBox('Angular Sweep Ensemble')
        self.angular_tooltip = '''
        Generates an ensemble of structures using angular sweeps with a given 
        rotation angle around a specified rotation axis using a translation vector.
        Chose the number of structures for the ensemble accordingly with the 
        angular increment step (loc_rotation_angle/no_of_structs) and
        translational increment step (translation_vector/no_of_structs) required. 
        Default setting is around the center of mass.
        '''
        self.angular_bt.setToolTip(self.angular_tooltip)
        self.ensemble_gridlayout.addWidget(self.angular_bt,3,0)
        
        self.structures_angular = window_utils.SimpleArgInput(
                                            parent=self,
                                            arg_name='no_of_structs_angular',
                                            args=self.args,
                                            width=200)
        self.ensemble_gridlayout.addWidget(self.structures_angular,4,0)
        self.structures_angular.hide()
        
        self.rotation_angle = window_utils.SimpleArgInput(
                                            parent=self,
                                            arg_name='rot_ang',
                                            args=self.args,
                                            width=200)
        self.ensemble_gridlayout.addWidget(self.rotation_angle,4,1)
        self.rotation_angle.hide()
        
        self.translation_vec1 = window_utils.SimpleArgInput(
                                            parent=self,
                                            arg_name='trans_vec1',
                                            args=self.args,
                                            width=200)
        self.ensemble_gridlayout.addWidget(self.translation_vec1,5,0)
        self.translation_vec1.hide()
        
        self.translation_vec2 = window_utils.SimpleArgInput(
                                            parent=self,
                                            arg_name='trans_vec2',
                                            args=self.args,
                                            width=200)
        self.ensemble_gridlayout.addWidget(self.translation_vec2,5,1)
        self.translation_vec2.hide()
        
        self.translation_vec3 = window_utils.SimpleArgInput(
                                            parent=self,
                                            arg_name='trans_vec3',
                                            args=self.args,
                                            width=200)
        self.ensemble_gridlayout.addWidget(self.translation_vec3,5,2)
        self.translation_vec3.hide()
        
        self.translation_axis1 = window_utils.SimpleArgInput(
                                            parent=self,
                                            arg_name='trans_axis1',
                                            args=self.args,
                                            width=200)
        self.ensemble_gridlayout.addWidget(self.translation_axis1,6,0)
        self.translation_axis1.hide()
        
        self.translation_axis2 = window_utils.SimpleArgInput(
                                            parent=self,
                                            arg_name='trans_axis2',
                                            args=self.args,
                                            width=200)
        self.ensemble_gridlayout.addWidget(self.translation_axis2,6,1)
        self.translation_axis2.hide()
        
        self.translation_axis3 = window_utils.SimpleArgInput(
                                            parent=self,
                                            arg_name='trans_axis3',
                                            args=self.args,
                                            width=200)
        self.ensemble_gridlayout.addWidget(self.translation_axis3,6,2)
        self.translation_axis3.hide()
        
        
        self.random_bt.toggled.connect(self.structures_random.setVisible)
        self.random_bt.toggled.connect(self.translation.setVisible)
        self.random_bt.toggled.connect(self.rotation_random.setVisible)
        
        list_to_show = [self.structures_angular,
                        self.rotation_angle,
                        self.translation_vec1,
                        self.translation_vec2,
                        self.translation_vec3,
                        self.translation_axis1,
                        self.translation_axis2,
                        self.translation_axis3
                        ]
        
        for widget in list_to_show:
            self.angular_bt.toggled.connect(widget.setVisible)

        
        self.ensembleFrame.setLayout(self.ensemble_gridlayout)
        self.args_widget.args_layout.addWidget(self.ensembleFrame)
        #to store the path of current file before changing directory to job_location
        self.gui_path = os.path.realpath(__file__)
        
    def mrc_clicked(self):
        '''
        Keeps the flag to mrc if second mapfile uploaded and display options
        '''
        self.flag = 'mrc'
        self.atomic_model_input.hide()
        self.pdbheader_button.hide()
        self.rigid_body_file.hide()
        self.map2_input.show()
        self.sheader_button.show() 
        self.map2_contour.show()
        self.map2_resolution.show()
        self.sccc_bt.setDisabled(True)
        self.sccc_bt.setChecked(False)
        
        
    def pdb_clicked(self):
        '''
        Changes the flag to pdb if pdbfile uploaded and display options
        '''
        self.flag = 'pdb'
        self.map2_input.hide()
        self.sheader_button.hide()
        self.map2_contour.hide()
        self.map2_resolution.hide()
        self.atomic_model_input.show()
        self.pdbheader_button.show()
        self.sccc_bt.setEnabled(True)
        self.sccc_bt.setChecked(True)
        self.rigid_body_file.show()
        self.ensemble_bt.setEnabled(True)
    
    def show_chimera(self):
        '''
        launches Chimera as a detached process to view input files and set mask
        threshold
        '''
        chimera_script = str(os.path.split(self.gui_path)[0]) + '/view_input_files.py'
        if self.mrc_bt.isChecked():
            chimera_command = "%s %s %s"%(
                str(chimera_script), 
                str(self.args.map1.value),
                str(self.args.map2.value))
        
        if self.pdb_bt.isChecked():
            chimera_command = "%s %s %s"%(
                    str(chimera_script), 
                    str(self.args.map1.value),
                    str(self.args.atomic_model.value))
            
        arg_list_chimera = ["--script", "%s" % (chimera_command)]
        process = QtCore.QProcess()
        chimera_bin = which('chimera')
        if chimera_bin is None:
            print 'Chimera executable not found (add executable to system PATH)'
        else:
            process.startDetached(chimera_bin, arg_list_chimera)
           
    def set_output_ui(self):
        '''
        Output UI, shows text browser of log file.
        '''
        self.output_ui = QtGui.QWidget()
        self.output_ui_vbox_layout = QtGui.QVBoxLayout()
        # Setup layout
        if self.job_manager is not None:
            output_file = self.job_manager.stdout_file  
        else:
            output_file = None
        self.ouput_tb = text_browser.TextBrowser(filepath=output_file,
                                                 parent=self)                                      
        self.output_ui_vbox_layout.addWidget(self.ouput_tb)
        self.output_ui.setLayout(self.output_ui_vbox_layout)
        self.output_dock.setWidget(self.output_ui)

    def set_contour_map1(self):
        '''
        Sets the contour of the first input map to 1sigma (header_rms from header)
        '''
        mapfile = str(self.map1_input.input_file_edit.text())
        self.header = read_mrc_header.MRCMapHeaderData(mapfile)
        if self.header.is_map_file:
            self.header_stdev = self.header.header_rms[0]
            self.map1_contour.input_edit.setText(str(round(float(self.header_stdev),4)))
            
    def set_contour_map2(self):
        '''
        Sets the contour of the second input map to 1sigma (header_rms from header)
        '''
        mapfile = str(self.map2_input.input_file_edit.text())
        self.header = read_mrc_header.MRCMapHeaderData(mapfile)
        if self.header.is_map_file:
            self.header_stdev = self.header.header_rms[0]
            self.map2_contour.input_edit.setText(str(round(float(self.header_stdev),4)))
        
    def pdb_header(self): 
        '''
        Sets the argument names for stage2.
        ''' 
        if str(self.args.atomic_model.value) != 'PDB':
            sobj = str(self.args.atomic_model.value)
            num_res = 0
            num_atoms =0
            input_pdb = PDBParser()
            structure = input_pdb.get_structure('X', sobj)
        
        #Parses the pdb header information for search object
            res_list = Selection.unfold_entities(structure, 'R')
            num_res = len(res_list)
            atom_list = Selection.unfold_entities(structure, 'A')
            num_atoms = len(atom_list)
            self.resolution = structure.header['resolution']
            pdbheader_values = [self.resolution, num_atoms, num_res]
            ncol = 1
            for value in pdbheader_values:
                self.pdbheader_gridlayout.addWidget(QtGui.QLineEdit(str(value)), 0, ncol)
                ncol +=2
    
    def run_detached_process(self):
        '''
        Launches job and calculates scores
        '''
        if self.score_bt.isChecked():
            score = True
        else:
            score = False
        if self.ensemble_bt.isChecked():
            ensemble = True
        else:
            ensemble = False
        if self.ccc_bt.isChecked():
            ccc = True
        else:
            ccc = False
        if self.mi_bt.isChecked():
            mi = True
        else:
            mi = False
        if self.sccc_bt.isChecked():
            sccc = True
        else:
            sccc = False
        if self.env_bt.isChecked():
            env = True
        else:
            env = False
        if self.nv_bt.isChecked():
            nv = True
        else:
            nv = False
        if self.random_bt.isChecked():
            random = True 
        else:
            random = False
        if self.angular_bt.isChecked():
            angular = True 
        else:
            angular = False
        self.status_widget.set_running()
        os.chdir(os.path.dirname(self.gui_path))
        if self.parent is not None:
            database_path = self.parent.database_path
            job_id, job_location = job_wrapper.create_job_database_entry(
                database_path=self.parent.database_path,
                project_location=self.parent.project_location,
                program=tasks.tempy.tempy.ProgramInfo().program_id,
                job_location=None)
        else:
            job_id = None
            job_location = core_utils.check_directory_and_make(
                                os.getcwd() + '/tempy_job_1',
                                verbose=True,
                                auto_suffix=True)
            self.job_location = job_location
            self.job_id = job_id
            database_path = None
        os.chdir(job_location)
        
        self.job_num = os.path.splitext(os.path.basename(self.job_location))[0]
        self.setWindowTitle('CCP-EM | ShapeEM ' + self.job_num)
        
        #
        run_task = tempy.RunTask(
            job_id=job_id,
            job_location=job_location,
            flag=self.flag,
            database_path=database_path,
            args = self.args,
            parent=self,
            ccc=ccc,
            mi=mi,
            sccc=sccc,
            env=env,
            nv=nv,
            score=score,
            ensemble=ensemble,
            random=random,
            angular=angular)
        self.ouput_tb.set_filepath(self.job_location+'/tempy_scores.log')
        self.filePath = job_location
    
    def start_job_status_monitor(self):
        self.job_status_timer = QtCore.QTimer()
        self.job_status_timer.timeout.connect(self.check_job_status)
        self.job_status_timer.start(500)

    def stop_job_status_monitor(self):
        self.job_status_timer.stop()

    def check_job_status(self):
        '''
        Detect if job completed.
        '''
        status = job_wrapper.get_job_manager_json_status(
            json_filepath=self.jm_json_filepath)
        if status == 'finished':
            self.set_on_job_finish()

    def set_on_job_finish(self):
        '''
        Actions to run on job completion.
        '''
        self.status_widget.set_finished()
        self.stop_job_status_monitor()

    def closeEvent(self, event):
        print 'Closing window...'
  
    def handle_toolbar_reload(self):
        '''
        Open job arguments and redraw widget.
        '''
        filename = QtGui.QFileDialog.getOpenFileName(None,
                                                     "Open a File",
                                                     QtCore.QDir.currentPath())
        self.args = tempy.tempy_parser().generate_arguments()
        
        #Check for json file
        if filename:    
            self.args.import_args_from_json(filename)
            print 'Import job arguments'
            print self.args.output_args_as_text()
        else:
            print 'Please upload arguments file'  
               
        #Delete the args and second dialog and widgets
        while self.setup_ui_vbox_layout.count():
            child = self.setup_ui_vbox_layout.takeAt(0)
            if child.widget() is not None:
                #child.widget() is none when its layout
                child.widget().deleteLater()
                
        self.args_widget = window_utils.CCPEMArgsDialog()
        
        prog_text = self.prog_label.text()
        self.prog_label = QtGui.QLabel(prog_text)
        self.prog_label.setWordWrap(True)
        self.prog_label.setFrameStyle(QtGui.QFrame.StyledPanel | 
                                      QtGui.QFrame.Sunken)
        self.setup_ui_vbox_layout.addWidget(self.prog_label)

        self.setup_ui_vbox_layout.addWidget(self.args_widget)

        self.set_args()


def main():
    '''
    Launch stand alone window.
    '''
       
    app = QtGui.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon(icons.icon_utils.get_ccpem_icon()))
    window = TemPyMainWindow()
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    window.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()