#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
'''
Task window for TEMPy SMOC Score.
'''
import os

from PyQt4 import QtGui, QtCore, QtWebKit

from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_core.tasks.tempy.smoc import smoc_task
from ccpem_core.tasks.map_process import mapprocess_task
from ccpem_gui.tasks.map_process import mapprocess_window
from ccpem_gui.utils import command_line_launch
from ccpem_core import chimera_scripts
from ccpem_core.settings import which
from ccpem_core.process_manager import job_register
from ccpem_gui.project_database import sqlite_project_database
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks.tempy import smoc as test_data


class PBBDatasets(object):
    def __init__(self,
                 pdb_path=None):
        self.pdb_path = None
        self.set_pdb_path(pdb_path=pdb_path)

    def set_pdb_path(self, pdb_path):
        if pdb_path is not None:
            self.pdb_path = os.path.abspath(pdb_path)

    def validate(self):
        if [self.pdb_path].count(None) == 0:
            return True
        else:
            return False


class PDBInput(QtGui.QWidget):
    def __init__(self,
                 label='Atomic model',
                 pdb_path=None,
                 chains=None,
                 parent=None):
        super(PDBInput, self).__init__()
        self.parent = parent
        self.dataset = PBBDatasets()
        self.pdb_stats = None
        button_width = 100
        label_width = 150
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)
        self.setToolTip('Add model')

        # PDB
        pdb_box = QtGui.QHBoxLayout()
        self.layout.addLayout(pdb_box)

        self.pdb_label = QtGui.QLabel(label)
        self.pdb_label.setFixedWidth(label_width)
        pdb_box.addWidget(self.pdb_label)
        self.pdb_select = QtGui.QPushButton('Select')
        self.pdb_select.setFixedWidth(button_width)
        self.pdb_select.clicked.connect(self.get_pdb_file)
        pdb_box.addWidget(self.pdb_select)
        self.pdb_value = QtGui.QLineEdit()
        self.pdb_value.editingFinished.connect(self.edit_finished_pdb)
        pdb_box.addWidget(self.pdb_value)

        width_ar = 30
        # Add
        add_button = QtGui.QPushButton('+')
        add_button.setFixedWidth(width_ar)
        add_button.setToolTip('Add dataset')
        add_button.clicked.connect(self.add)
        pdb_box.addWidget(add_button)

        # Remove
        remove_button = QtGui.QPushButton('-')
        remove_button.setFixedWidth(width_ar)
        remove_button.setToolTip('Remove dataset')
        remove_button.clicked.connect(self.remove)
        pdb_box.addWidget(remove_button)

        # Set initial values
        # PDB
        self.set_pdb_file(path=pdb_path)

    def get_pdb_file(self):
        path = self.get_file_from_brower(file_types=ccpem_file_types.pdb_ext)
        if path is not None:
            self.set_pdb_file(path=path)

    def edit_finished_pdb(self):
        path = str(self.pdb_value.text())
        self.set_pdb_file(path=path)

    def set_pdb_file(self, path):
        self.set_shading(self.pdb_label, shade=True)
        self.set_shading(self.pdb_value, shade=True)
        self.dataset.pdb_path = None
        if path is not None:
            if os.path.exists(path):
                self.pdb_value.setText(path)
                self.set_shading(self.pdb_label, shade=False)
                self.set_shading(self.pdb_value, shade=False)
                self.dataset.set_pdb_path(path)
            else:
                path = None

    def get_file_from_brower(self, file_types):
        dialog_path = window_utils.get_last_directory_browsed()
        path = QtGui.QFileDialog.getOpenFileName(
            self,
            'Open a File',
            QtCore.QDir.path(QtCore.QDir(dialog_path)),
            file_types)
        path = str(path)
        if path == '':
            path = None
        if path is not None:
            dialog_path = os.path.dirname(path)
            window_utils.set_last_directory_browsed(path=dialog_path)
        return path

    def set_shading(self, widget, shade=True):
        '''
        Shade text QLineEdit red to display warning.
        '''
        if shade:
            widget.setStyleSheet('color: red')
        else:
            widget.setStyleSheet('color: None')

    def remove(self):
        '''
        Only remove if more than 2 are present
        '''
        if hasattr(self.parent, 'number_pdb_inputs'):
            if self.parent.number_pdb_inputs() >= 2:
                self.deleteLater()

    def add(self):
        if hasattr(self.parent, 'add_dataset'):
            self.parent.add_dataset()
    
    def disable(self):
        self.pdb_value.setReadOnly(True)
        self.pdb_select.setEnabled(False)
        
class MultiPDBInput(QtGui.QWidget):
    def __init__(self,
                 group_label=None,
                 pdbs_arg=None,
                 chains_arg=None,
                 required=True,
                 parent=None):
        super(MultiPDBInput, self).__init__()
        self.parent = parent
        self.pdbs_arg = pdbs_arg
        # Layout
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)
        
        # Set tooltip
        tooltip_text = self.pdbs_arg.help
        if required:
            tooltip_text += ' | Required'
        self.setToolTip(tooltip_text)
        
        # Label
        if group_label is not None:
            ds_label = QtGui.QLabel(group_label)
            self.layout.addWidget(ds_label)

        # Set args
        pdbs = self.pdbs_arg()

        # Add datasets
        if isinstance(pdbs, list):
            for n in xrange(len(pdbs)):
                self.add_dataset(
                    pdb_path=pdbs[n])
        else:
            self.add_dataset(
                pdb_path=pdbs)
            
    def disable(self):
        index = self.layout.count()
        for l in xrange(index):
            PDBWidget = self.layout.itemAt(l).widget()
            try: 
                PDBWidget.disable()
            except AttributeError:
                pass
            
    def handle_add_button(self):
        '''
        Handle dataset button
        '''
        self.add_dataset()

    def add_dataset(self,
                    pdb_path=None,
                    chains=None):
        dataset = PDBInput(pdb_path=pdb_path,
                           parent=self)
        self.layout.addWidget(dataset)

    def number_pdb_inputs(self):
        return len(self.findChildren(PDBInput))

    def validate_datasets(self):
        children = self.findChildren(PDBInput)
        # Check at least one dataset
        if len(children) == 0:
            # Warn no datasets provided
            text = 'No input datasets provided'
            QtGui.QMessageBox.warning(self,
                'Error',
                text)
            return False
        warning = False
        # Check datasets are complete (must have pdb, map, resolution)
        for child in children:
            if not child.dataset.validate():
                warning = True
        if warning:
            # Warn datasets incomplete information
            text = 'Dataset(s) incomplete (requires PDB)'
            QtGui.QMessageBox.warning(self,
                'Error',
                text)
            return False
        else:
            return True

    def set_args(self):
        children = self.findChildren(PDBInput)
        pdbs = []
        if len(children) > 1:
            for child in children:
                pdbs.append(child.dataset.pdb_path)
            self.pdbs_arg.value = pdbs
        else:
            child = children[0]
            self.pdbs_arg.value = child.dataset.pdb_path


class SMOCMapWindow(window_utils.CCPEMTaskWindow):
    '''
    SMOC window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args_smoc.json')

    def __init__(self,
                 task,
                 parent=None):
        super(SMOCMapWindow, self).__init__(task=task,
                                             parent=parent)

    def set_args(self):
        '''
        Set input arguments
        '''

        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Input map
        self.map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='map_path',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_input)
        
        self.task.mapprocess_task = None
#         #set map process task button
#         self.task.mapprocess_task = None
#         self.mp_button = QtGui.QRadioButton('Process input map')
#         self.mp_button.setToolTip('Process input map')
#         self.args_widget.args_layout.addWidget(self.mp_button)
#         self.mp_button.clicked.connect(self.handle_mapprocess_task)
#         self.mp_button.setEnabled(False)
#         if self.args.map_path.value is not None and \
#             os.path.isfile(self.args.map_path.value):
#                 self.add_mapprocess_button()
#         self.map_input.select_button.clicked.connect(
#                                     self.add_mapprocess_button)
        self.task.map_input = self.map_input
#         self.map_input.value_line.editingFinished.connect(
#                                     self.add_mapprocess_button)
        # Map resolution
        self.map_resolution = window_utils.NumberArgInput(
            parent=self,
            arg_name='map_resolution',
            maximum = 50.5,
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_resolution)
        
        # Metric selection
        method_options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Metric Selection',
            button_tooltip=('By default: SMOC for map resolutions < 7.5A \n'
                            'SCCC otherwise'))
        self.args_widget.args_layout.addLayout(method_options_frame)
        # -> Metric choices (mutually exclusive)
        self.method_buttons = QtGui.QButtonGroup()
        self.use_smoc_method = window_utils.CheckArgInput(
            parent=self,
            arg_name='use_smoc',
#             tooltip_text=('score each overlapping residue fragment fit'
#                           'and report per residue score plots'),
            args=self.args)
        self.use_sccc_method = window_utils.CheckArgInput(
            parent=self,
            arg_name='use_sccc',
#             tooltip_text=('score each rigid body fit'
#                           'and report per rigid body scores'),
            args=self.args)
        #TODO: check why the checkboxes are cropped by default
        #adjusting height
        self.use_smoc_method.value_line.setStyleSheet(
                                "QCheckBox::indicator { height: 22px;}")
        self.use_sccc_method.value_line.setStyleSheet(
                                "QCheckBox::indicator { height: 22px;}")
        #add to mutually exclusive button group
        self.method_buttons.addButton(self.use_smoc_method.value_line,1)
        self.method_buttons.addButton(self.use_sccc_method.value_line,2)
        #add to layout
        method_options_frame.add_extension_widget(self.use_smoc_method)
        method_options_frame.add_extension_widget(self.use_sccc_method)
        #set SMOC/SCCC based on resolution
        self.map_resolution.value_line.editingFinished.connect(self.select_metric)
#         self.map_resolution.value_line.editingFinished.connect(self.check_processed_map)
        # Input PDBs
        self.input_pdbs = MultiPDBInput(
            group_label='Input PDB(s)',
            pdbs_arg=self.args.input_pdbs,
            chains_arg=self.args.input_pdb_chains,
            parent=self)
        self.args_widget.args_layout.addWidget(self.input_pdbs)
#                 # -> Use Refmac for model-map conversion
#         self.use_refmac = window_utils.CheckArgInput(
#             parent=self,
#             arg_name='use_refmac',
#             args=self.args,
#             label_width=245)
#         self.args_widget.args_layout.addWidget(self.use_refmac)
#         self.use_refmac.value_line.stateChanged.connect(self.set_modelmap)
#         # Ligand library input for Refmac5
#         self.lib_input = window_utils.FileArgInput(
#             parent=self,
#             arg_name='lib_in',
#             args=self.args,
#             file_types=ccpem_file_types.lib_ext,
#             required=False)
#         self.args_widget.args_layout.addWidget(self.lib_input)
        # Input rigid body file
        self.input_rigid_body = window_utils.FileArgInput(
            parent=self,
            arg_name='rigid_body_path',
            required=False,
            file_types=ccpem_file_types.text_ext,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.input_rigid_body)
        
        #self.select_metric()
        # Advanced options
        self.advanced_options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Advanced options',
            button_tooltip='Specify additional parameters')
        self.args_widget.args_layout.addLayout(self.advanced_options_frame)

        # Input map or PDB
        self.dist_or_fragment = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='dist_or_fragment_selection',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.dist_or_fragment)
        self.dist_or_fragment.value_line.currentIndexChanged.connect(
            self.set_dist_or_fragment_arguments)
        self.advanced_options_frame.add_extension_widget(self.dist_or_fragment)
        
        #optional distance setting disabled (auto calculated by resolution)
         # -> Weight auto
#         self.auto_distance = window_utils.CheckArgInput(
#             parent=self,
#             arg_name='auto_local_distance',
#             args=self.args)
#         self.advanced_options_frame.add_extension_widget(self.auto_distance)
#         self.auto_distance.value_line.stateChanged.connect(
#             self.set_local_distance_auto)
# 
#         # Local distance
#         self.local_distance = window_utils.NumberArgInput(
#             parent=self,
#             arg_name='local_distance',
#             required=True,
#             args=self.args)
#         self.advanced_options_frame.add_extension_widget(
#                                             self.local_distance)

        # -> Weight auto
        self.input_auto_length = window_utils.CheckArgInput(
            parent=self,
            arg_name='auto_fragment_length',
            args=self.args)
        self.advanced_options_frame.add_extension_widget(self.input_auto_length)
        self.input_auto_length.value_line.stateChanged.connect(
            self.set_fragment_length_auto)
        
        # Fragment length
        self.input_fragament_length = window_utils.NumberArgInput(
            parent=self,
            arg_name='fragment_length',
            required=True,
            args=self.args)
        self.advanced_options_frame.add_extension_widget(
                                            self.input_fragament_length)
        
        self.use_smoc_method.value_line.stateChanged.connect(
                                            self.set_smoc_advanced_frame)
        self.set_dist_or_fragment_arguments()
        #self.set_local_distance_auto()
        #self.set_fragment_length_auto()
    
    def set_dist_or_fragment_arguments(self):
        if self.args.dist_or_fragment_selection.value == 'Distance':
            self.input_auto_length.hide()
            #self.auto_distance.show()
            self.input_fragament_length.hide()
            #self.local_distance.hide()
        else:
            self.input_auto_length.show()
            #self.auto_distance.hide()
            self.input_fragament_length.hide()
            #self.local_distance.hide()
    
    def set_modelmap(self):
        if self.use_refmac.value_line.isChecked():
            self.task.args.use_refmac.value = True
        else:
            self.task.args.use_refmac.value = False

    def set_fragment_length_auto(self):
        if self.args.auto_fragment_length():
            self.input_fragament_length.hide()
        else:
            self.input_fragament_length.show()
    
    def set_local_distance_auto(self):
        if self.args.auto_local_distance():
            self.local_distance.hide()
        else:
            self.local_distance.show()

    def add_mapprocess_button(self):
        if os.path.isfile(self.args.map_path.value):
            self.mp_button.setEnabled(True)
    
    def handle_mapprocess_task(self,db_inject=None, job_title=None):
        assert os.path.isfile(self.args.map_path.value)

        if self.task.database_path is not None:
            path = os.path.dirname(self.task.database_path)
            db_inject = sqlite_project_database.CCPEMInjector(
                database_path=self.task.database_path)
        '''
        job_id, job_location = job_register.job_register(
            db_inject=db_inject,
            path=path,
            task_name=mapprocess_task.MapProcess.task_info.name)
        '''
        # Set task args
        args = mapprocess_task.MapProcess().args
        #args.job_title.value = job_title
        args.map_path.value = self.args.map_path.value
        #args_json = args.output_args_as_json()
        task = mapprocess_task.MapProcess(
            parent=self,
            args=args,
            database_path=self.task.database_path)
            #job_location=job_location)
        window = mapprocess_window.MapProcessWindow(
            parent=self,
            task=task)
        window.show()
        self.task.mapprocess_task = task
        #print self.task.processed_map_path
    def check_processed_map(self):
        processed_map_path = os.path.splitext(
                            os.path.basename(self.args.map_path.value))[0] \
                            +'_processed.mrc'
        #print processed_map_path
        if os.path.isfile(processed_map_path) and \
            self.task.mapprocess_task is not None:
            self.map_input.value_line.setText(processed_map_path)

    def select_metric(self):
        if self.args.map_resolution.value < 7.5:
            self.use_smoc_method.value_line.setChecked(True)
            #self.use_sccc_method.value_line.setChecked(False)
        else: 
            #self.use_smoc_method.value_line.setChecked(False)
            self.use_sccc_method.value_line.setChecked(True)
            
    def set_smoc_advanced_frame(self):
        if self.use_smoc_method.value_line.isChecked():
            self.advanced_options_frame.show()
        elif self.use_sccc_method.value_line.isChecked():
            self.advanced_options_frame.hide()
    
    def set_method(self):
        if self.args.use_smoc.value:
            self.use_sccc_method.setChecked(False)
        else:
            self.use_sccc_method.setChecked(True)
            
    
    def handle_chimera_view(self):
        button = self.sender()
        chim_script = os.path.join(
                os.path.dirname(os.path.realpath(chimera_scripts.__file__)),
                'color_by_existing_attribute.py')
        pdbs = self.task.args.input_pdbs()
        if not isinstance(pdbs, list):
            pdbs = [pdbs]
        list_pdbs = []
        l = 1
        for pdb in pdbs:
            pdb_id = os.path.splitext(os.path.basename(pdb))[0]+'_'+str(l)
            scored_pdb = os.path.join(self.task.job_location,
                        pdb_id+'_smoc.pdb')
            if os.path.isfile(scored_pdb):
                list_pdbs.append(scored_pdb)
            l += 1
        #include the map
        list_pdbs.append(self.args.map_path.value)
        pdb_str = ' '.join(list_pdbs)
        chimera_command = '{} {} bfactor' .format(
            str(chim_script),
            str(pdb_str))
        
        arg_list_chimera = ['--script', '%s' % (chimera_command)]
        process = QtCore.QProcess()
        chimera_bin = which('chimera')
        if chimera_bin is None:
            print 'Chimera executable not found (add executable to system PATH)'
        else: 
            process.startDetached(chimera_bin, arg_list_chimera)
            
    def set_chimera_button(self):
        #view in chimera
        chimera_button_widget = QtGui.QWidget()
        chimera_button_layout = QtGui.QHBoxLayout()
        chimera_button_widget.setLayout(chimera_button_layout)
        chimera_view_button = QtGui.QPushButton('View in Chimera')
        if which('chimera') is not None:
            chimera_view_button.clicked.connect(self.handle_chimera_view)
        else: chimera_view_button.setEnabled(False)
        chimera_button_layout.addWidget(chimera_view_button)
        return chimera_button_widget

    def set_rv_ui(self):
        '''
        RVAPI results viewer.
        '''
        # Create results if not already done
        rv_index = os.path.join(
            self.task.job_location,
            'rvapi_data/index.html')
        if os.path.exists(rv_index):
            self.rv_view = QtWebKit.QWebView()
            rvapi_layout = QtGui.QHBoxLayout()
            assert os.path.exists(rv_index)
            self.rv_view.load(QtCore.QUrl(rv_index))
            results_dock_layout = QtGui.QVBoxLayout()
            results_dock_widget = QtGui.QWidget()
            results_dock_widget.setLayout(results_dock_layout)
            self.results_dock = QtGui.QDockWidget('Results',
                                                   self,
                                                   QtCore.Qt.Widget)
            
            self.results_dock.setToolTip('Results overview')
            self.results_dock.setWidget(results_dock_widget)#self.rv_view)
            self.tabifyDockWidget(self.setup_dock, self.results_dock)
            chimera_view_button = self.set_chimera_button()
            self.rv_view.setMinimumHeight(300)
            self.rv_view.setLayout(rvapi_layout)
            results_dock_layout.addWidget(chimera_view_button)
            results_dock_layout.addWidget(self.rv_view)
            results_dock_layout.addStretch(1)
            self.results_dock.raise_()
            
    def set_bokeh_ui(self):
        # Create results if not already done
        rv_index = os.path.join(
            self.task.job_location,
            'bokeh_data/index.html')
        if os.path.exists(rv_index):
            self.rv_view = QtWebKit.QWebView()
            assert os.path.exists(rv_index)
            self.rv_view.load(QtCore.QUrl.fromLocalFile(rv_index))
            self.results_dock = QtGui.QDockWidget('Results',
                                                   self,
                                                   QtCore.Qt.Widget)
            self.results_dock.setToolTip('Results overview')
            self.results_dock.setWidget(self.rv_view)
            self.tabifyDockWidget(self.setup_dock, self.results_dock)
            self.results_dock.show()
            self.results_dock.raise_()

    def validate_input(self):
        '''
        Override from baseclass to allow validation of multiple datasets
        '''
        ready = super(SMOCMapWindow, self).validate_input()
        if ready:
            # Validate inputs for multiple datasets
            if not self.input_pdbs.validate_datasets():
                ready = False
            else:
                self.input_pdbs.set_args()
        return ready

    def set_on_job_finish_custom(self):
        self.input_pdbs.disable()
        # Add pdbs to launcher
        pdbs = self.task.args.input_pdbs()
        if not isinstance(pdbs, list):
            pdbs = [pdbs]
        for pdb in pdbs:
            self.launcher.add_file(
                path=pdb,
                file_type='pdb',
                description='Input pdb',
                selected=True)
        # Add map to launcher
        self.launcher.add_file(
            arg_name='map_path',
            file_type='map',
            description=self.args.map_path.help,
            selected=True)
        
        for pdb in pdbs:
            scored_pdb = os.path.join(self.task.job_location,
                                      os.path.basename(pdb).split('.')[0]+'_smoc.pdb')
            if os.path.isfile(scored_pdb):
                self.launcher.add_file(
                    path=scored_pdb,
                    file_type='pdb',
                    description='pdb: Scores as b-factors',
                    selected=True)
        # Add raw data
        self.rawdata_path = os.path.join(self.task.job_location,
                                         'smoc_score.csv')
        if os.path.exists(self.rawdata_path):
            self.launcher.add_file(
                description='Raw scores (CSV text)',
                path=self.rawdata_path)
        else:
            self.rawdata_path = os.path.join(self.task.job_location,
                                         'sccc_score.csv')
            if os.path.exists(self.rawdata_path):
                self.launcher.add_file(
                    description='Raw scores (CSV text)',
                    path=self.rawdata_path)
        self.launcher.set_tree_view()
        self.launcher_dock.raise_()
        self.launcher_dock.show()
        self.set_rv_ui()

def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=smoc_task.SMOC,
        window_class=SMOCMapWindow)

if __name__ == '__main__':
    main()
