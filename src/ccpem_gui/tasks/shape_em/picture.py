#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
For updating plots and displaying it in the output tabs
'''
from PyQt4 import QtGui, QtCore
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt4agg \
    import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4 import NavigationToolbar2QT \
    as NavigationToolbar


class Picture(QtGui.QDialog):
    '''
    Auto update checks file size, any increase automatically updates displayed
    text.
    '''
    def __init__(self,
                 filepath=None,
                 auto_update=True,
                 parent=None,
                 job_id=None,
                 job_location=None):
        super(Picture, self).__init__(parent)
        self.filepath = filepath
        self.job_location = job_location
        self.job_id = job_id
        self.auto_update = auto_update
        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        self.setLayout(layout)
        if self.filepath is not None:
            self.load_file()

    def set_filepath(self,
                     filepath,
                     job_id,
                     job_location):
        self.filepath = filepath
        self.job_location = job_location
        self.job_id = job_id
        self.auto_update = True
        self.load_file()

    def load_file(self):
        self.file_size = 0
        self.current_file_pos = 0
        self.screenout = ''
        self.qfile = QtCore.QFile(self.filepath)
        self.qfile.open(QtCore.QIODevice.ReadOnly)
        if self.auto_update:
            self.timer = QtCore.QTimer()
            self.timer.timeout.connect(self.check_file_exist)
            self.timer.start(500)
        else:
            self.make_plot()

    def check_file_exist(self):
        '''
        Change in file triggers update of displayed text.
        '''
        # Check if file has opened ok (log file may not have been created when
        # widget is initiated
        if self.qfile.error() == QtCore.QFile.NoError:
            if QtCore.QFile.exists(self.filepath) and QtCore.QFile.size != 0:
                self.make_plot()
                self.timer.stop()
        else:
            self.qfile = QtCore.QFile(self.filepath)
            self.qfile.open(QtCore.QIODevice.ReadOnly)

    def closeEvent(self, event):
        self.timer.stop()

    def make_plot(self):
        '''
        Draws a ccc plot for the number of solutions and gives an option to
        save
        '''
        result_file = open(self.filepath, 'r')
        sol_list = []
        corr_list = []
        for line in result_file:
            sol_num = line.split()[0]
            corr_value = line.split()[-1]
            sol_list.append(sol_num)
            corr_list.append(corr_value)
        ax = self.figure.add_subplot(1, 1, 1)
        ax.hold(False)
        ax.plot(sol_list, corr_list)
        self.canvas.draw()
        ax.set_ylabel('CCC value')
        ax.set_xlabel('Solution number')
