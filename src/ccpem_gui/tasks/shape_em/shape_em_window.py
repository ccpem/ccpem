#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Task window for ShapeEM program.
'''

import os
import math
from PyQt4 import QtGui, QtCore
from ccpem_gui.utils import window_utils
from ccpem_gui.utils import ccpem_widgets
from ccpem_core.tasks.shape_em import shape_em_task
from ccpem_core.settings import which
from ccpem_core import settings
from ccpem_gui.project_database import sqlite_project_database
from ccpem_gui.utils import gui_process
from ccpem_gui.utils import command_line_launch

class ShapeEMMainWindow(window_utils.CCPEMTaskWindow):
    '''
    Inherits from CCPEMTaskWindow and creates the form.
    '''
    def __init__(self,
                 parent=None,
                 task=None):
        self.grid_sampling_input = None
        self.box_extent = None
        self.mrc_header = None
        self.has_run = False
        self.review_dock = None
        super(ShapeEMMainWindow, self).__init__(task=task,
                                               parent=parent)

    def set_grid_sampling(self):
        if self.mrc_header is not None:
            if self.mrc_header.map_data is not None:
                voxel = round(self.mrc_header.map_data.voxx_voxy_voxz[2], 2)
                self.args.grid_sampling.value = voxel
                if self.grid_sampling_input is not None:
                    self.grid_sampling_input.value_line.setValue(voxel)

    def set_box_extent(self):
        if self.mrc_header is not None:
            if self.mrc_header.map_data is not None:
                half_box = int(0.5 * self.mrc_header.map_data.mx_my_mz[0])
                set_box = None
                for n in xrange(1000):
                    if set_box is None:
                        if half_box > math.pow(2,n):
                            continue
                        else:
                            set_box = math.pow(2,n)
                self.args.box_extent.value = set_box
                if self.box_extent is not None:
                    self.box_extent.value_line.setValue(set_box)

    def set_args(self):
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            label_width=150,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Input options frame
        self.input_options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Input options',
            button_tooltip='Input options search parameters')
        self.args_widget.args_layout.addLayout(self.input_options_frame)
        self.input_options_frame.show()

        # Input target mrc
        self.target_map_arg_input = window_utils.FileArgInput(
            parent=self,
            arg_name='target_map',
            args=self.args,
            required=True,
            tooltip_text='Target map in which you want to fit pdb',
            file_types='MRC(*.mrc *.map *.ccp4)')
        self.input_options_frame.add_extension_widget(self.target_map_arg_input)

        # MRC header display
        self.mrc_header_frame = window_utils.CCPEMExtensionFrame(
            button_name='Map details',
            button_tooltip='Show map details')
        self.input_options_frame.add_extension_frame(self.mrc_header_frame)
        self.mrc_header_frame.button.click()

        self.mrc_header = window_utils.MRCMapHeaderInfo(
            parent=self,
            filename=self.args.target_map.value)
        self.mrc_header_frame.add_extension_widget(self.mrc_header)
        
        # Fix doesn't show when reloaded...
        self.target_map_arg_input.value_line.textChanged.connect(
            lambda: self.mrc_header.set_filename(
                str(self.target_map_arg_input.value_line.text())))

        self.map_resolution_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='map_resolution',
            required=True,
            args=self.args)
        self.input_options_frame.add_extension_widget(self.map_resolution_input)

        if self.args.grid_sampling() is None:
            self.set_grid_sampling()
        self.grid_sampling_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='grid_sampling',
            args=self.args)
        self.input_options_frame.add_extension_widget(self.grid_sampling_input)
        self.target_map_arg_input.value_line.textChanged.connect(
            self.set_grid_sampling)

        self.high_pass_filter = window_utils.NumberArgInput(
            parent=self,
            arg_name='high_pass_filter',
            args=self.args)
        self.input_options_frame.add_extension_widget(self.high_pass_filter)

        # Input search object and pdb header
        self.search_object_arg_input = window_utils.FileArgInput(
            parent=self,
            arg_name='search_object',
            args=self.args,
            required=True,
            tooltip_text='Search object to be docked in target map',
            file_types='PDB(*.pdb)')
        self.input_options_frame.add_extension_widget(
            self.search_object_arg_input)

        # Centre model in map
        self.centre_model_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='centre_model',
            args=self.args,
            required=False)
        self.input_options_frame.add_extension_widget(
            self.centre_model_input)

        self.mask_smooth = window_utils.NumberArgInput(
            parent=self,
            arg_name='mask_smooth',
            args=self.args)
        self.input_options_frame.add_extension_widget(self.mask_smooth)

        # Input mask Threshold
        self.mask_threshold = window_utils.NumberArgInput(
            parent=self,
            arg_name='mask_threshold',
            args=self.args)
        self.input_options_frame.add_extension_widget(self.mask_threshold)

        # Base on 1/2 box size; to nearest power of 2
        # Input Extent search
        if self.args.box_extent() is None:
            self.set_box_extent()

        self.box_extent = window_utils.NumberArgInput(
            parent=self,
            arg_name='box_extent',
            required=True,
            args=self.args)
        self.input_options_frame.add_extension_widget(self.box_extent)
        self.target_map_arg_input.value_line.textChanged.connect(
            self.set_box_extent)

        # Full search options
                # Separator
        self.args_widget.args_layout.addWidget(
            ccpem_widgets.CCPEMMenuSeparator())

        # run mode
        self.full_run_option = window_utils.CheckArgInput(
            parent=self,
            arg_name='run_search',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.full_run_option)
        self.full_run_option.value_line.stateChanged.connect(
            self.set_run_mode)

        self.run_frame = window_utils.CCPEMExtensionFrame(
            button_name='Run options',
            button_tooltip='Full search parameters')
        self.args_widget.args_layout.addLayout(self.run_frame)
        self.set_run_mode()

        # Number of processors
        if self.args.n_proc() is None:
            self.args.n_proc.value = max(
                1,
                QtCore.QThread.idealThreadCount()-1)

        self.n_proc_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='n_proc',
            minimum=1,
            args=self.args,
            set_none=False)
        self.run_frame.add_extension_widget(
            self.n_proc_input)

        # Search parameters options frame
        self.ang_parameters_frame = window_utils.CCPEMExtensionFrame(
            button_name='Angular sampling options',
            button_tooltip='Specify angular sampling parameters')
        self.run_frame.add_extension_frame(self.ang_parameters_frame)
        self.ang_parameters_frame.button.click()

        # -> Input angular sampling
        self.angular_sampling = window_utils.NumberArgInput(
            parent=self,
            arg_name='angl_sampling',
            args=self.args)
        self.ang_parameters_frame.add_extension_widget(self.angular_sampling)
        self.angular_sampling.value_line.editingFinished.connect(
            self.set_theta)

        # -> Input theta step
        self.theta_step = window_utils.NumberArgInput(
            parent=self,
            arg_name='theta_step',
            args=self.args)
        self.ang_parameters_frame.add_extension_widget(self.theta_step)

        # Set inputs for launcher
        self.launcher.add_file(
            arg_name='target_map',
            file_type='map',
            description=self.args.target_map.help,
            selected=False)
        # Select filtered map by default (not input)
        self.launcher.add_file(
            arg_name='target_map_filtered',
            description=self.args.target_map_filtered.help,
            file_type='map',
            selected=True)
        # Search object i.e. pdb
        self.launcher.add_file(
            arg_name='search_object',
            file_type='pdb',
            description=self.args.search_object.help,
            selected=True)
        # Search object
        self.launcher.add_file(
            arg_name='search_object_filtered',
            file_type='map',
            description=self.args.search_object_filtered.help,
            selected=True)

    def set_run_mode(self):
        if self.args.run_search():
            self.run_frame.show()
        else:
            self.run_frame.hide()

    def set_review_ui(self):
        '''
        Pre-process review.
        '''
        self.review_dock = QtGui.QDockWidget('Review',
                                               self,
                                               QtCore.Qt.Widget)
        self.review_dock_name = 'review_dock'
        self.review_dock.setObjectName(self.review_dock_name)
        self.review_dock.setVisible(False)
        self.tabifyDockWidget(self.setup_dock, self.review_dock)
        self.review_ui = QtGui.QGroupBox(self)
        self.review_ui_hbox_layout = QtGui.QHBoxLayout()

        self.review_ui.setLayout(self.review_ui_hbox_layout)
        self.review_dock.setWidget(self.review_ui)
        # Information
        text = \
'''
Welcome to Review tab. 

Here the set-up parameters should be checked before the full search is launched, as otherwise CPU hours (and yours) could be wasted.

The set-up parameters should now be checked visually using UCSF Chimera (or other option for map display and molecular graphics). 

Use the top button on the panel to the right to launch UCSF Chimera to check: the mask threshold, and the initial position of the search object in relation to the map and search extent. Options to select to maps and models to be displayed can be adjusted in the Launcher tab.

If any adjustments have or should be made, use the middle button to go back to the Setup tab, and repeat these steps till you are happy with the set-up.

Once you are satisfied the set-up is satisfactory, press the lower button to proceed to the Run section in the lower part of the Setup tab.
Here you can enter/check some final run parameters, and launch the full DockEM run.
'''
        info_label = window_utils.CCPEMInfoLabel(text)
        self.review_ui_hbox_layout.addWidget(info_label)
        self.review_ui_hbox_layout.setAlignment(QtCore.Qt.AlignTop)
        
        # Layout for metadata and buttons 
        self.action_buttons_layout = QtGui.QVBoxLayout()
        self.action_buttons_layout.setAlignment(QtCore.Qt.AlignTop)

        # Action buttons
        self.review_ui_hbox_layout.addLayout(self.action_buttons_layout)
        self.review_chimera_button = QtGui.QPushButton('Run chimera')
        self.review_rerun_input_button = QtGui.QPushButton('Re-run setup')
        self.review_run_search_button = QtGui.QPushButton('Run search')
        # 
        self.action_buttons_layout.addWidget(self.review_chimera_button)
        self.action_buttons_layout.addWidget(self.review_rerun_input_button)
        self.action_buttons_layout.addWidget(self.review_run_search_button)
        #
        self.review_chimera_button.clicked.connect(
            self.chimera_view_setup)
        self.review_rerun_input_button.clicked.connect(
            self.re_run_setup_clicked)
        self.review_run_search_button.clicked.connect(
            self.run_search_clicked)

    def re_run_setup_clicked(self):
        self.setup_dock.raise_()
        self.input_options_frame.button.setChecked(True)
        self.input_options_frame.button_clicked()
        #
        self.full_run_option.value_line.setChecked(False)

    def run_search_clicked(self):
        self.setup_dock.raise_()
        self.input_options_frame.button.setChecked(False)
        self.input_options_frame.button_clicked()
        #
        self.full_run_option.value_line.setChecked(True)

    def set_analyzer_ui(self):
        '''
        Analyzer UI, to view the output of DockEM more interactively.
        '''
        # Save initial number of solutions and peak radius 
        self.start_number_solutions = self.args.number_solutions.value
        self.start_peak_radius = self.args.peak_radius.value
        self.analyzer_dock = QtGui.QDockWidget('Analyzer',
                                               self,
                                               QtCore.Qt.Widget)
        self.analyzer_dock_name = 'analyzer_dock'
        self.analyzer_dock.setObjectName(self.analyzer_dock_name)
        self.analyzer_dock.setVisible(False)
        self.tabifyDockWidget(self.setup_dock, self.analyzer_dock)
        self.analyzer_ui = QtGui.QWidget(self)
        self.analyzer_ui_vbox_layout = QtGui.QVBoxLayout()
        self.analyzer_ui.setLayout(self.analyzer_ui_vbox_layout)
        self.analyzer_dock.setWidget(self.analyzer_ui)

        ### Set args
        self.analyzer_args_layout = QtGui.QHBoxLayout()
        self.analyzer_ui_vbox_layout.addLayout(self.analyzer_args_layout)
        # Input peak radius
        self.peak_radius = window_utils.NumberArgInput(
            parent=self,
            arg_name='peak_radius',
            args=self.args,
            label='Peak radius')
        self.analyzer_args_layout.addWidget(self.peak_radius)

        # Input solution range
        self.number_solutions = window_utils.NumberArgInput(
            parent=self,
            arg_name='number_solutions',
            args=self.args,
            label='Number of solutions')
        self.analyzer_args_layout.addWidget(self.number_solutions)

        ### Set buttons
        # View button for raw data
        self.view_raw_data_button = QtGui.QPushButton('View raw data')
        self.view_raw_data_button.clicked.connect(self.view_raw_data)
        self.view_raw_data_button.setToolTip('View ')
        self.analyzer_ui_vbox_layout.addWidget(self.view_raw_data_button)
        # Run button for analyzer
        self.run_analyzer_button = QtGui.QPushButton('Re-run analyzer')
        self.run_analyzer_button.clicked.connect(
            self.handle_run_analyzer_button)
        self.analyzer_ui_vbox_layout.addWidget(self.run_analyzer_button)

        # Connect re-run button colour change when values are changed for 
        # peak radius or number of solutions
        self.number_solutions.value_line.valueChanged.connect(
            self.set_highlight_re_run)
        self.peak_radius.value_line.valueChanged.connect(
            self.set_highlight_re_run)

        ### Set plot and visualise
        self.plot_vis_splitter = QtGui.QSplitter(QtCore.Qt.Horizontal)
        self.analyzer_ui_vbox_layout.addWidget(self.plot_vis_splitter)
        # Get peak search output
        peak_search_file = ('searchdocIP' +
                            self.args.job_prefix.value +
                            '.dkm')
        self.peak_search_path = os.path.join(self.task.job_location,
                                             peak_search_file)
        if os.path.exists(self.peak_search_path):
            # Set plot
            self.analyzer_plot = dock_em_plot.DockEMPlot(
                parent=self,
                peak_search_filepath=self.peak_search_path)
            self.plot_vis_splitter.addWidget(self.analyzer_plot) 
            # Set solution visualise widget
            self.vis_widget = QtGui.QWidget()
            self.vis_layout = QtGui.QVBoxLayout()
            self.vis_widget.setLayout(self.vis_layout)
            self.visualise_button = QtGui.QPushButton('View')
            self.visualise_button.setToolTip(
                'Launch Chimera with selected solutions')
            self.visualise_button.clicked.connect(
                self.chimera_view_solutions)
            if settings.which('chimera') is None:
                self.visualise_button.setEnabled(False)

            self.vis_layout.addWidget(self.visualise_button)
            self.plot_vis_splitter.addWidget(self.vis_widget)
            #### Just add listwidget with all solutions....
            self.pdb_list = QtGui.QListWidget(self)
            # Allow multiple selections
            self.pdb_list.setSelectionMode(
                QtGui.QAbstractItemView.MultiSelection)
            self.vis_layout.addWidget(self.pdb_list)
            self.set_pdb_solution_items()

    def set_highlight_re_run(self):
        '''
        Set colour of re-run analyzer button in analyzer to green after updating
        peak radius or number of solutions 
        '''
        if hasattr(self, 'run_analyzer_button'):
            self.run_analyzer_button.setStyleSheet(
                'QPushButton { background-color: lightgreen; }')

    def set_pdb_solution_items(self):
        if hasattr(self, 'pdb_list'):
            self.pdb_list.clear()
            pdb_hits = {}
            for filename in os.listdir(self.task.job_location):
                if 'tophit' in filename:
                    try:
                        number = int(filename[-7:-4])
                        solution = 'Solution {0}'.format(number)
                        pdb_hits[solution] = filename
                    except ValueError:
                        pass
            for val, key in pdb_hits.iteritems():
                item = QtGui.QListWidgetItem(key)
                item.setToolTip(val)
                self.pdb_list.addItem(item)
            self.pdb_list.sortItems()

    def set_theta(self):
        '''
        Theta and angular sampling set to same. If user changes either value
        other value gets updated
        '''
        self.theta_step.value_line.setValue(self.args.angl_sampling.value)
        self.args.theta_step.value = self.args.angl_sampling.value

    def chimera_view_setup(self):
        '''
        launches chimera as a detached process to view input files and set mask
        threshold
        
            # target map + box (deactivate for movement)
            # filtered search object
            # mask object + set threshold as defined in input
            # cropped map (not on ctr bar) + box + high threshold (i.e. only see box)
            #
            # View - model
            # target model
        '''
        mask_script = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'dock_em_chimera_view_setup.py')
        chimera_command = '%s %s %s %s %s %s %s' % (
            str(mask_script),
            str(self.args.search_object_mask.value),
            str(self.args.target_map_filtered.value),
            str(self.args.search_object_filtered.value),
            str(self.args.search_object.value),
            str(self.args.mask_threshold.value),
            str(self.task.job_location))
        arg_list_chimera = ['--script', '%s' % (chimera_command)]
        process = QtCore.QProcess()
        chimera_bin = which('chimera')
        if chimera_bin is None:
            print 'Chimera executable not found (add executable to system PATH)'
        else:
            process.startDetached(chimera_bin, arg_list_chimera)

    def chimera_view_solutions(self):
        '''
        View selected solutions in Chimera
        loads target map, sets its transparency and contour and open the
        selected solutions
        '''
        open_chimera_script = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'dock_em_chimera_view_solutions.py')
        list_pdb_sols = []
        if len(self.pdb_list.selectedItems()) == 0:
            message = ('No solutions selected')
            QtGui.QMessageBox.warning(
                self,
                'Warning',
                message)
            return
        for sol in self.pdb_list.selectedItems():
            pdb_path = os.path.join(self.task.job_location,
                                    str(sol.text()))
            if os.path.exists(pdb_path):
                list_pdb_sols.append(pdb_path)
        ids_checked = '###pdb$$$'.join(list_pdb_sols)
        chimera_command = '%s %s %s' % (open_chimera_script,
                                        self.args.target_map_filtered.value,
                                        ids_checked)
        arg_list_chimera = ['--script', '%s' % (chimera_command)]
        process = QtCore.QProcess()
        chimera_bin = which('chimera')
        if chimera_bin is None:
            print 'Chimera executable not found (add executable to system PATH)'
        else:
            process.startDetached(chimera_bin, arg_list_chimera)

    def handle_run_analyzer_button(self):
        '''
        Re-Runs DockEMpeak and DockEMsoln and generate new ccc plot and hits
        '''
        self.set_pdb_solution_items()
        # Remove the solutions and old stdin files
        all_files = os.listdir(self.task.job_location)
        for filename in all_files:
            if filename.startswith('tophit'):
                os.remove(os.path.join(self.task.job_location,
                                       filename))
        dockem_files = ['stdin_dockemsoln.txt',
                        'stdin_dockempeak.txt',
                        'stdout_dockempeak.txt',
                        'stdout_dockemsoln.txt',
                        'searchdocIP123.dkm']
        for dockem_file in dockem_files:
            full_path = os.path.join(self.task.job_location,
                                     dockem_file)
            if os.path.exists(full_path):
                os.remove(full_path)
        self.task.run_pipeline(run=False,
                               db_inject=None)
        for jobs in self.task.pipeline.pipeline:
            for job in jobs:
                if job.name not in ['DockEM peak',
                                    'DockEM soln']:
                    job.set_status_finished()
        # Remove existing analyzer dock
        child_dock = self.findChild(QtGui.QDockWidget, self.analyzer_dock_name)
        child_dock.deleteLater()
        self.task.pipeline.start()
        self.pipeline = self.task.pipeline
        self.set_on_job_running()

    def view_raw_data(self):
        header = ('Solution / rank, x, y, z, ph1, ph2, phi, theta, psi, '
                    'sig above mean, CCC')
        # Copy above into peak_search_path
        import shutil
        peak_search_file = ('searchdocIP' +
                            self.args.job_prefix.value +
                            '.dkm')
        self.peak_search_path = os.path.join(self.task.job_location,
                                             peak_search_file)

        self.peak_output = os.path.join(self.task.job_location,
                                        'peak_output.txt')
        shutil.copyfile(self.peak_search_path, self.peak_output)
        with open(self.peak_output, 'r+') as f:
            content = f.read()
            f.seek(0, 0)
            f.write(header.rstrip('\r\n') + '\n' + content)
        ccpem_widgets.launch_desktop_services(path=self.peak_output)

    def run_chimera_custom(self):
        '''
        If job is finished show specified outputs
        '''
        if self.status == 'finished':
            self.chimera_view_setup()
        else:
            self.launcher.run_selected_chimera()

    def set_on_job_running_custom(self):
        if self.review_dock is not None:
            self.review_dock.deleteLater()
            self.review_dock = None
 
        # Mask map
        self.launcher.add_file(
            arg_name='search_object_mask',
            description=self.args.search_object_mask.help,
            file_type='map',
            selected=True)
        # Cropped ROI map
        self.launcher.add_file(
            description='Map cropped to region on interest',
            file_type='map',
            path=self.task.roi_crop_map,
            display_from='running',
            selected=True)
        if len(self.launcher.files) > 0:
            self.launcher_dock.setVisible(True)
            self.launcher.set_tree_view()

    def set_on_job_finish(self):
        '''
        Re-implement to allow GUI to be edited whilst processing files.
        '''
        # Allow args to be re-editable if in setup mode
        if self.args.run_search():
            self.status_widget.set_finished()
            self.tb_run_button.setEnabled(False)
            self.disable_args_widgets()
            self.tb_browser_button.setEnabled(True)
            self.tb_terminal_button.setEnabled(True)
            self.tb_stop_button.setEnabled(False)
        else:
            self.status_widget.set_finished()
            self.tb_run_button.setEnabled(True)
            self.disable_args_widgets(disable=False)
            self.tb_browser_button.setEnabled(True)
            self.tb_terminal_button.setEnabled(True)
            self.tb_stop_button.setEnabled(False)
        self.job_status_timer.stop()
        self.status = 'finished'
        # XXX check below...
#         self.pipeline_view.pipeline_widget.set_pipeline(
#             pipeline=self.task.pipeline)
#         self.pipeline_dock.setVisible(True)
        self.pipeline_dock.raise_()
        if len(self.launcher.files) > 0:
            self.launcher_dock.setVisible(True)
            self.launcher.set_tree_view()
        self.set_on_job_finish_custom()
 
    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.
        '''
        self.has_run = True
        if self.args.run_search():
            self.set_analyzer_ui()
            self.analyzer_dock.setVisible(True)
            self.analyzer_dock.raise_()
        else:
            self.set_review_ui()
            self.review_dock.setVisible(True)
            self.review_dock.raise_()

    def run_detached_process(self, job_id=None):
        '''
        Re-implement from window utils to stop creation of new job directory
        '''
        db_inject = None
        if self.database_path is not None:
            db_inject = sqlite_project_database.CCPEMInjector(
                database_path=self.database_path)
        self.task.run_task(set_job_location=not self.has_run,
                           job_id=job_id,
                           db_inject=db_inject)
        self.pipeline = self.task.pipeline
        self.set_on_job_running()

    def recreate_process(self):
        if self.task.job_location is not None:
            self.dockem_log_path = os.path.join(self.task.job_location,
                                                'dockem.log')
        self.check_job_status()

def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=shape_em_task.ShapeEM,
        window_class=ShapeEMMainWindow)

if __name__ == '__main__':
    main()
