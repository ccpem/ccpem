#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from PyQt4 import QtGui, QtCore, QtWebKit
from ccpem_gui.utils import window_utils
from ccpem_core.settings import which
from ccpem_gui.utils import command_line_launch
from ccpem_core.tasks.cryoEF import cryoEF_task
import shutil
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import cryoEF as test_data

class cryoEFWindow(window_utils.CCPEMTaskWindow):
    '''
    cryoEF interface window
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')
    def __init__(self,
                 task,
                 parent=None):
        super(cryoEFWindow, self).__init__(task=task,
                                             parent=parent)

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)
        
        # input options
        self.text_or_star = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='input_selection',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.text_or_star)
        self.text_or_star.value_line.currentIndexChanged.connect(
            self.set_input_options)
        
        # Input angles
        self.angles_input = window_utils.FileArgInput(
            parent=self,
            arg_name='angles_file_path',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.angles_input)
        
        #Input angles data file
        self.angles_star_file = window_utils.FileArgInput(
            parent=self,
            arg_name='angles_star_file',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.angles_star_file)
        
        #Input angles data file
        self.number_angles = window_utils.NumberArgInput(
            parent=self,
            arg_name='number_subset',
            maximum=50000,
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.number_angles)
        
        #Particle symmetry
        self.particle_symmetry = window_utils.StrArgInput(
            parent=self,
            arg_name='particle_symmetry',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.particle_symmetry)
        
        #particle diameter
        self.particle_diameter = window_utils.NumberArgInput(
            parent=self,
            arg_name='particle_diameter',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.particle_diameter)
        
        #Extended options
        self.options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Extended options',
            button_tooltip='Additional input options')
        self.args_widget.args_layout.addLayout(self.options_frame)
        #b-factor estimate
        self.bfactor = window_utils.NumberArgInput(
            parent=self,
            arg_name='bfactor',
            required=False,
            args=self.args)
        self.options_frame.add_extension_widget(self.bfactor)
        #Particle box size
        self.particle_box_size = window_utils.NumberArgInput(
            parent=self,
            arg_name='particle_box_size',
            required=False,
            args=self.args)
        self.options_frame.add_extension_widget(self.particle_box_size)
        #Particle box padding
        self.particle_box_padding = window_utils.NumberArgInput(
            parent=self,
            arg_name='particle_box_padding',
            required=False,
            args=self.args)
        self.options_frame.add_extension_widget(self.particle_box_padding)
        #resolution estimate
        self.resolution = window_utils.NumberArgInput(
            parent=self,
            arg_name='resolution',
            required=False,
            args=self.args)
        self.options_frame.add_extension_widget(self.resolution)
        
        #angular accuracy
        self.angular_accuracy = window_utils.NumberArgInput(
            parent=self,
            arg_name='angular_accuracy',
            required=False,
            args=self.args)
        self.options_frame.add_extension_widget(self.angular_accuracy)
        
        #max tilt angle
        self.max_tilt = window_utils.NumberArgInput(
            parent=self,
            arg_name='max_tilt',
            required=False,
            args=self.args)
        self.options_frame.add_extension_widget(self.max_tilt)
        
        self.set_input_options()
        
    def set_input_options(self):
        if self.args.input_selection.value == 'text file':
            self.angles_input.show()
            self.angles_star_file.hide()
            self.number_angles.hide()
            self.angles_input.set_required(True)
            self.angles_star_file.required=False
            self.angles_star_file.set_required(False)
        else:
            self.angles_star_file.show()
            self.number_angles.show()
            self.angles_input.hide()
            self.angles_star_file.set_required(True)
            self.angles_input.required=False
            self.angles_input.set_required(False)
            
            
    def set_on_job_running_custom(self):
        if os.path.exists(self.task.args.angles_file_path.value):
            self.launcher.add_file(
                arg_name=None,
                path=self.task.args.angles_file_path.value,
                description='Input angles file',
                selected=False)
            
    def set_on_job_finish_custom(self):
        # Add output files to launcher
        if os.path.exists(self.task.args.angles_file_path.value):
            basename = os.path.basename(self.task.args.angles_file_path.value)
            file_basename = '.'.join(basename.split('.')[:-1])
            log_file = os.path.join(os.path.dirname(self.task.args.angles_file_path.value),
                                    file_basename+'.log')
            #if self.args.input_selection.value == 'text file':
            if os.path.dirname(log_file) != self.task.job_location:
                shutil.copyfile(log_file,os.path.join(self.task.job_location,os.path.basename(log_file)))
            
            if os.path.exists(log_file):
                self.launcher.add_file(
                    arg_name=None,
                    path=log_file,
                    description='Log file',
                    selected=False)
            fourier_map = os.path.join(os.path.dirname(self.task.args.angles_file_path.value),
                                    file_basename+'_K.mrc')
            if os.path.exists(log_file):
                self.launcher.add_file(
                    arg_name=None,
                    path=fourier_map,
                    file_type='map',
                    description='Fourier space coverage map',
                    selected=True)
            psf_map = os.path.join(os.path.dirname(self.task.args.angles_file_path.value),
                                    file_basename+'_R.mrc')
            if os.path.exists(log_file):
                self.launcher.add_file(
                    arg_name=None,
                    path=psf_map,
                    file_type='map',
                    description='Point Spread Function map',
                    selected=True)
            angles_plot = os.path.join(self.task.job_location,"angles.png")
            if os.path.exists(angles_plot):
                self.launcher.add_file(
                    arg_name=None,
                    path=angles_plot,
                    description='Angle distribution plot',
                    selected=False)
        self.launcher.set_tree_view()
        self.launcher_dock.raise_()
        self.launcher_dock.show()
        self.set_rv_ui()
    
    def set_rv_ui(self):
        '''
        RVAPI results viewer.
        '''
        # Create results if not already done
        rv_index = os.path.join(
            self.task.job_location,
            'rvapi_data/index.html')
        if os.path.exists(rv_index):
            self.rv_view = QtWebKit.QWebView()
            assert os.path.exists(rv_index)
            self.rv_view.load(QtCore.QUrl(rv_index))
            results_dock_layout = QtGui.QVBoxLayout()
            results_dock_widget = QtGui.QWidget()
            results_dock_widget.setLayout(results_dock_layout)
            self.results_dock = QtGui.QDockWidget('Results',
                                                   self,
                                                   QtCore.Qt.Widget)
            
            self.results_dock.setToolTip('')
            self.results_dock.setWidget(results_dock_widget)#self.rv_view)
            self.tabifyDockWidget(self.setup_dock, self.results_dock)
            results_dock_layout.addWidget(self.rv_view)
            self.results_dock.raise_()

def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=cryoEF_task.cryoEF,
        window_class=cryoEFWindow)

if __name__ == '__main__':
    main()