#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import os

from PyQt4 import QtCore, QtGui

from ccpem_gui.utils import window_utils
from ccpem_core.tasks.model2map import model2map_task
from ccpem_core.settings import which
from ccpem_gui.utils import command_line_launch
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_core.test_data.tasks import model2map as test_data
from ccpem_core.process_manager import job_register
from ccpem_gui.project_database import sqlite_project_database
from ccpem_core.tasks.map_process import mapprocess_task
from ccpem_gui.tasks.map_process import mapprocess_window


class Model2MapWindow(window_utils.CCPEMTaskWindow):
    '''
    TEMPy Difference map window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(Model2MapWindow, self).__init__(task=task,
                                                  parent=parent)
        self.output_pdb = None

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)
        
        # Input pdb
        self.pdb_input = window_utils.FileArgInput(
            parent=self,
            arg_name='pdb_path',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.pdb_input)
        # Map resolution
        self.map_resolution_1 = window_utils.NumberArgInput(
            parent=self,
            arg_name='map_resolution',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_resolution_1)
        # Ligand library input for Refmac5
        self.lib_input = window_utils.FileArgInput(
            parent=self,
            arg_name='lib_in',
            args=self.args,
            file_types=ccpem_file_types.lib_ext,
            required=False)
        self.args_widget.args_layout.addWidget(self.lib_input)
        #simulated map apix
        self.map_apix = window_utils.NumberArgInput(
            parent=self,
            arg_name='map_apix',
            decimals=4,
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_apix)
        # Input map 1
        self.map_input_1 = window_utils.FileArgInput(
            parent=self,
            arg_name='map_path',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_input_1)


    def set_on_job_running_custom(self):
        # Add files to launcher
        self.launcher.add_file(
            arg_name='pdb_path',
            file_type='pdb',
            description=self.args.pdb_path.help,
            selected=True)

    def set_on_job_finish_custom(self):
        # Add output files to launcher
        self.launcher.add_file(
            arg_name=None,
            path=self.get_synthetic_map_name(),
            file_type='map',
            description='Synthetic map from PDB',
            selected=True)
        self.launcher.set_tree_view()
        self.launcher_dock.raise_()
        self.launcher_dock.show()

    def get_synthetic_map_name(self):
        syn_map_path = os.path.basename(self.args.pdb_path.value).split('.')[0]
        syn_map_path = os.path.join(self.task.job_location,
                                    syn_map_path + '_syn.mrc')
        return syn_map_path

def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=model2map_task.Model2Map,
        window_class=Model2MapWindow)

if __name__ == '__main__':
    main()