#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import shutil, glob
import subprocess
import time

import pyrvapi
from PyQt4 import QtCore, QtGui, QtWebKit
from ccpem_gui.utils import command_line_launch
from ccpem_gui.utils import window_utils, gui_process
from ccpem_core.tasks.atomic_model_validation import validate_task
from ccpem_core.settings import which
from ccpem_core.process_manager import process_utils
from ccpem_gui.utils.pdb_utils import *
from ccpem_core.tasks.atomic_model_validation import validate_results
from ccpem_core import ccpem_utils
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import model_validation as test_data


class ValidateWindow(window_utils.CCPEMTaskWindow):
    '''
    Model Validation window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(ValidateWindow, self).__init__(task=task,
                                                  parent=parent)
        self.output_pdb = None

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)
        # Input pdb(s)
        self.input_pdbs = MultiPDBInput(
            group_label='Input atomic model(s)',
            pdbs_arg=self.args.input_pdbs,
            chains_arg=self.args.input_pdb_chains,
            parent=self)
        self.args_widget.args_layout.addWidget(self.input_pdbs)
#         self.pdb_input = window_utils.FileArgInput(
#             parent=self,
#             arg_name='pdb_path',
#             required=True,
#             args=self.args)
#         self.args_widget.args_layout.addWidget(self.pdb_input)
        # Input map (optional)
        self.map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='map_path',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_input)
        self.map_input.value_line.editingFinished.connect(
                        self.check_input_for_methods)
        # Map resolution
        self.map_resolution = window_utils.NumberArgInput(
            parent=self,
            arg_name='map_resolution',
            maximum = 50.5,
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_resolution)
#         # -> Use Refmac for model-map conversion
#         self.use_refmac = window_utils.CheckArgInput(
#             parent=self,
#             arg_name='use_refmac',
#             args=self.args,
#             label_width=245)
#         self.args_widget.args_layout.addWidget(self.use_refmac)
#         self.use_refmac.value_line.stateChanged.connect(self.set_modelmap)
        # Ligand library input for Refmac5
        self.lib_input = window_utils.FileArgInput(
            parent=self,
            arg_name='lib_in',
            args=self.args,
            file_types=ccpem_file_types.lib_ext,
            required=False)
        self.args_widget.args_layout.addWidget(self.lib_input)

        self.half_map_1_input = window_utils.FileArgInput(
            parent=self,
            arg_name='half_map_1',
            args=self.args,
            required=False)
        self.args_widget.args_layout.addWidget(self.half_map_1_input)
        self.half_map_2_input = window_utils.FileArgInput(
            parent=self,
            arg_name='half_map_2',
            args=self.args,
            required=False)
        self.args_widget.args_layout.addWidget(self.half_map_2_input)
        #validation methods
        method_options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Method Selection',
            button_tooltip=('Select validation tools to run'))
        self.args_widget.args_layout.addLayout(method_options_frame)
        self.set_methods_button()
        
        # Map contour level
        self.map_contour_level = window_utils.NumberArgInput(
            parent=self,
            arg_name='map_contour_level',
            required=False,
            args=self.args,
            decimals=6)
        self.args_widget.args_layout.addWidget(self.map_contour_level)
        
        self.check_input_for_methods()
        #add to layout
        method_options_frame.add_extension_widget(self.run_molprobity)
        method_options_frame.add_extension_widget(self.run_cablam)
        method_options_frame.add_extension_widget(self.run_piscore)
        method_options_frame.add_extension_widget(self.run_scores)
        method_options_frame.add_extension_widget(self.run_smoc)
        method_options_frame.add_extension_widget(self.run_fdr)
#         method_options_frame.add_extension_widget(self.run_fsc)
#         method_options_frame.add_extension_widget(self.run_diffmap)
        method_options_frame.add_extension_widget(self.run_refmac)
        method_options_frame.add_extension_widget(self.run_jpred)
        
        
        self.run_scores.value_line.stateChanged.connect(self.set_run_methods)
        self.run_smoc.value_line.stateChanged.connect(self.set_run_methods)
        self.run_fdr.value_line.stateChanged.connect(self.set_run_methods)
#         self.run_fsc.value_line.stateChanged.connect(self.set_run_methods)
#         self.run_diffmap.value_line.stateChanged.connect(self.set_run_methods)
#         self.run_jpred.value_line.stateChanged.connect(self.set_run_methods)
        
        self.map_resolution.value_line.editingFinished.connect(
                        self.check_input_for_methods)
        # N cpu
        self.ncpu = window_utils.NumberArgInput(
            parent=self,
            arg_name='ncpu',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.ncpu)
        
        # generate results in parallel
        self.results_parallel = window_utils.CheckArgInput(
            parent=self,
            arg_name='results_parallel',
            args=self.args,
            label_width=170)
        self.args_widget.args_layout.addWidget(self.results_parallel)
        
        self.handle_run_results_in_parallel()
        #self.set_modelmap()
        
    def set_methods_button(self):
        self.run_molprobity = window_utils.CheckArgInput(
            parent=self,
            arg_name='run_molprobity',
            args=self.args,
            label_width=220)#=min(200,len(self.task.args.run_molprobity.metavar)*15))
        self.run_cablam = window_utils.CheckArgInput(
            parent=self,
            arg_name='run_cablam',
            args=self.args,
            label_width=220)#=min(200,len(self.task.args.run_cablam.metavar)*15))
        self.run_piscore = window_utils.CheckArgInput(
            parent=self,
            arg_name='run_piscore',
            args=self.args,
            label_width=220)#=min(200,len(self.task.args.run_cablam.metavar)*15))
        self.run_scores = window_utils.CheckArgInput(
            parent=self,
            arg_name='run_scores',
            args=self.args,
            label_width=220)#=min(200,len(self.task.args.run_scores.metavar)*15))
        self.run_smoc = window_utils.CheckArgInput(
            parent=self,
            arg_name='run_smoc',
            args=self.args,
            label_width=220)#=min(200,len(self.task.args.run_smoc.metavar)*15))
#         self.run_fsc = window_utils.CheckArgInput(
#             parent=self,
#             arg_name='run_fsc',
#             args=self.args)
        self.run_refmac = window_utils.CheckArgInput(
            parent=self,
            arg_name='run_refmac',
            args=self.args,
            label_width=220)#=min(200,len(self.task.args.run_refmac.metavar)*15))
        self.run_fdr = window_utils.CheckArgInput(
            parent=self,
            arg_name='run_fdr',
            args=self.args,
            label_width=220)#min(200,len(self.task.args.run_fdr.metavar)*15))
        self.run_jpred = window_utils.CheckArgInput(
            parent=self,
            arg_name='run_jpred',
            args=self.args,
            label_width=220)#min(200,len(self.task.args.run_jpred.metavar)*15))
        #self.run_molprobity.value_line.setChecked(True)
        #self.run_cablam.value_line.setChecked(True)
        #TODO: check why the checkboxes are cropped by default
        #adjusting height
        self.run_molprobity.value_line.setStyleSheet(
                                "QCheckBox::indicator { height: 22px;}")       
        self.run_cablam.value_line.setStyleSheet(
                                "QCheckBox::indicator { height: 22px;}")
        self.run_piscore.value_line.setStyleSheet(
                                "QCheckBox::indicator { height: 22px;}")
        self.run_scores.value_line.setStyleSheet(
                                "QCheckBox::indicator { height: 22px;}")
        self.run_smoc.value_line.setStyleSheet(
                                "QCheckBox::indicator { height: 22px;}")
        self.run_refmac.value_line.setStyleSheet(
                                "QCheckBox::indicator { height: 22px;}")
        self.run_fdr.value_line.setStyleSheet(
                                "QCheckBox::indicator { height: 22px;}")
#         self.run_fsc.value_line.setStyleSheet(
#                                 "QCheckBox::indicator { height: 22px;}")
        self.run_jpred.value_line.setStyleSheet(
                                "QCheckBox::indicator { height: 22px;}")

#     def set_modelmap(self):
#         if self.use_refmac.value_line.isChecked():
#             self.task.args.use_refmac.value = True
#             self.lib_input.show()
#         else:
#             self.task.args.use_refmac.value = False
#             self.lib_input.hide()

    def check_input_for_methods(self):
        if self.task.args.map_path.value is None or \
            self.task.args.map_resolution.value is None:
            self.run_scores.setEnabled(False)
            self.run_smoc.setEnabled(False)
            self.run_fdr.setEnabled(False)
#             self.run_fsc.setEnabled(False)
#             self.run_diffmap.setEnabled(False)
            self.run_refmac.setEnabled(False)
#             self.use_refmac.setEnabled(False)
#             self.task.args.use_refmac.value = False
            self.task.args.map_contour_level.value = None
            self.map_contour_level.hide()
        else:
            self.map_resolution.required=True
            self.map_resolution.set_required(True)
            if self.task.args.map_resolution.value is not None:
                self.run_scores.setEnabled(True)
                self.run_smoc.setEnabled(True)
                self.run_fdr.setEnabled(True)
    #             self.run_fsc.setEnabled(True)
    #             self.run_diffmap.setEnabled(True)
    
                self.run_refmac.setEnabled(True)
#             self.use_refmac.setEnabled(True)
#             if self.use_refmac.value_line.isChecked():
#                 self.task.args.use_refmac.value = True
            if self.run_scores.value_line.isChecked():
                self.map_contour_level.show()
        
    def set_run_methods(self):
        if self.run_smoc.value_line.isChecked():
            self.handle_run_tempy_smoc()
        else: self.task.args.run_smoc.value = False
        if self.run_fdr.value_line.isChecked():
            self.handle_run_fdr()
        else: self.task.args.run_fdr.value = False
#         if self.run_fsc.value_line.isChecked():
#             self.handle_run_fsc()
#         else: self.task.args.run_fsc.value = False
#         if self.run_diffmap.value_line.isChecked():
#             self.handle_run_diffmap()
#         else: self.task.args.run_diffmap.value = False
        if self.run_scores.value_line.isChecked():
            self.handle_run_tempy_scores()
            self.map_contour_level.show()
            
        else: 
            self.task.args.run_scores.value = False
            self.task.args.map_contour_level.value = None
            self.map_contour_level.hide()
            
        if self.run_jpred.value_line.isChecked():
            self.handle_run_jpred()
        else: self.task.args.run_jpred.value = False
        if self.run_refmac.value_line.isChecked():
            self.handle_run_refmac()
        else: self.task.args.run_refmac.value = False
        
    def handle_run_molprobity(self):
        self.task.args.run_molprobity.value = True
    def handle_run_jpred(self):
        self.task.args.run_jpred.value = True
    def handle_run_refmac(self):
        self.task.args.run_refmac.value = True
    def handle_run_fdr(self):
        self.task.args.run_fdr.value = True
#     def handle_run_fsc(self):
#         self.task.args.run_fsc.value = True
#     def handle_run_diffmap(self):
#         self.task.args.run_diffmap.value = True
    def handle_run_tempy_smoc(self):
        self.task.args.run_smoc.value = True
    def handle_run_tempy_scores(self):
        self.task.args.run_scores.value = True
        
    def handle_run_results_in_parallel(self):
        if self.results_parallel.value_line.isChecked():
            self.task.args.results_parallel.value = True
        else:
            self.task.args.results_parallel.value = False
#         if self.task.args.input_pdbs.value is None:
#             self.task.args.input_pdbs.value = [self.task.args.pdb_path.value]
#         window = smoc.SMOCMapWindow(
#             parent=self,
#             task=self.task)
#         if self.task.args.map_path.value is not None:
#             window.map_input.select_button.setDisabled(True)
#         self.smoc_dialog = QtGui.QDialog()
#         smoc_dialog_layout = QtGui.QVBoxLayout()
#         self.smoc_dialog.setLayout(smoc_dialog_layout)
#         smoc_dialog_layout.addWidget(window.args_widget)
#         self.smoc_dialog.setMinimumSize(600, 500)
#         self.smoc_dialog.show()

    def validate_input(self):
        '''
        Override from baseclass to allow validation of multiple datasets
        '''
        ready = super(ValidateWindow, self).validate_input()
        if ready:
            # Validate inputs for multiple datasets
            if not self.input_pdbs.validate_datasets():
                ready = False
            else:
                self.input_pdbs.set_args()
        return ready

#     def set_on_job_running_custom(self):
#         self.launcher.add_file(
#             arg_name='pdb_path',
#             file_type='pdb',
#             description=self.args.pdb_path.help,
#             selected=True)
    def initiate_results(self):
#         # setup doc
#         ccp4 = os.environ['CCPEM']
#         share_jsrview = os.path.join(ccp4, 'share', 'jsrview')
#         self.directory = os.path.join(self.task.job_location, 'report')
#         self.index = os.path.join(self.directory, 'index.html')
#         
#         if os.path.exists(self.directory):
#             shutil.rmtree(self.directory)
#         ccpem_utils.check_directory_and_make(self.directory)
#         #setup pages
#         pyrvapi.rvapi_init_document(self.task.job_location, self.directory,
#                                     self.task.job_location, 1, 4, share_jsrview,
#                                     None, None, None, None)
#         pyrvapi.rvapi_flush()
        #run script to generate results
        results_scriptfile = validate_results.__file__
        run_command = ['ccpem-python',results_scriptfile]
        run_command.append(os.path.join(self.task.job_location,'task.ccpem'))
        subprocess.Popen(run_command)
        
    def set_rv_ui(self):
        '''
        RVAPI results viewer.
        '''
        
        # Create results if not already done
        rv_index = os.path.join(
            self.task.job_location,
            'report/index.html')
#         if not os.path.exists(rv_index):
#             self.initiate_results()
        results_dock_widget = QtGui.QWidget()
        self.results_dock_layout = QtGui.QVBoxLayout()
        results_dock_widget.setLayout(self.results_dock_layout)
        self.results_dock = QtGui.QDockWidget('Results',
                                               self,
                                               QtCore.Qt.Widget)
        
        self.results_dock.setToolTip('') #removed tooltip text
        self.results_dock.setWidget(results_dock_widget)#self.rv_view)
        self.tabifyDockWidget(self.setup_dock, self.results_dock)

    
    def set_rv_view(self,rv_index):
        self.rv_view = QtWebKit.QWebView()
        self.rv_view.load(QtCore.QUrl(rv_index))
        self.rv_view.setMinimumHeight(300)
        rvapi_layout = QtGui.QHBoxLayout()
        self.rv_view.setLayout(rvapi_layout)
        
    def count_jobs_failed(self):
        '''
        Count fraction of failed jobs in the pipeline
        '''
        count_failed = 0
        count_jobs = 0
        for stage in self.task.pipeline.json_pl:
            for job in stage:
                status = process_utils.get_process_status(json_filepath=job)
                if status == 'failed':
                    count_failed += 1
                count_jobs += 1
        
        return float(count_failed)/count_jobs
    #check if only a few jobs failed
    def set_on_job_failed_custom(self):
        count_failed = 0
        count_finish = 0
        count_jobs = 0
        for stage in self.task.pipeline.json_pl:
            for job in stage:
                status = process_utils.get_process_status(json_filepath=job)
                if status == 'failed':
                    count_failed += 1
                elif status == 'finished':
                    count_finish += 1
                count_jobs += 1
        
        #if count_failed > 0 and float(count_failed)/count_jobs <= 0.25:
        if count_finish > 0:
            try: self.set_on_job_finish_custom()
            except AttributeError: pass
    
    def set_on_job_running_custom(self):
        #set pdb and map ids
        self.task.set_model_ids()        
#         frac_failed = self.count_jobs_failed()
#         
#         if frac_failed < 0.8: 
        # Add output files to launcher
        for pdb in self.task.list_pdbs:
            if os.path.isfile(pdb):
                self.launcher.add_file(
                    arg_name=None,
                    path=pdb,
                    description='Input atomic model',
                    selected=True)

        self.launcher.set_tree_view()
        self.launcher_dock.raise_()
        self.launcher_dock.show()
        #set Results tab
        rv_index = os.path.join(
            self.task.job_location,
            'report/index.html')
        if self.task.args.results_parallel.value:
            if not os.path.exists(rv_index):
                self.initiate_results()
            if not os.path.exists(rv_index): time.sleep(2)
            self.set_rv_ui()
            if not os.path.exists(rv_index): time.sleep(2)
            if os.path.exists(rv_index):
                self.set_rv_view(rv_index)
                self.results_dock_layout.addWidget(self.rv_view)
            else:
                print 'If jobs are successful, results will appear if you re-open this window'
            
        #show results tab upon finish
        if hasattr(self, 'results_dock'):
            self.results_dock.show()
            self.results_dock.raise_()

        
    def set_on_job_finish_custom(self):
        self.input_pdbs.disable()
        rv_index = os.path.join(
                self.task.job_location,
                'report/index.html')
        if os.path.exists(rv_index):
            self.set_rv_view(rv_index)
        if not self.task.args.results_parallel.value:
            # Create results if not already done
            self.set_rv_ui()
            if os.path.exists(rv_index):
                self.results_dock_layout.addWidget(self.rv_view)
                
        #set files on launcher
        for pdbid in self.task.list_pdbids:
            reduce_outpdb = os.path.join(self.task.job_location,
                                         pdbid,'reduce_out.pdb')
            if os.path.isfile(reduce_outpdb):
                self.launcher.add_file(
                    arg_name=None,
                    path=reduce_outpdb,
                    file_type='pdb',
                    description='Reduce output: '+pdbid,
                    selected=False)
            molprobity_output_file = os.path.join(
                self.task.job_location,pdbid,
                'molprobity.out')
            #rename .out file to .txt
            renamed_output = os.path.splitext(molprobity_output_file)[0]+'.txt'
            if os.path.isfile(molprobity_output_file):
                shutil.move(molprobity_output_file,renamed_output)
            if os.path.isfile(renamed_output):
                self.launcher.add_file(
                    arg_name=None,
                    path=renamed_output,
                    file_type='standard',
                    description='Molprobity output:'+pdbid,
                    selected=False)
            coot_script = os.path.join(
                self.task.job_location,'validation_cootdata',
                'outliers_cootscript_'+pdbid+'.py')
            if os.path.isfile(coot_script):
                self.launcher.add_file(
                    arg_name=None,
                    path=coot_script,
                    description='Molprobity coot script:'+pdbid,
                    selected=False)
            cablam_out = os.path.join(
                self.task.job_location,pdbid,
                'cablam'+pdbid+'_stdout.txt')
            if os.path.isfile(cablam_out):
                self.launcher.add_file(
                    arg_name=None,
                    path=cablam_out,
                    description='Cablam output:'+pdbid,
                    selected=False)
            for mapid in self.task.list_mapids:
                smoc_output = os.path.join(self.task.job_location,pdbid,mapid,
                                           'smoc_score.txt')
                if os.path.isfile(smoc_output):
                    self.launcher.add_file(
                        arg_name=None,
                        path=smoc_output,
                        description='SMOC scores: '+mapid+':'+pdbid,
                        selected=False)
                refmac_output = os.path.join(self.task.job_location,pdbid,
                                           'refmac'+mapid+':'+pdbid+'_stdout.txt')
                if os.path.isfile(refmac_output):
                    self.launcher.add_file(
                        arg_name=None,
                        path=refmac_output,
                        description='Refmac log: '+mapid+':'+pdbid,
                        selected=False)
                fdr_dir = os.path.join(self.task.job_location,mapid,pdbid)
                for ranked_residues in glob.glob(os.path.join(fdr_dir, '*_FDR_ranked_residues.csv')):

        #        output_model = os.path.join(self.pipeline.location, '*_BACKBONE_average.pdb')
                    self.launcher.add_file(
                        path=ranked_residues,
                        file_type='standard',
                        description='Residues with FDR scores',
                        selected=False)
                for output_model in glob.glob(os.path.join(fdr_dir, '*_FDR__attribute.txt')):

        #        output_model = os.path.join(self.pipeline.location, '*_BACKBONE_average.pdb')
                    self.launcher.add_file(
                        path=output_model,
                        file_type='standard',
                        description='Chimera attribute file (FDR)',
                        selected=False)
                            
            outlier_summary_file = os.path.join(
                self.task.job_location,
                pdbid+'_outliersummary.txt')
            if os.path.isfile(outlier_summary_file):
                self.launcher.add_file(
                    arg_name=None,
                    path=outlier_summary_file,
                    description='Outlier residue summary:'+pdbid,
                    selected=False)
        for map in self.task.list_maps:
            if os.path.isfile(map):
                self.launcher.add_file(
                    arg_name=None,
                    path=map,
                    description='Input map',
                    selected=True)
#     def run_coot_custom(self):
#         coot_bin = which('coot')
#         validation_coot_dir = os.path.join(self.task.job_location,
#                                             'validation_cootdata')
#         coot_script = os.path.join(validation_coot_dir,'outliers_cootscript.py')
#         if not os.path.isdir(validation_coot_dir) or \
#             not os.path.isfile(coot_script):
#             coot_script = os.path.join(self.task.job_location,
#                                     'molprobity_coot.py')
#         coot_args = [self.args.pdb_path.value]
#         coot_args += ['-c',coot_script]
#         gui_process.run_coot(coot_args)
        if self.task.args.run_molprobity.value or self.task.args.run_cablam.value or \
                self.task.args.run_smoc.value or self.task.args.run_jpred.value:
            self.set_cootscript()
            self.coot_button_widget = self.set_coot_buttons()
            
        try:
            if len(self.dict_cootscripts) > 0:
                self.results_dock_layout.addWidget(self.coot_button_widget)
        except AttributeError: pass
        
        
    def set_coot_buttons(self):
        coot_button_widget = QtGui.QWidget()
        coot_vert_layout = QtGui.QVBoxLayout()
        coot_button_widget.setLayout(coot_vert_layout)
        ct_pdb = 1
        ct_col = 1
        coot_horz_layout = QtGui.QHBoxLayout()
        for pdbid in self.task.list_pdbids:
            if not pdbid in self.dict_cootscripts: continue
            coot_view_button = QtGui.QPushButton("Fix "+pdbid)
            coot_view_button.setMaximumWidth(150)
            coot_view_button.setStyleSheet("background-color: orange")
            coot_view_button.setToolTip("Fix residue outliers in Coot")
            coot_view_button.clicked.connect(lambda checked, id=pdbid: self.handle_coot_view(id))
            coot_horz_layout.addWidget(coot_view_button)
            if ct_pdb%4 == 0:
                #print 'Adding new row of coot buttons'
                coot_vert_layout.addLayout(coot_horz_layout)
                coot_horz_layout = QtGui.QHBoxLayout()
                ct_col += 1
            ct_pdb += 1
        if (ct_pdb-1)%4 != 0 and ct_pdb > 1: 
            coot_vert_layout.addLayout(coot_horz_layout)
        return coot_button_widget
    
    def handle_coot_view(self,pdbid):
        coot_bin = which('coot')
        coot_script = self.dict_cootscripts[pdbid]
        if not os.path.isfile(coot_script):
            coot_script = os.path.join(self.task.job_location,
                                    'molprobity_coot.py')
        #print coot_script
        if os.path.isfile(coot_script):
            coot_args = ['--pdb',self.dict_cootpdbs[pdbid]]
            try: coot_args += ['--map',self.task.list_maps[0]]
            except (AttributeError,IndexError) as exc:
                pass
            coot_args += ['--script',coot_script]
            
            #print coot_args
            gui_process.run_coot(coot_args)
    
    def set_cootscript(self):
        self.dict_cootscripts = {}
        self.dict_cootpdbs = {}
        for pdbid in self.task.list_pdbids:
            cootscript_outliers = \
                os.path.join(self.task.job_location,
                            'validation_cootdata',
                            'outliers_cootscript_'+pdbid+'.py')
            if os.path.isfile(cootscript_outliers):
                self.dict_cootscripts[pdbid] = cootscript_outliers
        for p in range(len(self.task.list_pdbs)):
            self.dict_cootpdbs[self.task.list_pdbids[p]] = self.task.list_pdbs[p]
        #print self.dict_cootscripts

def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=validate_task.ValidateTask,
        window_class=ValidateWindow)

if __name__ == '__main__':
    main()