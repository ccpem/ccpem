#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


'''
Task window for Nautilus.
'''
import os

from PyQt4 import QtGui, QtCore, QtWebKit

from ccpem_core.tasks.nautilus import nautilus_task
from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_gui.utils import command_line_launch
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import nautilus as test_data


class NautilusWindow(window_utils.CCPEMTaskWindow):
    '''
    Nautilus window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(NautilusWindow, self).__init__(task=task,
                                             parent=parent)
        self.rv_timer = QtCore.QTimer()
        self.rv_timer.timeout.connect(self.set_rv_ui)
        self.rv_timer.start(1500)

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Input map
        self.map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_map',
            args=self.args,
            label='Input map',
            file_types=ccpem_file_types.mrc_ext,
            required=True)
        self.args_widget.args_layout.addWidget(self.map_input)

        # Half map vs full map refinement
        self.half_map_refinement_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='half_map_refinement',
            args=self.args,
            required=True)
        self.args_widget.args_layout.addWidget(
            self.half_map_refinement_input)
        self.half_map_refinement_input.value_line.currentIndexChanged.connect(
            self.set_maps_visible)
        
        self.half_map_1_input = window_utils.FileArgInput(
            parent=self,
            arg_name='half_map_1',
            args=self.args,
            required=False)
        self.args_widget.args_layout.addWidget(
            self.half_map_1_input)

        self.half_map_2_input = window_utils.FileArgInput(
            parent=self,
            arg_name='half_map_2',
            args=self.args,
            required=False)
        self.args_widget.args_layout.addWidget(
            self.half_map_2_input)

        self.build_from_nemap_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='build_from_nemap',
            label_width=380,
            label='Use calculated normalized expected map for model building',
            args=self.args,
            required=False)
        self.args_widget.args_layout.addWidget(
            self.build_from_nemap_input)
        self.build_from_nemap_input.value_line.currentIndexChanged.connect(
            self.set_options_visible)
        
        # Resolution
        resolution_input = window_utils.NumberArgInput(
            parent=self,
            decimals=1,
            step=0.1,
            arg_name='resolution',
            args=self.args,
            required=True)
        self.args_widget.args_layout.addWidget(resolution_input)

        # Map sharpen
        self.map_sharpen_input = window_utils.NumberArgInput(
            parent=self,
            decimals=1,
            minimum=-1000,
            maximum=1000,
            arg_name='map_sharpen',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_sharpen_input)

        # Input seq
        self.seq_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_seq',
            args=self.args,
            label='Input sequence',
            required=False)
        self.args_widget.args_layout.addWidget(self.seq_input)

        # Mask for sfcalc
        mask_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_mask',
            args=self.args,
            label='Input mask',
            required=False)
        self.args_widget.args_layout.addWidget(mask_input)
        mask_input.value_line.textChanged.connect(
            self.set_mask_for_fofc)
        
        # Option to use input mask as mask for Fo-Fc map calculation in Servalcat
        self.mask_for_fofc_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='mask_for_fofc',
            label_width=225,
            label='Use input mask for Fo-Fc calculation',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.mask_for_fofc_input)
        
        # Initial model input
        extend_model_input = window_utils.FileArgInput(
            parent=self,
            arg_name='extend_pdb',
            args=self.args,
            label='Extend model',
            file_types=ccpem_file_types.pdb_ext,
            required=False)
        self.args_widget.args_layout.addWidget(extend_model_input)

        # Number of Nautilus cycles
        ncycle_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='ncycle',
            args=self.args)
        self.args_widget.args_layout.addWidget(ncycle_input)

        # Pipeline internal cycles control
        pipeline_control_frame = window_utils.CCPEMExtensionFrame(
            button_name='Pipeline control',
            button_tooltip='Show pipeline controls for number of internal cycles')
        self.args_widget.args_layout.addLayout(pipeline_control_frame)

        # Set number of internal cycles in 1st Nautilus cycle
        nau1st_ncycle_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='ncycle_nau1st',
            args=self.args)
        pipeline_control_frame.add_extension_widget(nau1st_ncycle_input)

        # Set number of internal cycles in nth Nautilus cycles
        naunth_ncycle_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='ncycle_naunth',
            args=self.args)
        pipeline_control_frame.add_extension_widget(naunth_ncycle_input)

        # Set number of Refmac cycles in pipeline
        ref_ncyle_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='ncycle_refmac',
            args=self.args)
        pipeline_control_frame.add_extension_widget(ref_ncyle_input)

        # Trim maps option
        self.no_trim_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='no_trim',
            args=self.args)
        pipeline_control_frame.add_extension_widget(
            self.no_trim_input)

        # Use FreeR flags
        self.freer_on_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='freer_on',
            args=self.args)
        pipeline_control_frame.add_extension_widget(
            self.freer_on_input)
        
        # Masked refinement option
        self.masked_refinement_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='masked_refinement_on',
            args=self.args)
        pipeline_control_frame.add_extension_widget(
            self.masked_refinement_input)

        # Masked refinement on, mask radius
        self.mask_radius_input = window_utils.NumberArgInput(
            parent=self,
            decimals=1,
            step=0.5,
            minimum=0,
            required=False,
            arg_name='mask_radius',
            label='Mask radius',
            args=self.args)
        pipeline_control_frame.add_extension_widget(self.mask_radius_input)

        # Ligand library input for Refmac5
        lib_input = window_utils.FileArgInput(
            parent=self,
            label='Input ligand',
            arg_name='lib_in',
            args=self.args,
            file_types=ccpem_file_types.lib_ext,
            required=False)
        pipeline_control_frame.add_extension_widget(lib_input)

        # Extended options
        extended_options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Extended options',
            button_tooltip='Show extended options')
        self.args_widget.args_layout.addLayout(extended_options_frame)

        # Nautilus Keywords
        self.keyword_entry = window_utils.KeywordArgInput(
            parent=self,
            arg_name='keywords',
            args=self.args)
        extended_options_frame.add_extension_widget(self.keyword_entry)

        # refmac Keywords
        self.keyword_entry2 = window_utils.KeywordArgInput(
            parent=self,
            arg_name='refmac_keywords',
            args=self.args)
        extended_options_frame.add_extension_widget(self.keyword_entry2)

        # Set inputs for launcher
        if self.args.input_map.value is not None:
            self.launcher.add_file(
                arg_name='input_map',
                file_type='map',
                description=self.args.input_map.help,
                selected=False)
        self.launcher.add_file(
            arg_name='input_seq',
            file_type='standard',
            description=self.args.input_seq.help,
            selected=False)
        self.launcher.add_file(
            arg_name='extend_pdb',
            file_type='standard',
            description=self.args.extend_pdb.help,
            selected=True)
        if self.args.half_map_1.value is not None:
            self.launcher.add_file(
                arg_name='half_map_1',
                file_type='map',
                description='Input half map 1',
                selected=False)
            self.launcher.add_file(
                arg_name='half_map_2',
                file_type='map',
                description='Input half map 2',
                selected=False)
        if self.args.input_mask.value is not None:
            self.launcher.add_file(
                arg_name='input_mask',
                file_type='map',
                description='Input mask',
                selected=False)
        
        self.masked_refinement_input.value_line.currentIndexChanged.connect(
            self.set_mask_radius)
        self.set_mask_radius()
        self.set_maps_visible()
        self.set_options_visible()
        self.set_mask_for_fofc()
        
    def set_mask_radius(self):
        if self.args.masked_refinement_on.value:
            self.mask_radius_input.show()
        else:
            self.mask_radius_input.hide()

    def set_maps_visible(self):
        if self.args.half_map_refinement():
            self.half_map_1_input.show()
            self.half_map_2_input.show()
            self.build_from_nemap_input.show()
        else:
            self.half_map_1_input.hide()
            self.half_map_2_input.hide()
            self.build_from_nemap_input.hide()
            self.build_from_nemap_input.set_arg_value(value=False)

    def set_options_visible(self):
        if self.args.build_from_nemap():
            self.map_sharpen_input.hide()
            self.map_input.set_required(False)
            self.half_map_1_input.set_required(True)
            self.half_map_2_input.set_required(True)
        else:
            self.map_sharpen_input.show()
            self.map_input.set_required(True)
            self.half_map_1_input.set_required(False)
            self.half_map_2_input.set_required(False)

    def set_mask_for_fofc(self):
        if self.args.input_mask.value not in ['', None, 'None']:
            self.mask_for_fofc_input.show()
        else:
            self.mask_for_fofc_input.hide()
            
    def set_rv_ui(self):
        '''
        RVAPI results viewer.
        '''
        if hasattr(self.task, 'job_location'):
            if self.task.job_location is not None:
                report = os.path.join(self.task.job_location,
                                      'report/index.html')
                if os.path.exists(report):
                    self.rv_view = QtWebKit.QWebView()
                    self.rv_view.load(QtCore.QUrl(report))
                    self.results_dock = QtGui.QDockWidget('Results',
                                                          self,
                                                          QtCore.Qt.Widget)
                    self.results_dock.setToolTip('Results overview')
                    self.results_dock.setWidget(self.rv_view)
                    self.tabifyDockWidget(self.setup_dock, self.results_dock)
                    self.results_dock.show()
                    self.results_dock.raise_()
                    self.rv_timer.stop()

    def set_on_job_running_custom(self):
        # Structure factor from input map
        if hasattr(self.task, 'process_maptomtz'):
            self.launcher.add_file(
                arg_name=None,
                path=self.task.process_maptomtz.hklout_path,
                file_type='mtz',
                description='Structure factors from input map',
                selected=True,
                group='Output Files')
        # Trimmed input maps
        if not self.args.no_trim.value:
            if self.args.input_map.value is not None:
                if os.path.isfile(self.task.process_trim_maps.map_trim_path):
                    self.launcher.add_file(
                        arg_name=None,
                        path=self.task.process_trim_maps.map_trim_path,
                        file_type='map',
                        description='Trimmed input map',
                        selected=True,
                        group='Output Files')
            if self.args.half_map_1.value is not None:
                if os.path.isfile(self.task.process_trim_maps.hm1_trim_path):
                    self.launcher.add_file(
                        arg_name=None,
                        path=self.task.process_trim_maps.hm1_trim_path,
                        file_type='map',
                        description='Trimmed input halfmap 1',
                        selected=False,
                        group='Output Files')
                if os.path.isfile(self.task.process_trim_maps.hm2_trim_path):
                    self.launcher.add_file(
                        arg_name=None,
                        path=self.task.process_trim_maps.hm2_trim_path,
                        file_type='map',
                        description='Trimmed input halfmap 2',
                        selected=False,
                        group='Output Files')
            if os.path.isfile(self.task.process_trim_maps.mask_trim_path):
                self.launcher.add_file(
                    arg_name=None,
                    path=self.task.process_trim_maps.mask_trim_path,
                    file_type='map',
                    description='Trimmed starting mask',
                    selected=False,
                    group='Output Files')
        # Add PDB from last Nautilus cycle
        path = os.path.dirname(self.task.process_nautilus_pipeline.pdbout)
        pdbout_ext = os.path.splitext(self.task.process_nautilus_pipeline.pdbout)[-1]
        for i in range(1, (self.args.ncycle.value)):
            path = os.path.dirname(self.task.process_nautilus_pipeline.pdbout)
            fname = os.path.join(path, 'build' + str(i) + pdbout_ext)
            self.launcher.add_file(
                path=fname,
                file_type='pdb',
                description='Model built from Nautilus cycle #' + str(i),
                selected=False,
                group='Output Files')
            fname = os.path.join(path, 'refined' + str(i))
            self.launcher.add_file(
                path=fname + '.pdb',
                file_type='pdb',
                description='Refined model from Nautilus cycle #' +
                str(i),
                selected=False,
                group='Output Files')
            self.launcher.add_file(
                path=fname + '.mmcif',
                file_type='pdb',
                description='Refined model from Nautilus cycle #' +
                str(i),
                selected=False,
                group='Output Files')
        # needed this line to refresh the file launcher view
        self.launcher.set_tree_view()

    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.  For now show starting, refined
        pdb and experimental map.
        '''
        if hasattr(self.task, 'process_nautilus_pipeline'):
            if hasattr(self.task, 'process_refine'):
                self.launcher.add_file(
                    path=self.task.process_nautilus_pipeline.pdbout,
                    file_type='pdb',
                    description='Model built from final Nautilus cycle',
                    selected=False,
                    group='Output Files')
                self.launcher.add_file(
                    path=self.task.process_refine.pdbout_path,
                    file_type='pdb',
                    description='Final Nautilus built and refined model',
                    selected=True,
                    group='Output Files')
                
        if not self.args.no_trim.value:
            # add model files shifted back into original map (in /shiftback_models)
            path = os.path.dirname(self.task.process_nautilus_pipeline.pdbout)
            pdbout_ext = os.path.splitext(
                self.task.process_nautilus_pipeline.pdbout)[-1]
            for i in range(1, (self.args.ncycle.value) + 1):
                if i == (self.args.ncycle.value) + 1:
                    desc_txt = 'final Nautilus cycle'
                else:
                    desc_txt = 'Nautilus cycle #' + str(i)
                fname = os.path.join(path, 'shiftback_models/build' + str(i) +
                                     '_shiftback' + pdbout_ext)
                self.launcher.add_file(
                    path=fname,
                    file_type='pdb',
                    description='Model built from ' + desc_txt,
                    selected=False,
                    group='Models shifted back onto input map')
                fname = os.path.join(path, 'shiftback_models/refined' + str(i) +
                                     '_shiftback.pdb')
                self.launcher.add_file(
                    path=fname,
                    file_type='pdb',
                    description='Refined model from ' + desc_txt,
                    selected=False,
                    group='Models shifted back onto input map')
                fname = os.path.join(path, 'shiftback_models/refined' + str(i) +
                                     '_shiftback.mmcif')
                if os.path.isfile(self.task.process_refine.cifout_path):
                    self.launcher.add_file(
                        path=fname,
                        file_type='pdb',
                        description='Refined model from ' + desc_txt,
                        selected=False,
                        group='Models shifted back onto input map')
        self.launcher.set_tree_view()
        self.launcher_dock.raise_()


def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=nautilus_task.Nautilus,
        window_class=NautilusWindow)


if __name__ == '__main__':
    main()
