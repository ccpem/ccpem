#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Task window for Refmac5.
'''
import os
import textwrap

from PyQt4 import QtGui, QtCore, QtWebKit

from ccpem_core.tasks.refmac import refmac_task
from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_gui.utils import command_line_launch
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import refmac as test_data
from servalcat.utils import symmetry


class RefmacDataset(object):
    def __init__(self,
                 pdb_path=None,
                 map_path=None,
                 resolution=None):
        self.pdb_path = None
        self.map_path = None
        self.resolution = None
        self.set_pdb_path(pdb_path)
        self.set_map_path(map_path)
        self.set_resolution(resolution)

    def set_pdb_path(self, pdb_path):
        if pdb_path is not None:
            self.pdb_path = str(pdb_path)

    def set_map_path(self, map_path):
        if map_path is not None:
            self.map_path = str(map_path)

    def set_resolution(self, resolution):
        if resolution is not None:
            self.resolution = float(resolution)

    def validate(self):
        if [self.pdb_path, self.map_path, self.resolution].count(None) == 0:
            return True
        else:
            return False


class extRestraintDataset(object):
    def __init__(self,
                 restraints_file=None,
                 external_weight_scale=None,
                 external_weight_gmwt=None):
        self.restraints_file = None
        self.external_weight_scale = 10.0
        self.external_weight_gmwt = 0.02
        self.set_restraints_file(restraints_file)
        self.set_external_weight_scale(external_weight_scale)
        self.set_external_weight_gmwt(external_weight_gmwt)

    def set_restraints_file(self, restraints_file):
        if restraints_file is not None:
            self.restraints_file = str(restraints_file)

    def set_external_weight_scale(self, external_weight_scale):
        if external_weight_scale is not None:
            self.external_weight_scale = float(external_weight_scale)

    def set_external_weight_gmwt(self, external_weight_gmwt):
        if external_weight_gmwt is not None:
            self.external_weight_gmwt = float(external_weight_gmwt)

    def validate(self):
        if [self.restraints_file, self.external_weight_scale, self.external_weight_gmwt].count(None) == 0:
            return True
        else:
            return False


class RefmacDatasetInput(QtGui.QWidget):
    def __init__(self,
                 pdb_path=None,
                 map_path=None,
                 resolution=None):
        super(RefmacDatasetInput, self).__init__()
        self.dataset = RefmacDataset()

        button_width = 100
        label_width = 150
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)

        # PDB
        pdb_box = QtGui.QHBoxLayout()
        self.pdb_label = QtGui.QLabel('PDB')
        self.pdb_label.setFixedWidth(label_width)
        pdb_box.addWidget(self.pdb_label)
        pdb_select = QtGui.QPushButton('Select')
        pdb_select.setFixedWidth(button_width)
        pdb_select.clicked.connect(self.get_pdb_file)
        pdb_box.addWidget(pdb_select)
        self.pdb_value = QtGui.QLineEdit()
        self.pdb_value.editingFinished.connect(self.edit_finished_pdb)
        pdb_box.addWidget(self.pdb_value)
        self.layout.addLayout(pdb_box)

        # Map
        map_box = QtGui.QHBoxLayout()
        self.map_label = QtGui.QLabel('Map')
        self.map_label.setFixedWidth(label_width)
        map_box.addWidget(self.map_label)
        map_select = QtGui.QPushButton('Select')
        map_select.setFixedWidth(button_width)
        map_select.clicked.connect(self.get_map_file)
        map_box.addWidget(map_select)
        self.map_value = QtGui.QLineEdit()
        map_box.addWidget(self.map_value)
        self.layout.addLayout(map_box)

        # Resolution
        res_box = QtGui.QHBoxLayout()
        self.res_label = QtGui.QLabel('Resolution')
        self.res_label.setFixedWidth(label_width)
        res_box.addWidget(self.res_label)
        self.res_value = QtGui.QDoubleSpinBox()
        self.res_value.setDecimals(2)
        self.res_value.setFixedWidth(button_width)
        self.res_value.setSpecialValueText('None')
        self.res_value.setSingleStep(0.1)
        self.res_value.valueChanged.connect(self.set_resolution)
        res_box.addWidget(self.res_value)
        res_box.setAlignment(self.res_value, QtCore.Qt.AlignLeft)

        # Remove
        remove_button = QtGui.QPushButton('Remove')
        remove_button.setFixedWidth(button_width)
        remove_button.setToolTip('Remove dataset')
        remove_button.clicked.connect(self.remove)
        res_box.addWidget(remove_button)
        res_box.setAlignment(res_box, QtCore.Qt.AlignRight)
        self.layout.addLayout(res_box)

        # Set initial values
        # PDB
        self.set_pdb_file(path=pdb_path)
        # Map
        self.set_map_file(path=map_path)
        # Resolution
        if resolution is None:
            resolution = 0
        self.res_value.setValue(resolution)
        self.set_resolution(resolution)

    def get_pdb_file(self):
        path = self.get_file_from_brower(file_types=ccpem_file_types.pdb_ext)
        self.set_pdb_file(path=path)

    def edit_finished_pdb(self):
        path = str(self.pdb_value.text())
        self.set_pdb_file(path=path)

    def get_map_file(self):
        path = self.get_file_from_brower(file_types=ccpem_file_types.mrc_ext)
        self.set_map_file(path=path)

    def edit_finished_map(self):
        path = self.map_value.text()
        self.set_map_file(path=path)

    def set_pdb_file(self, path):
        self.set_shading(self.pdb_label, shade=True)
        self.set_shading(self.pdb_value, shade=True)
        self.dataset.pdb_path = None
        if path is not None:
            if os.path.exists(path):
                self.pdb_value.setText(path)
                self.set_shading(self.pdb_label, shade=False)
                self.set_shading(self.pdb_value, shade=False)
                self.dataset.set_pdb_path(path)

    def set_map_file(self, path):
        self.set_shading(self.map_label, shade=True)
        self.set_shading(self.map_value, shade=True)
        self.dataset.map_path = None
        if path is not None:
            if os.path.exists(path):
                self.map_value.setText(path)
                self.set_shading(self.map_label, shade=False)
                self.set_shading(self.map_value, shade=False)
                self.dataset.set_map_path(map_path=path)

    @QtCore.pyqtSlot(int)
    def set_resolution(self, value):
        if value < 0.1:
            self.set_shading(self.res_value, shade=True)
            self.set_shading(self.res_label, shade=True)
            self.dataset.resolution = None
        else:
            self.set_shading(self.res_value, shade=False)
            self.set_shading(self.res_label, shade=False)
            self.dataset.set_resolution(resolution=value)

    def get_file_from_brower(self, file_types):
        dialog_path = window_utils.get_last_directory_browsed()
        path = QtGui.QFileDialog.getOpenFileName(
            self,
            'Open a File',
            QtCore.QDir.path(QtCore.QDir(dialog_path)),
            file_types)
        path = str(path)
        if path == '':
            path = None
        if path is not None:
            dialog_path = os.path.dirname(path)
            window_utils.set_last_directory_browsed(path=dialog_path)
        return path

    def set_shading(self, widget, shade=True):
        '''
        Shade text QLineEdit red to display warning.
        '''
        if shade:
            widget.setStyleSheet('color: red')
        else:
            widget.setStyleSheet('color: None')

    def remove(self):
        self.deleteLater()


class RefmacDatasetsInput(QtGui.QWidget):
    def __init__(self,
                 pdbs_arg=None,
                 maps_arg=None,
                 resolutions_arg=None,
                 parent=None):
        super(RefmacDatasetsInput, self).__init__()
        self.parent = parent
        width = 100

        # Layout
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)
        
        # Label
        ds_label = QtGui.QLabel('Input Datasets')
        self.layout.addWidget(ds_label)

        # Add dataset
        add_button = QtGui.QPushButton('Add')
        add_button.setFixedWidth(width)
        add_button.setToolTip('Add another dataset')
        add_button.clicked.connect(self.handle_add_button)
        self.layout.addWidget(add_button)

        # Set args
        pdbs = pdbs_arg()
        maps = maps_arg()
        resolutions = resolutions_arg()
        # Add black dataset
        if isinstance(pdbs, list):
            for n in xrange(len(pdbs)):
                self.add_dataset(
                    pdb_path=pdbs[n],
                    map_path=maps[n],
                    resolution=resolutions[n])
        else:
            self.add_dataset(
                pdb_path=pdbs,
                map_path=maps,
                resolution=resolutions)

    def handle_add_button(self):
        '''
        Handle dataset button; stop bool signal transfer
        '''
        self.add_dataset()

    def add_dataset(self,
                    pdb_path=None,
                    map_path=None,
                    resolution=None):
        dataset = RefmacDatasetInput(pdb_path=pdb_path,
                                     map_path=map_path,
                                     resolution=resolution)
        self.layout.addWidget(dataset)

    def validate_datasets(self):
        children = self.findChildren(RefmacDatasetInput)
        # Check at least one dataset
        if len(children) == 0:
            # Warn no datasets provided
            text = 'No input datasets provided'
            QtGui.QMessageBox.warning(self,
                'Error',
                text)
            return False
        warning = False
        # Check datasets are complete (must have pdb, map, resolution)
        for child in children:
            if not child.dataset.validate():
                warning = True
        if warning:
            # Warn datasets incomplete information
            text = 'Dataset(s) incomplete (requires PDB, map and resolution)'
            QtGui.QMessageBox.warning(self,
                'Error',
                text)
            return False
        else:
            return True

    def set_args(self, pdbs_arg, maps_arg, resolutions_arg):
        children = self.findChildren(RefmacDatasetInput)
        pdbs = []
        maps = []
        resolutions = []
        if len(children) > 1:
            for child in children:
                pdbs.append(child.dataset.pdb_path)
                maps.append(child.dataset.map_path)
                resolutions.append(child.dataset.resolution)
            pdbs_arg.value = pdbs
            maps_arg.value = maps
            resolutions_arg.value = resolutions
        else:
            child = children[0]
            pdbs_arg.value = child.dataset.pdb_path
            maps_arg.value = child.dataset.map_path
            resolutions_arg.value = child.dataset.resolution


class extRestraintInput(QtGui.QWidget):
    def __init__(self,
                 restraints_file=None,
                 external_weight_scale=None,
                 external_weight_gmwt=None):
        super(extRestraintInput, self).__init__()
        self.dataset = extRestraintDataset()

        button_width = 100
        label_width = 150
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)

        def set_tooltip(widget, text):
            widget.setToolTip(textwrap.fill(text, 60))

        # restraints_file
        restraints_file_widget = QtGui.QWidget()
        set_tooltip(restraints_file_widget,
                    'File containing additional restraints, for example as produced by'
                    ' ProSMART')
        restraints_file_box = QtGui.QHBoxLayout()
        restraints_file_box.setContentsMargins(0, 0, 0, 0)
        self.restraints_file_label = QtGui.QLabel('Restraints file')
        self.restraints_file_label.setFixedWidth(label_width)
        restraints_file_box.addWidget(self.restraints_file_label)
        restraints_file_select = QtGui.QPushButton('Select')
        restraints_file_select.setFixedWidth(button_width)
        restraints_file_select.clicked.connect(self.get_restraints_file)
        restraints_file_box.addWidget(restraints_file_select)
        self.restraints_file_value = QtGui.QLineEdit()
        self.restraints_file_value.editingFinished.connect(self.edit_finished_restraints_file)
        restraints_file_box.addWidget(self.restraints_file_value)
        restraints_file_widget.setLayout(restraints_file_box)
        self.layout.addWidget(restraints_file_widget)

        # external_weight_scale
        ext_scale_widget = QtGui.QWidget()
        set_tooltip(ext_scale_widget,
                    'External restraints weight. Increasing this weight increases the'
                    ' influence of external restraints during refinement.')
        external_weight_scale_box = QtGui.QHBoxLayout()
        external_weight_scale_box.setContentsMargins(0, 0, 0, 0)
        self.external_weight_scale_label = QtGui.QLabel('Weight')
        self.external_weight_scale_label.setFixedWidth(label_width)
        external_weight_scale_box.addWidget(self.external_weight_scale_label)
        self.external_weight_scale_value = QtGui.QDoubleSpinBox()
        self.external_weight_scale_value.setDecimals(2)
        self.external_weight_scale_value.setFixedWidth(button_width)
        self.external_weight_scale_value.setValue(10.0)
        self.external_weight_scale_value.setSingleStep(0.1)
        self.external_weight_scale_value.valueChanged.connect(self.set_external_weight_scale)
        external_weight_scale_box.addWidget(self.external_weight_scale_value)
        external_weight_scale_box.setAlignment(self.external_weight_scale_value, QtCore.Qt.AlignLeft)
        ext_scale_widget.setLayout(external_weight_scale_box)
        self.layout.addWidget(ext_scale_widget)

        # external_weight_gmwt
        ext_gmwt_widget = QtGui.QWidget()
        set_tooltip(ext_gmwt_widget,
                    'Geman-McClure parameter, which controls robustness to outliers.'
                    ' Increasing this value reduces the influence of outliers (i.e.'
                    ' restraints that are very different from the current inter-atomic'
                    ' distance). Note this parameter will be ignored by Refmac from'
                    ' CCP4 8.0 onwards.')
        external_weight_gmwt_box = QtGui.QHBoxLayout()
        external_weight_gmwt_box.setContentsMargins(0, 0, 0, 0)
        self.external_weight_gmwt_label = QtGui.QLabel('GMWT')
        self.external_weight_gmwt_label.setFixedWidth(label_width)
        external_weight_gmwt_box.addWidget(self.external_weight_gmwt_label)
        self.external_weight_gmwt_value = QtGui.QDoubleSpinBox()
        self.external_weight_gmwt_value.setDecimals(2)
        self.external_weight_gmwt_value.setFixedWidth(button_width)
        self.external_weight_gmwt_value.setValue(0.02)
        self.external_weight_gmwt_value.setSingleStep(0.01)
        self.external_weight_gmwt_value.valueChanged.connect(self.set_external_weight_gmwt)
        external_weight_gmwt_box.addWidget(self.external_weight_gmwt_value)
        external_weight_gmwt_box.setAlignment(self.external_weight_gmwt_value, QtCore.Qt.AlignLeft)
        # Remove
        remove_button = QtGui.QPushButton('Remove')
        remove_button.setFixedWidth(button_width)
        remove_button.setToolTip('Remove this restraints file')
        remove_button.clicked.connect(self.remove)
        external_weight_gmwt_box.addWidget(remove_button)
        external_weight_gmwt_box.setAlignment(external_weight_gmwt_box, QtCore.Qt.AlignRight)
        ext_gmwt_widget.setLayout(external_weight_gmwt_box)
        self.layout.addWidget(ext_gmwt_widget)

        # Set initial values
        # restraints_file
        self.set_restraints_file(path=restraints_file)
        # external_weight_scale
        self.set_external_weight_scale(external_weight_scale)
        # external_weight_gmwt
        self.set_external_weight_gmwt(external_weight_gmwt)

    def get_restraints_file(self):
        path = self.get_file_from_browser(file_types='')
        self.set_restraints_file(path=path)

    def edit_finished_restraints_file(self):
        path = self.restraints_file_value.text()
        self.set_restraints_file(path=path)

    def set_restraints_file(self, path):
        self.set_shading(self.restraints_file_label, shade=True)
        self.set_shading(self.restraints_file_value, shade=True)
        self.dataset.restraints_file_path = None
        if path is not None:
            if os.path.exists(path):
                self.restraints_file_value.setText(path)
                self.set_shading(self.restraints_file_label, shade=False)
                self.set_shading(self.restraints_file_value, shade=False)
                self.dataset.set_restraints_file(path)

    @QtCore.pyqtSlot(int)
    def set_external_weight_scale(self, value):
        self.set_shading(self.external_weight_scale_value, shade=False)
        self.set_shading(self.external_weight_scale_label, shade=False)
        self.dataset.set_external_weight_scale(value)

    def set_external_weight_gmwt(self, value):
        self.set_shading(self.external_weight_gmwt_value, shade=False)
        self.set_shading(self.external_weight_gmwt_label, shade=False)
        self.dataset.set_external_weight_gmwt(value)


    def get_file_from_browser(self, file_types):
        dialog_path = window_utils.get_last_directory_browsed()
        path = QtGui.QFileDialog.getOpenFileName(self,
            'Open a File',
            QtCore.QDir.path(QtCore.QDir(dialog_path)),
            file_types)
        path = str(path)
        if path == '':
            path = None
        if path is not None:
            dialog_path = os.path.dirname(path)
            window_utils.set_last_directory_browsed(path=dialog_path)
        return path

    def set_shading(self, widget, shade=True):
        '''
        Shade text QLineEdit red to display warning.
        '''
        if shade:
            widget.setStyleSheet('color: red')
        else:
            widget.setStyleSheet('color: None')

    def remove(self):
        self.deleteLater()


class extRestraintsInput(QtGui.QWidget):
    def __init__(self,
                 restraints_files_arg=None,
                 external_weight_scales_arg=None,
                 external_weight_gmwts_arg=None,
                 parent=None):
        super(extRestraintsInput, self).__init__()
        self.parent = parent
        width = 100

        # Layout
        self.layout = QtGui.QVBoxLayout()
        self.setLayout(self.layout)
        self.setToolTip('External restraints')

        # Add dataset
        add_button = QtGui.QPushButton('Add')
        add_button.setFixedWidth(width)
        add_button.setToolTip('Add another restraints file')
        add_button.clicked.connect(self.handle_add_button)
        self.layout.addWidget(add_button)

        # Set args
        restraints_files = restraints_files_arg()
        external_weight_scales = external_weight_scales_arg()
        external_weight_gmwts = external_weight_gmwts_arg()
        # Add black dataset
        if isinstance(restraints_files, list):
            for n in xrange(len(restraints_files)):
                self.add_dataset(
                    restraints_files=restraints_files[n],
                    external_weight_scales=external_weight_scales[n],
                    external_weight_gmwts=external_weight_gmwts[n])
        else:
            self.add_dataset(
                restraints_files=restraints_files,
                external_weight_scales=external_weight_scales,
                external_weight_gmwts=external_weight_gmwts)

    def handle_add_button(self):
        '''
        Handle dataset button; stop bool signal transfer
        '''
        self.add_dataset()

    def add_dataset(self,
                    restraints_files=None,
                    external_weight_scales=None,
                    external_weight_gmwts=None):
        dataset = extRestraintInput(restraints_file=restraints_files,
                                     external_weight_scale=external_weight_scales,
                                     external_weight_gmwt=external_weight_gmwts)
        self.layout.addWidget(dataset)

    def validate_datasets(self):
        children = self.findChildren(extRestraintInput)
        # Check at least one dataset
        if len(children) == 0:
            # Warn no datasets provided
            text = 'No input external restraints provided'
            QtGui.QMessageBox.warning(self,
                'Error',
                text)
            return False
        warning = False
        # Check datasets are complete (must have pdb, map, resolution)
        for child in children:
            if not child.dataset.validate():
                warning = True
        if warning:
            # Warn datasets incomplete information
            text = ('Restraint(s) incomplete (each entry requires external restraints'
                    ' file, weight and GMWT)')
            QtGui.QMessageBox.warning(self,
                'Error',
                text)
            return False
        else:
            return True

    def set_args(self, restraints_files_arg, external_weight_scales_arg, external_weight_gmwts_arg):
        children = self.findChildren(extRestraintInput)
        restraints_files = []
        external_weight_scales = []
        external_weight_gmwts = []
        for child in children:
            restraints_files.append(child.dataset.restraints_file)
            external_weight_scales.append(child.dataset.external_weight_scale)
            external_weight_gmwts.append(child.dataset.external_weight_gmwt)
        restraints_files_arg.value = restraints_files
        external_weight_scales_arg.value = external_weight_scales
        external_weight_gmwts_arg.value = external_weight_gmwts


class Refmac5Window(window_utils.CCPEMTaskWindow):
    '''
    Refmac window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args_masked_hb_validation.json')

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Input pdb
        self.pdb_input = window_utils.FileArgInput(
            parent=self,
            arg_name='start_pdb',
            args=self.args,
            label='Input model',
            file_types=ccpem_file_types.pdb_ext,
            required=True)
        self.args_widget.args_layout.addWidget(
            self.pdb_input)

        # -> Input ligand cif library
        self.lib_input = window_utils.FileArgInput(
            parent=self,
            label='Restraints dictionary',
            arg_name='lib_in',
            args=self.args,
            file_types=ccpem_file_types.lib_ext,
            required=False)
        self.args_widget.args_layout.addWidget(
            self.lib_input)

        # Resolution
        self.resolution_input = window_utils.NumberArgInput(
                parent=self,
                arg_name='resolution',
                args=self.args,
                required=True)
        self.args_widget.args_layout.addWidget(
            self.resolution_input)

        # Half map vs full map refinement
        self.half_map_refinement_input = window_utils.ChoiceArgInput(
                parent=self,
                arg_name='half_map_refinement',
                args=self.args,
                required=True)
        self.args_widget.args_layout.addWidget(
            self.half_map_refinement_input)
        self.half_map_refinement_input.value_line.currentIndexChanged.connect(
            self.set_maps_visible)

        # Input map
        self.map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_map',
            args=self.args,
            label='Input map',
            file_types=ccpem_file_types.mrc_ext,
            required=False)
        self.args_widget.args_layout.addWidget(
            self.map_input)

        self.half_map_1_input = window_utils.FileArgInput(
            parent=self,
            arg_name='half_map_1',
            args=self.args,
            required=True)
        self.args_widget.args_layout.addWidget(
            self.half_map_1_input)

        self.half_map_2_input = window_utils.FileArgInput(
            parent=self,
            arg_name='half_map_2',
            args=self.args,
            required=True)
        self.args_widget.args_layout.addWidget(
            self.half_map_2_input)

        # -> FoFc Mask
        mask_for_fofc_input = window_utils.FileArgInput(
            parent=self,
            arg_name='mask_for_fofc',
            args=self.args)
        self.args_widget.args_layout.addWidget(
            mask_for_fofc_input)

        # -> Validation on
        self.validation_button = window_utils.CheckArgInput(
            parent=self,
            arg_name='validate_on',
            label='Cross validation',
            dependants=[self.half_map_1_input, self.half_map_2_input],
            args=self.args)
        self.args_widget.args_layout.addWidget(
            self.validation_button)

        self.masked_refinement_input = window_utils.CheckArgInput(
            parent=self,
            arg_name='masked_refinement_on',
            args=self.args)
        self.args_widget.args_layout.addWidget(
            self.masked_refinement_input)

        # -> Weight auto
        self.weight_auto_input = window_utils.CheckArgInput(
            parent=self,
            arg_name='weight_auto',
            args=self.args)
        self.args_widget.args_layout.addWidget(
            self.weight_auto_input)

        # -> Weight
        self.weight_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='weight',
            label='Auto weight scale',
            args=self.args,
            decimals = 5)
        self.args_widget.args_layout.addWidget(
            self.weight_input)

        # -> Ncycle
        ncycle_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='ncycle',
            args=self.args)
        self.args_widget.args_layout.addWidget(ncycle_input)

        # Refinement options frame
        self.refinement_options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Extended options',
            button_tooltip='Further refinement parameters')
        self.args_widget.args_layout.addLayout(
            self.refinement_options_frame)

        # -> LIBG restraints for nucleic acids
        self.libg_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='libg',
            label='Add RNA/DNA restraints',
            args=self.args)
        self.refinement_options_frame.add_extension_widget(
            self.libg_input)
        self.libg_input.value_line.currentIndexChanged.connect(
            self.set_libg_selection)

        # -> Keyword entry for libg nucleotide range selection
        self.libg_selection_input = window_utils.KeywordArgInput(
            parent=self,
            arg_name='libg_selection',
            args=self.args,
            label='Nucleotide range')
        self.refinement_options_frame.add_extension_widget(
            self.libg_selection_input)
        self.set_libg_selection()

        # -> Map sharpen / blur
        map_sharpen_input = window_utils.NumberArgInput(
            parent=self,
            decimals=1,
            minimum=-999,
            arg_name='map_sharpen',
            args=self.args)
        self.refinement_options_frame.add_extension_widget(
            map_sharpen_input)

        # -> Jelly body
        self.jelly_body_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='jelly_body',
            args=self.args)
        self.refinement_options_frame.add_extension_widget(
            self.jelly_body_input)

        # -> Hydrogens
        hydrogens_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='hydrogens',
            args=self.args)
        self.refinement_options_frame.add_extension_widget(
            hydrogens_input)
        # Reset input PDB b-factor
        bfactor_frame = window_utils.CCPEMExtensionFrame(
            button_name='Edit input model',
            button_tooltip='Set b-factor options')
        self.refinement_options_frame.add_extension_frame(
            bfactor_frame)

        # ->-> Set bfactor on
        bfactor_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='set_bfactor',
            args=self.args)

        bfactor_on = window_utils.CheckArgInput(
            parent=self,
            arg_name='set_bfactor_on',
            dependants=[bfactor_input],
            args=self.args)
        bfactor_frame.add_extension_widget(bfactor_on)

        # ->-> Set bfactor value
        bfactor_frame.add_extension_widget(bfactor_input)

        # -> Masked refinement options
        masked_refinement_frame = window_utils.CCPEMExtensionFrame(
            button_name='Masked refinement options',
            button_tooltip='Refine around radius of supplied molecule only')
        self.refinement_options_frame.add_extension_frame(
            masked_refinement_frame)

        # ->-> Masked refinement on
        mask_radius_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='mask_radius',
            args=self.args,
            required=False,
            label='Mask radius')

        # ->-> Mask Radius
        masked_refinement_frame.add_extension_widget(mask_radius_input)

        # -> Symmetry options frame
        self.symmetry_frame = window_utils.CCPEMExtensionFrame(
            button_name='Symmetry options',
            button_tooltip='Set symmetry parameters for refinement')
        self.args_widget.args_layout.addLayout(
            self.symmetry_frame)
        
        # -> Auto symmetry
        self.symmetry_auto_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='symmetry_auto',
            second_width=None,
            args=self.args)
        self.symmetry_frame.add_extension_widget(
            self.symmetry_auto_input)

        # -> Strict symmetry options
        strict_symmetry_group = QtGui.QGroupBox(parent=self,
                                                title='Strict symmetry options')
        self.symmetry_frame.add_extension_widget(strict_symmetry_group)
        strict_symmetry_group_layout = QtGui.QVBoxLayout()

         # -> Strict symmetry PG symbol
        point_group_input = window_utils.StrArgInput(
            parent=self,
            arg_name='ncsc_relion_symbol',
            args=self.args,
            label='Point group',
            tooltip_text=(
                'Point group symbol (using RELION conventions). For SPA, use point'
                ' group Cn, Dn, T, O or I and ensure helical twist and rise are set to'
                ' zero. For helical reconstruction, use axial symmetry (Cn or Dn) and'
                ' your helical twist and rise values below.'))
        strict_symmetry_group_layout.addWidget(point_group_input)

        # -> Helical twist
        twist_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='twist',
            args=self.args,
            required=False,
            label='Helical twist')
        strict_symmetry_group_layout.addWidget(twist_input)
        # -> Helical rise
        rise_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='rise',
            args=self.args,
            required=False,
            label='Helical rise')
        strict_symmetry_group_layout.addWidget(rise_input)

        strict_symmetry_group.setLayout(strict_symmetry_group_layout)

        # External restraints frame
        self.external_restraints_frame = window_utils.CCPEMExtensionFrame(
            button_name='External restraints',
            button_tooltip='Set external restraints')
        self.args_widget.args_layout.addLayout(
            self.external_restraints_frame)

        # -> Use external restraints
#        input_prosmart = window_utils.FileArgInput(
#            parent=self,
#            arg_name='input_restraints_path',
#            args=self.args)

        # Files with restraints
        self.restraints =  extRestraintsInput(
            restraints_files_arg =self.args.restraints_files,
            external_weight_scales_arg = self.args.external_weight_scales,
            external_weight_gmwts_arg = self.args.external_weight_gmwts,
            parent=self)
        self.args_widget.args_layout.addWidget(self.restraints)

        self.restraints_button = window_utils.CheckArgInput(
            parent=self,
            arg_name='external_restraints_on',
            dependants=[self.restraints],
            args=self.args)
        self.external_restraints_frame.add_extension_widget(
            self.restraints_button)

        # -> ProSMART restraints
        self.external_restraints_frame.add_extension_widget(
            self.restraints)

        # -> External restraints weights
        for arg in ['external_weight_dmx']:
            restraints_menu = window_utils.NumberArgInput(
                parent=self,
                arg_name=arg,
                args=self.args)
            self.external_restraints_frame.add_extension_widget(
                restraints_menu)
        # -> Main chain only option
        input_main_chain = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='external_main_only',
            args=self.args)
        self.external_restraints_frame.add_extension_widget(
            input_main_chain)

        # Keyword frame
        keyword_frame = window_utils.CCPEMExtensionFrame(
            button_name='Keywords',
            button_tooltip='Optional user-supplied keywords')
        self.args_widget.args_layout.addLayout(keyword_frame)

        # -> Keyword entry
        self.keyword_entry_new = window_utils.KeywordArgInput(
            parent=self,
            arg_name='keywords',
            args=self.args,
            label='Keywords')
        keyword_frame.add_extension_widget(self.keyword_entry_new)

        # Set dependencies
        self.set_maps_visible()
        self.weight_auto_input.value_line.stateChanged.connect(
            self.set_weight_auto)
        self.weight_auto_input.value_line.stateChanged.connect(
            self.set_validate_on)
        self.set_weight_auto()
        self.validation_button.value_line.stateChanged.connect(
            self.set_validate_on)
        self.restraints_button.value_line.stateChanged.connect(
            self.show_prosmart_jelly_body_warning)

    def set_maps_visible(self):
        if self.args.half_map_refinement():
            self.map_input.hide()
            self.half_map_1_input.show()
            self.half_map_2_input.show()
            self.validation_button.show()
        else:
            self.map_input.show()
            self.half_map_1_input.hide()
            self.half_map_2_input.hide()
            self.validation_button.hide()

    def set_weight_auto(self):
        if self.args.weight_auto.value:
            self.weight_input.hide()
        else:
            self.weight_input.show()

    def set_validate_on(self):
        pass

    def show_prosmart_jelly_body_warning(self):
        if self.jelly_body_input.value_line.currentText() == 'True' and \
                self.restraints_button.value_line.isChecked():
            message = \
                ('Jelly body restraints are not compatible with'
                 ' ProSMART external restraints. We recommend you'
                 ' turn off the jelly body restraints if using'
                 ' ProSMART restraints.')
            QtGui.QMessageBox.information(self,
                                          'CCP-EM | Refmac5',
                                          message)

    def set_libg_selection(self):
        if self.args.libg.value:
            self.libg_selection_input.show()
        else:
            self.libg_selection_input.hide()

    def set_dc_mode(self):
        if self.args.dc_mode():
            if hasattr(self, 'pdb_input'):
                self.pdb_input.deleteLater()
            if hasattr(self, 'map_input'):
                self.map_input.deleteLater()
            if hasattr(self, 'resolution_input'):
                self.resolution_input.deleteLater()

            # Set map, pdb and resolution datasets
            # Input datasets
            self.datasets_input = RefmacDatasetsInput(
                pdbs_arg=self.args.start_pdb,
                maps_arg=self.args.input_map,
                resolutions_arg=self.args.resolution,
                parent=self)
            self.args_widget.args_layout.insertWidget(3, self.datasets_input)

###
            self.use_cluster = window_utils.ChoiceArgInput(
                parent=self,
                arg_name='use_cluster',
                args=self.args)
            self.args_widget.args_layout.insertWidget(4, self.use_cluster)

            self.numberCPUsOrNodes = window_utils.NumberArgInput(
                parent=self,
                arg_name='numberCPUsOrNodes',
                minimum=1,
                args=self.args)
            self.args_widget.args_layout.insertWidget(5, self.numberCPUsOrNodes)

            # Resolution cutoff for Refmac DC
#            self.cutoff_input = window_utils.NumberArgInput(
#                parent=self,
#                arg_name='cutoff_res',
#                args=self.args,
#                required=True)
#            self.args_widget.args_layout.insertWidget(4, self.cutoff_input)

            self.external_restraints_frame.hide()
            self.validation_frame.hide()

        else:
            if hasattr(self, 'datasets_input'):
                self.datasets_input.deleteLater()
            if hasattr(self, 'use_cluster'):
                self.use_cluster.deleteLater()
            if hasattr(self, 'numberCPUsOrNodes'):
                self.numberCPUsOrNodes.deleteLater()

            # Input pdb
            self.pdb_input = window_utils.FileArgInput(
                parent=self,
                arg_name='start_pdb',
                args=self.args,
                label='Input model',
                file_types=ccpem_file_types.pdb_ext,
                required=True)
            self.args_widget.args_layout.insertWidget(2, self.pdb_input)

            # -> Input ligand cif library
            self.lib_input = window_utils.FileArgInput(
                parent=self,
                label='Input ligand',
                arg_name='lib_in',
                args=self.args,
                file_types=ccpem_file_types.lib_ext,
                required=False)
            self.args_widget.args_layout.insertWidget(3, self.lib_input)

            # Input map
            self.map_input = window_utils.FileArgInput(
                parent=self,
                arg_name='input_map',
                args=self.args,
                label='Input map',
                file_types=ccpem_file_types.mrc_ext,
                required=True)
            self.args_widget.args_layout.insertWidget(4, self.map_input)

            # Resolution
            self.resolution_input = window_utils.NumberArgInput(
                    parent=self,
                    arg_name='resolution',
                    args=self.args,
                    required=True)
            self.args_widget.args_layout.insertWidget(5, self.resolution_input)
            self.external_restraints_frame.button.show()
            self.validation_frame.button.show()

    def set_rv_ui(self):
        '''
        RVAPI results viewer.
        '''
        # Create results if not already done
        rv_index = os.path.join(
            self.task.job_location,
            'rvapi_data/index.html')
        if os.path.exists(rv_index):
            self.rv_view = QtWebKit.QWebView()
            assert os.path.exists(rv_index)
            self.rv_view.load(QtCore.QUrl(rv_index))
            self.results_dock = QtGui.QDockWidget('Results',
                                                   self,
                                                   QtCore.Qt.Widget)
            self.results_dock.setToolTip('Results overview')
            self.results_dock.setWidget(self.rv_view)
            self.tabifyDockWidget(self.setup_dock, self.results_dock)
            self.results_dock.show()
            self.results_dock.raise_()

    def validate_input(self):
        '''
        Override from baseclass to allow validation of multiple datasets
        '''
        ready = super(Refmac5Window, self).validate_input()
        if self.args.half_map_refinement():
            if not (self.args.half_map_1.value and self.args.half_map_2.value):
                QtGui.QMessageBox.warning(self, 'Error', 'Half maps not specified')
                ready = False
        else:
            if not self.args.input_map.value:
                QtGui.QMessageBox.warning(self, 'Error', 'Input map not specified')
                ready = False

        if ready and self.args.dc_mode():
            # Validate inputs for multiple datasets
            if not self.datasets_input.validate_datasets():
                ready = False
            else:
                self.datasets_input.set_args(
                    pdbs_arg=self.args.start_pdb,
                    maps_arg=self.args.input_map,
                    resolutions_arg=self.args.resolution)
        if ready and self.args.external_restraints_on.value:
            if not self.restraints.validate_datasets():
                ready = False
            else:
                self.restraints.set_args(
                  restraints_files_arg=self.args.restraints_files,
                  external_weight_scales_arg=self.args.external_weight_scales,
                  external_weight_gmwts_arg=self.args.external_weight_gmwts)

        if ready:
            if (self.args.ncsc_relion_symbol.value and self.args.ncsc_relion_symbol.value.strip()) or (self.args.twist() or self.args.rise() > 0.0):
                try:
                    symmetry.operators_from_symbol(self.args.ncsc_relion_symbol.value)
                    if self.args.twist() or self.args.rise() > 0.0:
                        assert self.args.ncsc_relion_symbol.value[0] in ['C', 'c', 'D', 'd']
                        assert int(self.args.ncsc_relion_symbol.value[1:])
                except:
                    QtGui.QMessageBox.warning(
                        self,
                        'Error',
                        'Invalid point group symbol ({})'.format(self.args.ncsc_relion_symbol.value))
                    ready = False

        return ready

    def set_on_job_running_custom(self):
        if not self.args.dc_mode():
            # Set inputs for launcher
            # If half maps are given as input, Servalcat calculates its best estimate
            # of the observed map density (Fo) and saves it in the output diffmap.mtz
            # file. By opening diffmap.mtz we get both Fo and Fo-Fc maps and so we leave
            # the input half maps deselected by default.
            # If a full map is given, diffmap.mtz contains only a difference map and
            # not an observed map, so in that case we select the full map input in the
            # launcher by default to ensure an Fo map is visualised.
            if self.args.half_map_refinement.value:
                self.launcher.add_file(
                    arg_name='half_map_1',
                    file_type='map',
                    description='Input half map 1',
                    selected=False)
                self.launcher.add_file(
                    arg_name='half_map_2',
                    file_type='map',
                    description='Input half map 2',
                    selected=False)
            else:
                self.launcher.add_file(
                    arg_name='input_map',
                    file_type='map',
                    description='Input full map',
                    selected=True)
            if self.args.mask_for_fofc.value:
                self.launcher.add_file(
                    arg_name='mask_for_fofc',
                    file_type='map',
                    description='Mask used for difference map calculation',
                    selected=False)
            self.launcher.add_file(
                arg_name='start_pdb',
                file_type='pdb',
                description=self.args.start_pdb.help,
                selected=True)
        self.launcher.set_mg_default('Coot')
        self.launcher_dock.show()

    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.
        '''
        # Add various output files to launcher
        if hasattr(self.task, 'process_refine'):
            # Maps
            refined_maps = os.path.join(
                self.task.job_location,
                'diffmap.mtz')
            if self.args.half_map_refinement.value:
                desc = 'Sharpened and weighted observed and difference maps (Fo and Fo-Fc)'
            else:
                desc = 'Sharpened and weighted difference map (Fo-Fc)'
            self.launcher.add_file(
                path=refined_maps,
                file_type='mtz',
                description=desc,
                selected=True)
            fo_mrc = os.path.join(
                self.task.job_location,
                'diffmap_normalized_fo.mrc')
            if os.path.isfile(fo_mrc):
                self.launcher.add_file(
                    path=fo_mrc,
                    file_type='map',
                    description='Sharpened and weighted Fo map (normalized within mask)',
                    selected=False)
            fofc_mrc = os.path.join(
                self.task.job_location,
                'diffmap_normalized_fofc.mrc')
            if os.path.isfile(fofc_mrc):
                self.launcher.add_file(
                    path=fofc_mrc,
                    file_type='map',
                    description='Sharpened and weighted Fo-Fc map (normalized within mask)',
                    selected=False)

            # Models
            if os.path.isfile(self.task.process_refine.cifout_path):
                self.launcher.add_file(
                    path=self.task.process_refine.cifout_path,
                    file_type='pdb',
                    description='Refined mmCIF file',
                    selected=False)
            if os.path.isfile(self.task.process_refine.pdbout_path):
                self.launcher.add_file(
                    path=self.task.process_refine.pdbout_path,
                    file_type='pdb',
                    description='Refined PDB file',
                    selected=True)
            expanded_cif = os.path.join(
                self.task.job_location,
                'refined_expanded.mmcif')
            if os.path.isfile(expanded_cif):
                self.launcher.add_file(
                    path=expanded_cif,
                    file_type='pdb',
                    description='Symmetry-expanded refined PDB file',
                    selected=False)
            expanded_pdb = os.path.join(
                self.task.job_location,
                'refined_expanded.pdb')
            if os.path.isfile(expanded_pdb):
                self.launcher.add_file(
                    path=expanded_pdb,
                    file_type='pdb',
                    description='Symmetry-expanded refined PDB file',
                    selected=False)
            
        self.launcher.set_tree_view()
        self.launcher_dock.raise_()
        self.launcher_dock.show()
        # Set results viewer
        self.set_rv_ui()


def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=refmac_task.Refmac,
        window_class=Refmac5Window)

if __name__ == '__main__':
    main()
