#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os, multiprocessing
from PyQt4 import QtCore, QtGui
from ccpem_gui.utils import window_utils
from ccpem_core.tasks.haruspex import haruspex_task
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_gui.utils import command_line_launch
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import haruspex as test_data
from numpy import maximum
from ccpem_gui.utils import gui_process

class HaruspexWindow(window_utils.CCPEMTaskWindow):
    '''
    Haruspex window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(HaruspexWindow, self).__init__(task=task,
                                           parent=parent)

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)

        # Input map
        input_target_map = window_utils.FileArgInput(
            parent=self,
            arg_name='target_map',
            required=True,
            file_types=ccpem_file_types.mrc_ext,
            args=self.args)
        self.args_widget.args_layout.addWidget(input_target_map)

        # Input display model
        input_display_model = window_utils.FileArgInput(
            parent=self,
            arg_name='display_model',
            required=False,
            file_types=ccpem_file_types.pdb_ext,
            args=self.args)
        self.args_widget.args_layout.addWidget(input_display_model)

        # Input percentage GPU memory to use
        pmem_input = window_utils.NumberArgInput(
            parent=self,
            decimals=1,
            minimum=10,
            maximum=100,
            step=10,
            arg_name='p_mem',
            args=self.args,
            required=False)
        self.args_widget.args_layout.addWidget(pmem_input)

        # Container widget for controls for number of CPU cores
        # This needs to be a widget (and not just a layout) so it can be shown and hidden easily
        self.mpi_widget = QtGui.QWidget()
        mpi_layout = QtGui.QHBoxLayout()
        self.mpi_widget.setLayout(mpi_layout)
        self.args_widget.args_layout.addWidget(self.mpi_widget)

        self.n_mpi_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='n_cpu',
            minimum=1,
            maximum=multiprocessing.cpu_count(),
            required=False,
            args=self.args)
        mpi_layout.addWidget(self.n_mpi_input)
 
        set_mpi_all_cpus_button = QtGui.QPushButton('Use all CPUs')
        set_mpi_all_cpus_button.clicked.connect(self.set_n_mpi_all_cores)
        set_mpi_all_cpus_button.setToolTip("Set the number of CPU cores nodes to use all of the CPU cores on this computer")
        mpi_layout.addWidget(set_mpi_all_cpus_button)
 
        set_mpi_half_cpus_button = QtGui.QPushButton('Use half CPUs')
        set_mpi_half_cpus_button.clicked.connect(self.set_n_mpi_half_cores)
        set_mpi_half_cpus_button.setToolTip("Set the number of CPU cores to use half of the CPU cores on this computer")
        mpi_layout.addWidget(set_mpi_half_cpus_button)
        self.set_n_mpi_half_cores()
 
        # Add horizontal stretch so buttons don't expand sideways
        mpi_layout.addStretch()

        # Set inputs for launcher
        self.launcher.add_file(
            arg_name='display_model',
            file_type='pdb',
            description=self.args.display_model.help,
            selected=True)

        self.launcher.add_file(
            arg_name='target_map',
            file_type='map',
            description=self.args.target_map.help,
            selected=True,
            checkable=False)

    def set_n_mpi_all_cores(self):
        self.n_mpi_input.set_arg_value(multiprocessing.cpu_count())

    def set_n_mpi_half_cores(self):
        self.n_mpi_input.set_arg_value(multiprocessing.cpu_count() / 2)

    def run_coot_custom(self):
        # Get coot script location
        from ccpem_core.tasks import haruspex
        script = os.path.join(
            os.path.dirname(haruspex.__file__),
            'haruspex_coot.py')

        # Get args
        # Must use this order for loading maps
        args = ['--map', self.helix_map,               # Helix
                '--map', self.sheet_map,               # Sheet
                '--map', self.npair_map,               # Npair
                '--map', self.args.target_map(),       # Target map
                '--script', script]
        gui_process.run_coot(args=args)

    def set_on_job_running_custom(self):
        prefix = os.path.join(
            self.task.job_location,
            os.path.splitext(os.path.basename(self.args.target_map()))[0]
            )
        self.helix_map = prefix + '_helix.mrc'
        self.sheet_map = prefix + '_sheet.mrc'
        self.npair_map = prefix + '_npair.mrc'

    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.  Show output pdb.
        '''
        # Set launcher
        # Add expected output map names based on input target
#         prefix = os.path.join(
#             self.task.job_location,
#             os.path.splitext(os.path.basename(self.args.target_map()))[0]
#             )
#         sheet_map = prefix + '_sheet.mrc'
#         helix_map = prefix + '_helix.mrc'
#         npair_map = prefix + '_npair.mrc'
        self.launcher.add_file(
            arg_name=None,
            path=self.helix_map,
            file_type='map',
            description='Alpha helix segmented map',
            selected=True,
            checkable=False)
        self.launcher.add_file(
            arg_name=None,
            path=self.sheet_map,
            file_type='map',
            description='Beta sheet segmented map',
            checkable=False)
        self.launcher.add_file(
            arg_name=None,
            path=self.npair_map,
            file_type='map',
            description='Nucleic acid segmented map',
            checkable=False)
        self.launcher.set_tree_view()

def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=haruspex_task.Haruspex,
        window_class=HaruspexWindow)

if __name__ == '__main__':
    main()

