#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Task window for symmetry expansion via Refmac
'''
import os
import warnings

from PyQt4 import QtGui

from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_core.tasks.sym_expand import sym_expand_task
from ccpem_gui.utils import command_line_launch
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import sym_expand as test_data


class SymExpandWindow(window_utils.CCPEMTaskWindow):
    '''
    SymExpand window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(SymExpandWindow, self).__init__(
            task=task,
            parent=parent)


    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Input pdb
        xyzin = window_utils.FileArgInput(
            parent=self,
            arg_name='xyzin',
            args=self.args,
            label='Input PDB file',
            file_types=ccpem_file_types.pdb_ext,
            required=True)
        self.args_widget.args_layout.addWidget(xyzin)

        # Input ligand library
        libin = window_utils.FileArgInput(
            parent=self,
            arg_name='libin',
            args=self.args,
            label='Ligand library (cif)',
            file_types=ccpem_file_types.lib_ext,
            required=False)
        self.args_widget.args_layout.addWidget(libin)


        # Set inputs for launcher
        self.launcher.add_file(
            arg_name='xyzin',
            file_type='pdb',
            description=self.args.xyzin.help,
            selected=True)

    def set_on_job_finish_custom(self):
        self.launcher.add_file(
            arg_name=None,
            path=self.task.process_symExpand.xyzout,
            file_type='pdb',
            description='Output PDB file',
            selected=True)
        self.launcher.set_tree_view()





def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=sym_expand_task.SymExpand,
        window_class=SymExpandWindow)

if __name__ == '__main__':
    main()
