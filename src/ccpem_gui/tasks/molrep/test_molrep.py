#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import sys
import shutil
import time
import tempfile
from PyQt4 import QtGui, QtCore
from PyQt4.QtTest import QTest
from ccpem_core.tasks.molrep import molrep_task
from ccpem_gui.tasks.molrep import molrep_window
from ccpem_core.test_data.tasks import molrep as molrep_test
from ccpem_core import ccpem_utils
from ccpem_core import process_manager

app = QtGui.QApplication(sys.argv)


class Test(unittest.TestCase):
    '''
    Unit test for Molrep (invokes GUI).
    '''
    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_data = os.path.dirname(molrep_test.__file__)
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        '''
        Remove temporary data.
        '''
        if os.path.exists(path=self.test_output):
            shutil.rmtree(self.test_output)

    def test_molrep_haemoglobin_with_target_sequence(self):
        '''
        Test Molrep with 5me2 haemoglobin map, 1mbn myoglobin model.
         
        This takes about 3 minutes to run, probably a bit long for a test...
        '''
        ccpem_utils.print_header(message='Unit test - Molrep with target sequence')
        # Load args
        args_path = os.path.join(self.test_data, 'target_seq_test_args.json')
        run_task = molrep_task.MolRep(job_location=self.test_output,
                                      args_json=args_path)
        self.run_molrep_task_in_window(run_task)
         
        # Check correlation factor can be found and is correct
        molrep_process = run_task.get_process('Molrep')
        found_sol_1 = False
        with open(molrep_process.stdout) as f:
            for line in f:
                if line.find('Nmon RF  TF   theta    phi     chi') != -1:
                    score = float(f.next().split()[-1])
                    if not found_sol_1:
                        print 'Score (first solution): {}'.format(score)
                        self.assertGreater(score, 0.39)
                        found_sol_1 = True
                        break
        assert found_sol_1
 
    def test_molrep_haemoglobin_with_partial_fixed_model(self):
        '''
        Test Molrep with 5me2 map and fixed AB chain model, 1mbn search model.
         
        This takes about 3XX minutes to run, probably a bit long for a test...
        '''
        ccpem_utils.print_header(message='Unit test - Molrep with fixed model')
        # Load args
        args_path = os.path.join(self.test_data, 'fixed_model_test_args.json')
        run_task = molrep_task.MolRep(job_location=self.test_output,
                                      args_json=args_path)
        self.run_molrep_task_in_window(run_task)
 
        # Check correlation factors can be found and are correct
        molrep_process = run_task.get_process('Molrep')
        found_sol_1 = False
        found_sol_2 = False
        with open(molrep_process.stdout) as f:
            for line in f:
                if line.find('Nmon RF  TF   theta    phi     chi') != -1:
                    score = float(f.next().split()[-1])
                    if not found_sol_1:
                        print 'Score (first solution): {}'.format(score)
                        self.assertGreater(score, 0.38)
                        found_sol_1 = True
                    else:
                        print 'Score (second solution): {}'.format(score)
                        self.assertGreater(score, 0.35)
                        found_sol_2 = True
                        break
        assert found_sol_1
        assert found_sol_2

    def run_molrep_task_in_window(self, run_task, timeout_limit=500):
        '''
        Run a Molrep task via the GUI.
        '''
        # Setup GUI
        self.window = molrep_window.MolrepWindow(task=run_task)
        # Mouse click run
        QTest.mouseClick(
            self.window.tool_bar.widgetForAction(self.window.tb_run_button),
            QtCore.Qt.LeftButton)
        # Wait for run to complete
        self.job_completed = False
        timeout = 0
        stdout = run_task.pipeline.pipeline[-1][0].stdout
        # stdout = sfcheckmapvsmodel_stdout.txt
        while not self.job_completed and timeout < timeout_limit:
            print 'Molrep running for {0} secs (timeout = {1})'.format(timeout, timeout_limit)
            time.sleep(5.0)
            timeout += 5
            status =\
                process_manager.get_process_status(run_task.pipeline.json)
            if status == 'finished':
                if os.path.isfile(stdout):
                    tail = ccpem_utils.tail(stdout, maxlen=10)
                    if tail.find('CCP-EM process finished') != -1:
                        self.job_completed = True
            elif status == 'failed':
                print 'Molrep job failed'
                break
        # Check timeout
        assert timeout < timeout_limit
        # Check job completed
        assert self.job_completed
        # Check output pdb created
        output_pdb = os.path.join(self.window.task.job_location, 'molrep.pdb')
        assert os.path.exists(output_pdb)
        # Check output PDB does not contain problematic '#' comment lines
        with open(output_pdb) as pdb:
            for line in pdb:
                assert '#' not in line, "Line should not be present in output PDB: {}".format(line)

if __name__ == '__main__':
    unittest.main()
