#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import os

from ccpem_gui.utils import window_utils
from ccpem_core.tasks.molrep import molrep_task
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_gui.utils import command_line_launch
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import molrep as test_data


class MolrepWindow(window_utils.CCPEMTaskWindow):
    '''
    Molrep window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(MolrepWindow, self).__init__(task=task,
                                           parent=parent)

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)

        # Mode
        self.mode_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='mode',
            second_width=None,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.mode_input)

        # Input map
        self.input_map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_map',
            required=True,
            file_types=ccpem_file_types.mrc_ext,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.input_map_input)

        # Input pdb
        input_pdb_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_pdb',
            required=True,
            file_types=ccpem_file_types.pdb_ext,
            args=self.args)
        self.args_widget.args_layout.addWidget(input_pdb_input)

        # Copies to find
        n_monomers_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='n_monomers',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(n_monomers_input)

        # Highly symmetrical structure?
        ncsm = window_utils.NumberArgInput(
            parent=self,
            arg_name='ncsm',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(ncsm)

        # Number of rotation peaks
        r_peaks = window_utils.NumberArgInput(
            parent=self,
            arg_name='r_peaks',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(r_peaks)

        # Number of rotation peaks
        t_peaks = window_utils.NumberArgInput(
            parent=self,
            arg_name='t_peaks',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(t_peaks)

        # Extended options
        extended_options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Extended options',
            button_tooltip='Show extended options')
        self.args_widget.args_layout.addLayout(extended_options_frame)

        # Fixed model pdb
        fixed_pdb_input = window_utils.FileArgInput(
            parent=self,
            arg_name='fixed_pdb',
            required=False,
            file_types=ccpem_file_types.pdb_ext,
            args=self.args)
        extended_options_frame.add_extension_widget(fixed_pdb_input)

        # Input seq
        self.seq_input = window_utils.FileArgInput(
            parent=self,
            arg_name='target_seq',
            args=self.args,
            required=False)
        extended_options_frame.add_extension_widget(self.seq_input)

        # Turn off scoring
        score_no = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='score_no',
            required=True,
            args=self.args)
        extended_options_frame.add_extension_widget(score_no)

        # Run sfcheck
        run_sfcheck_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='run_sfcheck',
            args=self.args)
        extended_options_frame.add_extension_widget(run_sfcheck_input)

        # Set scale
        scale_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='scale_xyz',
            required=False,
            args=self.args)
        extended_options_frame.add_extension_widget(scale_input)

        # Keyword entry
        self.keyword_entry = window_utils.KeywordArgInput(
            parent=self,
            arg_name='keywords',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.keyword_entry)

        # Set inputs for launcher
        self.launcher.add_file(
            arg_name='input_map',
            file_type='map',
            description=self.args.input_map.help,
            selected=True)
        self.launcher.add_file(
            arg_name='input_pdb',
            file_type='pdb',
            description=self.args.input_pdb.help,
            selected=True)
        self.launcher.add_file(
            arg_name='fixed_pdb',
            file_type='pdb',
            description=self.args.fixed_pdb.help,
            selected=True)

    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.
        '''
        corrected_pdb = os.path.join(self.pipeline.location, 'align.pdb')
        if os.path.isfile(corrected_pdb):
            self.launcher.add_file(
                arg_name=None,
                path=corrected_pdb,
                file_type='pdb',
                description='Corrected model file',
                selected=True)
        
        self.output_pdb = os.path.join(self.pipeline.location, 'molrep.pdb')
        # Set launcher
        self.launcher.add_file(
            arg_name=None,
            path=self.output_pdb,
            file_type='pdb',
            description='Output PDB file',
            selected=True)
        #
        sfcheck_ps = os.path.join(self.pipeline.location,
                                  'sfcheck_XXXX.ps')
        self.launcher.add_file(
            arg_name=None,
            path=sfcheck_ps,
            description='SFCHECK output',
            selected=True)
        #
        if self.args.scale_xyz.value != 1.0:
            self.scaled_map = os.path.join(self.pipeline.location,
                                           'sfcheck.map')
            self.launcher.add_file(
                arg_name=None,
                path=self.scaled_map,
                file_type='map',
                description='Scaled map',
                selected=True)
        self.launcher.set_tree_view()

def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=molrep_task.MolRep,
        window_class=MolrepWindow)

if __name__ == '__main__':
    main()

