C*** sleuth_view.inc
	real*4          bignum
	parameter       (bignum = 10.E+10)
	real*4		defedge
	parameter	(defedge = 0.05)
	real*4		defcut
	parameter	(defcut = 0.005)
	real*4		binsize
	parameter	(binsize = 1.0)
	real*4		defradfac
	parameter	(defradfac = 1.2)
	real*4		defratsdevs
	parameter	(defratsdevs = 1.0)
	real*4		defsdevs
	parameter	(defsdevs = 3.0)
	real*4		defsdevpar
	parameter	(defsdevpar = 2.5)
        real*4       	denmax
        parameter       (denmax = 255.0)
        real*4       	denmin
        parameter       (denmin = 1.)
	real*4          grey   
        parameter       (grey = 125.0)
	integer*4       iautoinc
        parameter       (iautoinc = 5)
	integer*4	icrad
	parameter	(icrad = 4)
	integer*4	idevcands
	parameter	(idevcands = 4)
	integer*4	idevcoords
	parameter	(idevcoords = 8)
	integer*4	idevdata
	parameter	(idevdata = 12)
	integer*4	idevimage
	parameter	(idevimage = 1)
	integer*4	idevlog
	parameter	(idevlog = 11)
	integer*4	idevparam
        parameter       (idevparam = 13)
	integer*4	idevrefin
	parameter	(idevrefin = 2)
	integer*4	idevrefout
	parameter	(idevrefout = 7)
	integer*4	idevscript
	parameter	(idevscript = 10)
	integer*4	idevstack
	parameter	(idevstack = 9)
	integer*4	idevstackout
	parameter	(idevstackout = 3)
	integer*4	imap
	parameter	(imap = 0)
	integer*4	idefaultcursor
	parameter	(idefaultcursor = 0)
	integer*4	iskullcursor
	parameter	(iskullcursor = 9)
	logical		itest
	parameter	(itest = .false.)
c	parameter	(itest = .true.)
	integer*4	izoomfac
	parameter	(izoomfac = 8)
	integer*4	izoomsize
	parameter	(izoomsize = 512)
	integer*4	maxbins
	parameter	(maxbins = 1000)
        integer*4       max_colour
        parameter       (max_colour = 65535)
	integer*4	maxlabels
	parameter	(maxlabels = 30)
C*** max radius provides maximum # rings for ring means
        integer*4       maxrings
        parameter       (maxrings = 12)
C*** maximum number of final coordinates
 	integer*4	maxcoords
	parameter	(maxcoords = 5000)
C*** set maximum compression factor to 5
	integer*4	maxiters
	parameter	(maxiters = 10)
        integer*4       maxline
        parameter       (maxline = 400)
	integer*4	maxbox
	parameter	(maxbox = maxline * maxline)
C!!! reduce this value this for smaller machines
	integer*4	maxmapline
	parameter	(maxmapline = 5000)
	integer*4	maxsize
	parameter	(maxsize = maxmapline * maxmapline)
        integer*4       maxpix
        parameter       (maxpix = 100 * 100)
        integer*4       maxrefs
        parameter       (maxrefs = 200)
	integer*4	maxsectors
	parameter	(maxsectors = 16)
        integer*4       maxcolour
        parameter       (maxcolour = 65535)
        integer*4       min_den
        parameter       (min_den = 0)
        integer*4       max_den
        parameter       (max_den = 127)
C*** nexclude is the # rings to exclude near the centre
	integer*4	nexclude
	parameter	(nexclude = 3)
	integer*4	nmaxrings
	parameter	(nmaxrings = 25)
C*** maximum entries per cluster approximates pi * (r / 2) **2
	integer*4	maxclus
	parameter	(maxclus = nmaxrings * nmaxrings)
C*** maximum # of candidate particles allowed
	integer*4	maxcands
	parameter	(maxcands = maxclus * maxcoords)
C*** maxdens is the max # densities in a ring = pi * d, and
C*** d <=maxrings + nexclude
	integer*4	maxdens
	parameter	(maxdens = 4 * (nmaxrings + nexclude))
	real*4		pi
	parameter	(pi=3.14159)
	real*4		pio2
	parameter	(pio2 = pi * 0.5)
	real*4		pio4
	parameter	(pio4 = pi * 0.25)
	real*4		pio8
	parameter	(pio8 = pi * 0.125)
	real*4		pixhigh
	parameter	(pixhigh = 0.5)
	integer*4	npixlowdef
	parameter	(npixlowdef = 7)
	real*4		radmin
	parameter	(radmin = 7.0)
	real*4		radius_max
	parameter	(radius_max = maxrings + nexclude)
	real*4		scalefac
	parameter	(scalefac = 10.0)
	real*4		taperfac1
	parameter	(taperfac1 = 10.0)
        real*4          taperfac2
        parameter       (taperfac2 = 0.9)
	real*4		threshmin
	parameter	(threshmin = 0.1)
C***
	character	intoch*256
	character	iolabel(maxlabels)*80
	character	menulist(maxlabels)*64
	character	label*80
	character	return_string*80
C***
        integer*4       indexband(maxline,2)
        integer*4       indexmax(maxline,2)
        integer*4       indexmin(maxline,2)
        integer*4       lxyz(3)
        integer*4       nxyz(3)
        integer*4       mxyz(3)
	integer*4	jxyz(3)
	integer*4	kxyz(3)
        integer*4       irgb 
        integer*4       red(0:255)
        integer*4       blue(0:255)
        integer*4       green(0:255)
C***
	real*4		aline(maxmapline)
        real*4          colourscale
        real*4          fmaxcolour
        real*4          frange
        real*4          ringmean(nmaxrings)
        real*4          ringsdev(nmaxrings)
	real*4		sectsum(maxsectors)
C***
C***
C***
	logical		gui
	logical		reverse
	logical		there
C***
	common		dmin,dmax,dmean,threshold,
     *			nxyz, mxyz,
     *			iolabel, nlabels,
     *			there, gui,
     *			cutmin,cutmax,
     *			tmin,tmax,tmean,threshlow,
     *			amin,amax,amean,
     *                  icompress,ixystart,ierr,
     *			reverse,
     *                  xcentre,ycentre,cenx,ceny,
     *			radius,radsq,radmax,radmaxsq,radgyr,
     *                  bandmean, bandvar,
     *                  boxmean, boxsdev,
     *                  centmean, centvar, centsum,
     *			histedge,histcut,mictype,
     *			sectsum,minsect,
     *                  iboxedge,nrad,nrings,ringmean,ringsdev,
     *			ipixhigh,ipixlow,
     *			lxyz,
     *			ifirstimage,
     *			max_screen_width, max_screen_height
C***
	common/colour/colourscale, fmaxcolour, cmin, cmax
