#!/bin/csh -f
source /public/xtal/ccp4/xtal.login

if ($#argv == 0) then
	echo "Usage: convmap <map_name>"
	echo "Converts <map_name>.map to <map_name>_cnv.map"
	exit
endif

set mapin = $1.map
if (! -e $mapin ) then
	echo "File $mapin does not exist"
	exit
endif
set mapout = $1_cnv.map


/public/xtal/ccp4/bin/mapmask mapin $mapin mapout $mapout << eof
symmetry 1
eof



