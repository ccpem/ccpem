#!/bin/csh
#
#  these soft links are used for various libraries and command files.
#
ln -s /public/curvy84/curvy84.com  curvy
ln -s /public/image/com/fftrans.com fftrans
ln -s /public/image/com/header.com header
ln -s /public/image/com/histo.com histo
ln -s /public/image/com/imedit.com imedit
ln -s /public/image/com/imexchange.com imexchange
ln -s /public/image/com/loclop.com loclop
ln -s /public/image/com/trilog.com trilog
ln -s /public/image/bin/image_display.exe imdisp
ln -s /public/image/bin/label.exe label
ln -s /public/laser/laserprint.exe laserprint
ln -s /public/xtal/ccp4/lib/libccp4.a libgenlib.a
ln -s /public/xtal/ipdisp/bin/libxdl_view.a libxdl_view.a
ln -s /public/image/imlib/ifftlib.a libifftlib.a
ln -s /public/image/imlib/imlib.a libimlib.a
ln -s /public/plot82/plot82lib.a libplot82.a
ln -s /public/plot84/plot84lib.a libplot84.a
ln -s /public/nag/libnag.a libnaglib.a
