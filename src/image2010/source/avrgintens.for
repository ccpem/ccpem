C	PROGRAM TO OBTAIN WEIGHTED AVERAGE INTENSITIES FROM MERGED LIST
C	INPUT IS OUTPUT FROM TOM'S MERGDIFF PROGRAM
C	OUTPUT TO INCLUDE EXPERIMENTAL R-FACTOR
C
C	VX1.0	RH	pre-1988	original rough program for getting 2.8
C					Angstrom bR projection intensities.
C	VX1.1	RH	21-Mar-1993	calculate R-factor, clean up code.
C	VX1.2	TSH	 4-Jan-2001	minor
C
C
C	     Control cards.
C	1.	A,B,GAMMA	Cell dimensions for statistics.
C	2.	NSER,ZMIN,ZMAX	Serial number on input file and zrange to use.
C
C
C
      	PARAMETER (NMAX=200)
	DIMENSION IH(NMAX),IK(NMAX),Z(NMAX),AMPSQ(NMAX),SIGMA(NMAX)
	DIMENSION TITLE(10)
C
      WRITE(6,1000)
1000	FORMAT(/'  AVRGINTENS vx1.2(4-Jan-2001)'/)
      	DRAD=3.1415926/180.0
        ASTAR=1.0/(62.45*0.866) ! purple membrane - old
      	READ(5,*) A,B,GAMMA
      	WRITE(6,1001) A,B,GAMMA
1001	FORMAT(' Cell dimensions read in', 3f10.2)
      	ASTAR=1.0/(A*SIN(GAMMA*DRAD))
      	BSTAR=1.0/(B*SIN(GAMMA*DRAD))
	READ(5,15)NSER,ZMIN,ZMAX
15	FORMAT(I10,2F10.5)
	READ(1,10)ISER
10	FORMAT(I10)
	IF(ISER.NE.NSER)THEN
		WRITE(6,13)NSER,ISER
13	FORMAT(' UNEXPECTED SERIAL NUMBER; REQUESTED',I10,' FOUND',I10)
		GO TO 500
      	ELSE
			WRITE(6,12)ISER
12	FORMAT(' CORRECT SERIAL NUMBER FOUND',I10)
	END IF
	BACKSPACE 1
	READ(1,11)TITLE
11	FORMAT(10A4)
C
	WRITE(6,14)TITLE
14	FORMAT(' TITLE OF MERGED LIST',10A4/)
	WRITE(6,16)ZMIN,ZMAX
16	FORMAT(' RANGE OF Z VALUES FOR INCLUSION IN AVERAGE',2F10.4/)
	WRITE(2,11)TITLE
C
	WRITE(6,22)
22	FORMAT(/' REFLECTION   NUMBER   AMPCOMBINED'//)
C
	IEND=0
	LH=0
	LK=0
	NUSED=0
	NPASS=0
	NREFL0=0
	NREFL1=0
	NREFLN=0
	NREAD=0
      	NOBS=0
      	NSTATS=0
      	AMPSQTOT=0.0
      	DIFFTOT=0.0
C
	I=1
19	READ(1,*,END=49)IH(I),IK(I),Z(I),AMPSQ(I),SIGMA(I),IFILM
      	IF(IH(I).EQ.100) GO TO 49
C
C   FUDGE THE MEASURED DEVIATION TO ENSURE REASONABLE SIGMAS !!!
      	SIGMA(I)=SIGMA(I)+10.0+0.05*AMPSQ(I)
C
	NREAD=NREAD+1
20	FORMAT(2I5,F10.4,2F10.2,I6)
21	IF(IH(I).EQ.LH.AND.IK(I).EQ.LK)THEN
C		CONTINUING SAME INDICES
          IF(Z(I).GE.ZMIN.AND.Z(I).LE.ZMAX)THEN
C		  Z NEAR ENOUGH TO ZERO
		  I=I+1
		  GO TO 19
		ELSE
C	  Z TOO FAR FROM ZERO;  BYPASS THIS DATA POINT
		  NPASS=NPASS+1
C	 	  WRITE(6,5018) IH(I),IK(I),Z(I),NPASS
5018		  FORMAT(2I5,F10.4,I5)
		  GO TO 19
		ENDIF
	ELSE
C		INDICES HAVE CHANGED
C		STORE LINE OF DATA
		ISAVEH=IH(I)
		ISAVEK=IK(I)
		SAVEZ=Z(I)
		SAVEAMPSQ=AMPSQ(I)
		SAVESIGMA=SIGMA(I)
48		NSPOT=I-1
		IF(LH.EQ.0.AND.LK.EQ.0)GO TO 31
		IF(NSPOT.EQ.0)THEN
			NREFL0=NREFL0+1
			GO TO 31
		END IF
	NUSED=NUSED+NSPOT
	IF(NSPOT.EQ.1)THEN
	NREFL1=NREFL1+1
C	ONLY ONE INTENSITY
      	AMPCOMB=AMPSQ(1)
      	SIGCOMB=SIGMA(1)
      	IF(AMPCOMB.LE.0.0) AMPCOMB=1.0
	WRITE(6,23)LH,LK,NSPOT,AMPCOMB,SIGCOMB
	WRITE(2,30)LH,LK,AMPCOMB,SIGCOMB
	GO TO 31
	ELSE
	CALL COMBINE(NSPOT,AMPCOMB,AVDIFF,SIGCOMB,IH,IK,Z,AMPSQ,SIGMA)
	NREFLN = NREFLN+1
      	NSTATS=NSTATS+NSPOT
      	AMPSQTOT = AMPSQTOT+AMPCOMB
      	DIFFTOT = DIFFTOT+AVDIFF
C
	WRITE(6,23)LH,LK,NSPOT,AMPCOMB,SIGCOMB
23	FORMAT(1X,I3,I5,I9,2X,F10.2,F10.3,F10.3,A4,2F10.2)
	WRITE(2,30)LH,LK,AMPCOMB,SIGCOMB
30	FORMAT(2I4,2F8.1)
      	NOBS=NOBS+1
      	END IF
C
C	NEW INDICES FOR NEXT SPOT
31	IF(IEND.EQ.1)GO TO 50
	LH=ISAVEH
	LK=ISAVEK
	I=1
	IH(I)=ISAVEH
	IK(I)=ISAVEK
	Z(I)=SAVEZ
	AMPSQ(I)=SAVEAMPSQ
	SIGMA(I)=SAVESIGMA
	GO TO 21
	END IF
C
49	IEND=1
	GO TO 48
50	WRITE(6,51)
51	FORMAT(' END OF MERGED LIST')
	WRITE(6,52)NREAD,NUSED,NPASS
52	FORMAT(' # DATA POINTS READ',I10/
     .' # DATA POINTS USED',I10/' # DATA POINTS SKIPPED AS',
     .' OUTSIDE Z  RANGE CHOSEN',I10)
	WRITE(6,53)NREFLN,NREFL1,NREFL0
53	FORMAT(' # REFLECTIONS WITH MORE THAN ONE MEASUREMENT AVERAGED',
     .I5/' # WITH ONLY ONE MEASUREMENT',I5/' # WITH ZERO MEASUREMENTS',
     .' IN CHOSEN Z RANGE',I5)
      	IF(AMPSQTOT.NE.0.0) WRITE(6,54) DIFFTOT/AMPSQTOT, NSTATS
54	FORMAT(/' OVERALL R-FACTOR =',F10.4,' for',I5,' repeated meas.')
C
500	CONTINUE
C********
	STOP
	END
C**************************************************************************
	SUBROUTINE COMBINE(NSPOT,AMPCOMB,AVDIFF,SIGCOMB,
     .	 IH,IK,Z,AMPSQ,SIGMA)
	DIMENSION IH(1),IK(1),Z(1),AMPSQ(1),SIGMA(1)
C
	SUMWEIGHT=0.0
      	SUMAMPSQ=0.0
	DO 10 I=1,NSPOT
C	WRITE(6,20)I,AMPSQ(I),SIGMA(I)
20	FORMAT(I5,F10.2)
	WEIGHT=1.0/SIGMA(I)**2
C	WRITE(6,23)WEIGHT
23	FORMAT(' WEIGHT',3F10.5)
	SUMWEIGHT=SUMWEIGHT+WEIGHT
      	SUMAMPSQ=SUMAMPSQ+AMPSQ(I)*WEIGHT
10	CONTINUE
	IF(SUMAMPSQ.LE.0.0)THEN
	AMPCOMB=1.0
	GO TO 11
	ELSE
	AMPCOMB=SUMAMPSQ/SUMWEIGHT
	END IF
11	CONTINUE
C  Now calculate standard deviation of mean.
      	SUMSQ=0.0
      	SUMAV=0.0
      	DO 50 I=1,NSPOT
      	SUMAV=SUMAV+ABS(AMPCOMB-AMPSQ(I))
50    	SUMSQ=SUMSQ+(AMPCOMB-AMPSQ(I))**2
      	DEVMEAN=SUMSQ/NSPOT
      	AVDIFF=SUMAV/NSPOT
      	RMSDEV=SQRT(DEVMEAN)
      	SIGCOMB=RMSDEV/SQRT(FLOAT(NSPOT-1))
	RETURN
	END
C*****************************************************************************
