C*****************************************************************************
C
C	CTFFIND2 - determines defocus and astigmatism for images of
C	arbitrary size (MRC format). Astigmatic angle is measured form
C	x axis (same convetiones as in the MRC 2D image processing
C	programs).
C
C	CARD 1: Input file name for image
C	CARD 2: Output file name to check result
C	CARD 3: CS[mm], HT[kV], AmpCnst, XMAG, DStep[um]
C	CARD 4: Box, ResMin[A], ResMax[A], dFMin[A], dFMax[A], FStep
C
C		The output image file to check the result of the fitting
C		shows the filtered average power spectrum of the input
C		image in one half, and the fitted CTF (squared) in the
C		other half. The two halfs should agree very well for a
C		sucessfull fit.
C
C		CS: Spherical aberration coefficient of the objective in mm
C		HT: Electron beam voltage in kV
C		AmpCnst: Amount of amplitude contrast (fraction). For ice
C		         images 0.07, for negative stain about 0.15.
C		XMAG: Magnification of original image
C		DStep: Pixel size on scanner in microns
C		Box: Tile size. The program devides the image into square
C		     tiles and calculates the average power spectrum. Tiles
C		     with a significatly higher or lower variance are
C		     excluded; these are parts of the image which are unlikely
C		     to contain useful information (beam edge, film number
C		     etc). IMPORTANT: Box must have a value of power of 2.
C		ResMin: Low resolution end of data to be fitted.
C		ResMaX: High resolution end of data to be fitted.
C		dFMin: Starting defocus value for grid search in Angstrom.
C		       Positive values represent an underfocus. The program
C		       performs a systematic grid search of defocus values
C		       and astigmatism before fitting a CTF to machine
C		       precision.
C		dFMax: End defocus value for grid search in Angstrom.
C		FStep: Step width for grid search in Angstrom.
C
C*****************************************************************************
C	example command file (UNIX):
C
C	#!/bin/csh -x
C	#
C	#   ctffind2
C	#
C	time /public/image/bin/ctffind2.exe << eof
C	image.mrc
C	power.mrc
C	2.6,200.0,0.07,60000.0,28.0
C	128,100.0,15.0,30000.0,90000.0,5000.0
C	eof
C	#
C*****************************************************************************
C
      PROGRAM CTFFIND2
C
C	NIKO, 11 MAY 1998
C
      IMPLICIT NONE
C
      INTEGER NXYZ(3),MXYZ(3),MODE,JXYZ(3),I,J,NX,NY,IX,IS
      INTEGER IXMX,IYMX,IXBMX,K,LXYZ(3),CNT,ID,L,M,LL,MM,ITEST
      INTEGER jstart,jfinsh
      PARAMETER (IXMX=20000,IYMX=512,IXBMX=IYMX)
      REAL DMIN,DMAX,DMEAN,AIN(IXMX*IYMX),DRMS,RMSLIM,RMS,WGH1,WGH2
      REAL ABOX(IXBMX*IXBMX),POWER(IXBMX/2*IXBMX),SCAL,MEAN,WL
      REAL CS,KV,WGH,XMAG,DSTEP,RESMIN,RESMAX,DFMID1,DFMID2,ANGAST
      REAL THETATR,STEPR,RMIN2,RMAX2,HW,OUT(IXBMX*IXBMX)
      REAL RES2,CTF,CTFV,TMP,FLT,DFMIN,DFMAX,FSTEP
      PARAMETER (RMSLIM=1.5,FLT=-0.1)
      DOUBLE PRECISION DDMEAN,DDSQR
      COMPLEX CIN(IXMX/2*IYMX),CINS(IYMX)
      COMPLEX CBOX(IXBMX/2*IXBMX),CBOXS(IXBMX)
      COMPLEX CPOW(IXBMX/4*IXBMX),CPOWS(IXBMX)
      CHARACTER FILEIN*70,FILEOUT*70
      EQUIVALENCE (AIN,CIN)
      EQUIVALENCE (ABOX,CBOX)
      EQUIVALENCE (POWER,CPOW)
      COMMON/FUNC/CS,WL,WGH1,WGH2,THETATR,RMIN2,RMAX2,POWER,JXYZ,HW
C
      WRITE(6,1000)
1000  FORMAT(/' CTF DETERMINATION, V1.0 (12-Oct-1998)'//,
     +	      ' Input image file name')
      READ(5,1010)FILEIN
1010  FORMAT(A)
      WRITE(6,1010)FILEIN
      WRITE(6,1060)
1060  FORMAT(/' Output diagnostic file name')
      READ(5,1010)FILEOUT
      WRITE(6,1010)FILEOUT
      WRITE(6,1020)
1020  FORMAT(/' CS[mm], HT[kV], AmpCnst, XMAG, DStep[um]')
CTSH      READ(5,1030)CS,KV,WGH,XMAG,DSTEP
CTSH1030  FORMAT(5F)
CTSH++
      READ(5,*)CS,KV,WGH,XMAG,DSTEP
CTSH--
      WRITE(6,1031)CS,KV,WGH,XMAG,DSTEP
1031  FORMAT(F5.1,F9.1,F8.2,F10.1,F9.1)
      WRITE(6,1040)
1040  FORMAT(/' Positive defocus values for underfocus',
     +	     /' Box, ResMin[A], ResMax[A], dFMin[A], dFMax[A], FStep')
CTSH      READ(5,1050)JXYZ(1),RESMIN,RESMAX,DFMIN,DFMAX,FSTEP
CTSH1050  FORMAT(1I,5F)
CTSH++
      READ(5,*)JXYZ(1),RESMIN,RESMAX,DFMIN,DFMAX,FSTEP
CTSH--
      WRITE(6,1051)JXYZ(1),RESMIN,RESMAX,DFMIN,DFMAX,FSTEP
1051  FORMAT(I4,2F11.1,2F10.1,F7.1/)
      IF (JXYZ(1).GT.IXBMX) THEN
      	WRITE(6,1070)IXBMX
1070	FORMAT(/' Box size too big, IXBMX=',I10)
      	STOP
      ENDIF
      ITEST=2**INT(LOG(REAL(JXYZ(1)))/LOG(2.0))
      IF (ITEST.LT.JXYZ(1)) ITEST=ITEST*2
      IF (ITEST.NE.JXYZ(1)) THEN
      	WRITE(6,1090)
1090	FORMAT(/' Box size must be power of 2')
      	STOP
      ENDIF
      JXYZ(2)=JXYZ(1)
      JXYZ(3)=1
      IF (RESMIN.LT.RESMAX) THEN
      	TMP=RESMAX
      	RESMAX=RESMIN
      	RESMIN=TMP
      ENDIF
      IF (DFMAX.LT.DFMIN) THEN
      	TMP=DFMAX
      	DFMAX=DFMIN
      	DFMIN=TMP
      ENDIF
C
      CALL IMOPEN(1,FILEIN,'RO')
      CALL IRDHDR(1,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
      IF (NXYZ(1).GT.IXMX) THEN
      	WRITE(6,1080)IXMX
1080	FORMAT(/' Image X dimensions too big, IXMX=',I10)
      	STOP
      ENDIF
C
      WRITE(*,1110)NXYZ(1),NXYZ(2)
1110  FORMAT(/,' READING IMAGE...'/' NX, NY= ',2I10)
C
      DDMEAN=0.0D0
      DDSQR=0.0D0
      DO 10 J=1,NXYZ(2)
      	CALL IRDLIN(1,AIN(1),*999)
        DO 10 I=1,NXYZ(1)
          DDMEAN=DDMEAN+DBLE(AIN(I))
      	  DDSQR=DDSQR+DBLE(AIN(I)**2)
10    CONTINUE
C
      DDMEAN=DDMEAN/NXYZ(1)/NXYZ(2)
      DDSQR=DDSQR/NXYZ(1)/NXYZ(2)
      DRMS=SQRT(DDSQR-DDMEAN**2)
C
      DO 30 K=1,JXYZ(1)*JXYZ(2)/2
      	POWER(K)=0.0
30    CONTINUE
      SCAL=1.0/SQRT(REAL(JXYZ(1)*JXYZ(2)))
      NX=NXYZ(1)/JXYZ(1)
      NY=NXYZ(2)/JXYZ(2)
      CNT=0
      CALL IMPOSN(1,0,0)
      WRITE(*,1100)
1100  FORMAT(/,' TILING IMAGE...'/)
C
      DO 100 I=1,NY
        DO 20 J=1,JXYZ(2)
      	  ID=1+NXYZ(1)*(J-1)
          CALL IRDLIN(1,AIN(ID),*999)
20	CONTINUE
      	DO 100 J=1,NX
      	  IX=(J-1)*JXYZ(1)+1
      	  CALL BOXIMG(AIN,NXYZ,ABOX,JXYZ,IX,1,MEAN,RMS)
      	  IF ((RMS.LE.DRMS*RMSLIM).AND.(RMS.GE.DRMS/RMSLIM/10.0)) THEN
      	    CALL RLFT3(ABOX,CBOXS,JXYZ(1),JXYZ(2),1,1)
      	    DO 40 K=1,JXYZ(1)*JXYZ(2)/2
      	      POWER(K)=POWER(K)+CABS(CBOX(K)*SCAL)**2
40	    CONTINUE
      	    CNT=CNT+1
      	  ENDIF
100   CONTINUE
      write(*,*)'Total tiles and number used',NX*NY,CNT
C
      DO 60 K=1,JXYZ(1)*JXYZ(2)/2
      	POWER(K)=SQRT(POWER(K))
60    CONTINUE
      LXYZ(1)=JXYZ(1)/2
      LXYZ(2)=JXYZ(2)
      LXYZ(3)=JXYZ(3)
      SCAL=1.0/CNT
      CALL SCLIMG(POWER,LXYZ,SCAL)
C
      CALL RLFT3(POWER,CPOWS,LXYZ(1),LXYZ(2),1,1)
      CALL MSKTRN(CPOW,CPOWS,CPOW,CPOWS,LXYZ,FLT)
      CALL RLFT3(POWER,CPOWS,LXYZ(1),LXYZ(2),1,-1)
      SCAL=1.0/REAL(LXYZ(1)*LXYZ(2))
      CALL SCLIMG(POWER,LXYZ,SCAL)
C
      CS=CS*(10.0**7.0)                         ! Angstroms
      KV=KV*1000.0                              ! Volts
      WL=12.3/SQRT(KV+KV**2/(10.0**6.0))        ! Angstroms
      WGH1=SQRT(1.0-WGH**2)
      WGH2=WGH
C
      STEPR=DSTEP*(10.0**4.0)/XMAG
      THETATR=WL/(STEPR*JXYZ(1))
      RESMIN=STEPR/RESMIN
      RESMAX=STEPR/RESMAX
C
      DFMID1=DFMIN
      DFMID2=DFMAX
      CALL SEARCH_CTF(CS,WL,WGH1,WGH2,THETATR,RESMIN,RESMAX,
     +	       POWER,JXYZ,DFMID1,DFMID2,ANGAST,FSTEP)
      RMIN2=RESMIN**2
      RMAX2=RESMAX**2
      HW=-1.0/RMAX2

      CALL REFINE_CTF(DFMID1,DFMID2,ANGAST)
C
      DO 50 I=1,JXYZ(1)*JXYZ(2)
      	OUT(I)=0.0
50    CONTINUE
      DO 200 L=1,JXYZ(1)/2
        LL=L-1
        DO 200 M=1,JXYZ(2)
          MM=M-1
          IF (MM.GT.JXYZ(2)/2) MM=MM-JXYZ(2)
          ID=L+JXYZ(1)/2*(M-1)
      	  I=L+JXYZ(1)/2
      	  J=M+JXYZ(2)/2
      	  IF (J.GT.JXYZ(2)) J=J-JXYZ(2)
      	  IS=I+JXYZ(1)*(J-1)
      	  OUT(IS)=POWER(ID)
      	  IF (OUT(IS).GT.1.0) OUT(IS)=1.0
      	  IF (OUT(IS).lT.-1.0) OUT(IS)=-1.0
          RES2=(REAL(LL)/JXYZ(1))**2+(REAL(MM)/JXYZ(2))**2
          IF ((RES2.LE.RMAX2).AND.(RES2.GE.RMIN2)) THEN
            CTFV=CTF(CS,WL,WGH1,WGH2,DFMID1,DFMID2,
     +               ANGAST,THETATR,LL,MM)
CJMS 16.11.2001      write(*,*)'ctf,ll,mm',ctfv,ll,mm
     	    I=JXYZ(1)/2-L+1
      	    J=JXYZ(2)-J+2
      	    IF (J.LE.JXYZ(2)) THEN
      	      IS=I+JXYZ(1)*(J-1)
              OUT(IS)=CTFV**2
      	    ENDIF
          ENDIF
200   CONTINUE
c      do 202 i=1,jxyz(2)
c      jstart=1+(i-1)*jxyz(1)
c      jfinsh=i*jxyz(1)
c202   write(*,201)(OUT(j),j=jstart,jfinsh)
c201   format(/20(30f4.1/))
      MODE=2
      CALL WRTIMG(FILEOUT,JXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN,OUT)
C
      GOTO 9999
999   STOP 'END-OF-FILE ERROR ON READ'
9999  CONTINUE
      CALL IMCLOSE(1)
      END
C
C**************************************************************************
      SUBROUTINE SCLIMG(AIN,NXYZ,SCAL)
C**************************************************************************
C
      IMPLICIT NONE
C
      INTEGER I,J,NXYZ(3),ID
      REAL SCAL,AIN(*)
C
      DO 10 J=1,NXYZ(2)
        DO 10 I=1,NXYZ(1)
          ID=I+NXYZ(1)*(J-1)
          AIN(ID)=AIN(ID)*SCAL
10    CONTINUE
C
      RETURN
      END
C
C**************************************************************************
      SUBROUTINE WRTIMG(FNAM,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN,AOUT)
C**************************************************************************
C
      IMPLICIT NONE
C
      INTEGER I,J,NXYZ(3),MXYZ(3),MODE,ID,NXYZST(3)
      REAL DMIN,DMAX,DMEAN,AOUT(*)
      DOUBLE PRECISION DDMEAN
      CHARACTER FNAM*70,TITLE*80
      DATA TITLE/' OUTPUT IMAGE FROM TUBE_UNBEND'/
C
      WRITE(*,1000)NXYZ(1),NXYZ(2)
1000  FORMAT(/,' WRITING IMAGE...'/' NX, NY= ',2I10)
C
      CALL IMOPEN(2,FNAM,'NEW')
      CALL ITRHDR(2,1)
      CALL IALMOD(2,MODE)
      NXYZST(1)=0
      NXYZST(2)=0
      NXYZST(3)=0
      CALL IALSIZ(2,NXYZ,NXYZST)
      CALL IWRHDR(2,TITLE,1,0.0,0.0,0.0)
      DMIN=1.E10
      DMAX=-1.E10
      DDMEAN=0.0D0
      DO 10 J=1,NXYZ(2)
      	ID=1+NXYZ(1)*(J-1)
      	CALL IWRLIN(2,AOUT(ID))
      	DO 10 I=1,NXYZ(1)
      	  ID=I+NXYZ(1)*(J-1)
      	  DDMEAN=DDMEAN+DBLE(AOUT(ID))
      	  IF (AOUT(ID).LT.DMIN) DMIN=AOUT(ID)
      	  IF (AOUT(ID).GT.DMAX) DMAX=AOUT(ID)
10    CONTINUE
      DMEAN=SNGL(DDMEAN)/NXYZ(1)/NXYZ(2)
      CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
      CALL IMCLOSE(2)
      WRITE(*,1030)DMIN,DMAX,DMEAN
1030  FORMAT(/,' NEW VALUES:',
     +       /,' DMIN, DMAX, DMEAN      ',3F14.3)
C
      RETURN
      END
C
C**************************************************************************
      SUBROUTINE rlft3(data,speq,nn1,nn2,nn3,isign)
C**************************************************************************
      INTEGER isign,nn1,nn2,nn3
      COMPLEX data(*),speq(*)
CU    USES fourn
      INTEGER i1,i2,i3,j1,j2,j3,nn(3),id,is,nn1h
      DOUBLE PRECISION theta,wi,wpi,wpr,wr,wtemp
      COMPLEX c1,c2,h1,h2,w
      c1=cmplx(0.5,0.0)
      c2=cmplx(0.0,-0.5*isign)
      theta=6.28318530717959d0/dble(isign*nn1)
      wpr=-2.0d0*sin(0.5d0*theta)**2
      wpi=sin(theta)
      nn(1)=nn1/2
      nn(2)=nn2
      nn(3)=nn3
      nn1h=nn1/2
      if(isign.eq.1)then
        call fourn(data,nn,3,isign)
        do 12 i3=1,nn3
          do 11 i2=1,nn2
            id=1+nn1h*((i2-1)+nn2*(i3-1))
            is=i2+nn2*(i3-1)
            speq(is)=data(id)
11        continue
12      continue
      endif
      do 15 i3=1,nn3
        j3=1
        if (i3.ne.1) j3=nn3-i3+2
        wr=1.0d0
        wi=0.0d0
        do 14 i1=1,nn1/4+1
          j1=nn1/2-i1+2
          do 13 i2=1,nn2
            j2=1
            if (i2.ne.1) j2=nn2-i2+2
            if(i1.eq.1)then
              id=1+nn1h*((i2-1)+nn2*(i3-1))
              is=j2+nn2*(j3-1)
              h1=c1*(data(id)+conjg(speq(is)))
              h2=c2*(data(id)-conjg(speq(is)))
              data(id)=h1+h2
              speq(is)=conjg(h1-h2)
            else
              id=i1+nn1h*((i2-1)+nn2*(i3-1))
              is=j1+nn1h*((j2-1)+nn2*(j3-1))
              h1=c1*(data(id)+conjg(data(is)))
              h2=c2*(data(id)-conjg(data(is)))
              data(id)=h1+w*h2
              data(is)=conjg(h1-w*h2)
            endif
13        continue
          wtemp=wr
          wr=wr*wpr-wi*wpi+wr
          wi=wi*wpr+wtemp*wpi+wi
          w=cmplx(sngl(wr),sngl(wi))
14      continue
15    continue
      if(isign.eq.-1)then
        call fourn(data,nn,3,isign)
      endif
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software -2#,-".
C
C**************************************************************************
      SUBROUTINE fourn(data,nn,ndim,isign)
C**************************************************************************
      INTEGER isign,ndim,nn(ndim)
      REAL data(*)
      INTEGER i1,i2,i2rev,i3,i3rev,ibit,idim,ifp1,ifp2,ip1,ip2,ip3,k1,
     *k2,n,nprev,nrem,ntot
      REAL tempi,tempr
      DOUBLE PRECISION theta,wi,wpi,wpr,wr,wtemp
      ntot=1
      do 11 idim=1,ndim
        ntot=ntot*nn(idim)
11    continue
      nprev=1
      do 18 idim=1,ndim
        n=nn(idim)
        nrem=ntot/(n*nprev)
        ip1=2*nprev
        ip2=ip1*n
        ip3=ip2*nrem
        i2rev=1
        do 14 i2=1,ip2,ip1
          if(i2.lt.i2rev)then
            do 13 i1=i2,i2+ip1-2,2
              do 12 i3=i1,ip3,ip2
                i3rev=i2rev+i3-i2
                tempr=data(i3)
                tempi=data(i3+1)
                data(i3)=data(i3rev)
                data(i3+1)=data(i3rev+1)
                data(i3rev)=tempr
                data(i3rev+1)=tempi
12            continue
13          continue
          endif
          ibit=ip2/2
1         if ((ibit.ge.ip1).and.(i2rev.gt.ibit)) then
            i2rev=i2rev-ibit
            ibit=ibit/2
          goto 1
          endif
          i2rev=i2rev+ibit
14      continue
        ifp1=ip1
2       if(ifp1.lt.ip2)then
          ifp2=2*ifp1
          theta=isign*6.28318530717959d0/(ifp2/ip1)
          wpr=-2.d0*sin(0.5d0*theta)**2
          wpi=sin(theta)
          wr=1.d0
          wi=0.d0
          do 17 i3=1,ifp1,ip1
            do 16 i1=i3,i3+ip1-2,2
              do 15 i2=i1,ip3,ifp2
                k1=i2
                k2=k1+ifp1
                tempr=sngl(wr)*data(k2)-sngl(wi)*data(k2+1)
                tempi=sngl(wr)*data(k2+1)+sngl(wi)*data(k2)
                data(k2)=data(k1)-tempr
                data(k2+1)=data(k1+1)-tempi
                data(k1)=data(k1)+tempr
                data(k1+1)=data(k1+1)+tempi
15            continue
16          continue
            wtemp=wr
            wr=wr*wpr-wi*wpi+wr
            wi=wi*wpr+wtemp*wpi+wi
17        continue
          ifp1=ifp2
        goto 2
        endif
        nprev=n*nprev
18    continue
      return
      END
C  (C) Copr. 1986-92 Numerical Recipes Software -2#,-".
C
C**************************************************************************
      SUBROUTINE VA04A(X,E,N,F,ESCALE,IPRINT,ICON,MAXIT)
C**************************************************************************
C  STANDARD FORTRAN 66 (A VERIFIED PFORT SUBROUTINE)
C      COMMON W
      DIMENSION W(40),X(1),E(1)
C	W[N*(N+3)]
      DDMAG=0.1*ESCALE
      SCER=0.05/ESCALE
      JJ=N*N+N
      JJJ=JJ+N
      K=N+1
      NFCC=1
      IND=1
      INN=1
      DO 1 I=1,N
      DO 2 J=1,N
      W(K)=0.
      IF(I-J)4,3,4
    3 W(K)=ABS(E(I))
      W(I)=ESCALE
    4 K=K+1
    2 CONTINUE
    1 CONTINUE
      ITERC=1
      ISGRAD=2
      CALL CALCFX(N,X,F)
      FKEEP=ABS(F)+ABS(F)
    5 ITONE=1
      FP=F
      SUM=0.
      IXP=JJ
      DO 6 I=1,N
      IXP=IXP+1
      W(IXP)=X(I)
    6 CONTINUE
      IDIRN=N+1
      ILINE=1
    7 DMAX=W(ILINE)
      DACC=DMAX*SCER
      DMAG=AMIN1(DDMAG,0.1*DMAX)
      DMAG=AMAX1(DMAG,20.*DACC)
      DDMAX=10.*DMAG
      GO TO (70,70,71),ITONE
   70 DL=0.
      D=DMAG
      FPREV=F
      IS=5
      FA=F
      DA=DL
    8 DD=D-DL
      DL=D
   58 K=IDIRN
      DO 9 I=1,N
      X(I)=X(I)+DD*W(K)
      K=K+1
    9 CONTINUE
      CALL CALCFX(N,X,F)
C
      NFCC=NFCC+1
      GO TO (10,11,12,13,14,96),IS
   14 IF(F-FA)15,16,24
   16 IF (ABS(D)-DMAX) 17,17,18
   17 D=D+D
      GO TO 8
   18 WRITE(6,19)
   19 FORMAT(5X,44HVA04A MAXIMUM CHANGE DOES NOT ALTER FUNCTION)
      GO TO 20
   15 FB=F
      DB=D
      GO TO 21
   24 FB=FA
      DB=DA
      FA=F
      DA=D
   21 GO TO (83,23),ISGRAD
   23 D=DB+DB-DA
      IS=1
      GO TO 8
   83 D=0.5*(DA+DB-(FA-FB)/(DA-DB))
      IS=4
      IF((DA-D)*(D-DB))25,8,8
   25 IS=1
      IF(ABS(D-DB)-DDMAX)8,8,26
   26 D=DB+SIGN(DDMAX,DB-DA)
      IS=1
      DDMAX=DDMAX+DDMAX
      DDMAG=DDMAG+DDMAG
      IF(DDMAX-DMAX)8,8,27
   27 DDMAX=DMAX
      GO TO 8
   13 IF(F-FA)28,23,23
   28 FC=FB
      DC=DB
   29 FB=F
      DB=D
      GO TO 30
   12 IF(F-FB)28,28,31
   31 FA=F
      DA=D
      GO TO 30
   11 IF(F-FB)32,10,10
   32 FA=FB
      DA=DB
      GO TO 29
   71 DL=1.
      DDMAX=5.
      FA=FP
      DA=-1.
      FB=FHOLD
      DB=0.
      D=1.
   10 FC=F
      DC=D
   30 A=(DB-DC)*(FA-FC)
      B=(DC-DA)*(FB-FC)
      IF((A+B)*(DA-DC))33,33,34
   33 FA=FB
      DA=DB
      FB=FC
      DB=DC
      GO TO 26
   34 D=0.5*(A*(DB+DC)+B*(DA+DC))/(A+B)
      DI=DB
      FI=FB
      IF(FB-FC)44,44,43
   43 DI=DC
      FI=FC
   44 GO TO (86,86,85),ITONE
   85 ITONE=2
      GO TO 45
   86 IF (ABS(D-DI)-DACC) 41,41,93
   93 IF (ABS(D-DI)-0.03*ABS(D)) 41,41,45
   45 IF ((DA-DC)*(DC-D)) 47,46,46
   46 FA=FB
      DA=DB
      FB=FC
      DB=DC
      GO TO 25
   47 IS=2
      IF ((DB-D)*(D-DC)) 48,8,8
   48 IS=3
      GO TO 8
   41 F=FI
      D=DI-DL
      DD=SQRT((DC-DB)*(DC-DA)*(DA-DB)/(A+B))
      DO 49 I=1,N
      X(I)=X(I)+D*W(IDIRN)
      W(IDIRN)=DD*W(IDIRN)
      IDIRN=IDIRN+1
   49 CONTINUE
      IF (DD.EQ.0.0) DD=1E-10
      W(ILINE)=W(ILINE)/DD
      ILINE=ILINE+1
      IF(IPRINT-1)51,50,51
   50 WRITE(6,52) ITERC,NFCC,F,(X(I),I=1,N)
   52 FORMAT (/1X,9HITERATION,I5,I15,16H FUNCTION VALUES,
     110X,3HF =,E21.14/(5E24.14))
      GO TO(51,53),IPRINT
   51 GO TO (55,38),ITONE
   55 IF (FPREV-F-SUM) 94,95,95
   95 SUM=FPREV-F
      JIL=ILINE
   94 IF (IDIRN-JJ) 7,7,84
   84 GO TO (92,72),IND
   92 FHOLD=F
      IS=6
      IXP=JJ
      DO 59 I=1,N
      IXP=IXP+1
      W(IXP)=X(I)-W(IXP)
   59 CONTINUE
      DD=1.
      GO TO 58
   96 GO TO (112,87),IND
  112 IF (FP-F) 37,37,91
   91 D=2.*(FP+F-2.*FHOLD)/(FP-F)**2
      IF (D*(FP-FHOLD-SUM)**2-SUM) 87,37,37
   87 J=JIL*N+1
      IF (J-JJ) 60,60,61
   60 DO 62 I=J,JJ
      K=I-N
      W(K)=W(I)
   62 CONTINUE
      DO 97 I=JIL,N
      W(I-1)=W(I)
   97 CONTINUE
   61 IDIRN=IDIRN-N
      ITONE=3
      K=IDIRN
      IXP=JJ
      AAA=0.
      DO 65 I=1,N
      IXP=IXP+1
      W(K)=W(IXP)
      IF (AAA-ABS(W(K)/E(I))) 66,67,67
   66 AAA=ABS(W(K)/E(I))
   67 K=K+1
   65 CONTINUE
      DDMAG=1.
      IF (AAA.EQ.0.0) AAA=1E-10
      W(N)=ESCALE/AAA
      ILINE=N
      GO TO 7
   37 IXP=JJ
      AAA=0.
      F=FHOLD
      DO 99 I=1,N
      IXP=IXP+1
      X(I)=X(I)-W(IXP)
      IF (AAA*ABS(E(I))-ABS(W(IXP))) 98,99,99
   98 AAA=ABS(W(IXP)/E(I))
   99 CONTINUE
      GO TO 72
   38 AAA=AAA*(1.+DI)
      GO TO (72,106),IND
   72 IF (IPRINT-2) 53,50,50
   53 GO TO (109,88),IND
  109 IF (AAA-0.1) 89,89,76
   89 GO TO (20,116),ICON
  116 IND=2
      GO TO (100,101),INN
  100 INN=2
      K=JJJ
      DO 102 I=1,N
      K=K+1
      W(K)=X(I)
      X(I)=X(I)+10.*E(I)
  102 CONTINUE
      FKEEP=F
      CALL CALCFX (N,X,F)
      NFCC=NFCC+1
      DDMAG=0.
      GO TO 108
   76 IF (F-FP) 35,78,78
   78 WRITE(6,80)
   80 FORMAT (5X,37HVA04A ACCURACY LIMITED BY ERRORS IN F)
      GO TO 20
   88 IND=1
   35 TMP=FP-F
      IF (TMP.GT.0.0) THEN
      DDMAG=0.4*SQRT(TMP)
      ELSE
      DDMAG=0.0
      ENDIF
      ISGRAD=1
  108 ITERC=ITERC+1
      IF (ITERC-MAXIT) 5,5,81
81    CONTINUE
C   81 WRITE(6,82) MAXIT
   82 FORMAT(I5,30H ITERATIONS COMPLETED BY VA04A)
      IF (F-FKEEP) 20,20,110
  110 F=FKEEP
      DO 111 I=1,N
      JJJ=JJJ+1
      X(I)=W(JJJ)
  111 CONTINUE
      GO TO 20
  101 JIL=1
      FP=FKEEP
      IF (F-FKEEP) 105,78,104
  104 JIL=2
      FP=F
      F=FKEEP
  105 IXP=JJ
      DO 113 I=1,N
      IXP=IXP+1
      K=IXP+N
      GO TO (114,115),JIL
  114 W(IXP)=W(K)
      GO TO 113
  115 W(IXP)=X(I)
      X(I)=W(K)
  113 CONTINUE
      JIL=2
      GO TO 92
  106 IF (AAA-0.1) 20,20,107
   20 RETURN
  107 INN=1
      GO TO 35
      END
C
C**************************************************************************
      SUBROUTINE BOXIMG(AIN,NXYZ,ABOX,JXYZ,IX,IY,MEAN,RMS)
C**************************************************************************
C
      IMPLICIT NONE
C
      INTEGER I,J,NXYZ(3),JXYZ(3),IX,IY,ID,IDB,II,JJ
      REAL AIN(*),ABOX(*),MEAN,RMS
C
      MEAN=0.0
      DO 10 J=1,JXYZ(2)
        DO 10 I=1,JXYZ(1)
      	  II=I+IX-1
      	  JJ=J+IY-1
          ID=II+NXYZ(1)*(JJ-1)
          IDB=I+JXYZ(1)*(J-1)
          ABOX(IDB)=AIN(ID)
      	  MEAN=MEAN+ABOX(IDB)
10    CONTINUE
      MEAN=MEAN/JXYZ(1)/JXYZ(2)
      RMS=0.0
      DO 20 I=1,JXYZ(1)*JXYZ(2)
      	RMS=RMS+(ABOX(I)-MEAN)**2
20    CONTINUE
      RMS=SQRT(RMS/JXYZ(1)/JXYZ(2))
C
      RETURN
      END
C
C**************************************************************************
      SUBROUTINE MSKTRN(CIN,CINS,CMSK,CMSKS,NXYZ,RMAX)
C**************************************************************************
C
      IMPLICIT NONE
C
      INTEGER I,J,K,L,NXYZ(3),ID,II,JJ,JC
      REAL RMAX,RMAX2,RES2,HW
      COMPLEX CIN(*),CMSK(*),CINS(*),CMSKS(*)
C
      WRITE(*,1000)
1000  FORMAT(/,' FILTERING POWER SPECTRUM...'/)
C
      JC=NXYZ(1)/2+1
      RMAX2=RMAX**2
      HW=-1.0/RMAX2
      DO 10 I=1,JC
        II=I-1
        DO 10 J=1,NXYZ(2)
          JJ=J-1
          IF (JJ.GT.NXYZ(2)/2) JJ=JJ-NXYZ(2)
          RES2=(REAL(II)/NXYZ(1))**2+(REAL(JJ)/NXYZ(2))**2
C          IF (RES2.LE.RMAX2) THEN
            IF (I.NE.JC) THEN
              ID=I+NXYZ(1)/2*(J-1)
      	      IF (RMAX.GT.0.0) THEN
      	 CMSK(ID)=CIN(ID)*EXP(HW*RES2)
      	      ELSE
      	 CMSK(ID)=CIN(ID)*(1.0-EXP(HW*RES2))
      	      ENDIF
            ELSE
      	      IF (RMAX.GT.0.0) THEN
      	 CMSKS(J)=CINS(J)*EXP(HW*RES2)
      	      ELSE
      	 CMSKS(J)=CINS(J)*(1.0-EXP(HW*RES2))
      	      ENDIF
            ENDIF
C          ENDIF
10    CONTINUE
C
      RETURN
      END
C
C**************************************************************************
      REAL FUNCTION CTF(CS,WL,WGH1,WGH2,DFMID1,DFMID2,ANGAST,
     +                  THETATR,IX,IY)
C**************************************************************************
C
      PARAMETER (TWOPI=6.2831853071796)
C
      RAD=IX**2+IY**2
      IF (RAD.NE.0.0) THEN
        RAD=SQRT(RAD)
        ANGLE=RAD*THETATR
        ANGSPT=ATAN2(REAL(IY),REAL(IX))
        C1=TWOPI*ANGLE*ANGLE/(2.0*WL)
        C2=-C1*CS*ANGLE*ANGLE/2.0
        ANGDIF=ANGSPT-ANGAST
        CCOS=COS(2.0*ANGDIF)
        DF=0.5*(DFMID1+DFMID2+CCOS*(DFMID1-DFMID2))
        CHI=C1*DF+C2
        CTF=-WGH1*SIN(CHI)-WGH2*COS(CHI)
      ELSE
        CTF=-WGH2
      ENDIF
C
      RETURN
      END
C
C**************************************************************************
      SUBROUTINE SEARCH_CTF(CS,WL,WGH1,WGH2,THETATR,RMIN,RMAX,
     +	      AIN,NXYZ,DFMID1,DFMID2,ANGAST,FSTEP)
C**************************************************************************
C
      IMPLICIT NONE
C
      INTEGER I,J,K,NXYZ(3),I1,I2
      REAL CS,WL,WGH1,WGH2,THETATR,DFMID1,DFMID2,ANGAST,RMIN,RMAX
      REAL RMIN2,RMAX2,SUM,AIN(*),SUMMAX,FSTEP
      REAL DFMID1S,DFMID2S,ANGASTS,HW,PI,EVALCTF
      PARAMETER (PI=3.1415926535898)
C
      WRITE(*,1000)
1000  FORMAT(/,' SEARCHING CTF PARAMETERS...'/,
     +       /,'      DFMID1      DFMID2      ANGAST          CC'/)
C
      RMIN2=RMIN**2
      RMAX2=RMAX**2
      HW=-1.0/RMAX2
      SUMMAX=-1.0E20
      I1=INT(DFMID1/FSTEP)
      I2=INT(DFMID2/FSTEP)
      DO 10 K=0,3
        DO 10 I=I1,I2
      	  DO 10 J=I1,I2
      	    DFMID1=FSTEP*I
      	    DFMID2=FSTEP*J
      	    ANGAST=22.5*K
      	    ANGAST=ANGAST/180.0*PI
      	    SUM=EVALCTF(CS,WL,WGH1,WGH2,DFMID1,DFMID2,ANGAST,
     +	  THETATR,HW,AIN,NXYZ,RMIN2,RMAX2)
      	    IF (SUM.GT.SUMMAX) THEN
      	      WRITE(*,1100)DFMID1,DFMID2,ANGAST/PI*180.0,SUM
1100	      FORMAT(3F12.2,F12.5)
      	      SUMMAX=SUM
      	      DFMID1S=DFMID1
      	      DFMID2S=DFMID2
      	      ANGASTS=ANGAST
      	    ENDIF
10    CONTINUE
C
      DFMID1=DFMID1S
      DFMID2=DFMID2S
      ANGAST=ANGASTS
C
      RETURN
      END
C
C**************************************************************************
      REAL FUNCTION EVALCTF(CS,WL,WGH1,WGH2,DFMID1,DFMID2,ANGAST,
     +	      THETATR,HW,AIN,NXYZ,RMIN2,RMAX2)
C**************************************************************************
C
      IMPLICIT NONE
C
      INTEGER L,LL,M,MM,NXYZ(3),ID
      REAL CS,WL,WGH1,WGH2,DFMID1,DFMID2,ANGAST,THETATR,SUM1
      REAL SUM,AIN(*),RES2,RMIN2,RMAX2,CTF,CTFV,HW,SUM2
C
      	    SUM=0.0
      	    SUM1=0.0
      	    SUM2=0.0
      	    DO 20 L=6,NXYZ(1)/2
      	      LL=L-1
      	      DO 20 M=6,NXYZ(2)-5
      	 MM=M-1
      	 IF (MM.GT.NXYZ(2)/2) MM=MM-NXYZ(2)
      	 RES2=(REAL(LL)/NXYZ(1))**2+(REAL(MM)/NXYZ(2))**2
      	 IF ((RES2.LE.RMAX2).AND.(RES2.GE.RMIN2)) THEN
      	   CTFV=CTF(CS,WL,WGH1,WGH2,DFMID1,DFMID2,
     +	     ANGAST,THETATR,LL,MM)
      	   ID=L+NXYZ(1)/2*(M-1)
      	   SUM=SUM+AIN(ID)*CTFV**2*EXP(HW*RES2)
      	   SUM1=SUM1+CTFV**4
      	   SUM2=SUM2+AIN(ID)**2*EXP(2.0*HW*RES2)
      	 ENDIF
20	    CONTINUE
      	    SUM=SUM/SQRT(SUM1*SUM2)
C
      EVALCTF=SUM
      RETURN
      END
C
C**************************************************************************
      SUBROUTINE REFINE_CTF(DFMID1,DFMID2,ANGAST)
C**************************************************************************
C
      IMPLICIT NONE
C
      INTEGER NCYCLS
      PARAMETER (NCYCLS=50)
      REAL DFMID1,DFMID2,ANGAST,XPAR(3),EPAR(3),RF,ESCALE,PI
      PARAMETER (PI=3.1415926535898)
      DATA EPAR/100.0,100.0,0.05/
      DATA ESCALE/100.0/
C
      WRITE(*,1000)
1000  FORMAT(/,' REFINING CTF PARAMETERS...'/,
     +       /,'      DFMID1      DFMID2      ANGAST          CC'/)
C
      XPAR(1)=DFMID1
      XPAR(2)=DFMID2
      XPAR(3)=ANGAST
      IF (XPAR(1).EQ.XPAR(2)) XPAR(1)=XPAR(1)+1.0
      CALL VA04A(XPAR,EPAR,3,RF,ESCALE,0,1,NCYCLS)
      DFMID1=XPAR(1)
      DFMID2=XPAR(2)
      ANGAST=XPAR(3)
      WRITE(*,1100)DFMID1,DFMID2,ANGAST/PI*180.0,-RF
1100  FORMAT(3F12.2,F12.5,'  Final Values')
C
      RETURN
      END
C
C******************************************************************************
C
      SUBROUTINE CALCFX(NX,XPAR,RF)
C
C     CALCULATES NEW VALUE FOR F TO INPUT TO SUBROUTINE VA04A
C
C******************************************************************************
C
      IMPLICIT NONE
C
      INTEGER NXYZ(3),NX,IXBMX
      PARAMETER (IXBMX=512)
      REAL CS,WL,WGH1,WGH2,THETATR,DFMID1,DFMID2,ANGAST
      REAL RMIN2,RMAX2,AIN(IXBMX/2*IXBMX),EVALCTF
      REAL DFMID1S,DFMID2S,ANGASTS,HW,PI,XPAR(3),RF
      PARAMETER (PI=3.1415926535898)
      COMMON/FUNC/CS,WL,WGH1,WGH2,THETATR,RMIN2,RMAX2,AIN,NXYZ,HW
C
      RF=-EVALCTF(CS,WL,WGH1,WGH2,XPAR(1),XPAR(2),XPAR(3),
     +	   THETATR,HW,AIN,NXYZ,RMIN2,RMAX2)
c      	print *,XPAR(1),XPAR(2),XPAR(3)/pi*180.0,sum
C
      RETURN
      END
