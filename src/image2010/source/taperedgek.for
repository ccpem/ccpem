C*TAPEREDGE.FOR**************************************************************
C     Simple program to taper edges of a rectangular image so that there are
C     no sharp discontinuities which make the Fourier tranform contain spikes.
C         Input original image on stream 1 (IN)
C         Output featheredged image on stream 2 (OUT)
C
C     Version  1.0      11-SEP-87    RH
C     Version  1.1      16-SEP-87    PAB
C     Version  1.2      25-JUN-88    RH    bigger value of IDEEP (taper)
C     Version  1.3       5-JUL-88    RH    check that dens<255 if MODE=0
C     Version  1.4       2-OCT-88    RH    prints out Y-dependence as plot
C     Version  1.5      14-MAR-92    RH    port to Alliant - UNIX
C     Version  1.6      14-FEB-96    JMS   bug fix - arrays went to 0
C     Version  1.7      27-FEB-96    JMS   remove redundant lines
C     Version  2.0      20-AUG-00    RH    convert to plot2000
C         "             13-JUN-01    TSH   P2K_FONT needed string terminator
C
C---------------------------------INPUT-------------------------------------
C
C	CARD 1:IAVER,ISMOOTH,ITAPER
C	CARD 2:JAVER,JSMOOTH,JTAPER
C
C       IAVER,JAVER:depths of strips parallel to x & y over which averaging
C      		    takes place.
C	ISMOOTH,JSMOOTH:for each pixel running average calculated over area
C		        defined by (-ISMOOTH to ISMOOTH) x (IAVER).(Similarly
C			for JSMOOTH)
C       ITAPER,JTAPER:depth over which tapering takes place.
C
C****************************************************************************
C
      PARAMETER (NMAX=10000)
      PARAMETER (NDEEP=200)
      COMMON//NX,NY,NZ
C*** jms	09.07.2010
c      DIMENSION ALINE(NMAX),NXYZR(3),MXYZR(3),IXYZ(3),MXYZ(3),TITLE(60)
      DIMENSION ALINE(NMAX),NXYZR(3),MXYZR(3),IXYZ(3),MXYZ(3)
      character TITLE*80
      DIMENSION ARRAY(NMAX,NDEEP),START(NMAX),FINISH(NMAX),AVEDGE(NMAX)
      DIMENSION BRRAY(NDEEP,NMAX),SRUNAV(NMAX),FRUNAV(NMAX)
c      DIMENSION STEST(NMAX),FTEST(NMAX),AVOD(NMAX)
      DIMENSION AVOD(NMAX)
      REAL*8 DTOT
      CHARACTER DAT*24
      EQUIVALENCE (NX,NXYZR),(ARRAY,BRRAY)
CTSH++
c	CHARACTER*240 TMPTITLE
c	EQUIVALENCE (TMPTITLE,TITLE)
CTSH--
C
      PRINT *,
     * ' TAPEREDGE V2.0(20.08.00) : applies smooth edge to images'
C
C     open files
C
      CALL  IMOPEN(1,'IN','RO')
      CALL  IMOPEN(2,'OUT','NEW')
      CALL  IRDHDR(1,NXYZR,MXYZR,MODE,DMIN,DMAX,DMEAN)
      IF(MODE.LT.0.OR.MODE.GT.2) THEN
      	WRITE(6,998)
998	FORMAT('  INPUT FILE IS NOT AN IMAGE, MODE.NE.0,1, or 2')
      ENDIF
      DMINO=DMIN
      DMAXO=DMAX
      DMEANO=DMEAN
      IF(NXYZR(1).GT.NMAX.OR.NXYZR(2).GT.NMAX) THEN
      	WRITE(6,999) NMAX
999	FORMAT(' ARRAY ALINE PROG DIMS TOO SMALL',I10)
      	STOP
      ENDIF
C
C     read density values on old image
C
C     	
C     read in parameters
C
      READ(5,*) IAVER,ISMOOTH,ITAPER
      READ(5,*) JAVER,JSMOOTH,JTAPER
      IF(IAVER.GT.NDEEP) GO TO 9100
      IF(JAVER.GT.NDEEP) GO TO 9102
      WRITE(6,10)IAVER,ISMOOTH,ITAPER
      WRITE(6,11)JAVER,JSMOOTH,JTAPER
   10 FORMAT(///'    IAVER=',I5/
     *          '  ISMOOTH=',I5/
     *	        '   ITAPER=',I5/)
   11 FORMAT(///'    JAVER=',I5/
     *	        '  JSMOOTH=',I5/
     *	        '   JTAPER=',I5/)
      IXYZ(1) = NX
      IXYZ(2) = NY
      IXYZ(3) = 1
C      NLINES = 0
      CALL  ICRHDR(2,IXYZ,IXYZ,MODE,TITLE,-1)
      CALL  ITRLAB(2,1)
C
C      CALL  IALSIZ(2,IXYZ,MXYZ)
      CALL  FDATE(DAT)
CTSH      ENCODE(80,50,TITLE) DAT(5:24)
CTSH++
c      WRITE(TMPTITLE,50) DAT(5:24)
CTSH--
c   50 FORMAT(' TAPEREDGE  : apply a smooth tapering to edge of image',
c     .	       5X,a)
C*** jms	09.07.2010
      write(title(1:80),'(
     * '' TAPEREDGE  : apply a smooth tapering to edge of image ''
     * ,a20)') dat(5:24)
	WRITE(6,'(A)')TITLE
C*** jms
      CALL  IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
C  First read the edge strips parallel to X.
C            : bottom edge (X=0)
C
      CALL IMPOSN(1,0,0)
      CALL IRDPAS(1,ARRAY,NMAX,NDEEP,0,NX-1,0,IAVER-1,*9000)
      DO 200 I=1,NX
      	START(I)=0.0
      	DO 210 J=1,IAVER
210   	START(I)=START(I)+ARRAY(I,J)
200   START(I)=START(I)/IAVER
C
C           : top edge (X=NX)
C
      CALL IMPOSN(1,0,0)
      CALL IRDPAS(1,ARRAY,NMAX,NDEEP,0,NX-1,NY-IAVER,NY-1,*9000)
      DO 300 I=1,NX
      	FINISH(I)=0.0
      	DO 310 J=1,IAVER
310   	FINISH(I)=FINISH(I)+ARRAY(I,J)
      FINISH(I)=FINISH(I)/IAVER
      AVEDGE(I)=(START(I)+FINISH(I))/2.0
      START(I)=START(I)-AVEDGE(I)
      FINISH(I)=FINISH(I)-AVEDGE(I)
300   CONTINUE
C
C Appply smoothing parallel to edge in the form of a running average.
      DO 350 I=1,NX
      	SRUNAV(I)=0.0
      	FRUNAV(I)=0.0
      	IPTS=0
      	DO 355 J=-ISMOOTH,ISMOOTH
      	 IN=I+J
      	 IF(IN.LE.0.OR.IN.GT.NX) GO TO 355
      	 SRUNAV(I)=SRUNAV(I)+START(IN)
      	 FRUNAV(I)=FRUNAV(I)+FINISH(IN)
      	 IPTS=IPTS+1
355   	CONTINUE
      	SRUNAV(I)=SRUNAV(I)/IPTS
350   	FRUNAV(I)=FRUNAV(I)/IPTS
351	FORMAT(//20(16F5.0/))
C
C Read in whole image and write out new intermediate image with
C    the top and bottom edges tapered.
C
      CALL IMPOSN(1,0,0)
      DO 400 IY=1,NY
      CALL IRDLIN(1,ALINE,*9000)
      CALL CALCAVOD(ALINE,NX,IY,AVOD)
      IF(IY.LE.ITAPER) THEN
      	DO 410 IX=1,NX
   	ALINE(IX)=ALINE(IX)-SRUNAV(IX)*(ITAPER+1-IY)/ITAPER
	IF(MODE.EQ.0) THEN
      	 IF(ALINE(IX).GT.255)ALINE(IX)=255.
      	 IF(ALINE(IX).LT.0)  ALINE(IX)=0.
      	ENDIF
410	CONTINUE
      ENDIF
      IF(IY.GE.NY-ITAPER) THEN
      	DO 420 IX=1,NX
   	ALINE(IX)=ALINE(IX)-FRUNAV(IX)*(IY+ITAPER-NY)/ITAPER
	IF(MODE.EQ.0) THEN
      	 IF(ALINE(IX).GT.255)ALINE(IX)=255.
      	 IF(ALINE(IX).LT.0)  ALINE(IX)=0.
      	ENDIF
420	CONTINUE
      ENDIF
400   CALL IWRLIN(2,ALINE)
      CALL IMCLOSE(1) ! Don't need input file again.
C
C  Plot out Y-dependent OD variation
      CALL YPLOT(AVOD,NY)
C
C  Second read the strips parallel to Y.
C            : left edge (Y=0)
C
      CALL IMPOSN(2,0,0)
      CALL IRDPAS(2,BRRAY,NDEEP,NMAX,0,JAVER-1,0,NY-1,*9000)
      DO 1200 I=1,NY
      	START(I)=0.0
      	DO 1210 J=1,JAVER
1210    START(I)=START(I)+BRRAY(J,I)
1200  START(I)=START(I)/JAVER
C
C           : right edge (Y=NY)
C
      CALL IMPOSN(2,0,0)
      CALL IRDPAS(2,BRRAY,NDEEP,NMAX,NX-JAVER,NX-1,0,NY-1,*9000)
      DO 1300 I=1,NY
      	FINISH(I)=0.0
      	DO 1310 J=1,JAVER
1310    FINISH(I)=FINISH(I)+BRRAY(J,I)
      FINISH(I)=FINISH(I)/JAVER
      AVEDGE(I)=(START(I)+FINISH(I))/2.0
      START(I)=START(I)-AVEDGE(I)
      FINISH(I)=FINISH(I)-AVEDGE(I)
1300  CONTINUE

C
C Appply smoothing parallel to edge in the form of a running average.
      DO 1350 I=1,NY
      	SRUNAV(I)=0.0
      	FRUNAV(I)=0.0
      	IPTS=0
      	DO 1355 J=-JSMOOTH,JSMOOTH
      	 IN=I+J
      	 IF(IN.LE.0.OR.IN.GT.NY) GO TO 1355
      	 SRUNAV(I)=SRUNAV(I)+START(IN)
      	 FRUNAV(I)=FRUNAV(I)+FINISH(IN)
      	 IPTS=IPTS+1
1355    CONTINUE
      	SRUNAV(I)=SRUNAV(I)/IPTS
1350    FRUNAV(I)=FRUNAV(I)/IPTS
C
C Read in intermediate image and overwrite with final image which should now
C    have the left and right edges also tapered.
C
      DMIN=1.0E10
      DMAX=-1.0E10
      DTOT=0.0
      CALL IMPOSN(2,0,0)
      DO 1400 IY=1,NY
      CALL IRDLIN(2,ALINE,*9000)
      	DO 1410 IX=1,JTAPER
1410     ALINE(IX)=ALINE(IX)-SRUNAV(IY)*(JTAPER+1-IX)/JTAPER
      	DO 1420 IX=NX-JTAPER,NX
1420     ALINE(IX)=ALINE(IX)-FRUNAV(IY)*(IX+JTAPER-NX)/JTAPER
      	DO 1430 IX=1,NX
      	 AL = ALINE(IX)
	IF(MODE.EQ.0) THEN
      	 IF(AL.GT.255) AL = 255.
      	 IF(AL.LT.0)   AL = 0.
      	 ALINE(IX) = AL
      	ENDIF
      	 IF(AL.GT.DMAX) DMAX = AL
      	 IF(AL.LT.DMIN) DMIN = AL
1430    DTOT = DTOT + AL
      CALL IMPOSN(2,0,IY-1)
1400  CALL IWRLIN(2,ALINE)
C
C
      NPTS=NX*NY
      DBMEAN = DTOT / NPTS
      CALL  IWRHDR(2,TITLE,-1,DMIN,DMAX,DBMEAN)
C
C     o/p density params & close files
C
      WRITE(6,90) NPTS,DMINO,DMAXO,DMEANO,DMIN,DMAX,DBMEAN
   90 FORMAT(//' number of image points =',I10//
     1 ' old min,max,mean density =',3F10.1//
     1 ' new min,max,mean density =',3F10.1)
      CALL  IMCLOSE(2)
      STOP
C
 9000 PRINT *,' end of data on reading image'
      STOP
 9100 WRITE(6,9101)IAVER,NDEEP
 9102 WRITE(6,9103)JAVER,NDEEP
 9101 FORMAT(' IAVER TOO BIG, IAVER, NDEEP=',2I5)
 9103 FORMAT(' JAVER TOO BIG, JAVER, NDEEP=',2I5)
      STOP
      END
C******************************************************************************
      SUBROUTINE CALCAVOD(ALINE,NX,IY,AVOD)
      DIMENSION ALINE(1),AVOD(1)
      SUMOD=0.0
      DO 10 I=1,NX
10    SUMOD=SUMOD+ALINE(I)
      AVOD(IY)=SUMOD/NX
      RETURN
      END
C******************************************************************************
      SUBROUTINE YPLOT(AVOD,NY)
CTSH      DIMENSION AVOD(1),TEXT(20),YA(10000),YB(10000)
CTSH++
      DIMENSION AVOD(1),YA(10000),YB(10000)
      CHARACTER*80 TEXT
CTSH--
      YMXDEV=-1000000
      YMNDEV=100000
      DO 10 I=1,NY
      IF(AVOD(I).LT.YMNDEV)YMNDEV=AVOD(I)
      IF(AVOD(I).GT.YMXDEV)YMXDEV=AVOD(I)
10    CONTINUE
C
      write(*,*)' YMNDEV=',YMNDEV
      PLTSIZ=200
      FONTSIZE=3.5
      CALL P2K_OUTFILE('YPLOT.PS',8)
      CALL P2K_HOME
      CALL P2K_FONT('Courier'//CHAR(0),FONTSIZE)
      CALL P2K_GRID(0.5*PLTSIZ,0.5*PLTSIZ,1.0)
      CALL P2K_ORIGIN(-0.5*PLTSIZ,-0.6*PLTSIZ,0.)
      CALL P2K_COLOUR(0)
      YPOSN=PLTSIZ+5.
      CALL P2K_MOVE(0.,0.,0.)
      CALL P2K_DRAW(200.,0.,0.)
      CALL P2K_DRAW(200.,150.,0.)
      CALL P2K_DRAW(0.,150.,0.)
      CALL P2K_DRAW(0.,0.,0.)
C
C
      YF1=200./(NY-1)
      RANGE=YMXDEV-YMNDEV
      YF2=150./RANGE
      NOUT=0
      DO 985 I=1,NY
      NOUT=NOUT+1
      YA(I)=(I-1)*YF1
      YB(I)=(AVOD(I)-YMNDEV)*YF2
985   CONTINUE
      CALL P2K_MOVE(YA(1),YB(1),0.)
      DO 990 I=2,NOUT
      CALL P2K_DRAW(YA(I),YB(I),0.)
990   CONTINUE
C      YZERO=-YMNDEV*YF2
C      CALL P2K_MOVE(0.,YZERO,0.)
C      CALL P2K_DRAW(200.,YZERO,0.)
C
      CALL P2K_MOVE(10.,-10.,0.)
CTSH      ENCODE(55,508,TEXT)
CTSH++
      WRITE(TEXT,508)
CTSH--
508   FORMAT(' PLOT OF AVERAGE OD VARIATION WITH Y-COORDINATE OF SCAN')
      CALL P2K_STRING(TEXT,55,0.)
      CALL P2K_MOVE(10.,-20.,0.)
CTSH      ENCODE(39,899,TEXT)YMXDEV,YMNDEV
CTSH++
      WRITE(TEXT,899)YMXDEV,YMNDEV
CTSH--
899   FORMAT(' Y VARIATION, AVMAX ',F6.2,' AVMIN ',F6.2)
      CALL P2K_STRING(TEXT,39,0.)
      CALL P2K_PAGE
      RETURN
      END
C******************************************************************************
