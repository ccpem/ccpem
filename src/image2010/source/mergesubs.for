C****MERGESUBS
C
C  SUBROUTINES USED IN MERGEDIFF PROGRAM. SUBROUTINES CONTAINED IN THIS FILE:
C
C	RDBCURVES
C	RDCURVES
C	READ_FILM
C	 ALASYM
C	 ASYM
C	  MULT
C	SCALWTWN
C	 GETITWIN
C	 CHKRANGE
C	 GETCRVVAL
C	APPLY_SCALE_B
C	TILT
C	DETTWIN	- (28.11.98) debug in situation when MASK(3) is set to zero
C	RFACTOR
C	FRIEDELR
C	SHLSRT
C	IOPRELCF
C	 S
C	 RESOLUTION
C	 TRIPLETCOR
C	  GETSLOT
C	  GETNSPOT
C	  SEARCHSPOT
C	  SUMMS
C	  CORREL
C	OUT3OR4
C       MA21 and all associated subroutines and functions.
C	MODINT
C	CRDELFS
C************************************************************************
C Read binary data curves
C  Binary curves created by ABCURVE
C  To be used in conjunction with the MERGEDIFF programs.
      SUBROUTINE RDBCURVES
      INCLUDE 'merge.inc'
      PARAMETER (IFILESZ=IARSIZ) !IARSIZ IN merge.inc
      PARAMETER (IRECL=16384)
      PARAMETER (IRECM=IRECL/40)
      DIMENSION ARRAY(IFILESZ*IRECM)
      EQUIVALENCE (JHC(1),ARRAY(1))
C
C Open a file for binary input
C
c      OPEN (UNIT=9, FILE='BCURVES', READONLY, STATUS='OLD',
c     $FORM='UNFORMATTED', BLOCKSIZE=IRECL, RECL=IRECM)
      OPEN (UNIT=9, FILE='BCURVES',  STATUS='OLD',
     $FORM='UNFORMATTED', RECL=IRECM)
C
C Read in data in binary format
C
      DO 100 IRECNO=1,IFILESZ
      READ(9) (ARRAY((IRECNO-1)*IRECM+J),J=1,IRECM)
100   CONTINUE
C
C Close file
C
      CLOSE (9)
C
      WRITE(6,105)
105   FORMAT('    Input of previously fitted curves.')
      WRITE(6,4001)KCURV,TITLE
4001  FORMAT(' Curve serial#, title:  ',I3,',  ',10A4)
      WRITE(6,4021)JREFC
4021  FORMAT(I10,'   Points from previous curves read in.')
      RETURN
      END
C****************************************************************************
      SUBROUTINE RDCURVES
      INCLUDE 'merge.inc'
C
C   INPUT OF PREVIOUSLY FITTED CURVES FOR USE IN PROPER S.F. AND TEMPF FITTING.
C
4000  WRITE(6,105)
105	FORMAT('    Input of previously fitted curves: ')
      READ(7,120)KCURV,TITLE
120	FORMAT(I10,10A4)
      WRITE(6,4001)KCURV,TITLE
4001  FORMAT(' Curve serial#, title:  ',I3,',  ',10A4)
      READ(7,*) CELL(1),CELL(2),CELL(3),CELL(4)
      DZ=1.0/CELL(4)
C   Read curves into virtual memory
      I=1
      READ(7,*,END=4500)JHC(I),JKC(I),ZCURVE(I),CURVIN(I),
     $                     CRVERR(I)
      ZCURVE(I)=ZCURVE(I)*DZ
      IH=JHC(I)
      IK=JKC(I)
      IPNTR(IH,IK,1)=I !START OF ZSTAR LINE
      IER=0
C
      DO WHILE (IER.EQ.0)
      	I=I+1
      	IF(I.GT.MAXPTS) THEN
   	  WRITE(6,4501) MAXPTS
4501  	  FORMAT(' Error - number of points from input ',
     $'curves exceeds ',I7/
     $' MAXPTS needs to be re-DIMENSIONED in program.')
      	  STOP
      	ENDIF
        READ(7,*,IOSTAT=IER,END=4500)JHC(I),JKC(I),
     $                            ZCURVE(I),CURVIN(I),CRVERR(I)
      	ZCURVE(I)=ZCURVE(I)*DZ
      	IF(JHC(I).EQ.100) THEN
          IER=1
      	  IPNTR(IH,IK,2)=I !END OF ZSTAR LINE
      	ELSE
         IF(IH.NE.JHC(I).OR.IK.NE.JKC(I)) THEN
      	 IPNTR(IH,IK,2)=I
      	 IH=JHC(I)
      	 IK=JKC(I)
      	 IPNTR(IH,IK,1)=I
      	 ENDIF
      	ENDIF
      END DO
C
4020  JREFC=I-1
      WRITE(6,4021)JREFC
4021  FORMAT(I10,'   Points from previous curves read in.')
      RETURN
4500  WRITE(6,149)I
149	FORMAT('  Error on reading curve input file, ',I5,
     $' reflections read in.')
      STOP
      END
C**********************************************************************
      SUBROUTINE READ_FILM(NO_MORE_FILMS,ISPGRP,NUMFLM,IN,JREFL,LIST,
     $RESMAXF,WSTAR,ZMIN,ZMAX,XISUMCUR,XISUMPRE,NCOMP,SCALEIN,LBISO,
     $TFPAR,TFPERP,TAXA,TANGL,TPIN,BBIN,HKREV,DETWIN,MASK,
     $NINOLD,IFILM,RMAXIN,INCYC,INLT0)
C
      LOGICAL NO_MORE_FILMS,LIST,REWOUND,FILM_FOUND,TPIN
      LOGICAL SGNXCH,ROT180,HKREV,DETWIN,LBISO
      INCLUDE 'merge.inc'
      DIMENSION TITLET(18),BBIN(1),MASK(1),MASKIN(4)
      INTEGER*2 IP1,IP2
      LOGICAL LDUMMY
      INTEGER IDUMMY
C
C     READ INPUT DATA, ONE FILM AT A TIME.
C
      WRITE(6,125)
125   FORMAT(/'*********************************************************
     1**************************************************************'//)
C
      READ(5,120)IFILM,ISUBTRACT,TITLE
120   FORMAT(I10,I1,10A4)
      IF(IFILM.LT.0) THEN
        NO_MORE_FILMS=.TRUE.
      	RETURN
      ENDIF
      IF(ISUBTRACT.EQ.0) THEN
      	WRITE(6,135) IFILM,TITLE
135   	FORMAT(' Pattern ',I10,5X,10A4)
      ELSE
      	ISUBTRACT=ISUBTRACT*10000
C        Option to subtract N*10000 in order to renumber films with identical
C         numbers - can be used for up to 9 films
      	WRITE(6,136) IFILM,TITLE,ISUBTRACT,IFILM-ISUBTRACT
136   	FORMAT(' For pattern ',I10,5X,10A4/
     .	 I10,' subtracted from film number, now',I10)
      ENDIF
C
C Need XISUMCUR upon returning, to allow proper scaling
      XISUMCUR=0.0
      XISUMPRE=0.0
      NCOMP=0
      RMAXIN=0.0
      TPIN=.FALSE.
      NUMFLM=NUMFLM+1
      WRITE(6,107)NUMFLM
107   FORMAT(I5,' <==== FILM  NUMBER IN THIS JOB -----')
C
      READ(5,*)NIN,TAXA,TANGL
      TAXB=TAXA+ABANG
      WRITE(6,155)TAXA,TAXB,TANGL
155   FORMAT('  A-STAR is ',F8.3,' degrees from the tilt axis,',
     $' B-STAR is ',F8.3,' degrees from the tilt axis',/
     $'     The tilt angle is ',F8.3,'   degrees --- as read in.')
C
      READ(5,*)SCALEIN,LBISO,TFPAR,TFPERP,
     $	 SGNXCH,ROT180,HKREV,WIN,RESIN
      IF(WIN.GT.0.0) WSTAR=WIN
      IF(RESIN.GT.0.0) RESMAXF=1.0/(RESIN*RESIN)/4.0
      WRITE(6,103) SGNXCH,ROT180,HKREV,WIN,RESIN,SCALEIN,
     $LBISO,TFPAR,TFPERP
103   FORMAT(' Control data for this film as read in:'//
     1' SGNXCH(flip about a axis)..............',L5/
     2' ROT180(rotate 180 degrees about z axis)',L5/
     3' HKREV.(reverse H and K axes)...........',L5/
     4' WIN....................................',F12.6/
     5' RESIN.(limiting resolution)............',F8.2/
     6' SCALE.(input)..........................',F10.4/
     7' LBISO.(isotropic temperature factors)..',L5/
     8' TEMPF.(input)..........................',2F10.2)
C
      READ(5,*) MASKIN(1),MASKIN(2),MASKIN(3),MASKIN(4),
     $BBIN(1),BBIN(2),BBIN(3),BBIN(4)
C
      IF(ISPGRP.EQ.13) THEN
      	ITEST=0
      	DO 10 I=1,4
      	  ITEST=ITEST+MASKIN(I)
10	CONTINUE
      	IF(ITEST.GT.0) THEN
      	  DO 20 I=1,4
      	    MASK(I)=MASKIN(I)
20	  CONTINUE
      	  WRITE(6,154) (MASK(I),I=1,4)
154	  FORMAT(' Input detwin mask: ',4I3)
C
      	  DETWIN=.TRUE.
      	  IF(MASK(2).EQ.0.AND.
     $       MASK(3).EQ.0.AND.
     $	     MASK(4).EQ.0) THEN
      	    DETWIN=.FALSE.
      	  ENDIF
      	ENDIF
C
        IF(BBIN(1).NE.0) THEN
      	  WRITE(6,156) (BBIN(I),I=1,4)
156	  FORMAT(' Input twin proportions : ',4F10.4,
     $' (overrides detwin mask)')
      	  TPIN=.TRUE.
      	  DETWIN=.FALSE.
        ENDIF
      ENDIF !SPACE GROUP = P3
C
C  THIS BIT OF MANIPULATION ADDED TO AVOID HAVING TO REDEFINE TAXA, TANGL WHEN
C  RUNNING PROGRAM USING SEVERAL DIFFERENT SETTINGS OF HKREV, ROT180, SGNXCH.
C
      IF(HKREV) THEN
      	TAXA=-TAXA-ABANG
      	TAXB=TAXA+ABANG
      	TANGL=-TANGL
      ENDIF
      IF(SGNXCH) THEN
      	TAXA=-TAXA
      	TAXB=TAXA+ABANG
      ENDIF
      IF(ROT180) THEN
      	TAXA=TAXA+180
      	TAXB=TAXA+ABANG
      ENDIF
      WRITE(6,157)TAXA,TAXB,TANGL
157   FORMAT('  New A-STAR is ',F8.3,' degrees from the tilt axis,',
     $' New B-STAR is ',F8.3,' degrees from the tilt axis',/
     $'     The tilt angle is ',F8.3,'   degrees ',
     $'--- after HKREV,ROT180,SGNXCH have been applied.')
      STAXA=ASTAR*SIN(DRAD*TAXA)
      STAXB=BSTAR*SIN(DRAD*TAXB)
      TTANGL=TAN(TANGL*DRAD)
      WRITE(6,162) RESMAXF,WSTAR
162   FORMAT(' At RESMAXF of ',F10.5,' scaling if any done on ',
     $'reflections closer than',F10.5,'  in ZSTAR '//)
C
      IF(SGNXCH) WRITE(6,164)
164   FORMAT(' Data will be flipped about the A axis')
C
      IF(HKREV) WRITE (6,1301)
1301  FORMAT (' H and K axes reversed for this film')
C
C Search through intensity list until proper film # is found
C
      IF(NINOLD.NE.NIN) THEN
      	IF(NINOLD.NE.0) THEN
      	  CLOSE(NINOLD)
      	ENDIF
c      	OPEN(UNIT=NIN, READONLY, STATUS='OLD')
      	OPEN(UNIT=NIN, STATUS='OLD')
      	NINOLD=NIN
      ENDIF
C
      FILM_FOUND=.FALSE.
      REWOUND=.FALSE.
      IER=0
      DO WHILE (IER.EQ.0.AND..NOT.FILM_FOUND)
        READ(NIN,193,IOSTAT=IER) LFILM,TITLET
193     FORMAT(I5,18A4)
      	IF(IER.EQ.-1) THEN !-1 = EOF
      	  IF(.NOT.REWOUND) THEN
      	    REWIND NIN
      	    REWOUND=.TRUE.
      	    IER=0
            WRITE(6,196) NIN
196	    FORMAT(' File on channel number',I4,' rewound.')
      	  ENDIF
      	ELSE
          WRITE(6,194) LFILM,TITLET
194       FORMAT(' On input, film #',I5,'  Title: '18A4)
          IF(LFILM.NE.IFILM) THEN
            J=0
            DO WHILE (J.NE.100)
              READ(NIN,*) J
            END DO
          ELSE
      	    FILM_FOUND=.TRUE.
      	  ENDIF
        ENDIF
      END DO
C
      IF((.NOT.FILM_FOUND).AND.REWOUND) THEN
        WRITE(6,189) IFILM,NIN
189     FORMAT(' Film # ',I5,' could not be found on unit',I5)
        STOP
      ENDIF
C
C  Have proper film list
C
        WRITE(6,195) LFILM,TITLET
195     FORMAT(' Film # ',I10,' found.  Title: ',18A4)
      	IF(ISUBTRACT.NE.0) THEN
      	 IFILM = IFILM - ISUBTRACT
      	ENDIF
        IF(LIST) WRITE(6,137)
137     FORMAT(4X,'H',4X,'K',5X,'ZSTAR',5X,'DELTA',4X,'INTENS')
      	IN=0
      	INLT0=0
      	INCYC=0
        READ(NIN,*)IH,IK,LINTIN,LDIFIN
      	DO WHILE (IH.LT.100)
      	  INCYC=INCYC+1
          IF(HKREV) THEN
            ISTORE=IH
            IH=IK
            IK=ISTORE
      	  ENDIF
          DPERP=IH*STAXA+IK*STAXB
          Z=DPERP*TTANGL
          STEST=((IH*ASTAR)**2+(IK*BSTAR)**2+2*IH*IK*ASTAR*BSTAR*
     1  COS(ABANG*DRAD)+Z**2)/4.0
          IF(STEST.LE.RESMAXF) THEN
      	    IF(STEST.GT.RMAXIN) RMAXIN=STEST
            IF(SGNXCH) THEN
              IK=-IK
              Z=-Z
            ENDIF
            IF(ROT180) THEN
              IH=-IH
              IK=-IK
            ENDIF
            IN=IN+1
      	    IF(IN.GT.MAXREFL) THEN
      	      WRITE(6,151) MAXREFL
151	      FORMAT(' Number of reflections in file greater than ',
     $I6/,' MAXREFL needs to be re-DIMENSIONED in program')
      	      STOP
      	    ENDIF
      	    IF(LINTIN.LT.0) INLT0=INLT0+1
            JIN=JREFL+IN
            IF(JIN.GT.MAXOBS) THEN
      	      WRITE(6,149) MAXOBS
149	      FORMAT(' Total number of reflections is greater than ',
     $I6/,' MAXOBS needs to be re-DIMENSIONED in program')
      	      STOP
      	    ENDIF
            IP1=1
            IP2=0
            RIIN(IN)=LINTIN
            DIN(IN)=LDIFIN
            IHIN(IN)=IH
            IKIN(IN)=IK
            ZIN(IN)=Z
C
C         ALTHOUGH THE PHASE CHANGE PART OF ASYM IS NOT USED IN THIS INTENSITY
C         MERGING PROGRAM, THE SAME ASYM SUBROUTINE AS USED BY S.D.FULLER'S
C         ORIGMERG PROGRAM IS USED HERE FOR THE SAKE OF CONSISTENCY.
C         THUS IP1 AND IP2 ARE NOT USED BY THIS PROGRAM.
C       IP1 AND IP2 GENERATE THE RELATIONSHIP BETWEEN PHASES OF REFLECTIONS
C         IN THE UNIQUE ASYMMETRIC UNIT AND THE INPUT REFLECTIONS. THE
C         REFLECTIONS FROM PREVIOUS FILMS WILL BE TRANSFORMED TO LIE IN THE
C         SAME POSITIONS AS THE INPUT REFLECTIONS AND ORIGIN REFINEMENT
C         WILL BE PERFORMED IN P1.
C
            CALL ALASYM(IH,IK,Z,IP1,IP2,LDUMMY,IDUMMY,WSTAR,ISPGRP)
C
C  The modified h,k are stored in arrays IIH,IIK
C               z is returned to ZIN
C
            IIH(IN)=IH
            IIK(IN)=IK
            ZIN(IN)=Z
C For anomalous differences Freidel switch must change sign of difference
      	    DINA(IN)=IP1*DIN(IN)
C Now set global arrays
            JH(JIN)=IH
            JK(JIN)=IK
            ZSTAR(JIN)=Z
      	    JFILM(JIN)=IFILM
C ZMIN,ZMAX set to non-zero values upon reading previously merged data list
            IF(Z.LT.ZMIN) ZMIN=Z
            IF(Z.GT.ZMAX) ZMAX=Z
C next line debugged 25.7.95
            IF(SCALEIN.LT.0.00001.AND.JREFL.NE.0) THEN
C i.e. no attempt at scaling on first film, scale set to 1.0 if no input.
              DO 240 IREF=1,JREFL
                IF(JK(JIN).EQ.JK(IREF).AND.
     $             JH(JIN).EQ.JH(IREF).AND.
     $             ABS(ZSTAR(JIN)-ZSTAR(IREF)).LE.WSTAR) THEN
C
                   NCOMP=NCOMP+1
                   XISUMPRE=XISUMPRE+RINT(IREF)
                   XISUMCUR=XISUMCUR+RIIN(IN)
      	 ENDIF
240   	      CONTINUE
            ENDIF
	  IF(LIST)WRITE(6,145)IIH(IN),IIK(IN),ZIN(IN),DIN(IN),RIIN(IN)
145       FORMAT(2I5,F10.4,2F10.1)
      	  ENDIF
      	  READ(NIN,*)IH,IK,LINTIN,LDIFIN
        END DO
C
      RES=SQRT(1.0/(4.0*RMAXIN))
      WRITE(6,166) IN,RES,INLT0
166   FORMAT(/,I6,' reflections read in, at a resolution of',
     $       F6.2,' Angstroms.'/
     $I6,' reflections with intensities less than zero.')
      WRITE(6,146) INCYC
146   FORMAT(' Number of reflections in input data list is ',I5/)
      IF(.NOT.LBISO.AND.RES.GT.5.0) THEN
      	LBISO=.TRUE.
      	WRITE(6,147)
147	FORMAT(' Resolution poorer than 5.0 A.',
     $' Isotropic temperature factor used.'/)
      ENDIF
      RETURN
      END
C*********************************************************************
      SUBROUTINE ALASYM(IH,IK,Z,IP1,IP2,LSPEC,IPTEST,WSTAR,ISPGRP)
C
C  Arms length routine to call ASYM. All matrices for manipulating
C   H,K's are in this subroutine instead of .MAIN (or other)
C
      LOGICAL LSPEC
      INTEGER*2 IP1,IP2
C
      INTEGER*2 ISPEC(5,17)
      DATA ISPEC/7*0,1,3*0,1,4*0,1,4*0,1,3*0,3*1,2*0,3*1,0,-1,3*1,0,1,
     A 3*1,4*0,1,0,0,4*1,0,5*1,8*0,1,0,1,1,5*0,1,4*0,1,1,0/
      INTEGER*2 IGO(8,17)
      DATA IGO/8*5,2*4,2*5,2*4,2*5,
     A 4,5,4,5,4,5,4,5,  4,5,4,5,4,5,4,5,  4,5,4,5,4,5,4,5,
     B 2,4,2,5,2,4,2,5,  2,4,2,5,2,4,2,5,  2,4,2,5,2,4,2,5,
     C 2,4,2,5,2,4,2,5,  3,4,3,5,3,4,3,5,  1,2,1,4,1,2,1,5,
     D 1,2,1,4,1,2,1,5,  4,5,4,5,3,5,3,5,  2,4,2,4,1,5,1,5,
     E 2,4,2,4,1,5,1,5,  3,4,3,5,1,4,1,5,  2,3,2,4,1,3,1,5/
C
C     IMAT SHOWS WHICH MATRICES WILL BE USED FROM MAT FOR EACH SPACE GROUP
C       THE FIRST ELEMENT OF EACH IS PASSED TO SET,ASYM FOR LATER USE.
C       THE SAME IS DONE FOR IGO WHICH CONTROLS PROGRAM FLOW IN SET,ASYM
C       AND FOR ISPEC WHICH INDICATES SPECIAL REFLECTIONS.
C
      INTEGER*2 IMAT(5,17)
      DATA IMAT/ 1,1,1,1,1,    1,2,1,1,1,    1,3,1,1,1,
     A           1,4,1,1,1,    1,3,1,1,1,    1,2,1,3,1,
     B           1,2,1,4,1,    1,2,1,6,1,    1,2,1,3,1,
     C           1,2,7,5,1,    1,8,1,2,3,    1,8,1,9,6,
     D           1,10,11,12,1, 1,8,1,10,11,  1,9,1,10,11,
     E           1,2,10,5,11,  1,8,9,10,11/
      INTEGER*2 MAT(8,12)
      DATA MAT/   -1,0,0,-1,-1,0,0,-1,      1,0,0,1,-1,0,0,-1,
     A            1,0,0,-1,1,0,0,-1,        1,0,0,-1,1,0,180,-1,
     B            0,1,-1,0,1,0,0,1,        1,0,0,-1,1,180,180,-1,
     C            0,-1,1,0,1,0,0,1,         0,1,1,0,1,0,0,-1,
     D            0,1,1,0,-1,0,0,1,         0,-1,1,1,-1,0,0,-1,
     E            -1,-1,1,0,1,0,0,1,        1,1,-1,0,-1,0,0,-1/
      LOGICAL LREV(17)
      DATA LREV/9*.FALSE.,.TRUE.,2*.FALSE.,.TRUE.,2*.FALSE.,.TRUE.,
     1.FALSE./
C        THESE MATRICES ARE USED BY ASYM TO TRANSFORM ALL REFLECTIONS
C           TO THE STANDARD ASYMMETRIC UNIT AND TO PICK OUT THE SPECIAL
C           REFLECTIONS.
C#####################################################################
C
C     NUMBER   SPACEGROUP    ASYMMETRIC UNIT        REAL   IMAGINARY
C
C          1          P1         H>=0
C
C          2         P21         H,Z>=0              Z=0
C
C          3         P12         H,K>=0              K=0
C
C          4        P121         H,K>=0              K=0
C
C          5         C12         H,K>=0              K=0
C
C          6        P222         H,K,Z>=0            H=0
C                                                    K=0
C                                                    Z=0
C
C          7       P2221         H,K,Z>=0          (0,2N,Z)  (0,2N+1,Z)
C                                                    (H,K,0)
C                                                    (H,0,Z)
C
C          8      P22121         H,K,Z>=0            (H,K,0)
C                                                   (2N,0,Z)  (2N+1,0,Z)
C                                                   (0,2N,Z)  (0,2N+1,Z)
C
C          9        C222         H,K,Z>=0            (H,K,0)
C                                                    (H,0,Z)
C                                                    (0,K,Z)
C
C         10          P4         H,K,Z>=0            (H,K,0)
C
C         11        P422         H,K,Z>=0            (H,K,0)
C                                K>=H                (H,0,Z)
C                                                    (0,K,Z)
C                                                    (H,H,Z)
C
C         12       P4212         H,K,Z>=0            (H,K,0)
C                                K>=H                (H,H,Z)
C                                                   (2N,0,Z)   (2N+1,0,Z)
C                                                   (0,2N,Z)   (0,2N+1,Z)
C
C         13          P3         H,K>=0
C
C         14        P312         H,K>=0              (H,H,Z)
C                                K>=H
C
C         15        P321         H,K>=0              (H,0,Z)
C                                 K>H                (0,K,Z)
C
C         16          P6       H,K,Z>=0             (H,K,0)
C
C         17        P622         H,K,Z>=0            (H,K,0)
C                                K>=H                (H,H,Z)
C
C#################################################################
C
C         ALTHOUGH THE PHASE CHANGE PART OF ASYM IS NOT USED IN THIS INTENSITY
C         MERGING PROGRAM, THE SAME ASYM SUBROUTINE AS USED BY S.D.FULLER'S
C         ORIGMERG PROGRAM IS USED HERE FOR THE SAKE OF CONSISTENCY.
C         THUS IP1 AND IP2 ARE NOT USED BY THIS PROGRAM.
C       IP1 AND IP2 GENERATE THE RELATIONSHIP BETWEEN PHASES OF REFLECTIONS
C         IN THE UNIQUE ASYMMETRIC UNIT AND THE INPUT REFLECTIONS. THE
C         REFLECTIONS FROM PREVIOUS FILMS WILL BE TRANSFORMED TO LIE IN THE
C         SAME POSITIONS AS THE INPUT REFLECTIONS AND ORIGIN REFINEMENT
C         WILL BE PERFORMED IN P1.
C       LSPEC IS TRUE A REFLECTION IS SPECIAL, HAS ITS PHASE RESTRICTED BY
C         SYMMETRY. IPTEST IS 0 IF THE REFLECTION SHOULD BE REAL AND 90
C         IF IT SHOULD BE IMAGINARY
C
      CALL ASYM(IH,IK,Z,IP1,IP2,LSPEC,IPTEST,WSTAR,
     1  MAT(1,IMAT(1,ISPGRP)),MAT(1,IMAT(2,ISPGRP)),
     2  MAT(1,IMAT(3,ISPGRP)),MAT(1,IMAT(4,ISPGRP)),
     3	MAT(1,IMAT(5,ISPGRP)),
     4  IGO(1,ISPGRP),ISPEC(1,ISPGRP),LREV(ISPGRP))
      RETURN
      END
C*************************************************************************
      SUBROUTINE ASYM(IH,IK,Z,IP1,IP2,SPEC,IPTEST,WSTAR,
     1	A1,A2,A3,A4,A5,IGO,ISPEC,LREV)
      INTEGER*2 A1(8),A2(8),A3(8),A4(8),A5(8),IGO(8),ISPEC(5)
      INTEGER*2 IP1,IP2
      LOGICAL SPEC,LREV
C
C      WRITE(6,904)A1,A2,A3,A4,A5,IH,IK,Z,IP1,IP2,SPEC,IPTEST,WSTAR
      IF(IH.LT.0) CALL MULT(A1,IH,IK,Z,IP1,IP2)
50    INDEX=1
      IF(IK.GE.0) INDEX=INDEX+1
      IF(Z.GE.0.0) INDEX=INDEX+2
      IF(IH.LT.IABS(IK)) INDEX=INDEX+4
      INDEX=IGO(INDEX)
C      WRITE(6,902) INDEX
902   FORMAT (I10)
C      WRITE(6,901) IH,IK,Z,IP1,IP2
901   FORMAT(2I5,F10.5,2I5)
      GO TO (100,150,200,250,500), INDEX
C
C     INDEX CLASSIFIES THE REFLECTION BY ITS INDICES
C     IGO INDICATES WHICH MATRIX WILL BRING THE REFLECTION
C        INTO THE UNIQUE ASYMMETRIC UNIT FOR A GIVEN INDEX
C
C    INDEX    K>=0     Z>=0   /K/>=/H/
C      1       NO       NO      NO
C      2       YES      NO      NO
C      3       NO       YES     NO
C      4       YES      YES     NO
C      5       NO       NO      YES
C      6       YES      NO      YES
C      7       NO       YES     YES
C      8       YES      YES     YES
C
C      P622 IS THE HIGHEST SYMMETRY AND ITS ASYMMETRIC UNIT IS ONLY
C         INDEX = 8
C
100    CALL MULT(A5,IH,IK,Z,IP1,IP2)
C       WRITE(6,900) A5,IH,IK,Z,IP1,IP2
900    FORMAT(8I5,5X,2I5,F10.5,2I10)
       GO TO 50
150    CALL MULT(A4,IH,IK,Z,IP1,IP2)
C       WRITE(6,900)A4,IH,IK,Z,IP1,IP2
       GO TO 50
200    CALL MULT(A3,IH,IK,Z,IP1,IP2)
C       WRITE(6,900)A3,IH,IK,Z,IP1,IP2
       GO TO 50
250    CALL MULT(A2,IH,IK,Z,IP1,IP2)
C       WRITE(6,900)A2,IH,IK,Z,IP1,IP2
C
C      THE PURPOSE OF LREV IS TO MOVE ANY 0,K POINTS TO BECOME H,0 POINTS
C       AND IS DONE ONLY FOR SPACE GROUPS P4,P3,P6 (LREV = .TRUE.) 20.3.84
C
C      AFTER REFLECTIONS HAVE BEEN PLACED INTO THE ASYMMETRIC UNIT
C       THEY ARE EXAMINED TO SEE IF THEY ARE SPECIAL REFLECTIONS,
C       ONES WHOSE PHASE MUST BE EITHER REAL (0 OR PI) OR IMAGINARY
C       (PI/2 OR 3*PI/2)
C
500    CONTINUE
       IF(IH.EQ.0 .AND. IK.LT.0) CALL MULT(A1,IH,IK,Z,IP1,IP2)
       IF(LREV .AND. IH.EQ.0) CALL MULT(A4,IH,IK,Z,IP1,IP2)
       SPEC=.FALSE.
       IPTEST=0
C      SPEC WILL BE TRUE IF THE REFLECTION IS SPECIAL .
C      IPTEST WILL BE 0 IF REAL AND 90 IF IMAGINARY
C      ISPEC INDICATES THE CONDITIONS FOR THE REFLECTIONS
C          ISPEC(1)=1  H=0 SPECIAL
C          ISPEC(2)=1  K=0 SPECIAL
C          ISPEC(3)=1  Z=0 SPECIAL
C          ISPEC(4)=1  H=K SPECIAL
C          ISPEC(5)=1  IF FOR H=0 OR K=0 K+H ODD INDICATES AN
C                       IMAGINARY VALUE FOR THE REFLECTION
C                      ALL OTHER SPECIAL REFLECTIONS ARE REAL
C
      IF(ISPEC(1).LT.1) GO TO 510
      IF(IH.EQ.0) GO TO 560
510   CONTINUE
      IF(ISPEC(2).LT.1) GO TO 520
      IF(IK.EQ.0) GO TO 560
520   CONTINUE
      IF(ISPEC(3).LT.1) GO TO 530
      IF(ABS(Z).LT.WSTAR) GO TO 570
530   CONTINUE
      IF(ISPEC(4).LT.1) GO TO 600
      IF(IH.EQ.IK) GO TO 570
      GO TO 600
560   CONTINUE
      IF(ISPEC(5).EQ.0) GO TO 570
      IF(ISPEC(5).EQ.-1) GO TO 563
      I=IH+IK
      GO TO 565
563   I=IK
565   I2=2*(I/2)
      IF(I.GT.I2) IPTEST=90
570   SPEC=.TRUE.
600    CONTINUE
C      WRITE(6,903)IH,IK,Z,IP1,IP2,SPEC,IPTEST,WSTAR
903   FORMAT(2I5,F10.5,2I10,L4,I5,F10.5)
      RETURN
      END
C***********************************************************************
      SUBROUTINE MULT(IA,IH,IK,Z,IP1,IP2)
C
C     DOES MATRIX MULTIPLICATION TO BRING REFLECTIONS INTO THE
C       ASYMMETRIC UNIT.
C
C     (H' K' Z' AMP' PHS')=(H K Z AMP PHI) <A>
C
C
C        <A> HAS FORM     IA(1)  IA(3)     0      0  IA(6)
C                         IA(2)  IA(4)     0      0  IA(7)
C                             0      0 IA(5)      0      0
C                             0      0     0      1      0
C                             0      0     0      0  IA(8)
C           FOR ALL CASES.
C
C
      INTEGER*2 IA(8),IP1,IP2
C      WRITE(6,900)IA,IH,IK,Z,IP1,IP2
      IH1=IA(1)*IH+IA(2)*IK
      IK=IA(3)*IH+IA(4)*IK
      IH=IH1
      Z=IA(5)*Z
      IP1=IA(8)*IP1
      IP2=IP2+IA(6)*IH+IA(7)*IK
C      WRITE(6,900)IA,IH,IK,Z,IP1,IP2
900   FORMAT(' IA,IH,IK,Z,IP1,IP2 ',8I5,2I5,F10.5,2I5)
      RETURN
      END
C**************************************************************************
C
      SUBROUTINE SCALWTWN(NUM_OF_REFL,SCALE,TFPAR,TFPERP,
     $	DETWIN,PROPTWIN,RMAXIN,LBISO,TAXA,TANGL,TA_REFMT,WSTAR)
      INCLUDE 'merge.inc'
C
      PARAMETER (MAXTWINS=4)
      PARAMETER (NMAXX=6)
      PARAMETER (NMAXY=4)
      PARAMETER (NMAXL=3)
      PARAMETER (NLIMIT1=7)
      PARAMETER (NLIMIT2=6)
      PARAMETER (XMAXRAT=12.0)   !MAX RATIO OF INTENTITES
      REAL SUM(NMAXX,NMAXY),SNAT(NMAXX,NMAXY),
     $SDER(NMAXX,NMAXY),X1(NMAXX,NMAXY),X2(NMAXX,NMAXY),
     $Y(NMAXX,NMAXY)
      INTEGER ITWIN(MAXTWINS),ITWERR(MAXTWINS)
      REAL*8   PROPTWIN(MAXTWINS),A(3,3),B(3),W(3,10),E
      REAL     DADZ(MAXTWINS)
      LOGICAL VALID,VALID1,VALID2,DETWIN,LBISO,TA_REFMT
C
C LBISO - LOGICAL FLAG FOR ISOTROPIC B FACTOR DETERMINATION
C
C  FIND SCALE AND TEMPERATURE FACTORS VERSUS CURVES (ISOTROPIC)
C   FORMULA: ISCALED=IRAW*EXP(2.0*B*(SINTHETA/LAMBDA)**2)*SCALE
C
C FOR ANISOTROPIC B FACTOR DETERMINATION:
C  ISCALED=IRAW*EXP(0.5*(BPAR*DSTARPAR**2+BPERP*DSTARPERP**2))
C  DSTARPAR=ASTAR*H*COS(TAXA)+BSTAR*K*COS(TAXB)
C  DSTARPERP=(ASTAR*H*SIN(TAXA)+BSTAR*K*SIN(TAXB))/COS(TANGL)
C
C STHMAX: 5A->0.01,3A->0.027, CALCULATED FROM FILM INPUT
C
      IF(RMAXIN.GT.0.0204) THEN
      	STHMAX=0.0204
      ELSE
      	STHMAX=RMAXIN
      ENDIF
      IF(DETWIN) THEN
      	MAXT=4
      	WRITE(6,3000)(PROPTWIN(I),I=1,MAXTWINS)
3000	FORMAT(' Twin proportions for this scale and temperature',
     $' factor determination: ',4F10.4)
      ELSE
      	MAXT=1
      ENDIF
C
      IF(LBISO) THEN
      	IF(RMAXIN.LE.0.01) THEN  !AT 5A USE ONLY 3 SLOTS
      	  NSLOTS=NMAXL
      	ELSE
      	  NSLOTS=NMAXX
      	ENDIF
      	LSLOTS=1
      	XNLIMIT=NLIMIT1
      ELSE
      	NSLOTS=NMAXY
      	LSLOTS=NSLOTS
      	XNLIMIT=NLIMIT2
      	DTAXA=TAXA*DRAD
      	DTAXB=(TAXA+ABANG)*DRAD
      	DTANGL=TANGL*DRAD
      	DSTHMAX=SQRT(STHMAX*4.0)
      ENDIF
C
      DO 5250 JY=1,LSLOTS
      DO 5250 JX=1,NSLOTS
      	SUM(JX,JY)=0.0
      	SNAT(JX,JY)=0.0
      	SDER(JX,JY)=0.0
5250  CONTINUE
C
      DO 5000 I=1,NUM_OF_REFL
C
C GET A VALUE (OR VALUES) FROM THE CURVES
C
      IF(DETWIN) THEN
      CALL GETITWIN(IIH(I),IIK(I),ZIN(I),VALID1,VALID2,
     $              ITWIN,ITWERR,DADZ)
      VALID=(VALID1.AND.VALID2)
      ELSE
      	CALL GETCRVVAL(IIH(I),IIK(I),ZIN(I),VALID,
     $	ITWIN,ITWERR,DADZ,TA_REFMT,WSTAR)
      ENDIF
C
C IF WE HAVE A VALID REFERENCE VALUE THEN PUT IT IN A SLOT
C
      IF(VALID) THEN
C
      	IF(LBISO) THEN
          AI=IIH(I)
          BI=IIK(I)
          SINTH=((AI*ASTAR)**2+(BI*BSTAR)**2+ZIN(I)**2 +
     1      2.*AI*BI*ASTAR*BSTAR*COS(ABANG*DRAD))/4.0
          ISLX=(SINTH*FLOAT(NSLOTS)/STHMAX)+1
      	  ISLY=1
C      	  WRITE(6,990) IIH(I),IIK(I),ZIN(I),SINTH,ISLX
C990	  FORMAT(2I4,2F10.4,I4)
C
      	ELSE      !LBISO
C
          AI=IHIN(I)
          BI=IKIN(I)
      	  DSPAR=ASTAR*AI*COS(DTAXA)+BSTAR*BI*COS(DTAXB)
      	  DSPERP=(ASTAR*AI*SIN(DTAXA)+BSTAR*BI*SIN(DTAXB))
     $           /COS(DTANGL)
      	  DSPAR2=ABS(DSPAR)
      	  DSPERP2=ABS(DSPERP)
          ISLX=(DSPAR2*FLOAT(NSLOTS)/DSTHMAX)+1
          ISLY=(DSPERP2*FLOAT(NSLOTS)/DSTHMAX)+1
C      WRITE(6,3008) AI,BI,DSPAR,DSPAR2,DSPERP,DSPERP2,ISLX,ISLY
C3008  FORMAT(2F6.0,F10.4,F10.6,F10.4,F10.6,2I4)
C
      	ENDIF      !LBISO
C
        IF((ISLX.GT.NSLOTS).OR.(ISLY.GT.LSLOTS)) THEN
C      	  WRITE(6,3004)
C3004	  FORMAT(' ***** Pigeon hole slot ERROR in SCALWTWN')
      	  GOTO 5000
      	ENDIF
        SUM(ISLX,ISLY)=SUM(ISLX,ISLY)+1.0
        DO 200 J=1,MAXT    !MAX OF 4 TWIN TYPES
          SNAT(ISLX,ISLY)=SNAT(ISLX,ISLY)+PROPTWIN(J)*ITWIN(J)
200     CONTINUE
        SDER(ISLX,ISLY)=SDER(ISLX,ISLY)+RIIN(I)
      ENDIF     !VALID
5000  CONTINUE
C
      S=0.0
      SY=0.0
      SX1=0.0
      SX2=0.0
      SX1X2=0.0
      SX1X1=0.0
      SX2X2=0.0
      SX1Y=0.0
      SX2Y=0.0
C
      WRITE(6,5263)
5263  FORMAT(' Intensity ratios in groups'/
     2' ((SINTH/LAMBDA)**2)/4.0   SDER      SNAT      RATIO      N')
C
      DO 5269 JY=1,LSLOTS
      DO 5269 JX=1,NSLOTS
      	IF(SNAT(JX,JY).LE.0.0) GO TO 5269
      	IF(SUM(JX,JY).LE.XNLIMIT) GO TO 5269
      	R=SDER(JX,JY)/SNAT(JX,JY)
      	IF((R.LE.0.0).OR.(R.GE.XMAXRAT)) GOTO 5269
      	Y(JX,JY)=ALOG(R)
      	IF(LBISO) THEN
      	  X1(JX,JY)=(JX-0.5)*STHMAX/FLOAT(NSLOTS)
      	  WRITE(6,5264) X1(JX,JY),SDER(JX,JY),SNAT(JX,JY),R,SUM(JX,JY)
5264  	  FORMAT(F12.4,11X,2F10.0,F10.3,3X,F5.0,2I5)
      	ELSE
C  Convert to x's in same form as normal sinth
      	  X1(JX,JY)=((JX-0.5)*DSTHMAX/FLOAT(NSLOTS))**2/4.0
      	  X2(JX,JY)=((JY-0.5)*DSTHMAX/FLOAT(LSLOTS))**2/4.0
      	  X=SQRT(X1(JX,JY)*X1(JX,JY)+X2(JX,JY)*X2(JX,JY))
      	  WRITE(6,5264) X,SDER(JX,JY),SNAT(JX,JY),R,
     $	 SUM(JX,JY),JX,JY
      	  SX2=SX2+X2(JX,JY)
      	  SX1X2=SX1X2+X1(JX,JY)*X2(JX,JY)
      	  SX2X2=SX2X2+X2(JX,JY)*X2(JX,JY)
      	  SX2Y=SX2Y+X2(JX,JY)*Y(JX,JY)
      	ENDIF
C
      	S=S+1.0
      	SY=SY+Y(JX,JY)
      	SX1=SX1+X1(JX,JY)
      	SX1X1=SX1X1+X1(JX,JY)*X1(JX,JY)
      	SX1Y=SX1Y+X1(JX,JY)*Y(JX,JY)
5269  CONTINUE
C
      IF(S.EQ.0.0) THEN
      	WRITE(6,5270)
5270  	FORMAT(' No common points in scaling, S=0.0')
      	STOP
      ENDIF
C
C USE MA21 TO SOLVE LINEAR EQUATIONS FOR LEAST SQUARES FITTING
C
      IA=3
      IF(LBISO) THEN
      	N=2
C      	WRITE(6,3009) S,SX1X1,SX1,SX1Y,SY
C3009	FORMAT(4F12.4)
      	A(1,1)=SX1X1
      	A(1,2)=SX1
      	A(2,1)=SX1
      	A(2,2)=S
      	B(1)=SX1Y
      	B(2)=SY
      ELSE
      	N=3
      	A(1,1)=SX1X1
      	A(1,2)=SX1X2
      	A(1,3)=SX1
      	A(2,1)=SX1X2
      	A(2,2)=SX2X2
      	A(2,3)=SX2
      	A(3,1)=SX1
      	A(3,2)=SX2
      	A(3,3)=S
      	B(1)=SX1Y
      	B(2)=SX2Y
      	B(3)=SY
      ENDIF
C
      E=-1.0
      CALL MA21AD(A,IA,N,B,W,E)
      IF(E.NE.0.0) THEN
      	WRITE(6,3001) E
3001	FORMAT(' MA21AD call failed - called from SCALWTWN, E=',F6.2)
      	STOP
      ENDIF
C
C SLOPES, INTERCEPTS RETURNED IN ARRAY B
C
C      WRITE(6,3009) B(1),B(2)
      TFPAR=-B(1)/2.0
      IF(LBISO) THEN
      	SCALE=1.0/EXP(B(2))
      	WRITE(6,3002) SCALE,TFPAR
3002	FORMAT(' Scale factor=',F10.5,'  Temperature factor=',F10.4/)
      ELSE
      	SCALE=1.0/EXP(B(3))
      	TFPERP=-B(2)/2.0
      	WRITE(6,3003) SCALE,TFPAR,TFPERP
3003	FORMAT(' Scale factor=',F10.5,
     $'  Anisotropic temperature factors (par/perp to TA)=',2F10.4/)
      ENDIF
C
      RETURN
      END
C ***********************************************************************
C
      SUBROUTINE GETITWIN(IH,IK,ZSTARG,VALID1,VALID2,ITWIN,ITWERR,DADZ)
      DIMENSION ITWIN(1),ITWERR(1),DADZ(1)
      LOGICAL VALID1,VALID2,IVALID
      INCLUDE 'merge.inc'
C
      VALID1=.FALSE.
      VALID2=.FALSE.
      CALL CHKRANGE(IH,IK,ZSTARG,IVALID,
     $     IZPOS1,IZPOS2,DELZP,IZNEG1,IZNEG2,DELZN)
      IF(.NOT.IVALID) RETURN   !NOT TODAY
      	ITWIN(1)=CURVIN(IZPOS1)*(1.0-DELZP)+CURVIN(IZPOS2)*DELZP
      	ITWIN(2)=CURVIN(IZNEG1)*(1.0-DELZN)+CURVIN(IZNEG2)*DELZN
      	ITWERR(1)=CRVERR(IZPOS1)*(1.0-DELZP)+CRVERR(IZPOS2)*DELZP
      	ITWERR(2)=CRVERR(IZNEG1)*(1.0-DELZN)+CRVERR(IZNEG2)*DELZN
      	DADZ(1)=(CURVIN(IZPOS2)-CURVIN(IZPOS1))/DZ
      	DADZ(2)=(CURVIN(IZNEG2)-CURVIN(IZNEG1))/DZ
      	VALID1=.TRUE.
C For K=0 then do this
C  Note that this will be correct for space group p3.
C  This may have to be changed at a later date.
      IF(IK.EQ.0) THEN
      	ITWIN(3)=ITWIN(2)
      	ITWIN(4)=ITWIN(1)
      	ITWERR(3)=ITWERR(2)
      	ITWERR(4)=ITWERR(1)
      	DADZ(3)=DADZ(2)
      	DADZ(4)=DADZ(1)
      ELSE
      CALL CHKRANGE(IK,IH,ZSTARG,IVALID,
     $     JZPOS1,JZPOS2,DELYP,JZNEG1,JZNEG2,DELYN)
      IF(.NOT.IVALID) RETURN
      	ITWIN(3)=CURVIN(JZPOS1)*(1.0-DELYP)+CURVIN(JZPOS2)*DELYP
      	ITWIN(4)=CURVIN(JZNEG1)*(1.0-DELYN)+CURVIN(JZNEG2)*DELYN
      	ITWERR(3)=CRVERR(JZPOS1)*(1.0-DELYP)+CRVERR(JZPOS2)*DELYP
      	ITWERR(4)=CRVERR(JZNEG1)*(1.0-DELYN)+CRVERR(JZNEG2)*DELYN
      	DADZ(3)=(CURVIN(IZPOS2)-CURVIN(IZPOS1))/DZ
      	DADZ(4)=(CURVIN(IZNEG2)-CURVIN(IZNEG1))/DZ
      ENDIF
      	VALID2=.TRUE.
      RETURN
      END
C*********************************************************************
C
      SUBROUTINE CHKRANGE(IH,IK,ZSTARG,VALID,
     $	    IZPOS1,IZPOS2,DELZP,
     $	    IZNEG1,IZNEG2,DELZN)
C
C USE POINTER ARRAY TO FIND APPROPRIATE ZSTAR VALUE FROM CURVE.
C  POINTER ARRAY SET UP SUCH THAT H,K MATRIX CONTAINS INDEX OFFSETS
C  INTO CURVE VECTOR.  SINCE NOT ALL H,K'S EXIST, MATRIX MAY NOT BE
C  FULLY POPULATED.  THE INTERVAL BETWEEN SUCCESSIVE DATA POINTS ALONG
C  THE ZSTAR DATA CURVE IS ASSUMED TO BE THE PARAMETER DZ
C
      INCLUDE 'merge.inc'
      LOGICAL VALID
      VALID=.FALSE.
      ICURRENT=IPNTR(IH,IK,1)  !STARTING INDEX
      IF(ICURRENT.EQ.0) RETURN  !NO CURVE FOR THIS H,K
      INEXT=IPNTR(IH,IK,2)  !ENDING INDEX+1
      ZSTART=ZCURVE(ICURRENT)  !GET VALUE FROM CURVE
C
      ZPOS=(ZSTARG-ZSTART)/DZ+ICURRENT
C      WRITE(6,1001)ICURRENT,INEXT,ZSTARG,ZSTART,ZPOS
C1001	FORMAT(' IN CHKRANGE ',2I5,3F10.4)
      IZPOS1=ZPOS
      IZPOS2=IZPOS1+1
      IF(IZPOS1.LT.ICURRENT.OR.IZPOS2.GE.INEXT)RETURN
      ZNEG=(-ZSTARG-ZSTART)/DZ+ICURRENT
      IZNEG1=ZNEG
      IZNEG2=IZNEG1+1
      IF(IZNEG1.LT.ICURRENT.OR.IZNEG2.GE.INEXT)RETURN
      DELZP=ZPOS-IZPOS1
      DELZN=ZNEG-IZNEG1
      VALID=.TRUE.
      RETURN
      END
C*************************************************************************
C
      SUBROUTINE GETCRVVAL(IH,IK,ZSTARG,VALID,ICURVVAL,
     $ICRVERR,GRAD,TA_REFMT,WSTAR)
C
C USE POINTER ARRAY TO FIND APPROPRIATE ZSTAR VALUE FROM CURVE.
C  POINTER ARRAY SET UP SUCH THAT H,K MATRIX CONTAINS INDEX OFFSETS
C  INTO CURVE VECTOR.  SINCE NOT ALL H,K'S EXIST, MATRIX MAY NOT BE
C  FULLY POPULATED.  THE INTERVAL BETWEEN SUCCESSIVE DATA POINTS ALONG
C  THE ZSTAR DATA CURVE IS ASSUMED TO BE THE PARAMETER DZ
C
      INCLUDE 'merge.inc'
      DIMENSION GRAD(4)
      LOGICAL VALID,TA_REFMT
      VALID=.FALSE.
      ICURRENT=IPNTR(IH,IK,1)  !STARTING INDEX
      IF(ICURRENT.EQ.0) RETURN  !NO CURVE FOR THIS H,K
      INEXT=IPNTR(IH,IK,2)  !ENDING INDEX+1
      ZSTART=ZCURVE(ICURRENT)  !GET VALUE FROM CURVE
C
      ZPOS=(ZSTARG-ZSTART)/DZ+ICURRENT
      IZPOS1=ZPOS
      IZPOS2=IZPOS1+1
C
C SLIGHT COMPLICATION.
C  TO GET R-FACTOR FROM UNTILTED DATA NEED CURVES.  BUT HAVE ONLY
C  ONE VALUE SO GRAD AND MORE ACCURATE VALUE CANNOT BE CALCULATED.
C  THUS HAVE TO CHECK FOR WSTAR EXCURSION LIMIT. ALSO TA REFINEMENTS
C  CANNOT BE DONE SINCE IT DEPENDS ON GRAD
C
      IF(IZPOS1.LT.ICURRENT) RETURN   !HAVE NO VALUE
      IF(IZPOS2.GE.INEXT) THEN
      	IF(TA_REFMT) RETURN    !CAN'T DO THIS
      	IF(ABS(ZSTARG-ZCURVE(IZPOS1)).GT.WSTAR) RETURN !ZSTAR CUTOFF
      	ICURVVAL=CURVIN(IZPOS1)
      ELSE
      	DELZP=ZPOS-IZPOS1
      	ICURVVAL=CURVIN(IZPOS1)*(1.0-DELZP)+CURVIN(IZPOS2)*DELZP
      	ICRVERR=CRVERR(IZPOS1)*(1.0-DELZP)+CRVERR(IZPOS2)*DELZP
      	GRAD(1)=(CURVIN(IZPOS2)-CURVIN(IZPOS1))/DZ
      ENDIF
      VALID=.TRUE.
      RETURN
      END
C****************************************************************************
      SUBROUTINE APPLY_SCALE_B(NREFLFLM,NREFLTOT,SCALE,
     $	   LBISO,TFPAR,TFPERP,LIST,TAXA,TANGL)
C
C  NREFLFLM - NUMBER OF LOCAL REFLECTIONS ON CURRENT FILM
C  NREFLTOT - NUMBER OF TOTAL REFLECTIONS PROCESSED SO FAR
C  LIST     - FLAG FOR LISTING SCALED INTENSITIES, DELTAS
C
      INCLUDE 'merge.inc'
      LOGICAL LIST,LBISO
C
      DTAXA=TAXA*DRAD
      DTAXB=(TAXA+ABANG)*DRAD
      DTANGL=TANGL*DRAD
      IF(LIST) WRITE(6,137)
137   FORMAT(4X,'H',4X,'K',5X,'ZSTAR',5X,'DELTA',4X,'INTENS',5X,
     1'ZCURVE',5X,'CURVE AMP',5X,'DELT F')
C
      DO 410  I=NREFLTOT+1,NREFLTOT+NREFLFLM
      JLINDEX=I-NREFLTOT  !LOCAL INDEX
      JOUT(I)=I
      IF(LBISO) THEN
      	AI=JH(I)
      	BI=JK(I)
C
C  HERE SINTH IS (SINTHETA/LAMBDA)**2 OR (1/2*D)**2 AS IN NORMAL EXPRESSION.
C
      	SINTH=((AI*ASTAR)**2+(BI*BSTAR)**2+ZSTAR(I)**2 +
     $  2.*AI*BI*ASTAR*BSTAR*COS(ABANG*DRAD))/4.0
      	ARG=TFPAR*SINTH*2.0
      ELSE
C
C  BUT FOR ANISOTROPIC TEMPERATURE FACTORS...
C
      	AI=IHIN(JLINDEX)
      	BI=IKIN(JLINDEX)
      	DSPAR=ASTAR*AI*COS(DTAXA)+BSTAR*BI*COS(DTAXB)
      	DSPERP=(ASTAR*AI*SIN(DTAXA)+
     $	 BSTAR*BI*SIN(DTAXB))/COS(DTANGL)
      	DSPAR2=DSPAR**2/4.0
      	DSPERP2=DSPERP**2/4.0
      	ARG=(TFPAR*DSPAR2+TFPERP*DSPERP2)*2.0
      ENDIF
      RINT(I)=RIIN(JLINDEX)*SCALE*EXP(ARG)
      DIFF(I)=DINA(JLINDEX)*SCALE*EXP(ARG)
      IF(RINT(I).GT.AMAX) AMAX=RINT(I)
C THE PREVIOUS 1 STATEMENT COULD BE DELETED
C
      IF(LIST) WRITE(6,145)JH(I),JK(I),ZSTAR(I),DIFF(I),RINT(I)
145   FORMAT(2I5,F10.4,2F10.1)
410   CONTINUE
      RETURN
      END
C***********************************************************************
      SUBROUTINE TILT(IN,JREFL,TAXA,TANGL,IFINSH,DETWIN,BB,
     $	  ISPGRP,R,WSTAR)
C
      INCLUDE 'merge.inc'
      REAL*8 A(2,2),B(2),W(30),E,BB(1)
      DIMENSION ITWIN(4),ITWERR(4),DADZCU(4)
C              THESE BELOW ARE JUST DUMMY VARIABLES FOR ASYM.
      INTEGER*2 IP1,IP2
      LOGICAL SPEC,VALID,IFINSH,VAL1,VAL2,DETWIN
      WSTAR=0.01
C
      IF(DETWIN) THEN
      	WRITE(6,3005)(BB(I),I=1,4)
3005	FORMAT(' Twin proportions for tilt axis/angle refinement:',
     $4F10.4)
      ENDIF
C      WRITE(6,3004)ISPGRP
C3004  FORMAT(' IN TILT ISPGRP IS ',I5)
      SHIFT=0.5
      IEND=0
      NCYCL=5
      DO 8300 ICYCL=1,NCYCL
      DO 8050 I=1,2
      B(I)=0.
      DO 8050 J=1,2
8050  A(I,J)=0.
      TAXB=TAXA+ABANG
C
      ZH=ASTAR*TAN(TANGL*DRAD)*SIN(TAXA*DRAD)
      ZK=BSTAR*TAN(TANGL*DRAD)*SIN(TAXB*DRAD)
      DZTHEH=ASTAR*SIN(TAXA*DRAD)/(COS(TANGL*DRAD))**2
      DZTHEK=BSTAR*SIN(TAXB*DRAD)/(COS(TANGL*DRAD))**2
      DZPHIH=ASTAR*TAN(TANGL*DRAD)*COS(TAXA*DRAD)
      DZPHIK=BSTAR*TAN(TANGL*DRAD)*COS(TAXB*DRAD)
C
      FUNCL=0.0
      INUMPTS=0
      ANUM=0.
      ADENOM=0.
      DO 8100 I=1,IN
C  FIRST CALCULATE H,K,Z IN CORRECT ASYMMETRIC UNIT.
      IH=IHIN(I)
      IK=IKIN(I)
      Z=ZH*IH+ZK*IK
      DZTHE=DZTHEH*IH+DZTHEK*IK
      DZPHI=DZPHIH*IH+DZPHIK*IK
      ZASYM=Z
C      WRITE(6,8700)IH,IK,Z,IP1,IP2
C8700  FORMAT(2I5,F10.5,2I5/7(8I5/))
      IP1=1
      IP2=0
      CALL ALASYM(IH,IK,ZASYM,IP1,IP2,SPEC,IPTEST,WSTAR,ISPGRP)
C
C  THEN FIND APPROPRIATE POINT IN CURVE LIST.
C  NOW CALCULATE CURVE INTENSITY AND GRADIENT.
      IF(DETWIN) THEN
        CALL GETITWIN(IH,IK,ZASYM,VAL1,VAL2,ITWIN,ITWERR,DADZCU)
        VALID=VAL1.AND.VAL2
      ELSE
        CALL GETCRVVAL(IH,IK,ZASYM,VALID,ITWIN,ITWERR,
     $	DADZCU,.TRUE.,WSTAR)
      ENDIF
      IF(VALID) THEN
      	INUMPTS=INUMPTS+1
        AREF=BB(1)*ITWIN(1)+BB(2)*ITWIN(2)+
     $       BB(3)*ITWIN(3)+BB(4)*ITWIN(4)
        DADZ=BB(1)*DADZCU(1)-BB(2)*DADZCU(2)+
     $       BB(3)*DADZCU(3)-BB(4)*DADZCU(4)
        IF(ZASYM.NE.-Z) GO TO 8234
        DADZ=-DADZ
        GO TO 8235
8234    IF(ZASYM.EQ.Z) GO TO 8235
        WRITE(6,8236)Z,ZASYM
8236    FORMAT(' Error at #8236 in TILT ',2F10.6)
        STOP
8235    CONTINUE
8230    CONTINUE
C
        WEIGHT=1.0
C  COULD BE CHANGED TO 1/SIGMA**2 LATER.
        JIN=JREFL+I
        ZIN(I)=ZASYM
        ZSTAR(JIN)=ZASYM
        AA0=AREF-RINT(JIN)
        AA1=DADZ*DZTHE
        AA2=DADZ*DZPHI
        A(1,1)=A(1,1)+WEIGHT*AA1*AA1
        A(1,2)=A(1,2)+WEIGHT*AA1*AA2
        A(2,1)=A(2,1)+WEIGHT*AA2*AA1
        A(2,2)=A(2,2)+WEIGHT*AA2*AA2
        B(1)  =B(1)  -WEIGHT*AA0*AA1
        B(2)  =B(2)  -WEIGHT*AA0*AA2
      	FUNCL=FUNCL+AA0*AA0  !MINIMIZING THIS FUNCTION
        ANUM  = ANUM+ABS(AA0)
        ADENOM= ADENOM+AREF
C       WRITE(6,8800)IH,IK,Z,AREF,RINT(JIN),AA0,AA1,AA2,DADZ
C8800    FORMAT(2I5,F8.5,2F8.1,F10.1,2F12.5,F12.1)
      ENDIF
8100  CONTINUE
      IF(IEND.EQ.1)GOTO 9999
C      WRITE(6,3003)((A(I,J),I=1,2),J=1,2)
C3003  FORMAT(' As are:',2(2G18.4/))
      IA=2
      N=2
      E=-1.0
      CALL MA21AD(A,IA,N,B,W,E)
      IF(E.EQ.0.0) GO TO 8150
      WRITE(6,8101)E
8101  FORMAT(' MA21AD called from TILT failed, E =',F10.5)
      STOP
8150  THETA=SHIFT*B(1)/DRAD
      PHI=SHIFT*B(2)/DRAD
      IF(ABS(PHI).GT.15.0) WRITE(6,8151)PHI,THETA
      IF(ABS(THETA).GT.10.0) WRITE(6,8151)PHI,THETA
8151  FORMAT(' SHIFTS CALCULATED TO BE TOO LARGE',2F12.2)
      IF(ABS(THETA).GT.10.0) THETA=SIGN(10.0,THETA)
      IF(ABS(PHI).GT.15.0) PHI=SIGN(15.0,PHI)
C      WRITE(6,3000)ANUM,ADENOM
C3000  FORMAT(' ANUM,ADENOM=',2F12.2)
      R=ANUM/ADENOM
      TAXA=TAXA+PHI
      TANGL=TANGL+THETA
      WRITE(6,8152)ICYCL,TAXA,TANGL,PHI,THETA,R,FUNCL
8152  FORMAT(' Cycle #',I3,':',
     1'  new TAXA,TANGL=',2F8.3,
     2' shifts=',2F9.4,'  R-factor=',F10.5,' FUNCL=',G18.6)
C
C      STOP
C
      IF((ABS(THETA).LT.0.1).AND.(ABS(PHI).LT.0.1)) IEND=1
      IF((ICYCL.EQ.1).AND.(IEND.EQ.1))IFINSH=.TRUE.
8300  CONTINUE
9999  WRITE(6,8153)INUMPTS
8153  FORMAT( ' Number of points used for tilt axis/angle refinement',
     $' was ',I6/)
      RETURN
      END
C***********************************************************************
      SUBROUTINE DETTWIN(NREFL,JREFL,BB,MASK,DETWIN,TPIN)
C DETERMINE PROPORTION OF EACH TWIN FORM CONTRIBUTING
      INCLUDE 'merge.inc'
      PARAMETER (MXTW=4)
c      LOGICAL VALID,VALID1,VALID2,DETWIN,TPIN
      LOGICAL VALID1,VALID2,DETWIN,TPIN
      DIMENSION ITWIN(MXTW),ITWERR(MXTW),DADZ(MXTW),MASK(MXTW)
c      REAL*8  WKSPCE(MXTW),AASTOR(MXTW,MXTW),E,BB(MXTW),
      REAL*8  WKSPCE(MXTW*9),E,BB(MXTW),
     $AA(MXTW,MXTW),SUMB,BTEMP(MXTW)
C
      IF(TPIN) RETURN
C
C FIRST, CHECK THAT MASK(2-4) ARE NOT ZERO
C
      MASKS=MASK(2)+MASK(3)+MASK(4)
      IF(MASKS.EQ.0) THEN
      	DETWIN=.FALSE.
      	BB(1)=1.0
      	DO I=2,MXTW
      	  BB(I)=0.0
      	END DO
        RETURN
      ENDIF
C
C	INITIALIZE MATRIX VARIABLES
C

      DO 10 I=1,MXTW
      DO 20 J=1,MXTW
20	AA(I,J)=0.0
      	BTEMP(I)=0.0
        BB(I)=0.0
      	WKSPCE(I)=0.0
10    CONTINUE
C
      NN=MXTW
      IAA=MXTW
      E=-1.0
C
      DO 30 I=1,NREFL
      CALL GETITWIN(IIH(I),IIK(I),ZIN(I),VALID1,VALID2,
     $              ITWIN,ITWERR,DADZ)
      IF(VALID1.AND.VALID2) THEN
      	DO 100 II=1,MXTW
      	  DO 110 JJ=1,MXTW
      	 AA(II,JJ)=AA(II,JJ)+ITWIN(II)*ITWIN(JJ)
110       CONTINUE
C Use scaled amplitude for twin proportion determinatiom
      	BTEMP(II)=BTEMP(II)+ITWIN(II)*RINT(I+JREFL)
C      WRITE(6,3000)(AA(II,IJ),IJ=1,4),BTEMP(II)
C3000  FORMAT(' 4AA,BTEMP ',4G18.2,4X,G18.3)
100	CONTINUE
      ENDIF
30    CONTINUE
C
C      WRITE(6,3020)
C3020  FORMAT(' BEFORE COMPRESSION')
C      DO 600 II=1,MXTW
C      WRITE(6,3001)(AA(IJ,II),IJ=1,MXTW),BTEMP(II)
C3001  FORMAT(' @600 AA,BTEMP ' 5G18.4)
C600    CONTINUE
C
C Use mask to determine which twin types to allow
C
C  Now determine total number of twins
      MTWIN=0
      DO 40 I=1,MXTW
      IF(MASK(I).NE.0) THEN
      	MTWIN=MTWIN+1
      ENDIF
40    CONTINUE
C
      IPOS=0
      DO 50 II=1,MXTW-1  !DO NOTHING IF MASK(4)=0
      IF(MASK(II).EQ.0) THEN
      	DO 55 I=1,MXTW
        DO 55 J=II-IPOS,MXTW-1-IPOS
      	  AA(I,J)=AA(I,J+1)
55	CONTINUE
      	IPOS=IPOS+1  !if MASK(2) and MASK(3) are zero
      ENDIF
50    CONTINUE
C
C      WRITE(6,3021)
C3021  FORMAT(' AFTER Y COMPRESSION')
C      DO 650 II=1,MTWIN
C      WRITE(6,3003)(AA(IJ,II),IJ=1,MXTW),BTEMP(II)
C3003  FORMAT(/' @650 AA,BTEMP ' 5G18.4)
C650    CONTINUE
C
      IPOS=0
      DO 60 II=1,MXTW-1  !DO NOTHING IF MASK(4)=0
      IF(MASK(II).EQ.0) THEN
        DO 67 I=II-IPOS,MXTW-1-IPOS
      	DO 65 J=1,MTWIN
          AA(I,J)=AA(I+1,J)
65	CONTINUE
        BTEMP(I)=BTEMP(I+1)
67	CONTINUE
      	IPOS=IPOS+1             !if MASK(2) and MASK(3) are zero
      ENDIF
60    CONTINUE
C
      NN=MTWIN
C
C      WRITE(6,3022)
C3022  FORMAT(' AFTER X COMPRESSION')
C      DO 700 II=1,MTWIN
C      WRITE(6,3002)(AA(IJ,II),IJ=1,MTWIN),BTEMP(II)
C3002  FORMAT(/' @700 AA,BTEMP ' 5G18.4)
C700    CONTINUE
C
C      IF(ABS(TLTANG).LT.0.005) RETURN
      CALL MA21AD(AA,IAA,NN,BTEMP,WKSPCE,E) !result in BTEMP
      IF(E.NE.0.0) THEN
      	WRITE (6,900)E
900	FORMAT(' In DETTWIN, matrix subroutine MA21AD failed,',
     $' ERROR = ',F10.5)
      	STOP
      ENDIF
C
C      WRITE(6,3023)
C3023  FORMAT(' AFTER MA21AD')
C      DO 910 I=1,MTWIN
C      WRITE(6,92)I,BTEMP(I)
C92    FORMAT(' TWIN PROPS: ',I5,F10.3)
C910   CONTINUE
C
C  Now recreate proper BB
C
      II=1
      DO 70 I=1,MXTW
      IF(MASK(I).NE.0) THEN
      	BB(I)=BTEMP(II)
      	II=II+1
      ENDIF
70    CONTINUE
C
C Write out unscaled twin proportions
C
      WRITE(6,26) (BB(J),J=1,MXTW)
26	FORMAT(' Redetermined twin contributions (unscaled) : ',4F10.4)
C
C  Rescale BB, normalize to total=1.0
C
      SUMB=0.0
      DO 80 I=1,MXTW
      SUMB=SUMB+BB(I)
80    CONTINUE
C
      DO 90 I=1,MXTW
      BB(I)=BB(I)/SUMB
C      WRITE(6,92)I,BB(I)
C92    FORMAT(' TWIN PROPS: ',I5,F10.3)
90    CONTINUE
C
      RETURN
      END
C************************************************************************
      SUBROUTINE RFACTOR(NREFL,JREFL,DETWIN,TPIN,BB,
     $	  RFAC,TA_REFMT,WSTAR)
C DETERMINE R FACTOR FOR MAXIMUM OF MTWIN POSSIBLE TWINS
      PARAMETER (MAXTWIN=4)
      LOGICAL VALID,VALID1,VALID2,DETWIN,TPIN,TA_REFMT
      DIMENSION ITWIN(MAXTWIN),ITWERR(MAXTWIN),DADZ(MAXTWIN)
      REAL*8  BB(MAXTWIN)
      INCLUDE 'merge.inc'
C
      FUNCL=0.0
      SUM=0.0
      DIFSUM=0.0
      ICOUNT=0
C
      DO 10 I=1,NREFL
      	IF(DETWIN.OR.TPIN) THEN
      	  CALL GETITWIN(IIH(I),IIK(I),ZIN(I),VALID1,VALID2,
     $              ITWIN,ITWERR,DADZ)
          IF(VALID1.AND.VALID2) THEN
      	    TEMPS=0.0
      	    DO 100 J=1,MAXTWIN
      	      TEMPS=TEMPS+ITWIN(J)*BB(J)
100	    CONTINUE
            SUM=SUM+TEMPS
C  Note RINT is a global array. Indexing should be offset by JREFL.
            DIFFER=RINT(I+JREFL)-TEMPS
      	    FUNCL=FUNCL+DIFFER*DIFFER
            DIFSUM=DIFSUM+ABS(DIFFER)
      	    ICOUNT=ICOUNT+1
          ENDIF   !VALID1.AND.VALID2
      	ELSE
      	  CALL GETCRVVAL(IIH(I),IIK(I),ZIN(I),
     $	  VALID,ITWIN,ITWERR,DADZ,TA_REFMT,WSTAR)
      	  IF(VALID) THEN
            SUM=SUM+ITWIN(1)
C  Note RINT is a global array. Indexing should be offset by JREFL.
            DIFFER=RINT(I+JREFL)-ITWIN(1)
      	    FUNCL=FUNCL+DIFFER*DIFFER
            DIFSUM=DIFSUM+ABS(DIFFER)
      	    ICOUNT=ICOUNT+1
      	  ENDIF   !VALID
      	ENDIF    !DETWIN
10    CONTINUE
C
      RFAC=DIFSUM/SUM
      IF(DETWIN.OR.TPIN) THEN
      	WRITE(6,26)(BB(J),J=1,MAXTWIN)
26	FORMAT(' Twin proportions used for R-factor',
     $' calculation : ',4F10.4)
      ENDIF
C
      WRITE(6,27)RFAC,ICOUNT,FUNCL
27	FORMAT('   R factor = ',F8.3,'  for ',I6,' reflections.',
     $'   Actual function minimized (FUNCL)=',G18.6/)
      RETURN
      END
C************************************************************************
C
      SUBROUTINE FRIEDELR(IN,JREFL,NRSUMS,RSUMS,RDIFS,
     $	      NRSUMW,RSUMW,RDIFW,NNEG,RFRIED)
C SUBROUTINE TO CALCULATE FRIEDEL R FACTOR IN FILM
      INCLUDE 'merge.inc'
C
      PARAMETER (AVREFL=400.00)
C
C   AVREFL IS AN ARBITRARY VALUE SEPARATING STRONG AND WEAK SPOTS FOR
C   R-FACTOR CALCULATIONS.
C
C  INITIALIZE UNSCALED FRIEDEL R
      RUNSSUM=0.
      RUNSDIF=0.
C
C  INITIALIZE FOR SCALED FRIEDEL R
C
      RSUM=0.
      RSUMS=0.
      RSUMW=0.
      RDIF=0.
      RDIFS=0.
      RDIFW=0.
C
      NRSUM=0
      NRSUMS=0
      NRSUMW=0
      NNEG=0
C
      DO 1210 I=1,IN
      JRMAX=JREFL-IN
      INEW=JRMAX+I
C INDEX I IS LOCAL, INEW IS GLOBAL
C FOR UNSCALED INTENSITIES
        RUNSSUM=RUNSSUM+RIIN(I)
        IF(RIIN(I).LT.0.0) THEN
      	  RUNSSUM=RUNSSUM-RIIN(I)+1.0
      	ENDIF
        RUNSDIF=RUNSDIF+ABS(DIN(I))
C NOW FOR SCALED INTENSITIES
      IF(RINT(INEW).GT.AVREFL) THEN
        RSUMS=RSUMS+RINT(INEW)
        NRSUMS=NRSUMS+1
        RDIFS=RDIFS+ABS(DIFF(INEW))
      ELSE
        RSUMW=RSUMW+RINT(INEW)
        IF(RINT(INEW).LT.0.0) THEN
      	  RSUMW=RSUMW-RINT(INEW)+1.0
      	  NNEG=NNEG+1
      	ENDIF
        NRSUMW=NRSUMW+1
        RDIFW=RDIFW+ABS(DIFF(INEW))
      ENDIF
1210  CONTINUE
C
      RUNSOUT=RUNSDIF/RUNSSUM
      IF(RSUMW.NE.0.0) THEN
        ROUTW=(RDIFW/RSUMW)
      ELSE
        ROUTW=0.0
      ENDIF
      ROUTS=RDIFS/RSUMS
      ROUT=(RDIFS+RDIFW)/(RSUMS+RSUMW)
C
      WRITE (6,1249) NNEG
1249  FORMAT (I5,' spots are negative.')
      WRITE (6,1250) RUNSOUT
1250  FORMAT (' Friedel R-factor for unscaled intensities: ',F10.4)
      WRITE (6,1251) NRSUMW,ROUTW
1251  FORMAT ('    After scaling,'/
     $' Friedel R-factor for',I5,' weak spots:  ',F10.5)
      WRITE (6,1252) NRSUMS,ROUTS
1252  FORMAT (' Friedel R-factor for',I5,' strong spots:',F10.5)
      WRITE (6,1253) IN,ROUT
1253  FORMAT (' Friedel R-factor for all',I5,' spots:   ',F10.5)
      RFRIED=ROUT/2.0
      RETURN
      END
C**************************************************************************
      SUBROUTINE SHLSRT(KEY,N,IPOINT,INCDEC)
C     SHELL SORT
C     REFERENCES:  D.L. SHELL, CACM 2, 32 (JULY 1959)
C                  D.E. KNUTH, TAOCP III, SECT. 5.2.1
C
C     CALLING SEQUENCE:
C     KEY    IS AN ARRAY OF KEYS ON WHICH TO SORT
C     N      IS THE NUMBER OF ITEMS
C     IPOINT IS THE ARRAY OF POINTERS
C            (ONLY THE POINTERS WILL MOVE)
C     INCDEC .GE. 0 FOR SORTING INTO INCREASING ORDER;
C            .LT. 0 FOR SORTING INTO DECREASING ORDER
C
      REAL*4 KEY,K
      INTEGER H,S,T
      DIMENSION KEY(1),IPOINT(1)
C
C     NEXT STATEMENT JUST IN CASE N .EQ. 1
      IPOINT(1) = 1
C
C     CHECK N
C
      IF (N - 1) 11,11,1
C
C
C     INITIALIZE POINTER ARRAY
C
C     (NOTE THAT N MUST BE .GT. 1 IF WE GOT HERE)
    1 DO 2 I = 2,N
    2 IPOINT(I) = I
C
C     CHOICE OF SEQUENCE OF INCREMENTS SUGGESTED
C     BY KNUTH III, EQ. 8, P. 95.   HIS FORMULA
C     IS EQUIVALENT TO:
C
C            H(S) = (3**S - 1)/2
C            INITIAL VALUE OF S IS MINIMAL INTEGER
C              SUCH THAT H(S+2) .GE. N
C
C
C     SMAX = (ALOG(2N + 1)/ALOG(3)) - 2 + 1
      S = INT( (ALOG(FLOAT(2*N+1))/1.09861229) - 0.95 )
      S = MAX0(S,1)
C
      H = (3**S - 1)/2
C
      DO 7 T = 1,S
C
      JMIN = H + 1
      DO 6 J = JMIN,N
C
      I = J - H
      JJ = IPOINT(J)
      K = KEY(JJ)
      IPT = IPOINT(J)
C
    3 II = IPOINT(I)
      IF (K - KEY(II)) 4,4,5
C
    4 IPLUSH = I + H
      IPOINT(IPLUSH) = IPOINT(I)
      I = I - H
      IF (I) 5,5,3
C
    5 IPLUSH = I + H
      IPOINT(IPLUSH) = IPT
C
    6 CONTINUE
C
C     CHANGE INCREMENT
C
      IF (H - 1) 8,8,7
    7 H = (H-1)/3
C
C
C      CHECK INCDEC: IF NEGATIVE, SWITCH POINTER ARRAY
C
    8 IF (INCDEC) 9,11,11
C
    9 M = N/2
C     NP1MI = N + 1 - I
      NP1MI = N
      DO 10 I = 1,M
      NTEMP = IPOINT(I)
      IPOINT(I) = IPOINT(NP1MI)
      IPOINT(NP1MI) = NTEMP
   10 NP1MI = NP1MI - 1
C
   11 RETURN
      END
C*************************************************************************
      SUBROUTINE IOPRELCF(NREFL,DETWIN,BB,RESMIN,RESMAX,LMODSIG,IFILM,
     $XINTMIN,XINTMAX,DCUTOFF,TAXA,TANGL,RMAXIN,LPRINT,TA_REFMT,WSTAR)
C creates pre-lcf file (channel 8) and delta F vs S (channel 10)
      LOGICAL VALID,VAL1,VAL2,LMODSIG
      INCLUDE 'merge.inc'
      DIMENSION DELTAF(MAXREFL)
      REAL*8  BB(1)
      DIMENSION ITWIN(4),ITWERR(4),DADZ(4)
      LOGICAL DETWIN,LPRINT,TA_REFMT
C
C Calculate angles for Z re-determination
C
      TAXB=TAXA+ABANG
      STAXA=ASTAR*SIN(DRAD*TAXA)
      STAXB=BSTAR*SIN(DRAD*TAXB)
      TTANGL=TAN(TANGL*DRAD)
C
C Simple calculation to find minimum SIGID:  MIN(SIGID)=0.5*AVG(SIGID)
C  SIGID is estimated by DIFF initially
C
      IF(LMODSIG) THEN
      	SUMSIGI=0.0
      	DO 30 I=1,NREFL
      	  SUMSIGI=SUMSIGI+ABS(DIFF(I))
30    	CONTINUE
      	SIGIMIN=0.5*SUMSIGI/NREFL
      	DO 40 I=1,NREFL
      	  IF(ABS(DIFF(I)).LT.SIGIMIN) DIFF(I)=SIGN(SIGIMIN,DIFF(I))
40    	CONTINUE
      ENDIF
      IZERO=0
C
      WRITE(6,890) RESMIN,RESMAX,LMODSIG,XINTMIN,XINTMAX,DCUTOFF,LPRINT
890   FORMAT('    Output of h,k,z, delta F and intensities'//
     $' Resolution limits:  minumum (Angstroms).........',F8.2/
     $'                     maximum (Angstroms).........',F8.2/
     $' Modify SIG values to average, lower limit.......',L5/
     $' Intensity cutoffs:  minumum.....................',F8.1/
     $'  (after scaling)    maximum.....................',F8.1/
     $' Difference amplitude truncation level...........',F8.1/
     $' Print Delta Fs for low angle correlation calc...',L5)
      WRITE(8,900)ITITLE  !A TITLE
      WRITE(10,900)ITITLE  !A TITLE
900   FORMAT(20A4)
      WRITE(8,999)   !A HEADER
999   FORMAT('     H     K     ZSTAR       DELTAF       FD',
     $'        SIGFD         FP         SIGFP       ANOM')
C
      ICOUNT=0
      DO 100 II=1,NREFL
      I=JOUT(II)
      IF(RINT(I).LE.XINTMAX.AND.RINT(I).GE.XINTMIN) THEN !OK
      	RES=RESOLUTION(IIH(I),IIK(I),ZSTAR(I))   !FROM ASYM
C
      	  IF(DETWIN) THEN
      	    CALL GETITWIN(IIH(I),IIK(I),ZSTAR(I),
     $	  VAL1,VAL2,ITWIN,ITWERR,DADZ)
      	    VALID=VAL1.AND.VAL2
      	    IF(VALID) THEN
      	      AREF=0.0
	      EREF=0.0
      	      DO 60 J=1,4
      	        AREF=AREF+BB(J)*ITWIN(J)
      	        EREF=EREF+BB(J)*ITWERR(J)
60	      CONTINUE
      	      ICURVEV=AREF
	      ICRVERR=EREF
      	    ENDIF
      	  ELSE
      	   CALL GETCRVVAL(IIH(I),IIK(I),ZSTAR(I),
     $	  VALID,ICURVEV,ICRVERR,DADZ,TA_REFMT,WSTAR)
      	  ENDIF
C      WRITE(6,997)VALID,ICURVEV,IHIN(I),IKIN(I),ZIN(I),RINT(I),JFILM(I)
C997	FORMAT(3X,L5,3I7,2F10.3,I8)
      	  IF(VALID.AND.(ICURVEV.GE.0)) THEN
      	    FP=SQRT(FLOAT(ICURVEV))
	    SIGFP=SQRT(FLOAT(ICURVEV+ICRVERR))-FP
 	    DELTAF(I)=SQRT(RINT(I))-FP
      	 WRITE(10,996)DELTAF(I),S(IIH(I),IIK(I),ZSTAR(I))*10000,IZERO
996	    FORMAT(2F12.2,I6)
            IF(RES.LE.RESMAX.AND.RES.GE.RESMIN) THEN
      	      IF(ABS(DELTAF(I)).GT.DCUTOFF)
     $	 DELTAF(I)=SIGN(DCUTOFF,DELTAF(I))
              Z=(IHIN(I)*STAXA+IKIN(I)*STAXB)*TTANGL
     	      FD=SQRT(RINT(I))
      	      SIGFD=SQRT(RINT(I)+ABS(DIFF(I)))-FD
      	      ANOM=SIGN(SQRT(ABS(DIFF(I))),DIFF(I))
      	      WRITE(8,1000)IHIN(I),IKIN(I),Z,DELTAF(I),
     $        FD,SIGFD,FP,SIGFP,ANOM
1000	      FORMAT(2I6,F10.4,6F12.4)
              ICOUNT=ICOUNT+1
      	    ENDIF
      	  ENDIF
      ENDIF
100   CONTINUE
      CLOSE(8,STATUS='KEEP',ERR=200)
      CLOSE(10,STATUS='KEEP',ERR=201)
C
      WRITE(6,891)ICOUNT
      WRITE(11,891)ICOUNT
891   FORMAT(I5,' delta F values written.')
C
      IF(ABS(TANGL).LT.5.0)
     $	CALL TRIPLETCOR(NREFL,DELTAF,RMAXIN,LPRINT,IFILM)
      RETURN
200	WRITE(6,998)
998	FORMAT(' Error on closing pre-LCF file - aborting')
      STOP
201	WRITE(6,988)
988	FORMAT(' Error on closing dF vs S file - aborting')
      STOP
      END
C*************************************************************************
      FUNCTION S(IH,IK,ZSTARG)
      INCLUDE 'merge.inc'
        AI=IH
        BI=IK
        S=((AI*ASTAR)**2+(BI*BSTAR)**2+ZSTARG**2 +
     1      2.*AI*BI*ASTAR*BSTAR*COS(ABANG*DRAD))/4.0
      RETURN
      END
C*************************************************************************
      FUNCTION RESOLUTION(IH,IK,ZSTARG)
      RESOLUTION=0.5/SQRT(S(IH,IK,ZSTARG))
      RETURN
      END
C*************************************************************************
      SUBROUTINE TRIPLETCOR(JREFL,DELTAF,RMAXIN,LPRINT,IFILM)
C
      INCLUDE 'merge.inc'
      DIMENSION DELTAF(1)
      PARAMETER (NSLOTS=5)
c      DIMENSION XDIF(3),LINDEX(3),FD(NSLOTS),NUM(NSLOTS)
      DIMENSION XDIF(3),FD(NSLOTS),NUM(NSLOTS)
      DIMENSION NDIF12(NSLOTS),SD12(NSLOTS),SD21(NSLOTS),SD1D2(NSLOTS),
     $SD1D12(NSLOTS),SD2D21(NSLOTS)
      DIMENSION NDIF13(NSLOTS),SD13(NSLOTS),SD31(NSLOTS),SD1D3(NSLOTS),
     $SD1D13(NSLOTS),SD3D31(NSLOTS)
      DIMENSION NDIF23(NSLOTS),SD23(NSLOTS),SD32(NSLOTS),SD2D3(NSLOTS),
     $SD2D23(NSLOTS),SD3D32(NSLOTS)
      DIMENSION CCOEF1(NSLOTS),CCOEF2(NSLOTS),CCOEF3(NSLOTS)
C
c      LOGICAL LFRIED,LNONE,LDIF(3),LPRINT,LSIGN(3)
      LOGICAL LDIF(3),LPRINT
C
      DO 5 I=1,NSLOTS
      NDIF12(I)=0
      SD12(I)=0.0
      SD21(I)=0.0
      SD1D2(I)=0.0
      SD1D12(I)=0.0
      SD2D21(I)=0.0
C
      NDIF13(I)=0
      SD13(I)=0.0
      SD31(I)=0.0
      SD1D3(I)=0.0
      SD1D13(I)=0.0
      SD3D31(I)=0.0
C
      NDIF23(I)=0
      SD23(I)=0.0
      SD32(I)=0.0
      SD2D3(I)=0.0
      SD2D23(I)=0.0
      SD3D32(I)=0.0
5     CONTINUE
C
      IF(LPRINT) WRITE(6,107)
107   FORMAT('   H     K     XDIF1     XDIF2     XDIF3   SLOT',
     $' RESOLUTION NOF3')
C
      II=1
      DO WHILE (II.LT.JREFL)
      I=JOUT(II)
      J=JOUT(II+1)
      K=JOUT(II+2)
      IH=JH(I)
      IK=JK(I)
      NOF3=1
      IF(RINT(I).GT.0.0) THEN
      	SUMF=SQRT(RINT(I))
      ELSE
      	SUMF=0.0
      ENDIF
      XDIF(1)=DELTAF(I)
      LDIF(1)=.TRUE.
      IF(IH.EQ.JH(J).AND.IK.EQ.JK(J)) THEN
      	IF(RINT(J).GT.0.0) THEN
      	  SUMF=SUMF+SQRT(RINT(J))
      	ENDIF
      	XDIF(2)=DELTAF(J)
      	NOF3=NOF3+1
      	LDIF(2)=.TRUE.
      ELSE
      	XDIF(2)=0.0
      	LDIF(2)=.FALSE.
      ENDIF
C
      IF(IH.EQ.JH(K).AND.IK.EQ.JK(K)) THEN
      	IF(RINT(K).GT.0.0) THEN
      	  SUMF=SUMF+SQRT(RINT(K))
      	ENDIF
      	XDIF(3)=DELTAF(K)
      	NOF3=NOF3+1
      	LDIF(3)=.TRUE.
      ELSE
      	XDIF(3)=0.0
      	LDIF(3)=.FALSE.
      ENDIF
C
C GET CORRELATION FOR EACH SPOT, AND PUT IN APPROPRIATE RES RANGE
C
      CALL GETSLOT(JH(I),JK(I),ZSTAR(I),RMAXIN,ISLOT,RES)
      IF(LPRINT) WRITE(6,111)JH(I),JK(I),(XDIF(K),K=1,3),ISLOT,RES,NOF3
111   FORMAT(2I5,3F10.2,I5,F8.2,I10)
C
      XDAVG=(XDIF(1)+XDIF(2)+XDIF(3))/FLOAT(NOF3)
      XFAVG=SUMF/FLOAT(NOF3)
      IF(XFAVG.NE.0.0) THEN
      	FD(ISLOT)=FD(ISLOT)+ABS(XDAVG/XFAVG)
      ENDIF
      NUM(ISLOT)=NUM(ISLOT)+1
C
      IF(LDIF(2)) THEN
      	CALL SUMMS(1,2,ISLOT,XDIF,SD12,SD21,SD1D2,SD1D12,SD2D21)
      	NDIF12(ISLOT)=NDIF12(ISLOT)+1
      ENDIF
      IF(LDIF(3)) THEN
      	CALL SUMMS(1,3,ISLOT,XDIF,SD13,SD31,SD1D3,SD1D13,SD3D31)
      	NDIF13(ISLOT)=NDIF13(ISLOT)+1
      ENDIF
      IF(LDIF(2).AND.LDIF(3)) THEN
      	CALL SUMMS(2,3,ISLOT,XDIF,SD23,SD32,SD2D3,SD2D23,SD3D32)
      	NDIF23(ISLOT)=NDIF23(ISLOT)+1
      ENDIF
      II=II+NOF3
      END DO
C
C      WRITE(6,*) (NDIF12(I),I=1,NSLOTS)
C      WRITE(6,*) (SD12(I),I=1,NSLOTS)
      WRITE(6,108)
108   FORMAT(/' Delta F correlation coefficients,  '/
     $' For three possible comparisons, computed:')
      CALL CORREL(NDIF12,SD12,SD21,SD1D2,SD1D12,SD2D21,RMAXIN,CCOEF1)
      CALL CORREL(NDIF13,SD13,SD31,SD1D3,SD1D13,SD3D31,RMAXIN,CCOEF2)
      CALL CORREL(NDIF23,SD23,SD32,SD2D3,SD2D23,SD3D32,RMAXIN,CCOEF3)
C
      CALL AVCOR(NDIF12,NDIF13,NDIF23,CCOEF1,CCOEF2,CCOEF3,
     $	 RMAXIN,FD,NUM)
      RETURN
      END
C*************************************************************************
      SUBROUTINE GETSLOT(IH,IK,Z,STHMAX,ISLOT,RES)
      INCLUDE 'merge.inc'
      PARAMETER (NSLOTS=5)  ! NUMBER OF GROUPS
C
        AI=IH
        BI=IK
        SINTH=((AI*ASTAR)**2+(BI*BSTAR)**2+Z**2 +
     1      2.*AI*BI*ASTAR*BSTAR*COS(ABANG*DRAD))/4.0
        ISLOT=(SINTH*FLOAT(NSLOTS)/STHMAX)+1
      	RES=SQRT(1.0/(SINTH*4.0))
        IF(ISLOT.GT.NSLOTS) ISLOT=NSLOTS
C
      RETURN
      END
C*************************************************************************
      SUBROUTINE GETNSPOT(IH,IK,IHNEW,IKNEW)
      IHNEW=-(IH+IK)
      IKNEW=IH
      RETURN
      END
C*************************************************************************
      SUBROUTINE SEARCHSPOT(ISTART,JREFL,IH,IK,INDEX,LFRIED,LNONE
     $	   ,LPRINT)
      INCLUDE 'merge.inc'
C
      LOGICAL LFRIED,LNONE,LPRINT
C
C      WRITE(6,*) ISTART,JREFL
      DO 10 I=ISTART,JREFL
C      WRITE(6,*) IH,IK,JH(I),JK(I)
      IF(IH.EQ.JH(I)) THEN
      	IF(IK.EQ.JK(I)) THEN
      	  LFRIED=.FALSE.
      	  GOTO 20
      	ENDIF
      ENDIF
      IF(IH.EQ.-JH(I)) THEN
      	IF(IK.EQ.-JK(I)) THEN
      	  LFRIED=.TRUE.
      	  GOTO 20
      	ENDIF
      ENDIF
10    CONTINUE
      LNONE=.TRUE.
      RETURN
20    LNONE=.FALSE.
      INDEX=I
C      IF(LPRINT) WRITE(6,112) IH,IK,JH(I),JK(I),LFRIED,DIFF(I)
C112   FORMAT(' OLD H,K',2I4,' NEW H,K',2I4,' FRIEDEL SPOT?',L3,
C     $' DIFF=',F10.2)
      RETURN
      END
C*************************************************************************
      SUBROUTINE SUMMS(I,J,ISLOT,XDIF,SUMD1,SUMD2,
     $	  SUMD1D2,SUMD1D1,SUMD2D2)
      DIMENSION XDIF(3)
C
      PARAMETER (NSLOTS=5)
c      DIMENSION NDIF12(NSLOTS),SUMD1(NSLOTS),SUMD2(NSLOTS),
      DIMENSION SUMD1(NSLOTS),SUMD2(NSLOTS),
     $SUMD1D2(NSLOTS),SUMD1D1(NSLOTS),SUMD2D2(NSLOTS)
C
      SUMD1(ISLOT)=SUMD1(ISLOT)+XDIF(I)
      SUMD2(ISLOT)=SUMD2(ISLOT)+XDIF(J)
      SUMD1D2(ISLOT)=SUMD1D2(ISLOT)+XDIF(I)*XDIF(J)
      SUMD1D1(ISLOT)=SUMD1D1(ISLOT)+XDIF(I)*XDIF(I)
      SUMD2D2(ISLOT)=SUMD2D2(ISLOT)+XDIF(J)*XDIF(J)
      RETURN
      END
C*************************************************************************
      SUBROUTINE CORREL(NDIF12,SD12,SD21,SD1D2,SD1D12,SD2D21,
     $	  STHMAX,CORREL1)
C
      PARAMETER (NSLOTS=5)
      DIMENSION NDIF12(NSLOTS),SD12(NSLOTS),SD21(NSLOTS),SD1D2(NSLOTS),
     $SD1D12(NSLOTS),SD2D21(NSLOTS),CORREL1(NSLOTS)
C
C      WRITE(6,*) (NDIF12(I),I=1,NSLOTS)
C      WRITE(6,*) (SD12(I),I=1,NSLOTS)
      WRITE(6,109)
109   FORMAT(/' Correlation coefficient in resolution ranges:'/
     $' Slot #   ((sinth/lambda)**2)/4.0     resolution (A)     ',
     $'#refl   correlation coef')
      DO 10 ISLOT=1,NSLOTS
      IF(NDIF12(ISLOT).EQ.0) GOTO 10
      SINTH=(ISLOT-0.5)*STHMAX/FLOAT(NSLOTS)
      RES=SQRT(1.0/(SINTH*4.0))
C
      TOP=SD1D2(ISLOT)-SD12(ISLOT)*SD21(ISLOT)/NDIF12(ISLOT)
      B1=SD1D12(ISLOT)-(SD12(ISLOT)**2)/NDIF12(ISLOT)
      B2=SD2D21(ISLOT)-(SD21(ISLOT)**2)/NDIF12(ISLOT)
      BOTTOM=SQRT(B1*B2)
      IF(BOTTOM.EQ.0.0) THEN
      	CORREL1(ISLOT)=0.0
      ELSE
      	CORREL1(ISLOT)=TOP/BOTTOM
      ENDIF
      WRITE(6,110) ISLOT,SINTH,RES,NDIF12(ISLOT),CORREL1(ISLOT)
110   FORMAT(I7,3X,F12.4,18X,F6.2,8X,I6,5X,F10.3)
10    CONTINUE
      RETURN
      END
C*************************************************************************
      SUBROUTINE AVCOR(N1,N2,N3,C1,C2,C3,STHMAX,DFI,NUM)
C
      INCLUDE 'merge.inc'
      PARAMETER (NSLOTS=5)
      DIMENSION DFI(NSLOTS),NUM(NSLOTS),RES(NSLOTS),AVGN(NSLOTS)
      DIMENSION N1(NSLOTS),N2(NSLOTS),N3(NSLOTS),AVGC(NSLOTS)
      DIMENSION C1(NSLOTS),C2(NSLOTS),C3(NSLOTS),ADFI(NSLOTS)
C
      DO 10 ISL=1,NSLOTS
      	IF(NUM(ISL).EQ.0) GOTO 20
      	SINTH=(ISL-0.5)*STHMAX/FLOAT(NSLOTS)
      	RES(ISL)=SQRT(1.0/(SINTH*4.0))
      	AVGN(ISL)=FLOAT(N1(ISL)+N2(ISL)+N3(ISL))/3.0
      	AVGC(ISL)=(C1(ISL)+C2(ISL)+C3(ISL))/3.0
      	ADFI(ISL)=DFI(ISL)/FLOAT(NUM(ISL))
10    CONTINUE
C
20    NSL=ISL-1   !NUMBER OF SLOTS WITH DATA

      WRITE(6,100) (RES(I),I=1,NSL)
100   FORMAT(/'    Resolution (A)          ',5(F6.2,1X))
      WRITE(6,101) (AVGN(I),I=1,NSL)
101   FORMAT('    # of triplet comparisons',1X,5(F4.0,3X))
      WRITE(6,102) (AVGC(I),I=1,NSL)
102   FORMAT('    Correlation coefficient ',5F7.3)
      WRITE(6,103) (ADFI(I),I=1,NSL)
103   FORMAT('    (Delta F)/F             ',5F7.3)
C
C WRITE THIS OUT TO FILE____.DAT
C
      WRITE(11,100) (RES(I),I=1,NSL)
      WRITE(11,101) (AVGN(I),I=1,NSL)
      WRITE(11,102) (AVGC(I),I=1,NSL)
      WRITE(11,103) (ADFI(I),I=1,NSL)
      RETURN
      END
C*************************************************************************
      SUBROUTINE OUT3OR4(JREFL,IFILE,IGUIDE,TA_REFMT,WSTAR,LSUMMARY)
C
C
      INCLUDE 'merge.inc'
      LOGICAL IGUIDE,TA_REFMT,LSUMMARY
C
      IF(IGUIDE) THEN
      	ZRANGE=0.106 !RES LIMIT OF GUIDE POINT DETERMINATION
      	JFGUIDE=0  !FILM # USED TO IDENTIFY GUIDE POINTS
      	DIFFGUID=500. !STD ERROR ON GUIDE POINTS
C
      	READ(5,*) ZSRTIN,DGUIDIN
      	IF(ZSRTIN.NE.0.0) THEN
      	  ZRANGE=ZSRTIN
      	  DIFFGUID=DGUIDIN
        ENDIF
      	WRITE(6,186) ZRANGE,DIFFGUID
186   	FORMAT(' ZRANGE....(Res. limit on guide pts)......',F8.3/
     $         ' DIFFGUID..(Error on guide pts)...........',F8.3)
C
      ENDIF !IGUIDE
      NGUIDE=0
      NONZERO=0
      I=1
C
300   ISTART=I
      LH=JH(JOUT(I))
      LK=JK(JOUT(I))
C
400   I=I+1
      IF((LH.EQ.JH(JOUT(I))).AND.(LK.EQ.JK(JOUT(I)))) GOTO 400
C
      IF(IFILE.EQ.4) THEN
        WRITE(4,187)
187     FORMAT(//)
      ENDIF
      IEND=I-1
      IF(IGUIDE) CALL GUIDELINE(LH,LK,-ZRANGE,ZSTAR(JOUT(ISTART)),
     $	 TA_REFMT,WSTAR,IFILE,DIFFGUID,JFGUIDE,NGUIDE)
C
      DO 600 J=ISTART,IEND
      	K=JOUT(J)
      	IF(RINT(K).NE.0.0.OR.DIFF(K).NE.0.0) THEN
      	  WRITE(IFILE,1280)LH,LK,ZSTAR(K),RINT(K),ABS(DIFF(K)),JFILM(K)
      	  NONZERO=NONZERO+1
1280      FORMAT(2I5,F10.4,2F10.1,I6)
      	 ENDIF
600   CONTINUE
C
      IF(IGUIDE) CALL GUIDELINE(LH,LK,ZSTAR(JOUT(IEND)),ZRANGE,
     $	 TA_REFMT,WSTAR,IFILE,DIFFGUID,JFGUIDE,NGUIDE)
C
      IF(I.LT.JREFL) GOTO 300
C
      I100=100
      ZERO=0.0
      IF(IFILE.EQ.3) WRITE(IFILE,1280) I100,I100,ZERO,ZERO,ZERO,I100
      WRITE(6,1281) NONZERO,IFILE,JREFL
1281  FORMAT(I8,' nonzero spots written to unit',I2,
     $', out of total of',I8)
      IF(NGUIDE.GT.0) WRITE(6,1282) NGUIDE
1282  FORMAT(I8,' guide points added beyond new data ',
     $'from old curves.')
      IF(LSUMMARY) THEN
      	WRITE(12,1283)
1283	FORMAT(/////)
      	WRITE(12,1281) NONZERO,IFILE,JREFL
      	IF(NGUIDE.GT.0) WRITE(12,1282) NGUIDE
      ENDIF
C
      RETURN
      END
C*************************************************************************
      SUBROUTINE GUIDELINE(LH,LK,ZSTART,ZEND,TA_REFMT,WSTAR,
     $	  IFILE,DIFFGUID,JFGUIDE,NGUIDE)
C
      INCLUDE 'merge.inc'
      DIMENSION GRAD(4)
      LOGICAL VALID,TA_REFMT
C
      ZRANGE = ZEND-ZSTART
      IF(ZRANGE.LE.0.0) RETURN
      IZR=ZRANGE/DZ
      DO 205 M=0,IZR
      	Z=ZSTART+M*DZ
      	CALL GETCRVVAL(LH,LK,Z,VALID,ICURVAL,ITWERR,
     $	 GRAD,TA_REFMT,WSTAR)
      	IF(VALID) THEN
      	  CURV=ICURVAL
      	  WRITE(IFILE,1280) LH,LK,Z,CURV,DIFFGUID,JFGUIDE
1280      FORMAT(2I5,F10.4,2F10.1,I6)
      	  NGUIDE=NGUIDE+1
      	ENDIF
205   CONTINUE
      RETURN
      END
C****************************************************************************
C
C  MATRIX INVERSION ROUTINE MODIFIED FROM HARWELL LIBRARY (RH15)
C  CHANGES PUT IN TO MAKE IT WORK ON VAX
C  1.  12 STATEMENTS FOR CALCULATION OF OVER/UNDERFLOW OF DETERMINANT IN MA21
C      REPLACED BY A SIMPLE ONE WHICH WILL OVERFLOW MORE EASILY(ENTRY MA21CD)
C  2.  JSCALE (COMMON BLOCK) SET TO 0 (NO SCALING)-PREVENTS ENTRY TO MC10AD.
C  3.  ALL DOUBLE PRECISION REPLACED BY REAL*8 (NOT REALLY NECESSARY).
C  4.  REPLACE A(N),B(N) BY A(1),B(1) IN FM02AD TO AVOID VAX ARRAY CHECKING.
C
      FUNCTION FA01AS(I)
      COMMON/FA01ES/G
      REAL*8 G
      G=DMOD(G* 9228907.D0,4294967296.D0)
      IF(I.GE.0)FA01AS=G/4294967296.D0
      IF(I.LT.0)FA01AS=2.D0*G/4294967296.D0-1.D0
      RETURN
      END
C*********************************************************************
      SUBROUTINE FA01BS(MAX,NRAND)
      NRAND=INT(FA01AS(1)*FLOAT(MAX))+1
      RETURN
      END
C**********************************************************************
      SUBROUTINE FA01CS(IL,IR)
      COMMON/FA01ES/G
      REAL*8 G
      IL=G/65536.D0
      IR=G-65536.D0*FLOAT(IL)
      RETURN
      END
C***********************************************************************
      SUBROUTINE FA01DS(IL,IR)
      COMMON/FA01ES/G
      REAL*8 G
      G=65536.D0*FLOAT(IL)+FLOAT(IR)
      RETURN
      END
      BLOCK DATAFA
      COMMON/FA01ES/G
      REAL*8 G
      DATA G/1431655765.D0/
      END
C    FM02AD - A ROUTINE TO COMPUTE THE INNER PRODUCT OF TWO
C      DOUBLE PRECISION REAL VECTORS ACCUMULATING THE RESULT
C      DOUBLE PRECISION.  IT CAN BE USED AS AN ALTERNATIVE
C      TO THE ASSEMBLER VERSION, BUT NOTE THAT IT IS LIKELY
C      TO BE SIGNIFICANTLY SLOWER IN EXECUTION.
C
      REAL*8 FUNCTION FM02AD(N,A,IA,B,IB)
      REAL*8 R1,A,B
C  THE FOLLOWING STATEMENT CHANGED FROM A(N),B(N) TO AVOID VAX DYNAMIC
C  ARRAY CHECK FAILURE.
      DIMENSION A(1),B(1)
C
C    N   THE LENGTH OF THE VECTORS (IF N<= 0  FM02AD = 0)
C    A   THE FIRST VECTOR
C    IA  SUBSCRIPT DISPLACEMENT BETWEEN ELEMENTS OF A
C    B   THE SECOND VECTOR
C    IB  SUBSCRIPT DISPLACEMENT BETWEEN ELEMENTS OF B
C    FM02AD  THE RESULT
C
      R1=0D0
      IF(N.LE.0) GO TO 2
      JA=1
      IF(IA.LT.0) JA=1-(N-1)*IA
      JB=1
      IF(IB.LT.0) JB=1-(N-1)*IB
      I=0
    1 I=I+1
      R1=R1+A(JA)*B(JB)
      JA=JA+IA
      JB=JB+IB
      IF(I.LT.N) GO TO 1
    2 FM02AD=R1
      RETURN
      END
      BLOCK DATAMA
      COMMON /MA21ED/LP,JSCALE,EA,EB
C JSCALE NORMALLY =1, TEMPORARILY SET TO 0
      INTEGER * 4 LP/6/,JSCALE/0/
      REAL * 8 EA/1.0D-16/,EB/1.0D-16/
      END
C**********************************************************************
      SUBROUTINE MA21AD (A,IA,N,B,W,E)
      COMMON /MA21ED/ LP,JSCALE,EA,EB
         REAL *  8 A(IA,N),B(N),W(N,N+5),AA,AC,DET,WW,Q(2),PCK
      REAL *8 EPS4/4.0D-16/,ZERO/0.0D0/,ONE/1.0D0/,TWO/2.0D0/,P5/0.5D0/
     1,E,EA,EB,EPSN,XNORM,AXNORM,ENORM,ENORMA,ANORMA,ANORM,ERR,AB,AM
     2,EPS /1.0D-16/,R(4),RA,FM02AD
CTSH      REAL*8DICT(8 )/' MA21AD ',' MA21BD ',' MA21CD ',' MA21DD ',
CTSH     1               ' N IS   ',' PIVOT  ','IS SMALL','IS ZERO '/
CTSH++
	REAL*8 DICT(8)
	CHARACTER*8 TMPDICT(8)/
     *  ' MA21AD ',' MA21BD ',' MA21CD ',' MA21DD ',
     *  ' N IS   ',' PIVOT  ','IS SMALL','IS ZERO '/
	EQUIVALENCE (TMPDICT,DICT)
CTSH--
C     EPS=MACHINE PRECISION,EPS4=EPS*4
C  I3, I4 previously initialised to 0 but changed because of EQUIVALENCE stmt.
C  MRO'D 23/10/81
c      INTEGER*4 I3,I4,I1(4)
      INTEGER*4 I3,I4
      REAL*4 UPCK(4)
c      LOGICAL*1 L1,L2,L3(4),L4(4),LQ,LA
      LOGICAL*1 L1,L2,L3(4),L4(4),LA
      EQUIVALENCE (Q(1),R(1),L1),(RA,LA),(WW,II),(UPCK(1),PCK),
     1            (Q(2),L2),(L3(1),I3),(L4(1),I4)
C	DATA LQ/Z40/  not needed in this implementation
C  Next two lines inserted to initialise I3,I4 by assignment - see above - MRO'D
      I3 = 0
      I4 = 0
      IENT = 1
      GO TO 100
      ENTRY MA21BD(A,IA,N,W,E)
      IENT = 2
      GO TO 100
      ENTRY MA21CD(A,IA,N,DET,IDET,W)
      IENT = 3
      DET = ONE
      IDET = 0
  100 IF(N.LT.1)GO TO 810
      IP = 0
      IF(JSCALE .LE. 0)GO TO 120
C     FIND SCALING FACTORS.
      CALL MC10AD(A,N,IA,W(1,5),W(1,1),IT)
      DO 110 I=1,N
      PCK = W(I,1)
      W(2*I-1,3) = UPCK(1)
  110 W(2*I,3) = UPCK(2)
C     STORE IN W(J,1),J=1,N THE MAXIMAL ELEMENTS  OF COLUMNS AFTER
C     APPLICATION OF SCALING.
  120 EPSN = ZERO
      DO 140 J=1,N
      AB = ZERO
      DO 130 I=1,N
      AM =  DABS(A(I,J))
      IF(JSCALE.GT.0)AM=AM*W(I,3)
  130 AB = DMAX1(AB,AM)
      W(J,1) = AB
      IF(JSCALE.GT.0)AB=AB*W(J,4)
  140 EPSN = DMAX1(EPSN,AB)
      EPSN = EPSN*EPS4
      IF((E.LE.ZERO).OR.(IENT.EQ.3))GO TO 160
C     MAKE COPY OF MATRIX
      DO 150 I=1,N
      DO 150 J=1,N
  150 W(I,J+5) = A(I,J)
C
C     FACTORISATION OF MATRIX INTO L*U, WHERE L IS A LOWER UNIT
C     TRIANGLE AND U IS UPPER TRIANGLE
  160 DO 230 L=1,N
      AM = ZERO
      II = L
C     EVALUATE  ELEMENTS IN PIVOTAL COLUMN.
      DO 170 I=L,N
      A(I,L)=A(I,L)-FM02AD(L-1,A(I,1),IA,A(1,L),1)
C     LOOK FOR MAXIMUM ELEMENT IN PIVOTAL COLUMN.
      AB =  DABS(A(I,L))
      IF(JSCALE .GT. 0)AB = AB*W(I,3)
      IF(AM .GE. AB)GO TO 170
      AM = AB
      II = I
  170 CONTINUE
C
C     TEST FOR SMALL OR ZERO PIVOT.
      AB = W(L,1)
      IF(AM .LE. EPS4*AB)IP=-L
      IF(AM.NE.ZERO)GO TO 180
      IF(IENT.EQ.2)GO TO 820
      IP=L
  180 IF(IENT.NE.3)GO TO 190
C
C     THE NEXT 12 STATEMENTS CALCULATE THE DETERMINANT OF MATRIX A.
C     TO PREVENT OVERFLOWS/UNDERFLOWS THE EXPONENTS OF THE NUMBERS
C     BEING MULTIPLIED ARE EXAMINED AND THE EXCESS EXPONENT IS STORED
C     IN IDET.
C FOR VAX NEXT TWELVE STATEMENTS REPLACED- MAY NEED ATTENTION IF MA21CD
C IS EVER NEEDED FOR BIG MATRICES.
C      Q(1)= DABS(DET)
C      Q(2)= DABS(A(II,L))
C      L3(4) = L1
C      L4(4) = L2
C      K = IDET+I3+I4-128
C      I3 = K
C      L2 = LQ
C      IF(IABS(K) .GT. 62)K=ISIGN(62,I3)
C      IDET = I3-K
C      I3 = K+64
C      L1 = L3(4)
C      DET =DSIGN(Q(1),DET)*DSIGN(Q(2),A(II,L))
      DET=DET*A(II,L)
C
  190 IF(II .EQ. L)GO TO 220
C
C     INTERCHANGE ROWS N AND II
C     INTERCHANGE EQUILIBRATION FACTORS
C     W(L,1)=II MEANS INTERCHANGE BETWEEN ROWS L AND II
      IF(IENT.EQ.3)DET=-DET
      IF(JSCALE .LE. 0)GO TO 200
      AA = W(L,3)
      W(L,3) = W(II,3)
      W(II,3) = AA
  200 DO 210 I=1,N
      AA = A(L,I)
      A(L,I) = A(II,I)
  210 A(II,I) = AA
  220 W(L,1) = WW
      IF( L .EQ. N)GO TO 240
      AA = ONE
      AC = A(L,L)
      IF( DABS(AC) .NE. ZERO)AA = AA/AC
      K= L+1
C     UPPER TRIANGLE
      DO 230 I=K,N
      A(I,L) = A(I,L)*AA
  230 A(L,I)=A(L,I)-FM02AD(L-1,A(L,1),IA,A(1,I),1)
C
  240 IF(IENT -2)250,500,720
C
  250 IF(E .LE. ZERO)GO TO 270
      DO 260 I=1,N
      W(I,5) = B(I)
  260 W(I,2) = ZERO
      IT = 0
C
C     FORWARD SUBSTITUTION
  270 DO 280 I=1,N
      WW = W(I,1)
      AA = B(II)
      B(II) = B(I)
  280 B(I)=AA-FM02AD(I-1,A(I,1),IA,B(1),1)
      ENORMA = ENORM
      ENORM = ZERO
C
C     CALCULATE NORMS OF X,CHANGE IN SOLUTION
C     BACKWARD SUBSTITUTION
      DO 310 K=1,N
      I=N+1-K
      AA = A(I,I)
      IF(   DABS(AA) .EQ. ZERO)GO TO 290
      AC=B(I)-FM02AD(N-I,A(I,I+1),IA,B(I+1),1)
      B(I) = AC/AA
      GO TO 300
  290 IP=I
      B(I) = ZERO
  300 AM=ONE
      IF(JSCALE.GT.0)AM=W(I,4)
  310 ENORM=DMAX1(ENORM, DABS(B(I))/AM)
      IF((E .LE. ZERO) .OR. (IP .NE. 0 ))GO TO 720
      IF(IT .EQ. 0)GO TO 320
      IF(ENORM .GT. P5*ENORMA) GO TO 460
      IF(ENORM   -  EPS*XNORM)470,470,330
  320 XNORM=ENORM
      CALL FA01CS(IRANDL,IRANDR)
      CALL FA01DS(21845,21845)
C
C     UPDATE SOLUTION VECTOR X
  330 DO 340 I=1,N
  340 W(I,2) = W(I,2)+B(I)
      IT = IT+1
C
C     COMPUTE RESIDUAL
  350 DO 450 J=1,N
      IF(IENT .NE. 2)GO TO 360
      AA = ZERO
      IF(L .EQ. J)AA = ONE
      GO TO 370
  360 AA = W(J,5)
  370 AC=AA-FM02AD(N,W(J,6),N,W(1,2),1)
      AA = ZERO
      IF(EA)400,430,380
C
C     MAKE PSEUDO RANDOM CHANGES TO ELEMENTS OF A AND B
  380 DO 390 K=1,N
  390 AA = AA+FA01AS(-K)*W(J,K+5)*W(K,2)
      GO TO 420
  400 DO 410 K=1,N
  410 AA = AA+FA01AS(-K)*W(K,2)
  420 AC =AC-DABS(EA)*AA
  430 IF(IENT .EQ. 2)GO TO 440
      AA  = DABS(EB)*FA01AS(-J)
      IF(EB .GE. ZERO)AA=AA*W(J,5)
      AC = AA+AC
      B(J) = AC
      GO TO 450
  440 W(J,5) = AC
  450 CONTINUE
      IF(IENT-2)270,630,270
  460 ENORM = ENORMA
  470 DO 480 I=1,N
  480 B(I) = W(I,2)
      E=ENORM
      IF(JSCALE .LE. 0)GO TO 710
C
C     SET UP ACCURACY ESTIMATES FOR SOLUTION VECTOR.
      ERR = ZERO
      DO 490 I=1,N
      W(I,2) = ENORM*W(I,4)
      AB = W(I,2)
  490 ERR = DMAX1(ERR,AB)
      E=ERR
      GO TO 710
C
C     OVERWRITE LU FACTORISATION OF A BY INVERSE OF PERMUTED A.
  500 IF(N .LT. 2)GO TO 520
      DO 510 I=2,N
      K=I-1
      DO 510 J=1,K
  510 A(I,J)=-A(I,J)-FM02AD(I-1-J,A(I,J+1),IA,A(J+1,J),1)
  520 DO 540 K=1,N
      I = N+1-K
      ERR = ONE/EPSN
      IF(JSCALE .GT. 0)ERR = ERR*W(I,4)
      DO 530 J=I,N
      W(J,2) = A(I,J)
  530 A(I,J) = ZERO
      A(I,I) = ONE
      AA = ONE/W(I,2)
      DO 540 J=1,N
      AB = ONE
      IF (JSCALE .GT. 0)AB = W(J,3)
      AC=A(I,J)-FM02AD(N-I,W(I+1,2),1,A(I+1,J),1)
      A(I,J) = AC*AA
      IF( DABS(A(I,J)) .GE. ERR*AB)IP=N*I+J
  540 CONTINUE
      ANORM = ZERO
      DO 590 K=1,N
      I = N+1-K
      WW = W(I,1)
      IF(II .EQ. I)GO TO 570
      IF(JSCALE .LE. 0)GO TO 550
      AA = W(I,3)
      W(I,3) = W(II,3)
      W(II,3) = AA
  550 DO 560 J=1,N
      AA = A(J,II)
      A(J,II) = A(J,I)
  560 A(J,I) = AA
      W(I,1) = W(II,1)
  570 ENORM = ZERO
      DO 580 J=1,N
      AB =  DABS(A(J,II))
      IF(JSCALE.GT.0)AB=AB/W(J,4)
  580 ENORM = DMAX1(ENORM,AB)
      W(II,1) = ENORM
      AB = ONE
      IF(JSCALE.GT.0)AB=W(II,3)
  590 ANORM = DMAX1(ANORM,ENORM/AB)
      IF((E .LE. ZERO) .OR. (IP .NE. 0 ))GO TO 720
       CALL FA01CS(IRANDL,IRANDR)
      CALL FA01DS(21845,21845)
      AXNORM=ANORM
  600 ANORMA = ANORM
      ANORM = ZERO
      L=0
  610 L=L+1
C
C     INVERSE OF A IS ITERATIVELY REFINED BY COLUMNS
C     MAKE COPY OF APPROPIATE COLUMN.
      DO 620 I=1,N
  620 W(I,2) = A(I,L)
      GO TO 350
C
C     CALCULATE THE CHANGE IN APPROPIATE COLUMN OF INVERSE OF A.
  630 ENORM = ZERO
      DO 640 I=1,N
      W(I,2) = ZERO
      W(I,2)=W(I,2)+FM02AD(N,A(I,1),IA,W(1,5),1)
      AM=ONE
      IF(JSCALE.GT.0)AM=W(I,4)
  640 ENORM=DMAX1(ENORM, DABS(W(I,2))/AM)
      AB = W(L,1)
      IF(ENORM .GT. P5*AB)GO TO 660
C
C     UPDATE APPROPIATE COLUMN OF INVERSE OF A.
      DO 650 J=1,N
  650 A(J,L) = A(J,L)+W(J,2)
      W(L,1) = ENORM
      AB = ENORM
  660 ERR = ONE
      IF(JSCALE .GT. 0)ERR = W(L,3)
      ANORM = DMAX1(ANORM,AB/ERR)
      IF(L.LT.N)GO TO 610
      IF(ANORM .GT. P5*ANORMA)GO TO 670
      IF(ANORM   -  EPS*AXNORM)680,680,600
  670 ANORM = ANORMA
  680 IF(JSCALE .LE. 0)GO TO 700
C
C     SET UP ACCURACY ESTIMATES FOR MATRIX INVERSE.
      ENORM = ZERO
      ERR = ZERO
      DO 690 I=1,N
      AB = ANORM*W(I,3)
      ENORM = DMAX1(ENORM,AB)
      W(I,1) = AB
      AB = W(I,4)
      ERR = DMAX1(ERR,AB)
  690 W(I,2) = AB
      E=ENORM*ERR
      GO TO 710
  700 E=ANORM
  710 CALL FA01DS(IRANDL,IRANDR)
      GO TO 850
C
      ENTRY MA21DD (A,IA,N,B,W,E)
      IENT = 4
      IF ( N .LE. 0)GO TO 810
C
C     CHECK WHETHER ON A PREVIOUS ENTRY A SMALL PIVOT WAS FOUND,IF
C     SO PUT ON ERROR FLAG.
      IP = 0
      WW = W(N,1)
      IF(II .GT. 0)GO TO 250
      IP = II
      I = -II
      II = N
      W(N,1) = WW
      GO TO 250
C
C     THE REMAINING INSTRUCTIONS HANDLE ERROR DIAGNOSTICS,ETC.
  720 IF(IP)730,800,740
  730 II=IP
      W(N,1) = WW
      J=7
      GO TO 770
  740 IF(IP.LE.N)GO TO 760
      I=(IP-1)/N
      J=IP-N*I
      WRITE(LP,750)DICT(IENT),I,J
  750 FORMAT(A8,'HAS FOUND THAT INVERSE ELEMENT (',I4,',',I4,') IS LARGE
     1,RESULTS MAY BE UNRELIABLE')
      GO TO 790
  760 J=8
  770 I=IABS(IP)
      WRITE(LP,780)DICT(IENT),DICT(6),I,DICT(J)
  780 FORMAT(A8,'HAS FOUND THAT',A8,I4,1X,A8,',RESULTS MAY',
     1  ' BE UNRELIABLE')
  790 E=-ONE
      GO TO 850
  800 E=ZERO
      GO TO 850
  810 WRITE(LP,830)DICT(IENT),DICT(5),N
      GO TO 840
  820 WRITE(LP,830)DICT(IENT),DICT(6),L,DICT(8 )
  830 FORMAT(' ERROR RETURN FROM',2A8,I5,1X,A8)
  840 E=-TWO
  850 RETURN
      END
C**********************************************************************
      SUBROUTINE MC10AD(A,N,NN,DIAG,RES,IS)
      REAL*8 A(NN,NN)
      REAL*4 RES(N,4)
      INTEGER*2DIAG(N,2),JU(2),KU
C     RES IS USED TO RETURN SCALING FACTORS AS INTEGRAL
C          POWERS OF BASE, AND AS WORKSPACE
C     IS IS SET TO 0 ON SUCCESSFUL COMPLETION, TO I IF ROW I HAS ONLY
C        ZERO ELEMENTS, TO -I IF COLUMN I HAS ONLY ZERO ELEMENTS
C     DIAG IS USED TO HOLD COUNTS OF NON-ZEROS IN ROWS AND COLUMNS
C      AND TO RETURN SCALING POWERS
C
      DATA SMIN/.01/
C     SMIN IS USED IN A CONVERGENCE TEST ON (RESIDUAL NORM)**2
      LOGICAL*1 IU,IW(3)
      EQUIVALENCE (UU,IW(1),KU),(U,IU,JU(1))
C     SET UP CONSTANTS
      write(6,950)
950	format(' entering MC10AD')
      UU = 100.
      IS=0
C     INITIALISE FOR ACCUMULATION OF SUMS AND PRODUCTS
      DO 2 L=1,2
      DO 2 I=1,N
      RES(I,L)=0.
      RES(I,L+2)=0.
    2 DIAG(I,L)=0
      DO 3 I=1,N
      DO 3 J=1,N
      U=DABS(A(I,J))
      IF(U.EQ.0.)GO TO 3
C     ON THE IBM 360 THE FOLLOWING TWO INSTRUCTIONS FIND THE SMALLEST
C     INTEGER GREATER THAN ALOG16(U).
      IW(2) = IU
      U=UU-64.
C     COUNT NON-ZEROS IN ROW AND COLUMN
      DIAG(I,1)=DIAG(I,1)+1
      DIAG(J,2)=DIAG(J,2)+1
      RES(I,1)=RES(I,1)+U
      RES(J,3)=RES(J,3)+U
    3 CONTINUE
C     COMPUTE RHS VECTORS TESTING FOR ZERO ROW OR COLUMN
      SSUM=0.
      J=0
      JU(1)=17920
      DO 8 I=1,N
      J=J+DIAG(I,1)
      DO 9 L=1,2
      IF(DIAG(I,L).GT.0 )GO TO 153
      DIAG(I,L)=1
      IS=I*(3-2*L)
  153 CONTINUE
C      ON IBM 360 NEXT INSTRUCTION SETS U TO VALUE OF POSITIVE INTEGER
      JU(2)=DIAG(I,L)
    9 RES(I,2*L-1)=RES(I,2*L-1)/U
    8 SSUM=SSUM+RES(I,3)
      SM=SMIN*J
C     SWEEP TO COMPUTE INITIAL RESIDUAL VECTOR
      RSUM=0.
      DO 110 I=1,N
      SUM = SSUM
      IF(DIAG(I,1).GE. N)GO TO 109
      SUM=0.
      DO 10 J=1,N
      IF(A(I,J).EQ.0D0)GO TO 10
      SUM=SUM+RES(J,3)
   10 CONTINUE
C      ON IBM 360 NEXT INSTRUCTION SETS U TO VALUE OF POSITIVE INTEGER
  109 JU(2)=DIAG(I,1)
      RES(I,1)=RES(I,1)-SUM/U
  110 RSUM=RSUM+RES(I,1)
C     INITIALISE ITERATION
      E=0.
      E1=0.
      Q=1.
      S=0.
      DO 11 I=1,N
C      ON IBM 360 NEXT INSTRUCTION SETS U TO VALUE OF POSITIVE INTEGER
      JU(2)=DIAG(I,1)
   11 S=S+U*RES(I,1)**2
      L=2
      IF(S.LE.SM)GO TO 100
C     ITERATION STEP
   20 EM=E*E1
C    SWEEP THROUGH MATRIX TO UPDATE RESIDUAL VECTOR
      DO 220 I=1,N
      SUM=RSUM
      IF(DIAG(I,L).GE. N)GO TO 220
      SUM=0.
      DO 22 J=1,N
      IF(L.EQ.2)GO TO 21
      IF(A(I,J))19,22,19
   21 IF(A(J,I))19,22,19
   19 SUM=SUM+RES(J,3-L)
   22 CONTINUE
  220 RES(I,L)=RES(I,L)+SUM
      S1=S
      S=0.
      RSUM=0.
      DO 23 I=1,N
      V=-RES(I,L)/Q
C      ON IBM 360 NEXT INSTRUCTION SETS U TO VALUE OF POSITIVE INTEGER
      JU(2)=DIAG(I,L)
      RES(I,L)=V/U
      RSUM=RSUM+RES(I,L)
   23 S=S+V*RES(I,L)
      E1=E
      E=Q*S/S1
      Q1=Q
      Q=1.-E
      M=3-L
      IF(S.GT.SM)GO TO 27
      E=M-1
      M=1
      Q=1.
   27 IF(L.EQ.2)GO TO 25
      QM=Q*Q1
      DO 24 I=1,N
      RES (I,4)=(EM*RES(I,4)+RES(I,2))/QM
   24 RES(I,3)=RES(I,3)+RES(I,4)
   25 L=M
      DO 26 I=1,N
C      ON IBM 360 NEXT INSTRUCTION SETS U TO VALUE OF POSITIVE INTEGER
      JU(2)=DIAG(I,L)
   26 RES(I,L)=RES(I,L)*U*E
      IF(S.GT.SM  )GO TO 20
C      SWEEP THROUGH MATRIX TO GET ROW SCALING POWERS
  100 DO 103 I=1,N
      DO 103 J=1,N
      U=DABS(A(I,J))
      IF(U.EQ.0.)GO TO 103
C      ON IBM 360 NEXT TWO INSTRUCTIONS FIND THE SMALLEST INTEGER
C     LESS THAN ALOG16(U)
      IW(2)=IU
      U=UU-64.
      RES(I,1)=RES(I,1)+RES(J,3)-U
  103 CONTINUE
C      CONVERT POWERS TO INTEGERS
      JU(1)=17920
      DO 104 I=1,N
C      ON IBM 360 NEXT INSTRUCTION SETS U TO VALUE OF POSITIVE INTEGER
      JU(2)=DIAG(I,1)
      V=RES(I,1)/U
      DIAG(I,1)=V+SIGN(0.5,V)
  104 DIAG(I,2)=-(RES(I,3)+SIGN(0.5,RES(I,3)))
C      SET SCALING FACTORS IN RES
      U=1.
      DO 105 L=1,2
      DO 105 I=1,N
      KU=DIAG(I,L)+65
      IU=IW(2)
  105 RES(I,L)=U
      RETURN
      END
C****************************************************************************
      SUBROUTINE MODINT(IN,JREFL,DETWIN,BB)
C
      INCLUDE 'merge.inc'
      PARAMETER (ERR=0.05)
      REAL*8  BB(1)
      DIMENSION ITWIN(4),ITWERR(4),DADZ(4)
      LOGICAL VALID,VAL1,VAL2
      LOGICAL DETWIN
C
      IF(.NOT.DETWIN) RETURN
      IF(BB(1).EQ.1.0) RETURN
C
      DO 100 II=1,IN
      I=II+JREFL-IN
C We are detwinning
        CALL GETITWIN(JH(I),JK(I),ZSTAR(I),
     $	  VAL1,VAL2,ITWIN,ITWERR,DADZ)
        VALID=VAL1.AND.VAL2
        IF(VALID) THEN
          AREF=0.0
          DO 60 J=2,4
            AREF=AREF+BB(J)*ITWIN(J)
60        CONTINUE
C  Intensity modified by subtracting proportion of three other twin types.
C   Note that intensities are obtained from curves
      	  RINT(I)=(RINT(I)-AREF)/BB(1)
C  Delta I's (errors) increased to reflect detwinning procedure.
C   We assume a 5% error in twin determination and add 5% of twin
C   types to error, via RMS addition.
      	  DIF1=FLOAT(ITWIN(1))**2+FLOAT(ITWIN(2))**2+
     1	       FLOAT(ITWIN(3))**2+FLOAT(ITWIN(4))**2
      	  DIF2=ERR*ERR*DIF1
      	  DIFF(I)=SQRT(DIFF(I)*DIFF(I)+DIF2)
        ELSE
      	  RINT(I)=0.0
          DIFF(I)=0.0
        ENDIF
100   CONTINUE
      RETURN
      END
C****************************************************************************
      SUBROUTINE CRDIFS(IN,JREFL,DETWIN,BB,TA_REFMT,WSTAR)
C
      INCLUDE 'merge.inc'
      REAL*8  BB(1)
      DIMENSION ITWIN(4),ITWERR(4),DADZ(4)
      LOGICAL VALID,VAL1,VAL2
      LOGICAL DETWIN,TA_REFMT
C
      DO 100 II=1,IN
      I=II+JREFL-IN
      IF(DETWIN) THEN
        CALL GETITWIN(JH(I),JK(I),ZSTAR(I),
     $	  VAL1,VAL2,ITWIN,ITWERR,DADZ)
        VALID=VAL1.AND.VAL2
        IF(VALID) THEN
          AREF=0.0
          DO 60 J=1,4
            AREF=AREF+BB(J)*ITWIN(J)
60        CONTINUE
          ICURVEV=AREF
      	ENDIF
      ELSE
        CALL GETCRVVAL(JH(I),JK(I),ZSTAR(I),
     $	  VALID,ICURVEV,ITWERR,DADZ,TA_REFMT,WSTAR)
      ENDIF
C  compute differences
      IF(VALID.AND.(ICURVEV.GE.0)) THEN
      	FCURVE=SQRT(FLOAT(ICURVEV))
C    Check for negative spots
        IF(RINT(I).LT.0) RINT(I)=0.0
 	RINT(I)=SQRT(RINT(I))-FCURVE
      ELSE
        RINT(I)=0.0
      ENDIF
C      DIFF(I)=0.0
100   CONTINUE
      RETURN
      END
C****************************************************************************
