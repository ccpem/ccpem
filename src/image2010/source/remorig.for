C*REMORIG.FOR***********************************************************
C	Remove origin peak in transform in order to correct slowly
C	varying background in image.  Uses inverted cosine bell
C	with elliptical shape specified by:
C		IXZERO   X zero for filter in X steps
C		IYZERO   Y zero for filter in Y steps
C	   Input transform on stream 1 (IN)
C	   Output weighted transform on stream 2 (OUT)
C
C     Note there are two FORMAT statements which mention version number.
C     statements 1000 and 1010 in printout and to write into output file
C	VX1.00 RAC 3.SEP.85	VAX version.
C       VX1.01 RH  28.May.87	Bigger dimensions
C       VX1.02 RH  23.Jul.94	Unix/Vax universal version
C       VX1.03 RH  6.Jan.01	check program array size
C       VX1.04 RH  11.Apr.06	include Terry's Linuxifications
C       VX1.05 JMS 25.jun.10    Jude's GFORTRAN mods
C
C*** jms 25.06.2010
	parameter (iadim = 5000)
C***      	PARAMETER (ADIM=5000)
	COMMON//NX,NY,NZ
	COMPLEX CARRAY(iadim)
      	REAL*8 FMEAN
	DIMENSION ARRAY(2*iadim),TITLE(20),NXYZ(3),MXYZ(3)
	CHARACTER DAT*24
CTSH++
      	CHARACTER*80 TMPTITLE
      	EQUIVALENCE (TMPTITLE,TITLE)
CTSH--
	EQUIVALENCE (NX,NXYZ),(ARRAY(1),CARRAY(1))
C
	WRITE(6,1000)
1000	FORMAT('1'///'  REMORIG vx1.04(11.APR.06):',
     .	 ' Remove origin peak in transform')
	ZERO=0.
	CALL IMOPEN(1,'IN','RO')
	CALL IMOPEN(2,'OUT','NEW')
	CALL FDATE(DAT)
	CALL IRDHDR(1,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
      	IF(NX.GT.iadim) STOP 'Program array dimension too small'
	CALL ICRHDR(2,NXYZ,MXYZ,MODE,TITLE,0)
	CALL ITRLAB(2,1)

CTSH  ENCODE(80,1010,TITLE) DAT(5:24)
CTSH++
      WRITE(TMPTITLE,1010) DAT(5:24)
CTSH--
1010	FORMAT(' REMORIG 1.04: Remove transform origin peak',10X,A20)
	CALL IWRHDR(2,TITLE,1,ZERO,ZERO,ZERO)
C
	READ(5,*) IXZERO,IYZERO
	WRITE(6,1020) IXZERO,IYZERO
1020	FORMAT(///'  Elliptical region removed has semi-axes :',
     *  2I5)
	NX1=NX-1
	NY2=NY/2
	NY21=NY2+1
	PIBY2=3.14159/2.
	FMIN=0.
	FMAX=0.
	FMEAN=0.
C
	DO 100 IY=1,NY
	JY=IY-NY21
	YFAC=FLOAT(JY)/FLOAT(IYZERO)
	CALL IRDPAL(1,ARRAY,0,NX1,*99)
C
	IF(IABS(JY).GT.IYZERO) GO TO 200
C
      	XSQ=1.00-FLOAT(JY*JY)/FLOAT(IYZERO*IYZERO)
      	IF(XSQ.LT.0.0) XSQ=0.0 ! sometimes rounding error
      	IF(XSQ.GT.0.0) XSQ=SQRT(XSQ)
	IXCUT = IXZERO*XSQ
C
	DO IX=1,IXCUT
		JX=IX-1
		XFAC=FLOAT(JX)/FLOAT(IXZERO)
		C=COS(PIBY2*(XFAC*XFAC+YFAC*YFAC))
		CARRAY(IX)=CARRAY(IX)*(1.-C*C)
	ENDDO
C
200	DO IX=1,NX
		F=CABS(CARRAY(IX))
      	 IF(F.LT.FMIN) FMIN=F
		IF(F.GT.FMAX) FMAX=F
		FMEAN=FMEAN+F
	ENDDO
C
	CALL IWRPAL(2,ARRAY,0,NX1)
100	CONTINUE
C
	F=FMEAN/(NX*NY)
	CALL IWRHDR(2,TITLE,-1,FMIN,FMAX,FMEAN)
	CALL IMCLOSE(1)
	CALL IMCLOSE(2)
	STOP
C
99	WRITE(6,2000)
2000	FORMAT(///'   End of file on stream 1')
	STOP
	END
