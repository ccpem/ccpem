C*INTERPO.FOR******************************************************
C
C	This is a general 2-D interpolation program that allows
C	the user to perform Rotations, Translations, Size
C	alterations and Re-sampling on a skewed or distorted
C	coordinate system.
C	It can also work on a stack of images and acrries out the
C	same transformation on each 2-D section.
C	All interpolation is done by the bi-quadratic method.
C	The rotation from OLD axis to NEW axis is positive for
C	an anti-clockwise rotation.
C	
C	V 1.0 17.01.92 to Alliant RH
C	V 1.1 08.03.95 remove bug in transformation matrices NIKO
C	V 1.2 15.11.00 link with imsubs2000   RAC
C	V 1.3 16.11.04 dimensions increase to 4096 * 4096 JMS
C	V 1.4 27.06.07 some improvements and debugs RH
C	V 2.0 01.07.07 major upgrade to take account of cell dimension permutation,
C	               applying the requested changes to the XYZ axes as specified
C	               by IUVW values in header of fast, medium and slow axes - RH.
C	V 2.1 09.08.07 output spacegroup and symmetry set to 0
C
C	Input parameters
C	  1.  ROTX		- requested rotation angle for fast axis
C	  2.  AXANG		- requested angle between new axes
C	  3.  AMAGx,AMAGy 	- magnification factors along fast and medium axes
C				  Note that these are not always X and Y
C	  4.  xCEN,yCEN		- centre for transformation along fast and medium
C				  axes.  Note that these are not always X and Y.
C	  5.  NXB,NYB		- size of output file in fast and medium directions
C				  Note that this will invalidate the effects of
C				  AMAGx, AMAGy unless the numbers are calculated
C				  correctly.
C
	COMMON//NXA,NYA,NZA,NXB,NYB,NZB,IFAST,IMED,ISLOW
	DIMENSION ARRAY(16777216),BRAY(16777216),AMAT(2,2)
	DIMENSION NXYZA(3),MXYZ(3),MXYZB(3),TITLE(20),NXYZB(3),AMAT2(2,2)
	DIMENSION CELL(6),NXYZST(3),ROT(2,2),T1(2,2),T2(2,2)
C*** jms 25.06.2010
        dimension iuvw(3)
        character ifastmedslow(3)
C***      	DIMENSION IUVW(3),IFASTMEDSLOW(3)
	EQUIVALENCE (NXA,NXYZA), (NXB,NXYZB), (IFAST,IUVW)
CTSH++
	CHARACTER TMPTITLE*80
	EQUIVALENCE (TMPTITLE,TITLE)
CTSH--
	DATA CNV/ .017453291/, NXYZST/3*0/
      	DATA IFASTMEDSLOW/'X','Y','Z'/
C
	WRITE(6,1000)
1000	FORMAT(//,' INTERPO (v2.1 - 9.8.07): ',
     .	'Generalized transformation program',/)
	CALL IMOPEN(1,'IN','RO')
	CALL IMOPEN(2,'OUT','NEW')
	CALL IRDHDR(1,NXYZA,MXYZ,MODE,DMIN,DMAX,DMEAN)
	CALL ITRHDR(2,1)
	CALL IRTCEL(1,CELL)
	CALL IRTUVW(1,IUVW)
	WRITE(6,1001) IUVW, IFASTMEDSLOW(IUVW(1)),IFASTMEDSLOW(IUVW(2)),
     .	  IFASTMEDSLOW(IUVW(3))
1001	FORMAT(' IMPORTANT change 1.7.2007, parameters now refer to axes',
     .	' as specified in map header',/'  IUVW =', 3I3/
     .	'  Fast, medium, slow axes ',3(2X,A1)/)
      	IF(IUVW(3).NE.3) THEN
      	 WRITE(6,1002)
1002		FORMAT(' INTERPO only works on 2D maps or stacks, ',
     .	  'Z must be slow axis'//)
      	 STOP
      	ENDIF
C
	NZB = NZA
	ROTX = 0.0
	AXANG = CELL(6)
	XT = 0.0
	YT = 0.0
	XCEN = NXA/2.
	YCEN = NYA/2.
	SCALE = 1.0
	TMIN =  1.E10
	TMAX = -1.E10
	TMEAN = 0.0
	AMAGX = 1.0
	AMAGY = 1.0
	ICELL = 0
C
	WRITE(6,1200) IFASTMEDSLOW(IUVW(1))
1200	FORMAT('$Enter rotation angle for fast (',A1,') axis [0.0]: ')
	READ(5,*) ROTX
	WRITE(6,1201) ROTX
1201	FORMAT(' Rotation angle was:',F10.3)
C
	WRITE(6,1300) AXANG
1300	FORMAT('$Enter angle between new axes [',F8.3,' ]: ')
	READ(5,*) AXANG
	WRITE(6,1301) AXANG
1301	FORMAT(' Angle between new axes was:',F10.3)
C
	WRITE(6,1400) IFASTMEDSLOW(IUVW(1)),IFASTMEDSLOW(IUVW(2))
1400	FORMAT('$Enter magnification factors along fast (',
     .	 A1,') and medium (',A1,') axes [1.0,1.0]: ')
C  larger value for magnification reduces the pixel size in Angstroms
	READ(5,*) AMAGX,AMAGY
	WRITE(6,1401) AMAGX,AMAGY
1401	FORMAT(' Magnification factors were:',2F10.3)
	NXB = NXA*AMAGX
	NYB = NYA*AMAGY
C
	WRITE(6,1500) IFASTMEDSLOW(IUVW(1)),IFASTMEDSLOW(IUVW(2)),XCEN,YCEN
1500	FORMAT('$Enter centre for transformation along fast (',
     .	 A1,') and medium (',A1,') axes [',2F7.1,' ]: ')
	READ(5,*) XCEN,YCEN
	WRITE(6,1501) XCEN,YCEN
1501	FORMAT(' Centre for transformation was:',2F10.3)
C
	WRITE(6,1600) IFASTMEDSLOW(IUVW(1)),IFASTMEDSLOW(IUVW(2)),NXB,NYB
1600	FORMAT('$Enter size of output file in fast and medium directions',
     .	 ' (N',A1,',N',A1,') [',2I5,']: ')
	READ(5,*) NXB,NYB
	WRITE(6,1601) NXB,NYB
1601	FORMAT(' Size of output map file was:',2I6)
C
	WRITE(6,1700)
1700	FORMAT('$Use original (0) or new (1) cell dimensions [0] : ')
	READ(5,*) ICELL
      	IF(ICELL.EQ.1) THEN
      	 WRITE(*,*)' New cell dimensions used'
      	ELSE
      	 WRITE(*,*)' Original cell dimensions used'
      	ENDIF
C
CTSH	ENCODE (80,2000,TITLE) ROTX,AXANG,XCEN,YCEN,AMAGX,AMAGY
CTSH++
	WRITE (TMPTITLE,2000) ROTX,AXANG,XCEN,YCEN,AMAGX,AMAGY
CTSH--
2000	FORMAT('INTERPO: Rot,Axang,XYcen,XYmag: ',4F8.2,2F8.3)
C
C  NEW HEADER
C
      	GAMMA = CELL(6)
	DX = CELL(IFAST)/MXYZ(IFAST)
	DY = CELL(IMED)/MXYZ(IMED)
	DXP = DX/AMAGX
	DYP = DY/AMAGY
	IF (ICELL .EQ. 1) THEN
	  CELL(IFAST) = DXP*NXB
	  CELL(IMED) = DYP*NYB
	  CELL(6) = AXANG
	END IF
      	WRITE(*,*)'CELL(fast,med,slow) =', CELL(IFAST), CELL(IMED),
     .	  CELL(ISLOW)
	CALL IALCEL(2,CELL)
C  the above statement changed only CELL(1), CELL(2), CELL(6)
C
C  note that the cell sampling MXYZ does refer to X, Y and Z, and not to
C  the fast, medium and slow axes, hence below.
	CALL IALSIZ(2,NXYZB,NXYZST)
      	MXYZB(1) = NXYZB(IUVW(1))
      	MXYZB(2) = NXYZB(IUVW(2))
      	MXYZB(3) = MXYZ(IUVW(3))
      	CALL IALSAM(2,MXYZB)
C
C alter spacegroup to 0 and symmetry bytes to 0
      	CALL IRTSYM(1,KSPG,KBS)
      	KSPG = 0
      	KBS = 0
      	CALL IALSYM(2,KSPG,KBS)
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
      	CALL IMPOSN(2,0,0)
C
C  SET UP CONVERSION MATRICES
C
	ROTX = CNV*ROTX
	AXANG = AXANG*CNV
	GAMMA = GAMMA*CNV
	CR = COS(ROTX)
	SR = SIN(ROTX)
	CA = COS(AXANG)
	SA = SIN(AXANG)
	CG = COS(GAMMA)
	SG = SIN(GAMMA)
C      		write(*,*)
C      		write(*,*)'Rotation matrices cos and sin values'
C      		write(*,*)'CR,SR', CR,SR
C      		write(*,*)'CA,SA', CA,SA
C      		write(*,*)'CG,SG', CG,SG
C  ROT is the matrix to rotate the orthogonal Angstrom input coordinates by
C  the required angle leaving the result still in orthogonal Angstroms
	ROT(1,1) = CR
	ROT(1,2) = SR
	ROT(2,1) = -SR
	ROT(2,2) = CR
C  T1 is the matrix to convert non-orthogonal input pixel coordinates into
C  orthogonal Angstroms in an (x,y*) axial frame
	T1(1,1) = DX
C	T1(1,2) = -DY*CG
	T1(1,2) = DY*CG
	T1(2,1) = 0.0
	T1(2,2) = DY*SG
C  T2 is the matrix to convert rotated orthogonal coordinates into the new
C  possibly non-orthogonal reference frame
	T2(1,1) = 1./DXP
C	T2(1,2) = CA/(DXP*SA)
	T2(1,2) = -CA/(DXP*SA)
	T2(2,1) = 0.0
	T2(2,2) = 1./(DYP*SA)
C
	CALL MM(AMAT,ROT,T1)
	CALL MM(AMAT2,T2,AMAT)
C
	DO 300 IZ = 1,NZB
	  CALL IRDSEC(1,ARRAY,*99)
C
	  CALL INTERP(ARRAY,BRAY,NXA,NYA,NXB,NYB,AMAT2,XCEN,YCEN,
     .	  XT,YT,SCALE,DXP,DYP,AXANG)
C
	  CALL ICLDEN(BRAY,NXB,NYB,1,NXB,1,NYB,DMIN,DMAX,DMEAN)
	  IF (DMIN .LT. TMIN) TMIN = DMIN
	  IF (DMAX .GT. TMAX) TMAX = DMAX
	  TMEAN = TMEAN + DMEAN
	  CALL IWRSEC(2,BRAY)
C
300	CONTINUE
	TMEAN = TMEAN/NZB
	CALL IWRHDR(2,TITLE,-1,TMIN,TMAX,TMEAN)
99	CALL IMCLOSE(1)
	CALL IMCLOSE(2)
      	WRITE(6,98)
98	FORMAT(/' INTERPO normal termination'///)
	STOP
	END
C******************************************************************************
C*MM  MULTIPLY 2X2 MATRICES
C
	SUBROUTINE MM(CMAT,AMAT,BMAT)
	DIMENSION AMAT(2,2),BMAT(2,2),CMAT(2,2)
C
	CMAT(1,1) = AMAT(1,1)*BMAT(1,1) + AMAT(1,2)*BMAT(2,1)
	CMAT(1,2) = AMAT(1,1)*BMAT(1,2) + AMAT(1,2)*BMAT(2,2)
	CMAT(2,1) = AMAT(2,1)*BMAT(1,1) + AMAT(2,2)*BMAT(2,1)
	CMAT(2,2) = AMAT(2,1)*BMAT(1,2) + AMAT(2,2)*BMAT(2,2)
	RETURN
	END
C*INTERP.FOR*******************************************************************
C
C	This subroutine will perform coordinate transformations
C	(rotations,translations, etc.) by quadratic interpolation.
C
C	BRAY = T[ ARRAY ]*scale
C
C	ARRAY	- The input image array
C	BRAY	- The output image array
C	NXA,NYA	- The dimensions of ARRAY
C	NXB,NYB	- The dimensions of BRAY
C	AMAT	- A 2x2 matrix to specify rotation,scaling,skewing
C	XC,YC	- The coordinates of the centre of ARRAY
C	XT,YT	- The translation to add to the final image. The
C		  center of the output array is normally taken as
C		  NXB/2, NYB/2
C	SCALE	- A multiplicative scale actor for the intensities
C	
C	Xo = a11(Xi - Xc) + a12(Yi - Yc) + NXB/2. + XT
C	Yo = a21(Xi - Xc) + a22(Yi - Yc) + NYB/2. + YT
C
C	Inside subroutine INTERP, the centre of the input array is XC,YC
C	and it is about this point that any rotations are performed.  This
C	point is also the centre of the area selected for output.  In the
C	output file, XCEN,YCEN is the equivalent point.
C
C
	SUBROUTINE INTERP(ARRAY,BRAY,NXA,NYA,NXB,NYB,AMAT,
     .	XC,YC,XT,YT,SCALE,DXP,DYP,AXANG)
	DIMENSION ARRAY(NXA,NYA),BRAY(NXB,NYB),AMAT(2,2)
	DATA CNV/ .017453291/

      	WRITE(6,10) NXA,NYA,NXB,NYB
10	FORMAT(/' Entering subroutine INTERP, NXA,NYA,NXB,NYB = ',4I6)
C
C   Calc inverse transformation
C
	XCEN = NXB/2. + XT + 1
	YCEN = NYB/2. + YT + 1
	XCO = XC + 1
	YCO = YC + 1
	DENOM = AMAT(1,1)*AMAT(2,2) - AMAT(1,2)*AMAT(2,1)
C      write(*,*) 'Value of DENOM in INTERP.FOR is',DENOM
	A11 =  AMAT(2,2)/DENOM
	A12 = -AMAT(1,2)/DENOM
	A21 = -AMAT(2,1)/DENOM
	A22 =  AMAT(1,1)/DENOM
C
C Loop over output image
C
	DO 200 IY = 1,NYB
	  DYO = IY - YCEN
	  DO 100 IX = 1,NXB
	    DXO = IX - XCEN
	    XP = A11*DXO + A12*DYO + XCO
	    YP = A21*DXO + A22*DYO + YCO
	    IXP = NINT(XP)
	    IYP = NINT(YP)
	    BRAY(IX,IY) = 0.0
	    IF (IXP .LT. 1 .OR. IXP .GT. NXA) GOTO 100
	    IF (IYP .LT. 1 .OR. IYP .GT. NYA) GOTO 100
C
C   Do quadratic interpolation
C
	    DX = XP - IXP
	    DY = YP - IYP
	    IXPP1 = IXP + 1
	    IXPM1 = IXP - 1
	    IYPP1 = IYP + 1
	    IYPM1 = IYP - 1
	    IF (IXPM1 .LT. 1) IXPM1 = 1
	    IF (IYPM1 .LT. 1) IYPM1 = 1
	    IF (IXPP1 .GT. NXA) IXPP1 = NXA
	    IF (IYPP1 .GT. NYA) IYPP1 = NYA
C
C	Set up terms for quadratic interpolation
C
	    V2 = ARRAY(IXP, IYPM1)
	    V4 = ARRAY(IXPM1, IYP)
	    V5 = ARRAY(IXP, IYP)
	    V6 = ARRAY(IXPP1, IYP)
	    V8 = ARRAY(IXP, IYPP1)
C
	    A = (V6 + V4)*.5 - V5
	    B = (V8 + V2)*.5 - V5
	    C = (V6 - V4)*.5
	    D = (V8 - V2)*.5
C
	    BRAY(IX,IY) = SCALE*(A*DX*DX + B*DY*DY + C*DX + D*DY + V5)
C      IF(IXP.EQ.1.AND.IYP.EQ.1) WRITE(*,*)' IXP,IYP,IX,IY',IXP,IYP,IX,IY
C      IF(IXP.EQ.301.AND.IYP.EQ.301) WRITE(*,*)' IXP,IYP,IX,IY',IXP,IYP,
C     .				IX,IY
C
100	  CONTINUE
200	CONTINUE
C
C calculate and output rotation test ******************************************
C all write statements commented out after debug
C      WRITE(6,203) AMAT(1,1),AMAT(1,2),AMAT(2,1),AMAT(2,2)
203   FORMAT(/' AMAT inside subroutine INTERP',4F10.4)
C      WRITE(6,204) A11,A12,A21,A22
204   FORMAT(' A11, A12, A21, A22 inside INTERP',4F10.4)
      DXO = 0.0
      DYO = 100.0
      XPY = A11*DXO + A12*DYO
      YPY = A21*DXO + A22*DYO
      DX1 = 100.0
      DY1 = 0.0
      XPX = A11*DX1 + A12*DY1
      YPX = A21*DX1 + A22*DY1
C      WRITE(6,205) XPY,YPY,XPX,YPX
205   FORMAT(' Test rotation angle of (0,Y) and (X,0)',4F10.2//)
C
      ANGLY = ATAN2(XPY*DXP*SIN(AXANG),
     .	  (YPY*DYP)-(XPY*DXP*COS(AXANG)))/CNV
      SCALY = SQRT((XPY*DXP)**2+(YPY*DYP)**2-
     .	 2.0*XPY*DXP*YPY*DYP*COS(AXANG))/(DYO*DYP)
C      WRITE(*,*) ' ANGLY, SCALY = ',ANGLY,SCALY
C
      ANGLX = ATAN2(YPX*DYP*SIN(AXANG),
     .                  (XPX*DXP)-(YPX*DYP*COS(AXANG)))/CNV
      SCALX = SQRT((XPX*DXP)**2+(YPX*DYP)**2-
     .          2.0*XPX*DXP*YPX*DYP*COS(AXANG))/(DX1*DXP)
C      WRITE(*,*) ' ANGLX, SCALX = ',ANGLX,SCALX
C end of test section *********************************************************
C
	RETURN
	END
C
C******************************************************************************
