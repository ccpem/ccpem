C*JOINSTK.FOR********************************************************
C                                                                   *
C       Program to join stacks of MAPFORMAT particles for single    *
C       particle analysis                                           *
C                                                                   *
C       Version 1.00    26-APR-99    JLR                            *
C********************************************************************
C        1         2         3         4         5         6        7
C23456789012345678901234567890123456789012345678901234567890123567890
C-------------------------------------------------
C  Declaration of variables
C-------------------------------------------------
      IMPLICIT NONE
C (Stack size info)
      INTEGER NXYZI,MXYZI,MODEI
     .       ,NXYZO,MXYZO,MODEO
     .       ,NXYZST
     .       ,NDIM
C (Some counters)
     .       ,IA
     .       ,MAIN
      PARAMETER (NDIM=100000)
C (Image statistics)
      REAL*4 DMINI,DMAXI,DMEANI
     .      ,DMINO,DMAXO,DMEANO
      REAL*4 ARRAY
C (Integer dimensions and character strings)
      DIMENSION NXYZI(3),MXYZI(3),MODEI(1)
     .         ,NXYZO(3),MXYZO(3),MODEO(1),NXYZST(3)
     .         ,ARRAY(NDIM)
      CHARACTER*80 INFILE,OUTFILE
      CHARACTER*24 DAT
      CHARACTER*85 TITLE
C-------------------------------------------------
C  Get output file name
C-------------------------------------------------
      WRITE(6,'(''  JOINSTK V1.00: join MAPFORMAT stacks'')')
      WRITE(6,'(''  Output filename?'')')
      READ(5,'(A)') OUTFILE
C------------------------------------------------
C  Start the file input loop Open the input files
C------------------------------------------------
      DO MAIN=1,NDIM
      WRITE (6,*) '  Input filename?',MAIN,' ("*" to stop)'
      READ(5,'(A)') INFILE
      IF (INFILE.EQ.'*') THEN
        CALL IMCLOSE(2)
        STOP '  finished joining stacks'
      ELSE
      CALL IMOPEN(1,INFILE,'RO')
      CALL IRDHDR(1,NXYZI,MXYZI,MODEI,DMINI,DMAXI,DMEANI)
C------------------------------------------------
C Set the particle dimensionss of OUTFILE to be the
C same as the first INFILE
C adjust the number of layers in OUTFILE
C Set OUTFILE Stats
C------------------------------------------------
      IF(MAIN.EQ.1) THEN
        NXYZO(1)=NXYZI(1)
        NXYZO(2)=NXYZI(2)
        NXYZO(3)=NXYZI(3)
        WRITE (6,*) '  Output file will contain',NXYZO(3),'sections'
        WRITE (6,*) '  Sections are',NXYZO(1),'x',NXYZO(2)
        DMINO=DMINI
        DMAXO=DMAXI
        DMEANO=DMEANI
      ELSE
        DMINO = MIN(DMINO,DMINI)
        DMAXO = MAX(DMAXO,DMAXI)
        DMEANO=((DMEANO*NXYZO(3))+(DMEANI*NXYZI(3)))
     .         /(NXYZO(3)+NXYZI(3))
        NXYZO(3)=NXYZO(3)+NXYZI(3)
        WRITE (6,*) '  Output file will contain',NXYZO(3),' sections'
      ENDIF
C------------------------------------------------
C  Check that particles are of the same size
C------------------------------------------------
      IF(NXYZI(1).EQ.NXYZO(1).AND.NXYZI(2).EQ.NXYZO(2)) THEN
            WRITE (6,*) '  Particle sizes in input files match'
      ELSE
            STOP '  Particle sizes in input files are different'
      ENDIF
C------------------------------------------------
C  Make the header or update the header
C  Get date
C------------------------------------------------
      IF(MAIN.EQ.1) THEN
        CALL IMOPEN(2,OUTFILE,'NEW')
        CALL FDATE(DAT)
        TITLE = ' JOINSTK: Joined MAPFORMAT Files '
        WRITE (TITLE(63:82),'(A20)') DAT(5:24)
        WRITE (TITLE(35:60),'(A20)') INFILE
        CALL ICRHDR(2,NXYZO,MXYZO,2,TITLE,1)
        CALL IWRHDR(2,TITLE,-1,DMINO,DMAXO,DMEANO)
      ELSE
        CALL FDATE(DAT)
        TITLE = '  '
        WRITE (TITLE(63:82),'(A20)') DAT(5:24)
        WRITE (TITLE(35:60),'(A20)') INFILE
        CALL IALSIZ(2,NXYZO,NXYZST)
        CALL IWRHDR (2,TITLE,1,DMINO,DMAXO,DMEANO)
      ENDIF
C------------------------------------------------
C Datatransfer loop
C------------------------------------------------
      CALL IMPOSN(1,0,0)
      CALL IMPOSN(2,NXYZO(3)-NXYZI(3),0)
      DO IA = 1,NXYZI(3)
        CALL IRDSEC(1,ARRAY,*999)
        CALL IWRSEC(2,ARRAY)
      END DO
      WRITE (6,*) '  Transfered',NXYZI(3),'  sections'
C------------------------------------------------
C  Close off the current input file
C------------------------------------------------
      CALL IMCLOSE(1)
      ENDIF
      END DO
999   STOP '  *end of file read error*'
      END
