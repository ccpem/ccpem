PROGRAM Density
!
!  DENSITY: given a 3D MAPFORMAT structure, molecular mass and 
!           pixel dimensions, DENSITY will output the nescessary
!           density cuttoff for a surface representation of the
!           structure
!
!  Version 1.00, 6 May 1999 JLR
!
!-------------------------------------------------
! Declaration of variables
!-------------------------------------------------


IMPLICIT NONE
CHARACTER(80)            :: infile
INTEGER                  :: nxyz(3),mxyz(3),mode,voxel,sec,nvoxels,totvoxels
REAL                     :: dmin,dmax,dmean
REAL                     :: weight, vvolume,volume,weightc,dense,step,minstep
INTEGER, PARAMETER       :: ndim = 10000
REAL,    PARAMETER       :: protdensity = 0.81
REAL, DIMENSION(ndim)    :: array


! The partial specific volume is taken as 0.81Da/A^3
! As used in Grigorieff 1998 and based on
! Matthews, 1968
! J. Mol. Biol. 33, 491-497


!-------------------------------------------------
! Get input file name
!-------------------------------------------------
WRITE (*,*) "DENSITY V1.00: determine density cutoff"
WRITE (*,*) "Input file name?"
READ  (*,*) infile



!-------------------------------------------------
! Open INFILE and get parameters   
!-------------------------------------------------
CALL Imopen(1,infile,'RO')
CALL Irdhdr(1,nxyz,mxyz,mode,dmin,dmax,dmean)


!-------------------------------------------------
! Input molecular mass, volume
!-------------------------------------------------
write(*,*) "Molecular mass of reconstruction (kDa)?"
READ (*,*) weight
WRITE (*,*) "Volume of voxel (A^3)?"
READ (*,*) vvolume

!-------------------------------------------------
! Calculate required number of voxels 
!-------------------------------------------------
totvoxels = (weight*1000)/(vvolume*protdensity) 
WRITE (*,*) "reconstruction should include",totvoxels," voxels"


!-------------------------------------------------
! Determine required density cutoff
!
! assumes structural information is present between 
! 0 and dmax (not dmin and dmax)
!------------------------------------------------- 

dense = 0.0
step  = 0.1


Changedense: DO WHILE(step>0.000001)
  Section: DO sec=0, nxyz(3)-1
    CALL Imposn(1,sec,0)
    CALL Irdsec(1,array,*999)
    Eachvoxel: DO voxel=0,((nxyz(1)+1)*(nxyz(2)+1))-1
      IF (array(voxel)>=dense) nvoxels=nvoxels+1
    END DO Eachvoxel
  END DO Section
  IF (nvoxels<totvoxels) THEN
    dense = dense - step 
    step = step*0.1
    nvoxels = 0
  ELSE IF (nvoxels==totvoxels) THEN
    EXIT
  ELSE
    nvoxels = 0  
    dense = dense + step
  END IF
END DO Changedense



weightc=(nvoxels*vvolume*protdensity)/1000
WRITE (*,*) "in order to have a mass of",weightc," kDa"
WRITE (*,*) "the surface must include",nvoxels," voxels"
WRITE (*,*) "by using a density cuttoff of",dense  


!-------------------------------------------------
! Close off input file
!-------------------------------------------------
CALL Imclose(1)


!-------------------------------------------------
! End
!-------------------------------------------------
 STOP " normal end"
 999  STOP " end of file error"
 END PROGRAM Density





