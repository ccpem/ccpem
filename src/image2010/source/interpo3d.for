C*INTERPO3D.FOR****************************************************************
C
C	This is a 3D interpolation program.
C	The program allows the user to perform Translations,
C	Size alterations, Re-sampling and Rescaling on 3D maps.
C	All interpolation is done by the tri-linear method.
C
C	The output map file is always centred at pixel NXB/2,NYB/2,NZB/2
C	e.g. the central pixel would be 64, on a map with 128 pixels, when
C	  numbered from 0 to 127.
C	The input map coordinate XCEN,YCEN,ZCEN is repositioned to be at the
C	  above central position in the output map.  This also acts as the point
C	  about which the magnification or demagnification transformation occurs.
C	  XCEN,YCEN,ZCEN are also numbered from 0.
C	A magnification greater than 1.0 means the pixels in the new map then
C	  correspond to a smaller distance in Angstroms.
C	
C	15.4.99 created by extension of INTERPO to do 3D maps
C	13.6.99 TMEANOUT added
C	11.7.99 NXYZST carried over from input file
C	24.8.99 debug (IZP.GE.NZA) to (IZP.GT.NZA) - writes last line
C	21.2.00 debug IRMAX & increase size, plus several ZCEN,NZB,ZT,ZCO
C	 3.9.01 add clearer explanation here and in title output
C
C   Input cards -
C
C	  1. AMAGX,AMAGY,AMAGZ magnification factors along X,Y,Z
C	  2. XCEN,YCEN,ZCEN center for transformation
C	  3. NXB,NYB,NZB size of output file (NX,NY,NZ)
C
C------------------------------------------------------------------------------
C
	INTEGER*4  IDMAX
      	PARAMETER  (IDMAX=400)
	INTEGER*4  IRMAX
	PARAMETER  (IRMAX=25*IDMAX**2)
	INTEGER*4  IAMAX
	PARAMETER  (IAMAX=IDMAX**3)
	COMMON//NXA,NYA,NZA,NXB,NYB,NZB
	CHARACTER TITLE*80
	REAL*4   AIN(IRMAX)
	REAL*4  AMAT(3,3)
	REAL*4   ARRAY(IAMAX)
	REAL*4   BOUT(IRMAX)
	REAL*4   BRAY(IAMAX)
	REAL*4  CELL(6)
	INTEGER*4  NXYZA(3)
	INTEGER*4  NXYZB(3)
	INTEGER*4  NXYZST(3)
	INTEGER*4  MXYZ(3)
c	DIMENSION ARRAY(IAMAX),BRAY(IAMAX),AIN(IRMAX),BOUT(IRMAX)
c	DIMENSION NXYZA(3),MXYZ(3),TITLE(20),NXYZB(3)
c	DIMENSION CELL(6),NXYZST(3),AMAT(3,3)
	EQUIVALENCE (NXA,NXYZA), (NXB,NXYZB)
      	DOUBLE PRECISION TMEAN
C
	WRITE(6,1000)
1000	FORMAT(//,' INTERPO3D: Generalized transformation program 3.9.01',
     .	 /)
	CALL IMOPEN(1,'IN','RO')
	CALL IMOPEN(2,'OUT','NEW')
	CALL IRDHDR(1,NXYZA,MXYZ,MODE,DMIN,DMAX,DMEAN)
      	IF(NXA*NYA*NZA.GT.IAMAX) STOP 'Array ARRAY dimensions too small'
      	IF(NXA*NYA.GT.IAMAX) STOP 'Array AIN dimensions too small'
      	CALL IRTSYM(1,KSPG,KBS)
	CALL ITRHDR(2,1)
	CALL IRTCEL(1,CELL)
      	CALL IRTSIZ(1,NXYZA,MXYZ,NXYZST)
C
	XT = 0.0
	YT = 0.0
	ZT = 0.0
	XCEN = NXA/2.
	YCEN = NYA/2.
	ZCEN = NZA/2.
	SCALE = 1.0
	TMIN =  1.E10
	TMAX = -1.E10
	TMEAN = 0.0
	AMAGX = 1.0
	AMAGY = 1.0
	AMAGZ = 1.0
C
C Read the three input cards
C
	WRITE(6,1400) AMAGX,AMAGY,AMAGZ
1400	FORMAT('$Enter magnification factors along X,Y,Z [',3F7.3,']: ')
	READ(5,*) AMAGX,AMAGY,AMAGZ
C
	WRITE(6,1500) XCEN,YCEN,ZCEN
1500	FORMAT('$Enter X,Y,Z center for transformation [',3F7.1,' ]: ')
	READ(5,*) XCEN,YCEN,ZCEN
C
	NXB = NXA*AMAGX
	NYB = NYA*AMAGY
	NZB = NZA*AMAGZ
	WRITE(6,1600) NXB,NYB,NZB
1600	FORMAT('$Enter size of output file (NX,NY,NZ) [',3I4,' ]: ')
	READ(5,*) NXB,NYB,NZB
C
      	WRITE(6,1601)AMAGX,AMAGY,AMAGZ,XCEN,YCEN,ZCEN,NXB,NYB,NZB
1601	FORMAT( ' AMAGX,AMAGY,AMAGZ....=',3F8.4/
     .	 ' XCEN,YCEN,ZCEN.......=',3F8.1/
     .	 ' NXB,NYB,NZB..........=',3I8)
C
      	IF(NXB*NYB*NZB.GT.IAMAX) STOP 'Array BRAY dimensions too small'
      	IF(NXB*NYB.GT.IRMAX) STOP 'Array BOUT dimensions too small'
	WRITE(TITLE,2001) XCEN,YCEN,ZCEN,AMAGX,AMAGY,AMAGZ
C++jms	ENCODE (80,2001,TITLE) XCEN,YCEN,ZCEN,AMAGX,AMAGY,AMAGZ
2001	FORMAT('INTERPO3D: from old centre',3F7.2,'   magnified by',3F6.3)
C
C  NEW HEADER
C
	DX = CELL(1)/MXYZ(1)
	DY = CELL(2)/MXYZ(2)
	DZ = CELL(3)/MXYZ(3)
	DXP = DX/AMAGX
	DYP = DY/AMAGY
	DZP = DZ/AMAGZ
C use new cell dimensions
	  CELL(1) = DXP*NXB
	  CELL(2) = DYP*NYB
	  CELL(3) = DZP*NZB
      	 NXYZST(1)=(NXYZST(1)*NXB)/NXA
      	 NXYZST(2)=(NXYZST(2)*NYB)/NYA
      	 NXYZST(3)=(NXYZST(3)*NZB)/NZA
	CALL IALSIZ(2,NXYZB,NXYZST)
	CALL IALCEL(2,CELL)
      	CALL IALSYM(2,KSPG,KBS)
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
C  SET UP CONVERSION MATRIX
C
	AMAT(1,1) = AMAGX
	AMAT(1,2) = 0.0
	AMAT(1,3) = 0.0
	AMAT(2,1) = 0.0
	AMAT(2,2) = AMAGY
	AMAT(2,3) = 0.0
	AMAT(3,1) = 0.0
	AMAT(3,2) = 0.0
	AMAT(3,3) = AMAGZ
C
      DO 300 IZ=1,NZA
c      write(*,*) ' reading section ',IZ
      	CALL IRDSEC(1,AIN,*99)
c      write(*,*) ' finished section ',IZ
      	DO 200 IXY=1,NXA*NYA
      	INDEX=IXY+(IZ-1)*NXA*NYA
200	ARRAY(INDEX)=AIN(IXY)
300   CONTINUE
C
      CALL INTERP3D(ARRAY,BRAY,NXA,NYA,NZA,NXB,NYB,NZB,AMAT,
     .	 XCEN,YCEN,ZCEN,XT,YT,ZT,SCALE)

      DO 800 IZ=1,NZB
      	DO 700 IXY=1,NXB*NYB
      	INDEX=IXY+(IZ-1)*NXB*NYB
	VAL=BRAY(INDEX)
      	BOUT(IXY)=VAL
      	TMEAN=TMEAN+VAL
	IF(VAL.GT.TMAX)TMAX=VAL
700	IF(VAL.LT.TMIN)TMIN=VAL
      	CALL IWRSEC(2,BOUT)
800   CONTINUE
	TMEANOUT = TMEAN/(NXB*NYB*NZB)
C
C
	CALL IWRHDR(2,TITLE,-1,TMIN,TMAX,TMEANOUT)
	CALL IMCLOSE(1)
	CALL IMCLOSE(2)
	STOP 'Normal termination'
99	STOP 'Read error on input file'
	END
C
C*INTERP3D.FOR***************************************************
C
C	BRAY = T[ ARRAY ]*scale
C
C	ARRAY		- The input image array
C	BRAY		- The output image array
C	NXA,NYA,NZA	- The dimensions of ARRAY
C	NXB,NYB,NZB	- The dimensions of BRAY
C	AMAT		- A 3x3 matrix to specify rescaling
C	XC,YC,ZC	- The coordinates of the Center of ARRAY
C	XT,YT,ZT	- The translation to add to the final image. The
C			  center of the output array is normally taken as
C			  NXB/2, NYB/2
C	SCALE		- A multiplicative scale actor for the intensities
C	
C	Xo = a11(Xi - Xc) + a12(Yi - Yc) + a13(Zi - Zc) + NXB/2. + XT
C	Yo = a21(Xi - Xc) + a22(Yi - Yc) + a23(Zi - Zc) + NYB/2. + YT
C	Zo = a31(Xi - Xc) + a32(Yi - Yc) + a33(Zi - Zc) + NZB/2. + ZT
C
      SUBROUTINE INTERP3D(ARRAY,BRAY,NXA,NYA,NZA,NXB,NYB,NZB,AMAT,
     .	   XC,YC,ZC,XT,YT,ZT,SCALE)
      DIMENSION ARRAY(NXA,NYA,NZA),BRAY(NXB,NYB,NZB),AMAT(3,3)
C
C   Calc inverse transformation
C
	XCEN = NXB/2. + XT + 1.
	YCEN = NYB/2. + YT + 1.
	ZCEN = NZB/2. + ZT + 1.
	XCO = XC + 1.
	YCO = YC + 1.
	ZCO = ZC + 1.
	A11 =  1.0/AMAT(1,1)
      	A12 =  0.0
      	A13 =  0.0
      	A21 =  0.0
	A22 =  1.0/AMAT(2,2)
      	A23 =  0.0
      	A31 =  0.0
      	A32 =  0.0
	A33 =  1.0/AMAT(3,3)
C
C Loop over output image
C
      DO 300 IZ = 1,NZB
        DZO = IZ - ZCEN
	DO 200 IY = 1,NYB
	  DYO = IY - YCEN
	  DO 100 IX = 1,NXB
	    DXO = IX - XCEN
	    XP = A11*DXO + A12*DYO + A13*DZO + XCO
	    YP = A21*DXO + A22*DYO + A23*DZO + YCO
	    ZP = A31*DXO + A32*DYO + A33*DZO + ZCO
	    IXP = XP
	    IYP = YP
	    IZP = ZP
	    BRAY(IX,IY,IZ) = 0.0
	    IF (IXP .LT. 1 .OR. IXP .GT. NXA) GOTO 100
	    IF (IYP .LT. 1 .OR. IYP .GT. NYA) GOTO 100
	    IF (IZP .LT. 1 .OR. IZP .GT. NZA) GOTO 100
C
C   Do trilinear interpolation
C
	    DX = XP - IXP
	    DY = YP - IYP
	    DZ = ZP - IZP
	    IXPP1 = IXP + 1
	    IYPP1 = IYP + 1
	    IZPP1 = IZP + 1
      	    IF(IXPP1.GT.NXA) IXPP1=IXP
      	    IF(IYPP1.GT.NYA) IYPP1=IYP
      	    IF(IZPP1.GT.NZA) IZPP1=IZP
C
C	Set up terms for trilinear interpolation
C
	    V1 = ARRAY(IXP,   IYP,   IZP)
	    V2 = ARRAY(IXPP1, IYP,   IZP)
	    V3 = ARRAY(IXP,   IYPP1, IZP)
	    V4 = ARRAY(IXPP1, IYPP1, IZP)
	    V5 = ARRAY(IXP,   IYP,   IZPP1)
	    V6 = ARRAY(IXPP1, IYP,   IZPP1)
	    V7 = ARRAY(IXP,   IYPP1, IZPP1)
	    V8 = ARRAY(IXPP1, IYPP1, IZPP1)
C
	    A = DX*DY*V4 + (1.-DX)*DY*V3 + DX*(1.-DY)*V2 + (1.-DX)*(1.-DY)*V1
	    B = DX*DY*V8 + (1.-DX)*DY*V7 + DX*(1.-DY)*V6 + (1.-DX)*(1.-DY)*V5
C
	    BRAY(IX,IY,IZ) = SCALE*(B*DZ + A*(1.-DZ))
C
100	  CONTINUE
200	CONTINUE
300   CONTINUE
C
	RETURN
	END
