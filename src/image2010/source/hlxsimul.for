C*HLXSIMUL.FOR***********************************************************
C     Program to simulate helix for program testing
C          Version 1.0 15NOV82     RAC     FOR VAX
C          Version 1.1 26JAN83     Mods to put in multiple blobs per asym. unit
C          Version 1.2 26APR84     Centre in y and tilt about middle of helix
C          Version 1.3 21SEP84     Front and back cut offs with ramp weighting
C          Version 1.4 11SEP91     Converted to Alliant by RB
C          Version 2.0 10NOV00     RAC Converted to IMAGE2000
C
      COMMON//NX,NY,NZ
      DIMENSION ARRAY(256,512),TITLE(20),NXYZ(3),MXYZ(3),ALINE(256)
      CHARACTER DAT*24
      EQUIVALENCE (NX,NXYZ,MXYZ)
CTSH++
	CHARACTER*80 TMPTITLE
	EQUIVALENCE (TMPTITLE,TITLE)
CTSH--
      DATA CRAD/0.0174532/
C
      NXD=256
      NY=512
      NY2=NY/2
      NZ=1
      WRITE(6,1000)
1000  FORMAT('1',///'   HLXSIMUL : PROGRAM TO SIMULATE HELIX')
      CALL IMOPEN(1,'OUT','NEW')
      READ(5,*) NBLOB,NUNIT,AXIS,DZ,DPHI,PHIROT,TILT,PZBACK,PZFRNT,
     1 IRAMP
      WRITE(6,1010) NBLOB,NUNIT,AXIS,DZ,DPHI,PHIROT,TILT,PZBACK,PZFRNT,
     1 IRAMP
1010  FORMAT(///'  Number of blobs          ',I10,
     1         /'  Number of subunits       ',I10,
     2         /'  X position of axis       ',F10.1,' pixels',
     3         /'  Z rise per subunit       ',F10.2,' pixels',
     4         /'  Phi twist per subunit    ',F10.2,' degrees',
     5         /'  Phi rotn of whole helix  ',F10.2,' degrees',
     6         /'  Tilt of axis out of plane',F10.2,' degrees',
     7         /'  Back cut off             ',F10.2,' pixels',
     8         /'  Front cut off            ',F10.2,' pixels',
     9         /'  Ramp or cut  1/0         ',I10)
C
      DPHI=DPHI*CRAD
      TILT=TILT*CRAD
      NX=256
      NBYTE=NXD*NY
      CALL CCPZI(ARRAY,NBYTE)
      CALL CCPZI(ALINE,256)
C
      DO 110 NB=1,NBLOB
      READ(5,*) RADH,RADS,Z0,PHI0,WT
      WRITE(6,1015) NB,RADH,RADS,Z0,PHI0,WT
1015  FORMAT(///'  Blob number           ',I10,
     1         /'  Radius from axis      ',F10.1,' pixels',
     2         /'  Radius of blob        ',F10.1,' pixels',
     3         /'  Starting Z            ',F10.1,' pixels',
     4         /'  Starting phi          ',F10.1,' degrees'
     5         /'  Weight                ',F10.1)
      PHI0=(PHI0+PHIROT)*CRAD
C     Put centre of helix at middle of plotted length
      Z=Z0-0.5*NUNIT*DZ
      PHI=PHI0
C
      DO 100 NU=1,NUNIT
C     Plotter coordinates of centre of subunit
C     Centre of helix at NY/2
      PX=RADH*SIN(PHI)+AXIS
      PY=Z*COS(TILT)-RADH*COS(PHI)*SIN(TILT)+NY2
      ZWT=WT
C     If back and front zero, do not apply cut off
      IF((PZBACK.EQ.0.).AND.(PZFRNT.EQ.0.)) GO TO 95
C     Work out plotter Z of sphere centre and plot only if in range
      PZ=Z*SIN(TILT)+RADH*COS(PHI)*COS(TILT)
      IF(PZ.GT.PZFRNT.OR.PZ.LT.PZBACK) GO TO 96
      IF(IRAMP.EQ.1) ZWT=WT*(PZ-PZBACK)/(PZFRNT-PZBACK)
      IF(IRAMP.EQ.-1) ZWT=WT*(PZFRNT-PZ)/(PZFRNT-PZBACK)
95    CALL SPHERE(ARRAY,PX,PY,RADS,ZWT)
96    Z=Z+DZ
      PHI=PHI+DPHI
100   CONTINUE
110   CONTINUE
C
      MODE=1
      CALL ICRHDR(1,NXYZ,MXYZ,MODE,TITLE,0)
C     Set origin for transform
      ORX=AXIS
      ORY=NY2
      ORZ=0
      CALL IALORG(1,ORX,ORY,ORZ)
      CALL FDATE(DAT)
CTSH      ENCODE(80,1020,TITLE) DAT(5:24)
CTSH++
      WRITE(TMPTITLE,1020) DAT(5:24)
CTSH--
1020  FORMAT(' HLXSIMUL : Helix simulation',9X,A20)
      CALL ICLDEN(ARRAY,NXD,NY,1,NX,1,NY,DMIN,DMAX,DMEAN)
      CALL IWRHDR(1,TITLE,1,DMIN,DMAX,DMEAN)
      DO 150 IY=1,NY
      DO 160 IX=1,NX
160   ALINE(IX)=ARRAY(IX,IY)
150   CALL IWRLIN(1,ALINE)
      WRITE(6,1030) DMIN,DMAX,DMEAN
1030  FORMAT(///'  MIn max and mean density in image',3F10.1)
      CALL IMCLOSE(1)
      STOP
      END
C
C     Subroutine to add projected density of sphere centered at
C     PX,PY to ARRAY
      SUBROUTINE SPHERE(ARRAY,PX,PY,RADS,WT)
      DIMENSION ARRAY(256,512)
      DEN=10.*WT
      I1=PX-RADS
      I2=PX+RADS
      J1=PY-RADS
      J2=PY+RADS
      RADSQ=RADS*RADS
      DO 100 J=J1,J2
      Y=J-PY
      YSQ=Y*Y
      DO 100 I=I1,I2
      X=I-PX
      XSQ=X*X
      RSQ=XSQ+YSQ
      IF(RSQ.GT.RADSQ) GO TO 100
      ARRAY(I+1,J+1)=ARRAY(I+1,J+1)+2.*DEN*SQRT(RADSQ-RSQ)
100   CONTINUE
      RETURN
      END
