C*HLXSEARCH	Version 1.0				RA
C               Version 2.0  imsubs2000    12NOV00      RAC
C		Version 2.1  more layer lines 01.08.2012 jms
C
C     Tilt & shift search program for helical particles
C
C      DATA input on unit 5
C
C      1)  TITLE (20A4)
C
C      2)  OMEGAM,DOMEGA,NOMEGA,EXM,DEX,NEX,NNLAY,NOP,IDIM1,IDIM2 (free format)
C
C          OMEGAM,DOMEGA,NOMEGA
C             Tilt angle for searching:
C                                starting angle in degrees
C                                increment in degrees
C                                no. of points in tilt search
C
C          EXM,DEX,NEX
C             X shift for searching:
C                                starting X shift in angstroms
C                                increment in angstroms
C                                no. of points in X shift search
C
C          NNLAY
C             no. of different selection rules(Bessels)to be tried
C             up to 28
C
C          NOP
C             no. of pairs of reflections (le. 50)
C
C          IDIM1,IDIM2
C             transform dimensions
C
C     3a) Y(I),D(I),F(I),DPHI(I)  (free format)
C                                  NOP of these pairs of cards
C
C          Y    layer line spacing in transform Y grid steps
C
C          D    spacing between reflection pair in transform X steps
C
C          F    average value of amplitude(arbitrary scale) for the
C               pair of reflections
C
C          DPHI (phase of left) - (phase of right) side reflection in
C               degrees
C
C      3b) NLAY(I,J) (free format)
C               bessel order for the layer line in question,NNLAY
C               different trial values are allowed for each line
C
	integer*4	nlaymax
	parameter	(nlaymax = 28)
      DIMENSION D(50),Y(50),F(50),DPHI(50),NLAY(nlaymax,50),LFOVR(25),
     1          TITLE(20),X(25)
C
      PI = 3.14159
      TWOPI = 2. * PI
      RADIAN = PI/180.
C
  200 READ(5,10,END=500) TITLE
      WRITE(6,15) TITLE
      READ(5,*) OMEGAM,DOMEGA,NOMEGA,EXM,DEX,NEX,NNLAY,NOP,IDIM1,IDIM2
	if(nnlay .gt. nlaymax) then
	 write(6,'(''Error - too many trial values, increase nlaymax'')')
	 stop
	end if
      WRITE(6,20) OMEGAM,DOMEGA,NOMEGA,EXM,DEX,NEX,NNLAY,NOP,
     1            IDIM1,IDIM2
      WRITE(6,30)
C
      DO 250 I=1,NOP
      READ(5,*)   Y(I),D(I),F(I),DPHI(I)
      READ(5,*)    (NLAY(J,I),J=1,NNLAY)
      WRITE(6,40) Y(I),D(I),F(I),DPHI(I),(NLAY(J,I),J=1,NNLAY)
C
      Y(I) = Y(I)/IDIM2
      D(I) = D(I)/IDIM1
  250 CONTINUE
      SUMF = 0
C
      DO 270 I=1,NOP
      SUMF = SUMF + F(I)
  270 CONTINUE
      SUMF = SUMF * PI
C
      DO 290 K=1,NEX
      X(K) = EXM + (K-1)*DEX
  290 CONTINUE
      IF(NEX.GT.25) NEX = 25
C
      WRITE(6,60)
      DO 360 I=1,NNLAY
	lmin = 1000
      WRITE(6,70)  (NLAY(I,L),L=1,NOP)
      WRITE(6,80)  (X(K),K=1,NEX)
      DO 350 J=1,NOMEGA
      OMEGA = OMEGAM + (J-1) * DOMEGA
      SOM = SIN(OMEGA * RADIAN)
      DO 330 K=1,NEX
      EX = X(K) * TWOPI
      ALFOVR = 0.
      DO 310 L=1,NOP
      PSI = 2. * ATAN(2. * Y(L) * SOM / D(L)) * NLAY(I,L) +
     1      EX * D(L) + NLAY(I,L) * PI + DPHI(L) * RADIAN
      A = ABS(AMOD(PSI,TWOPI))
      B = TWOPI - A
      A=AMIN1(A,B)
      ALFOVR = ALFOVR + F(L) * A
  310 CONTINUE
      LFOVR(K) = 180. * ALFOVR / SUMF
	 if(lfovr(k) .lt. lmin) then
	  lmin = lfovr(k)
          omega_min = omega
	  x_min = x(k)
	 end if
  330 CONTINUE
      WRITE(6,90) OMEGA,(LFOVR(K),K=1,NEX)
  350 CONTINUE
      WRITE(6,'(/'' Min :'',i3,'' omega :'',f5.1,'' xshift :'',f5.1,
     * '' for Bessels :'',20i4/)')  
     *  lmin,omega_min,x_min,(NLAY(I,L),L=1,NOP)
  360 continue
      GO TO 200
C
   10 FORMAT(20A4)
   15 FORMAT('1SHIFT AND TILT SEARCH FOR HELIX'//5X,20A4)
   20 FORMAT(//2X,'Starting tilt angle          ',E10.3/
     1         2X,'Tilt angle increment         ',E10.3/
     2         2X,'No of points in tilt search  ',I10///
     3         2X,'Starting X shift value       ',E10.3/
     4         2X,'Shift increment              ',E10.3/
     5         2X,'No of points in shift search ',I10///
     6         2X,'No of Bessel order sets      ',I10/
     7         2X,'No of pairs of data points   ',I10//
     8         2X,'Transform dimensions',I5,' *',I5)
   30 FORMAT(6X,'Z',10X,'Delta R',10X,'Ave F',7X,'Delta Phi',
     12X,'Trial values of Bessel orders')
   40 FORMAT(2X,4(E10.3,5X)/28I5)
   60 FORMAT(2X,'The arrays below are     Sum(F*Alpha)/Sum(F)'//
     1       2X,'where Alpha is the difference between Delphi corrected
     2 for tilt/shift and either 0 or 180',
     3 / '  (residual is in DEGREES!!)')
   70 FORMAT(2X,'Bessel orders ',(20I4))
   80 FORMAT(20X,'X shift'/(7X,25F5.1))
   90 FORMAT(2X,F5.1,25I5)
  500 STOP
      END
