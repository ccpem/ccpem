C*SMOOTH.FOR*****************************************************************
C     Local smoothing by averaging 3x3 pixels
C       Input standard image on stream 1(IN)
C       Output smoothed image on stream 2(OUT)
C
C         VERSION 1.01       16-MAR-82      RAC     FOR VAX
C         VERSION 1.02       17MAY84        Stack of sections
C         VERSION 2.0        15NOV00        RAC Link with imsubs2000
C
C****************************************************************************
C
      COMMON//NX,NY,NZ
      DIMENSION ARRAY(524288),TITLE(20),NXYZ(3),MXYZ(3),SMOOTH(512)
      CHARACTER DAT*24
      EQUIVALENCE (NX,NXYZ)
CTSH++
	CHARACTER*80 TMPTITLE
	EQUIVALENCE (TMPTITLE,TITLE)
CTSH--
      DATA ZERO/0.0/
C
      WRITE(6,1000)
1000  FORMAT(///'  SMOOTH : Local smoothing over 3x3 pixels')
      CALL IMOPEN(1,'IN','RO')
      CALL IMOPEN(2,'OUT','NEW')
      CALL IRDHDR(1,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
      CALL FDATE(DAT)
CTSH      ENCODE(80,1500,TITLE) DAT(5:24)
CTSH++
      WRITE(TMPTITLE,1500) DAT(5:24)
CTSH--
1500  FORMAT(' SMOOTH : Local smoothing over 3x3 pixels',10X,A20)
      CALL ITRHDR(2,1)
      CALL IWRHDR(2,TITLE,1,ZERO,ZERO,ZERO)
C
C     Loop over sections
      TMIN=1.E10
      TMAX=-1.E10
      TMEAN=0.
      NTOT=NX*NY
      DO 300 IZ=1,NZ
      CALL IRDSEC(1,ARRAY,*99)
C
      SMIN=1.E7
      SMAX=1.E-7
      SMEAN=0.
      SSQ=0.
      NX1=NX-1
      NY1=NY-1
      DO 200 NYS=0,NY1
      IY1=NYS-1
      IY2=NYS+1
      DO 201 NXS=0,NX1
      IX1=NXS-1
      IX2=NXS+1
      NPTS=0
      DTOT=0.
      DO 210 IX=IX1,IX2
      IF((IX.LT.0).OR.(IX.GT.NY1)) GO TO 210
      DO 211 IY=IY1,IY2
      IF((IY.LT.0).OR.(IY.GT.NY1)) GO TO 211
      NPTS=NPTS+1
      IND=IY*NX+IX+1
      DTOT=DTOT+ARRAY(IND)
211   CONTINUE
210   CONTINUE
C
      IF(NPTS.EQ.0) WRITE(6,900) NXS,NYS
900   FORMAT(' NXS,NYS',2I10)
      S=DTOT/NPTS
      IF(S.GT.SMAX) SMAX=S
      IF(S.LT.SMIN) SMIN=S
      SMEAN=SMEAN+S
      SSQ=SSQ+S*S
      SMOOTH(NXS+1)=S
201   CONTINUE
      CALL IWRLIN(2,SMOOTH)
200   CONTINUE
C
      SMEAN=SMEAN/NTOT
      SD=SQRT(SSQ/NTOT-SMEAN*SMEAN)
      WRITE(6,1600) IZ,SMIN,SMAX,SMEAN,SD
1600  FORMAT(' Min max mean and s.d.on section',I5,' after smoothing'
     1 /5X,4F10.1)
      IF(SMIN.LT.TMIN) TMIN=SMIN
      IF(SMAX.GT.TMAX) TMAX=SMAX
      TMEAN=TMEAN+SMEAN
300   CONTINUE
C
      TMEAN=TMEAN/NZ
      CALL IWRHDR(2,TITLE,-1,TMIN,TMAX,TMEAN)
      CALL IMCLOSE(1)
      CALL IMCLOSE(2)
      STOP
C
99    WRITE(6,2000)
2000  FORMAT(///'  End of file on reading image')
      STOP
      END

