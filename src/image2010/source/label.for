C*LABEL.FOR********************************************************************
C									      *
C	Program to perform simple manipulations on image files		      *
C									      *
C	Version 1.12	31-MAR-82    DAA	for VAX			      *
C	Version 1.13	13-OCT-82    DAA	for VAX			      *
C	Version 1.14	7- JAN-85    MK 	LABELB nicer displays options *
C	Version 1.15	20-APR-85    RH		line size 8192		      *
C	Version 1.16	12-JUN-87    RH		extra option		      *
C	Version 1.17	06-FEB-92    RH		max/min/mean on option 4      *
C	Version 1.18	26-MAR-92    RH		transfer "extra" data         *
C	Version 1.20	07-OCT-95    RH		increased dimensions          *
C	Version 1.21	17-DEC-96    RH		new single particle option 50 *
C	Version 1.22	01-JAN-97    RH		clean up new option           *
C	Version 1.23	01-SEP-98    RH		remove title option 50 I/P    *
C	Version 1.24	27-DEC-98    RH		3D adjacent pixel averaging   *
C						filename CHARACTER*40         *
C	Version 1.25	22-APR-99    RH		bigger dimension for mirrors  *
C						and 180 rotation              *
C	Version 1.26	24-AUG-99    RH		filenames CHARACTER*80        *
C	Version 1.27    02-FEB-00    JMS	encodes -> int. writes and    *
C						added zero subr for LINUX     *
C	Version 1.28	29-MAR-00    JMS	extra in header reduced to 25 *
C	Version 1.29    26-OCT-01    JMS/RAC	corrects images too close to  *
C						edge in stack selection       *
C	Version 1.30	19-JAN-02    RH		add TITLE when IMODE = -1,-2  *
C						remove all defunct ENCODEs    *
C	Version 1.31	01-JUL-02    RH/JMS	WARNING byte .lt.0 or .gt.255 *
C						increase LMAX,LCMAX,IBMAX     *
C						add error message for overflow*
C	Version 1.32	28-MAY-03    RH		increase LMAX to 80000        *
C	Version 1.33	28-JUL-03    RH		add derivative option         *
C	Version 1.34	23-SEP-04    RH		don't check 0-255 if MODE.NE.0*
C	Version 1.35    23-APR-07    JMS        NDIM increased to 2mb         *
C                							      *
C******************************************************************************
C
C Input is a single file in mapformat, output is a single file in mapformat
C
C      Card input, or online control
C
C        card  1 :  input filename
C        card  2 :  IMODE, describes which of the various simple manipulations
C                are required - can be -2,-1,0,1,2,3,4 or 99 for images
C                                      as well as 5,6,7,8 for transforms.
C                If IMODE is 50, single particle boxing and restacking option
C                If IMODE is 99, a second IMODE of 1,2,3,4 gives more options
C                                      and 5,6,7,8 more options for transforms.
C        card  3 :  output filename
C        cards 4 onwards are dependent on manipulation - see below
C -------------------------------------------------------------------------
C IMODE= -2: Change INTEGER*2/INTEGER*1 output format',/,
C        card  4 : SCALE, scale factor for INTEGER*2/BYTE conversion
C
C IMODE= -1: Change REAL/INTEGER*2 output format',/,
C        card  4 : SCALE, scale factor for REAL/INTEGER conversion
C
C IMODE=  0: Change Labels',/,
C        card  4 : NLA, number of labels to ADD ?  (can be negative)
C        cards 5 : TITLE,  enter titles to add for label number #
C     or card  5 : NLR, remove labels # (0 to exit) ?
C
C IMODE=  1: Select region',/,
C        card  4 : enter Limits (Xmin,max,Ymin,max) if NZ=1
C     or card  4 : enter Limits (Xmin,max,Ymin,max,Zmin,max) if NZ>1
C
C IMODE=  2: Linear OD stretch  ( y = mx + b )',/,
C        card  4 : A,B enter coefficients A & B ( Y = AX + B )
C        card  5 : IQ=0 or 1 - set values <0 to 0  (0= no, 1=yes) ?
C
C IMODE=  3: Logrithmic  OD stretch ( y = aLOGx + b )',/,
C        card  4 : A,B enter coefficients A & B ( Y = A*ALOG(X) + B )
C
C IMODE=  4: Average adjacent pixels',/,
C        card  4 : NREDX,NREDY,NREDZ enter integer reduction factor for X,Y,Z:
C
C IMODE=  5: Output amplitudes or Intensities',/,
C        card  4 : IQ, write out amplitudes (0) or Intensities*.01 (1) ?
C
C IMODE=  6: Output Phases (degrees)',/,
C        no further input needed
C
C IMODE=  7: Output REAL part of Complex value',/,
C        no further input needed
C
C IMODE=  8: Output IMAGINARY part of Complex value',/)
C        no further input needed
C
C -----------------------------------------------------------
C IMODE= 50: goes to the single particle selection option, with input
C            from a file of X,Y coordinates, and desired box size.
C        card  4 : NBOXX,NBOXY
C        card  5 : input filename for the list of X,Y coordinates of
C                  the centres of the areas to be selected.
C -----------------------------------------------------------
C IMODE= 99: More options, mainly for display purposes')
C      then asks for a second IMODE
C
C IMODE=  1: VARIOUS 90 DEG TURNS AND MIRRORS',/,
C        card  4 : ITURN, TURN NO? (1:Z90,2:Z-90,3:Z180,4:Xmir,5:Ymir)
C
C IMODE=  2: GEOMETRIC STRETCH ( y = m**X )',/,
C        card  4 : A, enter A (Y = X**A)
C
C IMODE=  3: CUT OFF OVER - AND UNDERFLOWS',/,
C        card  4 : A,B enter LOWEST AND HIGHEST VALUE TO PRUNE IMAGE WITH
C
C IMODE=  4: GET RID OF OUTLIERS BY INTERPOLATION')
C        card  4 : critdiff, enter critical difference
C
C****************************************************************************
      PARAMETER (LMAX=80000)
      PARAMETER (LCMX=LMAX/2)
	COMMON //NX,NY,NZ,IXMIN,IYMIN,IZMIN,IXMAX,IYMAX,IZMAX
	DIMENSION ALINE(LMAX),TITLE(20),NXYZ(3),MXYZ(3),NXYZST(3)
c	DIMENSION ALINE(LMAX),NXYZ(3),MXYZ(3),NXYZST(3)
      	DIMENSION BLINE(LMAX)
	DIMENSION IXYZMIN(3),IXYZMAX(3),OUT(LMAX)
C	DIMENSION LABELS(20,10),CELL(6),EXTRA(29)
	DIMENSION LABELS(20,10),CELL(6),EXTRA(25)
	COMPLEX CLINE(LCMX),COUT(LCMX),DLINE(LCMX)
C	CHARACTER*80 INFILE,OUTFILE
	CHARACTER*80 INFILE,OUTFILE,tmptitle
      	REAL*8 DOUBLMEAN
	EQUIVALENCE (NX,NXYZ), (ALINE,CLINE), (OUT,COUT)
	EQUIVALENCE (IXYZMIN, IXMIN), (IXYZMAX, IXMAX)
C*** jms 12.08.2010
	equivalence (tmptitle,title)
	DATA NXYZST/3*0/, CNV/57.29578/
C
	WRITE(6,1000)
1000	FORMAT(//' LABEL: Image Manipulation Program',
     .	         '  V1.34(23-Sept-2004)'/)
	WRITE(6,1100)
1100	FORMAT('$Input filename:  ')
	READ(5,1200) INFILE
1200	FORMAT(A)

	CALL IMOPEN(1,INFILE,'RO')
C
	CALL IRDHDR(1,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
	NXT = NX
	IF (MODE .GE. 3) NXT = NXT*2
C
1	WRITE(6,1300)
1300	FORMAT(/,' Available modes of operation are: ',/,
     .  ' -2: Change INTEGER*2/INTEGER*1 output format',/,
     .  ' -1: Change REAL/INTEGER*2 output format',/,
     .  '  0: Change Labels',/,
     .  '  1: Select region',/,
     .  '  2: Linear OD stretch  ( y = mx + b )',/,
     .  '  3: Logrithmic  OD stretch ( y = aLOGx + b )',/,
     .  '  4: Average adjacent pixels',/,
     .  '  9: Calculate and output complex gradient',/,
     .  ' 50: create stack of small boxes centred on coords read',
     .  ' in from list',/,
     .  ' 99: More options, mainly for display purposes')

	IF (MODE .GE. 3) WRITE(6,1400)
1400	FORMAT(
     .  '  5: Output amplitudes or Intensities',/,
     .  '  6: Output Phases (degrees)',/,
     .  '  7: Output REAL part of Complex value',/,
     .  '  8: Output IMAGINARY part of Complex value',/)
C
	WRITE(6,1500)
1500	FORMAT(/,'$Enter desired mode: ')
	READ(5,*) IMODE
      	 if (imode.eq.50) then
			call IMCLOSE(1)
			call LABELC(INFILE)
			stop
		endif
		if (IMODE.eq.99) then
			call IMCLOSE(1)
			call LABELB(INFILE)
			stop
		endif
	IF ((IMODE.GT.4 .AND. IMODE.NE.9).AND. MODE.LT.3) GOTO 1
	WRITE(6,1550)
1550	FORMAT(10X)
C
	IF (IMODE .NE. 0) THEN
	  WRITE(6,1600)
1600	  FORMAT('$Output filename:  ')
	  READ(5,1200) OUTFILE
	  CALL IMOPEN(2,OUTFILE,'NEW')
	  CALL ITRHDR(2,1)
	END IF
	GOTO (3,5,10,20,30,40,45,50,60,70,80,85) IMODE+3
C
C  MODE -2 :  CHANGING OUTPUT DATA FORMAT INTEGER*1/INTEGER*2
C
3	IF (MODE.LT.0.OR.MODE.GT.1) GO TO 997
	IF (MODE .EQ. 1) LMODE = 0
	IF (MODE .EQ. 0) LMODE = 1
	MODE = LMODE
	SCALE = 1.0
	IF (MODE .EQ. 0) THEN
	  WRITE(6,1625)
1625	  FORMAT(/,'$Scale factor for INTEGER*2/BYTE conversion [1]: ')
	  READ(5,*) SCALE
      IF(SCALE*DMAX.GT.255.0)
     .  WRITE(*,*)' WARNING DMAX GT 255 - DATA WILL BE CORRUPTED'
      IF(DMIN.LT.0.0)
     .  WRITE(*,*)' WARNING DMIN NEGATIVE - DATA MAY BE CORRUPTED'
	END IF
C
	CALL IALMOD(2,MODE)
	DMIN = DMIN*SCALE
	DMAX = DMAX*SCALE
	DMEAN = DMEAN*SCALE
	WRITE(tmptitle,1626) SCALE
1626	FORMAT('LABEL Mode -2: scale factor for INTEGER*2/BYTE',
     .  ' conversion',F12.4)
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
	DO 95 IZ = 1,NZ
	  DO 95 IY = 1,NY
	    CALL IRDLIN(1,ALINE,*999)
	    DO 90 IX = 1,NX
	       ALINE(IX) = SCALE*ALINE(IX)
90	    CONTINUE
	    CALL IWRLIN(2,ALINE)
95	CONTINUE
	GOTO 990
C
C  MODE -1 :  CHANGING OUTPUT DATA FORMAT
C
5	IF (MODE.LT.1.OR.MODE.GT.4) GO TO 997
	IF (MODE .EQ. 1) LMODE = 2
	IF (MODE .EQ. 2) LMODE = 1
	IF (MODE .EQ. 3) LMODE = 4
	IF (MODE .EQ. 4) LMODE = 3
	MODE = LMODE
	SCALE = 1.0
	IF (MODE .EQ. 1 .OR. MODE .EQ. 3) THEN
	  WRITE(6,1650)
1650	  FORMAT(/,'$Scale factor for REAL/INTEGER conversion [1]: ')
	  READ(5,*) SCALE
	END IF
C
	CALL IALMOD(2,MODE)
	DMIN = DMIN*SCALE
	DMAX = DMAX*SCALE
      	IF (MODE.EQ.1.OR.MODE.EQ.3) THEN
      	 IF (DMAX.GT.32000.0) DMAX=32000.
      	 IF (DMIN.LT.-32000.0) DMIN=-32000.
      	ENDIF
	DMEAN = DMEAN*SCALE
	WRITE(tmptitle,1656) SCALE
1656	FORMAT('LABEL Mode -1: scale factor for REAL/INTEGER*2',
     .         ' conversion',F12.4)
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
      	NTRUNC = 0
	DO 125 IZ = 1,NZ
	  DO 125 IY = 1,NY
	    CALL IRDLIN(1,ALINE,*999)
      	    IF (MODE.EQ.1.OR.MODE.EQ.3) THEN
	      DO 100 IX = 1,NX
	        ATEMP = SCALE*ALINE(IX)
      	        IF (ATEMP.GT.32000.) THEN ! check for overflows
      	  ATEMP=32000.
      	  NTRUNC=NTRUNC+1
      	        ENDIF
      	        IF (ATEMP.LT.-32000.) THEN ! and underflows
      	  ATEMP=-32000.
      	  NTRUNC=NTRUNC+1
      	        ENDIF
	        ALINE(IX) = ATEMP
100	      CONTINUE
      	    ELSE
      	      DO 101 IX = 1,NX
      	        ALINE(IX) = SCALE*ALINE(IX)
101	      CONTINUE
      	    ENDIF
	    CALL IWRLIN(2,ALINE)
125	CONTINUE
      	IF (NTRUNC.NE.0) WRITE(6,126)NTRUNC
126	FORMAT(//////' *********************************************',
     .  ' WARNING ********************************************'//
     .  ' in real to integer conversion',I10,' numbers were truncated',
     .  ' to be +/-32000'//////)
	GOTO 990
C
C  MODE 0 : CHANGE LABELS
C
10	CALL IRTLAB(1,LABELS,NL)
	DO 150 J = 1,NL
	  WRITE(6,1675) J,(LABELS(K,J),K=1,18)
150	CONTINUE
1675	FORMAT(I3,2X,18A4)
	WRITE(6,1680)
1680	FORMAT(/,'$Number of labels to ADD ? ')
	READ(5,*) NLA
	IF (NLA .GT. 0) THEN
	  NL = MIN(10,NL+NLA)
	  LST = NL - NLA + 1
	  DO 160 J=LST,NL
	    WRITE(6,1690) J
	    READ(5,1700) TITLE
C	Following statement changed for Alliant
	    CALL CCPMVI(LABELS(1,J),TITLE,20)
160	  CONTINUE
1690	  FORMAT(' Enter label # ',I3)
1700	  FORMAT(20A4)
	ELSE
12	  WRITE(6,1710)
1710	  FORMAT(/,'$Remove label # (0 to exit) ? ')
	  READ(5,*) NLR
	  IF (NLR .LE. 0) GOTO 15
	  IF (NLR .GT. NL) GOTO 12
	  NL = NL - 1
	  DO 170 J = NLR,NL
C	Following statement changed for Alliant
	    CALL CCPMVI(LABELS(1,J),LABELS(1,J+1),20)
170	  CONTINUE
	  DO 180 J = 1,NL
	    WRITE(6,1675) J,(LABELS(K,J),K=1,18)
180	  CONTINUE
	  GOTO 12
	END IF
15	CALL IALLAB(1,LABELS,NL)
	CALL IWRHDR(1,TITLE,-1,DMIN,DMAX,DMEAN)
	GOTO 995
C
C  MODE 1 : SELECTING A REGION
C
20	CALL ICLLIM(1,IXYZMIN,IXYZMAX,NXYZ)
C
	write(tmptitle,1750) ixmin,ixmax,iymin,iymax,izmin,izmax
1750	FORMAT('LABEL Mode 1: Min/Max XYZ = ',6I6)
C
	IQ = 1
C	WRITE(6,1775)
C1775	FORMAT('$Use "true" starting limits (0) or start at 0 (1) ? ')
C	READ(5,*) IQ
	DO 200 J = 1,3
	  NXYZST(J) = 0
	  IF (IQ .EQ. 0) NXYZST(J) = IXYZMIN(J)
200	CONTINUE
C
	CALL IALSIZ(2,NXYZ,NXYZST)
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
	DMIN =  1.E10
	DMAX = -1.E10
	DOUBLMEAN = 0.0
	DO 250 IZ = IZMIN,IZMAX
	  CALL IMPOSN(1,IZ,IYMIN)
	  DO 250 IY = 1,NY
	    CALL IRDPAL(1,ALINE,IXMIN,IXMAX,*999)
	    CALL IWRLIN(2,ALINE)
	    DO 250 IX = 1,NX
	      IF (MODE .LT. 3) THEN
	        VAL = ALINE(IX)
	      ELSE
		VAL = CABS(CLINE(IX))
	      END IF
	      DOUBLMEAN = DOUBLMEAN + VAL
	      IF (VAL .LT. DMIN) DMIN = VAL
	      IF (VAL .GT. DMAX) DMAX = VAL
250	CONTINUE
C
	DMEAN = DOUBLMEAN/(NX*NY*NZ)
	CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
	GOTO 990
C
C  MODE 2 : LINEAR OD STRETCH
C
30	WRITE(6,1800)
1800	FORMAT(/,'$Enter coeffecients A & B ( Y = AX + B )  ')
	READ(5,*) A,B
	WRITE(6,1850)
1850	FORMAT('$Set values <0 to 0  (0= no, 1=yes) ? ')
	READ(5,*) IQ
	IF (IQ .EQ. 0) write(tmptitle,1900) a,b
	IF (IQ .EQ. 1) write(tmptitle,1950) a,b
1900	FORMAT('LABEL Mode 2: Linear Stretch A,B =',2F15.7)
1950	FORMAT('LABEL Mode 2: Linear Stretch,Zero truncation A,B =',
     .  2F12.4)
	DMIN =  1.E10
	DMAX = -1.E10
	DOUBLMEAN = 0.0
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
	DO 350 IZ= 1,NZ
	  DO 350 IY = 1,NY
	    CALL IRDLIN(1,ALINE,*999)
	    DO 300 IX = 1,NXT
	      VAL = ALINE(IX)*A + B
	      IF (IQ .EQ. 1 .AND. VAL .LT. 0.0) VAL = 0.0
	      IF (VAL .LT. DMIN) DMIN = VAL
	      IF (VAL .GT. DMAX) DMAX = VAL
	      DOUBLMEAN = DOUBLMEAN + VAL
	      ALINE(IX) = VAL
300	    CONTINUE
	    CALL IWRLIN(2,ALINE)
350	CONTINUE
	DMEAN = DOUBLMEAN/(NXT*NY*NZ)
	CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
	GOTO 990
C
C  MODE 3 : LOG OD STRETCH
C
40	WRITE(6,2000)
2000	FORMAT(' Enter coeffecients A & B ( Y = A*ALOG(X) + B )')
	READ(5,*) A,B
	write(tmptitle,2100) a,b
2100	FORMAT('LABEL Mode 3: Logarithmic Stretch A,B = ',2F15.7)
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
	DMIN = 1.E10
	DMAX = -1.E10
	DOUBLMEAN = 0.0
C
      	NOVER=0
      	NUNDER=0
	DO 450 IZ= 1,NZ
	  DO 450 IY = 1,NY
	    CALL IRDLIN(1,ALINE,*999)
	    DO 400 IX = 1,NXT
	      VAL = ALINE(IX)
	      ABSVAL = ABS(VAL)
	      IF (ABSVAL .LT. 1.E-5) ABSVAL=1.
	      VAL = A*SIGN(ALOG10(ABSVAL),VAL) + B
      	      IF(MODE.EQ.0) THEN
      	 IF(VAL.GT.255.0) THEN
      	  VAL=255.0
      	  NOVER=NOVER+1
      	       ENDIF
      	 IF(VAL.LT.0.0) THEN
      	  VAL=0.0
      	  NUNDER=NUNDER+1
      	       ENDIF
      	      ENDIF
	      ALINE(IX) = VAL
	      IF (VAL .LT. DMIN) DMIN = VAL
	      IF (VAL .GT. DMAX) DMAX = VAL
	      DOUBLMEAN = DOUBLMEAN + VAL
400	    CONTINUE
	    CALL IWRLIN(2,ALINE)
450	CONTINUE
	DMEAN = DOUBLMEAN/(NXT*NY*NZ)
      	IF(MODE.EQ.0)WRITE(6,451) NOVER,NUNDER,DMIN,DMAX,DMEAN
451	FORMAT(' There were ',I10,' densities reduced to 255',
     .         '        and ',I10,' densities increased to 0',
     .         ' DMIN,DMAX,DMEAN ',3F10.1)
	CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
	GOTO 990
C
C  MODE 4 : PIXEL AVERAGING
C
45	WRITE(6,2200)
      	DMIN = 1.E10
      	DMAX = -1.E10
      	DOUBLMEAN = 0.0
2200	FORMAT('$Enter integer reduction factor for X,Y,Z: ')
	READ(5,*) NREDX,NREDY,NREDZ
      	 IF(NREDX.LT.1) NREDX=1
      	 IF(NREDY.LT.1) NREDY=1
      	 IF(NREDZ.LT.1) NREDZ=1
      	 IF(NX/NREDX*NREDX.NE.NX.OR.NY/NREDY*NREDY.NE.NY.OR.
     .             NZ/NREDZ*NREDZ.NE.NZ) WRITE(6,2201)
2201		FORMAT(' WARNING!! - inexact division, edge strip will be weak')
	write(tmptitle,2300) nredx, nredy, nredz
2300	FORMAT('LABEL Mode 4: Pixel averaging factor for X,Y,Z = ',3I4)
      	IF((NX.EQ.1.AND.NREDX.GT.1).OR.
     .     (NY.EQ.1.AND.NREDY.GT.1).OR.
     .     (NZ.EQ.1.AND.NREDZ.GT.1))
     .     STOP ' Only 1 pixel: cannot average'
	NX = NX/NREDX
	NY = NY/NREDY
      	NZ = NZ/NREDZ
      	  NX = MAX0(1,NX)
      	  NY = MAX0(1,NY)
      	  NZ = MAX0(1,NZ)
	IF(NX * NREDX .GT. LMAX) THEN
	 WRITE(6,'(''Error !! dimension overflow, increase LMAX'')')
	 STOP
	END IF
C  Put title labels, new cell and extra information only into header
	CALL IRTLAB(1,LABELS,NL)
	write(6,'(''Label creating header :'',i5)')nl
C	CALL IRTEXT(1,EXTRA,1,29)
	CALL IRTEXT(1,EXTRA,1,25)
	CALL IRTCEL(1,CELL)
	IF(MXYZ(1).NE.0) CELL(1) = CELL(1)*NX*NREDX/MXYZ(1)
	IF(MXYZ(2).NE.0) CELL(2) = CELL(2)*NY*NREDY/MXYZ(2)
	IF(MXYZ(3).NE.0) CELL(3) = CELL(3)*NZ*NREDZ/MXYZ(3)
	CALL ICRHDR(2,NXYZ,NXYZ,MODE,LABELS,NL)
C	CALL IALEXT(2,EXTRA,1,29)
	CALL IALEXT(2,EXTRA,1,25)
	CALL IALCEL(2,CELL)
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
	NX4 = NX*4
	SCL = 1./(NREDX*NREDY*NREDZ)
	IF (MODE .GE. 3) NX4 = NX4*2
	DO 475 IZ = 1,NZ
	  DO 474 IY = 1,NY
	    CALL ZERO(OUT,NX4)
      	    DO 473 JZ=1,NREDZ
      	 IZPOS=(IZ-1)*NREDZ+JZ-1
      	 IYPOS=(IY-1)*NREDY
C      	write(*,*) 'BEFORE IMPOSN IY=',IY,'  JY=',JY
      	 CALL IMPOSN(1,IZPOS,IYPOS)
      	 IF (MODE .LE. 2) THEN
      	    DO 460 JY = 1,NREDY
C      	write(*,*) 'AT 460 IY=',IY,'  JY=',JY
      	  CALL IRDLIN(1,ALINE,*999)
			INDEX = 0
		   DO 460 IX = 1,NX
		   DO 460 JX = 1,NREDX
      	  INDEX = INDEX + 1
			OUT(IX) = OUT(IX) + ALINE(INDEX)*SCL
460	           CONTINUE
C      	write(*,*) 'AFTER 460 , IY=',IY,'  JY=',JY
      	 ELSE
      	    DO 470 JY = 1,NREDY
      	  CALL IRDLIN(1,CLINE,*999)
			INDEX = 0
		   DO 470 IX = 1,NX
		   DO 470 JX = 1,NREDX
			INDEX = INDEX + 1
			COUT(IX) = COUT(IX) + CLINE(INDEX)*SCL
470		   CONTINUE
      	 END IF
473	    CONTINUE
      	    IF (MODE .LE. 2) THEN ! calculate only for reals
      	 DO 465 IX=1,NX
      	  VAL=OUT(IX)
      	  IF (VAL .LT. DMIN) DMIN = VAL
      	  IF (VAL .GT. DMAX) DMAX = VAL
      	  DOUBLMEAN = DOUBLMEAN + VAL
465		CONTINUE
      	    END IF
	  CALL IWRLIN(2,OUT)
474	  CONTINUE
475	CONTINUE
      	DMEAN = DOUBLMEAN/(NX*NY*NZ)
      	CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
	GOTO 990
C
C  MODE 5 : GETTING AMPLITUES OR INTENSITIES
C
50	CALL IALMOD(2,2)
	WRITE(6,2350)
2350	FORMAT('$Write out amplitudes (0) or Intensities*.01 (1) ? ')
	READ(5,*) IQ
	IF (IQ .EQ. 0) THEN
	  write(tmptitle,2400)
	ELSE
	  write(tmptitle,2450)
	ENDIF
2400	FORMAT('LABEL Mode 5: Amplitudes selected')
2450	FORMAT('LABEL Mode 5: Intensities*.01 selected')
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
	DMIN =  1.E10
	DMAX = -1.E10
	DOUBLMEAN = 0.0
	DO 550 IZ = 1,NZ
	  DO 550 IY = 1,NY
	    CALL IRDLIN(1,CLINE,*999)
	    IF (IQ .EQ. 0) THEN
	      DO 500 IX = 1,NX
	        VAL = CABS(CLINE(IX))
	        ALINE(IX) = VAL
	        DOUBLMEAN = DOUBLMEAN + VAL
	        IF (VAL .LT. DMIN) DMIN = VAL
	        IF (VAL .GT. DMAX) DMAX = VAL
500	      CONTINUE
	    ELSE
	      IND = 1
	      DO 525 IX = 1,NX
	        VAL = .01*(ALINE(IND)**2 + ALINE(IND+1)**2)
		IND = IND + 2
	        ALINE(IX) = VAL
	        DOUBLMEAN = DOUBLMEAN + VAL
	        IF (VAL .LT. DMIN) DMIN = VAL
	        IF (VAL .GT. DMAX) DMAX = VAL
525	      CONTINUE
	    ENDIF
	    CALL IWRLIN(2,ALINE)
550	CONTINUE
	DMEAN = DOUBLMEAN/(NX*NY*NZ)
	CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
	GOTO 990
C
C  MODE 6 : GETTING PHASES
C
60	CALL IALMOD(2,2)
	write(tmptitle,2500)
2500	FORMAT('LABEL Mode 6: Select phases from Fourier Transform')
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
	DMIN =  1.E10
	DMAX = -1.E10
	DOUBLMEAN = 0.0
	DO 650 IZ = 1,NZ
	  DO 650 IY = 1,NY
	    CALL IRDLIN(1,CLINE,*999)
	    DO 600 IX = 1,NX
	      A = REAL(CLINE(IX))
	      B = AIMAG(CLINE(IX))
	      PHASE = ATAN2(B,A)*CNV
	      ALINE(IX) = PHASE
	      DOUBLMEAN = DOUBLMEAN + VAL
	      IF (VAL .LT. DMIN) DMIN = PHASE
	      IF (VAL .GT. DMAX) DMAX = PHASE
600	    CONTINUE
	    CALL IWRLIN(2,ALINE)
650	CONTINUE
	DMEAN = DOUBLMEAN/(NX*NY*NZ)
	CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
	GOTO 990
C
C  MODE 7 : GETTING REAL PART
C
70	CALL IALMOD(2,2)
	write(tmptitle,2600)
2600	FORMAT('LABEL Mode 7: Select real part from Fourier Transform')
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
	DMIN =  1.E10
	DMAX = -1.E10
	DOUBLMEAN = 0.0
	DO 750 IZ = 1,NZ
	  DO 750 IY = 1,NY
	    CALL IRDLIN(1,CLINE,*999)
	    DO 700 IX = 1,NX
	      VAL = REAL(CLINE(IX))
	      ALINE(IX) = VAL
	      DOUBLMEAN = DOUBLMEAN + VAL
	      IF (VAL .LT. DMIN) DMIN = VAL
	      IF (VAL .GT. DMAX) DMAX = VAL
700	    CONTINUE
	    CALL IWRLIN(2,ALINE)
750	CONTINUE
	DMEAN = DOUBLMEAN/(NX*NY*NZ)
	CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
	GOTO 990
C
C  MODE 8 : GETTING IMAGINARY PART
C
80	CALL IALMOD(2,2)
	write(tmptitle,2700)
2700	FORMAT('LABEL Mode 8: Select imaginary part from'
     .  ' Fourier Transform')
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
	DMIN =  1.E10
	DMAX = -1.E10
	DOUBLMEAN = 0.0
	DO 850 IZ = 1,NZ
	  DO 850 IY = 1,NY
	    CALL IRDLIN(1,CLINE,*999)
	    DO 800 IX = 1,NX
	      VAL = AIMAG(CLINE(IX))
	      ALINE(IX) = VAL
	      DOUBLMEAN = DOUBLMEAN + VAL
	      IF (VAL .LT. DMIN) DMIN = VAL
	      IF (VAL .GT. DMAX) DMAX = VAL
800	    CONTINUE
	    CALL IWRLIN(2,ALINE)
850	CONTINUE
	DMEAN = DOUBLMEAN/(NX*NY*NZ)
	CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
	GOTO 990
C
C  MODE 9 : CALCULATING LOCAL GRADIENT (COMPLEX)
C
85	CALL IALMOD(2,4)
	write(tmptitle,2705)
2705	FORMAT('LABEL Mode 9: Calculate local gradient of density')
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
	DMIN =  1.E10
	DMAX = -1.E10
	DOUBLMEAN = 0.0
	DO 862 IZ = 1,NZ
	  DO 860 IY = 1,NY-1
	    IF(IY.EQ.1) CALL IRDLIN(1,ALINE,*999)
	    CALL IRDLIN(1,BLINE,*999)
	    DO 855 IX = 1,NX-1
      	      DX = ALINE(IX+1) - ALINE(IX)
      	      DY = BLINE(IX) - ALINE(IX)
      	      DLINE(IX) = CMPLX(DX,DY)
	      VAL = CABS(DLINE(IX))
	      DOUBLMEAN = DOUBLMEAN + VAL
	      IF (VAL .LT. DMIN) DMIN = VAL
	      IF (VAL .GT. DMAX) DMAX = VAL
855	    CONTINUE
	    CALL IWRLIN(2,DLINE)
      	    DO 857 IX=1,NX
      	 ALINE(IX)=BLINE(IX)
857	    CONTINUE
860	  CONTINUE
      	  DO 858 IX=1,NX
858       CLINE(IX)=CMPLX(0.0,0.0)
	  CALL IWRLIN(2,CLINE)
862	CONTINUE
	DMEAN = DOUBLMEAN/(NX*NY*NZ)
	CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
	GOTO 990
C
C   HERE FOR FINISH
C
990	CALL IMCLOSE(2)
995	CALL IMCLOSE(1)
	CALL EXIT
997	WRITE(6,998)
998	FORMAT(' THIS OPTION NOT COMPATIBLE WITH INPUT FILE MODE')
      	STOP
999	STOP 'END-OF-FILE ERROR ON READ'
	END

C*LABELB.FOR*************************************************************
C									*
C	Program to perform simple manipulations on image files		*
C									*
C	LABEL Version 1.12	31-MAR-82    DAA	for VAX	        *
C	LABEL Version 1.13	13-OCT-82    DAA	for VAX		*
C	LABELB Version 1.1      7- JAN-85    MK   some more routines    *
C	LABELB Version 1.2	20-APR-85    RH   bigger line length	*
C	LABELB Version 1.21	24-AUG-99    CHARACTER*80 filenames     *
C************************************************************************
C
      SUBROUTINE LABELB(INFILE)
      PARAMETER (LMAX=80000)
      PARAMETER (LCMX=LMAX/2)
      PARAMETER (IBMX=3000)
	COMMON //NX,NY,NZ,IXMIN,IYMIN,IZMIN,IXMAX,IYMAX,IZMAX
	DIMENSION ALINE(LMAX),BLINE(LMAX),TITLE(20),NXYZ(3),MXYZ(3),NXYZST(3)
c	DIMENSION ALINE(LMAX),BLINE(LMAX),NXYZ(3),MXYZ(3),NXYZST(3)
	DIMENSION BILD(IBMX,IBMX),CILD(IBMX,IBMX)
	DIMENSION IXYZMIN(3),IXYZMAX(3),OUT(LMAX)
c	DIMENSION LABELS(20,10),CELL(6)
	DIMENSION LABELS(20,10)
	COMPLEX CLINE(LCMX),COUT(LCMX)
      	REAL*8 DOUBLMEAN
c	CHARACTER*80 INFILE,OUTFILE
	CHARACTER*80 INFILE,OUTFILE,tmptitle
	EQUIVALENCE (NX,NXYZ), (ALINE,CLINE), (OUT,COUT)
	EQUIVALENCE (IXYZMIN, IXMIN), (IXYZMAX, IXMAX)
	equivalence (tmptitle,title)
	DATA NXYZST/3*0/, CNV/57.29578/
C
1000	FORMAT(//' LABELB: Image Manipulation Program  V1.21'/)
C*** jms 17.06.2010
1011	format(' these are some more options to manipulate image files
     *  '/' especially for nice diplays. You can turn them around in
     *  '/' 90 degree steps, cut off outliers, repair scratches, or
     *  '/' do geometrical stretch (square root, e.g.).')
C	WRITE(6,1100)
c1100	FORMAT('$Input filename:  ')
c	READ(5,1200) INFILE
1200	FORMAT(A)

	CALL IMOPEN(1,INFILE,'RO')
C
	CALL IRDHDR(1,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
	NXT = NX
	IF (MODE .GE. 3) NXT = NXT*2
	write(6,1000)
	write(6,1011)
C
1	WRITE(6,1300)
1300	FORMAT(/,' Available modes of operation are: ',/,
     .  '  1: VARIOUS 90 DEG TURNS AND MIRRORS',/,
     .  '  2: GEOMETRIC STRETCH ( y = m**X )',/,
     .  '  3: CUT OFF OVER - AND UNDERFLOWS',/,
     .  '  4: GET RID OF OUTLIERS BY INTERPOLATION')
	IF (MODE .GE. 3) WRITE(6,1400)
1400	FORMAT(
     .  '  5: Output amplitudes or Intensities',/,
     .  '  6: Output Phases (degrees)',/,
     .  '  7: Output REAL part of Complex value',/,
     .  '  8: Output IMAGINARY part of Complex value',/)
C
	WRITE(6,1500)
1500	FORMAT(/,'$Enter desired mode: ')
	READ(5,*) IMODE
	IF (IMODE .GT. 4 .AND. MODE .LT. 3) GOTO 1
	WRITE(6,1550)
1550	FORMAT(10X)
C
	IF (IMODE .NE. 0) THEN
	  WRITE(6,1600)
1600	  FORMAT('$Output filename:  ')
	  READ(5,1200) OUTFILE

	IF (IMODE.NE.1) THEN
	  CALL IMOPEN(2,OUTFILE,'NEW')
	  CALL ITRHDR(2,1)
	END IF
	END IF

	GOTO (5,10,20,30,40,45,50,60,70,80) IMODE+2

5	Continue
	GOTO 990
10	Continue
	GOTO 995
C
C  MODE 1 : VARIOUS 90 DEG TURNS AND MIRRORS
C
20	CONTINUE
	WRITE(6,287)
	READ(5,*)ITURN
287	FORMAT('$TURN NO? (1:Z90,2:Z-90,3:Z180,4:Xmir,5:Ymir)')
      	IF(ITURN.LE.2.AND.(NXYZ(1).GT.IBMX.OR.NXYZ(2).GT.IBMX)) THEN
      	 WRITE(6,201) IBMX, NXYZ(1), NXYZ(2)
201		FORMAT(' CANNOT DO 90 DEGREE TURNS IF IMAGE SIZE',
     .                 ' .GT.',I5,', NX AND NY ARE',2I5)
      	 GO TO 1
      	ENDIF
C
	write(tmptitle,1750) iturn
1750	FORMAT('MLABEL  Mode 1: TURN NO ',I1,
     .	'   (1:Z90,2:Z-90,3:Z180,4:X180,5:Y180)')
200	CONTINUE
C
	IF (ITURN.EQ.1.OR.ITURN.EQ.2) THEN
	NXYZST(1) = NXYZ(2)
	NXYZST(2) = NXYZ(1)
	NXYZST(3) = NXYZ(3)
	ELSE
	NXYZST(1) = NXYZ(1)
	NXYZST(2) = NXYZ(2)
	NXYZST(3) = NXYZ(3)
	ENDIF

	CALL IMOPEN(2,OUTFILE,'NEW')
	CALL ICRHDR(2,NXYZST,NXYZST,2,LABELS,10)
	CALL ITRLAB(2,1)
C       the cell parameters are not transformed or transferred, because this
C	operation was intended just for beautifying pictures
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
C	FOR ITURN=1 or 2, READ THE WHOLE PICTURE INTO BILD. IT HAS TO BE
C	ONE LAYER ONLY AND SMALLER THAN THE DIMENSIONS OF BILD.
      DO IZ = 1,NXYZ(3) !FOR EACH SECTION SEPARATELY

      	IF(ITURN.LE.2) THEN
		DO IY = 1,NXYZ(2)
		    CALL IRDLIN(1,BILD(1,IY),*999)
		ENDDO
C		NOW TRANSFORM THIS IN SUITABLE MANNER
		IF (ITURN.EQ.1) THEN
		DO IX = 1,NXYZ(1)
			DO IY = 1,NXYZ(2)
			CILD(IY,NXYZ(1)-IX+1)=BILD(IX,IY)
			ENDDO
		ENDDO
		ENDIF
		IF (ITURN.EQ.2) THEN
		DO IX = 1,NXYZ(1)
			DO IY = 1,NXYZ(2)
			CILD(NXYZ(2)-IY+1,IX)=BILD(IX,IY)
			ENDDO
		ENDDO
		ENDIF
		DO IY = 1,NXYZST(2)
			CALL IWRLIN(2,CILD(1,IY))
		ENDDO
      	ENDIF
	IF (ITURN.EQ.3) THEN
      	 CALL IRDLIN(1,ALINE,*999)
		DO IX = 1,NXYZ(1)
		BLINE(NXYZ(1)-IX+1)=ALINE(IX)
		ENDDO
      	 CALL IMPOSN(2,IZ-1,NXYZ(2)-IY)
		CALL IWRLIN(2,BLINE)
	ENDIF
	IF (ITURN.EQ.4) THEN
		DO IY = 1,NXYZ(2)
      	  CALL IRDLIN(1,ALINE,*999)
      	  CALL IMPOSN(2,IZ-1,NXYZ(2)-IY)
			CALL IWRLIN(2,ALINE)
		ENDDO
	ENDIF
	IF (ITURN.EQ.5) THEN
		DO IY = 1,NXYZ(2)
      	  CALL IRDLIN(1,ALINE,*999)
			DO IX = 1,NXYZ(1)
				BLINE(NXYZ(1)-IX+1)=ALINE(IX)
			ENDDO
			CALL IWRLIN(2,BLINE)
		ENDDO
	ENDIF
      ENDDO !IZ
C
	CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
	GOTO 990
C
C  MODE 2 : GEOMETRIC STRETCH
C
30	WRITE(6,1800)
1800	FORMAT(/,'$Enter A (Y = X**A) ')
	READ(5,*) A
	write(tmptitle,1900) a
1900	FORMAT('MLABEL Mode 2: GEOMETRIC Stretch A,= ',F12.4)
	DMIN =  1.E10
	DMAX = -1.E10
	DOUBLMEAN = 0.0
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
	DO 350 IZ= 1,NZ
	  DO 350 IY = 1,NY
	    CALL IRDLIN(1,ALINE,*999)
		IF (A.EQ.0.5) THEN  !SQRT
	    DO 300 IX = 1,NXT
	      VAL = SQRT(ALINE(IX))
	      IF (VAL .LT. DMIN) DMIN = VAL
	      IF (VAL .GT. DMAX) DMAX = VAL
	      DOUBLMEAN = DOUBLMEAN + VAL
	      ALINE(IX) = VAL
300	    CONTINUE
		ELSE
	    DO 392 IX = 1,NXT
	      VAL = (ALINE(IX))**A
	      IF (VAL .LT. DMIN) DMIN = VAL
	      IF (VAL .GT. DMAX) DMAX = VAL
	      DOUBLMEAN = DOUBLMEAN + VAL
	      ALINE(IX) = VAL
392	    CONTINUE
	        ENDIF
	    CALL IWRLIN(2,ALINE)
350	CONTINUE
	DMEAN = DOUBLMEAN/(NXT*NY*NZ)
	CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
	GOTO 990
C
C  MODE 3 : CUT OFF OVER AND UNDERFLOW
C
40	WRITE(6,2000)
2000	FORMAT(' Enter LOWEST AND HIGHEST VALUE TO PRUNE IMAGE WITH')
	READ(5,*) A,B
	write(tmptitle,2100) a, b
2100	FORMAT('MLABEL Mode 3: LIMIT DYNAMIC RANGE TO ',2F12.4)
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
	DMIN = 1.E10
	DMAX = -1.E10
	DOUBLMEAN = 0.0
C
	DO 450 IZ= 1,NZ
	  DO 450 IY = 1,NY
	    CALL IRDLIN(1,ALINE,*999)
	    DO 400 IX = 1,NXT
	      IF (ALINE(IX).LT.A) ALINE(IX)=A
	      IF (ALINE(IX).GT.B) ALINE(IX)=B
	      IF (ALINE(IX) .LT. DMIN) DMIN = ALINE(IX)
	      IF (ALINE(IX) .GT. DMAX) DMAX = ALINE(IX)
	      DOUBLMEAN = DOUBLMEAN + ALINE(IX)
400	    CONTINUE
	    CALL IWRLIN(2,ALINE)
450	CONTINUE
	DMEAN = DOUBLMEAN/(NXT*NY*NZ)
	CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
	GOTO 990
C
C  MODE 4 : GET RID OF OUTLIERS BY INTERPOLATION
C     if in one line  pixel n - pixel(n-1) is opposite in sign to
c     pixel n+1 - pixel n and greater than preset value, pixel n
c     is replaced by the average of n+1 and n-1. This should get
c     rid of vertical lines introduced through memory errrors in
c     2-d pictures.
45	WRITE(6,2200)
2200	FORMAT('$Enter critical difference ')
	READ(5,*) critdiff
	write(tmptitle,2300) critdiff
2300	FORMAT('MLABEL Mode 4: get rid of outlier rows, critdiff= ',G10.4)
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)

	DMIN = 1.E10
	DMAX = -1.E10
	DOUBLMEAN = 0.0
C
	DO 454 IZ= 1,NZ
	  DO 454 IY = 1,NY
	    CALL IRDLIN(1,ALINE,*999)
	    DO 404 IX = 2,NXT-1
		DLEFT = ALINE(IX)-ALINE(IX-1)
		DRIGH = ALINE(IX+1)-ALINE(IX)
		IF (DRIGH.EQ.0.) DRIGH=1.E-10
C*** jms 17.06.2010
	      IF (ABS(DLEFT).GT.CRITDIFF.AND.DLEFT/DRIGH.LE.0.)
     *            ALINE(IX)=(ALINE(IX+1)+ALINE(IX-1))/2.
	      IF (ALINE(IX) .LT. DMIN) DMIN = ALINE(IX)
	      IF (ALINE(IX) .GT. DMAX) DMAX = ALINE(IX)
	      DOUBLMEAN = DOUBLMEAN + ALINE(IX)
404	    CONTINUE
	    CALL IWRLIN(2,ALINE)
454	CONTINUE
	DMEAN = DOUBLMEAN/(NXT*NY*NZ)
	CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
	GOTO 990
C
C  MODE 5 : GETTING AMPLITUES OR INTENSITIES
C
50	CALL IALMOD(2,2)
	WRITE(6,2350)
2350	FORMAT('$Write out amplitudes (0) or Intensities*.01 (1) ? ')
	READ(5,*) IQ
	IF (IQ .EQ. 0) THEN
	  write(tmptitle,2400)
	ELSE
	  write(tmptitle,2450)
	ENDIF
2400	FORMAT('LABEL Mode 5: Select amplitudes from Fourier')
2450	FORMAT('LABEL Mode 5: Select Intensities *0.01 from Fourier '
     .   'Transform')
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
	DMIN =  1.E10
	DMAX = -1.E10
	DOUBLMEAN = 0.0
	DO 550 IZ = 1,NZ
	  DO 550 IY = 1,NY
	    CALL IRDLIN(1,CLINE,*999)
	    IF (IQ .EQ. 0) THEN
	      DO 500 IX = 1,NX
	        VAL = CABS(CLINE(IX))
	        ALINE(IX) = VAL
	        DOUBLMEAN = DOUBLMEAN + VAL
	        IF (VAL .LT. DMIN) DMIN = VAL
	        IF (VAL .GT. DMAX) DMAX = VAL
500	      CONTINUE
	    ELSE
	      IND = 1
	      DO 525 IX = 1,NX
	        VAL = .01*(ALINE(IND)**2 + ALINE(IND+1)**2)
		IND = IND + 2
	        ALINE(IX) = VAL
	        DOUBLMEAN = DOUBLMEAN + VAL
	        IF (VAL .LT. DMIN) DMIN = VAL
	        IF (VAL .GT. DMAX) DMAX = VAL
525	      CONTINUE
	    ENDIF
	    CALL IWRLIN(2,ALINE)
550	CONTINUE
	DMEAN = DOUBLMEAN/(NX*NY*NZ)
	CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
	GOTO 990
C
C  MODE 6 : GETTING PHASES
C
60	CALL IALMOD(2,2)
	write(tmptitle,2500)
2500	FORMAT('LABEL Mode 6: Select phases from Fourier Transform')
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
	DMIN =  1.E10
	DMAX = -1.E10
	DOUBLMEAN = 0.0
	DO 650 IZ = 1,NZ
	  DO 650 IY = 1,NY
	    CALL IRDLIN(1,CLINE,*999)
	    DO 600 IX = 1,NX
	      A = REAL(CLINE(IX))
	      B = AIMAG(CLINE(IX))
	      PHASE = ATAN2(B,A)*CNV
	      ALINE(IX) = PHASE
	      DOUBLMEAN = DOUBLMEAN + VAL
	      IF (VAL .LT. DMIN) DMIN = PHASE
	      IF (VAL .GT. DMAX) DMAX = PHASE
600	    CONTINUE
	    CALL IWRLIN(2,ALINE)
650	CONTINUE
	DMEAN = DOUBLMEAN/(NX*NY*NZ)
	CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
	GOTO 990
C
C  MODE 7 : GETTING REAL PART
C
70	CALL IALMOD(2,2)
	write(tmptitle,2600)
2600	FORMAT('LABEL Mode 7: Select real part from Fourier Transform')
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
	DMIN =  1.E10
	DMAX = -1.E10
	DOUBLMEAN = 0.0
	DO 750 IZ = 1,NZ
	  DO 750 IY = 1,NY
	    CALL IRDLIN(1,CLINE,*999)
	    DO 700 IX = 1,NX
	      VAL = REAL(CLINE(IX))
	      ALINE(IX) = VAL
	      DOUBLMEAN = DOUBLMEAN + VAL
	      IF (VAL .LT. DMIN) DMIN = VAL
	      IF (VAL .GT. DMAX) DMAX = VAL
700	    CONTINUE
	    CALL IWRLIN(2,ALINE)
750	CONTINUE
	DMEAN = DOUBLMEAN/(NX*NY*NZ)
	CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
	GOTO 990
C
C  MODE 8 : GETTING IMAGINARY PART
C
80	CALL IALMOD(2,2)
	write(tmptitle,2700)
2700	FORMAT('LABEL Mode 8: Select imaginary part from'
     .  ' Fourier Transform')
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
	DMIN =  1.E10
	DMAX = -1.E10
	DOUBLMEAN = 0.0
	DO 850 IZ = 1,NZ
	  DO 850 IY = 1,NY
	    CALL IRDLIN(1,CLINE,*999)
	    DO 800 IX = 1,NX
	      VAL = AIMAG(CLINE(IX))
	      ALINE(IX) = VAL
	      DOUBLMEAN = DOUBLMEAN + VAL
	      IF (VAL .LT. DMIN) DMIN = VAL
	      IF (VAL .GT. DMAX) DMAX = VAL
800	    CONTINUE
	    CALL IWRLIN(2,ALINE)
850	CONTINUE
	DMEAN = DOUBLMEAN/(NX*NY*NZ)
	CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
	GOTO 990
C
C   HERE FOR FINISH
C
990	CALL IMCLOSE(2)
995	CALL IMCLOSE(1)
	CALL EXIT
999	STOP 'END-OF-FILE ERROR ON READ'
	END

C*LABELC.FOR**************************************************************
C									 *
C	Program to perform simple manipulations on image files		 *
C	this subroutine selects many small areas and writes out a stack	 *
C	of mini areas surrounding aech selected area (particle)		 *
C									 *
C   this subroutine edited from						 *
C	LABEL Version 1.12	31-MAR-82    DAA	for VAX	         *
C	LABEL Version 1.13	13-OCT-82    DAA	for VAX		 *
C	LABELB Version 1.1      7- JAN-85    MK   some more routines     *
C	LABELB Version 1.2	20-APR-85    RH   bigger line length	 *
C       LABELC Version 1.21	17-Dec-1996  RH   creation of subroutine *
C       LABELC Version 1.22	24-Aug-1998  RH   CHARACTER*80 filenames *
C*************************************************************************
C
      SUBROUTINE LABELC(INFILE)
      PARAMETER (NDIM=2000000,NAREA=10000)
c      PARAMETER (NDIM=300000,NAREA=10000)
	COMMON //NX,NY,NZ,IXMIN,IYMIN,IZMIN,IXMAX,IYMAX,IZMAX
	DIMENSION ARRAY(NDIM),TITLE(20),NXYZ(3),MXYZ(3),NXYZST(3)
c	DIMENSION ARRAY(NDIM),NXYZ(3),MXYZ(3),NXYZST(3)
      	DIMENSION NXYZBOX(3),NXYZTMP(3)
	DIMENSION IXYZMIN(3),IXYZMAX(3)
	DIMENSION LABELS(20,10)
      	DIMENSION IAX(NAREA),IAY(NAREA)
      	REAL*8 DOUBLMEAN
        CHARACTER DAT*24
c	CHARACTER*80 INFILE,OUTFILE,DATAFILE,TITLELINE
	CHARACTER*80 INFILE,OUTFILE,DATAFILE,TITLELINE,tmptitle
	EQUIVALENCE (IXYZMIN, IXMIN), (IXYZMAX, IXMAX)
	equivalence (tmptitle,title)
	DATA NXYZST/3*0/, CNV/57.29578/
C
1000	FORMAT(//' LABELC: Image Manipulation Program  V1.22'/)
1011	format(' selects many small areas and puts them into',
     .  ' a file stack')
C
C	WRITE(6,1100)
c1100	FORMAT('$Input filename:  ')
c	READ(5,1200) INFILE
1200	FORMAT(A)

	CALL IMOPEN(1,INFILE,'RO')
C
	CALL IRDHDR(1,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
      	IF(MODE.LT.0.OR.MODE.GT.2)
     .       STOP ' only MODE 0,1 or 2 allowed for this option'
	write(6,1000)
	write(6,1011)
C
	  WRITE(6,1500)
1500	  FORMAT('$Output filename:  ')
	  READ(5,1200) OUTFILE
C
	WRITE(6,1600)
1600	FORMAT(/,'$Enter size of mini boxes to be selected, NBOXX,NBOXY ')
	READ(5,*) NBOXX, NBOXY
      	IF(NBOXX*NBOXY.GT.NDIM) STOP ' NBOXX*NBOXY too small for NDIM'
      	NBOXX  = (NBOXX/2)*2
      	NHBOXX =  NBOXX/2
      	NBOXY  = (NBOXY/2)*2
      	NHBOXY =  NBOXY/2
	WRITE(6,1610) NBOXX, NBOXY
1610	FORMAT(' Selected areas are',I5,' x'I5)
C  better to be an even number
	  WRITE(6,1700)
1700	  FORMAT('$filename for list of X,Y centres of areas required : ')
	  READ(5,1200) DATAFILE
C*** jms 17.06.2010
	  open(unit=10,file=datafile,status='old')
CC      	  OPEN(UNIT=10,NAME=DATAFILE,STATUS='OLD')
      	  READ(10,1200) TITLELINE
1701	  FORMAT(' First (title) line discarded',A)
      	  WRITE(6,1701) TITLELINE
      	DO 1800 IA=1,NAREA
      	 READ(10,*,END=1801) IAX(IA),IAY(IA)
1800	CONTINUE
      	STOP ' Too many selected areas for program dimension'
1801	IF(IA.GT.1) THEN
      	 NAREAS = IA-1
		NXYZBOX(1) = NBOXX
		NXYZBOX(2) = NBOXY
      	 NXYZBOX(3) = NAREAS
      	 NXYZTMP(1) = NBOXX*NBOXY
      	 NXYZTMP(2) = NAREAS
      	 NXYZTMP(3) = 1
      	 WRITE(6,1805)NAREAS
1805		FORMAT(' Number of particles selected',I6)
      	ENDIF
C
	CALL IMOPEN(2,OUTFILE,'NEW')
	CALL ITRHDR(2,1)
      	CALL IALSIZ(2,NXYZTMP,NXYZST)
	DMIN =  1.E10
	DMAX = -1.E10
	DOUBLMEAN = 0.0
	CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)
C
	iskip = 0
	DO 1950 IA = 1,NAREAS
      	 NX1 = IAX(IA) - NHBOXX + 1
      	 NX2 = IAX(IA) + NHBOXX
      	 NY1 = IAY(IA) - NHBOXY + 1
      	 NY2 = IAY(IA) + NHBOXY
      	 IF (NX1.LT.0.OR.NX2.GT.NXYZ(1)-1.OR.
     .              NY1.LT.0.OR.NY2.GT.NXYZ(2)-1) THEN
      	  WRITE(6,1901) IAX(IA),IAY(IA)
1901		 FORMAT('  particle too near edge ignored, coords',2I7)
		 iskip = iskip + 1
		else
      	  WRITE(6,*) NX1,NX2,NY1,NY2
      	  CALL IMPOSN(1,0,0)
      	  CALL IRDPAS(1,ARRAY,NBOXX,NBOXY,NX1,NX2,NY1,NY2,*999)
      	  CALL IWRLIN(2,ARRAY)
      	  DO 1960 J=1,NBOXX*NBOXY
      	   VAL = ARRAY(J)
      	   IF (VAL .LT. DMIN) DMIN = VAL
      	   IF (VAL .GT. DMAX) DMAX = VAL
      	   DOUBLMEAN = DOUBLMEAN + VAL
1960		 CONTINUE
		end if
1950	CONTINUE
	DMEAN = DOUBLMEAN/(NAREAS*NBOXX*NBOXY)
	nxyzbox(3) = nxyzbox(3) - iskip
      	CALL IALSIZ(2,NXYZBOX,NXYZST)
        CALL FDATE(DAT)
	write(tmptitle,1900) NAREAS-ISKIP,NBOXX,NBOXY,DAT(5:24)
1900    FORMAT('LABELC: select',I6,
     .   '  particles each with area',2I4,5X,A20)
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
990	CALL IMCLOSE(2)
995	CALL IMCLOSE(1)
	CALL EXIT
999	STOP 'END-OF-FILE ERROR ON READING input image'
	END

        SUBROUTINE ZERO(A,N)
C       ====================
C
C ZERO N BYTES OF A
C
        BYTE A(N)
        DO 1 I=1,N
1       A(I)=0
        RETURN
        END
