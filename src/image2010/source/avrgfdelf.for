C  AVRGFDELF program - SPECIFIC FOR SPACE GROUP P3, in subroutine_permute
C
C  TWO SETS OF DIFFERENCES CAN BE READ IN SO OUTPUT IS A DOUBLE-DIFFERENCE.
C	VX 1.0      08.6.89       RH.
C	VX 1.1      25.4.90       RH.	Option to have no native films added.
C	VX 1.2       6.2.92       RH.	Calculates mean deltaf/f in and output
C	VX 1.3      11.4.92       RH.	More films.
C	VX 2.0      16.4.92       RH.	Include averaged Fs and DelFs in O/P.
C	VX 3.0      21.3.94       RH.	Converted to UNIX general version
C	VX 3.1      23.2.95       RH	increase to use up to 50 films
C	VX 3.2      22.4.96	  JMS   removed READONLY from open statement
C	VX 4.0     29.12.97	  RH    read scale factor:  FDER - SCALE*FNAT
C	VX 4.1       4.1.01	  TSH   separate PARAMETER lines
C
C  input  -
C	CARD 1.    NFNAT, NFDER     -  Number of films for native and derivative
C                                       to be averaged and subtracted.
C	                                NFNAT can be zero.
C	CARD 2.    ZMIN, ZMAX       -  minimum and maximum zstar for average.
C	CARD 3.    N1....NN         -  Fortran unit numbers for native data
C       CARD 3A.   WN1...WNN        -  Weights for native data (can be all 1.0)
C	CARD 4.    D1....DN         -  Fortran unit numbers for derivative data
C       CARD 4A.   WD1...WDN        -  Weights for derivative data.
C       CARD 5.    TITLE            -  Title to be used on output file on unit 3
C       CARD 6.    SCALE            -  Overall scale factor
C
C  output -  will be to UNIT 3, same format as input with 5 dummy numbers at end
C             to maintain compatibility with Tom Ceska's EDLCFKPOS program.
C
C-------------------------------------------------------------------------------
      PARAMETER (MAXIND=20)
      PARAMETER (MAXOBS=150)
      PARAMETER (MAXFILM=MAXOBS/3) ! P3 SPECIFIC
      DIMENSION  FNAT(1:MAXIND,0:MAXIND,1:MAXOBS)
      DIMENSION  FDER(1:MAXIND,0:MAXIND,1:MAXOBS)
      DIMENSION DFNAT(1:MAXIND,0:MAXIND,1:MAXOBS)
      DIMENSION DFDER(1:MAXIND,0:MAXIND,1:MAXOBS)
      DIMENSION SIGDELFNAT(1:MAXIND,0:MAXIND,1:MAXOBS)
      DIMENSION SIGDELFDER(1:MAXIND,0:MAXIND,1:MAXOBS)
      DIMENSION DBLDELF(1:MAXIND,0:MAXIND)
      DIMENSION FDERIV1(1:MAXIND,0:MAXIND)
      DIMENSION FDERIV2(1:MAXIND,0:MAXIND)
      DIMENSION SIGFD1(1:MAXIND,0:MAXIND)
      DIMENSION SIGFD2(1:MAXIND,0:MAXIND)
      DIMENSION NN(MAXFILM),ND(MAXFILM),TITLE(20)
      DIMENSION WTN(MAXFILM),WTD(MAXFILM)
C
      WRITE(6,1)
1     FORMAT('  AVRGFDELF VX-4.1 (4.1.01) '/
     .	' Program to average native and',
     .	' derivative Fs and delFs and subtract the two')
      DUM=0.0
      ZDUM=0.0
      	 DO 40 I=1,MAXIND
      	 DO 40 J=0,MAXIND
      	 DO 40 K=1,MAXOBS
      	 DFNAT(I,J,K)=9999 ! to mark absence
40    	 DFDER(I,J,K)=9999 ! to mark absence
      READ(5,*) NFNAT,NFDER
      IF(NFNAT.GT.MAXFILM.OR.NFDER.GT.MAXFILM) THEN
      	WRITE(6,2)
2	FORMAT(' TOO MANY FILMS FOR PROGRAM DIMENSIONS')
      	STOP
      ENDIF
      IF(NFDER.LE.0) THEN
      	WRITE(6,3)
3	FORMAT(' MUST HAVE AT LEAST 1 DERIVATIVE FILM')
      	STOP
      ENDIF
      WRITE(6,12) NFNAT,NFDER
12    FORMAT(' Number of native and derivative films to be read in',2I5)
      READ(5,*) ZMIN,ZMAX
      WRITE(6,13) ZMIN,ZMAX
13    FORMAT(' Maximum and minimum zstar to be included',2F10.5)
      IF(NFNAT.GT.0) THEN
      	 READ(5,*)(NN(I),I=1,NFNAT)
      	 READ(5,*)(WTN(I),I=1,NFNAT)
      ENDIF
      	 READ(5,*)(ND(I),I=1,NFDER)
      	 READ(5,*)(WTD(I),I=1,NFDER)
      READ(5,399)TITLE ! FOR OUTPUT
      WRITE(3,399)TITLE ! ON OUTPUT FILE
      WRITE(3,398) ! TWO TITLE LINES ON OUTPUT FILE - AS EDLCF INPUT.
398   FORMAT('    IH    IK ZSTARZERO     DBLDELF     FDERIV1',
     .	 '      SIGFD1     FDERIV2      SIGFD2        FDUM')
399   FORMAT(20A4)
      SCALE=0.0   !
      READ(5,*,END=16) SCALE ! to be used as FDER-SCALE*FNAT
16    IF(SCALE.EQ.0.0) SCALE=1. !
      WRITE(6,15) SCALE
15    FORMAT(' Scale factor for subtraction was ',F10.3)
C
C
C
      IF(NFNAT.GT.0) THEN
      	DO 200 IFILM=1,NFNAT
      	  CALL READIN(IFILM,NN(IFILM),DFNAT,FNAT,SIGDELFNAT,ZMIN,ZMAX)
200   	CONTINUE
      ENDIF
      DO 300 IFILM=1,NFDER
      	 CALL READIN(IFILM,ND(IFILM),DFDER,FDER,SIGDELFDER,ZMIN,ZMAX)
300   CONTINUE
C
C
C   NOW AVERAGE NATIVE AND DERIVATIVE AND SUBTRACT, PUT RESULT INTO DBLDELF,
C	AND WRITE OUT.
C
      	SUMFD=0.   ! for statistics
      	SUMFN=0.
      	SUMDFN=0.
      	SUMDFD=0.
      	SUMDBLDELF=0.

      NOUT=0
      DO 400 IH=1,MAXIND
      DO 400 IK=0,MAXIND
      WNUMN=0  ! weights total native
      WNUMD=0  !    "      "   deriv
      DNTOT=0.0   ! deltaF native total
      DDTOT=0.0    ! deltaF deriv   "
      FNTOT=0.0  ! F native total
      FDTOT=0.0  ! F deriv   "
      SNTOT=0.0    ! sigmaF native total
      SDTOT=0.0   ! sigmaF deriv    "
      DO 380 IOBS=1,MAXOBS
      IF(DFNAT(IH,IK,IOBS).NE.9999) THEN
C			comment	  IOBS=IHEX+(IFILM-1)*3	! formula to calc. IOBS
      	   IFILM=1+(IOBS-1)/3
      	 WNUMN=WNUMN+WTN(IFILM)
      	 DNTOT=DNTOT+DFNAT(IH,IK,IOBS)*WTN(IFILM)
      	 FNTOT=FNTOT+ FNAT(IH,IK,IOBS)*WTN(IFILM)
      	 SNTOT=SNTOT+ SIGDELFNAT(IH,IK,IOBS)**2*WTN(IFILM)
      ENDIF
      IF(DFDER(IH,IK,IOBS).NE.9999) THEN
C				IOBS=IHEX+(IFILM-1)*3	! formula to calc. IOBS
      	   IFILM=1+(IOBS-1)/3
      	 WNUMD=WNUMD+WTD(IFILM)
      	 DDTOT=DDTOT+DFDER(IH,IK,IOBS)*WTD(IFILM)
      	 FDTOT=FDTOT+ FDER(IH,IK,IOBS)*WTD(IFILM)
      	 SDTOT=SDTOT+ SIGDELFDER(IH,IK,IOBS)**2*WTD(IFILM)
      ENDIF
380   CONTINUE
C
C  NEXT FEW LINES WHEN NATIVE AND DERIVATIVE DELTAFs BOTH PRESENT
      IF(WNUMN.NE.0.0.AND.WNUMD.NE.0.0) THEN
      	 DBLDELF(IH,IK)=(DDTOT/WNUMD) - SCALE*(DNTOT/WNUMN)
      	 FDERIV1(IH,IK)=(FDTOT/WNUMD)
      	 FDERIV2(IH,IK)=(FNTOT/WNUMN)
      	 SIGFD1(IH,IK) =SQRT(SDTOT/WNUMD)
      	 SIGFD2(IH,IK) =SQRT(SNTOT/WNUMN)
      	 WRITE(3,381) IH,IK,ZDUM,DBLDELF(IH,IK),
     .	  FDERIV1(IH,IK),SIGFD1(IH,IK),
     .	  FDERIV2(IH,IK),SIGFD2(IH,IK),DUM
381		FORMAT(2I6,F10.4,6F12.4)
      	  SUMFD=SUMFD+ABS(FDTOT/WNUMD)   ! for statistics
      	  SUMFN=SUMFN+ABS(FNTOT/WNUMN)
      	  SUMDFN=SUMDFN+ABS(DNTOT/WNUMN)
      	  SUMDFD=SUMDFD+ABS(DDTOT/WNUMD)
      	  SUMDBLDELF=SUMDBLDELF+ABS(DBLDELF(IH,IK))
      	 NOUT=NOUT+1
      ENDIF
C
C  NEXT 5 LINES WHEN ONLY DERIVATIVE DELTAFs PRESENT.
      IF(NFNAT.EQ.0.AND.WNUMN.EQ.0.0.AND.WNUMD.NE.0.0) THEN
      	 DBLDELF(IH,IK)=(DDTOT/WNUMD)
      	 FDERIV1(IH,IK)=(FDTOT/WNUMD)
      	 SIGFD1(IH,IK) =SQRT(SDTOT/WNUMD)
      	 WRITE(3,381) IH,IK,ZDUM,DBLDELF(IH,IK),
     .	  FDERIV1(IH,IK),SIGFD1(IH,IK),DUM,DUM,DUM
      	  SUMFD=SUMFD+ABS(FDTOT/WNUMD)   ! for statistics
      	  SUMDFD=SUMDFD+ABS(DDTOT/WNUMD)
      	 NOUT=NOUT+1
      ENDIF
C
400   CONTINUE
      	 RDFN=0.0
      	       IF(SUMFD.GT.0.0) RDFD   =SUMDFD/SUMFD
      	       IF(SUMFN.GT.0.0) RDFN   =SUMDFN/SUMFN
      	       RDBLDELF=SUMDBLDELF*2.0/(SUMFN+SUMFD)
      WRITE(6,401) NOUT
401   FORMAT(/' THERE WERE A TOTAL OF',I8,'   SPOTS OUTPUT')
      WRITE(6,402) RDFD,RDFN,RDBLDELF
402   FORMAT(/' The mean deltaf/f for der,nat,doublediff =',3F6.3/)
      END
C*******************************************************************************
      SUBROUTINE READIN(IFILM,NIN,DF,F,SIGDELF,ZMIN,ZMAX)
      PARAMETER (MAXIND=20)
      PARAMETER (MAXOBS=150)
      DIMENSION  F(1:MAXIND,0:MAXIND,1:MAXOBS)
      DIMENSION DF(1:MAXIND,0:MAXIND,1:MAXOBS)
      DIMENSION SIGDELF(1:MAXIND,0:MAXIND,1:MAXOBS)
      DIMENSION TITLE(10)
      	NREAD=0
      	NGOOD=0
c      	OPEN(UNIT=NIN,READONLY,STATUS='OLD')
      	OPEN(UNIT=NIN,STATUS='OLD')
	READ(NIN,11)TITLE
11	FORMAT(10A4)
	WRITE(6,14) TITLE
14	FORMAT(//' TITLE OF DELTAF list',10A4)
	WRITE(6,16) ZMIN,ZMAX
16	FORMAT(' RANGE OF Z VALUES FOR INCLUSION IN AVERAGE',2F10.4)
	READ(NIN,*)  ! SKIP COLUMN HEADERS
19		READ(NIN,20,END=49)IH,IK,Z,DELTAF,FD,SIGFD,FP,SIGFP,ANOM
20		FORMAT(2I6,F10.4,6F12.4)
		NREAD=NREAD+1
      	 IF(Z.GE.ZMIN.AND.Z.LE.ZMAX) THEN
			NGOOD=NGOOD+1
      	  CALL PERMUTE(IH,IK,IHEX)
      	  IF(IH.LE.MAXIND.OR.IK.LE.MAXIND) THEN
      	   KOBS=IHEX+(IFILM-1)*3
      	   DF(IH,IK,KOBS)=DELTAF
      	   F(IH,IK,KOBS)=FD
      	   SIGDELF(IH,IK,KOBS)=SQRT(SIGFD**2+SIGFP**2)
      	  ELSE
      	   WRITE(6,52)IH,IK
      	  ENDIF
      	 ENDIF
      	  GO TO 19
49	WRITE(6,50) IFILM,NREAD,NGOOD
      	CLOSE(NIN)
50	FORMAT(' ON FILM',I6,'   THERE WERE',I6,'  SPOTS INPUT'/
     .	  ' WITH',I6,'  HAVING ZSTAR WITHIN RANGE'/)
52	FORMAT(' INDEX TOO LARGE FOR PROGRAM DIMENSIONS',2I5)
      RETURN
      END
C*******************************************************************************
      SUBROUTINE PERMUTE(IH,IK,IHEX)
C		P3 SPECIFIC PERMUTATION OF INDICES
      II=-(IH+IK)
      IHOUT=IH
      IKOUT=IK
C
      IHEX=1
      	IF(IHOUT.GT.0.AND.IKOUT.GE.0) THEN
      	 IH=IHOUT
      	 IK=IKOUT
      	 RETURN
      	ENDIF
      IHOUT=-IH
      IKOUT=-IK
      	IF(IHOUT.GT.0.AND.IKOUT.GE.0) THEN
      	 IH=IHOUT
      	 IK=IKOUT
      	 RETURN
      	ENDIF
C
      IHOUT=IK
      IKOUT=II
      IHEX=2
      	IF(IHOUT.GT.0.AND.IKOUT.GE.0) THEN
      	 IH=IHOUT
      	 IK=IKOUT
      	 RETURN
      	ENDIF
      IHOUT=-IK
      IKOUT=-II
      	IF(IHOUT.GT.0.AND.IKOUT.GE.0) THEN
      	 IH=IHOUT
      	 IK=IKOUT
      	 RETURN
      	ENDIF
      IHOUT=II
      IKOUT=IH
      IHEX=3
      	IF(IHOUT.GT.0.AND.IKOUT.GE.0) THEN
      	 IH=IHOUT
      	 IK=IKOUT
      	 RETURN
      	ENDIF
      IHOUT=-II
      IKOUT=-IH
      	IF(IHOUT.GT.0.AND.IKOUT.GE.0) THEN
      	 IH=IHOUT
      	 IK=IKOUT
      	 RETURN
      	ENDIF
      WRITE(6,100)
100   FORMAT(' SOMETHING BADLY WRONG')
      STOP
      END
