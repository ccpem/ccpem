C     HLXPROJ
C	Version 1.0			RAC
C       Version 2.0   imsubs2000 conversion  RAC  13Nov00
C
C     PROJECT HELICAL DATA SIDEWAYS BY 2-D TRANSFORM OF LAYER LINES
C
C     UNIT 1 layer line data
C     UNIT 2 output image file
C     UNIT 5 input control data
C
C     Data :
C     TITLE  (20A4)
C
C     LLTOT,C,DELBR  (*)
C
C     RMAX,DELSR,IRSTEP,IJUMP,NV (*)
C
C     WT(I),I=1,LLTOT (*)
C
C     PHI(K),K=1,NV (*)
C
C     ZMIN,ZMAX,DELZED,DMIN (*)
C
C     FILIN (A)
C
C*********************************************************************
C
C     LLTOT number of separate layer lines in a single data set
C
C     C axial repeat of helical particle
C
C     DELBR spacing of data points along layer line
C
C     RMAX maximum radius of particle
C
C     DELSR radial separation between calculated density points in
C           real space.
C
C     IRSTEP intervals (in steps of DELBR) at which data are to be
C            included in the calculation. If = 1, every point along
C            the layer line is used, if = 2, alternate points etc.
C            The calculation may be very time consuming if all the
C            layer line points are used to calculate points in a
C            large filtered image array, and layer lines are often
C            sampled much more finely than necessary, especially
C            for this purpose.
C     IJUMP optional control: outputs one-sided filtered images for
C           negative values, double-sided images for positive values.
C           N.B. Filtered image is always a far side, if one sided
C           option selected, irrespective of input data.
C           If=+-1, prog thinks it is receiving 'near-side' data,
C             =+-2, prog thinks data is 'far-side',
C             =+-3, prog expects 'near-side' followed by 'far-side'
C                   on 2 separate files
C
C     NV number of different filtered images to be calculated from
C        the data set, showing views from different angles around
C        the helix axis.NV must not not exceed 20.
C
C     WT weighting factors for each layer line
C
C     PHI required view angles in degrees
C
C     ZMIN,ZMAX,DELZED lower & upper values of axial distance z
C                      required for the filtered image, and the
C                      interval between sample points.
C
C     DMIN   some sort of background added to final rho.
C
C     FILIN Input filename for layer line data on unit 1, two of
C           these if ABS(IJUMP) = 3
C
C     CONSTRAINTS :
C     -----------
C
C     RMAX/DELSR * (ZMAX-ZMIN)/DELZED must not exceed 50000
C
C     up to 40 layer lines, up to 200 steps in each layer line
C
C**************************************************************************
C
      DIMENSION TITLE(20),WT(40),NN(40),NL(40),A(40,200),
     .B(40,200),IBMAX(40),PHI(20),NXYZ(3)
      REAL*8 HEAD(10)
      CHARACTER FILIN*40
      COMMON PHI,A,B,NN,NL,IBMAX,ZMIN,ZMAX,DELZED,RMAX,DELSR,C,LLTOT,
     .DMIN,TITLE,DELBR,IRSTEP,IJUMP
      COMMON//NY,NZ,NV
      EQUIVALENCE(NY,NXYZ)
C
      CALL  IMOPEN(2,'OUT','NEW')
      READ(5,1009) TITLE
      WRITE(6,1002) TITLE
      READ(5,*) LLTOT,C,DELBR
      WRITE(6,1008) LLTOT,C,DELBR
      READ(5,*) RMAX,DELSR,IRSTEP,IJUMP,NV
      WRITE(6,1005) RMAX,DELSR,IRSTEP,IJUMP,NV
      IF(IRSTEP.LE.0) IRSTEP=1
      READ(5,*) (WT(L),L=1,LLTOT)
      WRITE(6,1004) (WT(L),L=1,LLTOT)
      READ(5,*) (PHI(IV),IV=1,NV)
      WRITE(6,1004) (PHI(IV),IV=1,NV)
      READ(5,*) ZMIN,ZMAX,DELZED,DMIN
      WRITE(6,1004) ZMIN,ZMAX,DELZED,DMIN
      READ(5,1007) FILIN
      WRITE(6,1007) FILIN
      CALL  CCPDPN(1,FILIN,'READONLY','F',0,0)
      PI=3.14159
      PIBY2=PI/2.0
      CRAD=PI/180.0
      IRMAX=0
      DO 40 L=1,40
   40 IBMAX(L)=0
      NB=1
      ISIDE=1
      IF(IABS(IJUMP).EQ.2) ISIDE=2
C
C     READ IN LAYER LINE DATA - F'S ARE FROM Y,Z PLANE OF TRANSFORM SPAC
C
    9 L=1
      WRITE(6,1000)
      GO TO 2
    1 IF(IR.GT.IBMAX(L)) IBMAX(L)=IR
      IF(IR.GT.IRMAX) IRMAX=IR
      L=L+1
      IF(L.GT.LLTOT) GO TO 499
    2 READ(1,1014,END=500) HEAD,SCALE,NN(L),NL(L)
      WRITE(6,1016) HEAD,SCALE,NN(L),NL(L)
      MODN=IABS(NN(L))
      PHIDEL=MOD(MODN,2)*PI
      IF(ISIDE.EQ.2.AND.IABS(IJUMP).EQ.3) GO TO 51
C
C     INITIALIZATION OF LAYER LINE DATA
   21 DO 50 IR=1,200
      A(L,IR)=0.0
   50 B(L,IR)=0.0
C
   51 IR=-1
C
    3 READ(1,1006,END=500) RR,GG,PSI
      IF(IR.GE.0.AND.RR.LE.0.0) GO TO 1
    6 IR=RR/DELBR+1.1
      IF(IR.GT.200) GO TO 3
      IF(NN(L).NE.0.OR.NL(L).NE.0) GO TO 58
C     Make J0 on equator real
      I180=(ABS(PSI)+90.0)/180.0
      A(L,IR)=A(L,IR)+GG*(-1.0)**I180
      PSI=MOD(I180,2)*180.0
      GO TO 59
   58 PHI1=PSI*CRAD
C     Convert near side data to far side - odd Bessels only
      IF(ISIDE.EQ.1) PHI1=PHI1-PHIDEL
      A(L,IR)=A(L,IR)+GG*COS(PHI1)*SCALE*WT(L)
      B(L,IR)=B(L,IR)+GG*SIN(PHI1)*SCALE*WT(L)
   59 CONTINUE
C      WRITE(6,1012) RR,GG,PSI,A(L,IR),B(L,IR)
      GO TO 3
  499 READ(1,1009,END=500) T
      GO TO 499
C
  500 IF(L-1.LT.LLTOT) LLTOT=L-1
      IF(IABS(IJUMP).LT.3) GO TO 501
      IF(ISIDE.EQ.2) GO TO 501
      ISIDE=2
      CLOSE(1)
      READ(5,1007) FILIN
      CALL  CCPDPN(1,FILIN,'READONLY','F',0,0)
      GO TO 9
  501 CALL IMAGE
      STOP
C
 1014 FORMAT(10A5,F10.3,2I5)
 1016 FORMAT('0',10A5,F10.3,2I5,5X,F10.3)
 1012 FORMAT(1X,F12.6,4F12.2)
 1000 FORMAT(' List of layer-lines'/)
 1002 FORMAT(1X,20A4)
 1004 FORMAT(1X,8F10.2)
 1005 FORMAT(2F10.2,3I10)
 1006 FORMAT(3E10.3)
 1007 FORMAT(A)
 1008 FORMAT(I10,2F10.2)
 1009 FORMAT(20A4)
      END
      SUBROUTINE IMAGE
C
      COMMON PHI,A,B,NN,NL,IBMAX,ZMIN,ZMAX,DELZED,RMAX,DELSR,C,LLTOT,
     .DMIN,TITLE,DELBR,IRSTEP,IJUMP
      COMMON//NY,NZ,NV
      DIMENSION RHO(50000),NXYZ(3)
      EQUIVALENCE (NY,NXYZ)
      DIMENSION PHI(20),NN(40),NL(40),A(40,200),B(40,200),IBMAX(40),
     .TITLE(20)
C
      PI=3.14159
      RADIAN=PI/180.0
      TWOPI=2.0*PI
      NZ=(ZMAX-ZMIN)/DELZED+1.1
      NY1=RMAX/DELSR+0.1
      NY=NY1*2+1
      NYZ = NY*NZ
C
C     check to see if dimensions fit
C
      IF(NYZ.GE.50000) THEN
      WRITE(6,1007) NYZ
      STOP
      END IF
C
      DBMIN = 1.E7
      DBMAX = 0.
      DBMEAN = 0.
C
      CALL  ICRHDR(2,NXYZ,NXYZ,2,TITLE,1)
      CALL  IWRHDR(2,TITLE,-1,DBMIN,DBMAX,DBMEAN)
C
      DO 500 IV=1,NV
      PHI1=PHI(IV)*RADIAN
      PHI2=PHI1+PI
      DO 50 IYZ=1,NYZ
      RHO(IYZ)=0.0
   50 CONTINUE
      DO 300 LL=1,LLTOT
      N=NN(LL)
      CNP1=COS(N*PHI1)
      SNP1=SIN(N*PHI1)
      CNP2=COS(N*PHI2)
      SNP2=SIN(N*PHI2)
      L=NL(LL)
      BZ=L/C
      IRMAX=IBMAX(LL)
      WTL=2.0
      IF(L.EQ.0) WTL=1.0
      SWT=1.0
      IF(IJUMP.LT.0) SWT=0.0
C
      DO 300 IR=2,IRMAX,IRSTEP
      BR=(IR-1)*DELBR
      A1=A(LL,IR)*CNP1+B(LL,IR)*SNP1
      A2=(A(LL,IR)*CNP2+B(LL,IR)*SNP2)*SWT
      B1=0.0
      B2=0.0
      IF(N.EQ.0.AND.L.EQ.0) GO TO 51
      B1=-A(LL,IR)*SNP1+B(LL,IR)*CNP1
      B2=(-A(LL,IR)*SNP2+B(LL,IR)*CNP2)*SWT
C
   51 ZARG1=TWOPI*BZ*ZMIN
      YARG1=-TWOPI*BR*DELSR*NY1
      DZARG=TWOPI*BZ*DELZED
      DYARG=TWOPI*BR*DELSR
      CZ1=COS(ZARG1)
      SZ1=SIN(ZARG1)
      CY1=COS(YARG1)
      SY1=SIN(YARG1)
      CDZ=COS(DZARG)
      SDZ=SIN(DZARG)
      CDY=COS(DYARG)
      SDY=SIN(DYARG)
C
      DO 300 IZ=1,NZ
      CC1=CZ1*CY1-SZ1*SY1
      SS1=CZ1*SY1+SZ1*CY1
      CC2=CZ1*CY1+SZ1*SY1
      SS2=-CZ1*SY1+SZ1*CY1
      DO 400 IY=1,NY
      IYZ = NY * (IZ-1) + IY
      RHO(IYZ)=RHO(IYZ)+WTL*(A1*CC1+B1*SS1+A2*CC2+B2*SS2)
      CC11=CC1*CDY-SS1*SDY
      SS11=CC1*SDY+SS1*CDY
      CC22=CC2*CDY+SS2*SDY
      SS22=-CC2*SDY+SS2*CDY
      CC1=CC11
      SS1=SS11
      CC2=CC22
      SS2=SS22
  400 CONTINUE
      CZ11=CZ1*CDZ-SZ1*SDZ
      SZ11=CZ1*SDZ+SZ1*CDZ
      CZ1=CZ11
      SZ1=SZ11
  300 CONTINUE
C
      IF(IV.GT.1) GO TO 101
      RHOMAX=0.0
      DO 100 IZ=1,NZ
      DO 100 IY=1,NY
      IYZ = NY * (IZ-1) + IY
      RHOM=ABS(RHO(IYZ))
      IF(RHOM.GT.RHOMAX) RHOMAX=RHOM
  100 CONTINUE
      SCALE=50.0/RHOMAX
  101 WRITE(6,1010) PHI(IV),TITLE
      DO 200 IZ=1,NZ
      DO 200 IY=1,NY
      IYZ = NY * (IZ-1) + IY
      RHO(IYZ)=RHO(IYZ)*SCALE+DMIN
  200 CONTINUE
      CALL  ICLDEN(RHO,100,120,1,NY,1,NZ,DCMIN,DCMAX,DCMEAN)
      IF(DCMIN.LT.DBMIN) DBMIN = DCMIN
      IF(DCMAX.GT.DBMAX) DBMAX = DCMAX
      DBMEAN = DBMEAN + DCMEAN
      CALL  IWRSEC(2,RHO)
  500 CONTINUE
      DBMEAN = DBMEAN / NV
      CALL  IWRHDR(2,TITLE,-1,DBMIN,DBMAX,DBMEAN)
      CALL  IMCLOSE(2)
      WRITE(6,1012) DBMIN,DBMAX,DBMEAN
      RETURN
C
 1007 FORMAT(/' Too many points asked for : NY * NZ =',I10)
 1008 FORMAT(/1X,44I3)
 1009 FORMAT(40I3)
 1010 FORMAT(//' PHI=',F8.1,5X,20A4)
 1012 FORMAT(/' min, max, mean densities : ',3F10.2/)
      END
