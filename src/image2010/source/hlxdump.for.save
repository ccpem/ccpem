C*HLXDUMP.FOR************************************************************
C     Program to plot and dump layer line data for helical particles
C       Input transform on stream 1 (IN)
C       File of layer line data on stream 2 (LLDAT1)  Near side
C       File of layer line data on stream 3 (LLDAT2)  Far side
C
C       COMPILE  +LLGRAPH     THEN  PIMLINK
C       VERSION 1.01    12NOV82   RAC  FOR VAX
C	VERSION 1.02    12FEB98   JMS  plotting range/dimensions increased
C	VERSION 1.03    09SEP99   JMS  izero read in to dump all data
C					or only non-zero data
C       VERSION 2.00    12NOV00   RAC  	Updated to IMAGE2000
C
C
      PARAMETER (MAXSIZ = 5000)
      PARAMETER (RLMAX = 0.5)
      COMMON//NX,NY,NZ,NXP2,KY,CONVX,CONVY,DELPX,DELPY,
     1 ARRAY(2101248)
      DIMENSION ARRF(MAXSIZ),ARRP(MAXSIZ),RECSP(MAXSIZ)
      DIMENSION TITLE(10),NXYZ1(3),MXYZ(3),SIDE(2)
      EQUIVALENCE (NX,NXYZ1)
      LOGICAL LAST
CTSH      DATA SIDE(1)/'NEAR'/,SIDE(2)/'FAR '/
CTSH++
      CHARACTER*4 TMPSIDE(2)
      EQUIVALENCE (TMPSIDE,SIDE)
      DATA TMPSIDE(1)/'NEAR'/,TMPSIDE(2)/'FAR '/
CTSH--
      DATA TWOPI/6.283185/,DEG/57.2958/,CRAD/0.0174532/,ZERO/0./
C     Set range for plot in rec Ang.  Together with parameters in
C     LLGRAPH sets scale of 1mm = .0004 rec Ang.
C
      WRITE(6,1000)
1000  FORMAT('1','  HLXDUMP v2.00 : Program to dump layer line data'//)
C
      CALL IMOPEN(1,'IN','RO')
      CALL IRDHDR(1,NXYZ1,MXYZ,MODE,DMIN,DMAX,DMEAN)
      CALL IRTORG(1,XOR,YOR,ZOR)
      WRITE(6,1050) XOR,YOR
1050  FORMAT(//'  Origin stored with transform',2F10.2)
C
      NXP2=2*NX
      NXM1=NX-1
      NX2=2*NXM1
C     Line length in transposed transform in reals
      KX=2*NXM1+1
C     No of lines in transposed transform
      KY=NY/2
C
      CALL IRDSEC(1,ARRAY,*99)
C
      READ(5,1060) TITLE
1060  FORMAT(10A4)
      WRITE(6,1070) TITLE
1070  FORMAT(///' Title of plot - ',10A4)
      READ(5,*) LLTOT,SAMPL,XSHFT,YSHFT,DELR,ROT,SHEAR,TILT
      WRITE(6,1080) LLTOT,SAMPL,XSHFT,YSHFT,DELR,ROT,SHEAR,TILT
1080  FORMAT(///' Total no of layer lines ',I10,
     1         /' Real space sample size  ',F10.2,' Angstroms',
     2         /' X phase origin shift    ',F10.2,' steps',
     3         /' Y phase origin shift    ',F10.2,' steps',
     4         /' Layer line sampling     ',F10.6,' rec. Angstroms',
     5         /' Rotation angle          ',F10.2,' degrees',
     6         /' Shear angle             ',F10.2,' degrees',
     7         /' Tilt angle              ',F10.2,' degrees')
C
      READ(5,*) RMIN,RMAX,IDUMP,MERID,izero
      IF(ABS(RMIN) .GT. RLMAX .OR. RMAX .GT. RLMAX) THEN
	WRITE(6,'(
     *  ''ERROR - RMIN, RMAX range too great for program.'')')
	STOP
      END IF
C
      WRITE(6,1090) RMIN,RMAX,IDUMP,MERID,izero
1090  FORMAT(///' Limits for layer line plotting',F10.4,' to',F10.4,
     1            '  reciprocal Angstroms'/
     2         /' Graph only if 0, graph and dump if 1,
     3 dump only if 2             ',I10,/
     4         /' Both halves of meridionals to both sides if 0, one
     5 half if 1     ',I10,/
     6	       /' all layer line data if 0,
     7 no zero layerlines if 1    ',I10///)
C
      WRITE(6,1091)
1091  FORMAT('1'//)
C     Open layer line data files if required
      IF(IDUMP.NE.0) THEN
             WRITE(6,1092)
1092         FORMAT('  File names for layer line data')
             CALL CCPDPN(2,'LLDAT1','UNKNOWN','F',0,0)
             CALL CCPDPN(3,'LLDAT2','UNKNOWN','F',0,0)
      ENDIF
C     Set phase shift factors for single X or Y step
      DELPX=-TWOPI*(XOR+XSHFT)/NX2
      DELPY=-TWOPI*(YOR+YSHFT)/NY
C     Set conversion factors from transform samples to rec Angstroms
      CONVX=1./(NX2*SAMPL)
      CONVY=1./(NY*SAMPL)
      ROT=ROT*CRAD
      SHEAR=SHEAR*CRAD
      TILT=TILT*CRAD
      LAST=.FALSE.
C
      DO 100 LL=1,LLTOT
      FMAX=0.
      IF(LL.EQ.LLTOT) LAST=.TRUE.
      READ(5,*) LLNO,NORD,YSP
      IF(IDUMP.NE.0) READ(5,*) NL1,NL2,NR1,NR2
C
C     Spacing and centre of layer line in rec Ang corrected for
C     rotation and shear.  Also geometry of tilt.
      SPLL=YSP*CONVY*COS(ROT)/COS(SHEAR-ROT)
      CLLX=-SPLL*SIN(SHEAR)
      CLLY=SPLL*COS(SHEAR)
      D=SPLL*SIN(TILT)
      DSQ=D*D
C
      WRITE(6,1095) LLNO,NORD,YSP,SPLL
1095  FORMAT(//'  Layer line no.',I10,'  Bessel order',I10,
     1' Spacing',F10.2,' steps or',F10.6,' rec Angstroms')
      IF(IDUMP.NE.0) WRITE(6,1096) NL1,NL2,NR1,NR2
1096  FORMAT(//'         Dump left side points',I5,'  to',I5,
     1'    Right side points',I5,'  to',I5)
C     No of samples along layer line for plotting
      N1=RMIN/DELR
      N2=RMAX/DELR
      IF(1-NL2-N1 .LT. 0 .OR. 1+NR1-N1 .LT. 0) THEN
	WRITE(6,'(
     *  ''Error specifying RMIN/RMAX/DELBR, cannot continue'')')
	STOP
      END IF
      NPTS=N2-N1+1
      IF(NPTS .GT. MAXSIZ) THEN
	WRITE(6,'(
     *  ''Error - number of points too great for program.'')')
	STOP
      END IF
C     Sample along layer line
      DO 110 N=1,NPTS
      RECSP(N)=(N+N1-1)*DELR
      ARRF(N)=0.
      ARRP(N)=0.
      R=RECSP(N)
      IF(TILT.EQ.0.) GO TO 105
C     Correct for geometry of tilt
      IF(ABS(RECSP(N)).LT.ABS(D)) GO TO 110
      R=SQRT(RECSP(N)*RECSP(N)-DSQ)
      IF(RECSP(N).LT.0.) R=-R
105   X=CLLX+R*COS(ROT)
      Y=CLLY+R*SIN(ROT)
      CALL INTERPT(X,Y,A,B)
      IF(TILT.EQ.0..OR.SPLL.EQ.0.) GO TO 106
C     Correct for  phase error of tilt
      THETA=IABS(NORD)*ASIN(ABS(D/RECSP(N)))
      IF(TILT.LT.0.) THETA=-THETA
      CTH=COS(THETA)
      STH=SIN(THETA)
      IF(RECSP(N).LT.0) STH=-STH
      IF(NORD.LT.0) STH=-STH
      AA=A*CTH+B*STH
      BB=B*CTH-A*STH
      A=AA
      B=BB
C
106   if(a .eq. 0. .and. b .eq. 0.0) then
	arrf(n) = 0.
	arrp(n) = 0.
      else
       ARRF(N)=SQRT(A*A+B*B)
       IF(ARRF(N).GT.FMAX) FMAX=ARRF(N)
       ARRP(N)=ATAN2(B,A)*DEG
      end if
C
110   CONTINUE
C
      IF(IDUMP.LE.1) CALL LLGRAPH(ARRF,ARRP,RECSP,RMIN,RMAX,
     1 FMAX,LLNO,SPLL,NORD,NPTS,TITLE,LAST)
      IF(IDUMP.EQ.0) GO TO 100
C     Dump layer line data. Near to stream 2, far to stream 3
C     Set strean nos.  If NORD>0, LHS goes to stream 2 (Near) and
C     RHS to stream 3 (Far).  If NORD<0, RHS to stream 2 (Near) and
C     LHS to stream 3 (Far).  If NORD=0, both sides to both streams
C     at half weight, except for equator
      ISTR1=2
      ISTR2=3
      IF(NORD.GE.0) GO TO 190
180   ISTR1=3
      ISTR2=2
190   WTFAC=1.
      IF(NORD.EQ.0.AND.LLNO.NE.0.AND.MERID.EQ.0) WTFAC=0.5
C*** insert jms to stop zero lines being written
      if(izero .gt. 0) then
       IF(NORD .GE. 0) THEN
        IF(NL2 .GT. 0)
     *   WRITE(2,1100) TITLE,LLNO,SIDE(1),WTFAC,NORD,LLNO
        IF(NR2 .GT. 0)
     *   WRITE(3,1100) TITLE,LLNO,SIDE(2),WTFAC,NORD,LLNO
       ELSE
        IF(NR2 .GT. 0)
     *   WRITE(2,1100) TITLE,LLNO,SIDE(1),WTFAC,NORD,LLNO
        IF(NL2 .GT. 0)
     *   WRITE(3,1100) TITLE,LLNO,SIDE(2),WTFAC,NORD,LLNO
       END IF
C     Write LHS
       IF(NL2 .GT. 0) THEN
        DO N=1-NL1-N1,1-NL2-N1,-1
         WRITE(ISTR1,1110) ABS(RECSP(N)),ARRF(N),ARRP(N)
        END DO
C     Terminator
        WRITE(ISTR1,1110) ZERO,ZERO,ZERO
       END IF
C     Write RHS
       IF(NR2 .GT. 0) THEN
        DO N=1+NR1-N1,1+NR2-N1,1
         WRITE(ISTR2,1110) RECSP(N),ARRF(N),ARRP(N)
        END DO
C     Terminator
        WRITE(ISTR2,1110) ZERO,ZERO,ZERO
       END IF
C*** write all layer line data even if zeroes in layer line
      else
       WRITE(2,1100) TITLE,LLNO,SIDE(1),WTFAC,NORD,LLNO
       WRITE(3,1100) TITLE,LLNO,SIDE(2),WTFAC,NORD,LLNO
C     Write LHS
       DO 200 N=1-NL1-N1,1-NL2-N1,-1
200    WRITE(ISTR1,1110) ABS(RECSP(N)),ARRF(N),ARRP(N)
C     Write RHS
       DO 210 N=1+NR1-N1,1+NR2-N1,1
210    WRITE(ISTR2,1110) RECSP(N),ARRF(N),ARRP(N)
C     Terminator
       WRITE(2,1110) ZERO,ZERO,ZERO
       WRITE(3,1110) ZERO,ZERO,ZERO
      end if
1100  FORMAT(10A4,'LL',I2,1X,A4,1X,F10.3,2I5)
1110  FORMAT(3E10.3)
C
C     If NORD=0 repeat with streams interchanged,except equator
      IF(MERID.NE.0) GO TO 100
      IF(NORD.EQ.0.AND.LLNO.NE.0.AND.ISTR1.EQ.2) GO TO 180
C
100   CONTINUE
C
      CALL IMCLOSE(1)
      STOP
99    WRITE(6,1400)
1400  FORMAT(////'  End of data on stream 1')
      STOP
      END
C
C
C     Routine for bilinear interpolation in transform
C     at position X,Y in reciprocal Angstrom;  A,B real and
C     imaginary parts of transform corrected for phase origin
      SUBROUTINE INTERPT(X,Y,A,B)
      COMMON//NX,NY,NZ,NXP2,KY,CONVX,CONVY,DELPX,DELPY,
     1 ARRAY(2101248)
      IF(X.GE.0.) THEN
         XX=X/CONVX
         YY=Y/CONVY
                 ELSE
         XX=-X/CONVX
         YY=-Y/CONVY
      ENDIF
      IX=XX
      XFR=XX-IX
      IY=YY+KY
      YFR=YY-IY+KY
      XBAR=1.-XFR
      YBAR=1.-YFR
C     Indices and phase shifts for 4 nearest points
      IND=IY*NXP2+2*IX+1
      PSHFT=IX*DELPX+(IY-KY)*DELPY
      CALL SHIFT(A1,B1,PSHFT,IND,ARRAY)
      IND=IND+2
      PSHFT=PSHFT+DELPX
      CALL SHIFT(A2,B2,PSHFT,IND,ARRAY)
      IND=IND+NXP2
      PSHFT=PSHFT+DELPY
      CALL SHIFT(A4,B4,PSHFT,IND,ARRAY)
      IND=IND-2
      PSHFT=PSHFT-DELPX
      CALL SHIFT(A3,B3,PSHFT,IND,ARRAY)
      A=A1*XBAR*YBAR+A2*XFR*YBAR+A3*XBAR*YFR+A4*XFR*YFR
      B=B1*XBAR*YBAR+B2*XFR*YBAR+B3*XBAR*YFR+B4*XFR*YFR
      IF(X.LT.0.) B=-B
      RETURN
      END
C
C
C     Routine to return phase origin corrected transform value
      SUBROUTINE SHIFT(APART,BPART,PSHFT,IND,ARRAY)
      DIMENSION ARRAY(1)
      C=COS(PSHFT)
      S=SIN(PSHFT)
      APART=ARRAY(IND)*C-ARRAY(IND+1)*S
      BPART=ARRAY(IND)*S+ARRAY(IND+1)*C
      RETURN
      END
