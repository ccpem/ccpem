	SUBROUTINE PSCRIPT_IMAGE
C  routines to generate a postscript image file
C  the routines should be called in the following order:
C  CALL PSCRIPT_INIT		to initialise the postscript system
C  1  (OUTSTREAM,		INTEGER*4. fortran stream number for this file
C  1  PAPERSIZE,		INTEGER*4. 0=A4, 1=A3 
C  1  ORIENTATION,		INTEGER*4. 0=portrait, 1=landscape
C  1  XORIGIN,YORIGIN,		REAL.      plot origin relative to bottom
C					   l.h. corner of paper, in user-units
C  1  XSCALE,YSCALE,		REAL.      image scale. mm per user unit.
C					   (pixels for image)
C  1  RLE,			INTEGER*4. Run-Length-Encoding. 0=no, 1=yes.
C					   run-length-encoding can make for
C					   much shorter postscript files but
C					   doesnt work on all printers. OK
C					   for clc, not for laser5.


C  CALL PSCRIPT_COLOURTABLE	if its a colour plot
C  CALL PSCRIPT_xxxIMAGE	for each line of image. xxx= GREY or COLOUR
C  CALL PSCRIPT_ENDIMAGE	at the end of the image
C  CALL PSCRIPT_COPY		for each line of raw postscript required
C  CALL PSCRIPT_END		at job completion


	IMPLICIT	NONE
	INTEGER*4	BLUE(256)
	INTEGER*4	GREEN(256)
	INTEGER*4	I
	INTEGER*4	IOSTAT
	INTEGER*4	J
	INTEGER*4	K
	INTEGER*4	N
	INTEGER*4	NCOLS
	INTEGER*4	NROWS
	INTEGER*4	ORIENTATION     !0=portrait, 1=landscape
	INTEGER*4	OSTREAM
	INTEGER*4	PAPERSIZE       !0=A4, 1=A3
	REAL		XORIGIN
	REAL		YORIGIN
	REAL		XSCALE
	REAL		YSCALE
	INTEGER*4	RED(256)
	INTEGER*4	RLE             !0=dont run-length-encode it, 1=do.
	BYTE		ROW(1)
	CHARACTER	STRING*(*)      !string to copy thro'

C  local storage
	INTEGER*4	L_BLUE(256)
	  SAVE		L_BLUE
	LOGICAL		L_COLOUR
	LOGICAL		L_HEADERDONE
	  SAVE		L_HEADERDONE
	INTEGER*4	L_GREEN(256)
	  SAVE		L_GREEN
	INTEGER*4	L_NCOLS
	  SAVE		L_NCOLS
	INTEGER*4	L_NROWS
	  SAVE		L_NROWS
	INTEGER*4	L_NVALS
	  SAVE		L_NVALS
	INTEGER*4	L_ORIENTATION
	  SAVE		L_ORIENTATION
	INTEGER*4	L_OSTREAM
	  SAVE		L_OSTREAM
	CHARACTER	L_PAPERSIZE*16
	  SAVE		L_PAPERSIZE
	REAL		L_ORIGINOFFSET
	  SAVE		L_ORIGINOFFSET
	INTEGER*4	L_RED(256)
	  SAVE		L_RED
	LOGICAL		L_RLE
	  SAVE		L_RLE
	INTEGER*4	L_RLE_LIST(2,20)
	  SAVE		L_RLE_LIST
	REAL		L_XORIGIN
	  SAVE		L_XORIGIN
	REAL		L_YORIGIN
	  SAVE		L_YORIGIN
	REAL		L_XSCALE
	  SAVE		L_XSCALE
	REAL		L_YSCALE
	  SAVE		L_YSCALE


	INTEGER		FCHUNK
	CHARACTER	OLINE*4096
	INTEGER*4	NCHUNKS
	INTEGER*4	NEWPIX
	INTEGER*4	NREM
	INTEGER*4	PIXCOUNT
	INTEGER*4	PREVPIX

Ceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
	ENTRY PSCRIPT_INIT(
     *  OSTREAM,
     *  PAPERSIZE,ORIENTATION,
     *  XORIGIN,YORIGIN,XSCALE,YSCALE,
     *  RLE,NCOLS,NROWS)

	L_OSTREAM=OSTREAM
	L_PAPERSIZE=' '                 !default to A4
ccc	L_ORIGINOFFSET=8.25
	L_ORIGINOFFSET=575              !in 'points'
	IF (PAPERSIZE.EQ.1) THEN
	  L_PAPERSIZE='/a3 load exec'
	  L_ORIGINOFFSET=(11.5/8.25)*575
	END IF
	L_ORIENTATION=0
	IF (ORIENTATION.EQ.1) L_ORIENTATION=90
	L_XORIGIN=XORIGIN
	L_YORIGIN=YORIGIN
	L_XSCALE=XSCALE                 !convert mm scale to points
	L_YSCALE=YSCALE
	L_RLE=(RLE.NE.0)
	L_NCOLS=NCOLS
	L_NROWS=NROWS
	DO I=1,256
	  L_RED(I)=I-1
	  L_GREEN(I)=I-1
	  L_BLUE(I)=I-1
	END DO
	L_HEADERDONE=.FALSE.
	L_NVALS=0
	WRITE(L_OSTREAM,1000)
     *  L_PAPERSIZE,
     *  L_ORIENTATION,
     *  72/25.4, 72/25.4,
     *  0.0,0.0-ORIENTATION*L_ORIGINOFFSET,
     *  25.4/72
1000	FORMAT(
     *  '%!PS-Adobe-'/
     *  '%EndComments'/
     *  'initgraphics'/
     *  A/
     *  I6,' rotate '/
     *  F15.7,F15.7,' scale'/
     *  F15.7,F15.7,' translate'/
     *  F15.7,' setlinewidth')
C*** jms     *  F15.7,' setlinewidth'   !reset line width to 1 point')
C*** jms     *  A/                      !select paper size (default = A4)
C***      *  I6,' rotate '/             !set orientation, origin, scale to mm
	RETURN

Ceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
	ENTRY PSCRIPT_COLOURTABLE(RED,GREEN,BLUE)
	DO I=1,256
	  L_RED(I)=RED(I)
	  L_GREEN(I)=GREEN(I)
	  L_BLUE(I)=BLUE(I)
	END DO
	RETURN

Ceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
	ENTRY PSCRIPT_COLOURIMAGE(ROW)
	L_COLOUR=.TRUE.
	GOTO 10

Ceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
	ENTRY PSCRIPT_GREYIMAGE(ROW)
	L_COLOUR=.FALSE.

10	IF (L_HEADERDONE) GOTO 20
	L_HEADERDONE=.TRUE.

	IF ((.NOT.L_RLE).AND.(.NOT.L_COLOUR)) WRITE(L_OSTREAM,1010)
     *  L_XORIGIN,L_YORIGIN,
     *  L_XSCALE,L_YSCALE,
     *  L_NCOLS,
     *  L_NCOLS,L_NROWS,
     *  L_NCOLS,-L_NROWS,L_NROWS
1010	FORMAT(
     *  '%greyscale, not run-length-encoded'/
     *  'gsave'/
     *  'initgraphics'/
     *  '10 dict begin'/
     *  '2.836464 2.836464 scale'/
     *  F15.7,F15.7,' translate'/
     *  F15.7,F15.7,' scale '/
     *  '/pixels',I6,' string def'/
     *  I6,I6,' 8'/
     *  '[',I6,' 0 0 ',I6,' 0',I6,']'/
     *  '{currentfile pixels readhexstring pop} bind'/
     * 'image' )

	IF ((.NOT.L_RLE).AND.L_COLOUR) WRITE(L_OSTREAM,1020)
     *  L_XORIGIN,L_YORIGIN,
     *  L_XSCALE,L_YSCALE,
     *  L_RED,
     *  L_GREEN,
     *  L_BLUE,
     *  L_NCOLS,
     *  3*L_NCOLS
1020	FORMAT(
     *  '%colour, not run-length-encoded'/
     *  'gsave'/
     *  '10 dict begin'/
     *  F15.7,F15.7,' translate'/
     *  F15.7,F15.7,' scale '/
     *  '/red ['/                        !set colour tables
     *  16(16I4.3/),'] def'/
     *  '/green ['/16(16I4.3/),'] def'/
     *  '/blue ['/16(16I4.3/),'] def'/
     *  '/inpixels',I6,' string def'/    !declare an input pixel-string
     *  '/outpixels',I6,' string def'/   !declare an output pixel-string
     *  '/temp 0 store'/
     *  '/pixel 0 store'/
     *  '/setpixel'                      !declare a procedure to:
     *  )
	IF ((.NOT.L_RLE).AND.L_COLOUR) WRITE(L_OSTREAM,1021)
     *  L_NCOLS,L_NROWS,
     *  L_NCOLS,-L_NROWS,L_NROWS,
     *  L_NCOLS-1
1021	FORMAT(
     *  '{'/                                    ! load the i/i+1/i+2 elements
     *  '  /temp exch store'/
     *  '  /pixel inpixels temp get store'/     !get the i''th element of the input string
     *  '  /temp temp 3 mul store'/             !temp=temp*3
     *  '  outpixels temp red pixel get put'/   !fill outpixels with r/g/b values
     *  '  outpixels temp 1 add blue pixel get put'/
     *  '  outpixels temp 2 add green pixel get put'/
     *  '} bind def'/
     *  I6,I6,' 8'/
     *  '[',I6,' 0 0 ',I6,' 0',I6,']'/
     *  '{'/
     *  'currentfile inpixels readhexstring pop'/      !get value
     *  '0 1',I6,' {setpixel} for'/              !and fill "pixels" with r/g/b
     *  'outpixels'/
     *  '} bind'/
     *  'false 3 colorimage'
     *  )

	IF (L_RLE.AND.(.NOT.L_COLOUR)) WRITE(L_OSTREAM,1030)
     *  L_XORIGIN,L_YORIGIN,
     *  L_XSCALE,L_YSCALE,
     *  L_NCOLS,L_NROWS,
     *  L_NCOLS,-L_NROWS,L_NROWS
1030	FORMAT(
     *  '%greyscale, run-length-encoded'/
     *  'gsave'/
     *  '10 dict begin'/
     *  F15.7,F15.7,' translate'/
     *  F15.7,F15.7,' scale '/
     *  '/runlen 2 string def'/                 !declare a hex input string
     *  '/pixel 0 def'/                         !declare a pixel-value store
     *  '/npixels -1 def'/                      !declare a pixel-count store
     *  '/pixels 256 string def'/               !declare a pixel string'/
     *  I6,I6,' 8'/
     *  '[',I6,' 0 0 ',I6,' 0',I6,']'/
     *  '{'/
     *  'currentfile runlen readhexstring pop pop'/     !get run-length and value
     *  '/npixels runlen 0 get store'/                  !store pixelcount in npixels
     *  '/pixel runlen 1 get store'/                    !store the pixelvalue in pixel
     *  '0 1 npixels {pixels exch pixel put} for'/      !and fill "pixels" with r/g/b
     *  'pixels 0 npixels 1 add getinterval'/           !build substring for "image"
     *  '} bind'/
     *  'image'
     *  )

	IF (L_RLE.AND.L_COLOUR) WRITE(L_OSTREAM,1040)
     *  L_XORIGIN,L_YORIGIN,
     *  L_XSCALE,L_YSCALE,
     *  L_RED,
     *  L_GREEN,
     *  L_BLUE
1040	FORMAT(
     *  '%colour, run-length-encoded'/
     *  'gsave'/
     *  '10 dict begin'/
     *  F15.7,F15.7,' translate'/
     *  F15.7,F15.7,' scale '/
     *  '/red ['/                       !set colour tables
     *  16(16I4.3/),'] def'/
     *  '/green ['/16(16I4.3/),'] def'/
     *  '/blue ['/16(16I4.3/), '] def'/
     *  '/runlen 2 string def'/         !declare a hex input string
     *  '/pixel 0 def'/                 !declare a pixel-value store
     *  '/npixels -1 def'/              !declare a pixel-count store
     *  '/pixels 768 string def'/       !declare a pixel string (3 x 256)
     *  '/temp 0 def'/                  !declare a temp store
     *  '/setpixel'                     !declare a procedure to:
     *  )
	IF (L_RLE.AND.L_COLOUR) WRITE(L_OSTREAM,1041)
     *  L_NCOLS,L_NROWS,
     *  L_NCOLS,-L_NROWS,L_NROWS
1041	FORMAT(
     *  '{'/                                      !load the i/i+1/i+2 elements
     *  '  /temp exch 3 mul store'/               !of "pixels" with appropriate
     *  '  pixels temp red pixel get put'/        !r/g/b values
     *  '  pixels temp 1 add blue pixel get put'/
     *  '  pixels temp 2 add green pixel get put'/
     *  '} bind def'/
     *  I6,I6,' 8'/
     *  '[',I6,' 0 0 ',I6,' 0',I6,']'/
     *  '{'/
     *  'currentfile runlen readhexstring pop pop'/ !get run-length and value
     *  '/npixels runlen 0 get store'/          !store pixelcount in npixels
     *  '/pixel runlen 1 get store'/            !store the pixelvalue in pixel
     *  '0 1 npixels {setpixel} for'/           !and fill "pixels" with r/g/b
     *  'pixels 0 npixels 1 add 3 mul getinterval'/   !build substring for "colorimage"
     *  '} bind'/
     *  'false 3 colorimage'
     *  )

20	CONTINUE
ccc	type *,'l_rle,l_ncols,l_nrows=',l_rle,l_ncols,l_nrows
C  write the optionally run-length-encoded line
	IF (L_RLE) THEN
	  PIXCOUNT=0
	  PREVPIX=-1
	  L_NVALS=0
	  DO I=1,L_NCOLS
	    NEWPIX=ROW(I)
	    IF (NEWPIX.LT.0) NEWPIX=NEWPIX+256
	    IF ((NEWPIX.NE.PREVPIX.AND.PIXCOUNT.NE.0).OR.
     *         PIXCOUNT.EQ.256) THEN
	      L_NVALS=L_NVALS+1
	      L_RLE_LIST(1,L_NVALS)=PIXCOUNT-1
	      L_RLE_LIST(2,L_NVALS)=PREVPIX
	      PIXCOUNT=1
	      PREVPIX=NEWPIX
	      N=1
	      IF (L_NVALS.EQ.20) THEN
	        DO K=1,20
	          DO J=1,2
	            CALL HEX_FORMAT(L_RLE_LIST(J,K),OLINE(N:N))
	            N=N+2
	          END DO
	        END DO
	        WRITE(L_OSTREAM,FMT='(A)') OLINE(1:N-1)
	        L_NVALS=0
	      END IF
	    ELSE
	      PIXCOUNT=PIXCOUNT+1
	      PREVPIX=NEWPIX
	    END IF
	  END DO
	  IF (PIXCOUNT.NE.0) THEN
	    L_NVALS=L_NVALS+1
	    L_RLE_LIST(1,L_NVALS)=PIXCOUNT-1
	    L_RLE_LIST(2,L_NVALS)=NEWPIX
	  END IF
	  IF (L_NVALS.NE.0) THEN
	    N=1
	    DO K=1,L_NVALS
	      DO J=1,2
	        CALL HEX_FORMAT(L_RLE_LIST(J,K),OLINE(N:N))
	        N=N+2
	      END DO
	    END DO
	    WRITE(L_OSTREAM,FMT='(A)') OLINE(1:N-1)
	    WRITE(L_OSTREAM,FMT='(1X)')
	  END IF
	ELSE
C  write line in FCHUNK-byte chunks
C  if this number is set to 25, lasertone takes 4 times longer !!
C  if we make it more than 256, edt will complain that the line is too long
C  should we wish to look at it with the editor.
C  FORTRAN I/O is a funny old game...
CCC	  FCHUNK=250
	  FCHUNK=25              !Wasi's HP printer seems to die with 250...
	  NCHUNKS=L_NCOLS/FCHUNK
	  NREM=L_NCOLS-FCHUNK*NCHUNKS
	  DO J=1,NCHUNKS
	    N=1
	    DO I=1,FCHUNK
	      K=ROW(FCHUNK*(J-1)+I)               !convert to I*4
	      CALL HEX_FORMAT(K,OLINE(N:N))
	      N=N+2
	    END DO
	    WRITE(L_OSTREAM,FMT='(A)') OLINE(1:N-1)
	  END DO
C  write remainder
	  N=1
	  DO I=1,NREM
	    J=ROW(FCHUNK*NCHUNKS+I)               !convert to INTEGER*4
	    CALL HEX_FORMAT(J,OLINE(N:N))
	    N=N+2
	  END DO
	  WRITE(L_OSTREAM,FMT='(A)') OLINE(1:N-1)
	ENDIF
	RETURN

Ceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
	ENTRY PSCRIPT_COPY(STRING)
	WRITE(L_OSTREAM,FMT='(A)') STRING
	RETURN

Ceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
	ENTRY PSCRIPT_ENDIMAGE
	WRITE(L_OSTREAM,1080)
1080	FORMAT(
     *  'end'/
     *  'grestore'
     *  )
	RETURN

Ceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
	ENTRY PSCRIPT_END
	WRITE(L_OSTREAM,1090)
1090	FORMAT(
     *  'showpage'/
     *  '%Trailer'
     *  )
	RETURN
	END

Cssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss
	SUBROUTINE HEX_FORMAT(IVAL,CHARDEST)
C  format a byte into 2 hex chars
	CHARACTER	CHARDEST*(*)
	INTEGER*4	IVAL
CTSH	INTEGER*4	MASK1/'F0'X/
CTSH	INTEGER*4	MASK2/'F'X/
	CHARACTER	HEXLIST*16/'0123456789ABCDEF'/
	DATA		ISIGNBIT/-2147483648/

	I=IVAL
	IF (I.LT.0) I=I-ISIGNBIT
C  extract l.s. byte
	I=MOD(I,256)
CTSH	J=(I.AND.MASK1)/16+1
CTSH++
	J=MOD(I/16,16)+1
CTSH--
	CHARDEST(1:1)=HEXLIST(J:J)
CTSH	K=(I.AND.MASK2)+1
CTSH++
	K=MOD(I,16)+1
CTSH++
	CHARDEST(2:2)=HEXLIST(K:K)
ccc	type *,ival,i,j,k
	RETURN
	END
