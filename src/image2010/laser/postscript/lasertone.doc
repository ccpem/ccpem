lasertone(lmb)                  lmb users guide                 lasertone(lmb)



NNAAMMEE
lasertone - make a postscript tone-plot of given map file.


SSYYNNOOPPSSIISS
       lasertone            [-opt[=param]           -opt...]           mapfile



OOPPTTIIOONNSS
     options  can  be  in  mixed  case  and  can  be unambiguously abbreviated


     -firstsection=i
          specifies   the   section-number   at   which   to   start   toning.
          default:          first           section           in           map


     -height=f
          specifies    the    height    of    the     tone-plot     in     mm.
          default:                     -height=see                    footnote


     -interpolate=i
          specifies  the number of intermediate interpolations to make per map
          cell.     Useful    in    large    images    of     coarse     maps.
          default:                                             -interpolate=0.


     -landscape
          specifies      landscape      orientation     for     the     paper.
          ***NOT                          WORKING                          YET
          default:                                                   portrait.


     -lower=f
          specifies  a lower threshold. Map intensities below this are plotted
          as                                                            white.
          default:        DMIN        from        the        map       header.


     -nsections=i
          specifies      the      number      of     sections     to     tone.
          default:                        all                         sections


     -noheader
          supresses  printing  of  the  header  at  the  top  of  each   plot.
          default:               header               is              printed.


     -outputfile=f
          specifies  the name of the postscript output file. The file is ready
          for       printing       on       a       postscript        printer.
          default:                                         -outputfile=tone.ps


     -reverse
          requests reverse-video. Normally, increasing intensity is plotted as
          blacker.            Reverse-video           reverses           this.
          default:                         no                         reverse.

     -rle
          requests  the  use  of run-length-encoding for the postscript image.
          For sparse images, this can reduce the  output  file-size  consider-
          ably,    however    not    all    laserprinters   can   handle   it.
          default:                           no                           rle.


     -upper=f
          specifies an upper threshold. Map intensities above this are plotted
          as                                                            black.
          default:       DMAX        from        the        map        header.


     -width=f
          specifies    the    width    of     the     tone-plot     in     mm.
          default:                     -width=see                     footnote


     -xorigin=f
          specifies  the  distance  of the left edge of the tone-plot from the
          left       edge       of       the        paper        in        mm.
          default:                                               -xorigin=10mm


     -yorigin=f
          specifies  the distance of the bottom edge of the tone-plot from the
          bottom       edge       of       the       paper       in        mm.
          default:  sufficient  to  place  the tone-plot just under the header
          text.


     ***      NOTE:      Skew      plots      are      not     working     yet
     *** If neither width nor height is specified, the  longest  map  side  is
     chosen.  If this is the map width then width is set to 200 mm. and height
     scaled accordingly. If the longest side is the map height then height  is
     set  to  200 mm and the width scaled accordingly.  If only width is given
     on the command line, height is scaled according to the map dimensions. If
     only  height  is given, width is scaled. If both are given, they are used
     verbatim.



                                                                lasertone(lmb)
