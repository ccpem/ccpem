
              ROTATIONAL AVERAGING and FILTERING PROGRAMS
              -------------------------------------------


1. RFILTIM:
----------
       Progam for filtering images with rotational symmetry. Using the
Fourier transform of the image, the program finds the best position for
the postulated rotational symmetry axis and then decomposes the transform
into cylindrical harmonics about this origin. The program produces a
rotational power spectrum, which is printed and displayed with CURVY.
Those harmonics consistent with the postulated symmetry are then
combined to produce a rotationally filtered image.
(Reference - Crowther and Amos  J. Mol. Biol.(1971) 60,123-130.)

   Input   Standard transform on stream 1 (IN)
   Output  Filtered image on stream 2     (OUT)
   Output  CURVY file on FOR003

   CONTROL DATA

     (1)  HEADER                                 (10A)

     (2)  N,NR,RSTEP,NLR,RSCAL                   (*)

     (3)  DXMIN,DXMAX,DELDX,DYMIN,DYMAX,DELDY,NBSTEP,NBMIN  (*)

     (4)  ASCAL,BSCAL,PHI0,SCALE,JOPT,LGDUMP,NORM     (*)


    N     Rotational symmetry to be tested
    NR    No. of annuli of data to be used from the transform
    RSTEP Spacing in transform steps of annuli
    NLR   No. of radial steps in filtered image

               Size of step in filtered image
    RSCAL ---------------------------------------------
          Size of step in original densitometered image

    DXMIN,DXMAX   Range of origin search in x direction measured in
                  densitometer steps
    DELDX         Interval of search in x direction in densitometer steps
    DYMIN,DYMAX   Similarly for y direction
    DELDY
    NBSTEP        No. of transform annuli to be binned into a band when
                  printing residuals from origin search 
    NBMIN         No. of bands (of size NBSTEP) to be omitted when
                  determining best origin.  (First band i.e. low resolution
                  data, is generally much the strongest and may be biased
                  by gross stain distribution, in which case it should be
                  omitted.)

    ASCAL,BSCAL   Radial weighting factors in transform which may be used
                  to increase artificially the strengths of the high
                  frequencies.  USE WITH CAUTION! Annulus number IR has a
                  scale factor (ASCAL+IR*BSCAL) applied. For normal use
                  set ASCAL=1.0, BSCAL=0.
    PHI0          The filtered image is rotated PHI0 degrees relative to the
                  densitometered image. Positive PHI0 rotates the image 
                  clockwise.
    SCALE         Densities in the filtered image are multiplied by SCALE.
                  SCALE=-1. inverts image contrast.
    JOPT          0  g0 included in filtered image
                  1  g0 omitted from filtered image
    LGDUMP        0  Do not dump gn's to disc file
                  1  Dump gn's to disc file (FORTRAN 10)
    NORM          0  Do not normalise power spectrum
                  1  Normalise spectrum with power of N=0 term equal to 1

   Current limitations:
     NR .LE. 40
     NLR .LE. 50
     Maximum image size transformed 256x256
     Maximum Bessel order 70
     Maximum of 15 different Bessel orders included in filtered image
     Maximum size of origin search 10x10 positions


2. ROTAV:
--------
      Rotational averaging of 2D image or 3D maps about z-axis.



3. IMROTRAN :
-----------

 IMROTRAN is a program to rotate/ translate an image file (the relative file) 
 and calculate a correlation coefficient wrt another image file (the reference 
 file), then to output another image file of the average the reference file 
 with the best fit of the relative file.

       INPUT DATA:

       (40A1) Input image file (reference file does not move)
       (40A1) Input image file (relative file is rotated/translated)
       (40A1) Output image file (average of above two)
       (*)    Theta1, Theta2, Thinc :
                       starting, finishing angles and increment in 
                       degrees. A +ve angle rotates relative file
                       clockwise.
       (*)    Xtrans1, Xtrans2, Xinc :
                       starting, finishing translation in X and increment
                       in pixels. A +ve value translates relative file
                       to the right.
       (*)    Ytrans1, Ytrans2, Yinc :
                       starting, finishing translation in Y and increment
                       in pixels. A +ve value translates relative file
                       upwards.
       (*)    Rmax :
                       maximum radius (in pixels) over which the correlation 
                       coefficient is to be calculated.

