************************************************************************
*									*
*		    Some useful documentation for:			*
*									*
*	   IMAGE Analysis and file format Programs & Routines		*
*									*
*									*
*	   Last Update:		22-AUG-2000	         		*
*									*
*									*
************************************************************************
*                                                                       *
*                Map/Image Header Format				*
*									*
*        Length = 1024 bytes, organized as 56 LONG words followed	*
*	by space for 10 80 byte text labels.				*
*									*
*       1       NX      number of columns (fastest changing in map)     *
*       2       NY      number of rows                                  *
*       3       NZ      number of sections (slowest changing in map)    *
*       4       MODE    data type :                                     *
*                       0       image : signed 8-bit bytes range -128   *
*                                       to 127                          *
*                       1       image : 16-bit halfwords                *
*                       2       image : 32-bit reals                    *
*                       3       transform : complex 16-bit integers     *
*                       4       transform : complex 32-bit reals        *
*       5       NXSTART number of first column in map (Default = 0)	*
*       6       NYSTART number of first row in map       "              *
*       7       NZSTART number of first section in map   "              *
*       8       MX      number of intervals along X                     *
*       9       MY      number of intervals along Y                     *
*       10      MZ      number of intervals along Z                     *
*       11-13   CELLA   cell dimensions in angstroms                    *
*       14-16   CELLB   cell angles in degrees                          *
*       17      MAPC    axis corresp to cols (1,2,3 for X,Y,Z)          *
*       18      MAPR    axis corresp to rows (1,2,3 for X,Y,Z)          *
*       19      MAPS    axis corresp to sections (1,2,3 for X,Y,Z)      *
*       20      DMIN    minimum density value                           *
*       21      DMAX    maximum density value                           *
*       22      DMEAN   mean density value                              *
*       23      ISPG    space group number 0 or 1 (default=0)           *
*       24      NSYMBT  number of bytes used for symmetry data (0 or 80)*
*       25-49   EXTRA   extra space used for anything  - 0 by default   *
*       50-52   ORIGIN  origin in X,Y,Z used for transforms             *
*       53      MAP     character string 'MAP ' to identify file type   *
*       54      MACHST  machine stamp                                   *
*       55      RMS     rms deviation of map from mean density          *
*       56      NLABL   number of labels being used                     *
*       57-256  LABEL(20,10) 10 80-character text labels                *
*                                                                       *
*	Symmetry records follow - if any - stored as text as in 	*
*	International Tables, operators separated by * and grouped into *
*	'lines' of 80 characters (ie. symmetry operators do not cross 	*
*	the ends of the 80-character 'lines' and the 'lines' do not 	*
*	terminate in a *).						*
*                                                                       *
*       Data records follow.						*
*                                                                       *
*                                                                       *
************************************************************************
Extra notes for clarity :

 NX,NY,NZ refer to the number of columns, rows, sections. 
 MXYZ(3) gives you sampling information. For EM processing they are 
 normally the same but for CCP4 they describe the way the unit cell is 
 divided in the map e.g. if the unit cell is 100A and it is in 5A 
 divisions, the value of MX (or MY or MZ) will be 20.
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


Linking libraries:

	imsubs2000 calls genlib which is built in the following way :
	m4 -D_dec_fortran=1 unix.m4 > unix.for
	fsplit unix.for
	fsplit diskio.for
	fsplit ccplib.for
	fsplit dummy.for
	fsplit parser.for
	fsplit symlib.for
	f77 -c  -O1 -fpe0 -assume dummy_aliases -nowarn *.f
	cc -c -O -DPROTOTYPE -w library.c
	ar cr genlib.a `lorder *.o | tsort`

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


		Higher-Level IMAGE File subroutines
		===================================

	These routine are designed to facilitate the rapid setup and
manipulation of image-format files. Either Sequential access or
Direct access I/O operations may be performed at any time.These routines
will now also cope with map-format files.
	In general, the X-Y origin is taken as 0,0 being in the
lower-left corner of the image AND the first data point in the file
(normally corresponding to array element 1,1).

			Top of Image		 1,NY	  Array    NX,NY
		^ !				   ^ !
		! !				   ! !
		! !				   ! !
		y !				   y !
		  !_____________   x-->              !_______________  x-->
		0,0				    1,1             NX,1




	The convention for FOURIER TRANSFORMS is that the transform of
an image of dimensions NX x NY yields a transform of NX/2+1 x NY
COMPLEX values. The origin of Fourier space is located at coordinates
0,NY/2 (disk coordinates, which would normally be 1,NY/2+1 in the FORTRAN
array). In reciprocal lattice units:
   X goes from 0 --> 0.5  and Y goes from -0.5 ---> (.5 - deltaY)





	Upto 5 image files may be open at any one time!!
             -

	All higher-level image routines begin with the letter I.
With the exception on the OPEN/CLOSE and Positioning routines a standard
naming convention has been established.
	The second 2 letters are chosen from the following.
	RD   =  Read
	WR   =  Write
	CL   =  Calculate
	CR   =  Create
	AL   =  Alter
	TR   =  Transfer
	RT   =  Return information

	The last 3 letters are chosen from the following:
	CDN  =  Density limits (Complex numbers, ie uses Modulus)
	CEL  =  unit cell paramters
	CON  =	conversion to reals
	DEN  =  Density limits
	EXT  =  Extra information ("unused" header slots)
	HDR  =  Header
	LAB  =  Labels
	MOD  =  Mode
	ORG  =	Origin
	RMS  =  RMS deviation
        SAM  =  Sampling information
 	SIZ  =  Size
        SYM  =  Symmetry information
	LIN  =  Line
	SEC  =  Section
	PAL  =  Part of Line
	PAS  =  Part of Section


	Specifications for higher-level image I/O routines:

File Set-Up Routines:
---------------------

CALL IMOPEN(ISTREAM,NAME,ATBUTE)		   Opens file NAME on ISTREAM

CALL IMCLOSE(ISTREAM)				   Closes file

CALL IMFORM(ISTREAM,IFORM)			   Returns flag to indicate
						   old or new style format

CALL IRDHDR(ISTREAM,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
				 		   Read  header on input file

CALL IWRHDR(ISTREAM,TITLE,NTFLAG,DMIN,DMAX,DMEAN)
						   Write header to output file

CALL ITRHDR(ISTREAM,JSTREAM)			   Transfer header from JSTREAM
						   to ISTREAM (does not write!)

CALL ITRLAB(ISTREAM,JSTREAM)			   Transfer labels from JSTREAM
						   to ISTREAM (does not write!)

CALL ITRCEL(ISTREAM,JSTREAM)			   Transfer CELL parmas  from
						   JSTREAM to ISTREAM (no write!)

CALL ICRHDR(ISTREAM,NXYZ,MXYZ,MODE,LABELS,NL)	   Create NEW header. The header
						   is NOT written out. You must
						   do a IWRHDR.
						   In general, it is advised that
						   one use ITRHDR followed by the
						   appropriate alter calls such as
						   CALL IALSIZ(....)   etc.
						   This is preferred because all
						   information is transferred by
						   the transfer call.

CALL IALCEL(ISTREAM,CELL)			   Alter unit cell paramters

CALL IALCON(ISTREAM,FLAG)			   Alter conversion to reals.
						   By default, all data is passed
						   to the user as REALS or COMPLEX
						   REALS, independent of storage mode
						   on disk. This call allows this
						   to be overridden.
						   This should be used with CAUTION!!!
						   FLAG = .TRUE.    conversion to REALS
						   FLAG = .FALSE.    NO conversion!!!

CALL IALEXT(ISTREAM,EXTRA,ISTART,NEXTRA)	   Alters  info in "unused"
						   slot in header, starting
						   at position ISTART. 
						   NEXTRA words are altered.

CALL IALLAB(ISTREAM,LABELS,NL)			   Alters NL labels in header

CALL IALMOD(ISTREAM,MODE)			   Alter MODE on existing header

CALL IALORG(ISTREAM,XORIGIN,YORIGIN,ZORIGIN)	   Alter XY origin information

CALL IALRMS(ISTREAM,RMSDEV)			   Alter RMS deviation

CALL IALSAM(ISTREAM,MXYZ)                          Alter sampling information

CALL IALSIZ(ISTREAM,NXYZ,NXYZST)		   Alter SIZE on existing header
						   Note, the NXYZST is for your
						   use ONLY. It is stored in the
						   file header for interfacing with
						   the X-ray programs, but all calls
						   within the image package, are
						   always taken relative to the
						   start of each line, or section.

CALL IALSYM(ISTREAM,KSPG,KBS)                      Alters symmetry parameters
                                                   KSPG is space group number
                                                   KBS is number of bytes of
                                                   symmetry data

CALL IALUVW(ISTREAM,IUVW)                          Alters matrix to permute
                                                   cell  dimensions.



File Positioning Routine:
-------------------------

CALL IMPOSN(ISTREAM,NZ,NY)			    Move to section NZ, line NY
						    Default convetions are all
						    numbers start at 0 !!!!


Line Read/Write Routines:
-------------------------

	In all cases, ARRAY is a REAL or COMPLEX REAL data array.
	All of these routines convert from/to Integer*2 as required.


CALL IRDLIN(ISTREAM,ARRAY,*LINE)		    Read line  (NX points)

CALL IWRLIN(ISTREAM,ARRAY)			    Write line (NX points)


CALL IRDPAL(ISTREAM,ARRAY,NX1,NX2,*LINE)	    Read  part line (NX1 - NX2)
						    Data is always loaded into
						    the FIRST element of ARRAY.
						    After READ, pointer is at
						    Start of next line.

CALL IWRPAL(ISTREAM,ARRAY,NX1,NX2)		    Write part line (NX1 - NX2)
						    from selected region in
						    core onto disk.



Section Read/Write Routines:
----------------------------

	In all cases, ARRAY is a REAL or COMPLEX REAL data array.
	All of these routines convert from/to Integer*2 as required.


CALL IRDSEC(ISTREAM,ARRAY,*LINE)		    Read  section 

CALL IWRSEC(ISTREAM,ARRAY)			    Write section 


CALL IRDPAS(ISTREAM,ARRAY,MX,MY,NX1,NX2,NY1,NY2,*LINE)
						    Read  part of section
						    After READ, pointer is at
						    Start of next section.

CALL IWRPAS(ISTREAM,ARRAY,MX,MY,NX1,NX2,NY1,NY2)    Write  part of section




Other Useful Routines:
----------------------


CALL ICLCDN(ARRAY,MX,MY,NX1,NX2,NY1,NY2,DMIN,DMAX,DMEAN)   (Mod of Complex vals)
CALL ICLDEN(ARRAY,MX,MY,NX1,NX2,NY1,NY2,DMIN,DMAX,DMEAN)   (Real numbers)
						    Calculates the MIN/
						    MAX, & MEAN densities
						    from the selected portion
						    of ARRAY (assumed to be
						    image!!). NOTE: Here and
						    only here does the counting
						    start from 1.
CALL ICLLIM(ISTREAM,IXYZMIN,IXYZMAX,NXYZ)
						    Prompts the user to supply
						    area limit (0,0 lower left)
						    and returns pixel limits
						    # pixels, and user limits

IUNIT = IMUNIT(ISTREAM)				    Returns IUNIT which is
						    compatible with the
						    LOW-LEVEL routines.

CALL IRTCEL(ISTREAM,CELL)			    returns CELL(6)

CALL IRTEXT(ISTREAM,EXTRA,ISTART,NEXTRA)	    Returns NEXTRA words extra
						    slot in header, starting
						    at position ISTART. There
						    are 29 available words.
						   

CALL IRTLAB(ISTREAM,LABELS,NL)			    Returns LABELS(20,10)
						    number of labels NL.

CALL IRTORG(ISTREAM,XORIGIN,YORIGIN,ZORIGIN)	    Returns XY origin information

CALL IRTRMS(ISTREAM,RMSDEV)			    Returns RMS deviation

CALL IRTSAM(ISTREAM,MXYZ)                           Returns sampling information

CALL IRTSIZ(ISTREAM,NXYZ,MXYZ,NXYZST)		    Returns SIZE info

CALL IRTSYM(ISTREAM,KSPG,KBS)                       Returns symmetry parameters
                                                    KSPG is space group number
                                                    KBS is number of bytes of
                                                    symmetry data

CALL IRTUVW(ISTREAM,IUVW)                           Returns matrix to permute
                                                    cell  dimensions.



Variable Definitions:		(ALL variables are INTEGER*4 or REAL!!)
---------------------


ARRAY:		A storage location for data transfer. NOTE ARRAY is either
		REAL*4 or COMPLEX*8 !!!!!
		
ATBUTE:		File attribute specification. One of the following character
		strings must be given:  'NEW'  'OLD'  'RO'  or  'SCRATCH'

DMIN/DMAX/	The minimum, maximum, & mean density values for this image.
DMEAN:

EXTRA(25)	An array (up to 25 long) or a storage location for holding
		the values for the EXTRA info slot in the header.

IFORM:		-1 Old 20th century style map, 0 new 2000 format with
		machine stamp in header.

ISTART:		A number between 1-29 to select the first element of the
		EXTRA info to transfer.

ISTREAM/	A number between 1 and 12 used to select which file
JSTREAM:	to use for READ/WRITE operations. A maximum of 5
		files can be active at any one time.

IXYZMIN(3)/	Returned lower & upper pixel limits that correspond to user
IXYZMAX(3):	selected input range. (pixels have 0,0 at bottom left)

MODE:		Defines data structure on disk:
		0 = INTEGER*1	 IMAGE note:values between 127 & 255
                                       stored as -128 to -1 but returned
                                       as their original values.
		1 = INTEGER*2    IMAGE
		2 = REAL*4       IMAGE
		3 = INTEGER*2    FOURIER TRANSFORM
		4 = REAL*4       FOURIER TRANSFORM

MX/MY:		Dimensions of ARRAY. If transferred data size is smaller
		than MX,MY the remainder of the space will be set to 0.
		Note: MX correspons to the number of REAL numbers in the
		fast dimension of ARRAY. If MODE = 3 or 4 (Fourier
		Transform) MX MUST be multiplied by 2!!!

MXYZ(3):	Specifies the number of columns, rows, & sections in the
		entire "unit cell"  (can be identical to NXYZ).

LABELS(20,N)	N = 1 - 10. This is a way to initialize more than
		one text label when creating a new header. LABELS(20)
		is also permitted. Each label is 80 characters long (A4).

NAME:		A character string specifying either the FILENAME or
		a LOGICAL name to associate with a given stream.

NEXTRA:		A number bewteen 1-25 to select the number of EXTRA
		value to transfer.

NL:		The number of LABELS being initialized (see LABELS, above).

NTFLAG:		-1 no titles added to list.
		 0 to use TITLE as sole title on file (overwrites old title).
		 1 to add TITLE to end of list. 
		 2 to add at top, pushing any other labels down.

NX1/NX2:	Beginning & ending COLUMN numbers for data transfer.
		On reading these numbers are relative to NXTZST(1).
		For Fourier Transforms, these are taken as Complex Indicies!!!

NXYZ(3):	Specifies the number of columns, rows, & sections in the
		current file (ie. fastest -> slowest changing).

NXYZST(3):	Specifies the starting column, row and section number.
		Note, the NXYZST is for your use ONLY. It is stored in the
		file header for interfacing with the X-ray programs, but all
		calls within the image package, are always taken relative to
		the start of each line, or section. The start is ALWAYS
		denoted by 0.

NY:		Line number for positioning pointer. This value
		is relative to NXYST(2)

NY1/NY2:	Beginning & ending ROW    numbers for data transfer.
		On reading these numbers are relative to NXTZST(2).

NZ:		Section number for positioning pointer. This value
		is relative to NXYST(3)

TITLE(20)	80 character title. Written as dictated by NTFLAG.

XORIGIN/	The X,Y,Z image origin (generally used for relating
YORIGIN		phase origins). ZORIGIN set to zero for 2D images.
ZORIGIN:

*LINE		LINE is a statement number to branch to on an
		END-OF-FILE error. Example:   *99






	LOW-LEVEL Random-Access Subroutines.
	====================================


	The LOW-LEVEL set of FORTRAN subroutines for doing
	either sequential or random access disk I/O with
	with variable record lengths are indicated below.
	The files are actually written as fixed-record
	direct-access files, but this is transparent to
	the user.


	the calls provided are:

  CALL QOPEN   (IUNIT,FILNAM,ATBUTE)        - Open file
  CALL QCLOSE  (IUNIT)                      - Close file
  CALL QMODE   (IUNIT,MODE,NMCITM)          - Change mode
  CALL QREAD   (IUNIT,ARRAY,NITEMS,IER)     - Read nitems
  CALL QREADI  (IUNIT,ARRAY,NITEMS,IER)     - Read nitems into integer array
  CALL QREADR  (IUNIT,ARRAY,NITEMS,IER)     - Read nitems into real array
  CALL QREADQ  (IUNIT,ARRAY,NITEMS,IER)     - Read nitems into complex array
  CALL QREADC  (IUNIT,CHAR,IER)             - Read bytes into character var.
  CALL QWRITE  (IUNIT,ARRAY,NITEMS)         - Write nitems
  CALL QWRITI  (IUNIT,ARRAY,NITEMS)         - Write nitems from integer array
  CALL QWRITR  (IUNIT,ARRAY,NITEMS)         - Write nitems from real array
  CALL QWRITQ  (IUNIT,ARRAY,NITEMS)         - Write nitems from complex array
  CALL QWRITC  (IUNIT,CHAR)                 - Write bytes from character var.
  CALL QSEEK   (IUNIT,IREC,IEL,LRECL)       - Move to irec,iel
  CALL QBACK   (IUNIT,LRECL)                - Backspace 1 record
  CALL QSKIP   (IUNIT,LRECL)                - Skip 1 record
  CALL QQINQ   (IUNIT,LFILNM,FILNAM,LENGTH) - Get filename and length
  CALL QLOCATE (IUNIT,LOCATE)               - Get position in file
  CALL QRARCH (IUNIT, IOFFSET)              - set up number conversion
  CALL QWARCH (IUNIT, IOFFSET)              - write conversion info

  QSEEK calculates the location as (IREC - 1)*LRECL + IEL. Note: as in
        Fortran, addressing begins at 1 for both record & element
        In these files, there are no true records: the use of "record length"
        and "record number" in QSEEK, QSKIP, QBACK is purely notional.
        For QSEEK, any combination of IREC, IEL & LRECL which gives the
        same value of (IREC - 1)*LRECL + IEL is equivalent.

  Where:

  IUNIT  = Variable returned by (Q)QOPEN to identify a file stream

  FILNAM = file name for the stream (should be restricted to eight
           characters for CCP4 programs)

  ATBUTE = File status for opening file
         = 'UNKNOWN', 'SCRATCH', 'OLD', 'NEW', or 'READONLY'

  ISTAT  = File status on opening the file:
           1, 'UNKNOWN'   open as 'OLD'/'NEW' check existence
           2, 'SCRATCH'   open as 'OLD' and delete on closing
           3, 'OLD'       file MUST exist or program halts
           4, 'NEW'       create (overwrite) new file
           5, 'READONLY'  self explanatory

  NOTE: When using QQOPEN or QOPEN with ATBUTE = 'NEW' [ISTAT = 4],
        a check is made on the environment variable CCP4_OPEN -
        if this is set to UNKNOWN then the file is opened with
        attribute UNKNOWN rather than NEW to allow overwriting files
        that already exist.
