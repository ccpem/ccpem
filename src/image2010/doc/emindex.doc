
           DOCUMENTATION OF EXISTING STAND-ALONE PROGRAMS
                  AND INDEX OF PROGRAMS FOR EMHELP
                  --------------------------------

             P = Procedure i.e. Type the name and program will prompt you.
             C = Command file which should be edited and entered as batch job.

DENSITOMETER PROGRAMS:
_____________________

 1.  NIKSCN	(RAC)	digitisation using NIKON
 2.  JLSCAN	(RH)	digitisation using JOYCE-LOEBL

GENERAL PROGRAMS FOR PROCESSING MICROGRAPHS:
-------------------------------------------

 0.  HEADER    P Prints out information in header records.
 1.  IMCON     P Get image from CDS dual-port disc, put on VAX disc.
 2.  LABEL     P Image handling, does a few nice things.                
 3.  FFTRANS   C Fast Fourier transform.                                
 4.  TRNOUT    C Output amplitudes & phases to line printer.
 5.  TRMASK    C Masks transform for filtering
 6.  INTERPO   P General 2D re-interpolation program
 7.  SPLINEFIT P Image straightening using AED
 8.  BOXIM     C Standard boxing program for selection of an area.
 9.  BOXIMAGE  C Similar to above, leaves boxed area in original position  (RH)
10.  IMEDIT    P General editing program for image headers
11.  MEDIAN      Median filtering for smoothing images.
12.  ENHANCE     Applies various radial filter functions to image transforms.
13.  IMEXCHANGE  Converts IMAGEFORMAT into ascii for editing/exchange.
14.  REMORIG     Removes origin with taper for crude high-pass filtering.
15.  TAPEREDGE   Tapers the edge of an image to remove spikes.

2-DIMENSIONAL PROCESSING PROGRAMS:
----------------------------------

 1.  LATREF     C Refine lattice parameters form set of spot posns         (RAC)
 2.  NNBOX      C Print out small part of transform as numbers             (JMS)
 3.  EMTILT     P Calculate tilt angles from lattice parameters             (RH)
 4.  TWOFILE    C Linear comb, or multiply/divide data in two files     (RAC/RH)
 5.  MASKTRANA  C Masks transform for filtering, like TRMASK                (RH)
 6.  AUTOCORRL  C Autocorrelation calc + expansion - use with QUADSERCH    (JMB)
 7.  QUADSERCHA C Correlation peak search on lattice, with profile fit     (JMB)
 8.  CCUNBENDC  C Unbend image using list of peaks from QUADSERCH       (JMB/RH)
 9.  MMBOX      C Sophisticated version of NNBOX, producing Amps, phases    (RH)
10.  MMLATREF   C Lattice parameter refinement based on MMBOX               (RH)
11.  TTBOX      C Corrects for tilted transfer function, gives Amps, phases (RH)
12.  TTMASK     C Combined MASKTRAN and TTBOX, masking + TTF corr on tilted (RH)
13.  TTREFINE   C Refines defocus, astig, tilt params on tilted images      (RH)
14.  TTBOXREF   C Lattice parameter refinement on images with TTF correction(RH)
15.  CTFREFINE  C Refines defocus, astig, on data from untilted images      (RH)
16.  CTFAPPLY   C Applies CTF to data from MMBOX, with graphical output     (RH)
17.  ORIGTILTC  C Combine data from different images using crystal symmetry (RH)
18.  LLFILT     C Lattice line fitting for smooth curves and sfacs         (RAC)
19.  LATLINE    C David Agard's least squares latline fit of Amps, phases   (DA)
20.  ALLSPACE   C Determines space group, origin, beamtilt on single image  (RH)
21a. AVRGPHASES C Overall averaging of projection data from ORIGTILT   (JMB/TAC)
21b. AVRGAMPHS  C Overall averaging of amplitude and phase projection data  (RH)

ELECTRON DIFFRACTION PATTERNS:   (TAC/JMB/RH)
------------------------------
 1.  BACKAUTO   C Calculates radial background and finds centre of pattern
 2.  AUTOINDEX  C Finds two simplest lattice vectors automatically
 3.  PICKAUTO   C Integrates and background corrects el.diff. spots
 4.  MERGEDIFF  C Merges e.d. data and does a host of corrections
 5.  AVRGDELTAF C Averages multiple measurements of delta-F from MERGEDIFF
 6.  SYNCFIT    C Fits lattice line curves to output from MERGEDIFF
 7a. MKLCF      C Converts formatted data (A,P) to LCF format
 7b. F2MTZ      C Converts formatted data (A,P) to MTZ format
 8.  UPDTXTRA   C Puts lattice params into extra records of header

3-DIMENSIONAL HELICAL PROGRAMS:
-------------------------------
 
 1.  HLXSEARCH  C Determine tilt and origin.                        (RAC)
 2.  HLXDUMP    C Dumps layer line data from transform              (RAC)
 3.  HLXFIT     C Orientations & origins of different particles     (RAC)
 4.  HLXAVG     C Averages data from different particles            (RAC)
 5.  HLXFOUR    C Fourier program                                   (RAC)
 6.  HLXPROJ    C 2D filtered image from layer line data            (RAC)
 7.  EM2FOLD    C Imposes twofold normal to axis                    (RAC)
 8.  HLXSEPDAT  C Feeder for HLXSEPR                                (RAC)
 9.  HLXSEPR    C Separates overlapping Bessel functions            (RAC) 
10.  HLXLLOUT   C Graphs standard layer line data                   (RAC) 
11.  HLXSIMUL   C Simulates image of helix                          (RAC)

ICOSAHEDRAL VIRUS RECONSTRUCTION PROGRAMS (RAC)
-----------------------------------------

 1.  REFINE  -  finds orientations and origins by common lines
 2.  X37CORR -  cross-correlates two particles of known orientation
                to find relative hand and scaling
 3.  RACMAT  -  extracts data from transforms needed for solving for big G's
 4.  RACBG   -  solves for big G's
 5.  RACLG   -  converts big G's to little g's
 6.  RACFB   -  does Fourier summation from little g's to make 3-D map
 7.  MAPCONV -  converts 3-D fourier map to MRC format

ROTATIONAL AVERAGING and FILTERING:
----------------------------------

 1.  RFILTIM   C Rotational filtering
 2.  ROTAV       Rotational averaging of 2D images or 3D maps about z axis.
 3.  IMROTRAN    Rotates/translates 2D image - search to maximise correlation.

GENERAL PROGRAMS FOR CALCULATING AND CONTOURING MAPS:  (all from CCP4 suite)
----------------------------------------------------

 1.  MKLCF      Converts formatted data (A,P) to LCF format.
 2.  FFT        Crystallographic Fourier program.
 3.  EXTEND     Extend maps/images to multiple unit cells.
 4.  PLUTO      General contouring, atomic model plotting program.

GENERAL DISPLAY PROGRAMS:
-------------------------

 1.  TRILOG      General conversion of .PLT files to line printer code.  (TSH)
 2.  LASERPRINT  General program for output of plot, tone, text files.   (TSH)
 3.  TONE        Outputs image on the line printer half tone image.      (TSH)
 4.  HISTO       Makes  histogram of densities in an image.              (DAA)
 5.  CURVY       Produces graph on line printer.                         (DAA)
 6.  AEDDSP      General display program for AED.                        (JMS)
 7.  NEWTONE     On vax, does same as laserprint -tone on Alliant        (TSH)
 8a. IMDISP      LMB raster graphics display for vaxstatns, X-terms.     (JMS)
 8b. XIMDISP     New (1994) LMB raster graphics display for X-terms.     (JMS)
 9.  THREED      Displays 2D array as  simulated 3D contoured surface.
10.  SURF, LIGHT Displays 3D maps as shaded surfaces on the AED.         (GV)
11.  AEDCAMERA   Automatic exposure series for photographing from AED.

