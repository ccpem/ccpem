setenv FINDEMDIR /usr/local/programs/FindEM2
# alter the above  path for your local installation.

#these aliases below are new for FindEM2
alias goFindEM2  ${FINDEMDIR}/goFindEM2-script.com
alias FindEM2 ${FINDEMDIR}/FindEM2_V1.00.exe

#the aliases below are the same as for the old FindEM
alias downsizeandfilter ${FINDEMDIR}/downsizeandfilter
alias goFindEM_processlcfmap ${FINDEMDIR}/goFindEM_processlcfmap
alias FindEM_processlcfmapI ${FINDEMDIR}/FindEM_processlcfmapI
alias FindEM_processlcfmapII ${FINDEMDIR}/FindEM_processlcfmapII

alias FindEM_GUI ${FINDEMDIR}/FindEM_GUI
alias mrc2gif2  ${FINDEMDIR}/mrc2gif2

alias FindEM_filter_sortedraw ${FINDEMDIR}/FindEM_filter_sortedraw
alias FindEM_filter_coords2 ${FINDEMDIR}/FindEM_filter_coords2
alias goFindEM_filtercoords2 ${FINDEMDIR}/goFindEM_filtercoords2


