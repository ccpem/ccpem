
#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
#---For Option 2---

# (i)   This script will read in the denisty map (and its details), details for the grid
#       search and the structure of best fitted model from step 1 (model.py) ("Top_model.txt")

from modeller import *
from modeller.scripts import complete_pdb
from modeller.automodel import *

import sys

#--------------------------------------------------------------------------------------
#---read file-names and details via keyboard-------------------------------

if len(sys.argv) != 11:
    print "usage: filename.py [map-format] [res] [ori x] [ori y] [ori z] [render_func] [box_size] [voxel] [start_type] [no steps]"
    sys.exit()


map_file    = 'map'                     # EM map name
map_format  = sys.argv[1]               # EM map format ('MRC' or 'XPLOR')
res         = float(sys.argv[2])        # resolution
x           = float(sys.argv[3])        # map origin (x-coordinate)
y           = float(sys.argv[4])        # map origin (y-coordinate)
z           = float(sys.argv[5])        # map origin (z-coordinate)
render_func = sys.argv[6]               # density type (options 'GAUSS' or 'SPHERE')
box_size    = int(sys.argv[7])          # grid size (has to be a box)
apix        = float(sys.argv[8])        # voxel size
start	    = sys.argv[9]               # start type
noSteps     = int(sys.argv[10])         # number of steps to perform in density grid search

#--------------------------------------------------------------------------------------
# Provided inputs

##start='CENTER'          #'CENTER' 'ENTIRE' 'SPECIFIC' ** we should use either center or specific
trans='EXHAUSTIVE'      #'EXAUSTIVE' :scanning MC (Monte Carlo)
                        #'NONE' rotation only #'RANDOM' MC ** we should use EXHAUSTIVE
temp=0.                 # for MC: 5000 for gauss
                        # can play with this parameter for MC acceptance <500 ** use T=0  
steps=noSteps           # optimization steps: 4-5 for 'EXAUSTIVE', 100-250 for 'RANDOM'
filter='NONE'           #'LAPLACIAN' (only works with GAUSS) | 'NONE'the default ** we should use NONE
fits_num=1              # number of wanted solutions (ranked) ** keep it 1

#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------


#---get the best ranked model from step 1
f = open("Top_model.txt","r")           # protein structure to be fit read from file top_model
struc = f.readline()                
f.close()


#--------------------------------------------------------------------------------------
#---density fit the best model from step 1 (model.py)

log.verbose()
env = environ()

den = density(env,
              file=map_file,
              em_density_format=map_format,
              voxel_size=apix,
              resolution=res,
              em_map_size=box_size,
              cc_func_type='CCF',
              density_type=render_func,
              px=x,
              py=y,
              pz=z)

den.grid_search(em_density_format=map_format,
                num_structures=1,
                em_pdb_name=[struc],
                chains_num=[1],
                start_type=start,
                number_of_steps=steps,
                angular_step_size=30.,
                temperature=temp,
                best_docked_models=fits_num,
                translate_type=trans,
                em_fit_output_file='em_fit.log')



### write the code of the fitted model into a textfile
##g = open("FittedCode.txt","w")
##g.write("target")
##g.close
