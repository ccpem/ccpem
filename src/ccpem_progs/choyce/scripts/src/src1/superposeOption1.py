#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
#---For Option 2---

##    (i)     This script will superpose all remained models, produced in step 1
##            on the fitted model from step 2 (best model from step 1 is fitted into density map)
##    (ii)    Calculate the cross correlation, the normalized CC scores (Z score) and the FULL (final) score
##
##    (iii)   save results in Output file
        

from modeller import *
from modeller.scripts import complete_pdb
from modeller.automodel import *

import sys
import math
import string


#--------------------------------------------------------------------------------------
#--------------read in file-names and details via keyboard-----------------------------


if len(sys.argv) != 11:
    print "usage: filename.py [map-format] [resolution] [origin x-coor] [origin y-coor] [origin z-coor] [render_func] [box_size] [voxel size (apix)] [start_type] [no of steps]"
    sys.exit()

map_file    = 'map'                     # EM map name
map_format  = sys.argv[1]               # EM map format ('MRC' or 'XPLOR')
res         = float(sys.argv[2])        # resolution
x           = float(sys.argv[3])        # map origin (x-coordinate)
y           = float(sys.argv[4])        # map origin (y-coordinate)
z           = float(sys.argv[5])        # map origin (z-coordinate)
render_func = 'GAUSS'                   # density type must be 'GAUSS' here
box_size    = int(sys.argv[7])          # grid size (has to be a box)
apix        = float(sys.argv[8])        # voxel size



fitted_Mod = 'targ_1_1.pdb'             # structure file of density fitted target from step 2 (modem.py) (reference model in this script)

#--------------------------------------------------------------------------------------
#---read in density map only once

log.verbose()
env = environ()

den = density(env,
              file=map_file,
              em_density_format=map_format,
              voxel_size=apix,
              resolution=res,
              em_map_size=box_size,
              cc_func_type='CCF',
              density_type=render_func,
              px=x,
              py=y,
              pz=z)


#--------------------------------------------------------------------------------------
#---store the names of the homology models from step 1 (model.py) in a dictionary

g = open("Models_summary.txt","r")
analysis = {}
g.readline()    # to delete 1.line "Filename ...
for line in g.readlines():
    modelname = line.split()[0]
    analysis[modelname] = []
    dope = line.split()[1]
    analysis[modelname].append(float(dope))         #---the dictionary looks like {target.B99990003.pdb:-0.26785100000000001 , target.B99990001.pdb:-0.480105 , ...} etc etc
g.close()



#--------------------------------------------------------------------------------------
#---superpose all models from step 1 on the fitted model (step 2) .....using for loop that operates like:
#---(i)     read in reference structure
#---(ii)    read in structure to superpose (homology models from step 1
#---(iii)   align and superpose both structures
#---(iv)    process the outputs

superposedNames = []                                                        # help variable to store output structure file names into end file

keys = analysis.keys()                                                      # get model names
for key in keys:                                                            # loop while homology models to superpose

    aln = alignment(env)

    #---reference model is read in and stored in alignment environment
    mdl = model(env)
    code = fitted_Mod.rstrip('.pdb')
    mdl.read(file=fitted_Mod)
    aln.append_model(mdl=mdl, align_codes=code, atom_files=code)
    
    #---structure(s) to superpose is read in
    mdl2 = model(env)                                                       
    struc_to_sup = key     #models[1]    #-----
    code = struc_to_sup.rstrip('.pdb')
    mdl2.read(file=struc_to_sup)
    aln.append_model(mdl=mdl2, align_codes=code, atom_files=code)
    protLen = 0
    for seq in aln:
        protLen =  len(seq)
    analysis[key].append(protLen)                   #---the dictionary looks like {target.B99990003.pdb:[-0.26785100000000001, 104] , ...} etc etc

       
    #---align and create alignment file (ALIGN2D here)
    aln.salign(rr_file='$(LIB)/as1.sim.mat',  # Substitution matrix used
               max_gap_length=20,
               gap_function=False, #True,              # If False then align2d not done
               feature_weights=(1., 0., 0., 0., 0., 0.),
               gap_penalties_1d=(-600, -400), #(-100, 0),
               gap_penalties_2d=(3.5, 3.5, 3.5, 0.2, 4.0, 6.5, 2.0, 0.0, 0.0),
#               gap_penalties_3d=(0, 2.0),
               similarity_flag=True)   # Ensuring that the dynamic programming
                                   # matrix is not scaled to a difference matrix
    aln.write(fitted_Mod+'_'+struc_to_sup+'.ali')
    
    
    #---now superpose the two structures
    mdl = model(env, file=fitted_Mod)
    atmsel = selection(mdl).only_atom_types('CA')
    mdl2 = model(env, file=struc_to_sup)
    r = atmsel.superpose(mdl2, aln,fit=True)

    
    #---write a superposed coordinate file
    struc_supo = struc_to_sup.rstrip('.pdb')+"Superposed.pdb"
    mdl2.write(file=struc_supo)
    analysis[key].append(struc_supo)                #---the dictionary looks like {target.B99990003.pdb:[-0.26785100000000001, 104, target.B99990003Superposed.pdb] , ...} etc etc


    #---calculate CC (cross-correlation) scores
    env.edat.density = den
    env.edat.dynamic_sphere = True
    mdl3 = model(env, file=struc_supo)
    atmsel = selection(mdl3)
    scal = physical.values(default=0.0, em_density=1.0)
    (molpdf, terms) = atmsel.energy(schedule_scale=scal)            #---CC is score is -molpdf


    #---store calculated values in the dictionary
    analysis[key].append(-molpdf)                                   #---the dictionary looks like {target.B99990003.pdb:[-0.26785100000000001, 104, target.B99990003Superposed.pdb, CCFobs] , ...} etc etc
    


#--------------------------------------------------------------------------------------
#---calculate CCFexp, CCZ and final score

#---calculate CCexp
#---ccf_exp = -0.011 * ln(len) + 0.577
keys = analysis.keys()
for key in keys:
    ccf_exp = -0.011 * math.log(analysis[key][1]) + 0.577
    analysis[key].append(ccf_exp)                                       #---the dictionary looks like {target.B99990003.pdb:[-0.26785100000000001, 104, target.B99990003Superposed.pdb, CCFobs, CCFexp] , ...} etc etc
#---calculate CCZ
#---CCZ = (CCFobs - CCFexp) / stdev
#---(where CCFexp is always 0.053, unpublished paper)
    CCZ = (analysis[key][3] - analysis[key][4] ) / 0.053
    analysis[key].append(CCZ)                                           #---the dictionary looks like {target.B99990003.pdb:[-0.26785100000000001, 104, target.B99990003Superposed.pdb, CCFobs, CCFexp, CCZ] , ...} etc etc
#---final score
#---final = dope - CCZ  (evt. -1(final)
#    final = -1 * (analysis[key][0] - analysis[key][5])                  #---using -1*(final score)
#    analysis[key].append(final)
#---reverse inputs, so that we can easily sort the output by the final score
    analysis[key].reverse()                                            #---the dictionary looks like {target.B99990003.pdb:[CCZ, CCFexp, CCF,obs, target.B99990003Superposed.pdb, 104, -0.26785100000000001] , ...} etc etc

##    print " %.3f %.3f %.3f %.3f %s %.3f %.3f" %(analysis[key][0], analysis[key][1], analysis[key][2], analysis[key][3], analysis[key][4], analysis[key][5], analysis[key][6])  
#    print analysis[key]
#    sys.exit()

#--------------------------------------------------------------------------------------
#---store all results in outputfile "Superposed_models_All.txt"
f = open("Superposed_models_All.txt","w")
f.write("Filename CCZ Normalized_DOPE 3D-Models CCFobs CCFexp ProteinLength\n")
sortedAnalysis = sorted(analysis.iteritems(), key=lambda (k,v):(v,k), reverse=True)
for data in sortedAnalysis:
##    print data[0] + " %.3f %.3f %.3f %s %.3f %.3f %.3f" %(data[1][0], data[1][6], data[1][1], data[1][4], data[1][3], data[1][2], data[1][5])
    f.write(data[0] + " %f %f %s %f %f %f\n" %(data[1][0], data[1][5], data[1][3], data[1][2], data[1][1], data[1][4]))
f.close()


#--------------------------------------------------------------------------------------
#---store necessary results in outputfile "Superposed_models_CC.txt"
f = open("Superposed_models_CC.txt","w")
f.write("Filename CCZ Normalized_DOPE 3D-Models\n")
sortedAnalysis = sorted(analysis.iteritems(), key=lambda (k,v):(v,k), reverse=True)
for data in sortedAnalysis:
##    print data[0] + " %.3f %.3f %.3f %s %.3f %.3f %.3f" %(data[1][0], data[1][6], data[1][1], data[1][4], data[1][3], data[1][2], data[1][5])
    f.write(data[0] + " %f %f %s\n" %(data[1][0], data[1][5], data[1][3]))
f.close()
