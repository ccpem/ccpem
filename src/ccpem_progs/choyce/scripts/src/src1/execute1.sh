#!/usr/bin/env tcsh

#---------------------------------------------------------------------------
#------------Input description----------------------------------------------

# $1: 	target sequence
# $2: 	template structure
# $3: 	number of models
# $4: 	density map
# $5: 	density map format ('MRC' or 'XPLOR')
# $6: 	map resolution
# $7: 	map origin x-coordinate
# $8: 	map origin y-coordinate
# $9: 	map origin z-coordinate
# $10: 	density type ('GAUSS' or 'SPHERE')
# $11: 	grid size
# $12: 	voxel size
# $13: 	option

# test if we have 12 arguments, exit with warning if we don't
if ($# != 12) then
 echo "Usage: $0 [target][template][number_models][map][map_format][map_resolution][origin_x][origin_y][origin_z][density type][grid_size][voxel]"
 exit 1
endif


#---------------------------------------------------------------------------
#------------Create work directories----------------------------------------
mkdir work

#---------------------------------------------------------------------------
#------------Copy necessary files-------------------------------------------
cp alignment.* work/.
cp template.pdb work/.
cp map work/.

#---------------------------------------------------------------------------
#------------start align-calculating----------------------------------------
cd work
rm -rf *.log

# step 1 
/d/mt1/u/ubcg67a/bin/modeller9v6/bin/modpy.sh python /d/mt1/u/ubcg67a/reda/src/src1/modelOption1.py $3 > modelOption1.log 

# step 2
/d/mt1/u/ubcg67a/bin/modeller9v6/bin/modpy.sh python /d/mt1/u/ubcg67a/reda/src/src1/modemOption1.py $4 $5 $6 $7 $8 $9 ${10} ${11} ${12}  > modemOption1.log 

# step 3
if ($3 != "1") then
	/d/mt1/u/ubcg67a/bin/modeller9v6/bin/modpy.sh python /d/mt1/u/ubcg67a/reda/src/src1/superposeOption1.py $4 $5 $6 $7 $8 $9 ${11} ${12}  > superposeOption1.log 
endif

