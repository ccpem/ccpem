#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
from ccpem_core import ccpem_utils
from ccpem_core import test_data
try:
    import modeller  # @UnusedImport
    modeller_available = True
    from ccpem_progs.choyce import model_build

except ImportError:
    modeller_available = False


class Test(unittest.TestCase):
    '''
    Unit test for Ribfind (invokes GUI).
    '''
    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_data = test_data.get_test_data_path()
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        '''
        Remove temporary data.
        '''
        if os.path.exists(path=self.test_output):
            shutil.rmtree(self.test_output)

    @unittest.skipIf(not modeller_available,
                     'Modeller not found: skipping FlexEM unittest')
    def test_model_build(self):
        ccpem_utils.print_header(message='Unit test - Choyce Model Build')
        #
        model_path = os.path.join(self.test_data,
                                  'pdb/1mbn.pdb')
        assert os.path.exists(path=model_path)
        seq_path = os.path.join(self.test_data,
                                'seq/5ni1_ac.fasta')
        assert os.path.exists(path=seq_path)
        #
        mb = model_build.ChoyceHomologyModel(
            job_location=self.test_output,
            model_path=model_path,
            seq_path=seq_path)
        assert os.path.exists(mb.output_model_path)

if __name__ == '__main__':
    unittest.main()
