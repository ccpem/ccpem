#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
import sys
import json
import modeller
from modeller.automodel import *
from ccpem_core import ccpem_utils


class ChoyceHomologyModel(object):
    '''
    Use modeller to generate homology model from input template model (PDB) 
    and target sequence (Fasta).
    '''
    def __init__(self,
                 model_path,
                 seq_path,
                 job_location=None):
        if job_location is not None:
            os.chdir(job_location)
        self.model_path = model_path
        self.seq_path = seq_path
        self.output_model_path = None
        self.env = modeller.environ()
        self.env.libs.topology.read(file='$(LIB)/top_heav.lib')
        self.env.libs.parameters.read(file='$(LIB)/par.lib')

        self.align()
        self.model()
        self.superpose()

    def align(self):
        ccpem_utils.print_sub_header(message='Choyce: Align')

        modeller.log.verbose()
        aln = modeller.alignment(self.env)
        # read template structure from "template.pdb"
        mdl = modeller.model(self.env)
        mdl.read(file=self.model_path)
        aln.append_model(mdl,
                         align_codes='template',
                         atom_files=self.model_path)

        # read target sequence"
        aln.append(file=self.seq_path,
                   align_codes='target',
                   alignment_format='fasta')

        # Align
        aln.salign(rr_file='$(LIB)/as1.sim.mat', # Substitution matrix used
                   output='',
                   max_gap_length=20,
                   gap_function=False, # If False then align2d not done
                   feature_weights=(1., 0., 0., 0., 0., 0.),
                   gap_penalties_1d=(-600, -400), #(-100, 0),
                   gap_penalties_2d=(3.5, 3.5, 3.5, 0.2, 4.0, 6.5, 2.0, 0.0, 0.0),
                   # d.p. score matrix
                   # write_weights=True, output_weights_file='salign.mtx'
                   similarity_flag=True) # Ensuring that the dynamic programming matrix is not scaled to a difference matrix
    
        # Output sequence alignment
        self.alignment_path = 'choyce_align.fasta'
        self.pir_path = 'choyce_align.ali'
        aln.check()
        aln.write(file = self.alignment_path, alignment_format='fasta')
        aln.write(file = self.pir_path, alignment_format='pir')

        # Store sequence identity of the alignment in file "Sequence_identity.txt"
        s1 = aln[0]
        s2 = aln[1]
        f = open('sequence_identity.txt', 'w')
        identity = '%s and %s have %d equivalences, and are %.2f%% identical' \
            % (s1, s2, s1.get_num_equiv(s2), s1.get_sequence_identity(s2))
        print '\n\nSeqeunce identity', identity
        f.write(identity)
        f.close()

    def model(self, num_models=1):
        ccpem_utils.print_sub_header(message='Choyce: Model')

        # create number of specified models via automodel()---------------------
        a = automodel(self.env,
                      alnfile=self.pir_path,                        #---target file should be named "target.fa" by the preprocess script "isFASTA.py"
                      knowns='template', 
                      sequence='target',         #---the structure file should be named "template.pdb" the target sequence file "target.fa" by the webserver
                      assess_methods=(assess.normalized_dope))      #---score the created models according to their normalized_dope score
        a.starting_model = 1
        a.ending_model = num_models                                          #---number of models to create
        a.make()
        self.output_model_path = os.path.abspath(a.outputs[0]['name'])

    def superpose(self):
        '''
        Superpose new model on reference.
        '''
        ccpem_utils.print_sub_header(message='Choyce: Superpose')

        aln = modeller.alignment(self.env, file=self.pir_path)
        mdl = modeller.model(self.env, file=self.model_path)
        atmsel = modeller.selection(mdl).only_atom_types('CA')
        mdl2 = modeller.model(self.env, file=self.output_model_path)
        atmsel.superpose(mdl2, aln)
        self.output_model_path = os.path.abspath(path='choyce_model.pdb')
        mdl2.write(self.output_model_path)


def main(json_path=None):
    ccpem_utils.print_header(message='CCP-EM Choyce')
    ccpem_utils.print_sub_header(message='Settings')
    if json_path is None:
        json_path = sys.argv[1]
    args = json.load(open(json_path, 'r'))
    reference_model_path = str(args['reference_model_path'])
    seq_path = str(args['sequence_path'])
    job_location = str(args['job_location'])

    # Print settings
    for key, value in args.items():
        print '  {0} : {1}'.format(key, value)

    model = ChoyceHomologyModel(
        model_path=reference_model_path,
        seq_path=seq_path,
        job_location=job_location)
    args['output_model_path'] = model.output_model_path
    json.dump(args, open(json_path, 'w'))

if __name__ == '__main__':
    main()
