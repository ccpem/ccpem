<?xml version="1.0" encoding="UTF-8"?>
<emdEntry version="1.9.6" accessCode="10101">
    <admin>
        <lastUpdate>2015-10-16</lastUpdate>
    </admin>
    <deposition>
        <status>HOLD</status>
        <depositionDate>2015-10-16</depositionDate>
        <depositionSite>RCSB</depositionSite>
        <processingSite>RCSB</processingSite>
        <title>3D reconstruction of TMV at 4.5A from film micrographs using Frealix</title>
        <authors>Rohou A, Grigorieff N</authors>
        <primaryReference published="false">
            <journalArticle>
                <authors>Rohou A, Grigorieff N</authors>
                <articleTitle>Frealix: model-based refinement of helical filament structures from electron micrographs.</articleTitle>
                <volume>186</volume>
                <firstPage>234</firstPage>
                <lastPage>244</lastPage>
                <year>2014</year>
                <externalReference type="pubmed">24657230</externalReference>
                <externalReference type="doi">10.1016/j.jsb.2014.03.012</externalReference>
            </journalArticle>
        </primaryReference>
    </deposition>
    <map>
        <file sizeKb="32001024" type="map" format="CCP4">EMD_10101.map.gz</file>
        <dataType>Image stored as floating point number (4 bytes)</dataType>
        <dimensions>
            <numRows>200</numRows>
            <numColumns>200</numColumns>
            <numSections>200</numSections>
        </dimensions>
        <origin>
            <originRow>0.</originRow>
            <originCol>0.</originCol>
            <originSec>0.</originSec>
        </origin>
        <limit>
            <limitRow>199.</limitRow>
            <limitCol>199.</limitCol>
            <limitSec>199.</limitSec>
        </limit>
        <spacing>
            <spacingRow>200</spacingRow>
            <spacingCol>200</spacingCol>
            <spacingSec>200</spacingSec>
        </spacing>
        <cell>
            <cellA units="A">232.0</cellA>
            <cellB units="A">232.0</cellB>
            <cellC units="A">232.0</cellC>
            <cellAlpha units="degrees">90.0</cellAlpha>
            <cellBeta units="degrees">90.0</cellBeta>
            <cellGamma units="degrees">90.0</cellGamma>
        </cell>
        <axisOrder>
            <axisOrderFast>x</axisOrderFast>
            <axisOrderMedium>y</axisOrderMedium>
            <axisOrderSlow>z</axisOrderSlow>
        </axisOrder>
        <statistics>
            <minimum>-2.5420177</minimum>
            <maximum>3.5639179</maximum>
            <average>-0.0005712905</average>
            <std>0.6760621</std>
        </statistics>
        <spaceGroupNumber>1</spaceGroupNumber>
        <pixelSpacing>
            <pixelX units="A">1.16</pixelX>
            <pixelY units="A">1.16</pixelY>
            <pixelZ units="A">1.16</pixelZ>
        </pixelSpacing>
        <contourLevel source="author">1.3</contourLevel>
        <annotationDetails>TMV map filtered using a negative B-factor, with amplitude matching against density derived from fitted atomic coordinates, a figure-of-merit filter, and subsequent helical symmetrization, as detailed</annotationDetails>
    </map>
    <supplement>
        <figureSet/>
    </supplement>
    <sample>
        <numComponents>2</numComponents>
        <sampleComponentList>
            <sampleComponent componentID="1">
                <entry>virus</entry>
                <molWtTheo units="kDa/nm">0.7</molWtTheo>
                <details>purified from infected tobacco leaves</details>
                <virus>
                    <empty>false</empty>
                    <enveloped>false</enveloped>
                    <isolate>OTHER</isolate>
                    <class>VIRION</class>
                    <natSource>
                        <hostSpecies ncbiTaxId="4094">Nicotiana sp.</hostSpecies>
                    </natSource>
                    <shell id="1">
                        <nameElement>capsid</nameElement>
                        <diameter>300.0</diameter>
                    </shell>
                </virus>
            </sampleComponent>
            <sampleComponent componentID="2">
                <entry>protein</entry>
                <sciName>tmv capsid</sciName>
                <protein>
                    <externalReferences/>
                </protein>
            </sampleComponent>
        </sampleComponentList>
    </sample>
    <experiment>
        <vitrification>
            <cryogenName>ETHANE</cryogenName>
            <instrument>HOMEMADE PLUNGER</instrument>
            <details>Blot for 2 seconds before plunging</details>
        </vitrification>
        <imaging>
            <electronSource>FIELD EMISSION GUN</electronSource>
            <electronDose units="e/A**2">20.0</electronDose>
            <imagingMode>BRIGHT FIELD</imagingMode>
            <nominalDefocusMin units="nm">1688.0</nominalDefocusMin>
            <nominalDefocusMax units="nm">3860.0</nominalDefocusMax>
            <illuminationMode>FLOOD BEAM</illuminationMode>
            <detector>OTHER</detector>
            <nominalCs units="mm">2.0</nominalCs>
            <microscope>FEI TECNAI F30</microscope>
            <specimenHolderModel>GATAN LIQUID NITROGEN</specimenHolderModel>
            <acceleratingVoltage units="kV">200</acceleratingVoltage>
            <nominalMagnification>59000</nominalMagnification>
        </imaging>
        <imageAcquisition>
            <samplingSize units="microns">7.0</samplingSize>
            <details>EMD-6325 reported images scanned using "ZEISS SCAI", but this does not seem to be on the pulldown list in the new system. </details>
        </imageAcquisition>
        <specimenPreparation>
            <specimenState>FILAMENT</specimenState>
            <specimenConc units="mg/ml">2.5</specimenConc>
            <buffer>
                <ph>7.</ph>
                <details>Phosphate, 5 mM EDTA</details>
            </buffer>
            <helicalParameters>
                <deltaPhi>22.0318</deltaPhi>
                <deltaZ>1.39259</deltaZ>
                <axialSymmetry>C1</axialSymmetry>
            </helicalParameters>
        </specimenPreparation>
    </experiment>
    <processing>
        <method>helical</method>
        <reconstruction>
            <software>Frealix</software>
            <resolutionByAuthor>4.5</resolutionByAuthor>
            <resolutionMethod>FSC 0.143 CUT-OFF</resolutionMethod>
            <details>Only data up to 5.5 Angstrom were used during refinement in the final rounds. In earlier rounds, this threshold was set to a lower resolution. Filaments were processed using Frealix assuming constant helical parameters and without continuity restraints on the helical parameters. </details>
        </reconstruction>
        <helical/>
    </processing>
</emdEntry>
