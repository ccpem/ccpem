<?xml version="1.0" encoding="UTF-8"?>
<emd emdb_id="EMD-1005" version="2.0">
    <admin>
        <current_status>
            <code>REL</code>
            <processing_site>PDBe</processing_site>
        </current_status>
        <sites>
            <deposition>PDBe</deposition>
            <last_processing>PDBe</last_processing>
        </sites>
        <key_dates>
            <deposition>2002-08-28</deposition>
            <header_release>2002-08-30</header_release>
            <map_release>2003-08-30</map_release>
            <update>2014-10-22</update>
        </key_dates>
        <title>Structure of the Escherichia coli ribosomal termination complex with release factor 2.</title>
        <authors_list>
            <author>Klaholz BP</author>
            <author>Pape T</author>
            <author>Zavialov AV</author>
            <author>Myasnikov AG</author>
            <author>Orlova EV</author>
            <author>Vestergaard B</author>
            <author>Ehrenberg M</author>
            <author>van Heel M</author>
        </authors_list>
        <keywords></keywords>
    </admin>
    <crossreferences>
        <citation_list>
            <primary_citation>
                <journal_citation published="true">
                    <author order="1">Klaholz BP</author>
                    <author order="2">Pape T</author>
                    <author order="3">Zavialov AV</author>
                    <author order="4">Myasnikov AG</author>
                    <author order="5">Orlova EV</author>
                    <author order="6">Vestergaard B</author>
                    <author order="7">Ehrenberg M</author>
                    <author order="8">van Heel M</author>
                    <title>Structure of the Escherichia coli ribosomal termination complex with release factor 2.</title>
                    <journal>NATURE</journal>
                    <volume>421</volume>
                    <first_page>90</first_page>
                    <last_page>94</last_page>
                    <year>2003</year>
                    <external_references type="pubmed">12511961</external_references>
                    <external_references type="doi">doi:10.1038/nature01225</external_references>
                </journal_citation>
            </primary_citation>
        </citation_list>
        <pdb_list>
            <pdb_reference id="1">
                <pdb_id>1ml5</pdb_id>
                <relationship>
                    <in_frame>FULLOVERLAP</in_frame>
                </relationship>
            </pdb_reference>
        </pdb_list>
    </crossreferences>
    <sample>
        <name>release factor RF2 bound to E. coli ribosomes</name>
        <supramolecule_list>
            <supramolecule id="1000" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="sample">
                <oligomeric_state>monomer</oligomeric_state>
                <number_unique_components>2</number_unique_components>
                <molecular_weight>
                    <experimental units="MDa">2.3</experimental>
                    <method>sedimentation</method>
                </molecular_weight>
            </supramolecule>
            <supramolecule id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="complex">
                <name synonym="ribosome">ribosome</name>
                <category>ribosome-prokaryote</category>
                <details>Proteins L7/L12 of the LSU 50S subunit are not seen
 in the map</details>
                <recombinant_exp_flag>false</recombinant_exp_flag>
                <natural_source>
                    <organism ncbi="562">Escherichia coli</organism>
                </natural_source>
                <recombinant_expression/>
                <molecular_weight>
                    <experimental units="MDa">2.3</experimental>
                    <theoretical units="MDa">2.3</theoretical>
                </molecular_weight>
                <ribosome-details>LSU 50S</ribosome-details>
            </supramolecule>
        </supramolecule_list>
        <macromolecule_list>
            <macromolecule id="2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ligand">
                <name synonym="RF2">release factor 2</name>
                <natural_source>
                    <organism ncbi="469008">Escherichia coli BL21(DE3)</organism>
                    <strain>BL21 (DE3)</strain>
                </natural_source>
                <molecular_weight>
                    <experimental units="MDa">0.040</experimental>
                    <theoretical units="MDa">0.04</theoretical>
                </molecular_weight>
                <number_of_copies>1</number_of_copies>
                <oligomeric_state>monomer</oligomeric_state>
                <recombinant_exp_flag>true</recombinant_exp_flag>
                <external_references type="INTERPRO">IPR004374</external_references>
                <natural_source>
                    <organism ncbi="469008">Escherichia coli BL21(DE3)</organism>
                    <strain>BL21 (DE3)</strain>
                </natural_source>
                <recombinant_expression>
                    <organism ncbi="562">Escherichia coli</organism>
                    <plasmid>pET11a</plasmid>
                </recombinant_expression>
            </macromolecule>
        </macromolecule_list>
    </sample>
    <structure_determination_list>
        <structure_determination>
            <method>singleParticle</method>
            <aggregation_state>particle</aggregation_state>
            <specimen_preparation_list>
                <specimen_preparation id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="base_preparation_type">
                    <concentration units="mg/mL">0.15</concentration>
                    <buffer>
                        <ph>7.5</ph>
                        <details>polymix buffer</details>
                    </buffer>
                    <staining>
                        <type>negative</type>
                        <details>no staining, cryo-EM with holey carbon grids</details>
                    </staining>
                    <vitrification>
                        <cryogen_name>ETHANE</cryogen_name>
                        <chamber_humidity>45</chamber_humidity>
                        <chamber_temperature>20</chamber_temperature>
                        <instrument>HOMEMADE PLUNGER</instrument>
                        <details>Vitrification instrument: home-made cryo-plunger</details>
                        <method>blot for 2 seconds before plunging</method>
                    </vitrification>
                </specimen_preparation>
            </specimen_preparation_list>
            <microscopy_list>
                <microscopy id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="base_microscopy_type">
                    <microscope>FEI/PHILIPS CM200FEG/ST</microscope>
                    <illumination_mode>FLOOD BEAM</illumination_mode>
                    <imaging_mode>BRIGHT FIELD</imaging_mode>
                    <electron_source>FIELD EMISSION GUN</electron_source>
                    <acceleration_voltage>200</acceleration_voltage>
                    <nominal_cs>2.1</nominal_cs>
                    <nominal_defocus_min>500</nominal_defocus_min>
                    <nominal_defocus_max>2900</nominal_defocus_max>
                    <nominal_magnification>50000.</nominal_magnification>
                    <calibrated_magnification>48000.</calibrated_magnification>
                    <specimen_holder_model>GATAN LIQUID NITROGEN</specimen_holder_model>
                    <temperature>
                        <temperature_min units="Kelvin">100</temperature_min>
                        <temperature_max units="Kelvin">102</temperature_max>
                        <temperature_average units="Kelvin">100</temperature_average>
                    </temperature>
                    <date>2001-09-06</date>
                    <image_recording_list>
                        <image_recording>
                            <film_or_detector_model category="film">KODAK SO-163 FILM</film_or_detector_model>
                            <digitization_details>
                                <scanner>PATCHWORK DENSITOMETER</scanner>
                                <sampling_interval units="um">3</sampling_interval>
                            </digitization_details>
                            <number_real_images>78</number_real_images>
                            <average_electron_dose_per_image units="e/A**2">10</average_electron_dose_per_image>
                            <bits_per_pixel>12.</bits_per_pixel>
                        </image_recording>
                    </image_recording_list>
                    <specimen_holder>Side entry liquid nitrogen-cooled cryo specimen holder.</specimen_holder>
                </microscopy>
            </microscopy_list>
            <image_processing id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="singleparticle_processing_type">
                <ctf_correction>
                    <details>each particle</details>
                </ctf_correction>
                <final_reconstruction>
                    <number_images_used>15800</number_images_used>
                    <applied_symmetry>
                        <point_group>C1</point_group>
                    </applied_symmetry>
                    <algorithm>angular reconstitution, exact filtered back-projection</algorithm>
                    <resolution>14</resolution>
                    <resolution_method>FSC: value between 3 sigma and 0.5 cut-off</resolution_method>
                    <software_list>
                        <software>
                            <name>IMAGIC</name>
                        </software>
                    </software_list>
                    <details>exact filtered back-projection</details>
                </final_reconstruction>
                <final_angle_assignment>
                    <details>beta gamma</details>
                </final_angle_assignment>
                <final_two_d_classification>
                    <number_classes>103</number_classes>
                </final_two_d_classification>
            </image_processing>
        </structure_determination>
    </structure_determination_list>
    <map size_kbytes="6751" format="CCP4">
        <file>emd_1005.map.gz</file>
        <symmetry>
            <space_group>1</space_group>
        </symmetry>
        <data_type>Image stored as Reals</data_type>
        <dimensions>
            <col>120</col>
            <row>120</row>
            <sec>120</sec>
        </dimensions>
        <origin>
            <col>-60</col>
            <row>-60</row>
            <sec>-60</sec>
        </origin>
        <spacing>
            <x>120</x>
            <y>120</y>
            <z>120</z>
        </spacing>
        <cell>
            <a>290.28</a>
            <b>290.28</b>
            <c>290.28</c>
            <alpha>90</alpha>
            <beta>90</beta>
            <gamma>90</gamma>
        </cell>
        <axis_order>
            <fast>X</fast>
            <medium>Y</medium>
            <slow>Z</slow>
        </axis_order>
        <statistics>
            <minimum>-0.159669</minimum>
            <maximum>0.213811</maximum>
            <average>-0.000130684</average>
            <std>0.0224271</std>
        </statistics>
        <pixel_spacing>
            <x>2.419</x>
            <y>2.419</y>
            <z>2.419</z>
        </pixel_spacing>
        <contour_list>
            <contour primary="true">
                <level>0.0401</level>
            </contour>
        </contour_list>
        <annotation_details>E. coli ribosomal termination
        complex       with release factor RF2.</annotation_details>
        <details>::::EMDATABANK.org::::EMD-1005::::</details>
    </map>
    <interpretation>
        <modelling_list>
            <modelling>
                <initial_model>
                    <access_code>1GIX</access_code>
                </initial_model>
                <refinement_protocol>rigid body</refinement_protocol>
                <software_list>
                    <software>
                        <name>eye</name>
                    </software>
                </software_list>
                <details>Protocol: rigid body. rigid body of three individual domains keeping the connectivity; linkers betweenn domains defined based on temperature factors of crystal structure, conserved Gly and Pro residues, proteolytic sites, and global domain architecture</details>
                <target_criteria>best visual fit using the program O</target_criteria>
                <refinement_space>REAL</refinement_space>
            </modelling>
            <modelling>
                <initial_model>
                    <access_code>1GIY</access_code>
                </initial_model>
                <refinement_protocol>rigid body</refinement_protocol>
                <software_list>
                    <software>
                        <name>eye</name>
                    </software>
                </software_list>
                <details>Protocol: rigid body. rigid body of three individual domains keeping the connectivity; linkers betweenn domains defined based on temperature factors of crystal structure, conserved Gly and Pro residues, proteolytic sites, and global domain architecture</details>
                <target_criteria>best visual fit using the program O</target_criteria>
                <refinement_space>REAL</refinement_space>
            </modelling>
        </modelling_list>
    </interpretation>
</emd>
