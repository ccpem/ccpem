<?xml version="1.0" encoding="UTF-8"?>
<emd emdb_id="EMD-1876" version="2.0">
    <admin>
        <current_status>
            <code>REL</code>
            <processing_site>PDBe</processing_site>
        </current_status>
        <sites>
            <deposition>PDBe</deposition>
            <last_processing>PDBe</last_processing>
        </sites>
        <key_dates>
            <deposition>2011-02-15</deposition>
            <header_release>2011-02-21</header_release>
            <map_release>2011-10-12</map_release>
            <update>2011-10-12</update>
        </key_dates>
        <title>Structural basis of substrate shuttling in bovine mitochondrial supercomplex I1III2IV1 by single particle cryo-EM</title>
        <authors_list>
            <author>Althoff T</author>
            <author>Mills DJ</author>
            <author>Popot J-L</author>
            <author>Kuehlbrandt W</author>
        </authors_list>
        <keywords>Supercomplex B, mitochondria, respiratory chain, complex I, complex III, complex IV, amphipol A8-35, random conical tilt</keywords>
    </admin>
    <crossreferences>
        <citation_list>
            <primary_citation>
                <journal_citation published="true">
                    <author order="1">Althoff T</author>
                    <author order="2">Mills DJ</author>
                    <author order="3">Popot J-L</author>
                    <author order="4">Kuehlbrandt W</author>
                    <title>Arrangement of electron transport chain components in bovine mitochondrial supercomplex I1III2IV1.</title>
                    <journal>EMBO J.</journal>
                    <volume>30</volume>
                    <first_page>4652</first_page>
                    <last_page>4664</last_page>
                    <year>2011</year>
                    <external_references type="pubmed">21909073</external_references>
                    <external_references type="doi">doi:10.1038/emboj.2011.324</external_references>
                </journal_citation>
            </primary_citation>
            <secondary_citation>
                <non_journal_citation published="true">
                    <author order="1">Althoff T</author>
                    <editor order="1">n/a</editor>
                    <book_title>Thesis</book_title>
                    <thesis_title>Strukturelle Untersuchungen am Superkomplex I1III2IV1 der Atmungskette mittels Kryoelektronenmikroskopie</thesis_title>
                    <book_chapter_title>Strukturelle Untersuchungen am Superkomplex I1III2IV1 der Atmungskette mittels Kryoelektronenmikroskopie</book_chapter_title>
                    <volume>n/a</volume>
                    <publisher>Johann Wolfgang Goethe Universitaet</publisher>
                    <publication_location>Frankfurt am Main (Germany)</publication_location>
                    <first_page>1</first_page>
                    <last_page>251</last_page>
                    <year>2011</year>
                    <external_references type="pubmed"/>
                </non_journal_citation>
            </secondary_citation>
        </citation_list>
        <pdb_list>
            <pdb_reference id="1">
                <pdb_id>2ybb</pdb_id>
                <relationship>
                    <in_frame>FULLOVERLAP</in_frame>
                </relationship>
            </pdb_reference>
        </pdb_list>
    </crossreferences>
    <sample>
        <name>Respiratory supercomplex B (I1III2IV1) composed of complex I, dimeric complex III and complex IV from bovine heart mitochondria</name>
        <supramolecule_list>
            <supramolecule id="1000" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="sample">
                <details>The supercomplexes were kept soluble by amphipol A8-35</details>
                <oligomeric_state>A monomer of complex I assembles with a dimer of complex III and a monomer of complex IV</oligomeric_state>
                <number_unique_components>4</number_unique_components>
                <molecular_weight>
                    <theoretical units="MDa">1.7</theoretical>
                </molecular_weight>
            </supramolecule>
        </supramolecule_list>
        <macromolecule_list>
            <macromolecule id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="protein_or_peptide">
                <name synonym="Complex I">NADH-dehydrogenase</name>
                <natural_source>
                    <organism ncbi="9913">Bos taurus</organism>
                    <synonym_organism>Cattle</synonym_organism>
                    <tissue>Heart Muscle</tissue>
                    <organelle>Mitochondrion</organelle>
                    <cellular_location>Inner mitochondrial membrane</cellular_location>
                </natural_source>
                <molecular_weight>
                    <experimental units="MDa">0.98</experimental>
                    <theoretical units="MDa">0.98</theoretical>
                </molecular_weight>
                <number_of_copies>1</number_of_copies>
                <oligomeric_state>Monomer</oligomeric_state>
                <recombinant_exp_flag>false</recombinant_exp_flag>
                <recombinant_expression/>
                <sequence/>
            </macromolecule>
            <macromolecule id="2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="protein_or_peptide">
                <name synonym="Complex III">Cytochrome c reductase</name>
                <natural_source>
                    <organism ncbi="9913">Bos taurus</organism>
                    <synonym_organism>Cattle</synonym_organism>
                    <tissue>Heart Muscle</tissue>
                    <organelle>Mitochondrion</organelle>
                    <cellular_location>Inner mitochondrial membrane</cellular_location>
                </natural_source>
                <molecular_weight>
                    <experimental units="MDa">0.243</experimental>
                    <theoretical units="MDa">0.241</theoretical>
                </molecular_weight>
                <number_of_copies>2</number_of_copies>
                <oligomeric_state>Dimer</oligomeric_state>
                <recombinant_exp_flag>false</recombinant_exp_flag>
                <recombinant_expression/>
                <sequence/>
            </macromolecule>
            <macromolecule id="3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="protein_or_peptide">
                <name synonym="Complex IV">Cytochrome c oxidase</name>
                <natural_source>
                    <organism ncbi="9913">Bos taurus</organism>
                    <synonym_organism>Cattle</synonym_organism>
                    <tissue>Heart Muscle</tissue>
                    <organelle>Mitochondrion</organelle>
                    <cellular_location>Inner mitochondrial membrane</cellular_location>
                </natural_source>
                <molecular_weight>
                    <experimental units="MDa">0.207</experimental>
                    <theoretical units="MDa">0.204</theoretical>
                </molecular_weight>
                <number_of_copies>1</number_of_copies>
                <oligomeric_state>Monomer</oligomeric_state>
                <recombinant_exp_flag>false</recombinant_exp_flag>
                <recombinant_expression/>
                <sequence/>
            </macromolecule>
            <macromolecule id="4" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="protein_or_peptide">
                <name synonym="Cytochrome c">Cytochrome c</name>
                <natural_source>
                    <organism ncbi="9913">Bos taurus</organism>
                    <synonym_organism>Cattle</synonym_organism>
                    <tissue>Heart Muscle</tissue>
                    <organelle>Mitochondrion</organelle>
                    <cellular_location>Inner mitochondrial membrane</cellular_location>
                </natural_source>
                <molecular_weight>
                    <experimental units="MDa">0.012</experimental>
                    <theoretical units="MDa">0.012</theoretical>
                </molecular_weight>
                <number_of_copies>1</number_of_copies>
                <oligomeric_state>Monomer</oligomeric_state>
                <recombinant_exp_flag>false</recombinant_exp_flag>
                <recombinant_expression/>
                <sequence/>
            </macromolecule>
        </macromolecule_list>
    </sample>
    <structure_determination_list>
        <structure_determination>
            <method>singleParticle</method>
            <aggregation_state>particle</aggregation_state>
            <specimen_preparation_list>
                <specimen_preparation id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="base_preparation_type">
                    <concentration units="mg/mL">0.3</concentration>
                    <buffer>
                        <ph>7.7</ph>
                        <details>10 mM KCl, 15 mM HEPES, residual trehalose</details>
                    </buffer>
                    <grid>
                        <details>Continuous carbon 400 mesh copper grid</details>
                    </grid>
                    <vitrification>
                        <cryogen_name>ETHANE</cryogen_name>
                        <chamber_humidity>70</chamber_humidity>
                        <chamber_temperature>95</chamber_temperature>
                        <instrument>HOMEMADE PLUNGER</instrument>
                        <details>Vitrification instrument: Custom-made hand plunger. Sample was adsorbed to carbon film for 30 seconds before blotting</details>
                        <method>One-sided blotting for 26 - 30 seconds before plunging</method>
                    </vitrification>
                </specimen_preparation>
            </specimen_preparation_list>
            <microscopy_list>
                <microscopy id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="base_microscopy_type">
                    <microscope>FEI POLARA 300</microscope>
                    <illumination_mode>FLOOD BEAM</illumination_mode>
                    <imaging_mode>BRIGHT FIELD</imaging_mode>
                    <electron_source>FIELD EMISSION GUN</electron_source>
                    <acceleration_voltage>200</acceleration_voltage>
                    <nominal_cs>2.0</nominal_cs>
                    <nominal_defocus_min>500</nominal_defocus_min>
                    <nominal_defocus_max>6000</nominal_defocus_max>
                    <nominal_magnification>59000.</nominal_magnification>
                    <calibrated_magnification>58829.</calibrated_magnification>
                    <specimen_holder_model>GATAN LIQUID NITROGEN</specimen_holder_model>
                    <temperature>
                        <temperature_min units="Kelvin">78</temperature_min>
                        <temperature_max units="Kelvin">95</temperature_max>
                        <temperature_average units="Kelvin">78</temperature_average>
                    </temperature>
                    <alignment_procedure>
                        <legacy>
                            <astigmatism>Objective lens astigmatism was corrected at 200,000 x magnification</astigmatism>
                            <electron_beam_tilt_params>Beam tilt was corrected by adjusting pivot points</electron_beam_tilt_params>
                        </legacy>
                    </alignment_procedure>
                    <details>Low dose conditions. For random conical tilt each area was imaged twice with the first image taken at a tilt angle of -45 or -50 degree</details>
                    <image_recording_list>
                        <image_recording>
                            <film_or_detector_model category="film">KODAK SO-163 FILM</film_or_detector_model>
                            <digitization_details>
                                <scanner>ZEISS SCAI</scanner>
                                <sampling_interval units="um">7</sampling_interval>
                            </digitization_details>
                            <number_real_images>154</number_real_images>
                            <average_electron_dose_per_image units="e/A**2">25</average_electron_dose_per_image>
                            <details>Scanned images were binned to 2.38 Angstroms per pixel in IMAGIC</details>
                            <od_range>0.7</od_range>
                            <bits_per_pixel>8.</bits_per_pixel>
                        </image_recording>
                    </image_recording_list>
                    <specimen_holder>FEI Polara MSC</specimen_holder>
                    <tilt_angle_min>-50</tilt_angle_min>
                    <tilt_angle_max>0</tilt_angle_max>
                </microscopy>
            </microscopy_list>
            <image_processing id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="singleparticle_processing_type">
                <details>Tilt pairs of particles were selected interactively in JWEB</details>
                <ctf_correction>
                    <details>Phase-flippping on each particle</details>
                </ctf_correction>
                <final_reconstruction>
                    <number_images_used>10684</number_images_used>
                    <algorithm>Random conical tilt and projection matching with direct Fourier backprojection and SIRT</algorithm>
                    <resolution>19</resolution>
                    <resolution_method>FSC 0.143</resolution_method>
                    <software_list>
                        <software>
                            <name>SPIDER, IMAGIC</name>
                        </software>
                    </software_list>
                    <details>Final map was calculated from combined tilted and untilted datasets with 10684 projections each and omitting 30 per cent of images with lowest cross correlation coefficient using SIRT</details>
                </final_reconstruction>
                <final_angle_assignment>
                    <details>SPIDER: theta 360 degrees, phi 360 degrees</details>
                </final_angle_assignment>
            </image_processing>
        </structure_determination>
    </structure_determination_list>
    <map size_kbytes="5489" format="CCP4">
        <file>emd_1876.map.gz</file>
        <symmetry>
            <space_group>1</space_group>
        </symmetry>
        <data_type>Image stored as Reals</data_type>
        <dimensions>
            <col>112</col>
            <row>112</row>
            <sec>112</sec>
        </dimensions>
        <origin>
            <col>-56</col>
            <row>-56</row>
            <sec>-55</sec>
        </origin>
        <spacing>
            <x>112</x>
            <y>112</y>
            <z>112</z>
        </spacing>
        <cell>
            <a>533.12</a>
            <b>533.12</b>
            <c>533.12</c>
            <alpha>90</alpha>
            <beta>90</beta>
            <gamma>90</gamma>
        </cell>
        <axis_order>
            <fast>Y</fast>
            <medium>X</medium>
            <slow>Z</slow>
        </axis_order>
        <statistics>
            <minimum>-8.248889999999999</minimum>
            <maximum>18.839099999999998</maximum>
            <average>-0.00000000106836</average>
            <std>1.</std>
        </statistics>
        <pixel_spacing>
            <x>4.76</x>
            <y>4.76</y>
            <z>4.76</z>
        </pixel_spacing>
        <contour_list>
            <contour primary="true">
                <level>1.3</level>
                <source>AUTHOR</source>
            </contour>
        </contour_list>
        <annotation_details>3D cryo-EM map of supercomplex B (I1III2IV1) from bovine heart mitochondria</annotation_details>
        <details>::::EMDATABANK.org::::EMD-1876::::</details>
    </map>
    <interpretation>
        <modelling_list>
            <modelling>
                <initial_model>
                    <access_code>3M9S</access_code>
                </initial_model>
                <refinement_protocol>rigid body</refinement_protocol>
                <software_list>
                    <software>
                        <name>UCSF Chimera</name>
                    </software>
                </software_list>
                <details>Protocol: Rigid Body. Only one monomer from the asymmetric unit was used. Docking was done manually according to detailed map features</details>
                <refinement_space>REAL</refinement_space>
            </modelling>
            <modelling>
                <initial_model>
                    <access_code>3IAM</access_code>
                </initial_model>
                <refinement_protocol>rigid body</refinement_protocol>
                <software_list>
                    <software>
                        <name>UCSF Chimera</name>
                    </software>
                </software_list>
                <details>Protocol: Rigid Body. Only one monomer from the asymmetric unit was used. Docking was done manually according to detailed map features. Gave better fit than</details>
                <refinement_space>REAL</refinement_space>
            </modelling>
            <modelling>
                <initial_model>
                    <access_code>3M9C</access_code>
                </initial_model>
                <refinement_protocol>rigid body</refinement_protocol>
                <software_list>
                    <software>
                        <name>UCSF Chimera</name>
                    </software>
                </software_list>
                <details>Protocol: Rigid Body. Docking was done manually according to detailed map features</details>
                <refinement_space>REAL</refinement_space>
            </modelling>
            <modelling>
                <initial_model>
                    <access_code>1PP9</access_code>
                </initial_model>
                <refinement_protocol>rigid body</refinement_protocol>
                <software_list>
                    <software>
                        <name>UCSF Chimera</name>
                    </software>
                </software_list>
                <details>Protocol: Rigid Body. Docking was done manually according to detailed map features</details>
                <refinement_space>REAL</refinement_space>
            </modelling>
            <modelling>
                <initial_model>
                    <access_code>1BGY</access_code>
                    <chain>
                        <id>K</id>
                    </chain>
                    <chain>
                        <id>W</id>
                    </chain>
                </initial_model>
                <refinement_protocol>rigid body</refinement_protocol>
                <software_list>
                    <software>
                        <name>UCSF Chimera</name>
                    </software>
                </software_list>
                <details>Protocol: Rigid Body. Structure was aligned to PDB 1PP9 with Chimera MatchMaker</details>
                <refinement_space>REAL</refinement_space>
            </modelling>
            <modelling>
                <initial_model>
                    <access_code>3CX5</access_code>
                </initial_model>
                <refinement_protocol>rigid body</refinement_protocol>
                <software_list>
                    <software>
                        <name>UCSF Chimera</name>
                    </software>
                </software_list>
                <details>Protocol: Rigid Body. Structure was aligned to PDB 1PP9 with Chimera MatchMaker</details>
                <refinement_space>REAL</refinement_space>
            </modelling>
            <modelling>
                <initial_model>
                    <access_code>2BCC</access_code>
                </initial_model>
                <refinement_protocol>rigid body</refinement_protocol>
                <software_list>
                    <software>
                        <name>UCSF Chimera</name>
                    </software>
                </software_list>
                <details>Protocol: Rigid Body. Used for comparison of the conformation of the Rieske-domain. Structure was aligned to PDB 1PP9 with Chimera MatchMaker</details>
                <refinement_space>REAL</refinement_space>
            </modelling>
            <modelling>
                <initial_model>
                    <access_code>1OCC</access_code>
                </initial_model>
                <refinement_protocol>rigid body</refinement_protocol>
                <software_list>
                    <software>
                        <name>UCSF Chimera</name>
                    </software>
                </software_list>
                <details>Protocol: Rigid Body. Only one monomer from the asymmetric unit was used. Docking was done manually according to detailed map features</details>
                <refinement_space>REAL</refinement_space>
            </modelling>
            <modelling>
                <initial_model>
                    <access_code>2B4Z</access_code>
                </initial_model>
                <refinement_protocol>rigid body</refinement_protocol>
                <software_list>
                    <software>
                        <name>UCSF Chimera</name>
                    </software>
                </software_list>
                <details>Protocol: Rigid Body. Structure was aligned to cytochrome c (chain W) in PDB 3CX5 with Chimera Matchmaker</details>
                <refinement_space>REAL</refinement_space>
            </modelling>
        </modelling_list>
    </interpretation>
</emd>
