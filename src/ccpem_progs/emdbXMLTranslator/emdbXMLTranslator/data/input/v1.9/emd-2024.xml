<?xml version="1.0" encoding="UTF-8"?>
<emdEntry accessCode="2024" version="1.9.6">
    <admin>
        <lastUpdate>2012-09-05</lastUpdate>
    </admin>
    <deposition>
        <status>REL</status>
        <depositionDate>2012-01-04</depositionDate>
        <depositionSite>PDBe</depositionSite>
        <processingSite>PDBe</processingSite>
        <headerReleaseDate>2012-01-20</headerReleaseDate>
        <mapReleaseDate>2012-01-27</mapReleaseDate>
        <title>Three-dimensional reconstruction of another targeted individual 17nm nascent high-density lipoprotein (HDL) particle by individual-particle electron tomography (IPET). The reconstruction displayed a ring-shaped structure of containing three protein - apoipoprotein A-Is (28 kDa each).</title>
        <authors>Zhang L, Ren G</authors>
        <keywords>reconstituted 17nm nascent high-density lipoprotein (HDL)</keywords>
        <primaryReference published="true">
            <journalArticle>
                <authors>Zhang L, Ren G</authors>
                <articleTitle>IPET and FETR: experimental approach for studying molecular structure dynamics by cryo-electron tomography of a single-molecule structure.</articleTitle>
                <journal>PLOS ONE</journal>
                <volume>7</volume>
                <firstPage>e30249</firstPage>
                <lastPage>e30249</lastPage>
                <year>2012</year>
                <externalReference type="pubmed">22291925</externalReference>
                <externalReference type="doi">doi:10.1371/journal.pone.0030249</externalReference>
            </journalArticle>
        </primaryReference>
    </deposition>
    <map>
        <file format="CCP4" sizeKb="31251" type="map">emd_2024.map.gz</file>
        <dataType>Image stored as Reals</dataType>
        <dimensions>
            <numColumns>200</numColumns>
            <numRows>200</numRows>
            <numSections>200</numSections>
        </dimensions>
        <origin>
            <originCol>0</originCol>
            <originRow>0</originRow>
            <originSec>0</originSec>
        </origin>
        <limit>
            <limitCol>199</limitCol>
            <limitRow>199</limitRow>
            <limitSec>199</limitSec>
        </limit>
        <spacing>
            <spacingCol>200</spacingCol>
            <spacingRow>200</spacingRow>
            <spacingSec>200</spacingSec>
        </spacing>
        <cell>
            <cellA units="A">346.0</cellA>
            <cellB units="A">346.0</cellB>
            <cellC units="A">346.0</cellC>
            <cellAlpha units="degrees">90.0</cellAlpha>
            <cellBeta units="degrees">90.0</cellBeta>
            <cellGamma units="degrees">90.0</cellGamma>
        </cell>
        <axisOrder>
            <axisOrderFast>X</axisOrderFast>
            <axisOrderMedium>Y</axisOrderMedium>
            <axisOrderSlow>Z</axisOrderSlow>
        </axisOrder>
        <statistics>
            <minimum>-0.05142312</minimum>
            <maximum>0.15066135</maximum>
            <average>0.00679005</average>
            <std>0.02501582</std>
        </statistics>
        <spaceGroupNumber>1</spaceGroupNumber>
        <details>::::EMDATABANK.org::::EMD-2024::::</details>
        <pixelSpacing>
            <pixelX units="A">1.73</pixelX>
            <pixelY units="A">1.73</pixelY>
            <pixelZ units="A">1.73</pixelZ>
        </pixelSpacing>
        <contourLevel source="author">0.1</contourLevel>
        <annotationDetails>17nm high density lipoprotein (HDL) particle</annotationDetails>
    </map>
    <supplement>
        <maskSet/>
        <sliceSet/>
        <figureSet/>
        <fscSet/>
    </supplement>
    <sample>
        <numComponents>1</numComponents>
        <name>second 17nm nascent HDL particle</name>
        <compDegree>monomer</compDegree>
        <molWtTheo units="MDa">0.2</molWtTheo>
        <details>Fresh sample was prepared with 2 days before using.</details>
        <molWtExp units="MDa">0.2</molWtExp>
        <sampleComponentList>
            <sampleComponent componentID="1">
                <entry>protein</entry>
                <sciName>17 nm nascent HDL</sciName>
                <synName>17 nm nascent HDL</synName>
                <molWtTheo units="MDa">0.2</molWtTheo>
                <molWtExp units="MDa">0.2</molWtExp>
                <details>HDL contains lipids and three apoA-I molecules. Each A-I molecular weight is about 28kDa.</details>
                <protein>
                    <sciSpeciesName ncbiTaxId="9606">Homo sapiens</sciSpeciesName>
                    <recombinantExpFlag>false</recombinantExpFlag>
                    <oligomericDetails>monomer</oligomericDetails>
                    <synSpeciesName>Human</synSpeciesName>
                    <numCopies>1</numCopies>
                    <sciSpeciesStrain>nascent HDL</sciSpeciesStrain>
                    <externalReferences/>
                    <natSource>
                        <cellLocation>Plasma</cellLocation>
                    </natSource>
                    <engSource/>
                </protein>
            </sampleComponent>
        </sampleComponentList>
    </sample>
    <experiment>
        <vitrification>
            <method>Blot for 2 seconds before plunging</method>
            <cryogenName>NITROGEN</cryogenName>
            <details>Vitrification instrument: Manual</details>
            <humidity>90</humidity>
            <instrument>HOMEMADE PLUNGER</instrument>
            <temperature units="Kelvin">103</temperature>
        </vitrification>
        <imaging>
            <electronSource>LAB6</electronSource>
            <electronDose units="e/A**2">140</electronDose>
            <imagingMode>BRIGHT FIELD</imagingMode>
            <nominalDefocusMin units="nm">1000</nominalDefocusMin>
            <nominalDefocusMax units="nm">2000</nominalDefocusMax>
            <illuminationMode>FLOOD BEAM</illuminationMode>
            <specimenHolder>Gatan</specimenHolder>
            <details>Tilt step is 1.5 degree</details>
            <detector>GATAN ULTRASCAN 4000 (4k x 4k)</detector>
            <nominalCs units="mm">2.2</nominalCs>
            <tiltAngleMin units="degrees">-69</tiltAngleMin>
            <tiltAngleMax units="degrees">69</tiltAngleMax>
            <temperature units="Kelvin">100</temperature>
            <microscope>FEI TECNAI 12</microscope>
            <specimenHolderModel>GATAN LIQUID NITROGEN</specimenHolderModel>
            <acceleratingVoltage units="kV">120</acceleratingVoltage>
            <nominalMagnification>67000</nominalMagnification>
        </imaging>
        <imageAcquisition>
            <numDigitalImages>93</numDigitalImages>
            <samplingSize units="microns">1.73</samplingSize>
            <quantBitNumber>16</quantBitNumber>
        </imageAcquisition>
        <fitting/>
        <specimenPreparation>
            <staining>EM Specimens were prepared by standard cryo-EM protocol.</staining>
            <specimenConc units="mg/ml">0.01</specimenConc>
            <specimenSupportDetails>200 mesh glow-discharged holey thin carbon-coated EM grids (Cu-200HN, Pacific Grid-Tech, USA)</specimenSupportDetails>
            <buffer>
                <details>1X Dulbeccos phosphate-buffered saline (Invitrogen, La Jolla, CA), 2.7 mM KCl, 1.46 mM KH2PO4, 136.9 mM NaCl, and 8.1 mM Na2HPO4</details>
                <ph>7.4</ph>
            </buffer>
        </specimenPreparation>
    </experiment>
    <processing>
        <method>subtomogramAveraging</method>
        <reconstruction>
            <algorithm>Focused ET reconstruction algorithms</algorithm>
            <eulerAnglesDetails>Tomography tilt angle from -60 to 60 in step of 1.5</eulerAnglesDetails>
            <software>IPET and FETR</software>
            <ctfCorrection>TOMOCTF</ctfCorrection>
            <details>Map was reconstructed by individual-particle electron tomography (IPET)and Focus ET Reconstruction Algorithm.</details>
            <resolutionByAuthor>36</resolutionByAuthor>
            <resolutionMethod>36</resolutionMethod>
        </reconstruction>
        <subtomogramAveraging>
            <details>Single targeted particle"s images were reconstructed by focus ET reconstruction (FETR) algorithm of individual particle electron tomography (IPET) method. Average number of tilts used in the 3D reconstructions: 93. Average tomographic tilt angle increment: 1.5.</details>
        </subtomogramAveraging>
    </processing>
</emdEntry>
