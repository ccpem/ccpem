"""
RNARIBFIND: Detecting rigid bodies in ncRNA structures.
Based on and including code from RIBFIND by Arun Pandurangan and Maya Topf, with permission.
"""

import os
import sys
import time
import re
from Bio.PDB import *
from numpy import *

def str_object(pdbfile):
    '''
    Given a pdb file creates a structure object
    '''
    if pdbfile.rsplit('.')[-1] == 'pdb':
        parser = PDBParser()
    elif pdbfile.rsplit('.')[-1] == 'cif' or pdbfile.rsplit('.')[-1] == 'mmcif':
        parser = MMCIFParser()
    else:
        print 'Not a valid PDB format'
        sys.exit()
    structure = parser.get_structure('id', pdbfile)
    return structure

def check_na_atoms(str_object):
    ch_list = Selection.unfold_entities(str_object, 'C')
    rna_ch = []
    for ch in ch_list:
        res_list = Selection.unfold_entities(ch, 'R')
        for res in res_list:
            if res.get_resname() in [' DG',' DC',' DU',' DA']:
                rna_ch.append(ch.get_id())
                break
    return rna_ch
    
def check_protein_atoms(str_object):
    aa_res_lst = ['CYS','GLY','ALA','LEU','ILE',
                  'VAL','PRO','SER','THR','MET',
                  'ASP','HIS','GLU','ASN','GLN',
                  'LYS','ARG','TRP','PHE','TYR',]
    ch_list = Selection.unfold_entities(str_object, 'C')
    protein_ch = []
    for ch in ch_list:
        res_list = Selection.unfold_entities(ch, 'R')
        for res in res_list:
            if res.get_resname() in aa_res_lst:
                protein_ch.append(ch.get_id())
                break
    print protein_ch
    
class protein(object):
    "class protein gathers information about helix, sheet and loop in the protein for clustering"
    def __init__(self, file1, file2):
        '''
        :param: file1 is the pdbfile
        :param: file2 is the dssp file
        '''
        "Read the PDB helix and sheet records and identify loops \
        Store them in their appropriate lists"
        self.n_helix = 0
        self.n_sheet = 0
        self.n_loop = 0
        self.helix_list = []
        self.sheet_dict = {}
        self.loop_list = []
        sheets = {}
        helices = []
        protein.structure = str_object(file1)
        dsspfile = file2
        parse_dssp(dsspfile, sheets, helices)
        print helices, sheets
        #load the helix information into helix_list
        for i in helices:
            self.helix_list.append(i)
            self.n_helix += 1
        print self.helix_list
        #load the sheet information into sheet_dict
        for keys in sheets:
            #print sheets[keys]
            self.sheet_dict[self.n_sheet] = sheets[keys]
            self.n_sheet += 1

    def in_helix(self, chain_id, resid):
        flag = 0
        for i in range(self.n_helix):
            h_chain_id = self.helix_list[i][0]
            start_res = self.helix_list[i][1]
            end_res = self.helix_list[i][2]
            if (chain_id == h_chain_id) and (resid >= start_res and resid <= end_res):
                flag = 1
        return flag

    #Check whether a give residue id is in sheet list
    def in_sheet(self, chain_id, resid):
        flag = 0
        for i in range(self.n_sheet):
            strand_list = self.sheet_dict[i]
            #print strand_list
            for j in range(len(strand_list)):
                s_chain_id = strand_list[j][0]
                start_res = strand_list[j][1]
                end_res = strand_list[j][2]
                if (chain_id == s_chain_id) and (resid >= start_res and resid <= end_res):
                    flag = 1
        return flag

    def find_loop(self):
        #Construct loop residue list(loop_list)
        all_loop = []
        for model in protein.structure.get_list():
            for chain in model.get_list():
                for residue in chain.get_list():
                    resinfo = residue.get_full_id()
                    resinfo = list(resinfo)
                    chain_id = str(resinfo[2])
                    resid = int(resinfo[3][1])
                    if not (self.in_helix(chain_id, resid)) and not (
                        self.in_sheet(chain_id, resid)) and residue.get_resname() != "HOH":
                    #if residue.get_resname() != "HOH":
                        tlist = [chain_id, resid]
                        all_loop.append(tlist)

        if (len(all_loop) > 0):
            tlist = all_loop[0]
            start = tlist[1]
            end = tlist[1]
            for i in range(len(all_loop)):
                #print i
                if (i == len(all_loop) - 1):
                    self.n_loop += 1
                    self.loop_list.append([chainid, start, end])
                else:
                    tlist = all_loop[i + 1]
                    resid = tlist[1]
                    chainid = tlist[0]
                    if (resid - 1 == end):
                        end = resid
                    else:
                        self.n_loop += 1
                        tlist = all_loop[i]
                        chainid = tlist[0]
                        self.loop_list.append([chainid, start, end])
                        start = resid
                        end = resid

class helix(protein):
    "Class helix stores information about the individual helical segments"
    def __init__(self,chain_id,startres,endres):
        "Constructor: Initializes helix data structure and creates the C-beta dictionary with its own residue numbering"
        self.chain_id = chain_id
        self.nres = endres-startres+1
        self.startres = startres
        self.endres = endres
        self.is_in_clust = 10
        self.helix_cb = {}
        #print chain_id,startres,endres

         # read from the structure object of the protein class and create C-beta dictionary and also include the GLY C-alpha
        n = 1
        for i in range(startres,endres+1):
            #print i
            natm = 0
            sum = Vector([0, 0, 0])
            for model in protein.structure.get_list():
                for chain in model.get_list():
                    for residue in chain.get_list():
                        rname = residue.get_resname()
                        for atom in residue.get_list():
                            #check for the C-beta and GLY C-alpha atoms
                            atmid = atom.get_id()
                            """if (atom.get_id() == "CB") or (protein.seq_list[i-1] == "G" and atom.get_id() == "CA"):
                                atminfo = atom.get_full_id()
                                atminfo = list(atminfo)
                                tlist = residue.get_id()
                                #print atminfo,structure.get_residues()
                                #print atminfo[2], chain_id
                                if str(atminfo[2]) == chain_id and tlist[1] == i:
                                    #print tlist[1],atom.get_id()
                                    v = atom.get_vector()
                                    #self.helix_cb[tlist[1]] = v
                                    self.helix_cb[n] = v
                                    n += 1"""
                            #if (atmid != "N") and (atmid != "CA") and (atmid != "C") and (atmid != "O") or (protein.seq_list[i-1] == "G" and atmid == "CA"):
                            if (atmid != "N") and (atmid != "CA") and (atmid != "C") and (atmid != "O") or (rname == "GLY" and atmid == "CA"):
                                atminfo = atom.get_full_id()
                                atminfo = list(atminfo)
                                tlist = residue.get_id()
                                if str(atminfo[2]) == chain_id and tlist[1] == i:
                                    v = atom.get_vector()
                                    sum = sum + v
                                    natm = natm + 1
            v = sum/natm
            self.helix_cb[n] = v
            n += 1
    def get_nres(self):
        "Return number of helical residues"
        return self.nres
    def get_cb_coord(self,resid):
        "Return C-beta coordinate vector"
        cb_vector = helix_cb[resid]
        return cb_vector
    def get_startres(self):
        "Return the starting residue of the helix"
        return self.startres
    def get_endres(self):
        "Return the ending residue of the helix"
        return self.endres
    def get_cb(self):
        cb_vec_list = []
        for key, value in self.helix_cb.iteritems():
            if(self.nres > 3):
                cb_vec_list.append(value)
        return cb_vec_list
    def print_cb_dict(self):
        "Prints the helix cbeta dictionary"
        print 'Helix information:'
        #print "chain_id: ", self.chain_id, "start_res: ",self.startres, "end_res: ",self.endres
        for key, value in self.helix_cb.iteritems():
            print key, value
    def get_type(self):
        return 1
    def get_start_end(self):
        tlist = []
        tlist.append(self.startres)
        tlist.append(self.endres)
        return tlist
    def get_start_end_chain(self):
        tlist = []
        tlist.append(self.chain_id)
        tlist.append(self.startres)
        tlist.append(self.endres)
        return tlist


class strand(object):
    "Class strand stores information about the individual stand segments"
    def __init__(self,chain_id,startres,endres):
        "Constructor: Initializes strand data structure and creates the C-beta dictionary with its own residue numbering"
        self.chain_id = chain_id
        self.nres = endres-startres+1
        self.startres = startres
        self.endres = endres
        self.strand_cb = {}
        #print chain_id,startres,endres
        # read from the structure object and create C-beta dictionary and also include the GLY C-alpha
        n = 1
        for i in range(startres,endres+1):
            sum = Vector([0, 0, 0])
            natm = 0
            #print i
            for model in protein.structure.get_list():
                for chain in model.get_list():
                    for residue in chain.get_list():
                        rname = residue.get_resname()
                        for atom in residue.get_list():
                            #check for the C-beta and GLY C-alpha atoms
                            atmid = atom.get_id()
                            """if (atom.get_id() == "CB") or (protein.seq_list[i-1] == "G" and atom.get_id() == "CA"):
                                atminfo = atom.get_full_id()
                                atminfo = list(atminfo)
                                tlist = residue.get_id()
                                #print atminfo,structure.get_residues()
                                #print atminfo[2], chain_id
                                if str(atminfo[2]) == chain_id and tlist[1] == i:
                                    #print tlist[1],atom.get_id()
                                    v = atom.get_vector()
                                    self.strand_cb[n] = v
                                    n += 1"""
                            #if (atmid != "N") and (atmid != "CA") and (atmid != "C") and (atmid != "O") or (protein.seq_list[i-1] == "G" and atmid == "CA"):
                            if (atmid != "N") and (atmid != "CA") and (atmid != "C") and (atmid != "O") or (rname == "GLY" and atmid == "CA"):
                                atminfo = atom.get_full_id()
                                atminfo = list(atminfo)
                                tlist = residue.get_id()
                                if str(atminfo[2]) == chain_id and tlist[1] == i:
                                    v = atom.get_vector()
                                    sum = sum + v
                                    natm = natm + 1
            v = sum/natm
            self.strand_cb[n] = v
            n += 1
    def get_nres(self):
        "Return number of strand residues"
        return self.nres
    def get_cb_coord(self,resid):
        "Return cb coordinate vector"
        cb_vector = strand_cb[resid]
        return cb_vector
    def get_startres(self):
        "Return the starting residue of the strand"
        return self.startres
    def get_endres(self):
        "Return the ending residue of the strand"
        return self.endres
    def print_cb_dict(self):
        "Prints the strand cbeta dictionary"
        for key, value in self.strand_cb.iteritems():
            print key, value
    def get_start_end(self):
        tlist = []
        tlist.append(self.startres)
        tlist.append(self.endres)
        return tlist
    def get_start_end_chain(self):
        tlist = []
        tlist.append(self.chain_id)
        tlist.append(self.startres)
        tlist.append(self.endres)
        return tlist

class sheet(protein):
    "Creates a sheet object from strand objects using the strand_list"
    def __init__(self,strand_list):
        self.nstrand = len(strand_list)
        self.is_in_clust = 20
        self.strand_list = strand_list
        self.strand = []
        for i in range(self.nstrand):
            #create individual strand objects and assemblem them into a sheet
            chain_id = str(strand_list[i][0])
            start_resid = int(strand_list[i][1])
            end_resid = int(strand_list[i][2])
            #print chain_id,start_resid,end_resid
            self.strand.append(strand(chain_id,start_resid,end_resid))
    """def get_nres(self,strandid):
        tlist = self.start_end_list[strandid-1]
        return tlist[1]-tlist[0]
    def get_cb_coord(self,resid):
        #Return cb coordinate vector
        cb_vector = cbeta_dict[resid]
        return cb_vector
    def get_startres(self,strandid):
        tlist = self.start_end_list[strandid-1]
        return tlist[0]
    def get_endres(self,strandid):
        tlist = self.start_end_list[strandid-1]
        return tlist[1]"""
    def get_cb(self):
        cb_vec_list = []
        for i in range(self.nstrand):
            for key, value in self.strand[i].strand_cb.iteritems():
                if(self.strand[i].nres > 3):
                    cb_vec_list.append(value)
        return cb_vec_list
    def print_cb_dict(self):
        print "Strand information:"
        for i in range(self.nstrand):
            print "Strand ",i,"= ",self.strand_list[i]
            #self.strand[i].print_cb_dict()
    def get_type(self):
        return 2
    def get_start_end(self):
        start_end = []
        for i in range(self.nstrand):
            start_end.extend(self.strand[i].get_start_end())
        return start_end
    def get_start_end_chain(self):
        """d = {}
        for i in range(self.nstrand):
            self.chain_id = self.strand_list[i][0]
            d[self.strand[i].get_startres()] = self.chain_id
            d[self.strand[i].get_endres()] = self.chain_id
        return d """
        tlist = []
        for i in range(self.nstrand):
            self.chain_id = self.strand_list[i][0]
            tlist.extend(self.chain_id)
            tlist.extend(self.strand[i].get_start_end())
        return tlist



class loop(protein):
    "Class loop stores information about the individual loop segments"
    def __init__(self,chain_id,startres,endres):
        "Constructor: Initializes loop data structure and creates the C-beta dictionary with its own residue numbering"
        self.chain_id = chain_id
        self.nres = endres-startres+1
        self.startres = startres
        self.endres = endres
        self.is_in_clust = 0
        self.loop_cb = {}
        #print chain_id,startres,endres

         # read from the structure object of the protein class and create C-beta dictionary and also include the GLY C-alpha
        n = 1
        for i in range(startres,endres+1):
            sum = Vector([0, 0, 0])
            natm = 0
            #print i
            for model in protein.structure.get_list():
                for chain in model.get_list():
                    for residue in chain.get_list():
                        rname = residue.get_resname()
                        #print rname
                        for atom in residue.get_list():
                            #print atom
                            #check for the C-beta and GLY C-alpha atoms
                            atmid = atom.get_id()
                            """if (atom.get_id() == "CB") or (protein.seq_list[i-1] == "G" and atom.get_id() == "CA"):
                                atminfo = atom.get_full_id()
                                atminfo = list(atminfo)
                                tlist = residue.get_id()
                                #print atminfo,structure.get_residues()
                                #print atminfo[2], chain_id
                                if str(atminfo[2]) == chain_id and tlist[1] == i:
                                    #print tlist[1],atom.get_id()
                                    v = atom.get_vector()
                                    #self.helix_cb[tlist[1]] = v
                                    self.loop_cb[n] = v
                                    n += 1"""
                            #if (atmid != "N") and (atmid != "CA") and (atmid != "C") and (atmid != "O") or (protein.seq_list[i-1] == "G" and atmid == "CA"):
                            if (atmid != "N") and (atmid != "CA") and (atmid != "C") and (atmid != "O") or (rname == "GLY" and atmid == "CA"):
                                atminfo = atom.get_full_id()
                                atminfo = list(atminfo)
                                tlist = residue.get_id()
                                if str(atminfo[2]) == chain_id and tlist[1] == i:
                                    #print atminfo
                                    v = atom.get_vector()
                                    sum = sum + v
                                    natm = natm + 1
            v = sum/natm
            self.loop_cb[n] = v
            n += 1
    def get_nres(self):
        "Return number of helical residues"
        return self.nres
    def get_cb_coord(self,resid):
        "Return C-beta coordinate vector"
        cb_vector = loop_cb[resid]
        return cb_vector
    def get_startres(self):
        "Return the starting residue of the helix"
        return self.startres
    def get_endres(self):
        "Return the ending residue of the helix"
        return self.endres
    def print_cb_dict(self):
        "Prints the loop cbeta dictionary"
        for key, value in self.loop_cb.iteritems():
            print key, value
    def get_type(self):
        return 3
    def get_start_end(self):
        tlist = []
        tlist.append(self.startres)
        tlist.append(self.endres)
        return tlist
    def get_start_end_chain(self):
        tlist = []
        tlist.append(self.chain_id)
        tlist.append(self.startres)
        tlist.append(self.endres)
        return tlist



class rna(object):
    def __init__(self, file1, file2):
        self.n_helix = 0
        self.n_loop = 0
        self.strand_dict = {}
        self.loop_list = []
        self.helix_list = []
        helices = []
        rna.numberingMaps = {}


        rna.structure = str_object(pdbfile=file1)

        #create the 2D structure object from the RNAML file
        import rnamlparse

        rna.rnaml = rnamlparse.fileParse(file2)
        rnamlparse.mapPDBChains(rna.rnaml, rna.structure) #maps the RNAML numbering to the PDB numbering

        #map helices and loops to objects and store them
        for chain in self.rnaml.chains:
            rna.numberingMaps[chain.PDBChain] = chain.numberingMap
            for struct in chain.structList:
                if struct.isHelix == True:
                    if not struct.title in self.strand_dict:
                        self.strand_dict[struct.title] = []
                    self.strand_dict[struct.title].append(rna_strand(chain.PDBChain, struct.startPos, struct.endPos))
                else:
                    self.loop_list.append(rna_strand(chain.PDBChain,struct.startPos,struct.endPos))
        for helix_title in self.strand_dict:
            self.helix_list.append(rna_helix(self.strand_dict[helix_title]))

        self.n_helix = len(self.helix_list)
        self.n_loop = len(self.loop_list)

    def in_helix(self,chain_id,resid):
        flag = 0
        for key in self.strand_dict:
            for strand in self.strand_dict[key]:
                h_chain_id = strand.chain_id
                start_res = strand.startres
                end_res = strand.endres
                if (chain_id == h_chain_id) and (resid >= start_res and resid <=end_res):
                    flag = 1
                    return flag
        return flag

class rna_strand(object):
    "Class rna_strand stores information about individual strands of RNA, in either helices or loops"
    def __init__(self,chain_id,startres,endres):
        "Constructor: Initializes strand data structure and creates the centroid dictionary"
        self.chain_id = chain_id
        self.nres = endres-startres+1
        self.startres = startres
        self.endres = endres
        self.is_in_clust = 10
        self.strand_cb = {}

        #Create centroid dictionary
        def getCentroid(residue):
            sumAtoms = array((0.0,0.0,0.0))
            count = 0
            for atom in residue:
                #print atom.get_name
                if not (re.search(r"P", atom.get_name()) or re.search(r"'", atom.get_name())):
                    sumAtoms += atom.get_coord()
                    count += 1
            return sumAtoms/count
        for i in range(startres,endres+1):
            try:
                self.strand_cb[i] = getCentroid(rna.structure[0][chain_id][int(rna.numberingMaps[self.chain_id][i])])
            except:
                pass

    def get_nres(self):
        "Return number of residues"
        return self.nres
    def get_cb_coord(self,resid):
        "Return C-beta coordinate vector"
        cb_vector = helix_cb[resid]
        return cb_vector
    def get_startres(self):
        "Return the starting residue of the helix"
        return self.startres
    def get_endres(self):
        "Return the ending residue of the helix"
        return self.endres
    def get_cb(self):
        cb_vec_list = []
        for key, value in self.strand_cb.iteritems():
            if(self.nres > 3):
                cb_vec_list.append(value)
        return cb_vec_list
    def print_cb_dict(self):
        "Prints the helix cbeta dictionary"
        print 'Helix information:'
        #print "chain_id: ", self.chain_id, "start_res: ",self.startres, "end_res: ",self.endres
        for key, value in self.helix_cb.iteritems():
            print key, value
    def get_type(self):
        return 1
    def get_start_end(self):
        tlist = []
        tlist.append(self.startres)
        tlist.append(self.endres)
        tlist.append(self.chain_id)
        return tlist
    def get_start_end_PDB(self):
        tlist = []
        tlist.append(rna.numberingMaps[self.chain_id][self.startres])
        tlist.append(rna.numberingMaps[self.chain_id][self.endres])
        tlist.append(self.chain_id)
        #print tlist
        return tlist


class rna_helix(object):
    "Class rna_helix stores information about RNA double helices"
    def __init__(self,strand_list):
        self.nstrand = len(strand_list)
        self.is_in_clust = 20
        self.strand_list = strand_list
    def get_cb(self):
        cb_vec_list = []
        for i in range(self.nstrand):
            for key, value in self.strand_list[i].strand_cb.iteritems():
                if(self.strand_list[i].nres > 3):
                    cb_vec_list.append(value)
        return cb_vec_list
    def print_cb_dict(self):
        print "Strand information:"
        for i in range(self.nstrand):
            print "Strand ",i,"= ",self.strand_list[i].get_start_end_PDB() #HS
            #self.strand[i].print_cb_dict()
    def get_type(self):
        return 2
    def get_start_end(self):
        start_end = []
        for i in range(self.nstrand):
            start_end.append(self.strand_list[i].get_start_end())
        return start_end
    def get_start_end_PDB(self):
        start_end = []
        for i in range(self.nstrand):
            start_end.append(self.strand_list[i].get_start_end_PDB())
        return start_end


#Function to return a random key from a given dictionary
def get_rand_key(hs_dict):
    #print 'size: ',len(hs_dict)
    keys = hs_dict.keys()
    random.shuffle(keys)
    return keys[1]
#distance between two vectors
def dist(v1,v2):
    #print v1[0],v1[1],v1[2],v2[0],v2[1],v2[2]
    s1 = (v1[0]-v2[0])**2
    s2 = (v1[1]-v2[1])**2
    s3 = (v1[2]-v2[2])**2
#    print (s1+s2+s3) ** 0.5
    return (s1+s2+s3) ** 0.5
#def dist(v1,v2):
    #return linalg.norm(v1 - v2)
#Function returns 1 if sse1 and sse2 are close; otherwise 0
def prot_is_close(sse1,sse2,control,cutoff,per_similar):
    #cutoff = 6.5
    #if(control==1):
    #    per_similar = 40.0
    #else:
    #    per_similar = 70.0
    close = 0
    per1 = 0.0
    per2 = 0.0
    small = 0
    large = 0
    l1 = sse1.get_cb()
    l2 = sse2.get_cb()
    if(len(l1)==0 or len(l2)==0):
        return 0
    #if two SSEs are helix and one is relatively small than the other, then treat it as not close
    # this condition will avoid merging of two different cluster due to a small SSE in between them!
    rangelen1 = len(sse1.get_start_end())
    rangelen2 = len(sse2.get_start_end())
    relativelen = 0.0
    #make sure that its a helix pair!
    if(rangelen1 == 2 and rangelen2 == 2):
        len1 = sse1.get_endres()-sse1.get_startres()+1
        len2 = sse2.get_endres()-sse2.get_startres()+1
        if(len1 <= len2):
            small = float(len1)
            large = float(len2)
        else:
            small = float(len2)
            large = float(len1)
        relativelen = (small/large)*100.0
        #print 'rel len :',small,large,relativelen
        #print sse1.get_start_end()
        #print sse2.get_start_end()
        if (relativelen <= 40.0):
            return 0
#    print "sse1 :"
#    print l1
#    print "sse2 :"
#    print l2
    #%sse1 cb close to sse2 cb
    c1 = 0.0
    for i in l1:
        for j in l2:
            if(dist(i,j)<=cutoff):
                c1 = c1 + 1.0
                break
    #%sse1 cb close to sse2 cb
    c2 = 0.0
    for i in l2:
        for j in l1:
            if(dist(i,j)<=cutoff):
                c2 = c2 + 1.0
                break
    per1 = c1/len(l1)*100
    per2 = c2/len(l2)*100
#    print c1,c2,per1,per2,len(l1),len(l2)
    if(per1 >= per_similar or per2 >= per_similar):
        close = 1

    return close
    #cutoff = 6.5
    #if(control==1):
    #    per_similar = 40.0
    #else:
    #    per_similar = 70.0
    close = 0
    per1 = 0.0
    per2 = 0.0
    small = 0
    large = 0
    l1 = sse1.get_cb()
    l2 = sse2.get_cb()
    if(len(l1)==0 or len(l2)==0):
        return 0
    #if two SSEs are helix and one is relatively small than the other, then treat it as not close
    # this condition will avoid merging of two different cluster due to a small SSE in between them!


def rna_is_close(sse1,sse2,control,cutoff,per_similar):
    #cutoff = 6.5
    #if(control==1):
    #    per_similar = 40.0
    #else:
    #    per_similar = 70.0
    close = 0
    per1 = 0.0
    per2 = 0.0
    small = 0
    large = 0
    l1 = sse1.get_cb()
    l2 = sse2.get_cb()
    if(len(l1)==0 or len(l2)==0):
        return 0
    c1 = 0.0
    for i in l1:
        for j in l2:
            if(dist(i,j)<=cutoff):
                c1 = c1 + 1.0
                break
    #%sse1 cb close to sse2 cb
    c2 = 0.0
    for i in l2:
        for j in l1:
            if(dist(i,j)<=cutoff):
                c2 = c2 + 1.0
                break
    per1 = c1/len(l1)*100
    per2 = c2/len(l2)*100
#    print c1,c2,per1,per2,len(l1),len(l2)
    if(per1 >= per_similar or per2 >= per_similar):
        close = 1

    return close

def isin(item,den_cluster):
    yes = False
    for i_list in den_cluster:
        for j_list in i_list:
            if(item == j_list):
                yes = True
    return yes
def parse_dssp(file,sheets,helices):
    #print file
    infile = open(file,'rU')
    #extract sheets information from dssp output file
    n = 0
    new = 0
    start = 0
    end = 0
    cid = ''
    #sheets = {}
    #helices = []
    tlist = []
    lstlbl = '99'
    check_dssp_loop = 0
    for line in infile:
        #print "sheet info:",sheets
        #temp = line.split(" ")
        #print line
        temp = line.split()
        if(temp[0] == "" or temp[0] =="."):
            continue
        if(temp[0]=="#" and check_dssp_loop == 0):
            check_dssp_loop = 1
            continue
        #if(n>28):
        if(temp[1]!="!*" and temp[1]!="!" and (check_dssp_loop==1 and temp[3]!="X") and temp[0]!="#" and check_dssp_loop==1):
        #if(temp[1]!="!" and temp[0]!="#" and check_dssp_loop==1):
            resnum = int(line[6:10])
            chain = str(line[11:12])
            stype = str(line[16:17])
            betalbl = str(line[33:34])
            if(stype=='E' and betalbl != ' '):
            #if(stype=='E' and not(betalbl in sheets)):
                if(new==0):
                    new = 1
                    start = resnum
                    cid = chain
                    lstlbl = betalbl
            else:
                if(stype!='E' and new==1):
                    new = 0
                    end = resnum - 1
                    if(lstlbl in sheets):
                        tlist = sheets[lstlbl]
                        tlist.append([cid,start,end])
                        new_entry = {lstlbl:tlist}
                        sheets.update(new_entry)
                        #break
                    else:
                        tlist = []
                        tlist.append([cid,start,end])
                        sheets[lstlbl] = tlist
                        #print 'added a new entry'
            if(stype=='E' and lstlbl!= betalbl and betalbl != ' ' and new == 1):
                #print 'inside this'
                #new = 0
                end = resnum - 1
                if(lstlbl in sheets):
                    tlist = sheets[lstlbl]
                    tlist.append([cid,start,end])
                    new_entry = {lstlbl:tlist}
                    sheets.update(new_entry)
                    #break
                else:
                    tlist = []
                    tlist.append([cid,start,end])
                    sheets[lstlbl] = tlist
                start = resnum
                cid = chain
                lstlbl = betalbl

    infile.close()
    #print 'SHEET INFORMATION'
    #for keys in sheets:
    #    print keys,sheets[keys]

    #extract helix information from dssp output file
    new = 0
    n = 0
    infile = open(file,'rU')
    check_dssp_loop = 0
    for line in infile:
        n =  n + 1
        temp = line.split()
        if(temp[0] == "" or temp[0] =="."):
            continue
        if(temp[0]=="#" and check_dssp_loop == 0):
            check_dssp_loop = 1
            continue

        if(temp[1]!="!*" and temp[1]!="!" and (check_dssp_loop==1 and temp[3]!="X") and temp[0]!="#" and check_dssp_loop==1):
        #if(temp[1]!="!" and temp[0]!="#" and check_dssp_loop==1):
            resnum = int(line[6:10])
            #print resnum
            chain = str(line[11:12])
            stype = str(line[16:17])
            betalbl = str(line[33:34])
            #print resnum, chain, stype, betalbl
            if(stype=='H' and new==0):
                new = 1
                start = resnum
                cid = chain
                lstlbl = betalbl
            else:
                if(stype!='H' and new==1):
                    new = 0
                    end = resnum -1
                    helices.append([cid,start,end])
                    #print 'added a new entry'
    infile.close()
    #print 'HELIX INFORMATION'
    #for i in helices:
    #    print i

#function to create chim.py files

def chim_file(filebase, model):
    
    for n in xrange(0,10,101):
        clustfile = filebase+"_denclust_"+str(n)+".txt"
        input = clustfile
        in_file = open(input, 'r')
        chimscript = filebase+'.chimscript_'+str(n)+'.py'
        chimfile = open(chimscript, 'w')
        chimfile.write('from chimera import runCommand\n')
        chimfile.write('runCommand("open '+model+'")\n')
        chimfile.write('runCommand("color white #0:*")\n') #colour everything white
        #chimfile.write('runCommand("color '+colname+' #0:'+tstr+'")\n')
        for line in in_file:
            tokens = line.split(' ')
            if(str(tokens[0].strip()) == "#RIBFIND_clusters:"):
                continue
            if(str(tokens[0].strip()) == "#individual_rigid_bodies"):
                break
            #print line
            r = random.random()
            g = random.random()
            b = random.random()
            colname = str(r)+','+str(g)+','+str(b)
            k = 0
            for i in range(len(tokens)/2):
                k = k + 1
                start = str(tokens[i*2])
                end = str(tokens[i*2+1])
                start_split = start.split(':')
                startres = start_split[0]
                chain_id = start_split[1]
                end_split = end.split(':')
                endres = end_split[0]
                #print startres, endres
                chimfile.write('runCommand("color '+colname+' #0:'+startres+'-'+endres+'.'+chain_id+'")\n')
        in_file.close()
        chimfile.close()
        
def compare(filebase):
    number = 0
    current_number = 1
    while current_number <= 100:
        clustfile1 = filebase+"_denclust_"+str(number)+".txt"
        clustfile2 = filebase+"_denclust_"+str(current_number)+".txt"
        chimfile1 = filebase+'.chimscript_'+str(number)+'.py'
        chimfile2 = filebase+'.chimscript_'+str(current_number)+'.py'
        f1 = open(clustfile1)
        f2 = open(clustfile2)
        lines1 = f1.readlines() # list of lines as strings
        lines2 = f2.readlines()
        minus1 = lines1[1:] #remove first line
        minus2 = lines2[1:]
        if len(minus1) != len(minus2):
            #print str(current_number), 'not the same'
            number = current_number
            current_number += 1
        else:
            for i in range(0,len(minus1)):
                line1 = minus1[i].split()
                line2 = minus2[i].split()
                set1 = set(line1)
                set2 = set(line2)
            if set1 == set2:
                #print str(current_number)
                os.remove(clustfile2)
                os.remove(chimfile2)
                current_number += 1
            elif set1 != set2:
                #print str(current_number), 'NOT THE SAME'
                number = current_number
                current_number += 1
    f2.close()
    f1.close()

def main_protein(prot_structure,protsec,prot_cutoff):
    start = time.time()
    obj = protein(prot_structure,protsec)
    file1_base = os.path.splitext(os.path.basename(sys.argv[1]))[0]
    outPath = './'+file1_base+'/protein'
    if not os.path.exists(outPath):
        os.makedirs(outPath)
    file1_base = outPath+'/'+file1_base
    os.system("cp '"+ sys.argv[1] + "' '" + outPath +"/.'")
    cutoff = prot_cutoff
    obj.find_loop()
    hx = []
    sh = []
    lp = []
    initial_reps = {}
    #Declare a dictionary containing pointers to helix and sheet objects
    main_hs_dict = {}
    hs_dict = {}

    #summary = file1_base+'.summary'
    #outfile2 = open(summary, 'w')

    #collect all helix objects
    print "===========RIBFIND: a program to find rigid bodies in protein structures============"
    print "Reference: Pandurangan, A.P., Topf, M., 2012. J. Struct. Biol. 177, 520-531."
    print "===================================================================================="
    print "Input PDB file : ",sys.argv[1]
    print "Input DSSP file : ",sys.argv[2]
    print "Contact distance : ",sys.argv[3]
    print "Summary of secondary structural elements"
    print "----------------------------------------"
    print "Helix initilization: "
    print "Number of helices : ",str(obj.n_helix)
    for i in range(obj.n_helix):
        chain_id = str(obj.helix_list[i][0])
        start_resid = int(obj.helix_list[i][1])
        end_resid = int(obj.helix_list[i][2])
        print "Helix ",i+1,":"," [",chain_id,",",start_resid,",",end_resid,"]"
        #outfile2.write("Helix ",i+1,":"," [",chain_id,",",start_resid,",",end_resid,"]\n")
        hx.append(helix(chain_id,start_resid,end_resid))
    print "----------------------------------------"
    print "Sheet initilization: "
    print "Number of sheets  ",str(obj.n_sheet)
    for i in range(obj.n_sheet):
        sh.append(sheet(obj.sheet_dict[i]))
    for i in range(obj.n_sheet):
        print "Sheet : ",i+1
        sh[i].print_cb_dict()

    #print "done Sheet initilization"
    #collect all loop objects
    print "----------------------------------------"
    #outfile2.write("----------------------------------------\n")
    print "Loop initilization: "
    #outfile2.write("----------------------------------------\n")
    for i in range(obj.n_loop):
        chain_id = str(obj.loop_list[i][0])
        start_resid = int(obj.loop_list[i][1])
        end_resid = int(obj.loop_list[i][2])
        print "Loop ",i+1,":"," [",chain_id,",",start_resid,",",end_resid,"]"
        #outfile2.write("Loop ",i+1,":"," [",chain_id,",",start_resid,",",end_resid,"],\n")
        #print start_resid, end_resid
        lp.append(loop(chain_id,start_resid,end_resid))
    #print "done loop initilization"
    """for i in range(obj.n_helix):
        print "Helix : ",i+1
        hx[i].print_cb_dict()

    for i in range(obj.n_sheet):
        print "Sheet : ",i+1
        sh[i].print_cb_dict()

    for i in range(obj.n_loop):
        print "loop : ",i+1
        lp[i].print_cb_dict()"""

    #print 'number of loops : ',obj.n_loop
    #print 'number of helix : ',obj.n_helix
    #print 'number of sheet : ',obj.n_sheet
    #sys.exit(1)
    #close = is_close(hx[3],hx[1])
    #print "Is close = ",close

    #hx[0].print_cb_dict()
    #sh[0].print_cb_dict()
    #lp[0].print_cb_dict()

    #exit(0)
    #clustering routine 2
    #clustfile = file1_base+'.denclust'
    #outfile = open(clustfile,'w')
    noclustfile = file1_base+'.noclust'
    outfile1 = open(noclustfile,'w')
    summary = file1_base+'.summary'
    outfile2 = open(summary, 'w')

    print "---------------------------------------------------------------------------------------------------"
    print "Summary of RIBFIND clusters for the range of cutoffs"
    print "Cutoff    No. of clusters    No. of SSEs in cluster    Total no. of SSEs    Weighted no. of cluster"
    outfile2.write("Summary of RIBFIND clusters for the range of cutoffs\n")
    outfile2.write("Cutoff    No. of clusters    No. of SSEs in cluster    Total no. of SSEs    Weighted no. of cluster\n")

    for per_similar in range(0,101,10):
        #write the *denclust file for each cutoff value (1 to 100)
        clustfile = file1_base+"_denclust_"+str(per_similar)+".txt"
        outfile = open(clustfile,'w')
        per_similar = per_similar*1.0
        #print per_similar
        den_cutoff = 2
        #Initialize a dictionary containing pointers to helix and sheet objects
        indx = 1
        for i in range(0,obj.n_helix):
            tlist = ['H',i]
            hs_dict[indx] = hx[i] #tlist """neesh filling up hs_dict and main_hs_dict with H/S and number???"""
            main_hs_dict[indx] = hx[i]
            indx = indx + 1
        for i in range(0,obj.n_sheet):
            tlist = ['S',i]
            hs_dict[indx] = sh[i] #tlist
            main_hs_dict[indx] = sh[i]
            indx = indx + 1
        #print main_hs_dict

        #h1 = hs_dict[1]
        #h1.print_cb_dict()


        #start of clustering loop
        #cutoff = 5.0
        #per_similar = 36.0
        den_cluster = []
        # calculate the neighbor density for each secondary structural elements
        indx = 1
        for i in hs_dict.keys():
            tlist = []
            tlist.append(i)
            #print i
            sse1 = hs_dict[i]
            nd = 0
            for j in hs_dict.keys():
                sse2 = hs_dict[j]
                if(i!=j):
                    if(prot_is_close(sse1,sse2,1,cutoff,per_similar)):
                        #print 'getting close with: ',j
                        tlist.append(j)
                        nd = nd + 1
            if(nd >= den_cutoff):
                initial_reps[i] = tlist
            else:
                initial_reps[i] = []

        """for i in initial_reps.keys():
            print i
            h1 = hs_dict[i]
            h1.print_cb_dict()
            print initial_reps[i]
            print '**********'"""

        #print 'Iterative clustering',per_similar
        # for each such initial_reps iteratively collect all closer elements and make a cluster
        for i in initial_reps.keys():
            #print 'representative :',i
            #tclust_list contains list of sse's close to initial_reps[i], including i
            tclust_list = initial_reps[i]
            #Iteratively find all the sse in the main pool close to the cluster members (tclust_list)
            itr_list = []
            for j in tclust_list:
                itr_list.append(j)

            if(len(itr_list) > 0 and (not(isin(itr_list[0],den_cluster)))):
                while(1):
                    tclose = []
                    for k in itr_list:
                        #print itr_list
                        for l in hs_dict.keys():
                            if(not(l in tclust_list)):
                                #print k,l
                                if(prot_is_close(hs_dict[k],hs_dict[l],1,cutoff,per_similar)):
                                    tclose.append(l)
                                    tclust_list.append(l)
                    #print tclose
                    itr_list = tclose

                    if(len(itr_list) <= 0 and len(tclust_list) > 1):
                        den_cluster.append(tclust_list)
                        #print tclust_list
                        #remove all the records corresponding to tclust_list from the hs_dict
                        for n in tclust_list:
                            del hs_dict[n]
                            #clear tclust_list
                        tclust_list = []
                        #print 'encounter break'
                        break
                    #else:
                        #remove the tclose members from the main pool
                    #    for i in tclose:
                    #        del hs_dict[i]

        nummem = 0
        for i in den_cluster:
            nummem = nummem + len(i)
        #print per_similar,len(den_cluster),nummem
        w_clust = 0.0
        total_sse = obj.n_helix+obj.n_sheet
        w_clust = float(len(den_cluster))+(float(nummem)/float(total_sse))
        #print "Results of density based clustering:"
        #print str(per_similar)+' '+str(len(den_cluster))+' '+str(nummem)+' '+str(total_sse)+' '+str(round(w_clust,2))+'\n'
        print "%-10s%-19s%-26s%-21s%-16s\n"%(str(per_similar),str(len(den_cluster)),str(nummem),str(total_sse),str(round(w_clust,2)))
        outfile2.write("%-10s%-19s%-26s%-21s%-16s\n"%(str(per_similar),str(len(den_cluster)),str(nummem),str(total_sse),str(round(w_clust,2))))
        outfile.write('#RIBFIND_clusters: '+str(cutoff)+' '+str(per_similar)+' '+str(len(den_cluster))+' '+str(nummem)+' '+str(total_sse)+' '+str(round(w_clust,2))+'\n')
        #if(len(den_cluster)==0):
        #    outfile.write("NONE"+'\n')
        #write the clustering information to the denclust file
        for i in den_cluster:
            #r = random.random()
            #g = random.random()
            #b = random.random()
            #colname = str(r)+','+str(g)+','+str(b)
            #print 'Cluster member:'
            #print i
            tlist = i
            for j in tlist:
                #print j
                sse = main_hs_dict[j]
                #print sse
                temp = sse.get_start_end()
                temp2 = sse.get_start_end_chain()
                modlen = (len(temp2)/3) + 1
                for k in range(1, modlen):
                    #print(str(temp2[3*(k-1)+1])+':'+str(temp2[3*(k-1)])+' '+str(temp2[3*(k-1)+2])+':'+str(temp2[3*(k-1)]+' '))
                    chainID = str(temp2[3*(k-1)])
                    startres = str(temp2[3*(k-1)+1])
                    endres = str(temp2[3*(k-1)+2])
                    outfile.write(startres+':'+chainID+' '+endres+':'+chainID+' ')
                n = 0
                tstr = ''
                #for k in temp:
                    #outfile.write(str(k)+' ')

            #Now append the loops that are connected within the cluster members
            for l in range(obj.n_loop):
                temp1 = lp[l].get_start_end_chain()
                loop_chain_id = temp1[0]
                loop_start = temp1[1]
                loop_end = temp1[2]
                loop_start_in_cluster = 0
                loop_end_in_cluster = 0
                #print "loop ",loop_start,loop_end
                for m in tlist:
                    sse = main_hs_dict[m]
                    temp2 = sse.get_start_end()
                    temp3 = sse.get_start_end_chain()
                    sse_chainID = temp3[0]
                    #temp3 = sse.get_start_end_chain()
                    #print temp2
                    #print 'mem list',temp2,len(temp2)
                    n = 1
                    while(n<=len(temp2)/2):
                        sse_start = temp2[n*2-2]
                        sse_end = temp2[n*2-1]
                        #print "sse ",sse_start,sse_end
                        n = n + 1
                        #check if chain IDs match for loop and SSE
                        if loop_chain_id == sse_chainID:
                            if(loop_start == sse_end+1):
                                loop_start_in_cluster = 1
                                #print "found loop start in cluster ",i
                            if(loop_end == sse_start-1):
                                loop_end_in_cluster = 1
                                #print "found loop end in cluster ",i
                if(loop_start_in_cluster==1 and loop_end_in_cluster==1):
                    #print lp[l].get_start_end_chain(),'in cluster ',i
                    outfile.write(str(loop_start)+':'+str(loop_chain_id)+' '+str(loop_end)+':'+str(loop_chain_id)+' ')
                    #color the loops falling into the respective clusters
                    tstr = str(loop_start)+'-'+str(loop_end)
                    #outfile1.write('runCommand("color '+colname+' #0:'+tstr+'")\n')
            outfile.write('\n')

        cmem = []
        all = []
        noclust = []

        #collect the list of all SSE indexes
        for i in range(1,obj.n_helix+obj.n_sheet+1):
            all.append(i)
        #collect the list of all cluster's SSE
        for j in den_cluster:
            tlist = j
            for k in tlist:
                cmem.append(k)
        #deduce the list of indexes which are not in the cluster
        for i in all:
            found = 0
            for j in cmem:
                #print i,j
                if(i == j):
                    found = 1
            if(found == 0):
                noclust.append(i)
        #print 'non cluster members:'
        #print noclust
        #write the SSE with their clusters information (input file to flex-em)
        outfile.write("#individual_rigid_bodies "+str(len(noclust))+"\n")
        for j in noclust:
            #print j
            sse = main_hs_dict[j]
            temp2 = sse.get_start_end_chain()
            modlen = (len(temp2)/3) + 1
            for k in range(1, modlen):
                #print(str(temp2[3*(k-1)+1])+':'+str(temp2[3*(k-1)])+' '+str(temp2[3*(k-1)+2])+':'+str(temp2[3*(k-1)]+' '))
                chainID = str(temp2[3*(k-1)])
                startres = str(temp2[3*(k-1)+1])
                endres = str(temp2[3*(k-1)+2])
                outfile.write(startres+':'+chainID+' '+endres+':'+chainID+' ')
                """temp = sse.get_start_end()
            #print temp
            for k in temp:
                outfile.write(str(k)+' ')"""
            outfile.write('\n')
        #outfile.close()

        #write the individual SSE without their clusters (input file to flex-em)
        if (per_similar <=1):
            outfile1.write("#individual_rigid_bodies "+str(len(all))+"\n")
            for j in all:
                #print j
                sse = main_hs_dict[j]
                temp = sse.get_start_end_chain()
                modlen = (len(temp)/3) + 1
                for k in range(1, modlen):
                    #print(str(temp2[3*(k-1)+1])+':'+str(temp2[3*(k-1)])+' '+str(temp2[3*(k-1)+2])+':'+str(temp2[3*(k-1)]+' '))
                    chainID = str(temp[3*(k-1)])
                    startres = str(temp[3*(k-1)+1])
                    endres = str(temp[3*(k-1)+2])
                    outfile1.write(startres+':'+chainID+' '+endres+':'+chainID+' ')
                    #print(startres+':'+chainID+' '+endres+':'+chainID+' ')
                outfile1.write('\n')
        outfile.close()
        
    outfile1.close()
    outfile2.close()
    chim_file(file1_base, sys.argv[1])
    #compare(file1_base)
    end = time.time()
    print 'Total cpu time : %f minutes' %((end-start)/60.0)



def main_rna(structure_file,rnasec,rna_cutoff):
    #Main program
    #create protein object and gather info about helix,sheet and loop
    #from the pdb file
    #obj = protein("/Users/arun/Dropbox/flexrigi/testcases/4AKE-1AKE/4AKE_A.pdb")
    start = time.time()

    obj = rna(structure_file,rnasec)
    #obj = protein("model.pdb","sse.dssp")
    #print "done protien initialization"
    #file1_base = os.path.splitext("model.pdb")[0]
    file1_base = os.path.splitext(os.path.basename(sys.argv[1]))[0]
    outPath = './'+file1_base+'/rna'
    if not os.path.exists(outPath):
        os.makedirs(outPath)
    file1_base = outPath+'/'+file1_base
    os.system("cp '"+ sys.argv[1] + "' '" + outPath +"/.'")
    cutoff = rna_cutoff
    #print "done loops initilization"
    hx = []
    lp = []
    initial_reps = {}
    #Declare a dictionary containing pointers to helix and sheet objects
    main_hs_dict = {}
    hs_dict = {}


    #collect all helix objects
    print "===========RIBFIND: a program to find rigid bodies in protein structures============"
    print "Reference: Pandurangan, A.P., Topf, M., 2012. J. Struct. Biol. 177, 520-531."
    print "===================================================================================="
    print "Input PDB file : ",sys.argv[1]

    print "Input RNAML file : ",sys.argv[2]
    print "Contact distance : ",sys.argv[3]
    print "Summary of secondary structural elements"
    print "----------------------------------------"
    print "Helix initilization: "
    print "Number of helices  ",str(obj.n_helix)
    for i in range(obj.n_helix):
        #print "sheet : ",i+1
        hx.append(obj.helix_list[i])

    for i in range(obj.n_helix):
            print "Helix : ",i+1
            hx[i].print_cb_dict()

    #print "done Sheet initilization"
    #collect all loop objects
    print "----------------------------------------"
    print "Loop initilization: "
    #(rna.numberingMaps[self.chain_id][self.startres]) #hs

    for i in range(obj.n_loop):
        loop = obj.loop_list[i]
        temp = loop.get_start_end_PDB()
        #print temp
        print "Loop ",i+1,":", temp
        #print start_resid, end_resid
        lp.append(obj.loop_list[i])
    #print lp
    noclustfile = file1_base+'.noclust'
    outfile1 = open(noclustfile,'w')
    summary = file1_base+'.summary'
    outfile2 = open(summary, 'w')

    print "---------------------------------------------------------------------------------------------------"
    print "Summary of RIBFIND clusters for the range of cutoffs"
    print "Cutoff    No. of clusters    No. of SSEs in cluster    Total no. of SSEs    Weighted no. of cluster"
    outfile2.write("Summary of RIBFIND clusters for the range of cutoffs\n")
    outfile2.write("Cutoff    No. of clusters    No. of SSEs in cluster    Total no. of SSEs    Weighted no. of cluster\n")

    for per_similar in range(1,101):
        #write the *denclust file for each cutoff value (1 to 100)
        clustfile = file1_base+"_denclust_"+str(per_similar)+".txt"
        outfile = open(clustfile,'w')
        per_similar = per_similar*1.0
        #print per_similar
        den_cutoff = 2
        #Initialize a dictionary containing pointers to helix objects
        indx = 1
        for i in range(0,obj.n_helix):
            tlist = ['H',i]
            hs_dict[indx] = hx[i] #tlist
            main_hs_dict[indx] = hx[i]
            indx = indx + 1
        if considerLoops == True:
            for i in range(0,obj.n_loop):
                tlist = ['L',i]
                hs_dict[indx] = lp[i] #tlist
                main_hs_dict[indx] = lp[i]
                indx = indx + 1
        #print main_hs_dict.get_start_end_PDB()
        #start of clustering loop
        #cutoff = 5.0
        #per_similar = 36.0
        den_cluster = []
        # calculate the neighbor density for each secondary structural elements
        indx = 1
        for i in hs_dict.keys():
            tlist = []
            tlist.append(i)
            #print i
            sse1 = hs_dict[i]
            nd = 0
            for j in hs_dict.keys():
                sse2 = hs_dict[j]
                if(i!=j):
                    if(rna_is_close(sse1,sse2,1,cutoff,per_similar)):
                        #print 'getting close with: ',j
                        tlist.append(j)
                        nd = nd + 1
            if(nd >= den_cutoff):
                initial_reps[i] = tlist
            else:
                initial_reps[i] = []

        """for i in initial_reps.keys():
            print i
            h1 = hs_dict[i]
            h1.print_cb_dict()
            print initial_reps[i]
            print '**********'"""

        #print 'Iterative clustering',per_similar
        # for each such initial_reps iteratively collect all closer elements and make a cluster
        for i in initial_reps.keys():
            #print 'representative :',i
            #tclust_list contains list of sse's close to initial_reps[i], including i
            tclust_list = initial_reps[i]
            #Iteratively find all the sse in the main pool close to the cluster members (tclust_list)
            itr_list = []
            for j in tclust_list:
                itr_list.append(j)

            if(len(itr_list) > 0 and (not(isin(itr_list[0],den_cluster)))):
                while(1):
                    tclose = []
                    for k in itr_list:
                        #print itr_list
                        for l in hs_dict.keys():
                            if(not(l in tclust_list)):
                                #print k,l
                                if(rna_is_close(hs_dict[k],hs_dict[l],1,cutoff,per_similar)):
                                    tclose.append(l)
                                    tclust_list.append(l)
                    #print tclose
                    itr_list = tclose

                    if(len(itr_list) <= 0 and len(tclust_list) > 1):
                        den_cluster.append(tclust_list)
                        #print tclust_list
                        #remove all the records corresponding to tclust_list from the hs_dict
                        for n in tclust_list:
                            del hs_dict[n]
                            #clear tclust_list
                        tclust_list = []
                        #print 'encounter break'
                        break
                    #else:
                        #remove the tclose members from the main pool
                    #    for i in tclose:
                    #        del hs_dict[i]

        nummem = 0
        for i in den_cluster:
            nummem = nummem + len(i)
        #print per_similar,len(den_cluster),nummem
        w_clust = 0.0
        total_sse = obj.n_helix
        if considerLoops == True:
            total_sse += obj.n_loop
        w_clust = float(len(den_cluster))+(float(nummem)/float(total_sse))
        #print "Results of density based clustering:"
        #print str(per_similar)+' '+str(len(den_cluster))+' '+str(nummem)+' '+str(total_sse)+' '+str(round(w_clust,2))+'\n'
        print "%-10s%-19s%-26s%-21s%-16s\n"%(str(per_similar),str(len(den_cluster)),str(nummem),str(total_sse),str(round(w_clust,2)))
        outfile2.write("%-10s%-19s%-26s%-21s%-16s\n"%(str(per_similar),str(len(den_cluster)),str(nummem),str(total_sse),str(round(w_clust,2))))
        outfile.write('#RIBFIND_clusters: '+str(cutoff)+' '+str(per_similar)+' '+str(len(den_cluster))+' '+str(nummem)+' '+str(total_sse)+' '+str(round(w_clust,2))+'\n')
        #if(len(den_cluster)==0):
        #    outfile.write("NONE"+'\n')
        #write the clustering information to the denclust file
        for i in den_cluster:
            #r = random.random()
            #g = random.random()
            #b = random.random()
            #colname = str(r)+','+str(g)+','+str(b)
            #print 'Cluster member:'
            tlist = i
            for j in tlist:
                #print j
                sse = main_hs_dict[j]
                temp = sse.get_start_end_PDB()
                #print temp
                n = 0
                tstr = ''
                if len(temp) == 2:
                    for strand in temp: #writes the helices in clustfile
                        outfile.write(str(strand[0])+':'+str(strand[2])+' '+str(strand[1])+':'+str(strand[2])+' ')
                        #print(str(strand[0])+':'+str(strand[2])+' '+str(strand[1])+':'+str(strand[2])+' ')
                else: #writes the loops in clustfile
                    outfile.write(str(temp[0])+':'+str(temp[2])+' '+str(temp[1])+':'+str(strand[2])+' ')
                    #print(str(temp[0])+':'+str(temp[2])+' '+str(temp[1])+':'+str(strand[2])+' ')

            #Now append the loops that are connected within the cluster members
            if considerLoops == False:
                for l in range(obj.n_loop):
                    temp1 = lp[l].get_start_end()
                    loop_start = temp1[0]
                    loop_end = temp1[1]
                    loop_chain = temp1[2]
                    loop_start_in_cluster = 0
                    loop_end_in_cluster = 0
                    #print "loop ",loop_start,loop_end
                    for m in tlist:
                        sse = main_hs_dict[m]
                        temp2 = sse.get_start_end()
                        #print 'mem list',temp2,len(temp2)
                        n = 1
                        for strand in temp2:
                            if strand[2] == loop_chain:
                                #print "sse ",sse_start,sse_end
                                n = n + 1
                                if(loop_start == strand[1]+1):
                                    loop_start_in_cluster = 1
                                    #print "found loop start in cluster ",i
                                if(loop_end == strand[0]-1):
                                    loop_end_in_cluster = 1
                                    #print "found loop end in cluster ",i
                    if(loop_start_in_cluster==1 and loop_end_in_cluster==1):
                        #print lp[l].get_start_end(),'in cluster ',i
                        #outfile.write(str(temp1[2])+':'+rna.numberingMaps[temp1[2]][temp1[0]]+'-'+rna.numberingMaps[temp1[2]][temp1[1]]+'\n')
                        outfile.write(rna.numberingMaps[temp1[2]][temp1[0]]+':'+str(temp1[2])+' '+rna.numberingMaps[temp1[2]][temp1[1]]+':'+str(temp1[2])+' ')
                        #color the loops falling into the respective clusters
                        tstr = str(loop_start)+'-'+str(loop_end)
                        #outfile1.write('runCommand("color '+colname+' #0:'+tstr+'")\n')
            outfile.write('\n')


        cmem = []
        all = []
        noclust = []

        #collect the list of all SSE indexes
        for i in range(1,obj.n_helix+obj.n_loop+1):
            all.append(i)
            #print all
        #for i in range(1,obj.n_loop+1):
            #all.append(i)
        #print all
        #n = 1
        #for j in all:
            #sse = main_hs_dict[j]
            #temp = sse.get_start_end_PDB()
        #    print n, temp
            #n += 1
        #collect the list of all cluster's SSE
        for j in den_cluster:
            tlist = j
            for k in tlist:
                cmem.append(k)
        #deduce the list of indexes which are not in the cluster
        for i in all:
            found = 0
            for j in cmem:
                #print i,j
                if(i == j):
                    found = 1
            if(found == 0):
                noclust.append(i)
        #print noclust
        #print 'non cluster members:'
        #write the SSE with their clusters information (input file to flex-em)
        outfile.write("#individual_rigid_bodies "+str(len(noclust))+"\n")
        #n = 1
        for j in noclust:
            #print j
            sse = main_hs_dict[j]
            #print n, sse
            temp = sse.get_start_end_PDB()
            #print n, temp
            #n += 1
            if len(temp) == 2:
                for strand in temp: #writes helcies into clustfiles
                        #outfile.write(str(strand[2])+':'+str(strand[0])+'-'+str(strand[1])+'\n')
                        outfile.write(str(strand[0])+':'+str(strand[2])+' '+str(strand[1])+':'+str(strand[2])+' ')
                        outfile.write('\n')
            else: #writes loops into clustfiles
                outfile.write(str(temp[0])+':'+str(temp[2])+' '+str(temp[1])+':'+str(strand[2])+' ')
                outfile.write('\n')
                #print(str(temp[0])+':'+str(temp[2])+' '+str(temp[1])+':'+str(strand[2])+' ')
            #outfile.write('\n')
        #outfile.close()

        #write the individual SSE without their clusters (input file to flex-em)
        if (per_similar <=1):
            outfile1.write("#individual_rigid_bodies "+str(len(all))+"\n")
            for j in all:
                #print j
                sse = main_hs_dict[j]
                temp = sse.get_start_end_PDB()
                #print temp
                if len(temp) == 2:
                    for strand in temp: #writes helcies into clustfiles
                            #outfile.write(str(strand[2])+':'+str(strand[0])+'-'+str(strand[1])+'\n')
                            outfile1.write(str(strand[0])+':'+str(strand[2])+' '+str(strand[1])+':'+str(strand[2])+' ')
                            outfile1.write('\n')
                else: #writes the loops into clustfile
                    outfile1.write(str(temp[0])+':'+str(temp[2])+' '+str(temp[1])+':'+str(strand[2])+' ')
                    outfile1.write('\n')
                    #print(str(temp[0])+':'+str(temp[2])+' '+str(temp[1])+':'+str(strand[2])+' ')
                #outfile1.write('\n')
        outfile.close()
    outfile1.close()
    outfile2.close()
    chim_file(file1_base, sys.argv[1])
    compare(file1_base)
    end = time.time()
    print 'Total cpu time : %f minutes' %((end-start)/60.0)
    #end of main program

def main():
    # if incorrect arguments put in by user
    if len(sys.argv) != 4:
        print(""" Error: Incorrect command line input
    
    Example input:
        (1) For protein: ribfindx.py 4zzz.pdb 4zzz.dssp 6.5
        (2) For rna: ribfindx.py 3eoh.pdb 3eoh.xml 7.5
        (3) For rna and protein: ribfindx.py 4qvi.pdb 4qvi.dssp:4qvi.pdb.xml 6.5:7.5""")
        sys.exit()
    assert os.path.isfile(sys.argv[1])
    input_structure = str_object(sys.argv[1])
    # rna_ch = check_na_atoms(input_structure)
    # protein_ch = check_protein_atoms(input_structure)
    
    #protein and rna
    file2 = sys.argv[2]
    filesplit = file2.split(':')
    if len(filesplit) == 2:
        protsec = filesplit[0]
        rnasec = filesplit[1]
        cutoff = sys.argv[3]
        cutoffsplit = cutoff.split(':')
        prot_cutoff = float(cutoffsplit[0])
        rna_cutoff = float(cutoffsplit[1])
        main_protein(sys.argv[1],protsec,prot_cutoff)
        considerLoops = True #Whether to consider single-stranded regions as SSEs or not
        main_rna(sys.argv[1],rnasec,rna_cutoff)
    elif len(filesplit) == 1 and '.dssp' in filesplit[0]:
        # protein
        protsec = filesplit[0]
        cutoff = sys.argv[3]
        cutoffsplit = cutoff.split(':')
        prot_cutoff = float(cutoffsplit[0])
        main_protein(sys.argv[1],protsec,prot_cutoff)
    elif len(filesplit) == 1:
        # rna
        rnasec = filesplit[0]
        cutoff = sys.argv[3]
        cutoffsplit = cutoff.split(':')
        rna_cutoff = float(cutoffsplit[0])
        considerLoops = True
        main_rna(sys.argv[1],rnasec,rna_cutoff)
    else:
        print 'Your protein does not have either a protein or RNA chain'

"""# protein and dna
if sys.argv[4] == 'dna':
    protsec = sys.argv[2]
    main_protein()
    main_dna()

# rna and dna
if sys.argv[4] == 'rnadna':
    rnasec = sys.argv[2]
    main_rna()
    main_dna()

# protein, rna and dna
if sys.argv[4] == 'all':
    file2 = sys.argv[2]
    filesplit = file2.split(':')
    protsec = filesplit[0]
    rnasec = filesplit[1]
    main_protein()
    considerLoops = True #Whether to consider single-stranded regions as SSEs or not
    main_rna()
    main_dna()"""

if __name__ == '__main__':
    main()