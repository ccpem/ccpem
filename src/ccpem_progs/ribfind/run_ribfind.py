#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.

'''
CCPEM wrapper to pass arguments to and run ribfind.
'''

import os
import sys
from ribfind import ribfind
from ccpem_core import ccpem_utils
import json


def main(args_path):
    assert os.path.exists(args_path)
    args = json.load(open(args_path, 'r'))
    assert os.path.exists(args['input_pdb'])
    assert os.path.exists(args['output_dssp'])
    # Set output directory
    output_path = args['job_location']
    if output_path is not None:
        ccpem_utils.check_directory_and_make(directory=output_path)
        os.chdir(output_path)
    ribfind(pdbfile=args['input_pdb'],
            dsspfile=args['output_dssp'],
            contactDistance=args['contact_distance'])

if __name__ == '__main__':
    # json file containing args must passed
    if len(sys.argv) > 1:
        main(args_path=sys.argv[1])
    else:
        print 'Warning: no json arguments file given'
