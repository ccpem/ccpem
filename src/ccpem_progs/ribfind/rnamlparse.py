
import xml.etree.ElementTree
import structures, sys
from operator import attrgetter

def fileParse(filename):
    """
    Parses an RNAML file and returns an RNAObject (see structures.py) describing its secondary structure. Requires a standard RNAML
    file as defined by the RNAML standards that contains 2D structure information. RNAML files can be obtained directly from the NDB
    or produced from PDB files by the RNAView program.
    """
    try:
        doc = xml.etree.ElementTree.parse(filename)
    except:
        print("File not found or not valid XML file")
        sys.exit()
    RNA = structures.RNAObject()
    def structToList(structList):
        structList.append(structures.StructObject(title, isHelix, startPos, endPos))
    for molecule in doc.findall("molecule"):
        chainID = int(molecule.attrib["id"])
        RNAMLNumbering = range(int(molecule.findtext("./sequence/numbering-system/numbering-range/start")),int(molecule.findtext("./sequence/numbering-system/numbering-range/end"))+1)
        PDBNumbering = molecule.findtext("./sequence/numbering-table").split()
        for n in range(len(PDBNumbering)-1):
            k = n+1
            aList = []
            while PDBNumbering[n]==PDBNumbering[k]:
                if not n in aList:
                    aList.append(n)
                aList.append(k)
                k += 1
            n = k
            for m in range(len(aList)):
                PDBNumbering[aList[m]] += chr(65+m)                
        numberingMap = dict(zip(RNAMLNumbering, PDBNumbering))
        RNA.chains.append(structures.ChainObject(int(molecule.attrib["id"]), int(molecule.find("./sequence/numbering-table").attrib["length"]),numberingMap))
        for helix in molecule.findall("./structure/model/str-annotation/helix"):
            title = helix.attrib["id"]+"."+str(chainID)
            isHelix = True
            startPos = int(helix.findtext("./base-id-5p/base-id/position"))
            endPos = int(helix.findtext("./base-id-5p/base-id/position"))+(int(helix.findtext("./length"))-1)
            structToList(RNA.chains[chainID-1].structList)
            startPos = int(helix.findtext("./base-id-3p/base-id/position"))-(int(helix.findtext("./length"))-1)
            endPos = int(helix.findtext("./base-id-3p/base-id/position"))
            structToList(RNA.chains[chainID-1].structList)
        for loop in molecule.findall("./structure/model/str-annotation/single-strand"):
            title = loop.findtext("./segment/seg-name")+"."+str(chainID)
            isHelix = False
            startPos = int(loop.findtext("./segment/base-id-5p/base-id/position"))
            endPos = int(loop.findtext("./segment/base-id-3p/base-id/position"))
            structToList(RNA.chains[chainID-1].structList)
    for helix in doc.find("interactions").findall("./str-annotation/helix"):
        titleAppend = helix.find("./base-id-5p/base-id/molecule-id").attrib["ref"]+"-"+helix.find("./base-id-3p/base-id/molecule-id").attrib["ref"]
        title = helix.attrib["id"]+"."+titleAppend
        chainID = int(helix.find("./base-id-5p/base-id/molecule-id").attrib["ref"])
        isHelix = True
        startPos = int(helix.findtext("./base-id-5p/base-id/position"))
        endPos = int(helix.findtext("./base-id-5p/base-id/position"))+(int(helix.findtext("./length"))-1)
        structToList(RNA.chains[chainID-1].structList)
        chainID = int(helix.find("./base-id-3p/base-id/molecule-id").attrib["ref"])
        startPos = int(helix.findtext("./base-id-3p/base-id/position"))-(int(helix.findtext("./length"))-1)
        endPos = int(helix.findtext("./base-id-3p/base-id/position"))
        structToList(RNA.chains[chainID-1].structList)
    for chain in RNA.chains:
        chain.structList.sort(key=attrgetter("startPos"))
    return RNA

def mapPDBChains(RNA,PDB):
	"""
	Adds a dictionary to convert from RNAML chain numbering to PDB chain numbering.
	"""
	chainList = []
	def testChain(chain):
		for residue in chain.get_list():
			if not residue.get_id()[0] == " ":
				continue
			else:
				d = residue.get_resname().strip()
				if d in ["G", "A", "U", "C"]:
					return True
				else:
					return False
	for PDBchain in PDB[0].get_list():
		if testChain(PDBchain):
			chainList.append(PDBchain.get_id())
	i = 0
	for chain in RNA.chains:
		chain.PDBChain = chainList[i]
		i += 1
	return None
        