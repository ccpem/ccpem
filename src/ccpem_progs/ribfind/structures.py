class RNAObject:
    """
    Builder of master RNA objects, mostly a container for a 'chains' list containing ChainObjects.
    """
    def __init__(self):
        self.chains = []

class ChainObject:
    """
    Builder of chain objects, containing:
    chainID: Integer (counting from 1 upwards, as per RNAML standards) identifying the chain.
    length: Integer defining the chain's length.
    structList: List of StructObjects defining the SSEs present in the chain.
    """
    def __init__(self, chainID, length, numberingMap):
        self.chainID = chainID
        self.length = length
        self.structList = []
        self.numberingMap = numberingMap

class StructObject:
    """
    Builder of SSE objects, with parameters:
    title: String naming the object, primarily for display purposes, shared between the two objects that form a helix.
    isHelix: Boolean identifying the object's nature, TRUE for helix, FALSE for loop.
    startPos: Integer identifying the first nucleotide (5'-3' direction) of the structure.
    endPos: Integer identifying the last nucleotide (5'-3' direction) of the structure.
    """
    def __init__(self, title, isHelix, startPos, endPos):
        self.title = title
        self.isHelix = isHelix
        self.startPos = startPos
        self.endPos = endPos