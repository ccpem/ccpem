!C******************************************************************************
!C    Copyright (c) Medical Research Council, Laboratory of Molecular          *
!C     Biology.    All rights reserved.                                        *
!C                                                                             *
!C     All files within this package, unless otherwise stated, are Copyright   *
!C     (c) Medical Research Council. Redistribution is forbidden.               *
!C     This program was written by Alan Roseman at the MRC Laboratory of        *
!C     Molecular Biology, Hills Road, Cambridge, CB2 2QH, United Kingdom.       *
!C                                                                             *
!C     The package and the source are provided without warranty of any kind,    *
!C     either express or implied, including but not limited to merchantability  *
!C     or fitness for a particular purpose.                                    *
!C                                                                             *
!C     The MRC will not be liable in any way for any losses howsoever caused    *
!C     by the use of the package or the source, such losses to include but not  *
!C     be limited to loss of profit, business interruption, loss of business    *
!C     information, or other pecuniary loss, including but not limited to       *
!C     special incidental consequential or other damages.  The user agrees      *
!C     to hold the MRC harmless for any loss, claim, damage, or liability,      *
!C     of whatsoever kind or nature, which may arise from or in connection      *
!C     with the use thereof.                                                   *
!C                                                                              *
!C     COPYRIGHT 2002                                                          *
!C                                                                             *
!C*******************************************************************************
!DockEMV2.00.f90, AMR Jan 2003.
!DockEM(version 2), incorporating the FLCF (see Ultramicroscopy 2002).
! The target function and rationale are as for DockEM v1 (Acta D, 2000)
!AMR 11/2002
! origin adjustment dec 2002. 
! activate scale search, feb 2003
! loops convered to integer for g95, alan 11/5

! 1. read map, search object, mask, angles
! 2. rotate sobj,mask, calc flcf, store max, loop over angles
! 3. output: cccmap, psimap, phimap, thetamap

!C input: spider angles file, 'angles.spi'
!C       Three digit run version key.
!C      Angular sampling for psi, degrees.



        Module image_arrays2
                real, dimension (:,:,:), allocatable :: map, rotsobj, rotmask,aa             
		real, dimension (:,:,:), allocatable :: lcf1,cnv2
		real meanmap,meanaa,meanmask,stdmap,stdaa,stdmask,meanrot,stdrot
        End Module image_arrays2



        program DockEMv2
  
  
        Use  image_arrays2
        Use  mrc_image
        real, dimension (:,:,:), allocatable :: cccmaxmap,psimap,phimap,thetamap,scalemap
        real, dimension (:,:,:), allocatable :: sobj,mask,lcfmask,pmask
     
     	
	real angles(99999,3),psi,theta,phi,val,STD
	integer b,nb
	
	integer runcode,err
	integer nm,ni,cxyz(3)
	integer a,numangles,ji1,ji2
	integer counter,counter2,acount,bcount
	
	
	character*80 filename,sobjfilename,maskfilename,anglesfile,junk,pdbfile
	character*3 num
	logical t,f
	real x,y,z,maskth
	integer nm8,numatoms
	real*8 summap,sumcccs,sumnumccc,stdccc
	real cx,cy,cz
	real deg,rad
	integer ix,iy,iz,vec(3)
	real s1,s2,s3,iscale,omst,domega
	integer sloop,ssteps
	
	t=.true.
	f=.false.

      print*, 'DockEMv2.31 '
      print*, '==========='

! fix at manchester 

	print*,'Enter the EM map filename.'
	read*,filename
	
	print*,'Enter the sampling of the map (A/pixel)'
	read*, sampling
	
	print*,'Enter the search object map filename.'
	read*,sobjfilename
	
	print*,'Enter the mask map filename.'
	read*,maskfilename
	
	print*,'Enter a threshold for the mask map.'
	read*,maskth
	
	
	print*,'Enter the angles file (spider format).'
	read*,anglesfile
	
	print*,'Enter the increment for the angular omega search.'
	read*,domega
	
      write (6,*) 'Enter a run code, for output files.'
      read (5,30) runcode
30    format (I3)

	print*,'Enter the pdbfile for the search object.'
	read*,pdbfile


	print*,'Enter the scale factor range, and step.'
	read*,s1,s2,s3
	print*,s1,s2,s3
	

! pdb file com used to set origin of rotations.

        call readpdb(pdbfile,numatoms)
        call com(cx,cy,cz,numatoms)
        cx=int((cx/sampling)+0.5)
        cy=int((cy/sampling)+0.5)
        cz=int((cz/sampling)+0.5)

	PRINT*,'COM of sobj at ',cx,cy,cz,' pixels.'

!C ***********************************************
!C open input from MRC format file
        
        CALL IMOPEN(1,filename,'RO')
        CALL IRDHDR(1,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)

	nxm1=nx-1
        nym1=ny-1
        nzm1=nz-1
        
        nxp1=nx+1
        nxp2=nx+2
        
!C allocate  image_files 
        allocate (map(0:nxp1,0:nym1,0:nzm1), sobj(0:nxp1,0:nym1,0:nzm1))
        allocate (mask(0:nxp1,0:nym1,0:nzm1),pmask(0:nxp1,0:nym1,0:nzm1))
         
        allocate (rotsobj(0:nxp1,0:nym1,0:nzm1),rotmask(0:nxp1,0:nym1,0:nzm1))
        allocate (AA(0:nxp1,0:nym1,0:nzm1))
	   
        CALL READMRCMAP(1,filename,map,nx,ny,nz,T,F,T,err) !read, is open, dont close, add 2 bytes
        CALL READMRCMAP(2,sobjfilename,sobj,nx,ny,nz,F,T,T,err) !read, not open,  close, add 2 bytes         
        CALL READMRCMAP(3,maskfilename,mask,nx,ny,nz,F,T,T,err) !read, not open,  close, add 2 bytes  

!make mask binary

! vs maskth
	where (mask.ge.0.0) 
			  pmask=1.0
	elsewhere
			pmask=0.0
	endwhere
	
	nm = int(sum(pmask(0:nxp1,0:nym1,0:nzm1)))			!number of non zero mask points

	sobj=sobj*mask			!this will speed up the rotations, uninteresting regions are now 0.
	
	print*, nm
	
!C ***************************************************


!c 2.  read angles
      open(2,file=anglesfile,status='old',err=902)
          read(2,*) junk

        a=0
!       do a=1,numangles
1999     continue
         a=a+1
                read(2,200,err=901,end=901) ji1,ji2,angles(a,1),angles(a,2),angles(a,3)
200             format (i5,i2,3G12.5)
        print*, ji1,ji2,angles(a,1),angles(a,2),angles(a,3)
        goto 1999
!       enddo
901     continue
        close(2)
        numangles=a-1
        write(6,*) numangles,' angles read from ', anglesfile
        if (numangles.eq.0) STOP 'Angles file is null.'



!C ***********************************************
!C  set up and open  output image_files

        allocate (cccmaxmap(0:nxp1,0:nym1,0:nzm1),phimap(0:nxp1,0:nym1,0:nzm1))
        allocate (thetamap(0:nxp1,0:nym1,0:nzm1), psimap(0:nxp1,0:nym1,0:nzm1))
        allocate (cnv2(0:nxp1,0:nym1,0:nzm1),lcf1(0:nxp1,0:nym1,0:nzm1),scalemap(0:nxp1,0:nym1,0:nzm1))          

!C ***********************************************
	cccmaxmap = -1.2
	phimap=-999
	psimap=-999
	thetamap=-999
	summap=0
	sumcccs=0
	sumnumccc=0
	stdccc=0
	
! get number of +ve map points for norm of stds.
	where (map.gt.0.0)
		aa=1.0
	elsewhere 
		aa=0.0
	endwhere
	
	summap=sum(aa)	
	
!C 5. do the search
     
     
! set up ffts
! fft map, mapsq, 
! A,AA     
		print*, "001"
		nm8=nx*ny*nz
		print*,nm,nm8

		val=minval(map(0:nxm1,0:nym1,0:nzm1))
!		map(0:nxm1,0:nym1,0:nzm1)=map(0:nxm1,0:nym1,0:nzm1)-val
		call msdset(map,nx,ny,nz,nm8,val,err)
		print*,nm,nm8		
		call msd(map,nx,ny,nz,nm8,meanmap,stdmap,err)
		PRINT*,'MSD t ',meanmap,stdmap
		
		print*,val
		AA=map*map
		call msd(AA,nx,ny,nz,nm8,meanaa,stdaa,err)
		print*,nm8,meanaa,stdaa,err
		print*,NXP2,NX,NY,NZ
                CALL BCKW_FT(map,NXP2,NX,NY,NZ)
                print*,'ft map done'
		CALL BCKW_FT(AA,NXP2,NX,NY,NZ)
		print*,'ft aa done'
	print*,'done fts'

	print*,nx,ny,nz
    	val=0
        counter = 0
        counter2 = 0
        acount=0
        
 
        
        do a = 1, numangles      
	        print*,'angle searching = ',a,' numangles = ',numangles,'nm=',nm
        	bcount = 0
       		omst=-((int(360./domega))/2)*domega
       		
		!do b=0,0
		nb=int(360/domega)
 !      		do b = omst,179.0,domega
 		do b=0,nb
        		bcount = bcount +1
       			 !psi = angles(a,1)
        		psi = b*domega+ omst
		        theta = angles(a,2)
       			 phi = angles(a,3)
       		
       		
!       			do iscale=s1,s2,s3
			ssteps=int((s2-s1)/s3)
			do sloop=0,ssteps
			iscale=sloop*s3+s1
       			print*,iscale,sloop,ssteps
       			 call rotobjS(sobj,rotsobj,psi,theta,phi,nx,ny,nz,cx,cy,cz,iscale)
       		
  		         call rotobjS(mask,rotmask,psi,theta,phi,nx,ny,nz,cx,cy,cz,iscale)
	
!make mask binary, get rid of feeble edge points
			where (rotmask.ge.0.5)  
					rotmask=1
				elsewhere
					rotmask=0
			endwhere
			
			
			nm = sum(rotmask(:,:,:))			!number of non zero mask points

       			rotsobj=rotsobj*rotmask			!get rid of ojbect points beyond the mask.
			call msd(rotmask,nx,ny,nz,nm8,meanmask,stdmask,err)
	
		ni=nx*ny*nz
	 	call msdset(rotsobj,nx,ny,nz,nm,val,err)	!set the msd of sobj to 0,1
			
		rotsobj=rotsobj*rotmask				! zero background region
		
			
		call msd(rotsobj,nx,ny,nz,nm,meanrot,stdrot,err)

 	  		    
    		    CALL BCKW_FT(rotsobj,NXP2,NX,NY,NZ)
		    CALL BCKW_FT(rotmask,NXP2,NX,NY,NZ)
	   
      		    call FLCF3D(nx,ny,nz,nm) ! map, mapsq,     
				print*,'done flcf',b
				
				
				where (lcf1.gt.cccmaxmap)
					cccmaxmap=lcf1
					phimap=phi
					psimap=psi
  					thetamap=theta
  					scalemap=iscale
  				endwhere
  				sumcccs=sumcccs+sum(lcf1)
  				sumcccssq=sumcccssq+sum(lcf1*lcf1)
  				sumnumccc=sumnumccc+nm8
  		enddo
  		enddo
  	enddo
  	
                     
!C 6. write the searchmaps





	CALL IMOPEN(7,'cccmaxmap'//num(runcode)//'.mrc','UNKNOWN')
        CALL ITRHDR(7,1)
	CALL WRITEMRCMAP(7,cccmaxmap,nx,ny,nz,sampling,err) !open already, close it, subtract 2 from nx

	CALL IMOPEN(8,'phimap'//num(runcode)//'.mrc','UNKNOWN')
        CALL ITRHDR(8,1)
        CALL WRITEMRCMAP(8,phimap,nx,ny,nz,sampling,err)
	        
	CALL IMOPEN(9,'thetamap'//num(runcode)//'.mrc','UNKNOWN')
        CALL ITRHDR(9,1)
	CALL WRITEMRCMAP(9,thetamap,nx,ny,nz,sampling,err)

        CALL IMOPEN(10,'psimap'//num(runcode)//'.mrc','UNKNOWN')
        CALL ITRHDR(10,1)
	CALL WRITEMRCMAP(10,psimap,nx,ny,nz,sampling,err)

	CALL IMOPEN(11,'scalemap'//num(runcode)//'.mrc','UNKNOWN')
        CALL ITRHDR(11,1)
	CALL WRITEMRCMAP(11,scalemap,nx,ny,nz,sampling,err)
	
	
	val=maxval(cccmaxmap)
	print*,'max ccc is :',val
	print*,'at ', maxloc(cccmaxmap)
	vec=maxloc(cccmaxmap)
	ix=vec(1)-1
	iy=vec(2)-1
	iz=vec(3)-1
	print*,ix,iy,iz
	print*,'phi=',phimap(ix,iy,iz)
	print*,'theta=',thetamap(ix,iy,iz)
	print*,'psi=',psimap(ix,iy,iz)
	print*,'ccc=',cccmaxmap(ix,iy,iz)
	print*,'scale=',scalemap(ix,iy,iz)
	
	print*,'meanCCC = ',sumcccs/sumnumccc
	stdccc=((sumnumccc*sumcccssq)+(sumcccs*sumcccs))/(sumnumccc*sumnumccc)
	print*,'stdevCCC = ',stdccc
	
	goto 999

!C ***********************************************

902     continue  
! err reading angles
        print*, 'error reading angles file.'
        stop
903     continue
!Cerr reading map
        print*,'error reading EM-map.'
        stop

999    continue    
       print*,'Program finished O.K.'

       
              
       
       CONTAINS


	SUBROUTINE READMRCMAP(stream,filename,map,X,Y,Z,open,close,add2,err)
 
        Use mrc_image

         INTEGER  X,Y,Z,err
         INTEGER  stream, IX,IY,IZ,xm1
         real, DIMENSION (0:X+1,0:Y-1,0:Z-1) :: map  
         INTEGER S(3),S1
         logical add2,open,close
         character*80 filename
            
          if(.not. open) then
          	       CALL IMOPEN(stream,filename,'RO')
       		       CALL IRDHDR(stream,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
	  endif
            
         
      
      
!     read in file 

      DO 350 IZ = 0,z-1
          DO 350 IY = 0,y-1
            CALL IRDLIN(stream,ALINE,*998)
            DO 300 IX = 0,x-1       
       !     print*,ix
                map(ix,iy,iz) = ALINE(IX+1)
300         CONTINUE  
	if(add2) then  
     		       map(X+0,iy,iz)=0
      		       map(X+1,iy,iz)=0
        endif
        
!           print*,ix,iy,iz
350   CONTINUE

	if (close) then
		CALL IMCLOSE(stream)
		print*,'closed ',stream
	endif

      return
998   STOP 'Error on file read.'
     	
	END SUBROUTINE READMRCMAP
	
	
       
      SUBROUTINE WRITEMRCMAP(stream,map,x,y,z,sampling,err)
      Use  mrc_image
      REAL val,sampling
      
      integer x,y,z,err,stream,ix,iy,iz
      real map(0:x+1,0:y-1,0:z-1)

      print*,'write_mrcimage'         

      print*,NX,NY,NZ  
!     write file 

      DMIN =  1.E10
      DMAX = -1.E10
      DOUBLMEAN = 0.0

      DO 450 IZ = 0,NZM1
      DO 450 IY = 0,NYM1
            
            DO 400 IX = 0,NXM1
     
                val = map(IX,IY,IZ)
                ALINE(IX+1) = val
                
                DOUBLMEAN = DOUBLMEAN + val         
                IF (val .LT. DMIN) DMIN = val
                IF (val .GT. DMAX) DMAX = val

400         CONTINUE   

      CALL IWRLIN(stream,ALINE)
450   CONTINUE
      DMEAN = DOUBLMEAN/(NX*NY*NZ)

      cell(1) = sampling *NX
      cell(2) = sampling *NY
      cell(3) = sampling *NZ
      cell(4) = 90
      cell(5) = 90
      cell(6) = 90

      CALL IALCEL(STREAM,CELL)  
      CALL IWRHDR(stream,TITLE,-1,DMIN,DMAX,DMEAN)
      CALL IMCLOSE(stream)
      
      return
999   STOP 'Error on file write.'
 
      END SUBROUTINE WRITEMRCMAP
	
	
	
	
	SUBROUTINE rotobj(obj,therotobj,psi,theta,phi,nx,ny,nz,cx,cy,cz)
	
	
	       

!c sub to apply rotation about centre of map to map object 
!c convert rotations to matrix form
!C spider convention for euler angles to mat

! the object maps are nxp1,ny,nz in size
!rotn centre is nx/2,ny/2,nz/2
     
	real obj(0:nx+1,0:ny-1,0:nz-1),therotobj(0:nx+1,0:ny-1,0:nz-1)
        real cx,cy,cz,psi,phi,theta,x,y,z
        integer nx,ny,nz,ix,iy,iz,nxm1,nym1,nzm1
	real rad,deg
        
        real mat(3,3),mata(3,3),matb(3,3),matc(3,3),matd(3,3)
        real val,b
        
        therotobj=0
        nxm1=nx-1
        nym1=ny-1
        nzm1=nz-1


        mata(1,1)=cos(rad(psi))
        mata(1,2)=sin(rad(psi))
        mata(1,3)=0.0

        mata(2,1)=-sin(rad(psi))
        mata(2,2)=cos(rad(psi))
        mata(2,3)=0.0

        mata(3,1)=0.0
        mata(3,2)=0.0
        mata(3,3)=1.0

        matb(1,1)=cos(rad(theta))
        matb(1,2)=0.0
        matb(1,3)=-sin(rad(theta))

        matb(2,1)=0.0
        matb(2,2)=1.0
        matb(2,3)=0.0

        matb(3,1)=sin(rad(theta))
        matb(3,2)=0.0
        matb(3,3)=cos(rad(theta))

        matc(1,1)=cos(rad(phi))
        matc(1,2)=sin(rad(phi))
        matc(1,3)=0.0

	matc(2,1)=-sin(rad(phi))
        matc(2,2)=cos(rad(phi))
        matc(2,3)=0.0

        matc(3,1)=0.0
        matc(3,2)=0.0
        matc(3,3)=1.0

        call matmult(matb,matc,mat)
        call matmult(mata,mat,matd)


!	print*,matd

	do iz=0,nzm1
		do iy=0,nym1
			do ix=0,nxm1
			
				x=float(ix)
				y=float(iy)
				z=float(iz)
				
				val=obj(ix,iy,iz)
				if (val.gt.0.1) then			!if density is 0 don't waste time.
					call matmult2(x,y,z,matd,cx,cy,cz)  	!transform coords by rotn
	
	call interpo2(therotobj,nx,ny,nz,x,y,z,val)		!interpo density at new posn to new map.)
	
				endif
  			enddo
        	enddo
        enddo
	therotobj(nx:nx+1,0:ny-1,0:nz-1)=0
        return

	END SUBROUTINE rotobj


	subroutine matmult(mat1,mat2,mat)
!C multiply 2 matrices together
        real mat(3,3),mat1(3,3),mat2(3,3)

        mat=matmul(mat1,mat2)
!	print*,'mm'
        return 
        end subroutine matmult

        subroutine matmult2(x,y,z,mat,cx,cy,cz)
!c mult coords by transformation mat


        real mat(3,3),cx,cy,cz, x,y,z
        real coords(3),newcoords(3)

        coords(1) = x-cx
        coords(2) = y-cy
        coords(3) = z-cz

        newcoords=matmul(mat,coords)

        x = newcoords(1) + cx
        y = newcoords(2) + cy
        z = newcoords(3) + cz

        return
        end subroutine matmult2

		
	
	SUBROUTINE FLCF3D(x,y,z,nm)
	
	
	! in, ft of :map , mapsq, sobj, mask
	! out FLCF/cccmap
	use image_arrays2
	integer x,y,z,nm

	real*8 nm8
	real sd,masksd,val
	integer err
	integer ni
	
	ni=x*y*z
	nm8=nm

	call CORRELATION3D(rotmask,map,lcf1,x,y,z)

	call CORRELATION3D(rotmask,aa,cnv2,x,y,z)

	cnv2=(nm8*cnv2)
	cnv2=(cnv2-(lcf1*lcf1))/(nm8*nm8)
	
	call CORRELATION3D(rotsobj,map,lcf1,x,y,z)

	val=minval(cnv2)
!	print*,'min of v=',val

	where (cnv2.gt.0.00001)
			cnv2=sqrt(cnv2)

		elsewhere
			cnv2=0.
!			print*,'V was lt 0 !!'
	endwhere
	
	where (cnv2.gt.0)
			lcf1=(lcf1/cnv2)/nm8
	elsewhere
			lcf1=-2
	endwhere
	

	END SUBROUTINE FLCF3D
	
	
	subroutine CONVOLUTION3D(a,b,c,nx2,ny2,nz2)
        integer nx2,ny2,nz2,i,j,k
        real a(0:nx2+1,0:ny2-1,0:nz2-1),b(0:nx2+1,0:ny2-1,0:nz2-1),c(0:nx2+1,0:ny2-1,0:nz2-1)
        real n
        integer nxp2
	integer dx

        nxp2=nx2+2

        c=0
        
        do k=0,nz2-1
  	      do j=0,ny2-1
            	    do i=0,nx2+1,2
                
                        c(i,j,k)=a(i,j,k)*b(i,j,k)-a(i+1,j,k)*b(i+1,j,k)
                        c(i+1,j,k)=a(i,j,k)*b(i+1,j,k)+a(i+1,j,k)*b(i,j,k)
               
                enddo
            enddo
       enddo

        CALL FORW_FT(c,NXP2,NX2,NY2,NZ2)

        n=nx2*ny2*nz2		
        C=C*N

        end subroutine CONVOLUTION3D


         subroutine CORRELATION3D(a,b,c,nx,ny,nz)

        integer nx,ny,nz
        real a(0:nx+1,0:ny-1,0:nz-1),b(0:nx+1,0:ny-1,0:nz-1),c(0:nx+1,0:ny-1,0:nz-1)
        real r1,r2,hp,lp,sampling,n
        integer nxp2
        integer i,j,k
  
	integer dx
	
        nxp2=nx+2
!        r1=hp/sampling
!       r2=lp/sampling
	dx=int(nx/2.)+1


        c=0
        do k=0,nz-1
       	    do j=0,ny-1
                do i=0,nx+1,2
                        !r=radft(i,j,ny)
                        !if ((r.gt.r1).and.(r.lt.r2)) then
                        c(i,j,k)=a(i,j,k)*b(i,j,k)+a(i+1,j,k)*b(i+1,j,k)
                        c(i+1,j,k)=a(i,j,k)*b(i+1,j,k)-a(i+1,j,k)*b(i,j,k)
                        !endif
                enddo
            enddo
        enddo
        
        CALL FORW_FT(c,NXP2,NX,NY,NZ)
 
        n=nx*ny*nz					
        C=C*n

        end subroutine CORRELATION3D



      subroutine msdset(map,nx,ny,nz,nm,val,err)
      
      real map(0:nx+1,0:ny-1,0:nz-1)
      real*8 map8(0:nx+1,0:ny-1,0:nz-1)
      integer nx,ny,nz,nxm1,nym1,nzm1,nxp1,nxp2,z,err
      real*8 mean,sd,lsum,sum_sqs,sq
      real val,th
      integer nm
      real nm8
     

   ! treats as if all vals not in the masked nm, are zero. must be 0 !!!
      nxp1=nx+1
      nym1=ny-1
      nxm1=nx-1
      nzm1=nz-1
      nm8=nm
      
      if (nm.le.0) STOP 'Mistake, nm=0'
      
      err=0
      lsum = sum(map(0:nxm1,0:nym1,0:nzm1))

      mean=lsum/nm8

	map8=map
	map8=map8*map8

       sum_sqs=sum(map8(0:nxm1,0:nym1,0:nzm1))
    
 	 lsum=lsum*lsum
 	 sum_sqs=nm8*sum_sqs
 	 sq=((sum_sqs-lsum))
 	 sq=sq/(nm8*nm8)
    

       if (sq.lt.0.0) stop 'sd lt zero in msd set.'

       th=0.000000000001
      if (sq.gt.th) then
                sd = sqrt(sq)
                map=(map-mean)/sd
                val= -mean/sd
                err=0
        elseif (sq.le.0.0) then
                err=1
                print*,'le0'
     		stop
        elseif (sq.le.th) then 
                map=(map-mean)
        	print*,'at threshold'
                val= -mean/sd
                err=0
		stop
        endif

       return
       end subroutine msdset

	SUBROUTINE MSD(map,nx,ny,nz,nm,val,STD,err)
      
      real map(0:nx+1,0:ny-1,0:nz-1)
 	real*8 map8(0:nx+1,0:ny-1,0:nz-1)
      integer nx,ny,nz,nxm1,nym1,nzm1,nxp1,nxp2,z,err
      real*8 mean,sd,lsum,sum_sqs,sq
      real val,th,STD
      integer nm
      real nm8
     
   ! treats as if all vals not in the masked nm, are zero. must be 0 !!!
      nxp1=nx+1
      nym1=ny-1
      nxm1=nx-1
      nzm1=nz-1
       nm8=nm
      SD =-9999999
      if (nm.le.0) STOP 'Mistake, nm=0'
      
      err=0
      lsum = sum(map(0:nxm1,0:nym1,0:nzm1))
      mean=lsum/nm8

	map8=map
	map8=map8*map8
       sum_sqs=sum(map8(0:nxm1,0:nym1,0:nzm1))
 
 	 lsum=lsum*lsum
 	 sum_sqs=nm8*sum_sqs
 	 sq=((sum_sqs-lsum))
 	 sq=sq/(nm8*nm8)
    	
	

       if (sq.lt.0) stop 'sd lt zero in msd.'

       th=0.00001
      if (sq.gt.th) then
                sd = sqrt(sq)
                val= -mean/sd
                err=0
        elseif (sq.le.0) then
                err=1
                print*,'le0'
                stop
        elseif (sq.le.th) then 
                !map=(map-mean)
        	print*,'at threshold'
                err=0
		stop
        endif

   	VAL=MEAN
   	STD=SD
       return
       end subroutine MSD
	
	
	
	SUBROUTINE rotobjS(obj,therotobj,psi,theta,phi,nx,ny,nz,cx,cy,cz,iscale)
	
	
	    
!c sub to apply rotation about centre of map to map object 
!c convert rotations to matrix form
!C spider convention for euler angles to mat

! the object maps are nxp1,ny,nz in size
!rotn centre is nx/2,ny/2,nz/2
     
	real obj(0:nx+1,0:ny-1,0:nz-1),therotobj(0:nx+1,0:ny-1,0:nz-1)
        real cx,cy,cz,psi,phi,theta,x,y,z,scx,scy,scz
        integer nx,ny,nz,ix,iy,iz,nxm1,nym1,nzm1
	real rad,deg
        
        real mat(3,3),mata(3,3),matb(3,3),matc(3,3),matd(3,3)
        real val,b,iscale
        
        therotobj=0
        nxm1=nx-1
        nym1=ny-1
        nzm1=nz-1


        mata(1,1)=cos(rad(psi))
        mata(1,2)=sin(rad(psi))
        mata(1,3)=0.0

        mata(2,1)=-sin(rad(psi))
        mata(2,2)=cos(rad(psi))
        mata(2,3)=0.0

        mata(3,1)=0.0
        mata(3,2)=0.0
        mata(3,3)=1.0

        matb(1,1)=cos(rad(theta))
        matb(1,2)=0.0
        matb(1,3)=-sin(rad(theta))

        matb(2,1)=0.0
        matb(2,2)=1.0
        matb(2,3)=0.0

        matb(3,1)=sin(rad(theta))
        matb(3,2)=0.0
        matb(3,3)=cos(rad(theta))

        matc(1,1)=cos(rad(phi))
        matc(1,2)=sin(rad(phi))
        matc(1,3)=0.0

	matc(2,1)=-sin(rad(phi))
        matc(2,2)=cos(rad(phi))
        matc(2,3)=0.0

        matc(3,1)=0.0
        matc(3,2)=0.0
        matc(3,3)=1.0

        call matmult(matb,matc,mat)
        call matmult(mata,mat,matd)


!	print*,matd

	do iz=0,nzm1
		do iy=0,nym1
			do ix=0,nxm1
			
				x=float(ix)*iscale
				y=float(iy)*iscale
				z=float(iz)*iscale
				scx=cx*iscale
				scy=cy*iscale
				scz=cz*iscale
				
				val=obj(ix,iy,iz)
				if (val.gt.0.1) then			!if density is 0 don't waste time.
					call matmult2(x,y,z,matd,scx,scy,scz)  	!transform coords by rotn
	
	call interpo2(therotobj,nx,ny,nz,x,y,z,val)		!interpo density at new posn to new map.)
	
				endif
  			enddo
        	enddo
        enddo
	therotobj(nx:nx+1,0:ny-1,0:nz-1)=0
        return

	END SUBROUTINE rotobjS

	end program DockEMv2
