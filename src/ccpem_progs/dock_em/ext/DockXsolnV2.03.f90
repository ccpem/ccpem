
!DockXsolnV2.03.f90, AMR Mar 2015

!C DockXsoln2V1.02.f90

! centre for rotn is at centre of mass of coordinates. dec 2002.
! com taken to nearest intger

!
!11/5 amr
!C input: 
!C       pdb file file - fit object
!C       run version key.
!C       sampling of the map used for the search
!C	range of solutions to generate


!C output: pdb files corresponding to searchdoc keys.
!C  

!C v4 F90 matmults.   20/6/01
! 4/3 some changes for sgi compilers



 

	program DockXsoln2
	
        Use coordinates
        
        real sampling
        real psi,theta,phi
        character*40 pdbfile,outfile2
	integer a,angnum,bcount,angle
	real j1,j2,j3,cx,cy,cz,x,y,z
	integer ji1,ji2
	real comx,comy,comz
	integer numangles,numatoms,count
      	integer runcode
      	character*3 num
      	real dx,dy,dz,b
!C sampling for position search, in voxels      		
        integer  counter,counter2,zx,zy,zz,r1,r2,N
	integer nx3,ny3,nz3
	
	
	
	

      
      write(6,*) 'DockXsoln2'
      write(6,*) '=============='
      write(6,*) '(for DockEM2)'
      write(6,*)

!C 1. read coords
      write (6,*) 'Enter the name of the search object pdb file '
      read (5,*)  pdbfile
10    format(A40)
      print*, pdbfile


      write (6,*) 'Enter a run code, for output files.'
      read (5,*) runcode
      print*, runcode
30    format (I3)


      write (6,*) 'Enter the sampling in Angstoms.'
      read*,sampling

      write (6,*) 'Enter the range of solutions to generate.'
      read *, r1,r2
      print *, r1,r2


	print*,'Enter pdbfile to get centre from.'
	read*,pdbfile

!C ***************************************************



	call readpdb(pdbfile,numatoms)
	call com(cx,cy,cz,numatoms)
	cx=sampling*int((cx/sampling)+0.5)
	cy=sampling*int((cy/sampling)+0.5)
	cz=sampling*int((cz/sampling)+0.5)

	!shift=0.5*sampling
         shift=0
	cx=cx-shift
	cy=cy-shift
	cz=cz-shift

!C open docfile

	open(4,file='searchdoc'//num(runcode)//'.dkm',status='old')
					
510		format (1x,I8,3F8.2,I8,F8.3,3F10.2,2x,2G12.6)	



!C write top set of hits here
	do N=1,r2
		
		
		read (4,510) ji1,x,y,z,angnum,b,psi,theta,phi,j2,ccc
		

		
		if (ji1.ge.r1) then			
			dx=x*sampling
			dy=y*sampling
			dz=z*sampling
			
			
			outfile2='tophit'//num(runcode)//'.'//num(ji1)//'.pdb'
		
			call spirot(pdbfile,outfile2,psi,theta,phi,dx,dy,dz,sampling,cx,cy,cz)
		endif



	end do




	close(4)
				

	goto 999




999    continue
       print*,'Program finished O.K.'
       stop

902    stop 'Error in reading the searchdoc file'
       end
!C ************************************************

 
         
         
         
 

