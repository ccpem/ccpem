!MakedensityV2.40.f90, AMR Mar 2015
!improve solvent model
!correct of cell in header of map 1 pixel, Nov 2003
!correct density placing in map by 0.5 pixel, Nov 2003
! extent MX = nx, then cell dim = nx*D.  AMR 6/2004
! Makedensity V2.3
! mod 5/7/01
!Program to convert pdb coordinates file into electron potential density A.
!for DockEM docking program
!'Docking structures of domains into maps from cryo electron microscopy
! using local correlation' Roseman (2000) Acta Cryst D56, 1332-1340.
! A.M. Roseman 28/2/1
! Method as described in Stewart, P. L., Fuller, S. D. & Burnett, R. M.
! (1993). EMBO J. 12, 2589-2599.
! V1.01 correct header for correct cell size, N. 
! though 0:N-1 21/9/01 AMR
! no changes, compatible with DockEM(v2), AMR Dec  2002.

! inputs:
! pdb coordinates file
! example map file (ie the one you will be docking into)
! sampling
! output filename
! output:
! a new density file
! notes:
! the output map has an assumed cell size of (N) x D
! range is 0:(N-1)*D
! the coordinates are positioned literally within this cell,

! 3/02 , add option to enter background level.

!structure:
! 1. Read inputs
! 2. Read coordinates
! 3. Read map
! 4. operate
! 5. write new map





        Program Makedensity2
        Use image_arrays
	Use mrc_image
	Use coordinates
	
        integer numatoms
        integer numangles,count,oorcount
        real cx,cy,cz,sx,sy,sz
        real comx,comy,comz
        integer atom
        character*40 pdbfile        

        real sampling,shift,backg
        
    
              
! ***************************************************

 	print*,'Makedensity V2.30'
	print*,'-----------------'
	print*,'last update 11/03'
	
        WRITE(6,1000)
1000    FORMAT(//'Program to make density from pdb file'///)
     
        
! 1. Read inputs

	write (6,*) 'Enter the name of the pdb file'
	read (5,10)  pdbfile
10      format(A40)

! 2. Read coordinates	
        call readpdb(pdbfile,numatoms)
199     write (6,*) numatoms, ' atoms read from pdbfile: ',pdbfile

	 


	WRITE(6,1100)
1100    FORMAT('$Enter the map to copy the dimensions and header from:  ')
        READ(5,1200) INFILE
1200    FORMAT(A)
	print*,'Sample map name is: ',INFILE
	
	WRITE(6,1600)
1600    FORMAT('Enter the output filename:  ')
        READ(5,1200) OUTFILE
        print*,'Output density map name is: ',OUTFILE
      
        print*,'Enter the sampling required in the output map.'
        read*,Sampling 
        print*,'The sampling required is: ',sampling
        
        backg=0
        print*,'Enter the background density required in the output map (default 0).'
        read*,backg
        print*,backg
        
        
        
               
! 3. Read map
        CALL IMOPEN(1,INFILE,'RO')
        CALL IRDHDR(1,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
        
        nxp2=nx+2
        nxp1=nx+1
        nxm1=nx-1
        nym1=ny-1
        nzm1=nz-1

    
        IF (MODE .NE. 2) THEN 
   			     GOTO 997
        ENDIF
        
! open out map and transfer header

        CALL IMOPEN(2,OUTFILE,'NEW')
        CALL ITRHDR(2,1)

        WRITE(TITLE,1900) 

1900    FORMAT('Makedensity from pdb, V2.30' )
        CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
      
!        CALL IMOPEN(2,OUTFILE,'NEW')
!        CALL ITRHDR(2,1)

!        WRITE(TITLE,1900) 


!        CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)


!C ***************************************************

! 4. operate
! init variables
	allocate (map(0:nxp1,0:nym1,0:nzm1))
	map=0

!C 4. calc com of coords
      call com(cx,cy,cz,numatoms)
       comx=(cx/sampling)
       comy=(cy/sampling)
       comz=(cz/sampling)
       
       shift=0.0
! shift of 0.5 is tranformation for difference in the 'O' convention
! for map display vs coordinates, compared to DockEM's.

       print*,'COM of coodinates is ',cx,cy,cz,' angstroms.'
       print*,' or ',comx+shift,comy+shift,comz+shift,' pixels on map.'

! main loop:

	oorcount=0
        
      
        do atom=1,numatoms
!        print*, atom
	     	   sx = coords(atom,1)/sampling 
	           sy = coords(atom,2)/sampling 
	           sz = coords(atom,3)/sampling 
 		   val = coords(atom,4)
      
        	   if ((sx.gt.nx-2) .or. (sx.lt.0)) then
                   		     print*,"atom out of range"
                   		     oorcount=oorcount+1                         	   
             	          	     goto 1999
    		   endif
    		   
        	   if ((sy.gt.ny-2) .or. (sy.lt.0)) then
                   		     print*,"atom out of range" 
                   		     oorcount=oorcount+1 
                        	     goto 1999
        	   endif
       		   if ((sz.gt.nz-2) .or. (sz.lt.0)) then
                  		     print*,"atom out of range"
                  		     oorcount=oorcount+1
                        	     goto 1999      
       		   endif
                                               
                                               
		   call interpo2(map,nx,ny,nz,sx,sy,sz,val)
 		
		   
1999    	   continue
        end do                         
	print*,oorcount,' atoms out of range.'

!*******************************************************
! model a boundary here.




!*******************************************************

! put solvent to "backg"

	where (map.eq.0)
		map=backg
	endwhere

!C ***********************************************
!C Now manipulate 

!C CTF ? or  ff
!C ***********************************************


! 5. write new map

!C Now write image

        DMIN =  1.E10
        DMAX = -1.E10
        DOUBLMEAN = 0.0

        DO 450 IZ= 0,NZM1
          DO 450 IY = 0,NYM1
             DO 400 IX = 0,NXM1

                VAL = map(IX,IY,IZ)                           
                ALINE(IX+1) = val
              
                DOUBLMEAN = DOUBLMEAN + VAL   
                IF (VAL .LT. DMIN) DMIN = VAL
                IF (VAL .GT. DMAX) DMAX = VAL

400          CONTINUE
          CALL IWRLIN(2,ALINE)
450     CONTINUE
        DMEAN = DOUBLMEAN/(NX*NY*NZ)
     
        print *, DMIN,DMAX,DMEAN
        
	CALL IRTCEL(2,CELL) 	
	cell(1) = sampling *NX
	cell(2) = sampling *NY
	cell(3) = sampling *NZ
	cell(4) = 90
        cell(5) = 90
        cell(6) = 90
	CALL IALCEL(2,CELL)
!	MXYZ(1)=nxm1 
!	MXYZ(2)=nym1 
!	MXYZ(3)=nzm1
!        CALL IALSAM(2,MXYZ)
        CALL IWRHDR(2,TITLE,-1,DMIN,DMAX,DMEAN)	
        
        GOTO 990

   

!C
!   HERE FOR FINISH
!C
990     CALL IMCLOSE(2)
995     CALL IMCLOSE(1)
        WRITE(6,2000) 

2000    FORMAT('Program finished O.K.' )
        CALL EXIT
997     WRITE(6,998)
998     FORMAT(' THIS FILE IS NOT MRC MODE 2. ')
        STOP
999     STOP 'END-OF-FILE ERROR ON READ'
        END

!C *********************************************



      

