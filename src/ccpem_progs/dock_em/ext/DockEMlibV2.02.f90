!DockEMlibV2.02.f90, AMR Jan 2003.

! library of subroutines 


      Module coordinates
                parameter(maxatoms=2000000)
                real coords(maxatoms,4),sobj(maxatoms,4),squares(maxatoms),localmap(maxatoms),amof(maxatoms)
                parameter(maxangles=30000)
                real angles(maxangles,3)        
        End Module coordinates


        Module image_arrays
                real, dimension (:,:,:), allocatable :: map, searchmap, smap
        End Module image_arrays


        Module mrc_image
                DIMENSION ALINE(8192),NXYZ(3),MXYZ(3),NXYZST(3)
                DIMENSION IXYZMIN(3),IXYZMAX(3),OUT(8192)
                DIMENSION LABELS(20,10),CELL(6)
                COMPLEX CLINE(4096),COUT(4096)
                CHARACTER*40 INFILE,OUTFILE 
                CHARACTER*80 TITLE
                INTEGER NX,NY,NZ,NXM1,NYM1,NZM1,NXP1,NXP2                 
                COMMON //NX,NY,NZ,IXMIN,IYMIN,IZMIN,IXMAX,IYMAX,IZMAX
                EQUIVALENCE (NX,NXYZ), (ALINE,CLINE), (OUT,COUT)
                EQUIVALENCE (IXYZMIN, IXMIN), (IXYZMAX, IXMAX)
                DATA NXYZST/3*0/, CNV/57.29578/ 
                real DMIN,DMAX,DMEAN
        End Module  mrc_image



      subroutine com(cx,cy,cz,numatoms)
      use  coordinates
      real cx,cy,cz,x,y,z
      integer numatoms
      real comx,comy,comz,wx,wy,wz
      integer a

	comx=0.0
	comy=0.0
	comz=0.0
	wx=0.0
	wy=0.0
	wz=0.0
	
	do a=1,numatoms
		comx = comx + (coords(a,1)*coords(a,4))
		wx = wx + coords(a,4)
		comy = comy + (coords(a,2)*coords(a,4))
		wy = wy + coords(a,4)		
		comz = comz + (coords(a,3)*coords(a,4))
		wz = wz + coords(a,4)		
	enddo
        comx=comx/wx
	comy=comy/wy
	comz=comz/wz

	cx=comx
	cy=comy
	cz=comz
	
	return
	end


	subroutine spirot(pdbfile,outfile2,psi,theta,phi,dx,dy,dz,sampling,cx,cy,cz)
!C spi-rot
!C A.M. Roseman
! com adjusted in spirot. dec 2002

!C rotation applied 
!C in spider euler angles format, psi,theta,phi
!C psi =0, psi range defined in this program


!C        pdbfile - coordinates

!C output: outfile, coordinates


        Use coordinates

!C sampling for position search, in voxels
      real sampling,step
      real domega
    
        
    
      real psi,theta,phi
      real mat(3,3)
      real max,val
     

      
        character*40 pdbfile
        character*40 outfile2
	integer a,b,bcount,x,y,z,angle
	real j1,j2,j3,cx,cy,cz
	integer ji1,ji2
	integer comx,comy,comz
	integer numangles,numatoms,count
	integer ma,mb,mx,my,mz,mbcount
	real sx,sy,sz

        
        real*8 mean,tot,c
        integer  counter

	real dx,dy,dz

      write(6,*) 'spi-rot'
      write(6,*) '======='
      write(6,*) 'apply rotation about com of coordinates'


	call readpdb(pdbfile,numatoms)

199	write (6,*) numatoms, ' atoms read from pdbfile: ',pdbfile



!C ***************************************************


!C 4. calc com of coords
 !     call com (cx,cy,cz,numatoms)
 !     
 !     cx=sampling*int((cx/sampling)+.5)
 !     cy=sampling*int((cy/sampling)+.5)
 !     cz=sampling*int((cz/sampling)+.5)
      
  !    shift =0.5*sampling
      
! the shift applied is the transformation because the O and DockEM coordinate systems are
! slighlty different.
      
 !	cx=cx-shift
 !	cy=cy-shift
 !	cz=cz-shift
 
	print*,'COM of coodinates is ',cx,cy,cz,' angstoms.'


!C *********************

!C 5 read the angles

      write (6,*) 'Euler angles (in "spider" convention) for the transformation, psi, theta, phi,  '
      print*, psi,theta,phi  
      write (6,*) 'Shift in x,y and z, in angstoms '
      print*,dx,dy,dz

!C 	apply the rot
		
        call rot(cx,cy,cz,psi,theta,phi,numatoms,sampling,dx,dy,dz)
	print*,'finrot'

!C ******************************

!C 7. write coords of tophit
!	open (2,file=outfile2,status='unknown')


        call writepdb(pdbfile,outfile2,numatoms,sampling)
	print*, numatoms,' atoms written to pdbfile ',outfile2

	goto 999



902     continue  
!C err reading angles
	print*, 'error reading angles file.'
	stop
903  	continue
!Cerr reading map
	print*,'error reading EM-map.'
        stop

999    continue
	return
       end




        subroutine rot(cx,cy,cz,psi,theta,phi,numatoms,sampling,dx,dy,dz)

!c fn to apply rotation about com to coords
!c convert rotations to matrix form
!C spider convention for euler angles to mat

	use coordinates
	use image_arrays

	real cx,cy,cz,psi,phi,theta,x,y,z
	integer numatoms,a
	real mat(3,3),mata(3,3),matb(3,3),matc(3,3),matd(3,3)
	real val,b
	real dx,dy,dz,sampling
	
	mata(1,1)=cos(rad(psi))
	mata(1,2)=sin(rad(psi))
	mata(1,3)=0.0

	mata(2,1)=-sin(rad(psi))
	mata(2,2)=cos(rad(psi))
	mata(2,3)=0.0

	mata(3,1)=0.0
	mata(3,2)=0.0
	mata(3,3)=1.0

	matb(1,1)=cos(rad(theta))
	matb(1,2)=0.0
	matb(1,3)=-sin(rad(theta))

	matb(2,1)=0.0
	matb(2,2)=1.0
	matb(2,3)=0.0

	matb(3,1)=sin(rad(theta))
	matb(3,2)=0.0
	matb(3,3)=cos(rad(theta))

	matc(1,1)=cos(rad(phi))
	matc(1,2)=sin(rad(phi))
	matc(1,3)=0.0

	matc(2,1)=-sin(rad(phi))
	matc(2,2)=cos(rad(phi))
	matc(2,3)=0.0

	matc(3,1)=0.0
	matc(3,2)=0.0
	matc(3,3)=1.0

	call matmult(matb,matc,mat)
	call matmult(mata,mat,matd)
	
	do a = 1,numatoms
	
		x =coords(a,1)
		y =coords(a,2)	
		z =coords(a,3)
		val = 	coords(a,4)
			
		call matmult2(x,y,z,matd,cx,cy,cz)

		sobj(a,1) = x+dx
		sobj(a,2) = y+dy
		sobj(a,3) = z+dz
		sobj(a,4) = val		
	enddo	
		
	return
	end


      subroutine interpo(sx,sy,sz,val)
!C bilinear ip of coords
	use image_arrays
	real sx,sy,sz,val
	integer x,y,z,ix,iy,iz
	real wx,wy,wz,nwx,nwy,nwz
	
	ix = int(sx)
	iy = int(sy)
	iz = int(sz)

!C	print*,sx,sy,sz,sampling
!C	print*,ix,iy,iz

	wx = sx - float(ix)
	wy = sy - float(iy)
	wz = sz - float(iz)

	nwx = 1.0-wx
	nwy = 1.0-wy
 	nwz = 1.0-wz

          val = nwx*nwy*nwz*map(ix,iy,iz) +                 &  
     &         nwx*nwy*wz*map(ix,iy,iz+1) +                 &  
     &	      nwx* wy*nwz*map(ix,iy+1,iz) +                 &  
     &         nwx* wy* wz*map(ix,iy+1,iz+1) +                 &  
     & 	       wx*nwy*nwz*map(ix+1,iy,iz) +                 &  
     &          wx*nwy* wz*map(ix+1,iy,iz+1) +                 &  
     &	       wx* wy*nwz*map(ix+1,iy+1,iz) +                 &  
     &          wx* wy* wz*map(ix+1,iy+1,iz+1)


        return
        end
		
        subroutine matmult(mat1,mat2,mat)
!C multiply 2 matrices together
	real mat(3,3),mat1(3,3),mat2(3,3)		
	
	mat=matmul(mat1,mat2)
		
	return 
	end

	subroutine matmult2(x,y,z,mat,cx,cy,cz)
!c mult coords by transformation mat


	real mat(3,3),cx,cy,cz,x,y,z
	real coords(3),newcoords(3)

	coords(1) = x-cx
	coords(2) = y-cy
	coords(3) = z-cz
	
	newcoords=matmul(mat,coords)
	
	x = newcoords(1) + cx
	y = newcoords(2) + cy
	z = newcoords(3) + cz
		
 	return
	end

     

        subroutine interpo2(map,nx,ny,nz,sx,sy,sz,val)
!C bilinear ip of coords
! add wraparound, dec 2002.

	integer nx,ny,nz
	real map(0:nx+1,0:ny-1,0:nz-1)
        real sx,sy,sz,val
        integer x,y,z,ix,iy,iz,ixp1,iyp1,izp1,nxm1,nym1,nzm1
        real wx,wy,wz,nwx,nwy,nwz

        ix = int(sx)
        iy = int(sy)
        iz = int(sz)

	nxm1=nx-1
	nym1=ny-1
	nzm1=nz-1
	
	ixp1=ix+1
	iyp1=iy+1
	izp1=iz+1
	
        wx = sx - float(ix)
        wy = sy - float(iy)
        wz = sz - float(iz)

        nwx = 1.0-wx
        nwy = 1.0-wy
        nwz = 1.0-wz
              
        do while ((ix.ge.nxm1) .or. (ix.lt.0))
                if (ix.ge.nxm1) ix=ix-nxm1
                if (ix.lt.0) ix=ix+nx
                ixp1=ix+1
                if (ixp1.ge.nxm1) ixp1=ixp1-nxm1
        enddo
        
        
        do while ((iy.ge.nym1) .or. (iy.lt.0))
                if (iy.ge.nym1) iy=iy-nym1
                if (iy.lt.0) iy=iy+ny
                iyp1=iy+1
                if (iyp1.ge.nym1) iyp1=iyp1-nym1
        enddo
        
        do while ((iz.ge.nzm1) .or. (iz.lt.0))  
                if (iz.ge.nzm1) iz=iz-nzm1
                if (iz.lt.0) iz=iz+nz
                izp1=iz+1
                if (izp1.ge.nzm1) izp1=izp1-nzm1
         enddo
      

        map(ix,iy,iz) = nwx*nwy*nwz*val +  map(ix,iy,iz)
        map(ix,iy,izp1) = nwx*nwy* wz*val + map(ix,iy,izp1)
        map(ix,iyp1,iz) = nwx* wy*nwz*val +map(ix,iyp1,iz)
        map(ix,iyp1,izp1) = nwx* wy* wz*val + map(ix,iyp1,izp1)
        map(ixp1,iy,iz) =   wx*nwy*nwz*val +  map(ixp1,iy,iz)
        map(ixp1,iy,izp1) =wx*nwy* wz*val + map(ixp1,iy,izp1)
        map(ixp1,iyp1,iz) = wx* wy*nwz*val +  map(ixp1,iyp1,iz)
        map(ixp1,iyp1,izp1) =   wx* wy* wz*val +map(ixp1,iyp1,izp1)
     
 
        return
        end subroutine interpo2



!C ************************************************
        subroutine readpdb(pdbfile,numatoms)
        Use coordinates
        use image_arrays
        character*2 element,charge
        character*81 line
        character*40 pdbfile
        character*6 head
        character*4  id
        character*6 space1a
        character*14 space1b
        character*10  space2
        real x,y,z,occupancy,temperature,vatom
        integer count,numatoms

        open(1,file=pdbfile,status='old',err=777)

        count = 0
    

!C*******READ IN ATOMS

40      continue
        read(1,20,err=777,end=777) line
       
20      format(A80)
        if (line(1:4).eq.'ATOM' ) THEN
          read(line,501) head,space1a,id,space1b,x,y,z,occupancy,temperature,space2,element,charge
501        FORMAT(A6,A6,a4,a14,3F8.3,2F6.2,A10,A2,A2)
          count = count+1
          write(6,501) head,space1a,id,space1b,x,y,z,occupancy,temperature,space2,element
       		
       		if (element.eq.'  ') then 
       					element=id(1:2)
       					print*, 'Read pdb in ID mode ',id
       				     else
       					print*, 'Read pdb in element mode ',element
       		endif
       		
		select case (element)
		
		
		case  (' H')
			 vatom = 1
		case  (' C')
			 vatom = 6	 
		case  (' N')
			 vatom = 7			 
		case  (' O')
			 vatom = 8
		case  (' S')
			 vatom = 16	 
		case  (' P')
			 vatom = 15				 			 
		case  ('Se')
			 vatom = 16			 
		case  ('Mg')
			 vatom = 12
		case  (' K')
			 vatom = 19
		case  ('Mn')
			 vatom = 25
		case  ('Na')
			 vatom = 11	 
		
						 				 			
		case default
			vatom = 0
			print*,'Unrecognised atom ',element,' encountered.'
			goto 666
		end select

        
                coords(count,1) = x
                coords(count,2) = y
                coords(count,3) = z
                coords(count,4) = vatom                     


        endif
666     continue       

        if (line(1:3).ne.'END') goto 40
777     continue
        if (count.le.0)  STOP 'Error reading pdb file.'
        numatoms = count
        close(1)
        return
        end        
        
        
                
!C ************************************************

        subroutine writepdb(pdbfile,outfile2,numatoms,sampling)
        Use coordinates
        use image_arrays
        character*2 element,charge
        character*81 line
        character*40 pdbfile,outfile2
        character*6 head
        character*6  atom
        character*24 space1
        character*10  space2
        real x,y,z,occupancy,temperature,vatom
        integer count,numatoms

        open(1,file=pdbfile,status='old',err=777)
  	open(2,file=outfile2,status='unknown',err=777)

        count = 0
    

!C*******READ IN ATOMS

40      CONTINUE
        READ(1,20,err=777,end=777) LINE
       
20      format(A80)
        if(line(1:4).eq.'ATOM' ) then
          read(line,501)head,space1,X,Y,Z,occupancy,temperature,space2,element,charge
501       FORMAT(A6,A24,3F8.3,2F6.2,A10,A2,A2)
          count = count+1
          write(6,501) head,space1,X,Y,Z,occupancy,temperature,space2,element
       		
       
	
        
		x=sobj(count,1)
 		y=sobj(count,2)
		z=sobj(count,3)
	
		write(2,501) head,space1,X,Y,Z,OCCUPANCY,TEMPERATURE,space2,element
          	

	else
		write(2,20) line

        endif
666     continue       

        goto 40
777     continue
        numatoms = count
        CLOSE(1)
        close(2)
        return
        end        
        


!C ************************************************

        function num(number)
        character(len=3) num
  
        
        integer number,order
        real fnum,a
        integer hun,ten,units
        character*1 digit(0:9)

        digit(0)='0'
        digit(1)='1'
        digit(2)='2'
        digit(3)='3'
        digit(4)='4'
        digit(5)='5'
        digit(6)='6'
        digit(7)='7'
        digit(8)='8'
        digit(9)='9'

        fnum=float(number)
        fnum=mod(fnum,1000.)
        hun=int(fnum/100.)
        a=mod(fnum,100.)
        ten=int(a/10.)
        units=mod(fnum,10.)
!C       print *,hun,ten,units


        num=digit(hun)//digit(ten)//digit(units)  
        return
        end function
       

           
        real function degrees(val)
        real val,pi
        parameter (pi=3.1415927)

        degrees=val*180/pi

        return
        end


        real function rad(val)
        real val,pi
        parameter (pi=3.1415927)

        rad=val*pi/180.

        return
        end

        
         
         
   	subroutine spirot2(pdbfile,outfile2,psi,theta,phi,dx,dy,dz,sampling,nx,ny,nz)
!C spi-rot
!C A.M. Roseman

!C rotation applied 
!C in spider euler angles format, psi,theta,phi
!C psi =0, psi range defined in this program


!C        pdbfile - coordinates

!C output: outfile, coordinates


        Use coordinates

!C sampling for position search, in voxels
      real sampling,step
      real domega
    
        
    
      real psi,theta,phi
      real mat(3,3)
      real max,val
   

      
        character*40 pdbfile
        character*40 outfile2
	integer a,b,bcount,x,y,z,angle
	real j1,j2,j3,cx,cy,cz
	integer ji1,ji2
	integer comx,comy,comz
	integer numangles,numatoms,count
	integer ma,mb,mx,my,mz,mbcount
	real sx,sy,sz
	integer nx,ny,nz
        
        real*8 mean,tot,c
        integer  counter

	real dx,dy,dz

      write(6,*) 'spi-rot'
      write(6,*) '======='
      write(6,*) 'apply rotation about com of coordinates'


	call readpdb(pdbfile,numatoms)

199	write (6,*) numatoms, ' atoms read from pdbfile: ',pdbfile





!C ***************************************************
	cx=int(nx/2.)
	cy=int(ny/2.)
	cz=int(nz/2.)

!C 4. calc com of coords
   !   call com (cx,cy,cz,numatoms)
   
   !   shift =0.5*sampling
      
! the shift applied is the transformation because the O and DockEM coordinate systems are
! slighlty different.
      
 !	cx=cx-shift
 !	cy=cy-shift
 !	cz=cz-shift
 
 	cx=cx*sampling
 	cy=cy*sampling
 	cz=cz*sampling
	print*,'COM of coodinates is ',cx,cy,cz,' angstoms.'


!C *********************

!C 5 read the angles

      write (6,*) 'Euler angles (in "spider" convention) for the transformation, psi, theta, phi,  '
      print*, psi,theta,phi  
      write (6,*) 'Shift in x,y and z, in angstoms '
      print*,dx,dy,dz

!C 	apply the rot
		
        call rot(cx,cy,cz,psi,theta,phi,numatoms,sampling,dx,dy,dz)
	print*,'finrot'

!C ******************************

!C 7. write coords of tophit
!	open (2,file=outfile2,status='unknown')


        call writepdb(pdbfile,outfile2,numatoms,sampling)
	print*, numatoms,' atoms written to pdbfile ',outfile2

	goto 999



902     continue  
!C err reading angles
	print*, 'error reading angles file.'
	stop
903  	continue
!Cerr reading map
	print*,'error reading EM-map.'
        stop

999    continue
	return
       end
      
 
!C***************************************************************************

        integer function next_ft_size(x)
        
        logical ftsize
        integer x
        real t

	t=float(x)
        if (int(t/2).ne.(t/2)) x=x+1
        
        do while (.not.ftsize(x))
               
                x=x+2
        enddo
        next_ft_size=x
        return

        end function next_ft_size

!C***************************************************************************
        logical function ftsize(x)
        integer x,n,a

        real b
        real primes(8)
        primes=(/2,3,5,7,11,13,17,19/)
!         primes=(/2,3/)
! 3dtft has trouble with some higher primes. 2/2003

        ftsize = .false.
        
        a=x
        do n=1,8
100     continue
        b=float(a)/primes(n)
       
        if (a.eq.0) stop 'error in primes'
        if (b.eq.int(b)) then
                                a=int(b)
                                goto 100
        endif
        enddo

        if (a.eq.1) then 
                                ftsize=.true.
                                else
                               ftsize=.false.
        endif 

        return

        end function ftsize

!C***************************************************************************


