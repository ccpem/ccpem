#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import sys
import os
import json
from ccpem_progs.flex_em.CG import opt_cg
from ccpem_progs.flex_em.MD import opt_md
from ccpem_core import ccpem_utils


class FlexEM(object):
    '''
    Wrapper to run Flex-EM
    '''
    def __init__(self, args):
        super(FlexEM, self).__init__()
        self.args = ccpem_utils.convert_unicode_to_string(args)

        # Print header and args
        ccpem_utils.print_header(message='Flex-EM')
        ccpem_utils.print_sub_header(message='Arguments')
        ccpem_utils.print_dictionary(dictionary=self.args)
        self.runFlexEM()

    def runFlexEM(self):
        optimization = self.args['optimization']
        input_pdb_file = self.args['input_pdb_file']
        code = self.args['code']
        em_map_file = self.args['em_map_file']
        map_format = self.args['map_format']
        apix = round(float(self.args['apix']),2)
        resolution = float(self.args['resolution'])
        x = round(float(self.args['x']),3)
        y = round(float(self.args['y']),3)
        z = round(float(self.args['z']),3)
        path = self.args['path']
        init_dir = int(self.args['init_dir'])
        num_of_iter = int(self.args['num_of_iter'])
        rigid_filename = self.args['rigid_filename']
        projectdirectory = self.args['job_location']
        test_mode = self.args['test_mode']
        max_atom_disp = self.args['max_atom_disp']
        phi_psi_retraints = self.args['phi_psi_restraints']
        distance_restraints = self.args['distance_restraints']
        max_distance = self.args['max_distance']
        density_weight = self.args['density_weight']
        keyword_jsonfile = self.args['keyword_json']
        # Set working directory
        self.scratch = os.path.join(projectdirectory,
                                    str(init_dir) + '_' + optimization)
        if not os.path.exists(self.scratch):
            os.makedirs(self.scratch)
        os.chdir(self.scratch)
        
        if keyword_jsonfile is not None and os.path.isfile(keyword_jsonfile):
            with open(keyword_jsonfile,'r') as kj:
                dict_keywords = json.load(kj)
        else: dict_keywords = {}
        # Check inputs exist
        assert os.path.exists(em_map_file)
        assert os.path.exists(input_pdb_file)
        assert os.path.exists(self.scratch)

        # CG
        if optimization == 'CG':
            num_of_runs = num_of_iter
            for i in range(init_dir, init_dir+num_of_runs):
                opt_cg(path=path,
                       code=code,
                       run_num=str(i),
                       rand=55*i,
                       em_map_file=em_map_file,
                       input_pdb_file=input_pdb_file,
                       format=map_format,
                       apix=apix,
                       res=resolution,
                       x=x,
                       y=y,
                       z=z,
                       rigid_filename=rigid_filename,
                       phi_psi_retraints=phi_psi_retraints,
                       test_mode=test_mode)
        # MD
        elif optimization == 'MD':
            opt_md(path=path,
                   code=code,
                   run_num=str(init_dir),
                   rand=10*init_dir,
                   em_map_file=em_map_file,
                   input_pdb_file=input_pdb_file,
                   format=map_format,
                   apix=apix,
                   res=resolution,
                   x=x,
                   y=y,
                   z=z,
                   rigid_filename=rigid_filename,
                   num_of_iter=num_of_iter,
                   test_mode=test_mode,
                   cap_shift=max_atom_disp,
                   phi_psi_retraints=phi_psi_retraints,
                   template_distance_restraint=distance_restraints,
                   max_distance=max_distance,
                   dict_add_restraints=dict_keywords,
                   density_weight=density_weight)
        os.chdir(projectdirectory)

if __name__ == '__main__':
    if len(sys.argv) > 1:
        args = json.load(open(sys.argv[1], 'r'))
        FlexEM(args=args)
