#  ============================================================================
#
#  Simulated annealing molecular dynamics optimization with cross-correlation,
#  non-bonded interaction, and stereochemical restraints.
#
#  =======================  Maya Topf, 4 Dec 2007 =============================
#  =======================  Latest update: 6/8/09 =============================

from modeller import *
from modeller.automodel import refine
from modeller.scripts import complete_pdb
from modeller.optimizers import conjugate_gradients, molecular_dynamics, actions
from modeller import schedule
from rigid import load_rigid
from random import *
import os
import json


class opt_md:

    def __init__(self, path, code, run_num, rand, em_map_file, input_pdb_file,
                 format, apix, res, x, y, z, rigid_filename,
                 num_of_iter, cap_shift=0.39, phi_psi_retraints=True,
                 template_distance_restraint=True,max_distance=5.0,
                 dict_add_restraints={},density_weight=0.9,
                 test_mode=False):

        env = environ(rand_seed=-rand)
        env.libs.topology.read(file='$(LIB)/top_heav.lib')
        env.libs.parameters.read(file='$(LIB)/par.lib')
        env.io.hetatm = True

        # Set metadata
        self.metadata = {'em_density': [],
                         'iteration': [],
                         'model_path': []}
        self.metadata_filename = 'metadata.json'

        # Test Model
        if test_mode:
            print '-'*80
            print '\n\nTest mode!\n\n'
            print '-'*80

        log.verbose()

        assert os.path.exists(em_map_file)
        den = density(env, file=em_map_file,
                      em_density_format=format,
                      density_type='GAUSS', voxel_size=apix,
                      resolution=res, px=x, py=y, pz=z)

        env.edat.density = den
        # Repel function non-bonded iteractions
        env.edat.dynamic_sphere = True

        # read pdb
        aln = alignment(env)
        mdl2 = model(env, file=input_pdb_file)

        aln.append_model(mdl2,
                         align_codes=input_pdb_file,
                         atom_files=input_pdb_file)
        mdl = model(env, file=input_pdb_file)
        aln.append_model(mdl, align_codes=code, atom_files=code)
        aln.align(gap_penalties_1d=(-600, -400))
        mdl.clear_topology()
        mdl.generate_topology(aln[input_pdb_file])
        mdl.transfer_xyz(aln)
        mdl.build(initialize_xyz=False, build_method='INTERNAL_COORDINATES')
        mdl.res_num_from(mdl2,aln) #added
        ##check number of chains
        nochain = False
#         if len(mdl.chains) == 1:
#             for c in mdl.chains:
#                 c.name = ' '
#             nochain = True
        mdl.write(file=code+'_ini.pdb')
        '''
        # renumber chains
        for c in mdl.chains:
            c.name = ' '
        mdl.write(file=code+'_ini.pdb')
        '''
        # Build restraints
        sel_all = selection(mdl)
        mdl.restraints.make(sel_all,
                            restraint_type='stereo',
                            spline_on_site=False)

        at = mdl.atoms
        # Add as option
        if phi_psi_retraints:
            print '>>Adding template phi/psi restraints'
            mdl.restraints.make(sel_all,
                                aln=aln,
                                restraint_type='phi-psi_binormal',
                                spline_on_site=True,
                                residue_span_range=(0, 99999))
        if template_distance_restraint:
            print '>>Adding template distance restraints'
            mdl.restraints.make_distance(sel_all,
                                         sel_all,
                                aln=aln,
                                restraint_group=physical.xy_distance,#Ca-Ca distance
                                maximal_distance=max_distance,
                                spline_on_site=False,
                                residue_span_range=(0, 99999))
        for kw in dict_add_restraints:
            if kw == 'helix':
                for res_seg in dict_add_restraints[kw]:
                    print '++Adding helix restraint', res_seg
                    try:
                        mdl.restraints.add(
                            secondary_structure.alpha(
                                mdl.residue_range(str(res_seg[0]), str(res_seg[1]))))
                    except IOError: pass
            elif kw == 'strand':
                for res_seg in dict_add_restraints[kw]:
                    print '++Adding strand restraint', res_seg
                    try:
                        mdl.restraints.add(
                            secondary_structure.strand(
                                mdl.residue_range(str(res_seg[0]), str(res_seg[1]))))
                    except IOError: pass
            elif kw == 'distance':
                for res_seg in dict_add_restraints[kw]:
                    print '++Adding distance restraint', res_seg
                    try:
                        mdl.restraints.add(
                            forms.gaussian(
                                group=physical.xy_distance,
                               feature=features.distance(at['CA:'+str(res_seg[0])],
                                                         at['CA:'+str(res_seg[1])]),
                               mean=res_seg[2], stdev=0.1))
                    except IOError: pass
        # define rigid bodies
        sel_rigid = []
        rand_rigid = []
        load_rigid('', mdl, sel_rigid, rand_rigid, rigid_filename,nochain)
        for n in sel_rigid:
            mdl.restraints.rigid_bodies.append(rigid_body(n))

        mdl.restraints.write(file=code+'.rsr')

        a = 0
        sel_list = []
        for n in sel_all:
            for m in sel_rigid:
                if n in m:
                    a = 1
                    break
                else:
                    a = 0
            if a == 0:
                sel_list.append(n)
        sel_flex = selection(sel_list)

        # print selections
        print sel_all
        print sel_rigid
        print sel_flex

        # start simulated annealing molecular dynamics
        print "MD annealing"
        scal = physical.values(default=1.0, em_density=int(10000*density_weight))

        cap = cap_shift
        timestep = 5.0
        icount = 0

        MD = molecular_dynamics(cap_atom_shift=cap, md_time_step=timestep,
                                md_return='FINAL', output='REPORT',
                                schedule_scale=scal)

        trc_file = open('MD'+run_num+'.trc', "a")

        # Get initial energies
        scal = physical.values(default=0.0, em_density=1.0)
        (molpdf, terms) = sel_all.energy(schedule_scale=scal)
        print "START EM score= %.3f" %(-molpdf)
        self.metadata['em_density'].append(-molpdf)
        self.metadata['iteration'].append(0)
        self.metadata['model_path'].append(os.path.abspath(input_pdb_file))
        self.metadata_output()

        CG = conjugate_gradients()
        # Start iterations loop
        a = 1
        b = num_of_iter + 1
        while a < b:

            # heating the system
            if test_mode:
                equil_its = 1
                equil_equil = 1
                equil_temps = [150.0]
                trc_step = 1
            else:
                equil_its = 100
                equil_equil = 20
                equil_temps = (150.0, 250.0, 500.0, 1000.0)
                trc_step = 5
            init_vel = True
            #
            for (its, equil, temps) in [(equil_its, equil_equil, equil_temps)]:
                for temp in temps:
                    MD.optimize(sel_all,
                                max_iterations=its,
                                temperature=temp,
                                init_velocities=init_vel,
                                equilibrate=equil,
                                actions=[actions.trace(trc_step, trc_file)])

                    scal = physical.values(default=0.0, em_density=1.0)
                    (molpdf, terms) = sel_all.energy(schedule_scale=scal)
                    print "iteration number= %s  step= %d %d  temp= %d  EM score= %.3f" %(a,icount,its,int(temp),-molpdf)
                    scal = physical.values(default=1.0, em_density=int(10000*density_weight))
                    icount = icount+equil_its
                init_vel = False

            # Cooling the system
            if test_mode:
                equil_its = 1
                equil_temps = [50.0]
            else:
                equil_its = 200
                equil_temps = (800.0, 500.0, 250.0, 150.0, 50.0, 0.0)

            MD = molecular_dynamics(cap_atom_shift=cap, md_time_step=timestep,
                                    md_return='FINAL', output='REPORT',
                                    schedule_scale=scal)

            for (its, equil, temps) in [(equil_its, equil_equil, equil_temps)]:
                for temp in temps:

                    MD.optimize(sel_all,
                                max_iterations=its,
                                temperature=temp,
                                init_velocities=init_vel,
                                equilibrate=equil,
                                actions=[actions.trace(trc_step, trc_file)])

                    scal = physical.values(default=0.0, em_density=1.0)
                    (molpdf, terms) = sel_all.energy(schedule_scale=scal)
                    print "iteration number= %s step= %d %d   temp= %d    EM score= %.3f" %(a,icount,its,int(temp),-molpdf)
                    scal = physical.values(default=1.0, em_density=int(10000*density_weight))
                    icount = icount+equil_its

            #CG refinement
            scal = physical.values(default=1.0, em_density=100.0)
            CG.optimize(sel_all, output='REPORT', 
                        max_iterations=100,
                        schedule_scale=scal)

            filename = 'md'+run_num+'_'+str(a)+'.pdb'
            sel_all.write(file=filename)

            # Get iteration energies
            scal = physical.values(default=0.0, em_density=1.0)
            (molpdf, terms) = sel_all.energy(schedule_scale=scal)

            # Write metadata
            self.metadata['em_density'].append(-molpdf)
            self.metadata['iteration'].append(a)
            filepath = os.path.abspath(filename)
            self.metadata['model_path'].append(filepath)
            self.metadata_output()

            a += 1

        trc_file.close()

        print "MD step %d: energy all (scaling 1:10000)" % icount
        eval = sel_all.energy(schedule_scale=scal)

        # Final minimization with and without CC restraints
        print " final conjugate_gradients"

        # Test mode
        if test_mode:
            max_it = 5
        else:
            max_it = 100

        scal = physical.values(default=1.0, em_density=100.0)
        CG.optimize(sel_all,
                    output='REPORT',
                    max_iterations=max_it,
                    schedule_scale=scal)
        eval = sel_all.energy(schedule_scale=scal)

        sel_all.write(file='final'+run_num+'_mdcg.pdb')

        os.system("rm -f *.MRC")

    def metadata_output(self):
        '''
        Metadata output
        '''
        json.dump(self.metadata,
                  open(self.metadata_filename, 'w'),
                  indent=4,
                  separators=(',', ': '))
