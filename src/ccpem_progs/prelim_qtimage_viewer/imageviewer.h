#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QMainWindow>
//#include <QImage>
#include <QtGui/QStandardItemModel>
#include <QtCore/QMap>
#include <iostream>
#include <QWidget>
#include <QLabel>
#ifndef QT_NO_PRINTER
#include <QPrinter>

#include <QtGui>
#include<QSlider>
#include <QGroupBox>


#endif

extern int imageCounter;

QT_BEGIN_NAMESPACE
class QAction;
class QLabel;
class QMenu;
class QScrollArea;
class QScrollBar;
class QString;
class QCheckBox;
class QComboBox;
class QGroupBox;
class QLabel;
class QSpinBox;
class QStackedWidget;


QT_END_NAMESPACE









class ImageViewer : public QMainWindow{
    Q_OBJECT

public:
   ImageViewer();



signals:
    void valueChanged(int value);




private slots:
    void open();
    void print();
    void zoomIn();
    void zoomOut();
    void normalSize();
    void fitToWindow();
    void nextImage();
    void previousImage();
    void info();
    void makeOpaque();
    void makeTransluscent();
   //void paintEvent();
    //void applyGreenAndTransparent(double factor);
    //void transluscentImage(int otSwitch);
    void changeOpacity(int otSwitch, int transparencySwitch);
   void valueHasChanged();

private:
    void createActions();
    void createMenus();
    void updateActions();
    void scaleImage(double factor);
    void adjustScrollBar(QScrollBar *scrollBar, double factor);
    void loadDataFromFile();
    void createModel();
    void ouputWindow();
    void drawMarker(int imageCounter);
    void createTable(int imageCounter);
    void createControls(const QString &title);
    void controlPanel();

    QStandardItemModel *model;
    QMap<int,int> freqMap;
   // QMap<int,int,int,int,int> tumourTable;

    QWidget m_window;
    QWidget window;
    QLabel *imageLabel;
    QLabel *l_img;
    QScrollArea *scrollArea;
    QPainter *painter1;
    QPainter *painter2;
    QPixmap *img;
    QPixmap *overlay;
    QPixmap *result;


    QString *string;
    QString *tmpStr1;
    double scaleFactor;
    int imageCounter;
    double opacityValue;
    int CMLT;
    int RMLT;
    QString fileName;
    QString fileName2;
    QImage TopMammogram;
    QImage BottomMammogram;

    QLabel *label;
    QLabel *label1;
    QImage *ash;
    QImage *ash5;





protected:
    void paintEvent(QPaintEvent *event);



#ifndef QT_NO_PRINTER
    QPrinter printer;
#endif

    QAction *openAct;
    QAction *printAct;
    QAction *exitAct;
    QAction *zoomInAct;
    QAction *zoomOutAct;
    QAction *normalSizeAct;
    QAction *fitToWindowAct;
    QAction *infoAct;
    QAction *infoQtAct;
    QAction *nextImageAct;
    QAction *previousImageAct;
    QAction *makeOpaqueAct;
    QAction *makeTransluscentAct;
    QMenu *fileMenu;
    QMenu *viewMenu;
    QMenu *helpMenu;
    QMenu *aboutMenu;
    QMenu *mammogramMenu;

};





#endif

//***********************************************************************************************





//#include "ui_mainwindow.h"

//class MainWindow : public QMainWindow, private Ui_MainWindow
//{
 //   public:
//        MainWindow( QWidget *parent = 0, Qt::WindowFlags f = 0 );
 //       ~MainWindow();

//    private:
//        QStandardItemModel *model;
//        QMap<int,int> freqMap;

//    private:
//        void loadDataFromFile();
//        void createModel();
 //       void updateTextEdit();
//};

//#endif // MAINWINDOW_H



















