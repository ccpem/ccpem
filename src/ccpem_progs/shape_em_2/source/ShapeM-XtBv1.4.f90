                                                                                          
!=******************************************************************************
!=* ShapeM - Programs                                     AUTHOR: A.ROSEMAN    *
!=*                                                                            *
!=*                                                                            *
!=* ShapeM is a set of software programs to match 3D density object shapes.    *
!=* Specifically, it has been designed to locate known molecular shapes within *
!=* molecular densities of larger complexes determined by cryoEM. It was       *
                                                                                  
!=******************************************************************************
!=* ShapeM - Programs                                     AUTHOR: A.ROSEMAN    *
!=*                                                                            *
!=*                                                                            *
!=* ShapeM is a set of software programs to match 3D density object shapes.    *
!=* Specifically, it has been designed to locate known molecular shapes within *
!=* molecular densities of larger complexes determined by cryoEM. It was       *
!=******************************************************************************
!=* ShapeM - Programs              		          AUTHOR: A.ROSEMAN    *
!=*                                    		                               *
!=*									       *
!=* ShapeM is a set of software programs to match 3D density object shapes.    *
!=* Specifically, it has been designed to locate known molecular shapes within *
!=* molecular densities of larger complexes determined by cryoEM. It was       *
!=* written by Alan Roseman at the University of Manchester, 2013. It is based *
!=* on earlier work by Alan Roseman at the MRC-LMB, Cambridge (1998-2007).     *
!=* It uses the same FLCF algorithm to efficiently calculate correlation       *
!=* coefficients over a defined 3D mask boundary around the search object,     *
!=* as in DockEM.							       *
!=*									       *
!=*    Copyright (C) 2013 The University of Manchester 		    	       *
!=*                                                                   	       *
!=*    This program is free software: you can redistribute it and/or modify    *
!=*    it under the terms of the GNU General Public License as published by    *
!=*    the Free Software Foundation, either version 3 of the License, or       *
!=*    (at your option) any later version.                                     *  
!=*        								       *
!=*    This program is distributed in the hope that it will be useful,         *
!=*    but WITHOUT ANY WARRANTY; without even the implied warranty of          *
!=*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
!=*    GNU General Public License for more details.                            *
!=* 									       *
!=*    You should have received a copy of the GNU General Public License       *
!=*    along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
!=* 								               *
!=*    For enquiries contact:						       *
!=*    									       *
!=*    Alan Roseman  							       *
!=*    Faculty of Life Sciences						       *
!=*    University of Manchester						       *
!=*    The Michael Smith Building					       *
!=*    Oxford Road							       *
!=*    Manchester. M13 9PT						       *
!=*    Email:Alan.Roseman@manchester.ac.uk    				       *
!=*									       *
!=*   									       *
!=******************************************************************************
!


!C ShapeM-XtB
! mod to dock densities, therefore not inverse transformation
!mod so no wrapparounds in density rotn

!v1.2 origin to match V1.2 search density prog.
!v1.3 extracted densities will match the sobj

!C input: 
!C       mrc density map file - fit object
!c       mask file
!C       threshold for mask file
!C       run version key.
!C       sampling of the map used for the search
!C	 range of solutions to generate

!C       size of extracted objects.  Keep at the target map size for convenience.
! v1.2 no, now match extracgted objects to size of the seachobject/mask, by default.



!C output: mrc files corresponding to searchdoc keys.
!C  

!V1.2
! window out boxes at centres given in searchdock files.
! apply reverse rotatoins about the centres of these boxes.
! these are the hits then aligned to the original search file.
! note: make the masked ones and not masked ones. Will be useful. 

!v1.3 make extracted object that are exactly aligned to the sobj.



	program ShapeMXtB

  
        Use  mrc_image
        Use coordinates
        
        real sampling
        real psi,theta,phi
        character*40 pdbfile,outfile2,filename,filename2,maskfilename,sobjfilename
        character*24 space1
 	character*6 head
	character*10 space2
	character*2 element,junk
	integer a,bcount,angle
	real j1,j2,j3,cx,cy,cz,x,y,z,b
	integer ji1,ji2,shx,shy,shz
	real comx,comy,comz
	integer numangles,numatoms,count
      	integer runcode
      	character*3 num
      	real dx,dy,dz,maskth
      	
      	
!C sampling for position search, in voxels      		
        integer  counter,counter2,zx,zy,zz,r1,r2,N
	integer nx3,ny3,nz3,lx,ly,lz

	integer size,sizeo2,sizem1,sx,fx,sy,fy,sz,fz
	real, dimension (:,:,:), allocatable :: map, obj, rotobj,mask       
	integer err,angnum
	logical T,F
	integer newxyz(3)
	
	t = .true.
	f=.false.
	

      
      write(6,*) 'ShapeM-XtB, v1.3d'
      write(6,*) '=================='
    
      write(6,*)

	print*,'Enter the EM map filename.'
	read*,filename
	print*, filename

	print*,'Enter the sampling of the map (A/pixel)'
	read*, sampling

!C 1. read coords
      write (6,*) 'Enter the filename of the search object map    '
      read (5,10) sobjfilename
10    format(A40)
      print*, sobjfilename

!	print*,"Enter the mask map filename. (This is mask just for presentation, it won't affect the origin of rotation.)";
!	read*,maskfilename
	
!	print*,'Enter a threshold for the mask map.'
!	read*,maskth
	


      write (6,*) 'Enter a run code, for input/output files.'
      read (5,30) runcode
      print*, runcode
30    format (I3)

       pdbfile = 'com'//num(runcode)//'.pdb'       
! pdb file com used to record origin of rotations.

!      write (6,*) 'Enter the sampling in Angstoms.'
!      read*,sampling

!      write (6,*) 'Enter the filename of the doc file    '
!      read (5,10) filename2
!40    format(A40)
!      print*, filename2

      filename2="searchdoc"//num(runcode)//".dkm"


      write (6,*) 'Enter the range of solutions to extract.'
      read *, r1,r2
      print *, r1,r2


 !     write (6,*) 'Enter a size (in pixels) for the extracted objects.'
 !     read *, size
 !     print*,size
 ! v1.3 .. make match the sobj in size.   

 	print*, pdbfile


!read the com from pdbfile

	open (1,file=trim(pdbfile),status='unknown', err=777)
	
	read(1,501) head,space1,comx,comy,comz,j1,j2,space2,element,junk
   	
501       FORMAT(A6,A24,3F8.3,2F6.2,A10,A2,A2)
      
 !      write(6,501) head,space1,X,Y,Z,occupancy,temperature,space2,element,charge
       		
	close (1)
	

        PRINT*,'COM of sobj at ',comx,comy,comz,' Angstroms.'
	
	comx=comx/sampling
	comy=comy/sampling
	comz=comz/sampling

        PRINT*,'COM of sobj at ',comx,comy,comz,' pixels.'

! calc nearest integer posn of com
! don't really need this. by definition the com will be the centre of the boxes windowed.
! actually this is all not neccessary. wan't objects to line up with the sobj, therefore the centre must be centre of sobj box, not the com,
! this will now be V1.3
! give a warning that sobj must be rotatable within own box, i.e. write out when corners zeroed, to show what could be cut off. 
! apply the corners cuttoff in the search. 
! or..write out a version of the sobj centered by com.


!v1.3 make nearest integer   

	comx=int((comx/1)+.5)
	comy=int((comy/1)+.5)
	comz=int((comz/1)+.5)
	print*,comx,comy,comz
   
!C ***************************************************


!C open docfile

	open(4,file=filename2,status='old')
					
510		format (1x,I8,3F8.2,I8,F8.3,3F10.2,2x,2G12.6)	


!C ***********************************************
!C open input from MRC format file

        
        CALL IMOPEN(2,sobjfilename,'RO')
        CALL IRDHDR(2,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)

	lx=nx
	ly=ny
	lz=nz

        if ((lx.ne.ly).or.(lx.ne.lz))  stop "Search object map must be cubic."
!        CALL IMCLOSE(2)

        
        CALL IMOPEN(1,filename,'RO')
        CALL IRDHDR(1,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)

        nxm1=nx-1
        nym1=ny-1
        nzm1=nz-1
        
        nxp1=nx+1
        nxp2=nx+2
        

!centre of small box, spider convention in mrc type file
        cx=int(float(lx)/2.)
        cy=int(float(ly)/2.)
        cz=int(float(lz)/2.)



!C allocate  image_files 
!        allocate (map(0:nxp1+lx,0:nym1+ly,0:nzm1+lz),mask(0:nxp1,0:nym1,0:nzm1) )    
        allocate (map(0:nxp1,0:nym1,0:nzm1),mask(0:nxp1,0:nym1,0:nzm1) )    
        map=0
       
    !    bnx=nx+lx
     !   bny=ny+ly
!	bnz=nz+lz

        CALL READMRCMAP(1,filename,map,nx,ny,nz,T,F,T,err) !read, is open, dont close, add 2 bytes
        
!C ***************************************************

        sizeo2=int(size+1)/2
	size=sizeo2*2 
	print*,size,sizeo2
	sizem1=size-1
	newxyz(1:3)=size
	

!        CALL IMOPEN(3,maskfilename,'RO')
!        CALL IRDHDR(3,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)

!        CALL READMRCMAP(3,maskfilename,mask,nx,ny,nz,T,F,T,err) !read, not open,  close, add 2 bytes  
     
!make mask binary

! vs maskth
!	where (mask.lt.maskth) 
!			  mask=0.0
!	elsewhere
!			  mask=1.0
!	end where

 !       print*,"Sum in mask = ", sum(mask)

 !       map=map*mask

 !       print*,"Sum in masked map = ", sum(map)

 
         allocate (obj(0:lx+1,0:ly-1,0:lz-1),rotobj(0:lx+1,0:ly-1,0:lz-1))      
         rotobj=1
     
              
	
!C write top set of hits here
	count=0
	
        !        comx=comx+cx
	 !       comy=comy+cy
          !      comz=comz+cz


	do N=1,r2
		count=count+1
	
		read (4,510) ji1,x,y,z,angnum,b,psi,theta,phi,j2,ccc
		 print*,
		 
		shx=x-comx+cx
		shy=y-comy+cy
		shz=z-comz+cz

		if (ji1.gt.r2) cycle
		if (ji1.ge.r1) then
               	       
			sx=int(lx/2.)
			sy=int(ly/2.)
			sz=int(lz/2.)
			fx=int((lx/2.) +0.5)-1
			fy=int((ly/2.) +0.5)-1
			fz=int((lz/2.) +0.5)-1

			print*,shx,sx,fx,shy,sy,fy,shz,sz,fz
			obj=0
		!	obj(0:lx-1,0:ly-1,0:lz-1)=map(shx-sx:shx+fx,shy-sy:shy+fy,shz-sz:shz+fz)
		
   call window(obj,map,lx,ly,lz,nx,ny,nz,0,shx,shy,shz)
   call rotobjI(obj,rotobj,psi,theta,phi,lx,ly,lz,comx,comy,comz)
		! do inverse rotn

!                        rotobj= eoshift(rotobj,shift=shx,boundary=0.0,dim=1)
 !                       rotobj= eoshift(rotobj,shift=shy,boundary=0.0,dim=2)
  !                      rotobj= eoshift(rotobj,shift=shz,boundary=0.0,dim=3)

				         
			CALL IMOPEN(9,'maphit-run'//num(runcode)//'-'//num(ji1)//'.mrc','UNKNOWN')
        		CALL ITRHDR(9,2)
       			CALL WRITEMRCMAP(9,rotobj,lx,ly,lz,sampling,err) !open already, close it, subtract 2 from nx
! call ialsiz ? 
		endif
        
	end do
	print*,count
 
 
	close(4)
							

    goto 999


902     continue  
! err reading angles
        print*, 'error reading angles file.'
        stop
903     continue
!Cerr reading map
        print*,'error reading EM-map.'
        stop
777     continue
        print*,'error writing pdbfile.'
        stop
           
    
999    continue
       print*,'Program finished O.K.'
                 
       
    
      
!C ************************************************


	contains         
                
        SUBROUTINE rotobjI(obj,therotobj,psi,theta,phi,nx,ny,nz,cx,cy,cz)
           

!c sub to apply rotation about centre of map to map object 
!c convert rotations to matrix form
!C spider convention for euler angles to mat
! invert

! the object maps are nxp1,ny,nz in size
!rotn centre is nx/2,ny/2,nz/2
     
        real obj(0:nx+1,0:ny-1,0:nz-1),therotobj(0:nx+1,0:ny-1,0:nz-1)
       
        real psi,phi,theta,x,y,z  ,cx,cy,cz
        integer nx,ny,nz,ix,iy,iz,nxm1,nym1,nzm1,sizeo2
        real rad,deg
        
        real mat(3,3),mata(3,3),matb(3,3),matc(3,3),matd(3,3),mate(3,3)
        real val,b,val2
       
        therotobj=0

        nxm1=nx-1
        nym1=ny-1
        nzm1=nz-1


        mata(1,1)=cos(rad(psi))
        mata(1,2)=sin(rad(psi))
        mata(1,3)=0.0

        mata(2,1)=-sin(rad(psi))
        mata(2,2)=cos(rad(psi))
        mata(2,3)=0.0

        mata(3,1)=0.0
        mata(3,2)=0.0
        mata(3,3)=1.0

        matb(1,1)=cos(rad(theta))
        matb(1,2)=0.0
        matb(1,3)=-sin(rad(theta))

        matb(2,1)=0.0
        matb(2,2)=1.0
        matb(2,3)=0.0

        matb(3,1)=sin(rad(theta))
        matb(3,2)=0.0
        matb(3,3)=cos(rad(theta))

        matc(1,1)=cos(rad(phi))
        matc(1,2)=sin(rad(phi))        
        matc(1,3)=0.0

        matc(2,1)=-sin(rad(phi))
        matc(2,2)=cos(rad(phi))
        matc(2,3)=0.0

        matc(3,1)=0.0
        matc(3,2)=0.0
        matc(3,3)=1.0

        call matmult(matb,matc,mat)
        call matmult(mata,mat,matd)


        mate=transpose(matd) !inverse of rotation is done

        do iz=0,nzm1
                do iy=0,nym1
                        do ix=0,nxm1

                                x=float(ix)
                                y=float(iy)
                                z=float(iz)

                                val2=obj(ix,iy,iz)
                                                             
				 call matmult2(x,y,z,mate,cx,cy,cz)      !transform coords by rotn
			

                                 call interpo3(therotobj,nx,ny,nz,x,y,z,val2)            !interpo density at newposn to new map.
!interpo3 will not wrapparound
  !                       
                        enddo
                enddo
        enddo
       
        return

        END SUBROUTINE rotobjI


        subroutine interpo3(map,nx,ny,nz,sx,sy,sz,val)
!C bilinear ip of coords

!takeout wrapparound 2013

	integer nx,ny,nz
	real map(0:nx+1,0:ny-1,0:nz-1)
        real sx,sy,sz,val
        integer x,y,z,ix,iy,iz,ixp1,iyp1,izp1,nxm1,nym1,nzm1
        real wx,wy,wz,nwx,nwy,nwz

        ix = int(sx)
        iy = int(sy)
        iz = int(sz)

	nxm1=nx-1
	nym1=ny-1
	nzm1=nz-1
	
	ixp1=ix+1
	iyp1=iy+1
	izp1=iz+1
	
        wx = sx - float(ix)
        wy = sy - float(iy)
        wz = sz - float(iz)

        nwx = 1.0-wx
        nwy = 1.0-wy
        nwz = 1.0-wz
              
        if ((ix.ge.nxm1) .or. (ix.lt.0)) return
              
        
        
       if  ((iy.ge.nym1) .or. (iy.lt.0)) return
               
        
        if  ((iz.ge.nzm1) .or. (iz.lt.0))  return
      
      

        map(ix,iy,iz) = nwx*nwy*nwz*val +  map(ix,iy,iz)
        map(ix,iy,izp1) = nwx*nwy* wz*val + map(ix,iy,izp1)
        map(ix,iyp1,iz) = nwx* wy*nwz*val +map(ix,iyp1,iz)
        map(ix,iyp1,izp1) = nwx* wy* wz*val + map(ix,iyp1,izp1)
        map(ixp1,iy,iz) =   wx*nwy*nwz*val +  map(ixp1,iy,iz)
        map(ixp1,iy,izp1) =wx*nwy* wz*val + map(ixp1,iy,izp1)
        map(ixp1,iyp1,iz) = wx* wy*nwz*val +  map(ixp1,iyp1,iz)
        map(ixp1,iyp1,izp1) =   wx* wy* wz*val +map(ixp1,iyp1,izp1)
     
 
        return
        end subroutine interpo3

        
        SUBROUTINE READMRCMAP(stream,filename,map,X,Y,Z,open,close,add2,err)
 
        Use mrc_image

         INTEGER  X,Y,Z,err
         INTEGER  stream, IX,IY,IZ,xm1
         real, DIMENSION (0:X+1,0:Y-1,0:Z-1) :: map  
         INTEGER S(3),S1
         logical add2,open,close
         character*80 filename
            
          if(.not. open) then
                       CALL IMOPEN(stream,filename,'RO')
                       CALL IRDHDR(stream,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
          endif
            
       
!     read in file 

      DO 350 IZ = 0,z-1
          DO 350 IY = 0,y-1
            CALL IRDLIN(stream,ALINE,*998)
            DO 300 IX = 0,x-1       
                map(ix,iy,iz) = ALINE(IX+1)
300         CONTINUE  
        if(add2) then  
                       map(X+0,iy,iz)=0
                       map(X+1,iy,iz)=0
        endif
        
!           print*,ix,iy,iz
350   CONTINUE

        if (close) then
                CALL IMCLOSE(stream)
                print*,'closed ',stream
        endif

      return
998   STOP 'Error on file read.'
     
        END SUBROUTINE READMRCMAP


      SUBROUTINE WRITEMRCMAP(stream,map,x,y,z,sampling,err)
      Use  mrc_image
      REAL val,sampling
      
      integer x,y,z,err,stream,ix,iy,iz,newxyz(3)
      real map(0:x+1,0:y-1,0:z-1)

      print*,'write_mrcimage'         
	newxyz(1)=x
	newxyz(2)=y
	newxyz(3)=z
	
        CALL IALSIZ(STREAM,newxyz,NXYZST)  
        
      print*,NX,NY,NZ  
!     write file 

      DMIN =  1.E10
      DMAX = -1.E10
      DOUBLMEAN = 0.0

      DO 450 IZ = 0,Z-1
      DO 450 IY = 0,Y-1
            
            DO 400 IX = 0,X-1
     
                val = map(IX,IY,IZ)
                ALINE(IX+1) = val
                
                DOUBLMEAN = DOUBLMEAN + val         
                IF (val .LT. DMIN) DMIN = val
                IF (val .GT. DMAX) DMAX = val

400         CONTINUE   

      CALL IWRLIN(stream,ALINE)
450   CONTINUE
      DMEAN = DOUBLMEAN/(NX*NY*NZ)

      cell(1) = sampling *X
      cell(2) = sampling *Y
      cell(3) = sampling *Z
      cell(4) = 90
      cell(5) = 90
      cell(6) = 90

      CALL IALCEL(STREAM,CELL)  
    
      
      CALL IWRHDR(stream,TITLE,-1,DMIN,DMAX,DMEAN)
      CALL IMCLOSE(stream)
      
      return
999   STOP 'Error on file write.'
 
      END SUBROUTINE WRITEMRCMAP


      SUBROUTINE WRITEMRCMAP2(stream,map,x,y,z,sampling,err)
      Use  mrc_image
      REAL val,sampling
      
      integer x,y,z,err,stream,ix,iy,iz,newxyz(3)
      real map(0:x+1,0:y-1,0:z-1)

      print*,'write_mrcimage'         
	newxyz(1)=x
	newxyz(2)=y
	newxyz(3)=z
	
        CALL IALSIZ(STREAM,newxyz,NXYZST)  
        
      print*,NX,NY,NZ  
!     write file 

      DMIN =  1.E10
      DMAX = -1.E10
      DOUBLMEAN = 0.0

      DO 1450 IZ = 0,Z-1
      DO 1450 IY = 0,Y-1
            
            DO 1400 IX = 0,X-1
     
                val = map(IX,IY,IZ)
                ALINE(IX+1) = val
                
                DOUBLMEAN = DOUBLMEAN + val         
                IF (val .LT. DMIN) DMIN = val
                IF (val .GT. DMAX) DMAX = val

1400         CONTINUE   

      CALL IWRLIN(stream,ALINE)
1450   CONTINUE
      DMEAN = DOUBLMEAN/(NX*NY*NZ)

      cell(1) = sampling *X
      cell(2) = sampling *Y
      cell(3) = sampling *Z
      cell(4) = 90
      cell(5) = 90
      cell(6) = 90

      CALL IALCEL(STREAM,CELL)  
    
      
      CALL IWRHDR(stream,TITLE,-1,DMIN,DMAX,DMEAN)
      CALL IMCLOSE(stream)
      
      return
1999   STOP 'Error on file write.'
 
      END SUBROUTINE WRITEMRCMAP2
 
      subroutine window(smallmap,bigmap,lx,ly,lz,nx,ny,nz,border,cx,cy,cz)
       integer lx,ly,lz,nx,ny,nz,border
       integer cx,cy,cz
       integer sx,fx,sy,fy,sz,fz
       real smallmap(0:lx+1,0:ly-1,0:lz-1)
       real bigmap(0:nx+1,0:ny-1,0:nz-1)
       
       smallmap=0
       
       sx=int(cx-int(lx/2.))
       fx=int(cx+(int(lx/2.+0.5))-1)
       sy=int(cy-int(ly/2.))
       fy=int(cy+int(ly/2. +0.5)-1)
       sz=int(cz-int(lz/2.))
       fz=int(cz+int(lz/2.+0.5)-1)
         
	px=0
	py=0
	pz=0
	qx=lx-1	
	qy=ly-1	
	qz=lz-1

      

       if ((sx.lt.0)) then  
       			print*,'Warning: Region exceeds map boundary'
			px=-sx
       			sx=0
       endif
       			
       if ((fx.gt.nx-1)) then 
       			 print*,'Warning: Region exceeds map boundary'
			 qx=(lx-(fx-(nx-1)))-1
       			 fx=nx-1
       endif
       
           
       if ((sy.lt.0)) then  
       			print*,'Warning: Region exceeds map boundary'
			py=-sy       			
			sy=0
       endif
       			
       if ((fy.gt.ny-1)) then 
       			 print*,'Warning: Region exceeds map boundary'
			 qy=(ly-(fy-(ny-1)))-1
       			 fy=ny-1
       endif
           
       if ((sz.lt.0)) then  
       			print*,'Warning: Region exceeds map boundary'
			pz=-sz       			
			sz=0
       endif
       			
       if ((fz.gt.nz-1)) then 
       			 print*,'Warning: Region exceeds map boundary'
			qz=(lz-(fz-(nz-1)))-1       			 
			fz=nz-1
       endif
    

       smallmap(px:qx,py:qy,pz:qz)=bigmap(sx:fx,sy:fy,sz:fz)
       print*,'window:',sx,fx,sy,fy,sz,fz
   	smallmap(lx:lx+1,:,:)=0
   
 !      smallmap(0:border-1,:,:)=0
  !     smallmap(lx-border:lx+1,:,:)=0
       
   !    smallmap(:,0:border-1,:)=0
    !   smallmap(:,ly-border:ly-1,:)=0
       
  !     smallmap(:,:,0:border-1)=0
   !    smallmap(:,:,lz-border:lz-1)=0
     
       end subroutine window      


  end program ShapeMXtB

         
         
 

! to do still : generalise windowing to avoid edge problems, use one of the window subroutines.

!put in invert rotn
