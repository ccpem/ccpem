!=******************************************************************************
!=* ShapeM - Programs              		          AUTHOR: A.ROSEMAN    *
!=*                                    		                               *
!=*									       *
!=* ShapeM is a set of software programs to match 3D density object shapes.    *
!=* Specifically, it has been designed to locate known molecular shapes within *
!=* molecular densities of larger complexes determined by cryoEM. It was       *
!=* written by Alan Roseman at the University of Manchester, 2013. It is based *
!=* on earlier work by Alan Roseman at the MRC-LMB, Cambridge (1998-2007).     *
!=* It uses the same FLCF algorithm to efficiently calculate correlation       *
!=* coefficients over a defined 3D mask boundary around the search object,     *
!=* as in DockEM.							       *
!=*									       *
!=*    Copyright (C) 2013 The University of Manchester 		    	       *
!=*                                                                   	       *
!=*    This program is free software: you can redistribute it and/or modify    *
!=*    it under the terms of the GNU General Public License as published by    *
!=*    the Free Software Foundation, either version 3 of the License, or       *
!=*    (at your option) any later version.                                     *  
!=*        								       *
!=*    This program is distributed in the hope that it will be useful,         *
!=*    but WITHOUT ANY WARRANTY; without even the implied warranty of          *
!=*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
!=*    GNU General Public License for more details.                            *
!=* 									       *
!=*    You should have received a copy of the GNU General Public License       *
!=*    along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
!=* 								               *
!=*    For enquiries contact:						       *
!=*    									       *
!=*    Alan Roseman  							       *
!=*    Faculty of Life Sciences						       *
!=*    University of Manchester						       *
!=*    The Michael Smith Building					       *
!=*    Oxford Road							       *
!=*    Manchester. M13 9PT						       *
!=*    Email:Alan.Roseman@manchester.ac.uk    				       *
!=*									       *
!=*   									       *
!=******************************************************************************
!
!

!XtA, extracts the peaks from the map files produced by ShapeM
! Output is a searchdock file, like the old DockEM produced.

! 1. read map,cccmap, psimap, phimap, thetamap
! 2. find peaks, mask neighbours, loop
! 3. output: searchdocNNN.dkm

!C input: 
!C       Three digit run version key.


       Module image_arrays2
               real, dimension (:,:,:), allocatable ::  rotsobj, rotmask,aa
                real, dimension (:,:,:), allocatable :: lcf, cnv1, cnv2,corr1,v
        End Module image_arrays2
      

        program ShapeMXtA
  
  
  	use coordinates
        Use  mrc_image
        real, dimension (:,:,:), allocatable :: cccmaxmap,cccmaxmap1,psimap,phimap,thetamap,mask
     
     
        real sampling,domega,omst
        real psi,theta,phi,b,val,blot(999999,3)

        integer runcode,err
        integer nm,i,bx,by,bz
        integer a,numangles,ji1,ji2,angnum
        integer counter,counter2,acount,bcount
	integer count,comix,comiy,comiz,shx,shy,shz,numatoms

        character*80 filename,sobjfilename,maskfilename,anglesfile,junk,pdbfile
        character*24 space1
        character*6 head
        character*10 space2
        character*2 element,j3

        logical t,f
        real th,sx,sy,sz,j1,j2,peakrad,maskth,peakr,r
        integer vec(3),x,y,z,c
	real comx,comy,comz
	real mean,sd
	real area(3,3,3),val2,ipx,ipy,ipz
	integer ipeakr,n
	
        t=.true.
        f=.false.
        blot=0

      print*, 'ShapeM-XtA'
      print*, '=========='


        print*,'Enter the sampling of the map (A/pixel)'
        read*, sampling
        
        print*,'Enter a CCC threshold for peaks to extract, or number of hits to get.'
        read*, n
        
      write (6,*) 'Enter a run code, for the DockEM in/output files.'
      read (5,*) runcode
30    format (I3)
 
      pdbfile = 'com'//num(runcode)//'.pdb'       
! pdb file com used to record origin of rotations.
	
	print*,' Enter a peak radius (in Angstroms), for exclusion of shoulder solutions'
	read*,peakr
	print*,peakr
	
	peakr= peakr/sampling
	ipeakr=int(peakr + 0.5)
	
!C ***********************************************
!C open input from MRC format files


        CALL IMOPEN(7,'cccmaxmap'//num(runcode)//'.mrc','RO')
	CALL IRDHDR(7,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)

        nxm1=nx-1
        nym1=ny-1
        nzm1=nz-1
        
        nxp1=nx+1
        nxp2=nx+2
 
!C allocate  image_files 

        allocate (cccmaxmap(0:nxp1,0:nym1,0:nzm1),cccmaxmap1(0:nxp1,0:nym1,0:nzm1),phimap(0:nxp1,0:nym1,0:nzm1))
        allocate (thetamap(0:nxp1,0:nym1,0:nzm1), psimap(0:nxp1,0:nym1,0:nzm1)) 
       
                CALL IMOPEN(8,'phimap'//num(runcode)//'.mrc','RO')
        	CALL IRDHDR(8,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
                CALL IMOPEN(9,'thetamap'//num(runcode)//'.mrc','RO')
		CALL IRDHDR(9,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
                CALL IMOPEN(10,'psimap'//num(runcode)//'.mrc','RO')
                CALL IRDHDR(10,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
               
         filename= 'cccmaxmap'//num(runcode)//'.mrc'
        CALL READMRCMAP(7,filename,cccmaxmap,nx,ny,nz,T,T,T,err) !read, is open,  close,      
          filename= 'phimap'//num(runcode)//'.mrc'
        CALL READMRCMAP(8,filename,phimap,nx,ny,nz,T,T,T,err) !read, is open,  close, add 2 bytes
         filename= 'thetamap'//num(runcode)//'.mrc'
        CALL READMRCMAP(9,filename,thetamap,nx,ny,nz,T,T,T,err) !read,  open,  close, add 2 bytes         
         filename= 'psimap'//num(runcode)//'.mrc'
        CALL READMRCMAP(10,filename,psimap,nx,ny,nz,T,T,T,err) !read,  open,  close, add 2 bytes  

	nm=nx*ny*nz
	
	call msd(cccmaxmap,nx,ny,nz,nm,mean,sd,err)

!read the com from pdbfile

	open (1,file=trim(pdbfile),status='unknown', err=777)
	
	read(1,501) head,space1,comx,comy,comz,j1,j2,space2,element,j3
   	
	501       FORMAT(A6,A24,3F8.3,2F6.2,A10,A2,A2)
      
 !      write(6,501) head,space1,X,Y,Z,occupancy,temperature,space2,element,charge
       		
    
	close (1)
	
        PRINT*,'COM of sobj at ',comx,comy,comz,' Angstroms.'
	
	comx=comx/sampling
	comy=comy/sampling
	comz=comz/sampling


        PRINT*,'COM of sobj at ',comx,comy,comz,' pixels.'
   

!	comix=int((comx/sampling)+.5)
!	comiy=int((comy/sampling)+.5)
!	comiz=int((comz/sampling)+.5)
!	print*,comix,comiy,comiz


	cccmaxmap1 = cccmaxmap

! create blot for peakshoulders

	count=0
	do ix=-ipeakr,ipeakr
		do iy=-ipeakr,ipeakr
			do iz=-ipeakr,ipeakr
				r=rad(ix,iy,iz,nx,ny,nz)
				if (r.lt.peakr) then
					count=count+1
					blot(count,1)=ix
					blot(count,2)=iy
					blot(count,3)=iz
				endif
			enddo
		enddo
	enddo
			
			
        open(4,file='searchdoc'//num(runcode)//'.dkm',status='UNKNOWN')
        open(3,file='searchdocIP'//num(runcode)//'.dkm',status='UNKNOWN')
        
        !loop over cccmaxmap
        
        th=-9999.
        if (n.lt.1.0) then
        		th=n
        		n=10000.
        endif
        
        cx=int(float(nx)/2.)+1
        cy=int(float(ny)/2.)+1
        cz=int(float(nz)/2.)+1

        
        do i=1,n
        	print*,i,n,psi,theta,phi
		vec=maxloc(cccmaxmap)
		x=vec(1)-1
		y=vec(2)-1
		z=vec(3)-1
		
		val=cccmaxmap(x,y,z)
		print*,'MAIN1:',x,y,z,val
		call subarea(cccmaxmap1,area,x,y,z,nxp1,nym1,nzm1)
		print*,area
		print*,'MAIN2:',x,y,z,val
		call interp(area,ipx,ipy,ipz,val2)
		print*,area
		print*,'MAIN3:',x,y,z,val,ipx,ipy,ipz
		if (val.eq.-2) cycle
		if (val.lt.th) exit
		cccmaxmap(x,y,z)=-2
		
		!mask neighbours too
		do c=1,count
			bx=x+blot(c,1)
			by=y+blot(c,2)
			bz=z+blot(c,3)
			if (bx.gt.nxm1) bx=nxm1
			if (by.gt.nym1) by=nym1
			if (bz.gt.nzm1) bz=nzm1
			if (bx.lt.0) bx=0
			if (by.lt.0) by=0
			if (bz.lt.0) bz=0
			cccmaxmap(bx,by,bz)=-1.3
		enddo
		
!		print*,x,y,z,val,vec
		
		
		psi = psimap(x,y,z)
		theta = thetamap(x,y,z)
		phi = phimap(x,y,z)
		
!		print*,x,y,z,val,phi,theta,psi
		
		j2=0
		b=0
		angnum=0
		
		sx=x
		sy=y
		sz=z
		
		if (sx.gt.cx) sx=sx-nx
		if (sy.gt.cy) sy=sy-ny
		if (sz.gt.cz) sz=sz-nz
		
		j2=(val-mean)/sd
		val2=val
		write (4,510) i,sx,sy,sz,angnum,b,psi,theta,phi,j2,val
      ipx=ipx+sx
      ipy=ipy+sy
      ipz=ipz+sz
		write (3,510) i,ipx,ipy,ipz,angnum,b,psi,theta,phi,j2,val2

		
	enddo
		
		
		 

510             format (1x,I8,3F8.2,I8,F8.3,3F10.2,2x,2G12.6)

       	close(4)
       	close(3)
       	
       	
       	goto 999
       	
902     continue  
! err reading angles
        print*, 'error reading angles file.'
        stop
903     continue
!Cerr reading map
        print*,'error reading EM-map.'
        stop
777     continue
        print*,'error writing pdbfile.'
        stop
           
999    continue    
       print*,'Program finished O.K.'

       
              
       
       CONTAINS
 
        
              
              
        function num(number)
        character(len=3) num
  
        
        integer number,order
        real fnum,a
        integer hun,ten,units
        character*1 digit(0:9)

        digit(0)='0'
        digit(1)='1'
        digit(2)='2'
        digit(3)='3'
        digit(4)='4'
        digit(5)='5'
        digit(6)='6'
        digit(7)='7'
        digit(8)='8'
        digit(9)='9'

        fnum=float(number)
        fnum=mod(fnum,1000.)
        hun=int(fnum/100.)
        a=mod(fnum,100.)
        ten=int(a/10.)
        units=mod(fnum,10.)
       print *,hun,ten,units


        num=digit(hun)//digit(ten)//digit(units)  
        return
        end function num
        
        
        SUBROUTINE READMRCMAP(stream,filename,map,X,Y,Z,open,close,add2,err)
 
        Use mrc_image
    
         INTEGER  X,Y,Z,err
         real map(0:X+1,0:Y-1,0:Z-1)
         INTEGER  stream, IX,IY,IZ
    
         INTEGER S(3),S1
         logical add2,open,close
         character*80 filename
            
          if(.not. open) then
                       CALL IMOPEN(stream,filename,'RO')
                       CALL IRDHDR(stream,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
          endif
            
         
      
      
!     read in file 

      DO 350 IZ = 0,z-1
          DO 350 IY = 0,y-1
            CALL IRDLIN(stream,ALINE,*998)
            DO 300 IX = 0,x-1       
!            print*,ix,ALINE(IX+1)
                map(ix,iy,iz) = ALINE(IX+1)
300         CONTINUE  
        if(add2) then  
                       map(X+0,iy,iz)=0
                       map(X+1,iy,iz)=0
        endif
        
!          print*,ix,iy,iz,ALINE(IX+1)
350   CONTINUE

        if (close) then
                CALL IMCLOSE(stream)
                print*,'closed ',stream
        endif

      return
998   STOP 'Error on file read.'
     
        END SUBROUTINE READMRCMAP

	real function rad(x,y,z,nx,ny,nz)
	integer x,y,z,nx,ny,nz,cx,cy,cz
	real r
	
	r=(x)**2+(y)**2+(z)**2
	rad=sqrt(r)
	end function rad
	
		
      SUBROUTINE MSD(map,nx,ny,nz,nm,val,STD,err)
      
      real map(0:nx+1,0:ny-1,0:nz-1)
        real*8 map8(0:nx+1,0:ny-1,0:nz-1)
      integer nx,ny,nz,nxm1,nym1,nzm1,nxp1,nxp2,z,err
      real*8 mean,sd,lsum,sum_sqs,sq
      real val,th,STD
      integer nm
        real*8 nm8
     
   ! treats as if all vals not in the masked nm, are zero. must be 0 !!!
      nxp1=nx+1
      nym1=ny-1
      nxm1=nx-1
      nzm1=nz-1
        nm8=nm
      SD =-9999999
      if (nm.le.0) STOP 'Mistake, nm=0'
      
      err=0
      lsum = sum(map(0:nxm1,0:nym1,0:nzm1))
      mean=lsum/nm8

        map8=map
        map8=map8*map8
       sum_sqs=sum(map8(0:nxm1,0:nym1,0:nzm1))
 
         lsum=lsum*lsum
         sum_sqs=nm8*sum_sqs
         sq=((sum_sqs-lsum))
         sq=sq/(nm8*nm8)
    


       if (sq.lt.0) stop 'sd lt zero in msd.'

       th=0.00001
      if (sq.gt.th) then
                sd = sqrt(sq)
                val= -mean/sd
                err=0
        elseif (sq.le.0) then
                err=1
                print*,'le0'
                stop
        elseif (sq.le.th) then 
                !map=(map-mean)
                print*,'at threshold'
                err=0
                stop
        endif

        VAL=MEAN
        STD=SD
       return
       end subroutine MSD

	subroutine interp(A,x,y,z,val)
	!com interpolation
	real a(3,3,3)
	real x,y,z,val
	integer i,j,k
	real suma,sumy,sumz
	real sumx(3)

	sumx=0;sumy=0;sumz=0
	print*,A
	print*,x,y,z,val
!	forall(i=1:3,j=1:3,k=1:3)
	do i=1,3
	do j=i,3
	do k=1,3

		sumx(1) = sumx(1)+a(i,j,k)*(i-2)
		sumx(2) = sumx(2)+a(i,j,k)*(j-2)
		sumx(3) = sumx(3)+a(i,j,k)*(k-2)
	end do
	end do
	end do

!	end forall
	
	suma=sum(a)
	print*,suma
	
	x=sumx(1)/suma
	y=sumx(2)/suma
	z=sumx(3)/suma
	
	return
	
	
	end subroutine interp
	
	subroutine subarea(A,B,x,y,z,nxp1,nym1,nzm1)
	! sub area with wrap around
	integer nxp1,nym1,nzm1,nxm1
	real a(0:nxp1,0:nym1,0:nzm1),b(3,3,3)
	integer x,y,z,val
	integer x1,x2,y1,y2,z1,z2,x0,y0,z0
	integer i,j,k
	real suma,sumy,sumz
	real sumx(3)
	
	nxm1=nxp1-2
	x0=x
	y0=y
	z0=z
	x1=x-1
	x2=x+1
	y1=y-1
	y2=y+1
	z1=z-1
	z2=z+1
	
	x0=wrap(x0,nxm1)
	x1=wrap(x1,nxm1)
	x2=wrap(x2,nxm1)
	y0=wrap(y0,nym1)
	y1=wrap(y1,nym1)
	y2=wrap(y2,nym1)
	z0=wrap(z0,nzm1)
	z1=wrap(z1,nzm1)
	z2=wrap(z2,nzm1)
	
	
	
	
	
	B(1,1,1)=A(x1,y1,z1)	
	B(1,1,2)=A(x1,y1,z0)
	B(1,1,3)=A(x1,y1,z2)		
	B(1,2,1)=A(x1,y0,z1)	
	B(1,2,2)=A(x1,y0,z0)
	B(1,2,3)=A(x1,y0,z2)		
	B(1,3,1)=A(x1,y2,z1)	
	B(1,3,2)=A(x1,y2,z0)
	B(1,3,3)=A(x1,y2,z2)		
	
	
		
	B(2,1,1)=A(x0,y1,z1)	
	B(2,1,2)=A(x0,y1,z0)
	B(2,1,3)=A(x0,y1,z2)		
	B(2,2,1)=A(x0,y0,z1)	
	B(2,2,2)=A(x0,y0,z0)
	B(2,2,3)=A(x0,y0,z2)		
	B(2,3,1)=A(x0,y2,z1)	
	B(2,3,2)=A(x0,y2,z0)
	B(2,3,3)=A(x0,y2,z2)
	
	B(3,1,1)=A(x2,y1,z1)	
	B(3,1,2)=A(x2,y1,z0)
	B(3,1,3)=A(x2,y1,z2)		
	B(3,2,1)=A(x2,y0,z1)	
	B(3,2,2)=A(x2,y0,z0)
	B(3,2,3)=A(x2,y0,z2)		
	B(3,3,1)=A(x2,y2,z1)	
	B(3,3,2)=A(x2,y2,z0)
	B(3,3,3)=A(x2,y2,z2)
	

	print*,'subarea:',x0,x1,x2
	print*,'subarea:',y0,y1,y2
	print*,'subarea:',z0,z1,z2
	
	
	return
	
	
	end subroutine subarea
	
	integer function wrap(x,nxm1)
	integer x,nxm1,v,nx
	v =x
	nx=nxm1+1
	if (x.lt.0) v=x+nx
	if (x.gt.nxm1) v=x-nx
	wrap=v
	end function wrap
	
	end program ShapeMXtA

