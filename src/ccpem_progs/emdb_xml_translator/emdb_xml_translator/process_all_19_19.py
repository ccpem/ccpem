#!/usr/bin/env python
"""
process_all_19_30_19.py

Wrapper script for emdb_xml_translate.py for converting v 1.9 files
in EMDB to v1.9. The only reason for doing this is that it puts elements
in a canonical order that makes comparison with 1.9 -> 2.0 -> 1.9
translation easier.

TODO:

Version history:
0.2, 2015-11-12, Ardan Patwardhan: Minor changes associated with moving file to new project structure
0.3, 2015-11-18, Ardan Patwardhan: Adding mechanism to exclude empty tags which should make comparison easier



Copyright [2014-2016] EMBL - European Bioinformatics Institute
Licensed under the Apache License, Version 2.0 (the
"License"); you may not use this file except in
compliance with the License. You may obtain a copy of
the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on
an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied. See the License for the
specific language governing permissions and limitations
under the License.
"""
import glob
import os
import re
import logging
import subprocess
from optparse import OptionParser
from emdb_settings import EMDBSettings

__author__ = 'Ardan Patwardhan, Sanja Abbott'
__email__ = 'ardan@ebi.ac.uk, sanja@ebi.ac.uk'
__date__ = '2017-06-14'

logging.basicConfig(level=EMDBSettings.log_level, format=EMDBSettings.log_format)


def process_all_19_19(file_path_template, out_dir):
    """
    Take a v1.9 file and read and write it using emdb_xml_translate.py to put it in a canonical form.
    Some post processing is also done to remove empty tags etc

    Parameters:
    @param file_path_template: Regular expression that is passed to a glob function to extract a list of input files
    @param out_dir: The canonical files will be written to this directory
    """
    command_list_base = ['python', './emdb_xml_translate.py', '-i', '1.9', '-o', '1.9', '-f']
    pat = re.compile(r'^[ ]*<(sliceSet|fscSet|maskSet|supplement|fitting|externalReferences|pdbEntryIdList|imageAcquisition|specimenPreparation)/>[ ]*$')
    space_tags = 'title|articleTitle|software|resolutionMethod|algorithm|name|timeResolvedState|molWtMethod'
    pat_spaces_start = re.compile(r'^(.*<(%s)>)(((?!</(%s)>).)*)((</(%s)>)?(((?!<(/%s>)).)*))' % (space_tags, space_tags, space_tags, space_tags))
    pat_spaces_end = re.compile(r'^(((?!</(%s>)).)*)(</(%s)>.*)' % (space_tags, space_tags))
    pat_spaces_sub = re.compile(r'\s+')
    pat_spaces_rep = ' '

    def clean_spaces(inf_hd, outf_hd):
        """
        Reduce spaces, new-lines and tabs to single spaces
        Note: will NOT handle nested tags!!

        Parameters:
        @param inf_hd: Input file handle
        @param outf_hd: Output file handle
        """
        inf_hd.seek(0)
        outf_hd.seek(0)
        start_tag_found = False
        for line in inf_hd:
            if start_tag_found is False:
                line_match = re.match(pat_spaces_start, line)
                if line_match is not None:
                    start_groups = line_match.groups()
                    if start_groups is not None:
                        prefix = start_groups[0]
                        start_tag = start_groups[1]
                        tag_content = start_groups[2]
                        suffix = start_groups[5]
                        end_tag = start_groups[7]
                        if end_tag is not None and end_tag == start_tag:
                            cleaned_content = re.sub(pat_spaces_sub, pat_spaces_rep, tag_content).strip()
                            outf_hd.write('%s%s%s\n' % (prefix, cleaned_content, suffix))
                        else:
                            start_tag_found = True
                else:
                    line_match = re.match(pat, line)
                    if line_match is None:
                        outf_hd.write(line)
            else:
                line_match = re.match(pat_spaces_end, line)
                if line_match is not None:
                    end_groups = line_match.groups()
                    if end_groups is not None:
                        tag_content += ' ' + end_groups[0]
                        end_tag = end_groups[4]
                        suffix = end_groups[3]
                        if end_tag is not None and end_tag == start_tag:
                            cleaned_content = re.sub(pat_spaces_sub, pat_spaces_rep, tag_content).strip()
                            outf_hd.write('%s%s%s\n' % (prefix, cleaned_content, suffix))
                            start_tag_found = False
                    else:
                        tag_content += ' ' + line
                else:
                    tag_content += ' ' + line

    emdb_files = glob.glob(file_path_template)
    num_errors = 0
    num_success = 0
    error_list = []
    for emdb_file in emdb_files:
        inf = os.path.basename(emdb_file)
        outf = os.path.join(out_dir, inf)
        tmpf = os.path.join(out_dir, inf + '.tmp')
        logging.info("Input file: %s, output file: %s", emdb_file, outf)
        command_list = list(command_list_base)
        command_list.append(tmpf)
        command_list.append(emdb_file)
        cmd_text = ' '.join(command_list)
        logging.info('Executing: %s', cmd_text)
        print 'commands %s' % command_list
        exit_code = subprocess.call(command_list)
        if exit_code != 0:
            num_errors += 1
            error_list.append(inf)
        else:
            tmpf_hd = open(tmpf, 'r')
            outf_hd = open(outf, 'w')
            """
            for line in tmpf_hd:
                m = re.match(pat, line)
                if m is None:
                    outf_hd.write(line)
            """
            clean_spaces(tmpf_hd, outf_hd)
            outf_hd.close()
            tmpf_hd.close()
            os.remove(tmpf)
            num_success += 1
    logging.warning('%d files successfully processed!', num_success)
    if num_errors > 0:
        logging.warning('%d errors!', num_errors)
        logging.warning('List of entries that were not translated')
        for entry in error_list:
            logging.warning(entry)


def main():
    """
    Convert all EMDB XML 1.9 header files to canonical XML 1.9 files.
    This makes comparison with output from round-trip conversion (1.9 -> 2.0 -> 1.9) easier.
    """
    default_file_path_template = EMDBSettings.header_19_template
    default_out_dir = EMDBSettings.emdb_19_to_19_dir_out

    # Handle command line options
    usage = """
            python process_all_19_19.py [options]
            Convert all EMDB XML 1.9 header files to canonical XML 1.9 files.
            This makes comparison with output from round-trip conversion (1.9 -> 2.0 -> 1.9) easier.

            Examples:
            python process_all_19_19.py

            Typical run:
            python process_all_19_19.py -t '/data/emstaging/EMD-*/header/emd-*.xml' -o '/data/emdb19_to_19'
            /data/emstaging/EMD-*/header/emd-*.xml is the template used to glob all input 1.9 header files
            /data/emdb19_to_19 is the output directory with the canonical EMDB XML 1.9 files
            """
    version = "0.3"
    parser = OptionParser(usage=usage, version=version)
    parser.add_option("-t", "--template", action="store", type="string", metavar="TEMPLATE", dest="filePathTemplate", default=default_file_path_template, help="Template used to glob all input 1.9 header files [default: %default]")
    parser.add_option("-o", "--out-dir", action="store", type="string", metavar="DIR", dest="outDir", default=default_out_dir, help="Directory for canonical EMDB 1.9 files [default: %default]")
    (options, args) = parser.parse_args()
    # print args
    process_all_19_19(options.filePathTemplate, options.outDir)

if __name__ == "__main__":
    main()
