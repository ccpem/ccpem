#!/usr/bin/env python
"""
unittest.py

Test that the translator is working

TODO:
1) Should discover files in data/cif


Version history:
0.1, 2015-11-11, Ardan Patwardhan: Define tests for 1.9 > 2.0 and 2.0 > 1.9
0.2, 2016-01-06, Ardan Patwardhan: Added tests for emdb_[19|20]_to_json.py


Copyright [2015-2016] EMBL - European Bioinformatics Institute
Licensed under the Apache License, Version 2.0 (the
"License"); you may not use this file except in
compliance with the License. You may obtain a copy of
the License at http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on
an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied. See the License for the
specific language governing permissions and limitations
under the License.
"""
import logging
import unittest
from glob import glob
from os import path
from emdb_xml_translate import EMDBXMLTranslator
from emdb_19_to_json import EMDBXML19JSONTranslator
from emdb_20_to_json import EMDBXML20JSONTranslator

__author__ = 'Ardan Patwardhan, Sanja Abbott'
__email__ = 'ardan@ebi.ac.uk, sanja@ebi.ac.uk'
__date__ = '2017-07-14'

logging.basicConfig(level=logging.INFO)


class TestTranslator(unittest.TestCase):
    """
    Run the translator on a bunch of example files
    """

    input_xml_19_dir = 'data/input/v1.9'
    input_xml_20_dir = 'data/input/v2.0'
    input_xml_30_dir = 'data/input/v3.0'
    output_xml_19_dir = 'data/output/v1.9'
    output_xml_20_dir = 'data/output/v2.0'
    output_json_19_dir = 'data/output/json/v1.9'
    output_json_20_dir = 'data/output/json/v2.0'
    roundtrip_19_dir = 'data/output/roundtrip_v1.9'

    def setUp(self):
        # Read files from input directories and make file lists
        self.in_19_search_path = path.join(self.input_xml_19_dir, '*.xml')
        self.in_full_path_19_list = glob(self.in_19_search_path)
        self.in_file_19_list = [path.splitext(path.basename(f)) for f in self.in_full_path_19_list]
        self.in_20_search_path = path.join(self.input_xml_20_dir, '*.xml')
        self.in_full_path_20_list = glob(self.in_20_search_path)
        self.in_file_20_list = [path.splitext(path.basename(f)) for f in self.in_full_path_20_list]

    def test_19_to_20(self):
        """
        Test conversion of files in the data/input/v1.9 directory from v1.9 to v2.0, and then back to v1.9
        """
        out_file_name_template = '%s-v20%s'
        logging.info('*** Testing conversion of v1.9 (%s) files to v2.0 (%s) ***', self.input_xml_19_dir, self.output_xml_20_dir)
        translator = EMDBXMLTranslator()
        i = 0
        out_file_list = []
        for inf in self.in_full_path_19_list:
            outf = path.join(self.output_xml_20_dir, out_file_name_template % (self.in_file_19_list[i][0], self.in_file_19_list[i][1]))
            out_file_list.append(outf)
            logging.info('Translating %s to %s', inf, outf)
            translator.translate_1_9_to_2_0(inf, outf)
            i += 1

        rt_file_name_template = '%s-rt%s'
        logging.info('*** Testing roundtrip conversion back to v1.9, from dir %s to dir %s ***', self.input_xml_20_dir, self.output_xml_19_dir)
        i = 0
        print
        print
        print 'outfilelist=', out_file_list
        for outf in out_file_list:
            rtripf = path.join(self.roundtrip_19_dir, rt_file_name_template % (self.in_file_19_list[i][0], self.in_file_19_list[i][1]))
            logging.info('Translating %s to %s', outf, rtripf)
            translator.translate_2_0_to_1_9(outf, rtripf)
            i += 1

    def test_20_to_19(self):
        """
        Test conversion of files in the data/input/v2.0 directory from v2.0 to v1.9
        """
        out_file_name_template = '%s-v19%s'
        logging.info('*** Testing conversion of v2.0 (%s) files to v1.9 (%s) ***', self.input_xml_20_dir, self.output_xml_19_dir)
        translator = EMDBXMLTranslator()
        i = 0
        for inf in self.in_full_path_20_list:
            outf = path.join(self.output_xml_19_dir, out_file_name_template % (self.in_file_20_list[i][0], self.in_file_20_list[i][1]))
            logging.info('Translating %s to %s', inf, outf)
            translator.translate_2_0_to_1_9(inf, outf)
            i += 1

    def test_19_to_json(self):
        """
        Test conversion of files in data/input/v1.9 directory to jsons in data/output/v2.0
        """
        out_file_name_template = '%s-v20.json'
        logging.info('*** Testing conversion of v1.9 (%s) files to summary JSONs (%s) ***', self.input_xml_19_dir, self.output_json_19_dir)
        translator = EMDBXML19JSONTranslator()
        i = 0
        out_file_list = []
        for inf in self.in_full_path_19_list:
            outf = path.join(self.output_json_19_dir, out_file_name_template % (self.in_file_19_list[i][0]))
            out_file_list.append(outf)
            logging.info('Translating %s to %s', inf, outf)
            translator.translate(inf, outf)
            i += 1

    def test_20_to_json(self):
        """
        Test conversion of files in data/input/v2.0 directory to jsons in data/output/v1.9
        """
        out_file_name_template = '%s-v19.json'
        logging.info('*** Testing conversion of v20 (%s) files to summary JSONs (%s) ***', self.input_xml_20_dir, self.output_json_20_dir)
        translator = EMDBXML20JSONTranslator()
        i = 0
        out_file_list = []
        for inf in self.in_full_path_20_list:
            outf = path.join(self.output_json_20_dir, out_file_name_template % (self.in_file_20_list[i][0]))
            out_file_list.append(outf)
            logging.info('Translating %s to %s', inf, outf)
            translator.translate(inf, outf)
            i += 1


if __name__ == '__main__':
    unittest.main()
