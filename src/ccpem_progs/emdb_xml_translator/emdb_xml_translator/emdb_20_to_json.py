#!/usr/bin/env python
"""
emdb_20_to_json

Reads in a EMDB header file following 2.0 schema and outputs summary information as a JSON.
This is an stub example to show how to use emdb_da.py to read the header file.

TODO:

Version history:

                 
                

Copyright [2014-2016] EMBL - European Bioinformatics Institute
Licensed under the Apache License, Version 2.0 (the
"License"); you may not use this file except in
compliance with the License. You may obtain a copy of
the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on
an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied. See the License for the
specific language governing permissions and limitations
under the License.
"""


__author__ = 'Ardan Patwardhan'
__email__ = 'ardan@ebi.ac.uk'
__date__ = '2016-01-05'

import sys
import logging
import traceback
import json
from datetime import date
from optparse import OptionParser
import emdb_da
from emdb_settings import emdb_settings



class EMDBXML20JSONTranslator:
    """
    Class for translating summary info from EMDB 2.0 to a JSON file
    """
                
    def __init__(self):
        self.warning_level = 1 # 0 = min, 3 = max
        logging.basicConfig(level=emdb_settings.log_level, format=emdb_settings.log_format)
        
    def set_warning_level(self, level):
        """
        Set the level of logging warnings. 0 = no warnings, 3 = max warnings, 1 = default
        
        Parameters
        @param level: warning level 0 -> 3
        """
        if level <= 0:
            self.warning_level = 0
        elif level >= 3:
            self.warning_level = 3
        else:
            self.warning_level = level    
            
    def warn(self, level, msg):
        """
        Log a warning message but take into account the warning_level
        
        Parameters:
        @param level: only messages with level >= warning_level are printed
        @param msg: warning message
        """
        if level <= self.warning_level:
            logging.warning(msg)
            
    
            
                
    def translate(self, input_file, output_file):
        """
        Translate EMDB 2.0 to a JSON. Summary info only
        
        Parameters
        @param input_file: input file in EMDB 2.0 XML
        @param output_file: output JSON file 
        """
        
        def check_set(get_value, key, json_dict, transform=None):
            """
            Call setVar only if get_value does not return None
            
            Parameters:
            @param get_value: getter function that must return value
            @param key: key in dictionary to be set json_dict[key]
            @param json_dict: JSON dictionary whose key will be set, json_dict[key]
            @param transform: Apply transform(x) before calling setter function
            """
            
            x = get_value()
            if x is not None:           
                if transform is not None:
                    try:
                        z = x
                        x = transform(z)
                    except Exception:
                        self.warn(3, "function check_set: Transform function did not work: %s(%s)" % (transform, z))
                        self.warn(3, traceback.format_exc())
                        return
                
                json_dict[key] = x

        def get_authors(auth_list_in, simple=False):
            """
            Get authors from 2.0  and return comma seperated  string
            
            Parameters
            @param auth_list_in: list object of 2.0 author objects
            @param simple: boolean - True means that the authors in 2.0 are simple strings, otherwise they are journal authors
            @return: 
            """
            authList = []
            for authIn in auth_list_in:
                if simple == True:
                    x = authIn
                else:
                    x = authIn.get_valueOf_()
                authList.append(x)
                
            if len(authList) > 0:
                authStr = ', '.join(authList)
            else:
                authStr = ''
            return authStr  
                      
        def copy_citation(cit_in):
            """
            Return JSON object containing citation - more complex example...
            
            Parameters:
            @param cite_in: Input citation in 2.0 schema  
            @return: python dictionary with reference info          
            """           
                    
            jrnlIn = cit_in.get_citation_type()
            cit_out = {}
            if jrnlIn.original_tagname_ == 'journal_citation':
                cit_out['citationType'] = 'Journal'
                cit_out['authors'] = get_authors(jrnlIn.get_author())
                check_set(jrnlIn.get_title, 'title', cit_out)
                check_set(jrnlIn.get_journal, 'journal', cit_out)
                check_set(jrnlIn.get_published, 'published', cit_out) 
                # This is a fix because of bad data - emd-1648.xml has an empty volume tag!
                vol = jrnlIn.get_volume()
                if vol is not None and len(vol) > 0:  
                    cit_out['volume'] = vol
                check_set(jrnlIn.get_first_page, 'firstPage', cit_out) 
                check_set(jrnlIn.get_last_page, 'lastPage', cit_out)     
                check_set(jrnlIn.get_year, 'year', cit_out)                
                
            else:
                cit_out['citationType'] = 'Non-journal'
                nonJrnlIn = jrnlIn
                cit_out['authors'] = get_authors(nonJrnlIn.get_author())
                check_set(nonJrnlIn.get_editor, 'editor', cit_out)
                check_set(nonJrnlIn.get_book_chapter_title, 'chapterTitle', cit_out)
                check_set(nonJrnlIn.get_book_title, 'title', cit_out)
                check_set(nonJrnlIn.get_thesis_title, 'thesisTitle', cit_out)
                check_set(nonJrnlIn.get_published, 'published', cit_out)                 
                check_set(nonJrnlIn.get_publisher, 'publisher', cit_out)
                check_set(nonJrnlIn.get_publication_location, 'location', cit_out)
                check_set(nonJrnlIn.get_volume, 'volume', cit_out)
                check_set(nonJrnlIn.get_first_page, 'firstPage', cit_out)
                check_set(nonJrnlIn.get_last_page, 'lastPage', cit_out)
                check_set(nonJrnlIn.get_year, 'year', cit_out)
                                 
            return cit_out
           
        xmlIn = emdb_da.parse(input_file, silence=True)
        jsonOut = {}
        admIn = xmlIn.get_admin()
        datesIn = admIn.get_key_dates()
        statusIn = admIn.get_current_status() 
        authListIn = admIn.get_authors_list()
        xRefIn = xmlIn.get_crossreferences()
        citeListIn = xRefIn.get_citation_list() 
        citeIn = citeListIn.get_primary_citation()
        
        check_set(xmlIn.get_emdb_id, 'emdbId', jsonOut)
        check_set(admIn.get_title, 'title', jsonOut)
        check_set(statusIn.get_code().get_valueOf_, 'status', jsonOut)
        jsonOut['authors'] = get_authors(authListIn.get_author(), simple=True)
        
        # dates
        check_set(datesIn.get_deposition, 'depositionDate', jsonOut, str)
        check_set(datesIn.get_header_release, 'headerReleaseDate', jsonOut, str)
        check_set(datesIn.get_map_release, 'mapReleaseDate', jsonOut, str)
        
        # primary citation
        jsonOut['citation'] = copy_citation(citeIn)

        fd = open(output_file, 'w') if output_file else sys.stdout
        json.dump(jsonOut, fd)
        if fd is not sys.stdout:
            fd.close()               

            
   
                    
        
def main():
    """
    Extract summary info from EMDB XML 2.0 file to JSON

    """
    
    # Handle command line options
    usage = """
            emdb_20_to_json.py [options] inputFile
            Convert EMDB XML

            Examples:
            python emdb_20_to_json.py inputFile

            Typical run:
            python emdb_20_to_json.py -f out.json in.xml
            in.xml is assumed to be a EMDB 2.0 XML file  
            out.json is a JSON file created with the summary information
               
            """
    version = "0.1"
    parser = OptionParser(usage = usage, version = version)
    parser.add_option("-f", "--out-file", action="store", type="string", metavar="FILE", dest="outputFile", help="Write output to FILE")
    parser.add_option("-w", "--warning-level", action="store", type="int", dest="warning_level", default=1, help="Level of warning output. 0 is none, 3 is max, default = 1")
    (options, args) = parser.parse_args()

    # Check for sensible/supported options
    if len(args) < 1:
        sys.exit("No input file specified!")
    else:
        inputFile = args[0]

    # Call appropriate conversion routine
    translator = EMDBXML20JSONTranslator()
    translator.set_warning_level(options.warning_level)
    translator.translate(inputFile, options.outputFile)



    
if __name__ == "__main__":
    main()