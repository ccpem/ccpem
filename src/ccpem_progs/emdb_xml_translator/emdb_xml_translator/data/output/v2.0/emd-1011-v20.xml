<?xml version="1.0" encoding="UTF-8"?>
<emd emdb_id="EMD-1011" version="2.0">
    <admin>
        <current_status>
            <code>REL</code>
            <processing_site>PDBe</processing_site>
        </current_status>
        <sites>
            <deposition>PDBe</deposition>
            <last_processing>PDBe</last_processing>
        </sites>
        <key_dates>
            <deposition>2002-10-08</deposition>
            <header_release>2002-10-09</header_release>
            <map_release>2002-10-09</map_release>
            <update>2012-10-17</update>
        </key_dates>
        <title>Minor proteins, mobile arms and membrane-capsid interactions in the bacteriophage PRD1 capsid.</title>
        <authors_list>
            <author>San Martin C</author>
            <author>Huiskonen JT</author>
            <author>Bamford JK</author>
            <author>Butcher SJ</author>
            <author>Fuller SD</author>
            <author>Bamford DH</author>
            <author>Burnett RM</author>
        </authors_list>
        <keywords></keywords>
    </admin>
    <crossreferences>
        <citation_list>
            <primary_citation>
                <journal_citation published="true">
                    <author order="1">San Martin C</author>
                    <author order="2">Huiskonen JT</author>
                    <author order="3">Bamford JK</author>
                    <author order="4">Butcher SJ</author>
                    <author order="5">Fuller SD</author>
                    <author order="6">Bamford DH</author>
                    <author order="7">Burnett RM</author>
                    <title>Minor proteins, mobile arms and membrane-capsid interactions in the bacteriophage PRD1 capsid.</title>
                    <journal>NAT.STRUCT.BIOL.</journal>
                    <volume>9</volume>
                    <first_page>756</first_page>
                    <last_page>763</last_page>
                    <year>2002</year>
                    <external_references type="pubmed">12219080</external_references>
                    <external_references type="doi">doi:10.1038/nsb837</external_references>
                </journal_citation>
            </primary_citation>
        </citation_list>
        <pdb_list>
            <pdb_reference id="1">
                <pdb_id>1gw7</pdb_id>
                <relationship>
                    <in_frame>FULLOVERLAP</in_frame>
                </relationship>
            </pdb_reference>
        </pdb_list>
    </crossreferences>
    <sample>
        <name>Bacteriophage PRD1</name>
        <supramolecule_list>
            <supramolecule id="1000" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="sample">
                <details>The sample is a virion containing at least 19 structural
      proteins and a double stranded DNA genome</details>
                <oligomeric_state>A pseudo T=25 assembly</oligomeric_state>
                <number_unique_components>1</number_unique_components>
                <molecular_weight>
                    <theoretical units="MDa">70</theoretical>
                </molecular_weight>
            </supramolecule>
            <supramolecule id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="virus">
                <name synonym="bacteriophage PRD1">Enterobacteria phage PRD1</name>
                <details>a T=25 virion</details>
                <sci_species_name ncbi="10658">Enterobacteria phage PRD1</sci_species_name>
                <natural_host>
                    <organism ncbi="28901">Salmonella enterica</organism>
                    <synonym_organism>BACTERIA(EUBACTERIA)</synonym_organism>
                </natural_host>
                <host_system/>
                <virus_shell id="1">
                    <name>P3</name>
                    <diameter units="A">70</diameter>
                    <triangulation>25</triangulation>
                </virus_shell>
                <virus_type>VIRION</virus_type>
                <virus_isolate>STRAIN</virus_isolate>
                <virus_enveloped>false</virus_enveloped>
                <virus_empty>false</virus_empty>
                <syn_species_name>bacteriophage PRD1</syn_species_name>
            </supramolecule>
        </supramolecule_list>
        <macromolecule_list/>
    </sample>
    <structure_determination_list>
        <structure_determination>
            <method>singleParticle</method>
            <aggregation_state>particle</aggregation_state>
            <specimen_preparation_list>
                <specimen_preparation id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="base_preparation_type">
                    <concentration units="mg/mL">1</concentration>
                    <buffer>
                        <ph>7.2</ph>
                        <details>20 mM Tris HCl</details>
                    </buffer>
                    <vitrification>
                        <cryogen_name>ETHANE</cryogen_name>
                        <chamber_humidity>60</chamber_humidity>
                        <chamber_temperature>23</chamber_temperature>
                        <instrument>HOMEMADE PLUNGER</instrument>
                        <details>Vitrification instrument: EMBL plunger. vitrification carried out at 23 degrees at ambient humidity</details>
                        <method>Blot for 1 s before plunging into ethane slush</method>
                    </vitrification>
                </specimen_preparation>
            </specimen_preparation_list>
            <microscopy_list>
                <microscopy id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="base_microscopy_type">
                    <microscope>FEI/PHILIPS CM200FEG/ST</microscope>
                    <illumination_mode>FLOOD BEAM</illumination_mode>
                    <imaging_mode>BRIGHT FIELD</imaging_mode>
                    <electron_source>FIELD EMISSION GUN</electron_source>
                    <acceleration_voltage>200</acceleration_voltage>
                    <nominal_cs>2</nominal_cs>
                    <nominal_defocus_min>1300</nominal_defocus_min>
                    <nominal_defocus_max>4100</nominal_defocus_max>
                    <nominal_magnification>36000.</nominal_magnification>
                    <specimen_holder_model>GATAN LIQUID NITROGEN</specimen_holder_model>
                    <temperature>
                        <temperature_average units="Kelvin">105</temperature_average>
                    </temperature>
                    <image_recording_list>
                        <image_recording>
                            <film_or_detector_model category="film">KODAK SO-163 FILM</film_or_detector_model>
                            <digitization_details>
                                <scanner>ZEISS SCAI</scanner>
                                <sampling_interval units="um">7.0</sampling_interval>
                            </digitization_details>
                            <number_real_images>50</number_real_images>
                            <average_electron_dose_per_image units="e/A**2">6</average_electron_dose_per_image>
                            <detector_distance>44</detector_distance>
                            <details>images were scanned at 7 micron steps size and then
        averaged to give a final size of 14 microns</details>
                            <od_range>1.</od_range>
                            <bits_per_pixel>8.</bits_per_pixel>
                        </image_recording>
                    </image_recording_list>
                    <specimen_holder>eucentric</specimen_holder>
                </microscopy>
            </microscopy_list>
            <image_processing id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="singleparticle_processing_type">
                <details>The particles were purified by rate-zonal
          centrifugation       and ion-exchange chromatography
          Walin,Tuma,Thomas and Bamford       Virology (1994) 201:1-7</details>
                <ctf_correction>
                    <details>normalized sum of ctf multiplied images</details>
                </ctf_correction>
                <final_reconstruction>
                    <number_images_used>1468</number_images_used>
                    <applied_symmetry>
                        <point_group>I</point_group>
                    </applied_symmetry>
                    <algorithm>Fourier-Bessel</algorithm>
                    <resolution>16</resolution>
                    <resolution_method>16</resolution_method>
                    <software_list>
                        <software>
                            <name>EMBL-ICOS, MRC</name>
                        </software>
                    </software_list>
                    <details>final maps were calculated by making a normalized sum of
        seperate ctf multiplied maps Baker, T. S., Olson, N. H., and
        Fuller, S. D. (1999). Adding the third dimension to virus life
        cycles: Three-Dimensional Reconstruction of Icosahedral Viruses
        from Cryo-Electron Micrographs. Microbiology and Molecular
        Biology       Reviews 63, 862-922. Butcher, S. J., Bamford, D.
        H., and Fuller,       S. D. (1995). DNA packaging orders the
        membrane of bacteriophage       PRD1. Embo J 14, 6078-6086.
        Ferlenghi, I., Gowen, B., de Haas, F.,       Mancini, E. J.,
        Garoff, H., Sjoberg, M., and Fuller, S. D. (1998).       The
        first step: activation of the Semliki Forest virus spike
        protein precursor causes a localized conformational change in
        the       trimeric spike. J Mol Biol 283, 71-81. Fuller, S. D.,
        Berriman, J.       A., Butcher, S. J., and Gowen, B. E. (1995).
        Low pH induces       swiveling of the glycoprotein heterodimers
        in the Semliki Forest       virus spike complex. Cell 81,
        715-725. Fuller, S. D., Butcher, S.       J., Cheng, R. H., and
        Baker, T. S. (1996). Three-dimensional       reconstruction of
        icosahedral particles--the uncommon line. J       Struct Biol
        116, 48-55. Mancini, E. J., Clarke, M., Gowen, B.,       Rutten,
        T., and Fuller, S. D. (2000). Cryo-electron microscopy
        reveals the functional organization of an enveloped virus,
        Semliki       Forest virus. Molecular Cell 5, 255-266. Mancini,
        E. J., de Haas,       F., and Fuller, S. D. (1997).
        High-resolution icosahedral       reconstruction: fulfilling the
        promise of cryo-electron       microscopy. Structure 5, 741-750.
        San Martin, C., Burnett, R. M.,       de Haas, F., Heinkel, R.,
        Rutten, T., Fuller, S. D., Butcher, S.       J., and Bamford, D.
        H. (2001). Combined EM/X-ray imaging yields a       quasi-atomic
        model of the adenovirus-related bacteriophage PRD1       and
        shows key capsid and membrane interactions. Structure (Camb)
        9, 917-930. San Martin, C., Huiskonen, J. T., Bamford, J. K.,
        Butcher, S. J., Fuller, S. D., Bamford, D. H., and Burnett, R.
        M.       (2002). Minor proteins, mobile arms and membrane-capsid
        interactions in the bacteriophage PRD1 capsid. Nat Struct Biol
        9,       756-763. Sheehan, B., Fuller, S. D., Pique, M. E., and
        Yeager, M.       (1996). AVS software for visualization in
        molecular microscopy. J       Struct Biol 116, 99-106.</details>
                </final_reconstruction>
                <final_angle_assignment>
                    <details>range giving min eigenvalues for inversion of
        less than .01</details>
                </final_angle_assignment>
            </image_processing>
        </structure_determination>
    </structure_determination_list>
    <map size_kbytes="65537" format="CCP4">
        <file>emd_1011.map.gz</file>
        <symmetry>
            <space_group>1</space_group>
        </symmetry>
        <data_type>Image stored as Reals</data_type>
        <dimensions>
            <col>256</col>
            <row>256</row>
            <sec>256</sec>
        </dimensions>
        <origin>
            <col>-127</col>
            <row>-127</row>
            <sec>-127</sec>
        </origin>
        <spacing>
            <x>256</x>
            <y>256</y>
            <z>256</z>
        </spacing>
        <cell>
            <a>880.64</a>
            <b>880.64</b>
            <c>880.64</c>
            <alpha>90</alpha>
            <beta>90</beta>
            <gamma>90</gamma>
        </cell>
        <axis_order>
            <fast>X</fast>
            <medium>Y</medium>
            <slow>Z</slow>
        </axis_order>
        <statistics>
            <minimum>-16843.700000000000728</minimum>
            <maximum>27000.</maximum>
            <average>188.24799999999999</average>
            <std>3602.7199999999998</std>
        </statistics>
        <pixel_spacing>
            <x>3.44</x>
            <y>3.44</y>
            <z>3.44</z>
        </pixel_spacing>
        <contour_list>
            <contour primary="true">
                <level>7770</level>
            </contour>
        </contour_list>
        <annotation_details>   This map is one of   four described in   San
      Martin, C.,    Huiskonen,    J. T.,    Bamford, J. K.,    Butcher,
      S. J.,    Fuller, S. D.,    Bamford, D. H.,   and Burnett, R. M.
      (2002).    Minor proteins,    mobile arms and    membrane-capsid
      interactions in    the bacteriophage    PRD1 capsid.    Nat Struct
      Biol    9, 756-763. The other three maps are also available.</annotation_details>
        <details>::::EMDATABANK.org::::EMD-1011::::</details>
    </map>
    <interpretation>
        <modelling_list>
            <modelling>
                <initial_model>
                    <access_code>1GW7</access_code>
                    <chain>
                        <id>A</id>
                    </chain>
                    <chain>
                        <id>B</id>
                    </chain>
                    <chain>
                        <id>C</id>
                    </chain>
                    <chain>
                        <id>D</id>
                    </chain>
                    <chain>
                        <id>E</id>
                    </chain>
                    <chain>
                        <id>F</id>
                    </chain>
                    <chain>
                        <id>G</id>
                    </chain>
                    <chain>
                        <id>H</id>
                    </chain>
                    <chain>
                        <id>I</id>
                    </chain>
                </initial_model>
                <refinement_protocol>rigid body</refinement_protocol>
                <software_list>
                    <software>
                        <name>emfit (M. Rossmann Cheng, R., Kuhn, R., Olson, N.,
        Rossmann, M., and Baker, T. (1995). Nucleocapsid and
        glycoprotein       organization in an enveloped virus. Cell 80, 621-630.)</name>
                    </software>
                </software_list>
                <details>Protocol: real space refinement. The scale of the map and the effective resolution were determined from a comparison between the P3 portion of the WT reconstruction and the atomic model</details>
                <target_criteria>minimizing R factor (final 47.7%)</target_criteria>
            </modelling>
        </modelling_list>
        <slices_list>
            <slice size_kbytes="257" format="CCP4">
                <file>emd_1011_slc.map</file>
                <symmetry>
                    <space_group>1</space_group>
                </symmetry>
                <data_type>Image stored as Reals</data_type>
                <dimensions>
                    <col>256</col>
                    <row>256</row>
                    <sec>1</sec>
                </dimensions>
                <origin>
                    <col>-127</col>
                    <row>-127</row>
                    <sec>0</sec>
                </origin>
                <spacing>
                    <x>256</x>
                    <y>256</y>
                    <z>0</z>
                </spacing>
                <cell>
                    <a>256.000</a>
                    <b>256.000</b>
                    <c>1.000</c>
                    <alpha>90.000</alpha>
                    <beta>90.000</beta>
                    <gamma>90.000</gamma>
                </cell>
                <axis_order>
                    <fast>X</fast>
                    <medium>Y</medium>
                    <slow>Z</slow>
                </axis_order>
                <statistics>
                    <minimum>-15940.523440000000846</minimum>
                    <maximum>24043.013670000000275</maximum>
                    <average>656.550230000000056</average>
                    <std>0.</std>
                </statistics>
                <pixel_spacing>
                    <x>3.44</x>
                    <y>3.44</y>
                    <z>3.44</z>
                </pixel_spacing>
                <annotation_details></annotation_details>
                <details>Created by IMAGIC: MRC image =
          /ebi/msd/emdeposit/submissions/EMD-1210/UP/tempCC P4/m
          09-10-20 13:30:08</details>
            </slice>
            <slice size_kbytes="257" format="CCP4">
                <file>emd_1011_1_slc.map</file>
                <symmetry>
                    <space_group>1</space_group>
                </symmetry>
                <data_type>Image stored as Reals</data_type>
                <dimensions>
                    <col>256</col>
                    <row>256</row>
                    <sec>1</sec>
                </dimensions>
                <origin>
                    <col>-127</col>
                    <row>-127</row>
                    <sec>0</sec>
                </origin>
                <spacing>
                    <x>256</x>
                    <y>256</y>
                    <z>0</z>
                </spacing>
                <cell>
                    <a>256.000</a>
                    <b>256.000</b>
                    <c>1.000</c>
                    <alpha>90.000</alpha>
                    <beta>90.000</beta>
                    <gamma>90.000</gamma>
                </cell>
                <axis_order>
                    <fast>X</fast>
                    <medium>Y</medium>
                    <slow>Z</slow>
                </axis_order>
                <statistics>
                    <minimum>-15709.902340000000549</minimum>
                    <maximum>24138.25</maximum>
                    <average>652.497560000000021</average>
                    <std>0.</std>
                </statistics>
                <pixel_spacing>
                    <x>3.44</x>
                    <y>3.44</y>
                    <z>3.44</z>
                </pixel_spacing>
                <annotation_details></annotation_details>
                <details>Created by IMAGIC: MRC image =
          /ebi/msd/emdeposit/submissions/EMD-1210/UP/tempCC P4/m
          09-10-20 13:31:02</details>
            </slice>
            <slice size_kbytes="257" format="CCP4">
                <file>emd_1011_2_slc.map</file>
                <symmetry>
                    <space_group>1</space_group>
                </symmetry>
                <data_type>Image stored as Reals</data_type>
                <dimensions>
                    <col>256</col>
                    <row>256</row>
                    <sec>1</sec>
                </dimensions>
                <origin>
                    <col>-127</col>
                    <row>-127</row>
                    <sec>0</sec>
                </origin>
                <spacing>
                    <x>256</x>
                    <y>256</y>
                    <z>0</z>
                </spacing>
                <cell>
                    <a>256.000</a>
                    <b>256.000</b>
                    <c>1.000</c>
                    <alpha>90.000</alpha>
                    <beta>90.000</beta>
                    <gamma>90.000</gamma>
                </cell>
                <axis_order>
                    <fast>X</fast>
                    <medium>Y</medium>
                    <slow>Z</slow>
                </axis_order>
                <statistics>
                    <minimum>-15620.572270000000572</minimum>
                    <maximum>24412.707030000001396</maximum>
                    <average>653.292910000000006</average>
                    <std>0.</std>
                </statistics>
                <pixel_spacing>
                    <x>3.44</x>
                    <y>3.44</y>
                    <z>3.44</z>
                </pixel_spacing>
                <annotation_details></annotation_details>
                <details>Created by IMAGIC: MRC image =
          /ebi/msd/emdeposit/submissions/EMD-1210/UP/tempCC P4/m
          09-10-20 13:31:39</details>
            </slice>
        </slices_list>
    </interpretation>
</emd>
