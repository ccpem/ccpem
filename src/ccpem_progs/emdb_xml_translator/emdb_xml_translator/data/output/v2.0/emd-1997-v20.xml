<?xml version="1.0" encoding="UTF-8"?>
<emd emdb_id="EMD-1997" version="2.0">
    <admin>
        <current_status>
            <code>REL</code>
            <processing_site>PDBe</processing_site>
        </current_status>
        <sites>
            <deposition>PDBe</deposition>
            <last_processing>PDBe</last_processing>
        </sites>
        <key_dates>
            <deposition>2011-12-01</deposition>
            <header_release>2012-01-13</header_release>
            <map_release>2012-04-17</map_release>
            <update>2012-04-17</update>
        </key_dates>
        <title>ATP-triggered molecular mechanics of the chaperonin GroEL</title>
        <authors_list>
            <author>Clare DK</author>
            <author>Vasishtan D</author>
            <author>Stagg S</author>
            <author>Quispe J</author>
            <author>Farr GW</author>
            <author>Topf M</author>
            <author>Horwich AL</author>
            <author>Saibil HR</author>
        </authors_list>
        <keywords>Tetradecamer of GroEL</keywords>
    </admin>
    <crossreferences>
        <citation_list>
            <primary_citation>
                <journal_citation published="true">
                    <author order="1">Clare DK</author>
                    <author order="2">Vasishtan D</author>
                    <author order="3">Stagg S</author>
                    <author order="4">Quispe J</author>
                    <author order="5">Farr GW</author>
                    <author order="6">Topf M</author>
                    <author order="7">Horwich AL</author>
                    <author order="8">Saibil HR</author>
                    <title>ATP-triggered conformational changes delineate substrate-binding and -folding mechanics of the GroEL chaperonin.</title>
                    <journal>CELL(CAMBRIDGE,MASS.)</journal>
                    <volume>149</volume>
                    <first_page>113</first_page>
                    <last_page>123</last_page>
                    <year>2012</year>
                    <external_references type="pubmed">22445172</external_references>
                    <external_references type="doi">doi:10.1016/j.cell.2012.02.047</external_references>
                </journal_citation>
            </primary_citation>
        </citation_list>
    </crossreferences>
    <sample>
        <name>GroEL</name>
        <supramolecule_list>
            <supramolecule id="1000" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="sample">
                <oligomeric_state>tetradecamer of GroEL</oligomeric_state>
                <number_unique_components>1</number_unique_components>
                <molecular_weight>
                    <experimental units="MDa">0.8</experimental>
                    <theoretical units="MDa">0.8</theoretical>
                </molecular_weight>
            </supramolecule>
        </supramolecule_list>
        <macromolecule_list>
            <macromolecule id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="protein_or_peptide">
                <name synonym="GroEL">hsp60</name>
                <natural_source>
                    <organism ncbi="562">Escherichia coli</organism>
                    <synonym_organism>E. coli</synonym_organism>
                    <cellular_location>cytoplasm</cellular_location>
                </natural_source>
                <molecular_weight>
                    <experimental units="MDa">0.056</experimental>
                    <theoretical units="MDa">0.056</theoretical>
                </molecular_weight>
                <details>ATPase Mutant, D398A</details>
                <number_of_copies>14</number_of_copies>
                <oligomeric_state>tetradecamer</oligomeric_state>
                <recombinant_exp_flag>true</recombinant_exp_flag>
                <recombinant_expression>
                    <organism ncbi="562">Escherichia coli</organism>
                </recombinant_expression>
                <sequence>
                    <external_references type="GO">GO:0042026</external_references>
                    <external_references type="INTERPRO">IPR002423</external_references>
                </sequence>
            </macromolecule>
        </macromolecule_list>
    </sample>
    <structure_determination_list>
        <structure_determination>
            <method>singleParticle</method>
            <aggregation_state>particle</aggregation_state>
            <specimen_preparation_list>
                <specimen_preparation id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="base_preparation_type">
                    <concentration units="mg/mL">4</concentration>
                    <buffer>
                        <ph>7.4</ph>
                        <details>50 mM Tris-HCl pH 7.4, 50 mM KCl and 10 mM MgCl2, 200uM ATP</details>
                    </buffer>
                    <grid>
                        <details>cflat grids r2/2</details>
                    </grid>
                    <vitrification>
                        <cryogen_name>ETHANE</cryogen_name>
                        <chamber_humidity>100</chamber_humidity>
                        <chamber_temperature>95</chamber_temperature>
                        <instrument>FEI VITROBOT</instrument>
                        <details>Vitrification instrument: Vitrobot</details>
                        <timed_resolved_state>vitrified within 3o seconds</timed_resolved_state>
                        <method>grids were blotted for 2-3 seconds</method>
                    </vitrification>
                </specimen_preparation>
            </specimen_preparation_list>
            <microscopy_list>
                <microscopy id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="base_microscopy_type">
                    <microscope>FEI TECNAI F20</microscope>
                    <illumination_mode>FLOOD BEAM</illumination_mode>
                    <imaging_mode>BRIGHT FIELD</imaging_mode>
                    <electron_source>FIELD EMISSION GUN</electron_source>
                    <acceleration_voltage>120</acceleration_voltage>
                    <nominal_cs>2</nominal_cs>
                    <nominal_defocus_min>700</nominal_defocus_min>
                    <nominal_defocus_max>3500</nominal_defocus_max>
                    <calibrated_magnification>148500.</calibrated_magnification>
                    <specimen_holder_model>GATAN LIQUID NITROGEN</specimen_holder_model>
                    <temperature>
                        <temperature_min units="Kelvin">95</temperature_min>
                        <temperature_max units="Kelvin">95</temperature_max>
                        <temperature_average units="Kelvin">95</temperature_average>
                    </temperature>
                    <details>The data was collected with leginon at SCRIPPS</details>
                    <image_recording_list>
                        <image_recording>
                            <film_or_detector_model category="CCD">GATAN ULTRASCAN 4000 (4k x 4k)</film_or_detector_model>
                            <digitization_details/>
                            <average_electron_dose_per_image units="e/A**2">15</average_electron_dose_per_image>
                        </image_recording>
                    </image_recording_list>
                    <specimen_holder>Eucentric</specimen_holder>
                </microscopy>
            </microscopy_list>
            <image_processing id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="singleparticle_processing_type">
                <details>The particles were automatically picked using FindEM</details>
                <ctf_correction>
                    <details>each particle was phase flipped</details>
                </ctf_correction>
                <final_reconstruction>
                    <number_images_used>1200</number_images_used>
                    <applied_symmetry>
                        <point_group>C7</point_group>
                    </applied_symmetry>
                    <algorithm>projection matching</algorithm>
                    <resolution>7</resolution>
                    <resolution_method>FSC 0.5</resolution_method>
                    <software_list>
                        <software>
                            <name>SPIDER, IMAGIC</name>
                        </software>
                    </software_list>
                    <details>SIRT was used to reconstruct the final map</details>
                </final_reconstruction>
                <final_angle_assignment>
                    <details>theta 80-100, phi 0-51.42</details>
                </final_angle_assignment>
            </image_processing>
        </structure_determination>
    </structure_determination_list>
    <map size_kbytes="3908" format="CCP4">
        <file>emd_1997.map.gz</file>
        <symmetry>
            <space_group>1</space_group>
        </symmetry>
        <data_type>Image stored as Reals</data_type>
        <dimensions>
            <col>100</col>
            <row>100</row>
            <sec>100</sec>
        </dimensions>
        <origin>
            <col>-50</col>
            <row>-50</row>
            <sec>-50</sec>
        </origin>
        <spacing>
            <x>100</x>
            <y>100</y>
            <z>100</z>
        </spacing>
        <cell>
            <a>202.0</a>
            <b>202.0</b>
            <c>202.0</c>
            <alpha>90.0</alpha>
            <beta>90.0</beta>
            <gamma>90.0</gamma>
        </cell>
        <axis_order>
            <fast>X</fast>
            <medium>Y</medium>
            <slow>Z</slow>
        </axis_order>
        <statistics>
            <minimum>-2.28869247</minimum>
            <maximum>3.46702909</maximum>
            <average>0.06064206</average>
            <std>0.33465365</std>
        </statistics>
        <pixel_spacing>
            <x>2.02</x>
            <y>2.02</y>
            <z>2.02</z>
        </pixel_spacing>
        <contour_list>
            <contour primary="true">
                <level>1</level>
                <source>AUTHOR</source>
            </contour>
        </contour_list>
        <annotation_details>GroEL-Apo</annotation_details>
        <details>::::EMDATABANK.org::::EMD-1997::::</details>
    </map>
    <interpretation>
        <modelling_list>
            <modelling>
                <initial_model>
                    <access_code>1OEL</access_code>
                </initial_model>
                <refinement_protocol>rigid body</refinement_protocol>
                <software_list>
                    <software>
                        <name>Chimera</name>
                    </software>
                </software_list>
                <details>Protocol: Rigid Body</details>
                <target_criteria>cross-correlation coefficient</target_criteria>
                <refinement_space>REAL</refinement_space>
            </modelling>
        </modelling_list>
    </interpretation>
</emd>
