"""
emdb_settings.py

Global settings for project wrapped as a class to confine namespace

TODO:

Version history:


                

Copyright [2014-2016] EMBL - European Bioinformatics Institute
Licensed under the Apache License, Version 2.0 (the
"License"); you may not use this file except in
compliance with the License. You may obtain a copy of
the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on
an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied. See the License for the
specific language governing permissions and limitations
under the License.
"""


__author__ = 'Ardan Patwardhan'
__email__ = 'ardan@ebi.ac.uk'
__date__ = '2014-12-04'

import logging
import os

class emdb_settings:
    log_level = logging.INFO
    log_format = '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
    archiveHeaderTemplate = '/data/emstaging/EMD-*/header/emd-*.xml'
    emdb20Dir = '/data/emdb20' # directory to contain EMDB XML 2.0 files
    header20Template = os.path.join(emdb20Dir, 'emd-*.xml')
    emdb19To19Dir = '/data/emdb19_to_19' # directory to contain EMDB XML 1.9 files
    emdb20To19Dir = '/data/emdb20_to_19' # directory to contain back translated EMDB XML 1.9 files
    diffDir = '/data/emdb_diff' # directory to outbut diffs between back-converted 1.9 files and the canonical ones
    