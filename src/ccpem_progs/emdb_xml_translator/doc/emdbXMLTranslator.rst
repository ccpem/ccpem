emdbXMLTranslator package
=========================

Submodules
----------

emdbXMLTranslator.diff_all module
---------------------------------

.. automodule:: emdbXMLTranslator.diff_all
    :members:
    :undoc-members:
    :show-inheritance:

emdbXMLTranslator.emdb_19 module
--------------------------------

.. automodule:: emdbXMLTranslator.emdb_19
    :members:
    :undoc-members:
    :show-inheritance:

emdbXMLTranslator.emdb_19_to_json module
----------------------------------------

.. automodule:: emdbXMLTranslator.emdb_19_to_json
    :members:
    :undoc-members:
    :show-inheritance:

emdbXMLTranslator.emdb_20_to_json module
----------------------------------------

.. automodule:: emdbXMLTranslator.emdb_20_to_json
    :members:
    :undoc-members:
    :show-inheritance:

emdbXMLTranslator.emdb_da module
--------------------------------

.. automodule:: emdbXMLTranslator.emdb_da
    :members:
    :undoc-members:
    :show-inheritance:

emdbXMLTranslator.emdb_settings module
--------------------------------------

.. automodule:: emdbXMLTranslator.emdb_settings
    :members:
    :undoc-members:
    :show-inheritance:

emdbXMLTranslator.emdb_user_methods module
------------------------------------------

.. automodule:: emdbXMLTranslator.emdb_user_methods
    :members:
    :undoc-members:
    :show-inheritance:

emdbXMLTranslator.emdb_xml_translate module
-------------------------------------------

.. automodule:: emdbXMLTranslator.emdb_xml_translate
    :members:
    :undoc-members:
    :show-inheritance:

emdbXMLTranslator.process_all_19_19 module
------------------------------------------

.. automodule:: emdbXMLTranslator.process_all_19_19
    :members:
    :undoc-members:
    :show-inheritance:

emdbXMLTranslator.process_all_19_20 module
------------------------------------------

.. automodule:: emdbXMLTranslator.process_all_19_20
    :members:
    :undoc-members:
    :show-inheritance:

emdbXMLTranslator.process_all_20_19 module
------------------------------------------

.. automodule:: emdbXMLTranslator.process_all_20_19
    :members:
    :undoc-members:
    :show-inheritance:

emdbXMLTranslator.translator_test module
----------------------------------------

.. automodule:: emdbXMLTranslator.translator_test
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: emdbXMLTranslator
    :members:
    :undoc-members:
    :show-inheritance:
