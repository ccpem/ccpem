#!/usr/bin/env python
"""
setup.py

Create a package for the translator

TODO:


Version history:

                

Copyright [2014-2016] EMBL - European Bioinformatics Institute
Licensed under the Apache License, Version 2.0 (the
"License"); you may not use this file except in
compliance with the License. You may obtain a copy of
the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on
an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied. See the License for the
specific language governing permissions and limitations
under the License.
"""


__author__ = 'Ardan Patwardhan'
__email__ = 'ardan@ebi.ac.uk'
__date__ = '2014-12-03'

from setuptools import setup

def readme(fname):
    with open(fname) as f:
        return f.read()
    
setup(name='emdbXMLTranslator',
      version='0.19',
      description='EMDB file conversion utilities',
      long_description=readme('README'),
      url='http://pdbe.org',
      author='Ardan Patwardhan',
      author_email='ardan@ebi.ac.uk',
      license='Apache License Version 2.0',
      packages=['emdbXMLTranslator'],
      zip_safe=False,
      install_requires=['lxml>3.0'])
