#!/usr/bin/env python

##
# Rewrite of the original csh script of the same name
##
# by ccpem

import os.path, sys, subprocess, FindEM_functions

class main(FindEM_functions.Functions):

  def __init__(self, errorsdict, numerrors):

    #check valid number of arguments - shouldn't be a problem since calling from gui
    if(len(sys.argv) != 11): #scale and scale-factor don't need to be given to this script
      print "You have not submitted the correct number of arguments"
      sys.exit()

    self.errors = errorsdict
    self.err_count = numerrors
    arg_string = self.make_args()

    suffixes = ["A","B","CA","CB"]

    for var in suffixes:
      ftc = sys.argv[11] + "/coords" + var + ".xyz"
      print ftc

      #if(os.path.isfile(ftc)):
      try:
        os.remove(ftc)
        print ftc + " deleted successfully"
      except OSError:
        if(os.path.isfile(ftc)):  # include this check after the try - good practice?
          ftc + " couldn't be deleted"
        else:
          ftc + " didn't exist"
        pass

    self.runSubscript('FindEM_filter_coords', arg_string)
    
    print "Filtering completed!"

    if(len(self.errors)>0):
      print "however, the program found issues you may want to examine:"
      #only 1 program, therefore only 1 subarray in errors array
      for i in self.errors[0][1]:
        print "  " + str(i)
        print "\n"
        
if __name__ == "__main__":
  errors = {}
  errors_count = 0
  main(errors, errors_count)
