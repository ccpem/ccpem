!C*   FindEM programs  ***************************************************
!C                                                                       *
!C    Programs for automatic particle finding in electron micrographs.   *
!C                                                                       *
!C                                                                       *
!C************************************************************************
!C   Copyright (c) Medical Research Council, Laboratory of Molecular     *
!C    Biology.    All rights reserved.                                   *
!C                                                                       *
!C  All files within this package, unless otherwise stated, are Copyright*
!C  (c) Medical Research Council 2002. Redistribution is forbidden.      *
!C   This program was written by Alan Roseman at the MRC Laboratory of   *
!C   Molecular Biology, Hills Road, Cambridge, CB2 2QH, United Kingdom.  *
!C                                                                       *
!C   MRC disclaims all warranties with regard to this software.          *
!C                                                                       *
!C************************************************************************

! FindEM filter coords v2
! extract 'good' particle from the list "cleansortedrawcoordsnnn.xyz"
!in: threshold for ccc A,B, diameter A, diameter B, peak width, sampling, codeA, codeB,maxnumA,maxnumB
! also reads the correalation map for A.
!out: coordinates in xyz, 4 files, those in set A, or B, or rejected from A, or B. 
! Projection matching programs, findem.

! called by the the FindEM_GUI program

!  amr 28/7/00

! AB use to sep t3 and t4's
!AMR 15/2/2001
!quicker version 24/5/2002
! bug fixed 8/02
! increase mask size 26/9/02

!DONE
!  Pass folder name from GUI to this script so that any folder can be used 


      Module image_arrays
      Real, dimension (:,:,:), allocatable :: map1,image1a, image1b,image2,map2, projections,maxmap2,pmap,wmap,smap
      Real, dimension (:,:), allocatable :: cccmap1,cccmap2,current_image,search_object,image_object,pvectors,maxmap
      Real, dimension (:), allocatable :: array,products
      End Module image_arrays

      Program MAIN
      Use  image_arrays
    
   
      real scale,pw
   
      Character*250 imagefilename,cccfilename2,relaxfile,pmapname,wmapname,cccmaxmap,smapname
      !Character(:), allocatable :: cccfilename1
      Character*250 :: cccfilename1
      Character(len=250) :: projectFolder
      Real threshold,sampling,real_sampling,amaxr,aminr,bmaxr,bminr,defocus,downsize,abminr,abmaxr
      real diameter, rot_matrix(2,2)
      Integer iteration, search_radius, fnamel

      Integer nx,ny,nz,err
   
      Real ccc_max,w_max,p_max,x_max,y_max
!	character*3 num
	integer runcode,i,j,acount,bcount,n,p,counter,ccounter
	logical flag
	real val,xyz(3)
	integer x,y,px,py
	equivalence(xyz(1),x),(xyz(2),y),(xyz(3),z)
	real alist(1000000,4),blist(1000000,4)
	integer a,b
	real thresholdA,thresholdB
	integer codeA,codeB,maxnumA,maxnumB
  
	integer numinmask_pw,mask_pw(2,10000),numinmask_a,maska(2,10000),numinmask_b,maskb(2,10000)
	real g

       Write (6,*) 'Filter coords/extract program AB:'
    
       Write (6,*) '_________________________________'


! 1. Read in filenames and parameters 

      Write (6,*) 'Enter the folder path for the current project: '
      Read(*,'(a)') projectFolder
      print*, projectFolder !n.b. write(*,'(a)') works but  print(*,'(a)') and write(*,*) don't... Also don't know why it adds in a line break!

      Write (6,*) 'Enter a threshold for ccc A: '
      Read*,         thresholdA
      print*, thresholdA
      
      Write (6,*) 'Enter a threshold for ccc B: '
      Read*,         thresholdB
      print*, thresholdB
 
    
      Write (6,*) 'Enter d particles (A) for A str '
      Read*,         amaxr
      print*,         amaxr      
      
      Write (6,*) 'Enter d particles (A) for B str '
      Read*,        bmaxr
      print*,        bmaxr
      
      print*, 'Enter peak width (A)'
      read *,pw
      print*,pw
      
      Write (6,*) 'Enter sampling '
      Read*,         sampling
        print*,         sampling 
        
        print*,'Enter codeA'
        read*,codeA
        print*,codeA
        
        print*,'Enter codeB'
        read*,codeB
        print*,codeB
        
        print*,'Enter maxnumA'
        read*,maxnumA
        print*,maxnumA
        
        print*,'Enter maxnumB'
        read*,maxnumB
        print*,maxnumB
        
        !fnamel=LEN(TRIM(ADJUSTL(projectFolder))//'/cccmaxmap'//num(codeA)//'.mrc')
        !allocate(character(LEN=fnamel) :: cccfilename1)
        cccfilename1=trim(adjustl(projectFolder))//'/cccmaxmap'//num(codeA)//'.mrc'

        print*,codeA,cccfilename1
 	
 	!3 MAIN loop
      call readmrcheader(1,cccfilename1,nx,ny,nz,err)
      if (err.gt.0) goto 9000
      

          allocate(image1a(0:nx-1,0:ny-1,0:nz-1),image1b(0:nx-1,0:ny-1,0:nz-1),image2(0:nx-1,0:ny-1,0:nz-1))
	image1a=0
	image1b=0
	image2=0
	
100    format (A50)
110    format (A80)
120    format (G12.5)
130    format (3G12.5)
140    format (2G12.5)
    

! 2 initialise other variables

	scale = 1

	alist = -1000
	blist = -1000 

	
!3 MAIN loop
   
      	call set_up_mask(mask_pw,numinmask_pw,pw,sampling)
	print*,numinmask
   
	print*,'A'
	if (codeA .gt.0) then 
	
     	open(1,file=trim(adjustl(projectFolder))//'/cleansortedrawcoords'//num(codeA)//'.xyz')
! get ccc's above threshold

	flag=.true.
	acount=0
	do while  (flag)
		read(1,*,err=800) val,x,y
		
		if (val.ge.thresholdA) then
				acount=acount+1	
				alist(acount,1)=x
				alist(acount,2)=y
				alist(acount,3)=val
				alist(acount,4)=0.
				
!				print*,acount,x,y,val
		else
				flag=.false.
		endif
	enddo
800	continue
	close(1)



!	map footprint of particles at  the peak to radius of particle size

!	1. shoulders of A to go.
	do n=1,acount
		val=alist(n,3)
		x=alist(n,1)
		y=alist(n,2)
		
		flag = .false.
		do i=1,numinmask_pw
!			print*,n,x,y
				px=x+mask_pw(1,i)
				py=y+mask_pw(2,i)
				if ((px.ge.0).and.(px.le.nx-1).and.(py.ge.0).and.(py.le.ny-1)) then 
				
												
g=image1a(px,py,0)
												if (val.lt.g) then 
													alist(n,4)=-500
													flag = .true.
												endif
				endif
		enddo	
		if ( .not. flag) then
			do i=1,numinmask_pw
				px=x+mask_pw(1,i)
				py=y+mask_pw(2,i)
				if ((px.ge.0).and.(px.le.nx-1).and.(py.ge.0).and.(py.le.ny-1)) then 
												image1a(px,py,0)=val
				endif		
			enddo	
		endif		     
			
	enddo
	
	endif




! now read the  ccc list for the other image









	print*,'B'	
	if (codeB.gt.0) then 
      
 !      allocate(image(nx,ny,nz))
 
     	open(2,file=trim(adjustl(projectFolder))//'/cleansortedrawcoords'//num(codeB)//'.xyz')

! get ccc's above threshold

	flag=.true.
	bcount=0
	do while (flag)
		read(2,*,err=801) val,x,y
		if (val.ge.thresholdB) then
				bcount=bcount+1	
				blist(bcount,1)=x
				blist(bcount,2)=y
				blist(bcount,3)=val
				blist(bcount,4)=0.
				
!				print*,bcount,x,y,val
		else
				flag=.false.
		endif
	enddo
801      continue
! eliminate shoulders

!	2. shoulders of B to go.
	do n=1,bcount
		val=blist(n,3)
		x=blist(n,1)
		y=blist(n,2)
		
		flag = .false.
		do i=1,numinmask_pw
				px=x+mask_pw(1,i)
				py=y+mask_pw(2,i)
				if ((px.ge.0).and.(px.le.nx-1).and.(py.ge.0).and.(py.le.ny-1)) then 
										g=image1b(px,py,0)
										if (val.lt.g) then 
											blist(n,4)=-500
											flag = .true.
										endif
				endif
		enddo	
		if (.not. flag ) then
			do i=1,numinmask_pw
				px=x+mask_pw(1,i)
				py=y+mask_pw(2,i)
				if ((px.ge.0).and.(px.le.nx-1).and.(py.ge.0).and.(py.le.ny-1)) then 
												image1b(px,py,0)=val	
				endif	
			enddo	
		endif		     
			
	enddo
	endif
	
! now sort out coincing peaks from A and B.
! run through 1 list and look for problems

	print*,'aXb'
	
!do alist first
	do n=1,acount
		val=alist(n,3)
		x=alist(n,1)
		y=alist(n,2)
		if (alist(n,4).ge.0) then
				
				if (val.le.image1b(x,y,0)) alist(n,4)=-600
				
		endif
	enddo
	print*,'a'						
! now the blist	
	do n=1,bcount
		val=blist(n,3)
		x=blist(n,1)
		y=blist(n,2)
		if (blist(n,4).ge.0) then
				
				if (val.le.image1a(x,y,0)) blist(n,4)=-600
				
		endif
	enddo
							
		print*,'b'				
		





! now go through the lists and maps to perform the filtering and elimination
! now do one combined filter in the map method to check for to close in:  A, AXB
! and B, BXA



	if (codeA.ge.0) then

	call set_up_mask(maska,numinmask_a,amaxr,sampling)
	print*,numinmask

!	map footprint of particles A at  the peak to radius of particle size
	
!	make the mask for A
	do n=1,acount
		if (alist(n,4).lt.0)  cycle
		val=alist(n,3)
		x=alist(n,1)
		y=alist(n,2)

		do i=1,numinmask_a
				px=x+maska(1,i)
				py=y+maska(2,i)
				if ((px.ge.0).and.(px.le.nx-1).and.(py.ge.0).and.(py.le.ny-1)) then 
											g=image2(px,py,0)
											image2(px,py,0)=g+1
				endif
				
		enddo
		
		
	enddo
	endif	

	if (codeB.ge.0) then
	call set_up_mask(maskb,numinmask_b,bmaxr,sampling)
	print*,numinmask

!	map footprint of particles B at  the peak to radius of particle size

!	make the mask for B
	do n=1,bcount
		if (blist(n,4) .lt.0)  cycle
		val=blist(n,3)
		x=blist(n,1)
		y=blist(n,2)


		do i=1,numinmask_b
				px=x+maskb(1,i)
				py=y+maskb(2,i)
				if ((px.ge.0).and.(px.le.nx-1).and.(py.ge.0).and.(py.le.ny-1)) then 
											g=image2(px,py,0)
											image2(px,py,0)=g+1
				endif
				
		enddo
			
	enddo
	endif



!	now check for clashes for A list

	if (codeA.ge.0) then
	do n=1,acount
		if (alist(n,4) .lt.0)  cycle
		val=alist(n,3)
		x=alist(n,1)
		y=alist(n,2)


		do i=1,numinmask_a
				px=x+maska(1,i)
				py=y+maska(2,i)
				if ((px.ge.0).and.(px.le.nx-1).and.(py.ge.0).and.(py.le.ny-1)) then 
												g=image2(px,py,0)
												if (g.ge.2)  alist(n,4)=2 
				endif
		enddo
		
		
	enddo
	endif


!	now check for clashes for B list

	if (codeB.ge.0) then
	do n=1,bcount
		if (blist(n,4) .lt.0)  cycle
		val=blist(n,3)
		x=blist(n,1)
		y=blist(n,2)


		do i=1,numinmask_b
				px=x+maskb(1,i)
				py=y+maskb(2,i)
				if ((px.ge.0).and.(px.le.nx-1).and.(py.ge.0).and.(py.le.ny-1)) then 
												g=image2(px,py,0)
												if (g.ge.2)  blist(n,4)=2 
				endif
				
		enddo
		
		
	enddo
	endif	








	
!now write the list

	
	if (codeA.gt.0 ) then
	open(1,file=trim(adjustl(projectFolder))//'/coordsA.xyz', status='unknown')	
	open(2,file=trim(adjustl(projectFolder))//'/coordsallA.xyz', status='unknown')	
	open(3,file=trim(adjustl(projectFolder))//'/coordsCA.xyz', status='unknown')
	counter=0
	ccounter=0
	if (maxnumA.lt.acount) acount = maxnumA
	do n=1,acount
		if (alist(n,4).eq.0 ) then	
				counter=counter+1
				write(1,111) int(alist(n,1)),int(alist(n,2)),alist(n,3),counter,alist(n,4)
		endif
	write(2,111) int(alist(n,1)),int(alist(n,2)),alist(n,3),n,alist(n,4)
	if (alist(n,4).ge.1 ) then	
				ccounter=ccounter+1
				write(3,111) int(alist(n,1)),int(alist(n,2)),alist(n,3),ccounter,alist(n,4)
		endif


	enddo
	close(1)
	close(2)
	close(3)
	endif
	
	if (codeB.gt.0 ) then
	open(1,file=trim(adjustl(projectFolder))//'/coordsB.xyz', status='unknown')	
	open(2,file=trim(adjustl(projectFolder))//'/coordsallB.xyz', status='unknown')
	open(3,file=trim(adjustl(projectFolder))//'/coordsCB.xyz', status='unknown')	
	 counter=0
	 ccounter=0
	 if (maxnumB.lt.acount) acount = maxnumB
	do n=1,bcount
		if (blist(n,4).eq.0 ) then	
				counter=counter+1
				write(1,111) int(blist(n,1)),int(blist(n,2)),blist(n,3),counter,blist(n,4)
		endif
	write(2,111) int(blist(n,1)),int(blist(n,2)),blist(n,3),n,blist(n,4)
	if (blist(n,4).ge.1 ) then	
				ccounter=ccounter+1
				write(3,111) int(blist(n,1)),int(blist(n,2)),blist(n,3),ccounter,blist(n,4)
		endif
	enddo
	close(1)
	close(2)	
	close(3)
111     format(2i10,g20.5,i10,4x,g8.2,f6.2)			
	endif			




! normal exit
	close(1)
      print*,'Program finished O.K.'
 
      goto 9998

! Errors
9000   continue
	print*, 'Image read error.'
9998   continue
         
         
      CONTAINS   
 



      subroutine rad(deg,r)
      real deg,pi,r
      parameter (pi=3.1415927)

      r = deg/180.*pi

      return
      end subroutine rad


      Subroutine readmrcheader(stream,filename,nx,ny,nz,err)
      Integer stream,err
      Character*250 filename


	
! mrc file stuff
        INTEGER NX,NY,NZ
        DIMENSION ALINE(8192),NXYZ(3),MXYZ(3),NXYZST(3)
        DIMENSION IXYZMIN(3),IXYZMAX(3),OUT(8192)
        DIMENSION LABELS(20,10),CELL(6)
        COMPLEX CLINE(4096),COUT(4096)
        CHARACTER*250 TITLE
        EQUIVALENCE  (ALINE,CLINE), (OUT,COUT)
        EQUIVALENCE (IXYZMIN, IXMIN), (IXYZMAX, IXMAX) 
        DATA NXYZST/3*0/, CNV/57.29578/


        err=0
       Call imopen(stream,filename,'RO')
       CALL IRDHDR(stream,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
       If (mode.lt.0 .or.mode.gt.2) err=1
 
	nx=nxyz(1)
	ny=nxyz(2)
	nz=nxyz(3)      	

      Return
      End subroutine readmrcheader

      subroutine read_mrcimage(stream,image,NX,NY,NZ,err)
    

      Integer stream,err,ix,iy,iz
       real image(:,:,:)


     	
! mrc file stuff
        INTEGER NX,NY,NZ 
        DIMENSION ALINE(8192),NXYZ(3),MXYZ(3),NXYZST(3)
        DIMENSION IXYZMIN(3),IXYZMAX(3),OUT(8192)
        DIMENSION LABELS(20,10),CELL(6)
        COMPLEX CLINE(4096),COUT(4096)
        CHARACTER*250 TITLE
        EQUIVALENCE  (ALINE,CLINE), (OUT,COUT)
        EQUIVALENCE (IXYZMIN, IXMIN), (IXYZMAX, IXMAX) 
        DATA NXYZST/3*0/, CNV/57.29578/

	err=0
      print*,NX,NY           	      	
!     read in file 
	DO 400 IZ=1,NZ
      DO 350 IY = 1,NY
            CALL IRDLIN(stream,ALINE,*998)
            image(1:NX,IY,IZ) = ALINE(1:NX)
!	 image(NX+1:NX+2,IY,IZ)=0.0

350         CONTINUE   
400   CONTINUE


	

	call imclose(stream)

      return
998   STOP 'Error on file read.'
      end subroutine read_mrcimage

 

!C ************************************************

        function num(number)
        character(len=3) num
  
        
        integer number,order
        real fnum,a
        integer hun,ten,units
        character*1 digit(0:9)

        digit(0)='0'
        digit(1)='1'
        digit(2)='2'
        digit(3)='3'
        digit(4)='4'
        digit(5)='5'
        digit(6)='6'
        digit(7)='7'
        digit(8)='8'
        digit(9)='9'

        fnum=float(number)
        fnum=mod(fnum,1000.)
        hun=int(fnum/100.)
        a=mod(fnum,100.)
        ten=int(a/10.)
        units=mod(fnum,10.)
!C       print *,hun,ten,units


        num=digit(hun)//digit(ten)//digit(units)  
        return
        end function
       
	real function dist(coorda,coordb)
	real coorda(4),coordb(4),dx,dy
	
	dx=coorda(1)-coordb(1)
	dy=coorda(2)-coordb(2)
		
	dist=sqrt(dx**2 + dy**2)


	end function dist 
	
	
	
	
	       
	subroutine set_up_mask(mask,numinmask,min_peaksize,sampling)
        integer numinmask
        real min_peaksize,sampling
        integer mask(2,10000)
        integer x,y,i,rad

	mask=0
        rad=int((min_peaksize/sampling/2)+1)

        i=0
        do x=-rad,rad

                do y=-rad,rad
                        
                 
                                if (radius(x,y).le.rad) then
                                                        i=i+1
                                                        mask(1,i)=x
                                                        mask(2,i)=y
                                                  
                                endif
                enddo
        enddo
        numinmask=i
     
        return

        end subroutine


        real function radius(x,y)
        integer x,y
        real h
        h=float(x**2 + y**2)
        radius=sqrt(h)


        end function radius


       END PROGRAM MAIN
