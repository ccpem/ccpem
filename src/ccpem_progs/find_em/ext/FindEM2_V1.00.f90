!=******************************************************************************
!=* FINDEM2_V1.00.f90                		  AUTHORS: S.DOLGOBRODOV       *
!=*                                    		  AUTHORS: A.ROSEMAN           *
!=*									       *
!=* FindEM2 - Semi-automated particle selection program.		       *
!=* This program performs a template based alignment of a template             *
!=* against an image using the locally normalised correlation coefficient.     *
!=* It is for the purpose of finding particles in electron micrographs.	       *
!=*									       *
!=* Copyright (C) 2012 The University of Manchester 			       *
!=*                                                                   	       *
!=*                                                                            *
!=*    This program is free software: you can redistribute it and/or modify    *
!=*    it under the terms of the GNU General Public License as published by    *
!=*    the Free Software Foundation, either version 3 of the License, or       *
!=*    (at your option) any later version.                                     *  
!=*        								       *
!=*    This program is distributed in the hope that it will be useful,         *
!=*    but WITHOUT ANY WARRANTY; without even the implied warranty of          *
!=*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
!=*    GNU General Public License for more details.                            *
!=* 									       *
!=*    You should have received a copy of the GNU General Public License       *
!=*    along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
!=* 									       *
!=*    For enquiries contact:						       *
!=*    									       *
!=*    Alan Roseman  							       *
!=*    Faculty of Life Sciences						       *
!=*    University of Manchester						       *
!=*    The Michael Smith Building					       *
!=*    Oxford Road							       *
!=*    Manchester. M13 9PT						       *
!=*    Email:Alan.Roseman@manchester.ac.uk    				       *
!=*									       *
!=*   									       *
!=******************************************************************************
!
!
!  PURPOSE:  Programs for automatic particle finding in electron 
!            micrographs.
!
! FindEM2 is new f90 code. Uses fftw libraries.
! It has assymetric binary masks.
! 
! amr May 2012, Sept 2012. This nonSK version for direct particle
! picking from electron micrographs.
!
! Publication to cite: DOLGOBRODOV and ROSEMAN (2013)
!
!*******************************************************************************

! Example of an input file:
!
! "/home/user/KLH/klh1-512.mrc"                       !  Input image file name
! "/home/user/for/mrcw/sideview-templateNEG.mrc"      !  Template image file name
!min val for template
!sampling (Anstroms)
!diameter of radial mask to apply, in A.              !  Enter large value for none. 0 or neg = none applied.
!runcode
!angle st, angle max,step
! "/home/user/for/mrcw/binmask_in.mrc"                !  Binary mask file name
!(EOF)
!***********************************************************************!


program FindEM2
! vmap version, amr oct2012
! FindEMnonskversion amr oct2012
! For FindEM2 particle picking compatibility, Weighted mask disabled.
! Next version will have Wmasks. 
  
  use mrc_image
  use Weighted_vs !, only  : selectsize, inimg, imgpad, rotate_pad, imgcut, outimg, &
  !       imageFilename, maskfilename, pi, outfilename, weighting, templFilename, normm, CM
  implicit none
 
  integer ( kind = 8 ) ::  M, N,Mt,Nt,Kt,Ki, iThrds=8   ! image & template, weight sizes, no. threads for parallel processing
  real  ( kind = 8 ), dimension (:,:), pointer :: img, templ3, binmask, W, templ2
! real  ( kind = 8 ), dimension (:,:,:), pointer :: imgST,OmMAXST,cwST
  real  ( kind = 8 ), dimension (:,:,:), pointer :: imgST,OmMAXST,cwST
  real  ( kind = 8 ), dimension (:,:),allocatable :: Om, OmMAX, stackIND, imginout, cw,templ,W2,M2
  real  ( kind = 8 ), dimension (:,:),allocatable :: vmap

  real  ( kind = 8 ) ::   angle_step=0, max_angle=0,angleinit=0, s, eps=0.5,sampling=0,minv=0,diameter
  CHARACTER(LEN=80)  :: tempfn, Anglefile, INDfile 
  integer :: i,j,k,runcode

  weighting = .false.


  print*,'FindEM2'
  print*,'-------'
  print*,'V1.0, Oct 2012'
  print*,'By Dolgobrodov and Roseman'
  print*,'--------------------------'

  read *,imagefilename; imagefilename=TRIM(imagefilename); print *,'INPUT IMAGE:',imagefilename

  imgST => inimgST(imagefilename);  M = NX; N = NY;Ki=Nz

  if (Ki.ne.1) stop "This version for single input image."

  read *,templFilename; templfilename=TRIM(templfilename); print *,'TEMPLATE IMAGE:',templfilename
 
  read *,minv
  read *,sampling
  read *,diameter
  read *,runcode
  read *,angleinit,max_angle,angle_step

  read *,maskfilename; maskfilename=TRIM(maskfilename); print *,'MASK IMAGE:',maskfilename
 ! read *,Wfilename; Wfilename=TRIM(Wfilename); print *,'WEIGHT IMAGE:  ',Wfilename
 ! read *,outfilename; 

   outfilename = "cccmaxmap"//num(runcode)//'.mrc'
   outfilename=TRIM(outfilename); print *,'OUTPUT IMAGE:  ',outfilename

#  read*, angle_step, max_angle ;
   print *,'Angle init (deg):  ',angleinit,' Angle step:  ',angle_step,' Angle max:  ',max_angle
#  read*,Anglefile; Anglefile = trim(Anglefile);  print *,'MaxAngle IMAGE:  ',Anglefile

   Anglefile="wmap"//num(runcode)//'.mrc'
   Anglefile = trim(Anglefile);  print *,'MaxAngle IMAGE:  ',Anglefile

  !read*,INDfile; INDfile = trim(INDfile);  print *,'MaxIND IMAGE:  ',INDfile
  !read*,eps;   print*,"eps= ",eps
  eps=0.0

  allocate(Om(1:M, 1:N), OmMAX(1:M, 1:N), stackIND(1:M, 1:N), imginout(1:M, 1:N), cw(1:M, 1:N))
  allocate( OmMAXST(1:M, 1:N, 1:ki), cwST(1:M, 1:N, 1:ki))!,templ(1:M, 1:N))!,w2(1:M, 1:N),m2(1:M, 1:N))
  allocate(vmap(1:M, 1:N))

  !templST => inimgST(templfilename);  ! Initial stack of templates

  templ2 => inimg(templfilename)
  Mt = NX; Nt = NY; Kt = NZ   ! Template sizes
  !W => inimg(Wfilename);
 ! W=1

  allocate(templ(1:Mt, 1:Nt),w2(1:Mt, 1:Nt),m2(1:Mt, 1:Nt)) !templ and masks should be same size

  binmask => inimg(maskfilename)

#some checks before launch of cm routine.


   if (max_angle.lt.angleinit) max_angle=angleinit


   vmap=0

  do k=1, Ki
  cw = -1000.
  print*,k
     templ = templ2
     img => imgST(:,:,1)
     imginout = img
     imginout = max(imginout,minv)

      M2=binmask
#apply circular mask to the mask
      call Cmask(M2,diameter/sampling/2.)

!      W2=W
      W2=1.
     call CM( imginout, templ, M2, W2, Om, angle_step, max_angle, eps,iThrds,vmap)
     print*,'cw ',k
     forall ( i=1:M, j=1:N, imginout(i,j) >= cw(i,j) )
        cwST(i,j,k) = imginout(i,j)
        OmMAXST(i,j,k) = Om(i,j)
!        stackIND(i,j) = k
     end forall
  enddo

  call outimgST(cwST,outfilename)

  call outimgST(OmMAXST,Anglefile)
  call outimg(vmap,"vmap.mrc")

CONTAINS

        function num(number)
        character(len=3) num
        integer number,order
        real fnum,a
        integer hun,ten,units
        character*1 digit(0:9)

        digit(0)='0'
        digit(1)='1'
        digit(2)='2'
        digit(3)='3'
        digit(4)='4'
        digit(5)='5'
        digit(6)='6'
        digit(7)='7'
        digit(8)='8'
        digit(9)='9'

        fnum=float(number)
        fnum=mod(fnum,1000.)
        hun=int(fnum/100.)
        a=mod(fnum,100.)
        ten=int(a/10.)
        units=mod(fnum,10.)
!C       print *,hun,ten,unit
        num=digit(hun)//digit(ten)//digit(units)  
        return
end function num




real function radiusXY(x,y,xc,yc)     
        integer x,y,xc,yc,xx,yy
        real r2
        xx=x-xc
        yy=y-yc
	r2=float(xx**2 +  yy**2)
        radiusXY=sqrt(r2)
        return
end function radiusXY

subroutine Cmask(mask,radius)
 	integer nx,ty,tx
 	real ( kind = 8 ) mask(:,:)
        real ( kind = 8 ) x,y,radius    
        integer msum,i,j,cx,cy 
#       real radiusXY
        
	tx=size(mask,1)
        ty=size(mask,2)

	cx=int(float(tx)/2.)+1
	cy=int(float(ty)/2.)+1
	
        do i=1,tx
          do j=1,ty
!       	print*,i,j
                if (radiusXY(i,j,cx,cy).gt.radius) mask(i,j) = 0.0                      
          enddo
        enddo
       
        return
end subroutine Cmask


end program FindEM2

