!C*   FindEM programs  ***************************************************
!C                                                                       *
!C    Programs for automatic particle finding in electron micrographs.   *
!C                                                                       *
!C                                                                       *
!C************************************************************************
!C   Copyright (c) Medical Research Council, Laboratory of Molecular     *
!C    Biology.    All rights reserved.                                   *
!C                                                                       *
!C  All files within this package, unless otherwise stated, are Copyright*
!C  (c) Medical Research Council 2002. Redistribution is forbidden.      *
!C   This program was written by Alan Roseman at the MRC Laboratory of   *
!C   Molecular Biology, Hills Road, Cambridge, CB2 2QH, United Kingdom.  *
!C                                                                       *
!C   MRC disclaims all warranties with regard to this software.          *
!C                                                                       *
!C************************************************************************

! 11/5 minor syntax edits for g95
!Findem preparation:  Downsizeandfilter2 program:
! deals with 2d stacks of images.
! last edit 5/8/02

        Module maps
        Real, dimension (:,:), allocatable :: M,S,LA
        End module maps

        Module mrc_image
                DIMENSION ALINE(32768),NXYZ(3),MXYZ(3),NXYZST(3),NXYZ2(3)
                DIMENSION IXYZMIN(3),IXYZMAX(3),OUT(32768)
                DIMENSION LABELS(20,10),CELL(6)
                COMPLEX CLINE(16384),COUT(16384)
                CHARACTER*250 INFILE,OUTFILE 
                CHARACTER*250 TITLE, AA 
                INTEGER NX,NY,NZ,NXM1,NYM1,NZM1,NXP1,NXP2                 
                COMMON //NX,NY,NZ,IXMIN,IYMIN,IZMIN,IXMAX,IYMAX,IZMAX
                EQUIVALENCE (NX,NXYZ), (ALINE,CLINE), (OUT,COUT)
                EQUIVALENCE (IXYZMIN, IXMIN), (IXYZMAX, IXMAX)
                DATA NXYZST/3*0/, CNV/57.29578/  
                REAL*8 DMIN,DMAX,DMEAN
        End Module  mrc_image



      Program downsizeandfilter
      Use  maps
      Use  mrc_image

	Real d,sampling
	Real a,c
	integer e,f,dd
      integer I,j,k,l,x,y
      Real resolution
      integer ix,iy,iz
      real val,tot,mean,lp,hp
      integer b,next_ft_size
      real amax,amin,sign
      integer a1,a2,b1,b2,area,bsize
	character*1 nc
	
	
         DATA IFOR/0/,IBAK/1/,ZERO/0.0/
         Character*250 filename

       Print*,'Enter image to operate on:'
       Read*, filename
       Print*,'Enter the output filename:'
       Read*, outfile
       
       Print*,'Enter downsize the factor (an integer):'
       Read*, dd
       print*,dd
       Print*,'Enter filter resolution (A), lp,hp:'
       Read*, lp,hp
       print*, lp,hp
       print*,'Enter the box size for background flattening (A):'
       read*,bsize
       print*,bsize
     
22	continue
       print*, 'Normalise columns? (Y/N)'
       read*,nc
       if (nc.eq.'y') nc='Y'
       if (nc.eq.'n') nc='N'
       if (nc.ne.'Y'.and.nc.ne.'N')  goto 22
       
       Print*,'Enter the sampling of the image (A/pixel):'
       Read*, sampling
       print*, sampling

  	bsize=bsize/(sampling*dd)
  	
  	print*,'Box size is ',bsize,' pixels.'
       bsize=int(bsize/2)+1

	
10	continue
       print*,'Flip sign ? (enter -1 or 1):'
       read*, sign
 	if (abs(sign).ne.1) goto 10
 

!C ***********************************************
!C open input from MRC format file
        
        CALL IMOPEN(3,filename,'RO')
        CALL IRDHDR(3,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
        
       
       
  !      IF (MODE .NE. 2) THEN 
     
   !        WRITE(6,1400)
1400    FORMAT(//' Found non mrc mode 2 file, aborting.  ')
   !        STOP
    !    ENDIF

!C ****************************************************************

!C open input from MRC format file
        
        CALL IMOPEN(4,outfile,'NEW')
        CALL ITRHDR(4,3)
        
        	
!        CALL IALSIZ(4,nxyz2,nxyzst)
        CALL IALMOD(4,2)
        
       
!C ***********************************************
   
         A=max(nx,ny)
         B=int((a+1)/dd)
         
         
          e = next_ft_size(b)
     	  print*,'e=',e
     
          F=e*dd

           Allocate (s(e+2,e),m(f,f),la(e+2,e))
	    M=0
            S=0
	    la=0



	do 800 iz=1,nz

!C Read in image from MRC format file   

        tot=0.0
   	
          DO 350 IY = 1,ny
            CALL IRDLIN(3,ALINE,*999)
            DO 300 IX = 1,nx
               val = ALINE(IX)

               M(IX,IY) = val
                tot=tot+val
300         CONTINUE   
350     CONTINUE
        mean=tot/(nx*ny)
       
        
        print*,'map read.'
        print*,'mean is ',mean


	m(1:nx,1:ny)=m(1:nx,1:ny)-mean
!C ***************************************************
!downsize

	



            Do i=1,e
                 Do j = 1,e
                 K=((I-1)*dd)+1
            	     L=((j-1)*dd)+1
		     S(I,j)=sum(M(K:K+dd-1,L:L+dd-1))/(dd*dd)
		  !   print*,i,j
		enddo
	enddo
	print*,'done downsize.'
   !         deallocate (M)
 
 	if (bsize.gt.1)  then
            Do i=1,e
                 Do j = 1,e
                 	a1=i-bsize
              	        a2=i+bsize
                	b1=j-bsize
                 	b2=j+bsize
                 
                 	a1=max(1,a1)
                 	a2=min(a2,e)
                 	b1=max(1,b1)
                 	b2=min(b2,e)
             		area=(a2-a1+1)*(b2-b1+1)
             		la(i,j)=sum(s(a1:a2,b1:b2))/area
             		
                 
		enddo
		print*,i,j
	     enddo
	
	s=s-la
	print*,'done flatten.'
	endif
	
	! now correct for columns
	if (nc.eq.'Y') then
	  Do i=1,e 
	  	val=sum(s(i:i,1:e))/e
	  	print*,i,val
	  	!print*, s(i:i,1:e)
		s(i:i,1:e)=s(i:i,1:e)-val
		!print*, s(i:i,1:e)
	  enddo
	print*,'done col corn.'
	endif
            call todfft(S,e,e,ifor)
            call filter(sampling*dd,lp,hp)
            call todfft(S,e,e,ibak)
            S=S*sign
            
             x=int(nx/dd)
             y=int(ny/dd)         
	     z=1
	     
	     nxyz2(1)=x
	     nxyz2(2)=y
	     nxyz2(3)=z
    	     CALL IALSIZ(4,nxyz2,nxyzst)
	     CALL IWRHDR(4,TITLE,NTFLAG,aMIN,aMAX,DMEAN)
! writemrcimage(S,x,y)

!
!C ****************************************************************
!C write image to MRC format file
        print*,nz,ny,nx,x,y


        tot=0.0
   
          DO 450 IY = 1,y          
            DO 400 IX = 1,x
        !    print*,ix,iy
            	val=S(ix,iy)
              ALINE(IX)=val
		amin=min(amin,val)
		amax=max(amax,val)
       
                tot=tot+val
400         CONTINUE   
	 CALL IWRLIN(4,ALINE,*999)
450     CONTINUE
        dmean=tot/(x*y)
  	mean =dmean      
  
800	continue
       
        print*,'map written.'
        print*,'mean is ',mean
	CALL IWRHDR(4,TITLE,NTFLAG,aMIN,aMAX,DMEAN)
       call imclose(3)
      call imclose(4)

	print*,'Program finished O.K.'
!C ***************************************************
	stop
999	continue
	print*, 'Read error'
	stop


	end   program
	
	
!    contains

!            subroutine FT

!            end subroutine FT

         

!             end program

  !C***************************************************************************

        SUBROUTINE FILTER(sampling,lp,hp)
	use maps
      
        INTEGER ix,iy,INDEX,nx,ny
        real r,i,j,x,y,nxm1,nym1,nxp2,nxp1
        real cx,cy,lp_cutoff,hp_cutoff,lp,hp,sampling

	print*,'filtering...'
        nx=size(S,1)
        ny=size(S,2)
      
        nx=nx-2

        nxm1=nx-1
        nym1=ny-1
        nzm1=nz-1

        nxp1=nx+1

        cx=int(nx/2) +1
        cy=int(ny/2) +1
        cz=int(nz/2) +1

        lp_cutoff = nx*sampling/lp
        hp_cutoff = nx*sampling/hp

        DO 301 iy=1,ny
        DO 303 ix=1,nx+1,2
        !	print*,ix,iy
        	x=(float(ix-1))/2
!       
		if (iy.le.ny/2) then  
		
					y=iy-1      
				else
					y=ny-iy-1
		endif
					
					  	
                r=sqrt((x*x) + (y*y))
               if (r.ge.lp_cutoff) then
                	S(ix,iy)=(0.0)
                	S(ix+1,iy)=(0.0)
         !      	print*,'lp cut', ix,iy,x,y,r,lp_cutoff
                endif
                if (r.le.hp_cutoff) then
                	S(ix,iy)=(0.0)
               		S(ix+1,iy)=(0.0)
                 endif
                
                
                
303     CONTINUE
301     CONTINUE

        RETURN
        END

!C***************************************************************************

	integer function next_ft_size(x)
	logical ftsize
	integer x
	real t
	
	
	t=float(x)
	if (int(t/2).ne.(t/2)) x=x+1
	do while (.not.ftsize(x))
		
		x=x+2
	enddo
	next_ft_size=x
	return
	
	end function next_ft_size
	
!C***************************************************************************	
	logical function ftsize(x)
	integer x,n,a
	
	real b
	real primes(8)
	primes=(/2,3,5,7,11,13,17,19/)
	
	ftsize = .false.
	
	a=x
	do n=1,8
100 	continue
	b=float(a)/primes(n)
	
	
	if (a.eq.0) stop 'error in primes'
	if (b.eq.int(b)) then
				a=int(b)
				goto 100
	endif
	enddo
	
	if (a.eq.1) then 
				ftsize=.true.
				else
			       ftsize=.false.
	endif 
	
	return
	
	end function ftsize
	
!C***************************************************************************	
	
	
	
	
	
