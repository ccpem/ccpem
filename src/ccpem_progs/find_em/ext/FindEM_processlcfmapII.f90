!C*   FindEM programs  ***************************************************
!C                                                                       *
!C    Programs for automatic particle finding in electron micrographs.   *
!C                                                                       *
!C                                                                       *
!C************************************************************************
!C   Copyright (c) Medical Research Council, Laboratory of Molecular     *
!C    Biology.    All rights reserved.                                   *
!C                                                                       *
!C  All files within this package, unless otherwise stated, are Copyright*
!C  (c) Medical Research Council 2002. Redistribution is forbidden.      *
!C   This program was written by Alan Roseman at the MRC Laboratory of   *
!C   Molecular Biology, Hills Road, Cambridge, CB2 2QH, United Kingdom.  *
!C                                                                       *
!C   MRC disclaims all warranties with regard to this software.          *
!C                                                                       *
!C************************************************************************

! Find_EM particle finding program
! FindEM_processlcfmap.f90 PART II - split to make use of the fast unix sort.
! extracts coordinates from the LCF maps output by the correlation program FindEM_FLCF.
! 
!INPUT: cccmaxmap 
!OUTPUT: rawcoords00n.xyz

! Projection matching programs, findem.
! quick version of get CCC's,
!gets all gt .2, then sorts. just does 1 cccmax file at a time
! 25/10/01
!  amr 28/7/00

! AB used to sep t3 and t4's
!AMR 15/2/2001

! minimum peak width of 50 A imposed - AMR 1/6/2002

      Module image_arrays
      Real, dimension (:,:,:), allocatable :: map1,image, map2, projections,maxmap2,pmap,wmap,smap
      Real, dimension (:,:), allocatable :: cccmap1,cccmap2,current_image,search_object,image_object,pvectors,maxmap
      Real, dimension (:,:), allocatable :: alist
      Real, dimension (:), allocatable :: array,products
      End Module image_arrays

      Program MAIN
      Use  image_arrays
    
   
      real min_peaksize,threshold
      parameter (min_peaksize=50.0 ,threshold=0.2)
! min_peaksize is the minimum peak size as radius in angstroms. shoulder values within this radius will be ignored.      
      
      integer numinmask,mask(2,10000)	
      	
      real scale
   
      Character*250 imagefilename,cccfilename1,cccfilename2,relaxfile,pmapname,wmapname,cccmaxmap,smapname
      Real sampling,real_sampling,amaxr,aminr,bmaxr,bminr,defocus,downsize,abminr,abmaxr
      real diameter, rot_matrix(2,2)
      Integer iteration, search_radius

      Integer nx,ny,nz,err,count
   
        Real ccc_max,w_max,p_max,x_max,y_max
!	character*3 num
	integer runcode,i,j,acount,bcount,n,p,counter
	logical flag
	real val,xyz(3)
	integer x,y
	equivalence(xyz(1),x),(xyz(2),y),(xyz(3),z)
	
	integer a,b,code
	real maxnum
	integer ps2
  

       Write (6,*) 'FindEM_processlcfmapII'
       Write (6,*) '----------------------'
           


! 1. Read in filenames and parameters 
      Write (6,*) 'Enter the code for the LCF map file (1-999):'
      Read *,code
 	cccfilename1='cccmaxmap'//num(code)//'.mrc'
 	print*,code,cccfilename1
 	print*,'Enter the sampling of the correlation map:'
 	read*,sampling
 	print*,sampling
 


100    format (A50)
110    format (A80)
120    format (G12.5)
130    format (3G12.5)
140    format (2G12.5)
    
    	scale=1

! 2 initialise other variables

	mask=0
	
	
!3 MAIN loop
      err=0
      print*,"B4readheader"
      call readmrcheader(1,cccfilename1,nx,ny,nz,err)
      if (err.gt.0) goto 9000
     
      print*,"readheader"

      
       allocate(image(0:nx-1,0:ny-1,0:nz-1))

              
       allocate(alist(nx*ny,3))
       alist = 0
 	print*,nx,ny,nz
        call read_mrcimage(1, image, nx,ny,nz,err)
!	call imclose(1)
        print*,'Read image.'
      
        ps2=int(min_peaksize/2)+1
        image(:ps2,:,:)=0
        image(nx-ps2:,:,:)=0
        image(:,:ps2,:)=0
        image(:,ny-ps2:,:)=0

      
      
! get ccc's above threshold
	open(2,file='sortedrawcoords'//num(code)//'.xyz',status='unknown')
	

! now read the list, 
	i=0
	read(2,*) val,x,y,i,count
	do a=2,count
		read(2,*) val,x,y,i,n
		alist(a,1)=val
		alist(a,2)=x
		alist(a,3)=y
	enddo 
	
	close(2)	

	





	call set_up_mask(mask,numinmask,min_peaksize,sampling)
	print*,numinmask




!	mask out shoulders of the peak within min_peaksize

	do n=1,count
		val=alist(n,1)
		x=alist(n,2)
		y=alist(n,3)
		if (image(x,y,0).ge.0.00001) then
						do i=1,numinmask
							image(x+mask(1,i),y+mask(2,i),0)=0.0
						enddo
				      else
						alist(n,1)=0.0
				      endif
!				   print*,val,x,y,alist(n,1)
	enddo
	
	

! now write the list, skip the filtered out ones
	open(2,file='cleansortedrawcoords'//num(code)//'.xyz',status='unknown')
	
	i=0
	do a=1,count
		
		val=alist(a,1)
!		print*,a,val
		if (val.ge.0.000001) then
				i=i+1	
				x=alist(a,2)*scale
				y=alist(a,3)*scale
				write(2,*) val,x,y,i,count
		endif

	enddo 
	close(2)
	
	
	

! normal exit
	close(1)
      print*,'Part II done.'
      print*,'Program finished O.K.'
 
      goto 9998

! Errors
9000   continue
	print*, 'Image read error.'
9998   continue
         
         
      CONTAINS   
   


      Subroutine readmrcheader(stream,filename,nx,ny,nz,err)
      Integer stream,err
      Character*250 filename


	
! mrc file stuff
        INTEGER NX,NY,NZ
        DIMENSION ALINE(8192),NXYZ(3),MXYZ(3),NXYZST(3)
        DIMENSION IXYZMIN(3),IXYZMAX(3),OUT(8192)
        DIMENSION LABELS(20,10),CELL(6)
        COMPLEX CLINE(4096),COUT(4096)
        CHARACTER*250 TITLE
        EQUIVALENCE  (ALINE,CLINE), (OUT,COUT)
        EQUIVALENCE (IXYZMIN, IXMIN), (IXYZMAX, IXMAX) 
        DATA NXYZST/3*0/, CNV/57.29578/


   
       Call imopen(stream,filename,'RO')
       CALL IRDHDR(stream,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
       If (mode.lt.0 .or.mode.gt.2) err=1
 
	nx=nxyz(1)
	ny=nxyz(2)
	nz=nxyz(3)      	

      Return
      End subroutine readmrcheader

      subroutine read_mrcimage(stream,image,NX,NY,NZ,err)
    

      Integer stream,err,ix,iy,iz
       real image(1:nx,1:ny,1:nz)


     	
! mrc file stuff
        INTEGER NX,NY,NZ 
        DIMENSION ALINE(8192),NXYZ(3),MXYZ(3),NXYZST(3)
        DIMENSION IXYZMIN(3),IXYZMAX(3),OUT(8192)
        DIMENSION LABELS(20,10),CELL(6)
        COMPLEX CLINE(4096),COUT(4096)
        CHARACTER*250 TITLE
        EQUIVALENCE  (ALINE,CLINE), (OUT,COUT)
        EQUIVALENCE (IXYZMIN, IXMIN), (IXYZMAX, IXMAX) 
        DATA NXYZST/3*0/, CNV/57.29578/

	
      print*,NX,NY,NZ          	      	
!     read in file 
	DO 400 IZ=1,NZ
      DO 350 IY = 1,NY
            CALL IRDLIN(stream,ALINE,*998)
            image(1:NX,IY,IZ) = ALINE(1:NX)
!	 image(NX+1:NX+2,IY,IZ)=0.0

350         CONTINUE   
400   CONTINUE


	
      print*,'in read_mrcimage'
      call imclose(stream)

      return
998   STOP 'Error on file read.'
      end subroutine read_mrcimage

 

      subroutine write_mrcimage(stream,image, x,y,z,err)
 
      real image(:,:,:)    
      INTEGER stream,IX,IY,err,x,y,z
! mrc file stuff

        INTEGER NX,NY,NZ
        DIMENSION ALINE(8192),NXYZ(3),MXYZ(3),NXYZST(3)
        DIMENSION IXYZMIN(3),IXYZMAX(3),OUT(8192)
        DIMENSION LABELS(20,10),CELL(6)
        COMPLEX CLINE(4096),COUT(4096)   
        DIMENSION LABELS2(20,10),CELL2(6)
        CHARACTER*250 TITLE
        REAL*8 DOUBLMEAN,DOUBLMEAN2,DMEAN,DMEAN2
        EQUIVALENCE  (ALINE,CLINE), (OUT,COUT)
        EQUIVALENCE (IXYZMIN, IXMIN), (IXYZMAX, IXMAX)  
        DATA NXYZST/3*0/, CNV/57.29578/
        
        
        
        CALL IRDHDR(stream,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)

        nx=nxyz(1)
        ny=nxyz(2)     
        nz=nxyz(3)
      
!       write file 
        DO 450 IZ = 1,NZ
        DO 450 IY = 1,NY
            DO 400 IX = 1,NX
                ALINE(IX) = image(IX,IY,IZ)
400         CONTINUE   

      CALL IWRLIN(stream,ALINE,*999)
450   CONTINUE
      call imclose(stream)

      return
999   STOP 'Error on file read.'
      end subroutine write_mrcimage


        subroutine copyaheader(filename1,filename2,stream)
  
        character*250 filename1,filename2
        integer stream
  ! mrc file stuff

        INTEGER NX,NY,NZ
        DIMENSION ALINE(8192),NXYZ(3),MXYZ(3),NXYZST(3)
        DIMENSION IXYZMIN(3),IXYZMAX(3),OUT(8192)
        DIMENSION LABELS(20,10),CELL(6)
        COMPLEX CLINE(4096),COUT(4096)   
        DIMENSION LABELS2(20,10),CELL2(6)
        CHARACTER*250 TITLE
        REAL*8 DOUBLMEAN,DMEAN,DMIN,DMAX
        EQUIVALENCE (NX,NXYZ), (ALINE,CLINE), (OUT,COUT)
        EQUIVALENCE (IXYZMIN, IXMIN), (IXYZMAX, IXMAX)  
        DATA NXYZST/3*0/, CNV/57.29578/
        integer ntflag
         ntflag=-1
                  
        call imopen(4,filename1,'RO')        
        call imopen(stream,filename2,'NEW')
        call irdhdr(4,TITLE,NXYZ,MXYZ,DMIN,DMAX,DMEAN)
        call itrhdr(stream,4)
      !make all out images just 1 layer 4 now.
        nxyz(3)=1
        call IALSIZ(stream,NXYZ,NXYZST) 
        call iwrhdr(stream,TITLE,NTFLAG,DMIN,DMAX,DMEAN)
        
        call imclose(4)
        return
      
        end subroutine copyaheader


!C ************************************************

        function num(number)
        character(len=3) num
  
        
        integer number,order
        real fnum,a
        integer hun,ten,units
        character*1 digit(0:9)

        digit(0)='0'
        digit(1)='1'
        digit(2)='2'
        digit(3)='3'
        digit(4)='4'
        digit(5)='5'
        digit(6)='6'
        digit(7)='7'
        digit(8)='8'
        digit(9)='9'

        fnum=float(number)
        fnum=mod(fnum,1000.)
        hun=int(fnum/100.)
        a=mod(fnum,100.)
        ten=int(a/10.)
        units=mod(fnum,10.)
!C       print *,hun,ten,units


        num=digit(hun)//digit(ten)//digit(units)  
        return
        end function
       
	real function dist(coorda,coordb)
	real coorda(4),coordb(4),dx,dy
	
	dx=coorda(1)-coordb(1)
	dy=coorda(2)-coordb(2)
		
	dist=sqrt(dx**2 + dy**2)


	end function dist
	
	
	subroutine bubble_sort(map,nx,ny,list,n,threshold)
	integer nx,ny,n
	real map(0:nx-1,0:ny-1,0:0)
	Real threshold,list(nx*ny,3)	
	integer I,j,count
	real a
	logical flag

	N=nx*ny
	Count=0
	Do y=0,ny-1
		Do x=0,nx-1
       			 A=map(x,y,0)
       		 	 If (a.ge.threshold) then
             					         Count=count+1
                                        	         List(count,1)=a
                                       			 List(count,2)=x
							 List(count,3)=y
    	                 Endif
  	       Enddo
  	       print*,y
	Enddo
	N=count
	print*,N
!	return
	Do i=1,count-1 
!	print*,'i',i
		Flag=.false.
		
 	       		Do j=1,count-1
        	       
       	 	      	  	If (list(j,1).lt.list(j+1,1)) then
                                				        Call swop(list,n,j)
                                        				Flag=.true.
         	       	  	Endif
    	 	        Enddo
        	        If (.not.flag) then
        					 exit
        	        endif
	Enddo
	
	N=count
	return
	end subroutine

	Subroutine swop(list,n,j)
	integer n,j
	Real list(n,3),a(3) 

	A(1:3)=list(j,1:3)

	list(j,1:3)=list(j+1,1:3)
	list(j+1,1:3)=a(1:3)

	end subroutine



	subroutine set_up_mask(mask,numinmask,min_peaksize,sampling)
	integer numinmask
	real min_peaksize,sampling
	integer mask(2,10000)
	integer x,y,i,rad
		
	rad=int((min_peaksize/sampling)+1)
	
	i=0
	do x=-rad,rad
		
		do y=-rad,rad
			if ((y.eq.0).and.(x.eq.0))  cycle
				if (radius(x,y).le.rad) then
							i=i+1
							mask(1,i)=x
							mask(2,i)=y
							
				endif
		enddo
	enddo
	numinmask=i

	return	

	end subroutine


	real function radius(x,y)
	integer x,y
	real h
	h=float(x**2 + y**2)	
	radius=sqrt(h)


	end function radius

	
       END PROGRAM MAIN
