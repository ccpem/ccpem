FindEM is a program to pick particles from electron micrographs.
It works by using a very sensitive correlation algortihm to correlate
a template image with the digitsed electron micrograph.


There are two main steps: 
1. A correlation map is produced by correlating the image and template.
   The correlations are normalised in the range -1 to 1.
   This part of the program is run on a farm node.
2. The correlation map is interpreted with the aid of a GUI program,
   to optimise the final selection of particles, the coordinates
   of which are saved to a file. 
  

Alan Roseman June 2002.
update 21/11/02 AMR

An example of an image, templates, and the files produced is given in the 
example directory.


Required:
 

1. 	digitized electron micrograph.

2. 	template or series of templates representing the objects
	in the electron micrograph. This can be made by picking a few
	particles manually, possibly with Ximdisp, and averaging them.
	They should preferably be optimally aligned using SPIDER, or another
	image processing system.

Setup:

1.	Add the line:
	source /public/roseman/FindEM.login
	to your .cshrc and .login files.

Steps:

1.	Bandpass filter the EM image and the templates in the range 
	30 A to maximum-dimension-of-a-particle, using the program
	"downsizeandfilter2". Set the downsize scale (an integer)
	to make the resultant sampling about 10 A/pixel.
	The resultant template and image must be at the same scale.   
	   
2.	Run the FindEM program to perform the correlations.
	goFindEM is an example script to launch this program.
	See the example given in the example directory.

3.	Run the goFindEM_processlcfmap script to do some preliminary processing of the
	results. The GUI step uses the files output.
	
	
4.	Convert the downsized and filtered image to gif format using:
	mrc2gif2. The GUI reads the gif format file.
	
5.	Run the GUI program, FindEM_GUI, to display the particles
	found and filter these results (according to parameters such as a
	correlation threshold, and particle diameter), then save the final coordinates
	selection as a file. This file can be used by the mrc program 'LABEL' to cut out 
	the particles from the original scan file.
	
More detail:

downsizeandfilter2
------------------
downsizeandfilter2
enter the image filename to reduce and filter
enter the output filename
enter an integeral downsize factor
enter the band pass filter range in angstroms, e.g. 30.,500. (for no HP-filter enter a large value)
enter a box size for local background subtraction (alternative to HP-filter), 0=no filter applied.
normalise columns (Y/N), option to correct for streaks produced by the Z/I scanner.
enter the sampling of the input image in A/pixel
enter -1 to reverse the contrast or 1 to have it reamin the same (the contrast
of the search template and the image should be the same.  


(about 5 times the particles diameter is good for the local background 
subtraction)


FindEM
------
FindEM
enter the filename of the micrograph scan
enter the filename of the template
enter a numberical value that is the minimum threshold for data to include from
the template. (default: use a number less that the minimum value in your template image, e.g. -9999.)
enter the sampling in A/pixel of the images.
enter the diameter of the particles in A, this is used to
calculate a real space outer cutoff radius for the template image.
enter a run 
code from the range 1-999. This is used to identify the
correlation map and other files produced by the programs.
enter the inplane angular range of the template to be searched. Three values 
are required, start angle, maximum angle, angular interval or step to be used. 


goFindEM_processlcfmap
----------------------
goFindEM_processlcfmap runcode sampling


mrc2gif2
--------
mrc2gif2 'filename of downsized_filtered file'
The extension .mrc is assumed.
The output is the filename.gif


FindEM_GUI
----------
FindEM_GUI
This launches the gui program.
The first step is to choose the image file that the correaltion was
run against, in gif format. This image is displayed.

Now there are several boxes and sliders on the right.
These are, starting from the top:
code A
code B
sampling
scale factor
diameter of particles A.
diameter of particles B.
scale factor for displayed circles.
peak width
ccc threshold for A
ccc threshold for B
Max number of particles A
Max number of particles B.
Update particle positions
clear
Write coordinates A.
Write coordinates B.
New image
Exit 

Enter the parameters, display the selected particles with "Update particle
positions", adjust, display again, repeat till the selection is
optimal. Now reject any particles contaminated with ice, near the edge of 
the carbon, etc, by clicking on the coloured ring with the left mouse button.
These go black in colour. Finally, write the coordinates. The coordinates file 
will be called "FindEMcoordsAn.xyz" or "FindEMcoordsBn.xyz" where n indicates
the runcode, and A or B signifies the designated type , A or B.


There is an option to process two correlation maps at once, usefull
to separate two types of particle, or types of view. If peaks from
the two maps overlap within the peak radius, then the particle
is assigned to the type that gives the higher correlation.

Selected A particles are displayed green. Selected B type particles 
are displayed blue. Rejected A and B particles are red and orange
respectively.

If you are just using one template then you can ignore the options
for B (or alternatively A). 



code A - the runcode for the FindEM correlation with template 'A'.
code B - similar for B.
sampling - the sampling in A/pixel of the image AFTER "downsizeandfilter".
scale factor - a scale factor to be applied to the coordinates before writing,
    usually = the downsize factor used in the "downsizeand filter" program. 
diameter of particles A. - diameter of the particles in A, used to determine
    when particles are too close and should be rejected.
diameter of particles B. -similarly for B
scale factor for displayed circles. - If this is 1 then the circles around the
particles reflect their true diameter, and if they touch then these particles
are rejected. There is the option to scale the displayed circles larger,
for a clearer critical view of the particles.
peak width - values within this diameter are considered to be from the same peak.
ccc threshold for A - correlation values lower than this cutoff are discarded.
ccc threshold for B - similar for B.
Max number of particles A - the number of peaks considered is limited to this
number (not the number in the final selection).
Max number of particles B. - similar for B.
Update particle positions - redraws the circles indicating selected and
rejected particles, according to the latest parameters.
clear - clears the displayed circles.
Write coordinates A. - writes out the list of selected particles of type A
in Ximdisp format, applying the scale factor.
Write coordinates B. - simlilar for B.
New image - this option is not active.
Exit - quit the program.


Other tips:
The search templates should be centered, otherwise the peaks found will appear shifted or
off center from the particles. A SPIDER procedure for this is centre.amr

The peakwidth is set to a minimum of 50 A in the preprocess_lcf program.
This saves time.

The ripples seen on the displayed image are due to the sharp cutoff used
in the Fourier filtering, and the contrast enhancement used so that
the particles can be seen. This may be a bit distracting, but the
final images for processing are extracted from the unaffected image.
An alternative to Fourier HP filtering the the local background
subtraction option in "downsizeandfilter2". Only one of these options needs
to be used. Enter a very large value for the Fourier HP, eg 999999,
if it is not desired.


Use LABEL to cut the particles out of the original scan, using the coordinates
file writen by the 'FindEM_GUI'.



The example
-----------
The example demonstrates how two different templates can be correlated
against an image to separate two different sizes of virus shells.
runcode = 001 and runcode = 002 correspond to the correlation with the
 T=3 or T=4 template respectively.



Constructive criticism appreciated: roseman@mrc-lmb.cam.ac.uk

