Find EM - To Do:

1) downsizeandfilter.f90 / downsizeandfilter2.f90
-> Bin in repository:
        /ccpem/ccpem/bin/
   Should be built from src:
        /ccpem/ccpem/src/ccpem_progs/find_em/
    -> For now... fix ccpem bin path.


2) Move src files to ccpem_progs

3) Remove/replace python shell script rewrite with production code:
    mrc2gif.py
    FindEM_functions.py
    goFindEM_processlcfmap.py
    goFindEM_filtercoords.py

4) Remove duplicate mrc image processing src code:
    mrc2tif.f

5) Move find em readme from doc into ccpem_progs

6) Fix top level Cmakelists.txt
    -> For now remove python file copying etc.
    -> XXX TODO compile from fortran src


FindEM - Processing Steps:

1) Preprocess image
  -> prepareforFindEM.com
        -> Should this be part of image viewer / interactive?
        -> For now just form style gui.



