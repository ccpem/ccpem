!*******************************************************************************
!=* DockEM2-LocalSeachV2.2.f90,  AMR June 2015      							   *
!=******************************************************************************
!=* DOCKEM2.0.f90                		   								       *
!=*                                    		  AUTHOR: A.ROSEMAN  	           *
!=*									    									   *
!=* DockEM2 - Molecular density matching programs.		   					   *
!=* This program performs a molecular density matching a template based        *
!=* alignment of a template motif against a 3D target map,      			   *
!=* using the locally normalised correlation coefficient.    				   *
!=* It is for the purpose of docking domain structures into 3D maps	       	   *
!=*									      	 								   *
!=* Copyright (C) 2015 The University of Manchester 			      		   *
!=*                                                                   	       *
!=*                                                                            *
!=*    This program is free software: you can redistribute it and/or modify    *
!=*    it under the terms of the GNU General Public License as published by    *
!=*    the Free Software Foundation, either version 3 of the License, or       *
!=*    (at your option) any later version.                                     *  
!=*        								       								   *
!=*    This program is distributed in the hope that it will be useful,         *
!=*    but WITHOUT ANY WARRANTY; without even the implied warranty of          *
!=*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
!=*    GNU General Public License for more details.                            *
!=* 									      								   *
!=*    You should have received a copy of the GNU General Public License       *
!=*    along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
!=* 									     								   *
!=*    For enquiries contact:						    					   *
!=*    									  								       *
!=*    Alan Roseman  													       *
!=*    Faculty of Life Sciences											       *
!=*    University of Manchester											       *
!=*    The Michael Smith Building										       *
!=*    Oxford Road														       *
!=*    Manchester. M13 9PT												       *
!=*    Email:Alan.Roseman@manchester.ac.uk    							       *
!=*																		       *
!=*   																	       *
!=******************************************************************************

! reduce verbosity. amr 19/7/2017


!from DockEM(version 2), incorporating the FLCF (see Ultramicroscopy 2003).
! The target function and rationale are as for DockEM v1 (Acta D, 2000)
!AMR 11/2002

! window will cope with exceeding bigmap's boundaries. AMR 4/2k4.

! loops convered to integer for g95, alan 11/5


! Version to implement coarse grain parallelisation, integrated with ccpem gui.   amr 10/2016

! 1. read map, search object, mask, angles
! 2. rotate sobj,mask, calc flcf, store max, loop over angles
! 3. output: cccmap, psimap, phimap, thetamap

!C input: spider angles file, 'angles.spi'
!C       Three digit run version key.
!C      Angular sampling for psi, degrees.



        Module image_arrays2
                real, dimension (:,:,:), allocatable :: map, rotsobj, rotmask,aa             
		real, dimension (:,:,:), allocatable :: lcf1,cnv2
		real meanmap,meanaa,meanmask,stdmap,stdaa,stdmask,meanrot,stdrot
        End Module image_arrays2



        program DockEMv2
  
  
        Use  image_arrays2
        Use  mrc_image
        real, dimension (:,:,:), allocatable :: cccmaxmap,psimap,phimap,thetamap,sobj,mask,lcfmask,bigmap
     
     	
	real angles(99999,3),psi,theta,phi,val,STD
	
	integer b,nb
	integer runcode,err

        integer nm,ni,cxyz(3)
	integer a,numangles,ji1,ji2
	integer counter,counter2,acount,bcount
	
	
	character*256 filename,sobjfilename,maskfilename,anglesfile,junk,pdbfile
	character*3 num
	logical t,f
	real x,y,z,maskth
	integer nm8,numatoms
	real*8 summap,sumcccs,sumnumccc,stdccc
	real cx,cy,cz
	real deg
	integer ix,iy,iz,vec(3)
	integer l,border,lx,ly,lz,lxp1,lxm1,lym1,lzm1,lxp2
	real lcx,lcy,lcz,srad
	real s1,s2,s3,scale,omst,domega
	integer sloop,ssteps
	integer nj,nojobs, ang_st, ang_fin


!	INTEGER NX,NY,NZ,NXM1,NYM1,NZM1,NXP1,NXP2       
	
! V2.1 update here. AMR 17/6/2015
        filename=" "
	
	t=.true.
	f=.false.

	nj=1
	nojobs = 1

      print*, 'DockEM2-LSV2.2Pll '
      print*, '================='


	print*,'Enter the EM map filename.'
	read (5,'(a)') filename
! V2.1 update here. AMR 17/6/2015
	
	print*,'Enter the sampling of the map (A/pixel)'
	read*, sampling
	
	print*,'Enter the search object map filename.'
	read (5,'(a)') sobjfilename
	
	print*,'Enter the mask map filename.'
	read (5,'(a)') maskfilename
	
	print*,'Enter a threshold for the mask map.'
	read*,maskth
	
	print*,'Enter the angles file (spider format).'
	read (5,'(a)') anglesfile
	
	print*,'Enter the increment for the angular omega search.'
	read*,domega
	
      write (6,*) 'Enter a run code, for output files.'
      read (5,30) runcode
30    format (I3)

	print*,'Enter the pdbfile for the search object.'
	read (5,'(a)') pdbfile

	print*,'Enter the size of the local area in pixels (a box), and a border to impose blank:'
	read*,l,border 

35	continue
	print*,'Enter the number of the job, and the total number of jobs: (default = 1,1)'
	read*,nj,nojobs


	if (nj.gt.nojobs) then
		print*,'*** error, job number can''t be greater than number of jobs. '
		goto 35
	endif

	if (nj.lt.1) then
		print*,'*** error, job number can''t be <= zero. '
		goto 35
	endif


	if (nojobs.lt.1) then
		print*,'*** error, number of jobs can''t be <= zero. '
		goto 35
	endif





! make box size even
	l=2*(int(l/2))
	print*,l,border

	srad=int((float(l)/2.)-border)-1
	lx=l
	ly=l
	lz=l
	lxm1=lx-1
	lym1=ly-1
	lzm1=lz-1
	lxp1=lx+1
	lxp2=lx+2
	
	lcx=int(float(lx/2))
	lcy=int(float(ly/2))		
	lcz=int(float(lz/2))

! pdb file com used to set origin of rotations.
! for printing the pdbfile name
	print*,pdbfile
        call readpdb(pdbfile,numatoms)
        call com(cx,cy,cz,numatoms)
        cx=int((cx/sampling)+0.5)
        cy=int((cy/sampling)+0.5)
        cz=int((cz/sampling)+0.5)

	PRINT*,'COM of sobj at ',cx,cy,cz,' pixels.'
	print*,'Local origin at:',lcx,lcy,lcz

!C ***********************************************
!C open input from MRC format file
        print*,nx,ny,nz,filename,filename(1:lnblnk(filename))
        CALL IMOPEN(1,filename(1:lnblnk(filename)),'RO')
! V2.1 update here. AMR 17/6/2015

        CALL IRDHDR(1,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
        print*,nx,ny,nz,mode,dmin,dmax,dmean
	nxm1=nx-1
        nym1=ny-1
        nzm1=nz-1
        
        nxp1=nx+1
        nxp2=nx+2




!C allocate  image_files 
	allocate (bigmap(0:nxp1,0:nym1,0:nzm1))
        allocate (map(0:lxp1,0:lym1,0:lzm1), sobj(0:lxp1,0:lym1,0:lzm1),mask(0:lxp1,0:lym1,0:lzm1) )
         
       allocate (rotsobj(0:lxp1,0:lym1,0:lzm1), rotmask(0:lxp1,0:lym1,0:lzm1))  
       allocate (AA(0:lxp1,0:lym1,0:lzm1))
	   
        CALL READMRCMAP(1,filename,bigmap,nx,ny,nz,T,F,T,err) !read, is open, dont close, add 2 bytes

	print*,nx,ny,xz
	print*, nxyz
        call window(map,bigmap,lx,ly,lz,nx,ny,nz,border,cx,cy,cz)

! put boundary of map windowed region to zero , avoids unnatural wrap around.           
       border=border+1	!dv2.1
       map(0:border-1,:,:)=0
       map(lx-border:lx+1,:,:)=0
       map(:,0:border-1,:)=0
       map(:,ly-border:ly-1,:)=0
       map(:,:,0:border-1)=0
       map(:,:,lz-border:lz-1)=0
        
        CALL READMRCMAP(2,sobjfilename,bigmap,nx,ny,nz,F,T,T,err) !read, not open,  close, add 2 bytes
        call window(sobj,bigmap,lx,ly,lz,nx,ny,nz,border,cx,cy,cz)
                 
        CALL READMRCMAP(2,maskfilename,bigmap,nx,ny,nz,F,T,T,err) !read, not open,  close, add 2 bytes  
        call window(mask,bigmap,lx,ly,lz,nx,ny,nz,border,cx,cy,cz)
       mask(0:border-1,:,:)=0
       mask(lx-border:lx+1,:,:)=0
       mask(:,0:border-1,:)=0
       mask(:,ly-border:ly-1,:)=0
       mask(:,:,0:border-1)=0
       mask(:,:,lz-border:lz-1)=0
!make mask binary
	where (mask.ge.maskth) 
			  mask=1
		elsewhere
			mask=0
	endwhere

	call spherecut(mask,lx,ly,lz,srad)
	
	nm = sum(mask(:,:,:))			!number of non zero mask points

	sobj=sobj*mask			!this will speed up the rotations, uninteresting regions are now 0.
	
	print*, nm
	
!C ***************************************************


!c 2.  read angles
      open(2,file=anglesfile(1:lnblnk(anglesfile)),status='old',err=902)
          read(2,*) junk




        a=0
!       do a=1,numangles
1999     continue
         a=a+1
                read(2,200,err=901,end=901) ji1,ji2,angles(a,1),angles(a,2),angles(a,3)
200             format (i5,i2,3G12.5)
        print*, ji1,ji2,angles(a,1),angles(a,2),angles(a,3)
        goto 1999
!       enddo
901     continue
        close(2)
        numangles=a-1
        write(6,*) numangles,' angles read from ', anglesfile
        if (numangles.eq.0) STOP 'Angles file is null.'

	if (nojobs.gt.numangles) nojobs = numangles

	if (nj.gt.nojobs) then
		STOP '*** error, job number exceeds number of angles/number of jobs. '
	endif


!C ***********************************************
!C  set up and open  output image_files

        allocate (cccmaxmap(0:lxp1,0:lym1,0:lzm1),phimap(0:lxp1,0:lym1,0:lzm1))
        allocate (thetamap(0:lxp1,0:lym1,0:lzm1), psimap(0:lxp1,0:lym1,0:lzm1))
        allocate (cnv2(0:lxp1,0:lym1,0:lzm1),lcf1(0:lxp1,0:lym1,0:lzm1))    

       if (nojobs.le.1) then      

	CALL IMOPEN(7,'cccmaxmap'//num(runcode)//'.mrc','UNKNOWN')
        CALL ITRHDR(7,1)


!	CALL IWRHDR(7)
	print*,'7.5'
	CALL IMOPEN(8,'phimap'//num(runcode)//'.mrc','UNKNOWN')
        CALL ITRHDR(8,1)
!        CALL IWRHDR(8)

        CALL IMOPEN(9,'thetamap'//num(runcode)//'.mrc','UNKNOWN')
        CALL ITRHDR(9,1)
!        CALL IWRHDR(9)

        CALL IMOPEN(10,'psimap'//num(runcode)//'.mrc','UNKNOWN')
        CALL ITRHDR(10,1)
!        CALL IWRHDR(10)

		else

	CALL IMOPEN(7,'cccmaxmap'//num(runcode)//'_'//num5(nj)//'.mrc','UNKNOWN')
        CALL ITRHDR(7,1)


!	CALL IWRHDR(7)
	print*,'7.5'
	CALL IMOPEN(8,'phimap'//num(runcode)//'_'//num5(nj)//'.mrc','UNKNOWN')
        CALL ITRHDR(8,1)
!        CALL IWRHDR(8)

        CALL IMOPEN(9,'thetamap'//num(runcode)//'_'//num5(nj)//'.mrc','UNKNOWN')
        CALL ITRHDR(9,1)
!        CALL IWRHDR(9)

        CALL IMOPEN(10,'psimap'//num(runcode)//'_'//num5(nj)//'.mrc','UNKNOWN')
        CALL ITRHDR(10,1)
!        CALL IWRHDR(10)

	endif



!C ***********************************************
	cccmaxmap = -1.2
	phimap=-999
	psimap=-999
	thetamap=-999
	summap=0
	sumcccs=0
	sumnumccc=0
	stdccc=0
	
! get number of +ve map points for norm of stds.
	where (map.gt.0)
		aa=1
	elsewhere 
		aa=0
	endwhere
	
	summap=sum(aa)	
	
!C 5. do the search
     
     
! set up ffts
! fft map, mapsq, 
! A,AA     
		print*, 0
		nm8=lx*ly*lz
		
		val=minval(map(0:lxm1,0:lym1,0:lzm1))
		map(0:lxm1,0:lym1,0:lzm1)=map(0:lxm1,0:lym1,0:lzm1)-val
		call msdset(map,lx,ly,lz,nm8,val,err)		
		call msd(map,lx,ly,lz,nm8,meanmap,stdmap,err)
		PRINT*,'MSD t ',meanmap,stdmap
		
		print*,val
		AA=map*map
		call msd(AA,lx,ly,lz,nm8,meanaa,stdaa,err)
                CALL BCKW_FT(map,lXP2,lX,lY,lZ)
		CALL BCKW_FT(AA,lXP2,lX,lY,lZ)
		
	print*,'done fts'

	print*,nx,ny,nz
	print*,lx,ly,lz
	print*,lXP2,lX,lY,lZ
    	val=0
        counter = 0
        counter2 = 0
        acount=0
        
 	print*,'l:',lx,ly,lz,lcx,lcy,lcz,srad
        
	ang_st =1
	ang_fin =numangles

	
	if (nojobs.gt.1) then
		j_angles = int (float(numangles)/float(nojobs)) 
		if (j_angles.ge.1) then 
			if (nojobs.gt.j_angles) nojobs = j_angles
			ang_st = ((nj-1) * j_angles) + 1
			ang_fin = nj * j_angles
			if (nj.ge.nojobs) ang_fin = numangles
			
				else
			STOP '*** error in j_angles.'
			
		endif
	endif 



        do a = ang_st, ang_fin      
	        print*,'angle searching=',a,' numangles=',numangles,' nm=',nm,' j_angles=',j_angles,' job number=',nj,'no of jobs=',nojobs
        	bcount = 0
       		omst=-((int(360./domega))/2)*domega
       		
		!do b=0,0
		nb=int(360/domega)
   !    		do b = omst,179,domega
  		  do b=0,nb
        		bcount = bcount +1
       			 !psi = angles(a,1)
        		psi = b*domega+ omst
		        theta = angles(a,2)
       			 phi = angles(a,3)
       		
       			 call rotobj(sobj,rotsobj,psi,theta,phi,lx,ly,lz,lcx,lcy,lcz)
       		
  		         call rotobj(mask,rotmask,psi,theta,phi,lx,ly,lz,lcx,lcy,lcz)
	
!make mask binary, get rid of feeble edge points
			where (rotmask.ge.0.5)  
					rotmask=1
				elsewhere
					rotmask=0
			endwhere
			
			
			nm = sum(rotmask(:,:,:))			!number of non zero mask points

       			rotsobj=rotsobj*rotmask			!get rid of ojbect points beyond the mask.
			call msd(rotmask,lx,ly,lz,nm8,meanmask,stdmask,err)
	
		ni=lx*ly*lz
	 	call msdset(rotsobj,lx,ly,lz,nm,val,err)	!set the msd of sobj to 0,1
			
		rotsobj=rotsobj*rotmask				! zero background region
		
			
		call msd(rotsobj,lx,ly,lz,nm,meanrot,stdrot,err)

 	  	    
    		    CALL BCKW_FT(rotsobj,lXP2,lX,lY,lZ)
		    CALL BCKW_FT(rotmask,lXP2,lX,lY,lZ)
	   
      		    call FLCF3D(lx,ly,lz,nm) ! map, mapsq,     
		!	print*,'done flcf',b
		  
		   		
				where (lcf1.gt.cccmaxmap)
					cccmaxmap=lcf1
					phimap=phi
					psimap=psi
  					thetamap=theta
  				endwhere
  				sumcccs=sumcccs+sum(lcf1)
  				sumcccssq=sumcccssq+sum(lcf1*lcf1)
  				sumnumccc=sumnumccc+nm8
  		enddo
  	enddo
  	
                     
!C 6. write the searchmaps
                CALL FORW_FT(map,lXP2,lX,lY,lZ)
               
! phimap
!	call unwindow2(bigmap,phimap,nx,ny,nz,lx,ly,lz,cx,cy,cz)
	CALL WRITEMRCMAP(8,phimap,lx,ly,lz,sampling,err)

!thetamap	
!	call unwindow2(bigmap,thetamap,nx,ny,nz,lx,ly,lz,cx,cy,cz)
	CALL WRITEMRCMAP(9,thetamap,lx,ly,lz,sampling,err)
	
!	call unwindow2(bigmap,psimap,nx,ny,nz,lx,ly,lz,cx,cy,cz)
	CALL WRITEMRCMAP(10,psimap,lx,ly,lz,sampling,err)
	

!	call unwindow2(bigmap,cccmaxmap,nx,ny,nz,lx,ly,lz,cx,cy,cz)
	CALL WRITEMRCMAP(7,cccmaxmap,lx,ly,lz,sampling,err) !open already, close it, subtract 2 from nx
	
		
	
	val=maxval(cccmaxmap)
	print*,'max ccc is :',val
	print*,'at ', maxloc(cccmaxmap)
	vec=maxloc(cccmaxmap)
	print*,vec
	ix=vec(1)-1
	iy=vec(2)-1
	iz=vec(3)-1
	print*,ix,iy,iz
	print*,'phi=',phimap(ix,iy,iz)
	print*,'theta=',thetamap(ix,iy,iz)
	print*,'psi=',psimap(ix,iy,iz)
	print*,'ccc=',cccmaxmap(ix,iy,iz)
	
	
	print*,'meanCCC = ',sumcccs/sumnumccc
	stdccc=((sumnumccc*sumcccssq)+(sumcccs*sumcccs))/(sumnumccc*sumnumccc)
	print*,'stdevCCC = ',stdccc
	
	
	
! create a cshift CCCmaxmap for overlay display on map.
!amr 19/5/18





	
	
	
	goto 999

!C ***********************************************

902     continue  
! err reading angles
        print*, 'error reading angles file.'
        stop
903     continue
!Cerr reading map
        print*,'error reading EM-map.'
        stop

999    continue    
       print*,'Program finished O.K.'

       
              
       
       CONTAINS


	SUBROUTINE READMRCMAP(stream,filename,map,X,Y,Z,open,close,add2,err)
 
        Use mrc_image

         INTEGER  X,Y,Z,err
         INTEGER  stream, IX,IY,IZ,xm1
         real, DIMENSION (0:X+1,0:Y-1,0:Z-1) :: map  
         INTEGER S(3),S1
         logical add2,open,close
         character*256 filename
            
          if(.not. open) then
          	       CALL IMOPEN(stream,filename(1:lnblnk(filename)),'RO')
       		       CALL IRDHDR(stream,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
	  endif
            
         
      
      
!     read in file 

      DO 350 IZ = 0,z-1
          DO 350 IY = 0,y-1
            CALL IRDLIN(stream,ALINE,*998)
            DO 300 IX = 0,x-1       
       !     print*,ix
                map(ix,iy,iz) = ALINE(IX+1)
300         CONTINUE  
	if(add2) then  
     		       map(X+0,iy,iz)=0
      		       map(X+1,iy,iz)=0
        endif
        
     !      print*,ix,iy,iz
350   CONTINUE

	if (close) then
		CALL IMCLOSE(stream)
		print*,'closed ',stream
	endif

      return
998   STOP 'Error on file read.'
     	
	END SUBROUTINE READMRCMAP
	
	
       
      SUBROUTINE WRITEMRCMAP(stream,map,x,y,z,sampling,err)
      Use  mrc_image
      REAL val,sampling
      
      integer x,y,z,err,stream,ix,iy,iz
      real map(0:x+1,0:y-1,0:z-1)
      integer newnxyz(3)

      print*,'write_mrcimage'         

      print*,NX,NY,NZ  ,x,y,z
!     write file 

	NXYZST=0
        NXYZ(1)=x
        NXYZ(2)=y
        NXYZ(3)=z

      cell(1) = sampling *NX
      cell(2) = sampling *NY
      cell(3) = sampling *NZ
      cell(4) = 90
      cell(5) = 90
      cell(6) = 90

      CALL IALCEL(STREAM,CELL)        

        MXYZ(1)=x
        MXYZ(2)=y 
        MXYZ(3)=z
  
        CALL IALSAM(2,MXYZ)

	

        CALL IALSIZ(STREAM,NXYZ,NXYZST)

        CALL IALORG(STREAM,0.0,0.0,0.0)



      DMIN =  1.E10
      DMAX = -1.E10
      DOUBLMEAN = 0.0

      DO 450 IZ = 0,Z-1
      DO 450 IY = 0,Y-1
           
            DO 400 IX = 0,X-1
     
                val = map(IX,IY,IZ)
                ALINE(IX+1) = val
                
                DOUBLMEAN = DOUBLMEAN + val         
                IF (val .LT. DMIN) DMIN = val
                IF (val .GT. DMAX) DMAX = val

400         CONTINUE   

       NX=x
	Ny=y
	nz=z
      CALL IWRLIN(stream,ALINE)
450   CONTINUE
      DMEAN = DOUBLMEAN/(NX*NY*NZ)

 


      CALL IWRHDR(stream,TITLE,-1,DMIN,DMAX,DMEAN)
      CALL IMCLOSE(stream)
      
      return
999   STOP 'Error on file write.'
 
      END SUBROUTINE WRITEMRCMAP
	
	
	
	
	SUBROUTINE rotobj(obj,therotobj,psi,theta,phi,nx,ny,nz,cx,cy,cz)
	
	
	       

!c sub to apply rotation about centre of map to map object 
!c convert rotations to matrix form
!C spider convention for euler angles to mat

! the object maps are nxp1,ny,nz in size
!rotn centre is nx/2,ny/2,nz/2
     
	real obj(0:nx+1,0:ny-1,0:nz-1),therotobj(0:nx+1,0:ny-1,0:nz-1)
        real cx,cy,cz,psi,phi,theta,x,y,z
        integer nx,ny,nz,ix,iy,iz,nxm1,nym1,nzm1
	real rad,deg
        
        real mat(3,3),mata(3,3),matb(3,3),matc(3,3),matd(3,3)
        real val,b
        
        therotobj=0
        nxm1=nx-1
        nym1=ny-1
        nzm1=nz-1


        mata(1,1)=cos(rad(psi))
        mata(1,2)=sin(rad(psi))
        mata(1,3)=0.0

        mata(2,1)=-sin(rad(psi))
        mata(2,2)=cos(rad(psi))
        mata(2,3)=0.0

        mata(3,1)=0.0
        mata(3,2)=0.0
        mata(3,3)=1.0

        matb(1,1)=cos(rad(theta))
        matb(1,2)=0.0
        matb(1,3)=-sin(rad(theta))

        matb(2,1)=0.0
        matb(2,2)=1.0
        matb(2,3)=0.0

        matb(3,1)=sin(rad(theta))
        matb(3,2)=0.0
        matb(3,3)=cos(rad(theta))

        matc(1,1)=cos(rad(phi))
        matc(1,2)=sin(rad(phi))
        matc(1,3)=0.0

	matc(2,1)=-sin(rad(phi))
        matc(2,2)=cos(rad(phi))
        matc(2,3)=0.0

        matc(3,1)=0.0
        matc(3,2)=0.0
        matc(3,3)=1.0

        call matmult(matb,matc,mat)
        call matmult(mata,mat,matd)


!	print*,matd

	do iz=0,nzm1
		do iy=0,nym1
			do ix=0,nxm1
			
				x=float(ix)
				y=float(iy)
				z=float(iz)
				
				val=obj(ix,iy,iz)
				if (val.gt.0.1) then			!if density is 0 don't waste time.
					call matmult2(x,y,z,matd,cx,cy,cz)  	!transform coords by rotn
	
	call interpo2(therotobj,nx,ny,nz,x,y,z,val)		!interpo density at new posn to new map.)
	
				endif
  			enddo
        	enddo
        enddo
	therotobj(nx:nx+1,0:ny-1,0:nz-1)=0
        return

	END SUBROUTINE rotobj


	subroutine matmult(mat1,mat2,mat)
!C multiply 2 matrices together
        real mat(3,3),mat1(3,3),mat2(3,3)

        mat=matmul(mat1,mat2)
!	print*,'mm'
        return 
        end subroutine matmult

        subroutine matmult2(x,y,z,mat,cx,cy,cz)
!c mult coords by transformation mat


        real mat(3,3),cx,cy,cz, x,y,z
        real coords(3),newcoords(3)

        coords(1) = x-cx
        coords(2) = y-cy
        coords(3) = z-cz

        newcoords=matmul(mat,coords)

        x = newcoords(1) + cx
        y = newcoords(2) + cy
        z = newcoords(3) + cz

        return
        end subroutine matmult2

		
	
	SUBROUTINE FLCF3D(x,y,z,nm)
	
	
	! in, ft of :map , mapsq, sobj, mask
	! out FLCF/cccmap
	use image_arrays2
	integer x,y,z,nm

	real*8 nm8
	real sd,masksd,val
	integer err
	integer ni
	
	ni=x*y*z
	nm8=nm

	call CORRELATION3D(rotmask,map,lcf1,x,y,z)

	call CORRELATION3D(rotmask,aa,cnv2,x,y,z)

	cnv2=(nm8*cnv2)
	cnv2=(cnv2-(lcf1*lcf1))/(nm8*nm8)
	
	call CORRELATION3D(rotsobj,map,lcf1,x,y,z)

	val=minval(cnv2)
!	print*,'min of v=',val

	where (cnv2.gt.0.00001)
			cnv2=sqrt(cnv2)

		elsewhere
			cnv2=0.
!			print*,'V was lt 0 !!'
	endwhere
	
	where (cnv2.gt.0)
			lcf1=(lcf1/cnv2)/nm8
	elsewhere
			lcf1=-2
	endwhere
	

	END SUBROUTINE FLCF3D
	
	
	subroutine CONVOLUTION3D(a,b,c,nx2,ny2,nz2)
        integer nx2,ny2,nz2,i,j,k
        real a(0:nx2+1,0:ny2-1,0:nz2-1),b(0:nx2+1,0:ny2-1,0:nz2-1),c(0:nx2+1,0:ny2-1,0:nz2-1)
        real n
        integer nxp2
	integer dx

        nxp2=nx2+2

        c=0
        
        do k=0,nz2-1
  	      do j=0,ny2-1
            	    do i=0,nx2+1,2
                
                        c(i,j,k)=a(i,j,k)*b(i,j,k)-a(i+1,j,k)*b(i+1,j,k)
                        c(i+1,j,k)=a(i,j,k)*b(i+1,j,k)+a(i+1,j,k)*b(i,j,k)
               
                enddo
            enddo
       enddo

        CALL FORW_FT(c,NXP2,NX2,NY2,NZ2)

        n=nx2*ny2*nz2		
        C=C*N

        end subroutine CONVOLUTION3D


         subroutine CORRELATION3D(a,b,c,nx,ny,nz)

        integer nx,ny,nz
        real a(0:nx+1,0:ny-1,0:nz-1),b(0:nx+1,0:ny-1,0:nz-1),c(0:nx+1,0:ny-1,0:nz-1)
        real r1,r2,hp,lp,sampling,n
        integer nxp2
        integer i,j,k
  
	integer dx
	
        nxp2=nx+2
!        r1=hp/sampling
!       r2=lp/sampling
	dx=int(nx/2.)+1


        c=0
        do k=0,nz-1
       	    do j=0,ny-1
                do i=0,nx+1,2
                        !r=radft(i,j,ny)
                        !if ((r.gt.r1).and.(r.lt.r2)) then
                        c(i,j,k)=a(i,j,k)*b(i,j,k)+a(i+1,j,k)*b(i+1,j,k)
                        c(i+1,j,k)=a(i,j,k)*b(i+1,j,k)-a(i+1,j,k)*b(i,j,k)
                        !endif
                enddo
            enddo
        enddo
        
        CALL FORW_FT(c,NXP2,NX,NY,NZ)
 
        n=nx*ny*nz					
        C=C*n

        end subroutine CORRELATION3D



      subroutine msdset(map,nx,ny,nz,nm,val,err)
      integer nx,ny,nz,nxm1,nym1,nzm1,nxp1,nxp2,z,err
      real map(0:nx+1,0:ny-1,0:nz-1)
      real*8 map8(0:nx+1,0:ny-1,0:nz-1)      
      real*8 mean,sd,lsum,sum_sqs,sq
      real val,th
      integer nm
      real*8 nm8
     

   ! treats as if all vals not in the masked nm, are zero. must be 0 !!!
      nxp1=nx+1
      nym1=ny-1
      nxm1=nx-1
      nzm1=nz-1
	nm8=nm
      
      if (nm.le.0) STOP 'Mistake, nm=0'
      
      err=0
      lsum = sum(map(0:nxm1,0:nym1,0:nzm1))

      mean=lsum/nm8

	map8=map
	map8=map8*map8

       sum_sqs=sum(map8(0:nxm1,0:nym1,0:nzm1))
    
 	 lsum=lsum*lsum
 	 sum_sqs=nm8*sum_sqs
 	 sq=((sum_sqs-lsum))
 	 sq=sq/(nm8*nm8)
    

       if (sq.lt.0.0) stop 'sd lt zero in msd set.'

       th=0.000000000001
      if (sq.gt.th) then
                sd = sqrt(sq)
                map=(map-mean)/sd
                val= -mean/sd
                err=0
        elseif (sq.le.0.0) then
                err=1
                print*,'le0'
     		stop
        elseif (sq.le.th) then 
                map=(map-mean)
        	print*,'at threshold'
                val= -mean/sd
                err=0
		stop
        endif

       return
       end subroutine msdset

	SUBROUTINE MSD(map,nx,ny,nz,nm,val,STD,err)
            integer nx,ny,nz,nxm1,nym1,nzm1,nxp1,nxp2,z,err
      real map(0:nx+1,0:ny-1,0:nz-1)
 	real*8 map8(0:nx+1,0:ny-1,0:nz-1)
      real*8 mean,sd,lsum,sum_sqs,sq
      real val,th,STD
      integer nm
	real*8 nm8
     
   ! treats as if all vals not in the masked nm, are zero. must be 0 !!!
      nxp1=nx+1
      nym1=ny-1
      nxm1=nx-1
      nzm1=nz-1
	nm8=nm
      SD =-9999999
      if (nm.le.0) STOP 'Mistake, nm=0'
      
      err=0
      lsum = sum(map(0:nxm1,0:nym1,0:nzm1))
      mean=lsum/nm8

	map8=map
	map8=map8*map8
       sum_sqs=sum(map8(0:nxm1,0:nym1,0:nzm1))
 
 	 lsum=lsum*lsum
 	 sum_sqs=nm8*sum_sqs
 	 sq=((sum_sqs-lsum))
 	 sq=sq/(nm8*nm8)
    	
	

       if (sq.lt.0) stop 'sd lt zero in msd.'

       th=0.00001
      if (sq.gt.th) then
                sd = sqrt(sq)
                val= -mean/sd
                err=0
        elseif (sq.le.0) then
                err=1
                print*,'le0'
                stop
        elseif (sq.le.th) then 
                !map=(map-mean)
        	print*,'at threshold'
                err=0
		stop
        endif

   	VAL=MEAN
   	STD=SD
       return
       end subroutine MSD
       
       subroutine window(smallmap,bigmap,lx,ly,lz,nx,ny,nz,border,cx,cy,cz)

       integer lx,ly,lz,border,nx,ny,nz
       real cx,cy,cz
       integer sx,fx,sy,fy,sz,fz
       real smallmap(0:lx+1,0:ly-1,0:lz-1)
       real bigmap(0:nx+1,0:ny-1,0:nz-1)
       integer smbxl,smbyl,smbzl,smbxr,smbyr,smbzr
       

       smallmap=0
       
       sx=int(cx-int(lx/2.))
       fx=int(cx+int(lx/2.)-1)
       sy=int(cy-int(ly/2.))
       fy=int(cy+int(ly/2.)-1)
       sz=int(cz-int(lz/2.))
       fz=int(cz+int(lz/2.)-1)
         
       smbxl=0
       smbyl=0
       smbzl=0
       
       smbxr=lx-1
       smbyr=ly-1
       smbzr=lz-1
       
       print*,'window:',sx,fx,sy,fy,sz,fz,nx,ny,nz
       print*,smbxl,smbxr,smbyl,smbyr,smbzl,smbzr
         
       if ((sx.lt.0)) then  
       			print*,'Warning: Region exceeds map boundary'
       			smbxl=-sx
       			sx=0
       endif
       			
       if ((fx.gt.nx-1)) then 
       			 print*,'Warning: Region exceeds map boundary'
       			 smbxr=smbxr-(fx-(nx-1))
       			 fx=nx-1
       endif
       
           
       if ((sy.lt.0)) then  
       			print*,'Warning: Region exceeds map boundary'
       			smbyl=-sy
       			sy=0
       endif
       			
       if ((fy.gt.ny-1)) then 
       			 print*,'Warning: Region exceeds map boundary'
       			 smbyr=smbyr-(fy-(ny-1))
       			 fy=ny-1
       endif
           
       if ((sz.lt.0)) then  
       			print*,'Warning: Region exceeds map boundary Zl'
       			smbzl=-sz
       			sz=0
       endif
       			
       if ((fz.gt.nz-1)) then 
       			 print*,'Warning: Region exceeds map boundary Zh'
       			 smbzr=smbzr-(fz-(nz-1))
       			 fz=nz-1
       endif
    
       smallmap(smbxl:smbxr,smbyl:smbyr,smbzl:smbzr)=bigmap(sx:fx,sy:fy,sz:fz)
       print*,'window:',sx,fx,sy,fy,sz,fz
       print*,smbxl,smbxr,smbyl,smbyr,smbzl,smbzr
   	smallmap(lx:lx+1,:,:)=0
   
 !      smallmap(0:border-1,:,:)=0
  !     smallmap(lx-border:lx+1,:,:)=0
       
   !    smallmap(:,0:border-1,:)=0
    !   smallmap(:,ly-border:ly-1,:)=0
       
  !     smallmap(:,:,0:border-1)=0
   !    smallmap(:,:,lz-border:lz-1)=0
     
       end subroutine window
       
       subroutine unwindow(bigmap,smallmap,nx,ny,nz,lx,ly,lz,cx,cy,cz)
       integer lx,ly,lz,nx,ny,nz
       real cx,cy,cz
       integer sx,fx,sy,fy,sz,fz
       real smallmap(0:lx+1,0:ly-1,0:lz-1)
       real bigmap(0:nx+1,0:ny-1,0:nz-1)
       
       sx=int(cx-int(lx/2.))
       fx=int(cx+int(lx/2.)-1)
       sy=int(cy-int(ly/2.))
       fy=int(cy+int(ly/2.)-1)
       sz=int(cz-int(lz/2.))
       fz=int(cz+int(lz/2.)-1)
       
       print*,sx,fx,sy,fy,sz,fz
       
       if ((sx.lt.0)) then  
       			print*,'Warning: Region exceeds map boundary'
       			sx=0
       endif
       			
       if ((fx.gt.nx-1)) then 
       			 print*,'Warning: Region exceeds map boundary'
       			 fx=nx-1
       endif
       
           
       if ((sy.lt.0)) then  
       			print*,'Warning: Region exceeds map boundary'
       			sy=0
       endif
       			
       if ((fy.gt.ny-1)) then 
       			 print*,'Warning: Region exceeds map boundary'
       			 fy=ny-1
       endif
           
       if ((sz.lt.0)) then  
       			print*,'Warning: Region exceeds map boundary'
       			sz=0
       endif
       			
       if ((fz.gt.nz-1)) then 
       			 print*,'Warning: Region exceeds map boundary'
       			 fz=nz-1
       endif
       
       
       bigmap(:,:,:)=0
       bigmap(sx:fx,sy:fy,sz:fz) = smallmap(0:lx-1,0:ly-1,0:lz-1)
       
     
       
       end subroutine unwindow
            
       
      subroutine unwindow2(bigmap,smallmap,nx,ny,nz,lx,ly,lz,cx,cy,cz)
       integer lx,ly,lz,nx,ny,nz
       real cx,cy,cz
       integer icx,icy,icz
       integer shx,shy,shz
       integer sx,fx,sy,fy,sz,fz
       real smallmap(0:lx+1,0:ly-1,0:lz-1)
       real bigmap(0:nx+1,0:ny-1,0:nz-1)
       
       icx=int(cx)
       icy=int(cy)
       icz=int(cz)
       sx=0
       fx=lx-1
       sy=0
       fy=ly-1
       sz=0
       fz=lz-1
       
       print*,sx,fx,sy,fy,sz,fz,cx,cy,cz,nx,ny,nz
     
     	shx=-int(cx)
        shy=-int(cy)
        shz=-int(cz)
       
        bigmap(:,:,:)=0
 !      bigmap(0:fx,0:fy,0:fz) = smallmap(0:fx,0:fy,0:fz)
  !     bigmap=cshift(bigmap,shx,1)
  !     bigmap=cshift(bigmap,shy,2)
   !    bigmap=cshift(bigmap,shz,3)
       
!        bigmap(0:cx,0:cy,0:cz)= smallmap(cx+1:lx-1,cy+1:ly-1,cz+1:lz-1)
!        bigmap(nx-cx+1:nx-1,ny-cy+1:ny-1,nz-cz+1:nz-1)=smallmap(0:cx,0:cy,0:cz)
!        bigmap(0:cx,ny-cy+1:ny-1,0:cz) = smallmap(cx+1:lx-1,0:cy,cz+1:lz-1)
!        bigmap(0:cx,0:cy,nz-cz+1:nz-1) = smallmap(cx+1:lx-1,cy+1:ly-1,0:cz)
!        bigmap(nx-cx+1:nx-1,0:cy,0:cz) = smallmap(0:cx,cy+1:ly-1,cz+1:lz-1)
!        bigmap(nx-cx+1:nx-1,0:cy,nz-cz+1:nz-1) = smallmap(0:cx,cy+1:ly-1,0:cz)
!        bigmap(nx-cx:nx-1,ny-cy+1:ny-1,0:cz) = smallmap(0:cx,0:cy,cz+1:lz-1)
!        bigmap(0:cx,ny-cy+1:ny-1,nz-cz+1:nz-1) = smallmap(cx+1:lx-1,0:cy,0:cz)
        
        bigmap(0:icx,0:icy,0:icz)= smallmap(0:icx,0:icy,0:icz)
        bigmap(nx-icx+1:nx-1,ny-icy+1:ny-1,nz-icz+1:nz-1)=smallmap(icx+1:lx-1,icy+1:ly-1,icz+1:lz-1) 
        bigmap(0:icx,ny-icy+1:ny-1,0:icz) = smallmap(0:icx,icy+1:ly-1,0:icz)
        bigmap(0:icx,0:icy,nz-icz+1:nz-1) = smallmap(0:icx,0:icy,icz+1:lz-1)
        bigmap(nx-icx+1:nx-1,0:icy,0:icz) = smallmap(icx+1:lx-1,0:icy,0:icz)
        bigmap(nx-icx+1:nx-1,0:icy,nz-icz+1:nz-1) = smallmap(icx+1:lx-1,0:icy,icz+1:lz-1)
        bigmap(nx-icx:nx-1,ny-icy+1:ny-1,0:icz) = smallmap(icx+1:lx-1,icy+1:ly-1,0:icz)
        bigmap(0:icx,ny-icy+1:ny-1,nz-icz+1:nz-1) = smallmap(0:icx,icy+1:ly-1,icz+1:lz-1)
     
      
       
     
     
       end subroutine unwindow2
       
      subroutine unwindow3(bigmap,smallmap,nx,ny,nz,lx,ly,lz,cx,cy,cz)
       integer lx,ly,lz,nx,ny,nz
       real cx,cy,cz
       integer icx,icy,icz
       integer shx,shy,shz
       integer sx,fx,sy,fy,sz,fz
       real smallmap(0:lx+1,0:ly-1,0:lz-1)
       real bigmap(0:nx+1,0:ny-1,0:nz-1)
       
       icx=int(cx)
       icy=int(cy)
       icz=int(cz)
       sx=0
       fx=lx-1
       sy=0
       fy=ly-1
       sz=0
       fz=lz-1
       
       print*,sx,fx,sy,fy,sz,fz,cx,cy,cz,nx,ny,nz
     
     	shx=-int(cx)
        shy=-int(cy)
        shz=-int(cz)
       
        bigmap(:,:,:)=0
       bigmap(0:fx,0:fy,0:fz) = smallmap(0:fx,0:fy,0:fz)
       bigmap=cshift(bigmap,shift = shx,dim=1)
       bigmap=cshift(bigmap,shift =shy,dim=2)
       bigmap=cshift(bigmap,shift =shz,dim=3)
       
!        bigmap(0:cx,0:cy,0:cz)= smallmap(cx+1:lx-1,cy+1:ly-1,cz+1:lz-1)
!        bigmap(nx-cx+1:nx-1,ny-cy+1:ny-1,nz-cz+1:nz-1)=smallmap(0:cx,0:cy,0:cz)
!        bigmap(0:cx,ny-cy+1:ny-1,0:cz) = smallmap(cx+1:lx-1,0:cy,cz+1:lz-1)
!        bigmap(0:cx,0:cy,nz-cz+1:nz-1) = smallmap(cx+1:lx-1,cy+1:ly-1,0:cz)
!        bigmap(nx-cx+1:nx-1,0:cy,0:cz) = smallmap(0:cx,cy+1:ly-1,cz+1:lz-1)
!        bigmap(nx-cx+1:nx-1,0:cy,nz-cz+1:nz-1) = smallmap(0:cx,cy+1:ly-1,0:cz)
!        bigmap(nx-cx:nx-1,ny-cy+1:ny-1,0:cz) = smallmap(0:cx,0:cy,cz+1:lz-1)
!        bigmap(0:cx,ny-cy+1:ny-1,nz-cz+1:nz-1) = smallmap(cx+1:lx-1,0:cy,0:cz)
   
       
     
     
       end subroutine unwindow3

      subroutine spherecut(mask,nx,ny,nz,r)
            integer nx,ny,nz,i,j,k,cx
            real r
            real mask(0:nx+1,0:ny-1,0:nz-1)
            
            cx=int(float(nx)/2.)
       
       	do i=0,nx-1
       	 do j=0,ny-1
       	   do k=0,nz-1
       	    if (radius(i,j,k,cx).gt.r) mask(i,j,k)=0.0
       	    enddo
          enddo
        enddo
       return
       end  subroutine spherecut

       
       real function radius(x,y,z,cx)
       integer x,y,z,cx
       real r
       r=(x-cx)**2+(y-cx)**2+(z-cx)**2
       radius=sqrt(r)
       end function radius

       !C ************************************************

        function num5(number)
        character(len=5) num5
  
        
        integer number,order
        real fnum,a
        integer hun,ten,units, thou,tenthou
        character*1 digit(0:9)

        digit(0)='0'
        digit(1)='1'
        digit(2)='2'
        digit(3)='3'
        digit(4)='4'
        digit(5)='5'
        digit(6)='6'
        digit(7)='7'
        digit(8)='8'
        digit(9)='9'

        fnum=float(number)
        fnum=mod(fnum,1000000.)
        tenthou=int(fnum/10000.)
        a=mod(fnum,10000.)
        thou=int(a/1000.)


        fnum=mod(fnum,1000.)
        hun=int(fnum/100.)
        a=mod(fnum,100.)
        ten=int(a/10.)
        units=mod(fnum,10.)
!C       print *,hun,ten,units


        num5 = digit(tenthou)//digit(thou)//digit(hun)//digit(ten)//digit(units)  
        return
        end function num5
       

           
        real function degrees(val)
        real val,pi
        parameter (pi=3.1415927)

        degrees=val*180/pi

        return
        end function


        real function rad(val)
        real val,pi
        parameter (pi=3.1415927)

        rad=val*pi/180.

        return
        end function

        

        
	end program DockEMv2
