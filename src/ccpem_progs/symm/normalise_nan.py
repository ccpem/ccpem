#!/usr/bin/env python
# coding: utf-8

from helper_functions.nptomo import MRCfile, NumpyTom
from npy2mrc import save_npy_as_mrc_file
import argparse
import sys
import numpy as np


# minmax normalisation, ignoring nan values, followed by replacing nan values with 0
def nan_minmax(data, scaling_factor=1):

    max_value = np.nanmax(data)
    min_value = np.nanmin(data)

    data = (data-min_value)/(max_value-min_value) * scaling_factor
    return data


def nan_norm(data, scaling_factor=1):

    data = nan_minmax(data, scaling_factor=scaling_factor)
    data[np.isnan(data)] = 0

    return data


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('input_file',
                        help="File to be normalised. Expected format: .npy or .mrc")

    args = parser.parse_args()

    # Acquire the data
    if args.input_file[-4:].lower() == ".npy":
        open_tom = NumpyTom
    elif args.input_file[-4:].lower() == ".mrc":
        open_tom = MRCfile
    else:
        print 'Error: Input file format not supported'
        sys.exit(1)

    with open_tom(args.input_file) as in_file:
        in_data = in_file.get_all_tom_density_values()

    out_data = nan_norm(in_data)

    save_npy_as_mrc_file(args[1][0]+"_normalised.mrc", out_data)
