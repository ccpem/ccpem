#!/usr/bin/env python
# coding: utf-8

import unittest
import unittests.test_avg_correlated_symmetries as test1
import unittests.test_autocorr as test2
import unittests.test_get_autocorr_noise as test3
import unittests.test_analysis_methods as test4
import unittests.test_make_autocorr_samples as test5
import unittests.test_make_wedge_mask as test6
import unittests.test_wedge_masking as test7
import unittests.test_normalise_nan as test8
import unittests.test_npy2mrc as test9
import unittests.test_nptomo as test10
import unittests.test_peet_make_wedge_masks as test11

suite = unittest.TestSuite()
suite.addTest(unittest.TestLoader().loadTestsFromModule(test1))
suite.addTest(unittest.TestLoader().loadTestsFromModule(test2))
suite.addTest(unittest.TestLoader().loadTestsFromModule(test3))
suite.addTest(unittest.TestLoader().loadTestsFromModule(test4))
suite.addTest(unittest.TestLoader().loadTestsFromModule(test5))
suite.addTest(unittest.TestLoader().loadTestsFromModule(test6))
suite.addTest(unittest.TestLoader().loadTestsFromModule(test7))
suite.addTest(unittest.TestLoader().loadTestsFromModule(test8))
suite.addTest(unittest.TestLoader().loadTestsFromModule(test9))
suite.addTest(unittest.TestLoader().loadTestsFromModule(test10))
suite.addTest(unittest.TestLoader().loadTestsFromModule(test11))

runner = unittest.TextTestRunner()
runner.run(suite)
