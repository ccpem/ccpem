#!/usr/bin/env python
# coding: utf-8

import unittest
import analysis_methods
import numpy as np


class TestAnalysisMethods(unittest.TestCase):

    def setUp(self):
        self.in_array = np.load("unittests/testdata/analysis_methods_in.npy")  # an autocorrelated array
        self.test_mask = np.load("unittests/testdata/analysis_methods_mask.npy")  # a random mask

    # expected are the outputs as produced by the version from 7/31/2017
    def test_fft_no_mask(self):
        expected = np.load("unittests/testdata/expected_fft.npy")
        np.testing.assert_array_equal(analysis_methods.fft_data(self.in_array), expected,
                                      "fft without mask did not produce expected result")

    def test_fft_masked(self):
        expected = np.load("unittests/testdata/expected_fft_masked.npy")
        np.testing.assert_array_equal(analysis_methods.fft_data(self.in_array, self.test_mask), expected,
                                      "fft with mask did not produce expected result")

    def test_spectral_no_masked(self):
        expected = np.load("unittests/testdata/expected_ls.npy")
        np.testing.assert_array_equal(analysis_methods.spectral_analyze_data(self.in_array), expected,
                                      "LS without mask did not produce expected result")

    def test_spectral_masked(self):
        expected = np.load("unittests/testdata/expected_ls_masked.npy")
        np.testing.assert_array_equal(analysis_methods.spectral_analyze_data(self.in_array, self.test_mask), expected,
                                      "LS with mask did not produce expected result")


if __name__ == "__main__":
    unittest.main()
