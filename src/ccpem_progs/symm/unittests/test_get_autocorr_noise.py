#!/usr/bin/env python
# coding: utf-8

import unittest
import get_autocorr_noise
from helper_functions.nptomo import MRCfile
import os
import numpy as np


class TestAnalysisMethods(unittest.TestCase):

    def setUp(self):
        with MRCfile("unittests/testdata/exp_avgNoiseProfile.mrc") as exp_avg, \
                MRCfile("unittests/testdata/exp_stdNoiseProfile.mrc") as exp_std:
            self.exp_avg_data = exp_avg.get_all_tom_density_values()
            self.exp_std_data = exp_std.get_all_tom_density_values()

    def tearDown(self):
        os.system("rm unittests/testdata/temp_avg.mrc")
        os.system("rm unittests/testdata/temp_std.mrc")
        os.system("rm make_autocorr_samples.log")

    def test_get_noise(self):
        # note: 1 is the random seed used to generate the expected data to compare to
        get_autocorr_noise.generate_profiles("unittests/testdata/get_noise_in.mrc", xdimension=20, number=3,
                                             seed_random=1, avgfile="unittests/testdata/temp_avg",
                                             stdfile="unittests/testdata/temp_std")

        with MRCfile("unittests/testdata/temp_avg.mrc") as avg_in, MRCfile("unittests/testdata/temp_std.mrc") as std_in:
            avg_data = avg_in.get_all_tom_density_values()
            std_data = std_in.get_all_tom_density_values()

        np.testing.assert_array_equal(self.exp_avg_data, avg_data, "average noise profile differs from expected array.")
        np.testing.assert_array_equal(self.exp_std_data, std_data, "std dev noise profile differs from expected array.")

if __name__ == "__main__":
    unittest.main()
