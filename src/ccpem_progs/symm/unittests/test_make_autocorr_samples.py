#!/usr/bin/env python
# coding: utf-8

import unittest
import numpy as np
import make_autocorr_samples
import os


class TestAnalysisMethods(unittest.TestCase):

    def setUp(self):
        self.npz = np.load("unittests/testdata/make_ac_samples_exp_results.npz")

    def tearDown(self):
        os.system("rm make_autocorr_samples.log")
        os.system("rm temp.mrc")
        os.system("rm temp_warning.mrc")

    def test_truncate(self):
        self.assertEqual(make_autocorr_samples.truncate(1.35), 1.0)
        self.assertEqual(make_autocorr_samples.truncate(0.9), 0)

    def test_make_autocorr_samples_standard(self):
        results = make_autocorr_samples.main("unittests/testdata/get_noise_in.mrc", ofilepath="temp.mrc", verbose=False)
        np.testing.assert_array_equal(results, self.npz["sample_std"],
                                      "Did not receive expected result making the autocorr sampling matrix without "
                                      "additional arguments")

    def test_make_autocorr_samples_mask(self):
        results = make_autocorr_samples.main("unittests/testdata/get_noise_in.mrc", ofilepath="temp.mrc", verbose=False,
                                             missing_wedge="unittests/testdata/small_40_mask.npy")
        np.testing.assert_array_equal(results, self.npz["sample_sm"],
                                      "Did not receive expected result making the autocorr sampling matrix with "
                                      "a small missing wedge.")

    def test_make_autocorr_samples_large_mask(self):
        results = make_autocorr_samples.main("unittests/testdata/get_noise_in.mrc", ofilepath="temp.mrc", verbose=False,
                                             missing_wedge="unittests/testdata/large_40_mask.npy")
        np.testing.assert_array_equal(results, self.npz["sample_lm"],
                                      "Did not receive expected result making the autocorr sampling matrix with "
                                      "a large missing wedge.")

    def test_make_autocorr_samples_adjust_centre(self):
        results = make_autocorr_samples.main("unittests/testdata/get_noise_in.mrc", ofilepath="temp.mrc", verbose=False,
                                   adjust_centre="(18, 18)")
        np.testing.assert_array_equal(results, self.npz["sample_ac"],
                                      "Did not receive expected result making the autocorr sampling matrix with "
                                      "center adjusted to 18, 18 (from 20, 20).")

    def test_make_autocorr_samples_window_width(self):
        results = make_autocorr_samples.main("unittests/testdata/get_noise_in.mrc", ofilepath="temp.mrc", verbose=False,
                                             window_width=3)
        np.testing.assert_array_equal(results, self.npz["sample_ww"],
                                      "Did not receive expected result making the autocorr sampling matrix with "
                                      "an averaging window of size 3x3")

if __name__ == "__main__":
    unittest.main()
