#!/usr/bin/env python
# coding: utf-8

import unittest
import numpy as np
from helper_functions.nptomo import MRCfile
import os


class TestAnalysisMethods(unittest.TestCase):

    def setUp(self):
        os.chdir("./unittests/testdata")
        os.system("python ../../peet_make_wedge_masks.py test_peet.prm")
        with MRCfile("test_peet_make_mask_exp1.mrc") as in_1, MRCfile("test_peet_make_mask_exp2.mrc") as in_2:
            self.exp1 = in_1.get_all_tom_density_values()
            self.exp2 = in_2.get_all_tom_density_values()

    def tearDown(self):
        os.system("rm unoriented_wedgeMask_Tom*")
        os.system("rm wedgeMask_Tom*.mrc")

    def test_peet_make_wedge_masks(self):
        np.testing.assert_array_equal(self.exp1, MRCfile("wedgeMask_Tom1.mrc").get_all_tom_density_values(),
                                      "First wedge mask generated from PEET run information did not match the expected"
                                      "mask.")
        np.testing.assert_array_equal(self.exp2, MRCfile("wedgeMask_Tom2.mrc").get_all_tom_density_values(),
                                      "Second wedge mask generated from PEET run information did not match the expected"
                                      "mask.")


if __name__ == "__main__":
    unittest.main()
