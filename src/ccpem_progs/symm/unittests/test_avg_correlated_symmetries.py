#!/usr/bin/env python
# coding: utf-8

import unittest
import avg_correlated_symmetries
import numpy as np
import os


class TestAnalysisMethods(unittest.TestCase):

    def setUp(self):
        self.npz = np.load("unittests/testdata/avg_corr_symm_testdata.npz")

    def test_import(self):
        array = avg_correlated_symmetries.import_tomos("unittests/testdata/avg_corr_test_data*")
        np.testing.assert_array_equal(array, self.npz["expected_import"], "Import array different from expected array.")

    def test_import_avg_many(self):
        avg_array, import_list_len = avg_correlated_symmetries.import_avg_many_tomos("unittests/testdata/avg_corr_test_data*")
        np.testing.assert_array_equal(avg_array, self.npz["expected_avg_import"],
                                      "Import array different from expected array.")
        self.assertEqual(import_list_len, self.npz["exp_list_len"])

    def test_import_avg_many_text(self):
        txt_in_array, import_list_len = avg_correlated_symmetries.import_avg_many_tomos(path=None,
                                                                       text_file="unittests/testdata/pathlist.txt")
        np.testing.assert_array_equal(txt_in_array, self.npz["expected_avg_import"], "Textfile import array different "
                                                                                     "from expected array.")
        self.assertEqual(import_list_len, self.npz["exp_list_len"])

    def test_import_txt(self):
        txt_in_array = avg_correlated_symmetries.import_tomos(path=None, text_file="unittests/testdata/pathlist.txt")
        np.testing.assert_array_equal(txt_in_array, self.npz["expected_import"], "Textfile import array different from "
                                                                                 "expected array.")

    def test_fft(self):
        result = avg_correlated_symmetries.main("unittests/testdata/avg_corr_test_data*", method="fft",
                                                ofilepath="temp.mrc")
        os.system("rm temp.mrc")  # remove the temporary result file created in the run
        np.testing.assert_array_equal(self.npz["fft_out"], result, "Result from FFT analysis does not match expected "
                                                                   "result.")

    def test_spectral(self):
        result = avg_correlated_symmetries.main("unittests/testdata/avg_corr_test_data*", method="spectral",
                                                ofilepath="temp.mrc")
        os.system("rm temp.mrc")  # remove the temporary result file created in the run
        np.testing.assert_array_equal(self.npz["ls_out"], result, "Result from Lomb-Scargle analysis does not match "
                                                                  "expected result.")

    def test_is_full(self):
        false_array = np.zeros((10, 5, 5))  # all empty

        false_array2 = np.copy(false_array)
        false_array2[0:5, 1, 2] = 1  # some full

        true_array = np.copy(false_array)
        true_array[:, 1, 2] = 1  # all full

        self.assertEqual(avg_correlated_symmetries.is_full(false_array), (False, 0),
                         "is_full() returned True on only 0 array")
        self.assertEqual(avg_correlated_symmetries.is_full(false_array2), (False, 5),
                         "is_full() returned True on only partially full array.")
        self.assertEqual(avg_correlated_symmetries.is_full(true_array), (True, 0),
                         "is_full() returned False on only full array.")

    def test_is_empty(self):
        true_array = np.zeros((5, 3, 2))  # all empty

        false_array2 = np.copy(true_array)
        false_array2[0:3, 1, 0] = 1  # some full

        false_array = np.copy(true_array)
        false_array[:, 2, 1] = 1  # all full

        self.assertEqual(avg_correlated_symmetries.is_empty(true_array), True,
                         "is_empty() returned False on only 0 array")
        self.assertEqual(avg_correlated_symmetries.is_empty(false_array2), False,
                         "is_empty() returned True on only partially empty array.")
        self.assertEqual(avg_correlated_symmetries.is_empty(false_array), False,
                         "is_empty() returned True on full array.")

if __name__ == "__main__":
    unittest.main()
