#!/usr/bin/env python
# coding: utf-8

import unittest
import numpy as np
import wedge_masking


class TestAnalysisMethods(unittest.TestCase):

    def setUp(self):
        pass

    def test_mask_wedge_tangent(self):
        polar = np.load("unittests/testdata/polar_sampling.npy")
        exp = np.load("unittests/testdata/expected_wedge_masking.npy")
        wedgepath = "unittests/testdata/small_40_mask.npy"
        result = wedge_masking.mask_wedge_tangent(polar_in=polar, wedgepath=wedgepath, mask_dims=(122, 38, 2))
        np.testing.assert_array_equal(exp, result, "generated internal wedge mask differs from expected values.")

if __name__ == "__main__":
        unittest.main()
