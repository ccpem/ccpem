#!/usr/bin/env python
# coding: utf-8


# Program to find and visualise symmetries in proteins.
#
# Note Constantin: Many comments now in the code have been added during review,
# need to be taken out again before publication

import sys
import os
from math import pi, cos, sin
import numpy as np
from helper_functions.nptomo import NumpyTom, MRCfile
from npy2mrc import save_npy_as_mrc_file
from autocorr import autocorrelation
from wedge_masking import mask_wedge_tangent
import logging
import argparse
from analysis_methods import fft_data, spectral_analyze_data

try:
    from scipy.ndimage import map_coordinates
    from scipy.signal import convolve2d
    # use fftconvolve instead of convolve2d to make it faster
except ImportError:
    print "\nScipy could not be imported.\nA slower function will be used instead.\n"
    from helper_functions.helper import convolve2d_hack


# passes floating point errors to log file
def err_handler(err_type, flag):
    logging.warning("Floating point error: %s. Flag provided: %s" % (err_type, flag))


def main(ifilepath,
         ofilepath=None,
         save_format="mrc",
         window_width=None,
         adjust_centre=None,
         verbose=True,
         missing_wedge=None,
         no_autocorrelate=None,
         save_sampling_matrix=None,
         save_autocorrelated=None,
         only_autocorrelate=None,
         for_get_noise=False,  # this option and method are only called by the get_autocorr_noise.py script
         method=None):
    """
    Transforms a file with EM 3D values into a sampling matrix of circles of varying height and radius along the
    rotational symmetry axis, autocorrelated along circles. Accepts either MRC or Numpy format for both input and
    output.
    """

    logging.basicConfig(filename='make_autocorr_samples.log', level=logging.INFO)
    logging.info("Processing subtomogram %s" % os.path.basename(ifilepath))
    np.seterrcall(err_handler)

    if not for_get_noise:
        print "\nRunning...\n"

    if verbose:  # displays additional information while running
        print "Start processing subtom '%s'..." % (os.path.basename(ifilepath))

    # -------------------------------------------------------------------------
    # Parameters / Settings
    # -------------------------------------------------------------------------

    if not ofilepath:
        if save_format.lower() == ".mrc" or save_format.lower() == "mrc":
            ac_path = ifilepath[:-4] + "_AC.mrc"
        elif save_format.lower() == ".npy" or save_format.lower() == "npy":
            ac_path = ifilepath[:-4] + "_AC.npy"
        else:
            print "Save format not supported. Choose from .mrc or .npy."
            logging.error("Output file format not supported.")
            sys.exit(1)

    else:
        if save_format.lower() == ".mrc" or save_format.lower() == "mrc":
            ac_path = ofilepath.replace(".mrc", "") + ".mrc"
        elif save_format.lower() == ".npy" or save_format.lower() == "npy":
            ac_path = ofilepath.replace(".npy", "") + ".npy"
        else:
            print "Save format not supported. Choose from .mrc or .npy."
            logging.error("Save file format not supported.")
            sys.exit(1)

    if window_width:  # option to define the ww
        ww = int(window_width)
    else:
        ww = None

    if adjust_centre:

        adjust_centre = adjust_centre.rstrip(")").lstrip("(")
        adjust_centre = adjust_centre.replace(",", "")
        adjust_centre = adjust_centre.split()

        adjust_centre = tuple(map(int, adjust_centre))  # all coordinates to int

        if len(adjust_centre) != 2:
            print "ERROR: When adjusting centre, provide exactly two coordinates, X Y."
            logging.error("Wrong number of coordinates for adjust_centre received.")
            sys.exit(1)

    if save_sampling_matrix:
        sampling_filepath = ifilepath[:-4] + "sampled"
    else:
        sampling_filepath = None

    # -------------------------------------------------------------------------
    # Preparation Step
    # -------------------------------------------------------------------------

    # Acquire the data
    if ifilepath[-4:].lower() == ".npy":  # sets the class NumpyTom or MRCfile to open_tom
        open_tom = NumpyTom
    elif ifilepath[-4:].lower() == ".mrc" or ifilepath[-4:].lower() == ".rec":
        open_tom = MRCfile
    else:
        print 'Error: Input file format not supported'
        logging.error("Input file format not supported.")
        return 0

    with open_tom(ifilepath) as t:  # t is the instance of the above defined class

        # Clean the data (reformat etc)
        data = t.get_all_tom_density_values()

        # What does the data look like ?
        # data[20,40,40] = 150
        # save_npy_as_mrc_file("test_mrc.mrc", data)
        # sys.exit(0)

        # Variables we need from the tomogram
        z_dim_tomo = t.width  # actually z
        y_dim_tomo = t.height  # indeed y
        x_dim_tomo = t.depth  # actually x

        centre = adjust_centre if adjust_centre else (truncate(x_dim_tomo / 2), truncate(y_dim_tomo / 2))

        if verbose:
            print "Width: %d\t Height: %d\t Depth: %d" % (z_dim_tomo, y_dim_tomo, x_dim_tomo)
            logging.info("Width: %d\t Height: %d\t Depth: %d\n" % (z_dim_tomo, y_dim_tomo, x_dim_tomo))

    # Sampling (ie. getting the necessary values in the right format)

    # Calculate variables we need for sampling matrix
    min_radius = 1
    max_radius = min(x_dim_tomo - centre[0], min(centre), y_dim_tomo - centre[1]) - 1
    sample_size = int(pi * max_radius)
    min_height = 1
    max_height = z_dim_tomo - 1

    # Building the sampling matrix
    r = radii = np.arange(min_radius, max_radius)
    a = angles = np.arange(sample_size)
    a = 2 * pi * a / sample_size  # equals times 2/max_radius
    dt = [('radius', int),
          ('angle', float)]
    dim = (len(r), len(a))
    p = polar_coor = np.zeros(dim, dtype=dt)  # sets a new array full of 0
    p['radius'] = r[:, np.newaxis]
    p['angle'] = a

    # generate the missing wedge mask
    if missing_wedge:
        mask = mask_wedge_tangent(p, missing_wedge,
                                  (sample_size, max_height - 1, max_radius - 1))
    else:
        mask = np.ones((sample_size, max_height - 1, max_radius - 1))

    p = p.ravel()  # returns P as a 1dim array, ie an array of tuples in 1dim

    # convert from polar coordinates (radius, angle) to cartesian coordinates (x,z)
    ct = [('x', float),
          ('z', float)]
    c = map(lambda radius, angle: (cos(angle) * radius, sin(angle) * radius), p['radius'], p['angle'])
    c = np.array(c, dtype=ct).reshape(dim)

    # this step is necessary to center the cartesian coordinate system on centre
    c['x'] += centre[0]
    c['z'] += centre[1]
    # at this point, C is a 2d array of cartesian coordinate tuples, where every row corresponds to points on the same
    # radius, every column to points at the same angle

    # add z dimension to get the sampling matrix
    z = np.arange(min_height, max_height)  # have to change this to z
    z_ = z[np.newaxis, :, np.newaxis]
    c_ = np.swapaxes(c, 0, 1)
    c_ = c_[:, np.newaxis, :]

    # dim means that for every a, z and r, there is a subarray with 3 cartesian coordinates
    # two dimensions are added compared to the C array since no more tupels are used on the lowest level
    dim = (len(a), len(z), len(r), 3)

    vp = voxel_positions = np.zeros(dim)
    vp[:, :, :, 0] = z_
    vp[:, :, :, 1] = c_['z']
    vp[:, :, :, 2] = c_['x']
    # VP at this point, 1st dim = same angle, 2nd = same Y slice, 3rd same radius, 4th coordinates

    np.set_printoptions(threshold=np.inf)  # for diagnostics

    # map sampling matrix voxel positions to voxel data (ie in tomogram)
    v = vp.ravel().reshape(-1, 3).T

    try:
        vv = map_coordinates(data, v)  # reminder: data are the tomogram density values
    except NameError:
        print NameError
        logging.warning(NameError)
        vv = map(lambda z, y, x: data[z, y, x], v[0], v[1], v[2])
        vv = fvv = np.array(vv)
    fvv = vv  # what is the point of that??
    vv = vv.reshape(dim[:-1])
    # at this point, VV contains density values at the sampling coordinates?

    # average data across 3*3 window
    if window_width:

        avv = np.zeros(vv.shape)  # averaged voxel values
        avg_mask = np.zeros(mask.shape)

        try:
            ww_mask = np.ones((ww, ww)) / ww ** 2
            for z in xrange(avv.shape[0]):
                avv[z] = convolve2d(vv[z], ww_mask, mode="same", boundary="fill", fillvalue=0)
            for z in xrange(avg_mask.shape[0]):  # applying same averaging to the mask to ensure same dimensions
                avg_mask[z] = convolve2d(mask[z], ww_mask, mode="same", boundary="fill", fillvalue=0)
        except NameError:
            logging.warning(NameError)
            for z in xrange(avv.shape[0]):
                stmt = convolve2d_hack("VV[z]", "AVV[z]", ww)
                exec stmt in locals(), locals()
                avv[z] /= ww ** 2
            for z in xrange(avg_mask.shape[0]):
                stmt = convolve2d_hack("mask[z]", "avg_mask[z]", ww)
                exec stmt in locals(), locals()
                avg_mask[z] /= ww ** 2

        mask = avg_mask

    else:
        avv = vv

    # -------------------------------------------------------------------------
    # Optional save of diagnostic output
    # -------------------------------------------------------------------------

    if save_sampling_matrix:
        save_npy_as_mrc_file(sampling_filepath, avv)

    # -------------------------------------------------------------------------
    # Autocorrelation
    # -------------------------------------------------------------------------
    if not no_autocorrelate:

        if verbose:
            print "\nAutocorrelating the sampling circles."

        ac_avv, mask, warning = autocorrelation(avv, mask, verbose=verbose)
        avv = ac_avv

        if warning:
            ac_path = ac_path.rstrip(".mrc") + "_warning.mrc"

            print "'_warning' label added. For details, see documentation and log file."
            logging.warning('\nThe particle is oriented so that the information in the sampling radii is in large '
                            'parts derived from the missing wedge. This leads to a large amount of masked data in the '
                            'analysis. The file therefore was labeled "_warning" and it might be useful not to include '
                            'this file in the final averaging.\n')

    # masking with NaN so masked values are ignored in the averaging in the next step
    avv = np.where(mask == 0, np.nan, avv)

    # the noise profiles are generated from angular power spectra generated from individual noise files. Spectral
    # decomposition therefore has to be performed.
    if for_get_noise:

        if for_get_noise == "alt":
            return avv
        else:
            if method.lower() == "fft":
                if verbose:
                    print "Using FFT...\n"
                logging.info("Using FFT.")
                analysis_result = fft_data(avv)

            elif method.lower() == "spectral":
                logging.info("Using Lomb-Scargle analysis.")
                if verbose:
                    print "Using Lomb-Scargle analysis...\n"
                analysis_result = spectral_analyze_data(avv, precenter=True, normalize=True,
                                                        missing_wedge=mask)

            else:
                print "ERROR: Method not recognised, available: Fast-Fourier-Transform (fft) and " \
                      "Lomb-Scargle-periodigram (spectral)"
                sys.exit(1)

            # save_npy_as_mrc_file(ifilepath.replace(".mrc", "") + "_freq.mrc", analysis_result)
            return analysis_result

    if save_format.lower() == ".mrc" or save_format.lower() == "mrc":
        save_npy_as_mrc_file(ac_path, avv)

    elif save_format.lower() == ".npy" or save_format.lower() == "npy":
        np.save(ac_path, avv)

    else:
        print "Save format not supported. Choose from .mrc or .npy."

    return avv


# Truncates/pads a float f to 0 decimal places without rounding
def truncate(f):
    s = '{}'.format(f)
    if 'e' in s or 'E' in s:
        return '{0:.{1}f}'.format(f, 0)
    i, p, d = s.partition('.')
    return int(''.join([i, (d + '0' * 0)[:0]]))


if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('ifilepath',
                        help="Input file from which to generate the autocorrelated sampling array. Format needs to be "
                             ".npy or .mrc")
    parser.add_argument('-o', '--ofilepath',
                        default=None,
                        help='Path, to which the autocorrelated data should be saved. If None, save to '
                             '<infilepath>_AC.mrc')
    parser.add_argument('-f', '--save-format',
                        default="mrc",
                        type=str,
                        help='Format of output file. Can be ".mrc" or ".npy". Default: .mrc')
    parser.add_argument('-w', '--window-width',
                        default=None,
                        help='Window width to bin pixels in.')
    parser.add_argument('-c', '--adjust-centre',
                        default=None,
                        help='Adjusts the centre to user input. (x,y) coordinate order.')
    parser.add_argument('-v', '--verbose',
                        action="store_true",
                        help='Verbosity. If provided, print name of the subtomogram file being processed and size.')
    parser.add_argument('-mw', '--missing-wedge',
                        default=None,
                        help='Missing wedge volume to mask missing data')
    parser.add_argument('-na', '--no-autocorrelate',
                        action="store_true",
                        help='If provided, do not autocorrelate the data before analysis.')
    parser.add_argument('-sm', '--save-sampling-matrix',
                        action="store_true",
                        help='Save the sampling matrix before autocorrelation. Can be useful for diagnostics')

    args = parser.parse_args()

    if not args.ifilepath:
        parser.error("Incorrect number of parameters!\n")
        sys.exit(1)

    main(**vars(args))
