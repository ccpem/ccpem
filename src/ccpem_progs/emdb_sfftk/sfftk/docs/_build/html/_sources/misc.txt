====================================
Miscellaneous Operations Using sfftk
====================================

Viewing File Metadata
=====================

$ sff view <file>

Settings Configurations
=======================

TBA

Running Unit Tests
==================

$ sff test [tool]

$ sff tests [tool]

<tool> is optional
