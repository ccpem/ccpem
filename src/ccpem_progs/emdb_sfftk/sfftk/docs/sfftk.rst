sfftk packages
===============

.. automodule:: sfftk.__init__
	:members:
	:show-inheritance:


sfftk.readers package
---------------------

.. automodule:: sfftk.readers.amreader
	:members:
	:show-inheritance:
	:inherited-members:

.. automodule:: sfftk.readers.mapreader
	:members:
	:show-inheritance:
	:inherited-members:
	
.. automodule:: sfftk.readers.modreader
	:members:
	:show-inheritance:
	:inherited-members:
	
.. automodule:: sfftk.readers.segreader
	:members:
	:show-inheritance:
	:inherited-members:
	
.. automodule:: sfftk.readers.stlreader
	:members:
	:show-inheritance:
	:inherited-members:

.. automodule:: sfftk.readers.surfreader
	:members:
	:show-inheritance:
	:inherited-members:

sfftk.formats package
---------------------

.. automodule:: sfftk.formats.am
	:members:
	:show-inheritance:
	:inherited-members:

.. automodule:: sfftk.formats.map
	:members:
	:show-inheritance:
	:inherited-members:

.. automodule:: sfftk.formats.mod
	:members:
	:show-inheritance:
	:inherited-members:

.. automodule:: sfftk.formats.seg
	:members:
	:show-inheritance:
	:inherited-members:

.. automodule:: sfftk.formats.stl
	:members:
	:show-inheritance:
	:inherited-members:

.. automodule:: sfftk.formats.surf
	:members:
	:show-inheritance:
	:inherited-members:


sfftk.notes package
-----------------------

.. automodule:: sfftk.notes.find
	:members:
	:show-inheritance:
	:inherited-members:

.. automodule:: sfftk.notes.view
	:members:
	:show-inheritance:
	:inherited-members:
	
.. automodule:: sfftk.notes.modify
	:members:
	:show-inheritance:
	:inherited-members:
	

sfftk.core package
-----------------------

.. automodule:: sfftk.core.configs
	:members:
	:show-inheritance:
	:inherited-members:

.. automodule:: sfftk.core.parser
	:members:
	:show-inheritance:
	:inherited-members:

.. automodule:: sfftk.core.print_tools
	:members:
	:show-inheritance:
	:inherited-members:
