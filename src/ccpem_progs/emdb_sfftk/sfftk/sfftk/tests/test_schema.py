'''
Created on 20 Feb 2017

@author: pkorir
'''
import unittest
from ..schema import SFFSegmentation
import tempfile


class Test(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass


    def test_as_json(self):
        """Test that we can convert an EMDB-SFF to a JSON"""
        # read the sff
        seg = SFFSegmentation("/Users/pkorir/Documents/workspace/bioimaging-scripts/trunk/sfftk/sfftk/test_data/sff/emd_1014.sff",silence=True)
        # read the json (previously converted)
        j = open("/Users/pkorir/Documents/workspace/bioimaging-scripts/trunk/sfftk/sfftk/test_data/json/emd_1014.json")
        # convert the sff
        tf = tempfile.NamedTemporaryFile()
        seg.as_json(tf.name)
        # compare
        self.assertEqual(j.read(), tf.read())
        j.close()


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_as_json']
    unittest.main()