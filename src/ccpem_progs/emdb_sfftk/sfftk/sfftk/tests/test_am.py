#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
"""
sfftk.tests.test_am

Copyright 2017 EMBL - European Bioinformatics Institute
Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License. 
You may obtain a copy of the License at 

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software 
distributed under the License is distributed on an 
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
either express or implied. 

See the License for the specific language governing permissions 
and limitations under the License.
"""

__author__ = "Paul K. Korir, PhD"
__email__ = "pkorir@ebi.ac.uk, paul.korir@gmail.com"
__date__ = 2017-03-28

import sys, os
import unittest
from ..formats.am import AmiraMeshSegmentation
from . import TEST_DATA_PATH
import h5py


class Test_format_am(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(Test_format_am, cls).setUpClass()
        cls.am_seg = AmiraMeshSegmentation(
            os.path.join(
                TEST_DATA_PATH, 
                'seg', 
                'test_data.am'
                ),
            verbose=True,
            )
        cls.sff_seg = cls.am_seg.convert_to_sff(
            name="My AmiraMesh Segmentation",
            processingDetails="Processing details",
            softwareVersion="0.0.0",
            details="Some details",
            )
        with h5py.File('temp.hff', 'w') as h:
            cls.am_seg.convert_to_hff(h)
    @classmethod
    def tearDownClass(cls):
        os.remove('temp.hff')
        assert not os.path.exists('temp.hff')
        super(Test_format_am, cls).tearDownClass()
    def test_convert_to_sff(self):
        """Conversion to EMDB-SFF (XML)"""
        # header
        self.assertEqual(self.sff_seg.name, "My AmiraMesh Segmentation")
        self.assertEqual(self.sff_seg.version, '0.5.8')
        # software
        self.assertEqual(self.sff_seg.software.name, "Amira")
        self.assertEqual(self.sff_seg.software.version, "0.0.0")
        self.assertEqual(self.sff_seg.software.processingDetails, "Processing details")
        self.assertEqual(self.sff_seg.details, "Some details")
    def test_convert_to_hff(self):
        """Conversion to HDF5 format of EMDB-SFF"""
        h = h5py.File('test.hff')
        # header
        self.assertEqual(h['/name'], "My AmiraMesh Segmentation")





if __name__ == "__main__":
    unittest.main()