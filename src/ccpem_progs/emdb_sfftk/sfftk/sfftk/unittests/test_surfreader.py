#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division

"""
test_surfreader.py

Unit tests for surfreader module

Run as:
python -m unittest test_surfreader


TODO:
- 

Version history:
0.0.1, 2015-10-30, commandline tests
0.0.2, 2015-11-05, basic and data tests

"""

__author__ = 'Paul K. Korir, PhD'
__email__ = 'pkorir@ebi.ac.uk'
__date__ = '2015-10-30'

import sys
import unittest
from readers import surfreader
import tests

class TestSurfreader_basic(unittest.TestCase):
    def test_fails_on_nonbinary_file(self):
        """
        fileformat ASCII not supported
        """
        f = open(tests.TEST_DATA_PATH + 'test_data/test_bad_data1.surf', 'rb')
        with self.assertRaises(AssertionError):
            surfreader.get_data(f) # missing 'IMOD' start


class TestSurfreader_data(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        """
        Setup
        """
        cls.surf = surfreader.get_data(open(tests.TEST_DATA_PATH + 'test_data/test_data.surf', 'rb'))
    
    def test_no_of_patches(self):
        """
        number of patches
        """
        self.assertEqual(self.surf.Patches_count, len(self.surf.Patches))
    
    def test_no_of_vertices(self):
        """
        number of vertices
        """
        self.assertEqual(self.surf.Vertices_count, len(self.surf.Vertices))
    
    def test_no_of_branching_points(self):
        """
        number of branching points
        """
        self.assertEqual(self.surf.NBranchingPoints_count, len(self.surf.NBranchingPoints))
        
    def test_no_of_vertices_on_curves(self):
        """
        number of vertices on curves
        """
        self.assertEqual(self.surf.NVerticesOnCurves_count, len(self.surf.NVerticesOnCurves))
    
    def test_no_of_boundary_curves(self):
        """
        number of boundary curves
        """
        self.assertEqual(self.surf.BoundaryCurves_count, len(self.surf.BoundaryCurves))
    
    def test_no_of_triangles_in_patches(self):
        """
        number of triangles in patches
        """
        for p,Patch in self.surf.Patches.iteritems():
            self.assertEqual(Patch.triangle_count, len(Patch.Triangles))
        
    def test_surfmodel_is_not_populated(self):
        """
        check the is_populated flag as False for empty model
        """
        surf = surfreader.Model('0.1', 'BINARY')
        self.assertFalse(surf.is_populated)
    
    def test_surfmodel_is_populated(self):
        """
        check the is_populated flag as True for populated model
        """
        self.assertTrue(self.surf.is_populated)
        

class TestSurfreader_commandlineoptions(unittest.TestCase):
    def test_missing_arguments(self):
        """
        Test that missing arguments raise TypeError
        """
        self.assertRaises(TypeError, surfreader.parse_args, [])
    
    def test_missing_surf_file(self):
        """
        .surf file absent -> TypeError
        """
        self.assertRaises(TypeError, surfreader.parse_args, ['-o', 'output.txt'])
    
    def test_pos_arg_not_surf_file(self):
        """
        pos_arg != .surf -> TypeError
        """
        self.assertRaises(TypeError, surfreader.parse_args, ['file.xxxx'])
    
    @unittest.expectedFailure
    def test_good_args(self):
        """
        working commandline args
        
        add a new line for every proper format
        """
        self.assertRaises(TypeError, surfreader.parse_args, ['file.surf'])
        self.assertRaises(TypeError, surfreader.parse_args, ['-o', 'output.txt', 'file.surf'])
    


if __name__ == "__main__":
    unittest.main()