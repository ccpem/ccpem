#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division

"""
test_stlreader.py

Unit tests for stlreader.py
"""

__author__  = 'Paul K. Korir, PhD'
__email__   = 'pkorir@ebi.ac.uk, paul.korir@gmail.com'
__date__    = '2016-08-11'


import sys
from readers import stlreader
import unittest
# import os, glob
# from readers.sffreader import get_data, get_aux_data
# import __main__ as Main
# from utils.parser import Parser


class TestStLReader_basic(unittest.TestCase):
    def test_read_ascii(self):
        """Test that we can read an ASCII StL file"""
        name, vertices, polygons = next(stlreader.get_data('sff/test_data/test_data.stl'))
        
        self.assertTrue(name, "None")
        self.assertTrue(len(vertices) > 0)
        self.assertTrue(len(polygons) > 0)
        
        # advanced test: all references vertices are present
        polygon_ids = list()
        for a, b, c in polygons.itervalues():
            polygon_ids += [a, b, c]
            
        self.assertItemsEqual(set(vertices.keys()), set(polygon_ids))
    
    def test_read_binary(self):
        """Test that we can read a binary StL file"""
        name, vertices, polygons = next(stlreader.get_data('sff/test_data/test_data_binary.stl'))
        
        self.assertTrue(name, "Binary mesh")
        self.assertTrue(len(vertices) > 0)
        self.assertTrue(len(polygons) > 0)
        
        polygon_ids = list()
        for a, b, c in polygons.itervalues():
            polygon_ids += [a, b, c]
        
        self.assertItemsEqual(set(vertices.keys()), set(polygon_ids))
    
    def test_read_multiple(self):
        """Test that we can read a multi-solid StL file
        
        Only works for ASCII by concatenation"""
        for name, vertices, polygons in stlreader.get_data('sff/test_data/test_data_multiple.stl'):
            self.assertTrue(name, "None")
            self.assertTrue(len(vertices) > 0)
            self.assertTrue(len(polygons) > 0)
            
            polygon_ids = list()
            for a, b, c in polygons.itervalues():
                polygon_ids += [a, b, c]
            
            self.assertItemsEqual(set(vertices.keys()), set(polygon_ids))
        
    