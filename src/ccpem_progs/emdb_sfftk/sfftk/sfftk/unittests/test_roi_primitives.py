# -*- coding: utf-8 -*-
from __future__ import division
"""
test_roi_primitives.py

Unit tests for roi_primitives module

Run as:
python -m unittest test_regions

Requirements
============
* OMERO 5.2.0 installed and working
* A user account (set credentials below)
* A map image loaded in the database for which the imageId should be set in BaseTest.setUp()

TODO:
* pass params e.g. imageId as command-line args
* delete ALL ROIs created

Version history:
0.0.1, 2016-01-20, First incarnation

"""

__author__  = 'Paul K. Korir, PhD'
__email__   = 'pkorir@ebi.ac.uk'
__date__    = '2016-01-20'

import sys
import unittest
import random

# the module under test
from omero_wrapper import roi_primitives

from omero.gateway import BlitzGateway
import omero.model
import omero.rtypes
import omero.callbacks
import omero.cmd

# globals
HOST    = 'localhost' # change if testing on another server
USER    = 'root'
PASSWD  = 'root'
PORT    = '4064'


imageId = 2
roiName = 'MyRoi'

# utility functions
def colorInt(r, g, b, a):
    """Function that return a signed 32-bit integer from RGBA tuple.
    
    0 <= r,g,b,a <= 1
    """
    assert 0 <= r <= 1
    assert 0 <= g <= 1
    assert 0 <= b <= 1
    assert 0 <= a <= 1
    # the actual format is 'ARGB'!!!
    rgba_int = (int(a*255) << 24) + (int(r*255) << 16) + (int(g*255) << 8) + int(b*255)
    if rgba_int > 2147483647:
        rgba_int = -2147483648 + rgba_int % 2147483648
    return rgba_int 


"""
All tests will need a connection to the OMERO server
"""
class BaseTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        """Connect to the database and load the image.
        """
        cls.conn = BlitzGateway(
            host=HOST,
            username=USER,
            passwd=PASSWD,
            port=PORT,
            )
        cls.conn.connect()
        cls.updateService = cls.conn.getUpdateService()
        cls.image = cls.conn.getObject('Image', imageId)
#         cls.roi = roi_primitives.ROI(name=roiName, image=cls.image)
        # go the long way to create an ROI
        cls.roi = omero.model.RoiI()
        cls.roi.setImage(cls.image._obj)
        cls.roi.setName(omero.rtypes.rstring(roiName))
        cls.roi = cls.updateService.saveAndReturnObject(cls.roi)
        
    @classmethod
    def tearDownClass(cls):
        """Close the connection.
        Also should delete the image and ROIs created.
        """        
        # You can delete a number of objects of the same type at the same
        # time. In this case 'Project'. Use deleteChildren=True if you are
        # deleting a Project and you want to delete Datasets and Images.
        obj_ids = [cls.roi.getId().getValue()]
        handle = cls.conn.deleteObjects(
            "Roi", 
            obj_ids, 
            deleteAnns=True, 
            deleteChildren=True
        )
        
        # This is not necessary for the Delete to complete. Can be used
        # if you want to know when delete is finished or if there were any errors
        cb = omero.callbacks.CmdCallbackI(cls.conn.c, handle)
#         print >> sys.stderr, "Deleting, please wait."
#         while not cb.block(500):
#             print >> sys.stderr, "."
        err = isinstance(cb.getResponse(), omero.cmd.ERR)
#         print >> sys.stderr,"Error?", err
        if err:
            print >> sys.stderr, cb.getResponse()
        cb.close(True)      # close handle too
        
        # When you are done, close the session to free up server resources.
        cls.conn._closeSession()
        


class TestROI(BaseTest):
    """
    Test ROI convenience object
    """ 
    @classmethod
    def setUpClass(cls):
        super(TestROI, cls).setUpClass()
        
    
    @classmethod
    def tearDownClass(cls):
        super(TestROI, cls).tearDownClass()
        
    def test_ROI_type(self):
        self.assertTrue(isinstance(self.roi, omero.model.RoiI))
    
    def test_ROI_name(self):
        self.assertEqual(self.roi.getName().getValue(), roiName)
     
    def test_ROI_image(self):
        self.assertTrue(isinstance(self.roi.getImage(), omero.model.ImageI))
            


class TestShape(BaseTest):
    """
    Test generic Shape attributes with a Rectangle ROI object
        FillColor
        FontFamily
        FontSize
        FontStyle
        StrokeColor
        StrokeWidth
        TheC
        TheT
        TheZ
        TextValue
    """
    @classmethod
    def setUpClass(cls):
        super(TestShape, cls).setUpClass()
        cls.theT = 0
        cls.theZ = 50
        cls.X = 10
        cls.Y = 20
        cls.Width = 30
        cls.Height = 40
        cls.TextValue = 'I am a rectangle'
        
        # create the shape with the above params
        cls.rect = roi_primitives.Rectangle(theT=cls.theT, theZ=cls.theZ, X=cls.X, Y=cls.Y, Width=cls.Width, Height=cls.Height)
        cls.rect.setFillColor(1, 0, 0) # red
        cls.rect.setFontFamily('serif')
        cls.rect.setFontSize(12, 'POINT')
        cls.rect.setFontStyle('italic')
        cls.rect.setStrokeColor(0, 1, 0) # green
        cls.rect.setTextValue(cls.TextValue)
        
        # add the shape to the ROI
        cls.roi.addShape(cls.rect)
        cls.roi = cls.updateService.saveAndReturnObject(cls.roi)
    
    @classmethod
    def tearDownClass(cls):
        super(TestShape, cls).tearDownClass()
        
    def setUp(self, *args, **kwargs):
        super(TestShape, self).setUp(*args, **kwargs)
    
    def test_added_shape(self):
        self.assertTrue(isinstance(TestShape.roi.getShape(0), omero.model.RectangleI))
    
    def test_rectangle_type(self):
        self.assertTrue(isinstance(TestShape.rect, omero.model.RectangleI))
    
    def test_theT(self):
        self.assertEqual(TestShape.rect.getTheT(), TestShape.theT)
    
    def test_theZ(self):
        self.assertEqual(TestShape.rect.getTheZ(), TestShape.theZ)
    
    def test_X(self):
        self.assertEqual(TestShape.rect.getX(), TestShape.X)
    
    def test_Y(self):
        self.assertEqual(TestShape.rect.getY(), TestShape.Y)
    
    def test_Width(self):
        self.assertEqual(TestShape.rect.getWidth(), TestShape.Width)
    
    def test_Height(self):
        self.assertEqual(TestShape.rect.getHeight(), TestShape.Height)
    
    def test_fillColor(self):
        self.assertEqual(TestShape.rect.getFillColor(), colorInt(1, 0, 0, 1))
    
    def test_fontFamily(self):
        self.assertEqual(TestShape.rect.getFontFamily(), 'serif')
    
    def test_fontSize(self):
        self.assertEqual(TestShape.rect.getFontSize(), 12)
    
    def test_fontStyle(self):
        self.assertEqual(TestShape.rect.getFontStyle(), 'italic')
    
    def test_strokeColor(self):
        self.assertEqual(TestShape.rect.getStrokeColor(), colorInt(0, 1, 0, 1))
    
    def test_textValue(self):
        self.assertEqual(TestShape.rect.getTextValue(), TestShape.TextValue)
    
    def test_remove_shape(self):
        # remove the first and only item in the ROI
        # use the _shapesSeq list
        TestShape.roi.removeShape(TestShape.roi._shapesSeq[0])
        TestShape.roi = TestShape.updateService.saveAndReturnObject(TestShape.roi)
        with self.assertRaises(IndexError):
            TestShape.roi.getShape(0)

    def tearDown(self, *args, **kwargs):
        super(TestShape, self).tearDown(*args, **kwargs)


class TestLine(BaseTest):
    @classmethod
    def setUpClass(cls):
        super(TestLine, cls).setUpClass()
        cls.theT = 0
        cls.theZ = 50
        cls.X1 = 10
        cls.X2 = 20
        cls.Y1 = 30
        cls.Y2 = 40
        cls.TextValue = 'I am an line'
        
        # create the shape with the above params
        cls.line = roi_primitives.Line(theT=cls.theT, theZ=cls.theZ, X1=cls.X1, X2=cls.X2, Y1=cls.Y1, Y2=cls.Y2)
        cls.line.setFillColor(1, 0, 0) # red
        cls.line.setFontFamily('serif')
        cls.line.setFontSize(12, 'POINT')
        cls.line.setFontStyle('italic')
        cls.line.setStrokeColor(0, 1, 0) # green
        cls.line.setTextValue(cls.TextValue)
        
        # add the shape to the ROI
        cls.roi.addShape(cls.line)
        cls.roi = cls.updateService.saveAndReturnObject(cls.roi)
    
    @classmethod
    def tearDownClass(cls):
        super(TestLine, cls).tearDownClass()
        
    def setUp(self, *args, **kwargs):
        super(TestLine, self).setUp(*args, **kwargs)
    
    def test_added_shape(self):
        self.assertTrue(isinstance(TestLine.roi.getShape(0), omero.model.LineI))
    
    def test_rectangle_type(self):
        self.assertTrue(isinstance(TestLine.line, omero.model.LineI))
    
    def test_theT(self):
        self.assertEqual(TestLine.line.getTheT(), TestLine.theT)
    
    def test_theZ(self):
        self.assertEqual(TestLine.line.getTheZ(), TestLine.theZ)
    
    def test_X1(self):
        self.assertEqual(TestLine.line.getX1(), TestLine.X1)
    
    def test_X2(self):
        self.assertEqual(TestLine.line.getX2(), TestLine.X2)
    
    def test_Y1(self):
        self.assertEqual(TestLine.line.getY1(), TestLine.Y1)
    
    def test_Y2(self):
        self.assertEqual(TestLine.line.getY2(), TestLine.Y2)


class TestEllipse(BaseTest):
    @classmethod
    def setUpClass(cls):
        super(TestEllipse, cls).setUpClass()
        cls.theT = 0
        cls.theZ = 50
        cls.Cx = 10
        cls.Cy = 20
        cls.Rx = 30
        cls.Ry = 40
        cls.TextValue = 'I am an ellipse'
        
        # create the shape with the above params
        cls.ell = roi_primitives.Ellipse(theT=cls.theT, theZ=cls.theZ, Cx=cls.Cx, Cy=cls.Cy, Rx=cls.Rx, Ry=cls.Ry)
        cls.ell.setFillColor(1, 0, 0) # red
        cls.ell.setFontFamily('serif')
        cls.ell.setFontSize(12, 'POINT')
        cls.ell.setFontStyle('italic')
        cls.ell.setStrokeColor(0, 1, 0) # green
        cls.ell.setTextValue(cls.TextValue)
        
        # add the shape to the ROI
        cls.roi.addShape(cls.ell)
        cls.roi = cls.updateService.saveAndReturnObject(cls.roi)
    
    @classmethod
    def tearDownClass(cls):
        super(TestEllipse, cls).tearDownClass()
        
    def test_added_shape(self):
        self.assertTrue(isinstance(TestEllipse.roi.getShape(0), omero.model.EllipseI))
    
    def test_rectangle_type(self):
        self.assertTrue(isinstance(TestEllipse.ell, omero.model.EllipseI))
    
    def test_theT(self):
        self.assertEqual(TestEllipse.ell.getTheT(), TestEllipse.theT)
    
    def test_theZ(self):
        self.assertEqual(TestEllipse.ell.getTheZ(), TestEllipse.theZ)
    
    def test_Cx(self):
        self.assertEqual(TestEllipse.ell.getCx(), TestEllipse.Cx)
    
    def test_Cy(self):
        self.assertEqual(TestEllipse.ell.getCy(), TestEllipse.Cy)
    
    def test_Rx(self):
        self.assertEqual(TestEllipse.ell.getRx(), TestEllipse.Rx)
    
    def test_Ry(self):
        self.assertEqual(TestEllipse.ell.getRy(), TestEllipse.Ry)
    
    def test_textValue(self):
        self.assertEqual(TestEllipse.ell.getTextValue(), TestEllipse.TextValue)


# tested in TestShape
class TestRectangle(BaseTest):
    @classmethod
    def setUpClass(cls):
        super(TestRectangle, cls).setUpClass()
        cls.theT = 0
        cls.theZ = 50
        cls.X = 10
        cls.Y = 20
        cls.Width = 30
        cls.Height = 40
        cls.TextValue = 'I am a mask'
        
        # create the shape with the above params
        cls.rect = roi_primitives.Rectangle(theT=cls.theT, theZ=cls.theZ, X=cls.X, Y=cls.Y, Width=cls.Width, Height=cls.Height)
        cls.rect.setFillColor(1, 0, 0) # red
        cls.rect.setFontFamily('serif')
        cls.rect.setFontSize(12, 'POINT')
        cls.rect.setFontStyle('italic')
        cls.rect.setStrokeColor(0, 1, 0) # green
        cls.rect.setTextValue(cls.TextValue)
        
        # add the shape to the ROI
        cls.roi.addShape(cls.rect)
        cls.roi = cls.updateService.saveAndReturnObject(cls.roi)
    
    @classmethod
    def tearDownClass(cls):
        super(TestRectangle, cls).tearDownClass()
        
    def setUp(self, *args, **kwargs):
        super(TestRectangle, self).setUp(*args, **kwargs)
    
    def test_added_shape(self):
        self.assertTrue(isinstance(TestRectangle.roi.getShape(0), omero.model.RectangleI))
    
    def test_rectangle_type(self):
        self.assertTrue(isinstance(TestRectangle.rect, omero.model.RectangleI))
    
    def test_theT(self):
        self.assertEqual(TestRectangle.rect.getTheT(), TestRectangle.theT)
    
    def test_theZ(self):
        self.assertEqual(TestRectangle.rect.getTheZ(), TestRectangle.theZ)
    
    def test_X(self):
        self.assertEqual(TestRectangle.rect.getX(), TestRectangle.X)
    
    def test_Y(self):
        self.assertEqual(TestRectangle.rect.getY(), TestRectangle.Y)
    
    def test_Width(self):
        self.assertEqual(TestRectangle.rect.getWidth(), TestRectangle.Width)
    
    def test_Height(self):
        self.assertEqual(TestRectangle.rect.getHeight(), TestRectangle.Height)
    
    def test_textValue(self):
        self.assertEqual(TestRectangle.rect.getTextValue(), TestRectangle.TextValue)


class TestMask(BaseTest):
    @classmethod
    def setUpClass(cls):
        super(TestMask, cls).setUpClass()
        cls.theT = 0
        cls.theZ = 50
        cls.X = 10
        cls.Y = 20
        cls.Width = 30
        cls.Height = 40
        cls.TextValue = 'I am a mask'
        
        # create the shape with the above params
        cls.mask = roi_primitives.Mask(theT=cls.theT, theZ=cls.theZ, X=cls.X, Y=cls.Y, Width=cls.Width, Height=cls.Height)
        cls.mask.setFillColor(1, 0, 0) # red
        cls.mask.setFontFamily('serif')
        cls.mask.setFontSize(12, 'POINT')
        cls.mask.setFontStyle('italic')
        cls.mask.setStrokeColor(0, 1, 0) # green
        cls.mask.setTextValue(cls.TextValue)
        
        # add the shape to the ROI
        cls.roi.addShape(cls.mask)
        cls.roi = cls.updateService.saveAndReturnObject(cls.roi)
    
    @classmethod
    def tearDownClass(cls):
        super(TestMask, cls).tearDownClass()
        
    def setUp(self, *args, **kwargs):
        super(TestMask, self).setUp(*args, **kwargs)
    
    def test_added_shape(self):
        self.assertTrue(isinstance(TestMask.roi.getShape(0), omero.model.MaskI))
    
    def test_rectangle_type(self):
        self.assertTrue(isinstance(TestMask.mask, omero.model.MaskI))
    
    def test_theT(self):
        self.assertEqual(TestMask.mask.getTheT(), TestMask.theT)
    
    def test_theZ(self):
        self.assertEqual(TestMask.mask.getTheZ(), TestMask.theZ)
    
    def test_X(self):
        self.assertEqual(TestMask.mask.getX(), TestMask.X)
    
    def test_Y(self):
        self.assertEqual(TestMask.mask.getY(), TestMask.Y)
    
    def test_Width(self):
        self.assertEqual(TestMask.mask.getWidth(), TestMask.Width)
    
    def test_Height(self):
        self.assertEqual(TestMask.mask.getHeight(), TestMask.Height)
    
    def test_textValue(self):
        self.assertEqual(TestMask.mask.getTextValue(), TestMask.TextValue)


class TestPolyline(BaseTest):
    @classmethod
    def setUpClass(cls):
        super(TestPolyline, cls).setUpClass()
        cls.theT = 0
        cls.theZ = 50
        cls.points = [(random.randint(0, 100), random.randint(0, 100)) for i in xrange(10)]
        cls.TextValue = 'I am a polyline'
        
        # create the shape with the above params
        cls.polyline = roi_primitives.Polyline()
        cls.polyline.setTheT(cls.theT)
        cls.polyline.setTheZ(cls.theZ)
        cls.polyline.setPoints(cls.points)
        cls.polyline.setFillColor(1, 0, 0) # red
        cls.polyline.setFontFamily('serif')
        cls.polyline.setFontSize(12, 'POINT')
        cls.polyline.setFontStyle('italic')
        cls.polyline.setStrokeColor(0, 1, 0) # green
        cls.polyline.setTextValue(cls.TextValue)
        
        # add the shape to the ROI
        cls.roi.addShape(cls.polyline)
        cls.roi = cls.updateService.saveAndReturnObject(cls.roi)
    
    @classmethod
    def tearDownClass(cls):
        super(TestPolyline, cls).tearDownClass()
        
    def setUp(self, *args, **kwargs):
        super(TestPolyline, self).setUp(*args, **kwargs)
    
    def test_added_shape(self):
        self.assertTrue(isinstance(TestPolyline.roi.getShape(0), omero.model.PolylineI))
    
    def test_rectangle_type(self):
        self.assertTrue(isinstance(TestPolyline.polyline, omero.model.PolylineI))
    
    def test_theT(self):
        self.assertEqual(TestPolyline.polyline.getTheT(), TestPolyline.theT)
    
    def test_theZ(self):
        self.assertEqual(TestPolyline.polyline.getTheZ(), TestPolyline.theZ)
    
    def test_textValue(self):
        self.assertEqual(TestPolyline.polyline.getTextValue(), TestPolyline.TextValue)


class TestPolygon(BaseTest):
    @classmethod
    def setUpClass(cls):
        super(TestPolygon, cls).setUpClass()
        cls.theT = 0
        cls.theZ = 50
        cls.points = [(random.randint(0, 100), random.randint(0, 100)) for i in xrange(10)]
        cls.TextValue = 'I am a polygon'
        
        # create the shape with the above params
        cls.polygon = roi_primitives.Polygon()
        cls.polygon.setTheT(cls.theT)
        cls.polygon.setTheZ(cls.theZ)
        cls.polygon.setPoints(cls.points)
        cls.polygon.setFillColor(1, 0, 0) # red
        cls.polygon.setFontFamily('serif')
        cls.polygon.setFontSize(12, 'POINT')
        cls.polygon.setFontStyle('italic')
        cls.polygon.setStrokeColor(0, 1, 0) # green
        cls.polygon.setTextValue(cls.TextValue)
        
        # add the shape to the ROI
        cls.roi.addShape(cls.polygon)
        cls.roi = cls.updateService.saveAndReturnObject(cls.roi)
    
    @classmethod
    def tearDownClass(cls):
        super(TestPolygon, cls).tearDownClass()
        
    def setUp(self, *args, **kwargs):
        super(TestPolygon, self).setUp(*args, **kwargs)
    
    def test_added_shape(self):
        self.assertTrue(isinstance(TestPolygon.roi.getShape(0), omero.model.PolygonI))
    
    def test_rectangle_type(self):
        self.assertTrue(isinstance(TestPolygon.polygon, omero.model.PolygonI))
    
    def test_theT(self):
        self.assertEqual(TestPolygon.polygon.getTheT(), TestPolygon.theT)
    
    def test_theZ(self):
        self.assertEqual(TestPolygon.polygon.getTheZ(), TestPolygon.theZ)
    
    def test_textValue(self):
        self.assertEqual(TestPolygon.polygon.getTextValue(), TestPolygon.TextValue)


class TestLabel(BaseTest):
    @classmethod
    def setUpClass(cls):
        super(TestLabel, cls).setUpClass()
        cls.theT = 0
        cls.theZ = 50
        cls.X = 40
        cls.Y = 54
        cls.TextValue = 'I am a label'
        
        # create the shape with the above params
        cls.label = roi_primitives.Label(theZ=cls.theZ, theT=cls.theT, X=cls.X, Y=cls.Y, label=cls.TextValue)
        cls.label.setFillColor(1, 0, 0) # red
        cls.label.setFontFamily('serif')
        cls.label.setFontSize(12, 'POINT')
        cls.label.setFontStyle('italic')
        cls.label.setStrokeColor(0, 1, 0) # green
        cls.label.setTextValue(cls.TextValue)
        
        # add the shape to the ROI
        cls.roi.addShape(cls.label)
        cls.roi = cls.updateService.saveAndReturnObject(cls.roi)
    
    @classmethod
    def tearDownClass(cls):
        super(TestLabel, cls).tearDownClass()
        
    def setUp(self, *args, **kwargs):
        super(TestLabel, self).setUp(*args, **kwargs)
    
    def test_added_shape(self):
        self.assertTrue(isinstance(TestLabel.roi.getShape(0), omero.model.LabelI))
    
    def test_rectangle_type(self):
        self.assertTrue(isinstance(TestLabel.label, omero.model.LabelI))
    
    def test_theT(self):
        self.assertEqual(TestLabel.label.getTheT(), TestLabel.theT)
    
    def test_theZ(self):
        self.assertEqual(TestLabel.label.getTheZ(), TestLabel.theZ)
    
    def test_textValue(self):
        self.assertEqual(TestLabel.label.getTextValue(), TestLabel.TextValue)


class TestPoint(BaseTest):
    @classmethod
    def setUpClass(cls):
        super(TestPoint, cls).setUpClass()
        cls.theT = 0
        cls.theZ = 50
        cls.Cx = 40
        cls.Cy = 54
        cls.TextValue = 'I am a point'
        
        # create the shape with the above params
        cls.point = roi_primitives.Point(theZ=cls.theZ, theT=cls.theT, Cx=cls.Cx, Cy=cls.Cy)
        cls.point.setFillColor(1, 0, 0) # red
        cls.point.setFontFamily('serif')
        cls.point.setFontSize(12, 'POINT')
        cls.point.setFontStyle('italic')
        cls.point.setStrokeColor(0, 1, 0) # green
        cls.point.setTextValue(cls.TextValue)
        
        # add the shape to the ROI
        cls.roi.addShape(cls.point)
        cls.roi = cls.updateService.saveAndReturnObject(cls.roi)
    
    @classmethod
    def tearDownClass(cls):
        super(TestPoint, cls).tearDownClass()
        
    def setUp(self, *args, **kwargs):
        super(TestPoint, self).setUp(*args, **kwargs)
    
    def test_added_shape(self):
        self.assertTrue(isinstance(TestPoint.roi.getShape(0), omero.model.PointI))
    
    def test_rectangle_type(self):
        self.assertTrue(isinstance(TestPoint.point, omero.model.PointI))
    
    def test_theT(self):
        self.assertEqual(TestPoint.point.getTheT(), TestPoint.theT)
    
    def test_theZ(self):
        self.assertEqual(TestPoint.point.getTheZ(), TestPoint.theZ)
    
    def test_textValue(self):
        self.assertEqual(TestPoint.point.getTextValue(), TestPoint.TextValue)


if __name__ == "__main__":
    unittest.main()